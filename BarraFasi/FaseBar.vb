Imports System.Windows.Forms
Imports System.Drawing

Public Class FaseBar
    Inherits System.Windows.Forms.UserControl

#Region " Codice generato da Progettazione Windows Form "

    Public Sub New()
        MyBase.New()

        'Chiamata richiesta da Progettazione Windows Form.
        InitializeComponent()

        'Aggiungere le eventuali istruzioni di inizializzazione dopo la chiamata a InitializeComponent()

       

        ' Add any initialization after the InitializeComponent() call.
        Me.SetStyle(ControlStyles.AllPaintingInWmPaint, True)
        Me.SetStyle(ControlStyles.DoubleBuffer, True)
        Me.SetStyle(ControlStyles.SupportsTransparentBackColor, True)
        Me.SetStyle(ControlStyles.UserPaint, True)
        Me.SetStyle(ControlStyles.ResizeRedraw, True)

        m_Phases = New Collection

        'now add the default version of the phases. you can do this here, as you must, 
        ' but I would suggest doing it at the form, if you can, so that the developer knows which 
        ' phases are which because they created them. 

        Me.SuspendLayout()

        Dim PhaseProgressBarItem As New PhaseProgressBarItem
        PhaseProgressBarItem.Text = ""
        PhaseProgressBarItem.Font = New Font("Arial", 10, FontStyle.Regular)
        PhaseProgressBarItem.Font_Selected = New Font("Arial", 12, FontStyle.Bold)
        PhaseProgressBarItem.FontColor = Color.FromArgb(&HFF000000)
        PhaseProgressBarItem.FontColor_Selected = Color.FromArgb(&HFF0000FF)
        PhaseProgressBarItem.BackColor = Color.FromArgb(&H78FFFFCC)
        PhaseProgressBarItem.BackColor_Selected = Color.FromArgb(&H78FFFF00)
        PhaseProgressBarItem.BorderColor = Color.Black
        PhaseProgressBarItem.BorderColor_Selected = Color.DodgerBlue
        Me.m_Phases.Add(PhaseProgressBarItem)

        PhaseProgressBarItem = New PhaseProgressBarItem
        PhaseProgressBarItem.Text = ""
        PhaseProgressBarItem.Font = New Font("Arial", 10, FontStyle.Regular)
        PhaseProgressBarItem.Font_Selected = New Font("Arial", 12, FontStyle.Bold)
        PhaseProgressBarItem.FontColor = Color.FromArgb(&HFF000000)
        PhaseProgressBarItem.FontColor_Selected = Color.FromArgb(&HFF0000FF)
        PhaseProgressBarItem.BackColor = Color.FromArgb(&H78FFFFCC)
        PhaseProgressBarItem.BackColor_Selected = Color.FromArgb(&H78FFFF00)
        PhaseProgressBarItem.BorderColor = Color.Black
        PhaseProgressBarItem.BorderColor_Selected = Color.DodgerBlue
        Me.m_Phases.Add(PhaseProgressBarItem)

        PhaseProgressBarItem = New PhaseProgressBarItem
        PhaseProgressBarItem.Text = ""
        PhaseProgressBarItem.Font = New Font("Arial", 10, FontStyle.Regular)
        PhaseProgressBarItem.Font_Selected = New Font("Arial", 12, FontStyle.Bold)
        PhaseProgressBarItem.FontColor = Color.FromArgb(&HFF000000)
        PhaseProgressBarItem.FontColor_Selected = Color.FromArgb(&HFF0000FF)
        PhaseProgressBarItem.BackColor = Color.FromArgb(&H78FFFFCC)
        PhaseProgressBarItem.BackColor_Selected = Color.FromArgb(&H78FFFF00)
        PhaseProgressBarItem.BorderColor = Color.Black
        PhaseProgressBarItem.BorderColor_Selected = Color.DodgerBlue
        Me.m_Phases.Add(PhaseProgressBarItem)

        PhaseProgressBarItem = New PhaseProgressBarItem
        PhaseProgressBarItem.Text = ""
        PhaseProgressBarItem.Font = New Font("Arial", 10, FontStyle.Regular)
        PhaseProgressBarItem.Font_Selected = New Font("Arial", 12, FontStyle.Bold)
        PhaseProgressBarItem.FontColor = Color.FromArgb(&HFF000000)
        PhaseProgressBarItem.FontColor_Selected = Color.FromArgb(&HFF0000FF)
        PhaseProgressBarItem.BackColor = Color.FromArgb(&H78FFFFCC)
        PhaseProgressBarItem.BackColor_Selected = Color.FromArgb(&H78FFFF00)
        PhaseProgressBarItem.BorderColor = Color.Black
        PhaseProgressBarItem.BorderColor_Selected = Color.DodgerBlue
        Me.m_Phases.Add(PhaseProgressBarItem)

        PhaseProgressBarItem = New PhaseProgressBarItem
        PhaseProgressBarItem.Text = ""
        PhaseProgressBarItem.Font = New Font("Arial", 10, FontStyle.Regular)
        PhaseProgressBarItem.Font_Selected = New Font("Arial", 12, FontStyle.Bold)
        PhaseProgressBarItem.FontColor = Color.FromArgb(&HFF000000)
        PhaseProgressBarItem.FontColor_Selected = Color.FromArgb(&HFF0000FF)
        PhaseProgressBarItem.BackColor = Color.FromArgb(&H78FFFFCC)
        PhaseProgressBarItem.BackColor_Selected = Color.FromArgb(&H78FFFF00)
        PhaseProgressBarItem.BorderColor = Color.Black
        PhaseProgressBarItem.BorderColor_Selected = Color.DodgerBlue
        Me.m_Phases.Add(PhaseProgressBarItem)

        PhaseProgressBarItem = New PhaseProgressBarItem
        PhaseProgressBarItem.Text = ""
        PhaseProgressBarItem.Font = New Font("Arial", 10, FontStyle.Regular)
        PhaseProgressBarItem.Font_Selected = New Font("Arial", 12, FontStyle.Bold)
        PhaseProgressBarItem.FontColor = Color.FromArgb(&HFF000000)
        PhaseProgressBarItem.FontColor_Selected = Color.FromArgb(&HFF0000FF)
        PhaseProgressBarItem.BackColor = Color.FromArgb(&H78FFFFCC)
        PhaseProgressBarItem.BackColor_Selected = Color.FromArgb(&H78FFFF00)
        PhaseProgressBarItem.BorderColor = Color.Black
        PhaseProgressBarItem.BorderColor_Selected = Color.DodgerBlue
        Me.m_Phases.Add(PhaseProgressBarItem)

        PhaseProgressBarItem = New PhaseProgressBarItem
        PhaseProgressBarItem.Text = ""
        PhaseProgressBarItem.Font = New Font("Arial", 10, FontStyle.Regular)
        PhaseProgressBarItem.Font_Selected = New Font("Arial", 12, FontStyle.Bold)
        PhaseProgressBarItem.FontColor = Color.FromArgb(&HFF000000)
        PhaseProgressBarItem.FontColor_Selected = Color.FromArgb(&HFF0000FF)
        PhaseProgressBarItem.BackColor = Color.FromArgb(&H78FFFFCC)
        PhaseProgressBarItem.BackColor_Selected = Color.FromArgb(&H78FFFF00)
        PhaseProgressBarItem.BorderColor = Color.Black
        PhaseProgressBarItem.BorderColor_Selected = Color.DodgerBlue
        Me.m_Phases.Add(PhaseProgressBarItem)

        'PhaseProgressBarItem = New PhaseProgressBarItem
        'PhaseProgressBarItem.Text = "Happy New Year"
        'PhaseProgressBarItem.BackColor = Color.FromArgb(&H78FFFFCC)
        'PhaseProgressBarItem.BackColor_Selected = Color.FromArgb(&H78FFBBAA)
        ''PhaseProgressBarItem.BackColor = Color.FromArgb(&H78000000)
        'PhaseProgressBarItem.FontColor = Color.FromArgb(&HFF000000)
        'PhaseProgressBarItem.FontColor_Selected = Color.FromArgb(&HFFCC00FF)
        'Me.m_Phases.Add(PhaseProgressBarItem)

        Me.ResumeLayout()

        'now create a timer so that the blinking effect will work as expected
        cycle = New Timer
        cycle.Interval = Me.m_CycleSpeed
        cycle.Enabled = Me.Enabled

    End Sub

    'UserControl esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue � richiesta da Progettazione Windows Form.
    'Pu� essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        '
        'FaseBar
        '
        Me.Name = "FaseBar"
        Me.Size = New System.Drawing.Size(696, 64)

    End Sub

#End Region






#Region "Private Variable Declarations"

    Dim m_BorderStyle As Border3DStyle = Border3DStyle.Flat
    Dim m_Phases As Collection
    Dim m_PointLength As Integer = 15
    Dim m_TextBuffer As Integer = 10
    Dim m_TextBufferForCentering As Integer = 0 'this is for the extra space to fill in the controls overall bound area
    Dim m_SelectedIndex As Integer = 0
    Dim m_CycleSpeed As Integer = 500 'for the blink effect
    Dim m_fdraw As Boolean
    Dim WithEvents cycle As Timer

#End Region

#Region "Description"
    'in here, we create each one of the tabs.  
    'tabs should look like: 
    ' -------------- --------------------- -----------------     ---------------       --------- ------------
    ' |              \      ALL           \                |     |             |      |         \           |
    ' |   First       \    Middle          \     Last      |     |  Only One   |      |  First   \  Last    |
    ' |   Tab         /     Tabs           /     Tab       |  OR |     Tab     |  OR  |   Tab    /   Tab    |
    ' |              /                    /                |     |             |      |         /           |
    ' ------------- ---------------------- ----------------      ---------------      ---------- ------------
    '
    ' Each tab should exactly match the bordering tabs because the OnClick event uses the polygon's points
    ' to determine which "tab" is clicked

#End Region

#Region "Properties"

#Region "Public Properties"

    Public Function AddPhase(ByVal PhaseItem As PhaseProgressBarItem) As Boolean
        'return true of the phase was added successfully
        m_Phases.Add(PhaseItem)
        AddPhase = True
    End Function

    Public Property Phases() As Collection
        Get
            Phases = m_Phases
        End Get
        Set(ByVal value As Collection)
            m_Phases = value
        End Set
    End Property

    <System.ComponentModel.Category("Appearance")> _
    Public Property CycleSpeed() As Integer
        Get
            CycleSpeed = m_CycleSpeed
        End Get
        Set(ByVal value As Integer)
            m_CycleSpeed = value
        End Set
    End Property
    Public Property SelectedIndex() As Integer
        Get
            SelectedIndex = m_SelectedIndex
        End Get
        Set(ByVal value As Integer)
            m_SelectedIndex = value
        End Set
    End Property

    <System.ComponentModel.Category("Appearance")> _
    Public Property TextBuffer() As Integer
        Get
            TextBuffer = m_TextBuffer
        End Get
        Set(ByVal value As Integer)
            m_TextBuffer = value
        End Set
    End Property

    <System.ComponentModel.Category("Appearance")> _
    Public Property PointLength() As Integer
        Get
            PointLength = m_PointLength
        End Get
        Set(ByVal value As Integer)
            m_PointLength = value
        End Set
    End Property

    <System.ComponentModel.Category("Appearance")> _
    Public Property BorderStyle() As Border3DStyle
        Get
            Return m_BorderStyle
        End Get
        Set(ByVal Value As Border3DStyle)
            If Value <> m_BorderStyle Then
                m_BorderStyle = Value
                Me.Invalidate()
            End If
        End Set
    End Property

    <System.ComponentModel.Category("Appearance")> Public Property FDraw() As Boolean
        Get
            Return m_fdraw
        End Get
        Set(ByVal value As Boolean)
            m_fdraw = value
        End Set
    End Property


#End Region


#Region "Private Properties"

    Private Property TextBufferForCentering() As Integer
        Get
            TextBufferForCentering = m_TextBufferForCentering
        End Get
        Set(ByVal value As Integer)
            m_TextBufferForCentering = value
        End Set
    End Property

#End Region

#End Region


    Public Function clearPolygons() As Boolean
        Dim returnValue As Boolean = True
        For ii As Int16 = 1 To Me.Phases.Count
            Me.Phases.Remove(1)
        Next ii
        clearPolygons = returnValue
    End Function

    Protected Overrides Sub OnClick(ByVal e As System.EventArgs)
        'Dim MouseX, MouseY As Single
        'Dim LocalMousePosition As Point
        'LocalMousePosition = Me.PointToClient(Me.MousePosition)
        'MouseX = LocalMousePosition.X
        'MouseY = LocalMousePosition.Y

        'For i As Integer = 1 To m_Phases.Count
        '    If pointInPolygon(MouseX, MouseY, i) Then
        '        Me.SelectedIndex = i
        '        Me.Invalidate()
        '        m_Phases.Item(i).IsSelected = True
        '    Else
        '        m_Phases.Item(i).IsSelected = False
        '    End If
        'Next i

        'MyBase.OnClick(e)
    End Sub

    'Determines if the mouse's click fell inside the polygon passed
    Private Function pointInPolygon(ByVal Xin As Single, ByVal Yin As Single, ByVal ItemNumber As Integer) As Boolean
        Dim oddNODES As Boolean = False
        Dim i As Int32 = 0 'Point1 Reference
        Dim j As Int32 = 0 'Point2 Reference
        Dim polysides As Integer 'number of sides

        'This algorithm was adapted from an online article
        'by Darel Rex Finley
        'http://alienryderflex.com/polygon/
        'The original code was in C++ and used global variable arrays.

        polysides = UBound(m_Phases.Item(ItemNumber).PolyPoints) + 1 'zero based, so ubound will return element offset, not number of points
        For i = 0 To polysides - 1 'Point1
            j += 1 'increment Point2 to the next point in the polygon
            If (j = polysides) Then
                j = 0 'this will move Point2 to the first point in the polygon
            End If

            If (m_Phases.Item(ItemNumber).polyPoints(i).Y < Yin And m_Phases.Item(ItemNumber).polyPoints(j).Y >= Yin) Or (m_Phases.Item(ItemNumber).polyPoints(j).Y < Yin And m_Phases.Item(ItemNumber).polyPoints(i).Y >= Yin) Then
                If (m_Phases.Item(ItemNumber).polyPoints(i).X + (Yin - m_Phases.Item(ItemNumber).polyPoints(i).Y) / (m_Phases.Item(ItemNumber).polyPoints(j).Y - m_Phases.Item(ItemNumber).polyPoints(i).Y) * (m_Phases.Item(ItemNumber).polyPoints(j).X - m_Phases.Item(ItemNumber).polyPoints(i).X)) < Xin Then
                    oddNODES = Not oddNODES
                End If
            Else
            End If
        Next i

        pointInPolygon = oddNODES
    End Function

    Protected Overrides Sub OnPaint(ByVal e As System.Windows.Forms.PaintEventArgs)
        'Every time we have an Invalidate() or just a basic redraw, we need to repaint the whole control
        '1. Draw the border first so it falls into the background
        '2. Determine the control's extra space to be divided equally to all polygons for text centering
        '3. Draw each polygon from last (right most) polygon to the first (leftmost) 
        '  3a.  Determine current Polygon's points
        '  3b.  Draw (as fill) the polygon's background color 
        '  3c.  Draw (as line) the polygon's border color
        '  3d.  Draw (as line) the polygon's text

        Dim g As Graphics = e.Graphics
        Dim pPen As Pen
        Dim penColor As Color
        Dim fFont As Font
        Dim bFontBrush As SolidBrush
        Dim bBrush As SolidBrush
        Dim xForText As Single = 0
        Dim yForText As Single = 0
        Dim sText As String
        Dim mloop As Integer
        ' disegno solamente quando  ho finito di inserire fasi 
        If m_fdraw Then




            If m_Phases.Count > 0 Then
                ControlPaint.DrawBorder3D(g, Me.ClientRectangle, Me.BorderStyle)
                DetermineTextCenterBuffer(g)
                mloop = 1

                For i As Integer = m_Phases.Count To mloop Step -1 ' To m_Phases.Count
                    'now set the colors 
                    penColor = m_Phases.Item(i).CurrentBorderColor
                    pPen = New Pen(penColor, 2)
                    bFontBrush = New SolidBrush(m_Phases.Item(i).CurrentFontColor)
                    bBrush = New SolidBrush(m_Phases.Item(i).CurrentBackColor)
                    fFont = m_Phases.Item(i).CurrentFont
                    sText = m_Phases.Item(i).Text

                    m_Phases.Item(i).PolyPoints = DeterminePhasePoly(g, i)

                    g.FillPolygon(bBrush, m_Phases.Item(i).PolyPoints)
                    g.DrawPolygon(pPen, m_Phases.Item(i).PolyPoints)

                    ' Debug.Print(i.ToString & ":" & sText)

                    'now we need to determine which tab type this is before putting the text in place. 
                    'for the "only one" tab, we flat left, and flat right side.
                    '   only leftmost x point (point(0).x), 1/2 of the text buffer and 1/2 textbuffer for control center
                    'for the "first" tab, we have a flat left, and arrow out right.
                    '   need leftmost x point (point(0).x), 1/2 text buffer, 1/2 textbuffer for control center, and 1/3 arrow out
                    'for the "middle" tab, we have arrow in left, and arrow out right. 
                    '   need leftmost x point (point(0).x), 1/2 text buffer, 1/2 textbuffer for control center, arrow in length, and 1/3 arrow out
                    'for the "last" tab, we have arrow in left, and flat right.
                    '   need leftmost x point (point(0).x), 1/2 text buffer, 1/2 textbuffer for control center, arrow in length

                    If i = m_Phases.Count And i = 1 Then 'only one tab
                        xForText = m_Phases.Item(i).polypoints(0).X + (Me.TextBuffer / 2) + (Me.TextBufferForCentering / 2)
                        yForText = (Me.Height - fFont.Height) / 4
                    ElseIf i = m_Phases.Count Then 'last tab
                        xForText = m_Phases.Item(i).polypoints(0).X + (Me.TextBuffer / 2) + (Me.TextBufferForCentering / 2) + Me.PointLength
                        yForText = (Me.Height - fFont.Height) / 4
                    ElseIf i = 1 Then 'first tab
                        xForText = m_Phases.Item(i).polypoints(0).X + (Me.TextBuffer / 2) + (Me.TextBufferForCentering / 2) + (Me.PointLength / 3)
                        yForText = (Me.Height - fFont.Height) / 4
                    Else 'all middle tabs 
                        xForText = m_Phases.Item(i).polypoints(0).X + (Me.TextBuffer / 2) + (Me.TextBufferForCentering / 2) + Me.PointLength + (Me.PointLength / 3)
                        yForText = (Me.Height - fFont.Height) / 4
                    End If

                    'draw the text for this polygon in its proper location
                    g.DrawString(sText, fFont, bFontBrush, xForText, yForText)

                Next
            End If
            
            
            MyBase.OnPaint(e)
        End If
    End Sub

    Private Function DeterminePhasePoly(ByVal g As Graphics, ByVal ItemPosition As Integer) As Point()
        'This determines the Polygon's Points for each type of tab to be generated
        'based on how many polygons to need to be generated.
        Dim sF As SizeF
        Dim startPoint As Single = 0
        Dim sFThis As SizeF
        Dim iPrevious As Integer
        Dim Points(1) As Point
        Dim Hiniziale As Integer
        Dim Hfinale As Integer
        
        'this is the measurement of the current text
        sFThis = g.MeasureString(m_Phases.Item(ItemPosition).Text, m_Phases.Item(ItemPosition).CurrentFont)
       
        For i As Integer = 2 To ItemPosition
            'measure the previous poly's text width
            iPrevious = i - 1
            sF = g.MeasureString(m_Phases.Item(iPrevious).Text, m_Phases.Item(iPrevious).CurrentFont)
            If iPrevious = 1 Or iPrevious = 15 Then 'first tab
                startPoint += sF.Width + Me.TextBuffer + Me.TextBufferForCentering + 2
            Else ' middle tabs 
                startPoint += sF.Width + Me.TextBuffer + Me.TextBufferForCentering + Me.PointLength + 2
            End If
        Next i

     
        Hfinale = Me.Height
        

        If ItemPosition = m_Phases.Count And (ItemPosition = 1) Then 'only one tab
            'tab should look like: 
            '  -----------------
            '  |      Only      |
            '  |      One       | 
            '  |      Tab       | 
            '  |                |  
            '  -----------------
            '
            ReDim Points(3)
            Points(0) = New Point(startPoint, 0)
            Points(1) = New Point(startPoint + sFThis.Width + Me.TextBuffer + Me.TextBufferForCentering, 0)
            Points(2) = New Point(startPoint + sFThis.Width + Me.TextBuffer + Me.TextBufferForCentering, Me.Height)
            Points(3) = New Point(startPoint, Me.Height)

        ElseIf (ItemPosition = m_Phases.Count) Then 'last tab
            'tab should look like: 
            '  -----------------
            '  \                |
            '   \     Last      | 
            '   /     Tab       | 
            '  /                |  
            '  -----------------
            '
            ReDim Points(4)
            Points(0) = New Point(startPoint, Hiniziale)
            Points(1) = New Point(startPoint + sFThis.Width + Me.TextBuffer + Me.TextBufferForCentering + Me.PointLength, Hiniziale)
            Points(2) = New Point(startPoint + sFThis.Width + Me.TextBuffer + Me.TextBufferForCentering + Me.PointLength, Hfinale)
            Points(3) = New Point(startPoint, Hfinale)
            Points(4) = New Point(startPoint + Me.PointLength, Hfinale / 2)

        ElseIf ItemPosition = 1 Then 'first tab
            'tab should look like: 
            ' -------------- 
            ' |              \
            ' |   First       \
            ' |   Tab         /
            ' |              / 
            ' -------------- 
            '
            ReDim Points(4)
            Points(0) = New Point(startPoint, Hiniziale)
            Points(1) = New Point(startPoint + sFThis.Width + Me.TextBuffer + Me.TextBufferForCentering, Hiniziale)
            Points(2) = New Point(startPoint + sFThis.Width + Me.TextBuffer + Me.TextBufferForCentering + Me.PointLength, Hfinale / 2)
            Points(3) = New Point(startPoint + sFThis.Width + Me.TextBuffer + Me.TextBufferForCentering, Hfinale)
            Points(4) = New Point(startPoint, Hfinale)

        Else 'all middle tabs
            'tab should look like: 
            ' --------------------- 
            '  \      ALL           \ 
            '   \    Middle          \
            '   /     Tabs           /
            '  /                    /
            ' ----------------------
            '
            ReDim Points(5)
            Points(0) = New Point(startPoint, Hiniziale)
            Points(1) = New Point(startPoint + sFThis.Width + Me.TextBuffer + Me.TextBufferForCentering + Me.PointLength, Hiniziale)
            Points(2) = New Point(startPoint + sFThis.Width + Me.TextBuffer + Me.TextBufferForCentering + (Me.PointLength * 2), Hfinale / 2)
            Points(3) = New Point(startPoint + sFThis.Width + Me.TextBuffer + Me.TextBufferForCentering + Me.PointLength, Hfinale)
            Points(4) = New Point(startPoint, Hfinale)
            Points(5) = New Point(startPoint + Me.PointLength, Hfinale / 2)

        End If

        DeterminePhasePoly = Points

    End Function

    Private Function DetermineTextCenterBuffer(ByVal g As Graphics) As Boolean
        'As the control can be bigger than the entire length of the phases, 
        ' we need to get that extra space and evenly distribute it to 
        ' all the phases. 
        Dim returnValue As Boolean = True
        Dim overallLength As Single = Me.Width
        Dim phasesLength As Single = 0
        Dim sFThis As SizeF

        For i As Integer = 1 To m_Phases.Count
            sFThis = g.MeasureString(m_Phases.Item(i).Text, m_Phases.Item(i).CurrentFont)
            If i = 1 And m_Phases.Count = i Then 'one tab
                phasesLength += sFThis.Width + Me.TextBuffer + 2
            ElseIf i = m_Phases.Count Then 'last tab
                phasesLength += sFThis.Width + Me.TextBuffer + Me.PointLength + 2
            ElseIf i = 1 Then 'first tab
                phasesLength += sFThis.Width + Me.TextBuffer + 2
            Else 'middle tabs
                phasesLength += sFThis.Width + Me.TextBuffer + Me.PointLength + 2
            End If
        Next


        Me.TextBufferForCentering = (overallLength - phasesLength) / m_Phases.Count

        DetermineTextCenterBuffer = returnValue
    End Function

    Private Sub cycle_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles cycle.Tick
        'just change the BlinkState of the blinking items, and invalidate control for repaint
        'this is set up to allow for multiple items to be blinking at the same time.
        For i As Integer = 1 To Me.m_Phases.Count
            If Me.m_Phases.Item(i).IsBlinking Then
                Me.m_Phases.Item(i).BlinkState = Not Me.m_Phases.Item(i).BlinkState
                Me.Invalidate() 'force repaint
            End If
        Next
    End Sub

    Private Sub FaseBar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Click

    End Sub

    Private Sub FaseBar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class
