Imports System.Drawing

<Serializable()> _
Public Class PhaseProgressBarItem

    Dim m_Text As String = ""
    Dim m_IsSelected As Boolean = False
    Dim m_IsBlinking As Boolean = False
    Dim m_BlinkState As Boolean = False
    Dim m_Font As Font = New Font("Arial", 8)
    Dim m_Font_Selected As Font = New Font("Arial", 8)
    Dim m_FontColor As Color = Color.Black
    Dim m_FontColor_Selected As Color = Color.Blue
    Dim m_BackColor As Color = Color.FromArgb(&H78FFFFCC)
    Dim m_BackColor_Selected As Color = Color.FromArgb(&H78FFFF00)
    Dim m_BorderColor As Color = Color.Black
    Dim m_BorderColor_Selected As Color = Color.Teal
    Dim m_PolyPoints As Point() = {New Point(0, 0), New Point(1, 1), New Point(2, 2), New Point(3, 3), New Point(4, 4), New Point(5, 5)}
    Dim m_SetState As Short

    Public Property SetState() As Short
        Get
            SetState = m_SetState
        End Get
        Set(ByVal Value As Short)
            m_SetState = Value

            If m_SetState = 1 Then
                ' ok
                m_FontColor_Selected = Color.Blue
                m_BackColor_Selected = Color.GreenYellow
            ElseIf m_SetState = 0 Then
                ' ko
                m_FontColor_Selected = Color.Blue
                m_BackColor_Selected = Color.Red
            ElseIf m_SetState = 2 Then
                ' in esecuzione
                m_FontColor_Selected = Color.Blue
                m_BackColor_Selected = Color.WhiteSmoke
            End If

        End Set
    End Property


    <System.ComponentModel.Category("Appearance")> _
    Public Property BlinkState() As Boolean
        Get
            BlinkState = m_BlinkState
        End Get
        Set(ByVal value As Boolean)
            m_BlinkState = value
        End Set
    End Property

    <System.ComponentModel.Category("Custom")> _
    Public Property IsSelected() As Boolean
        Get
            IsSelected = m_IsSelected
        End Get
        Set(ByVal value As Boolean)
            m_IsSelected = value
        End Set
    End Property

    <System.ComponentModel.Category("Appearance")> _
    Public ReadOnly Property CurrentBorderColor() As Color
        Get
            'If Me.IsSelected Or (Me.IsBlinking And Me.BlinkState) Then
            If Me.IsSelected Or (Me.IsBlinking And Me.BlinkState) Then
                CurrentBorderColor = m_BorderColor_Selected
            Else
                CurrentBorderColor = m_BorderColor
            End If
        End Get
    End Property

    <System.ComponentModel.Category("Appearance")> _
    Public Property BorderColor_Selected() As Color
        Get
            BorderColor_Selected = m_BorderColor_Selected
        End Get
        Set(ByVal value As Color)
            m_BorderColor_Selected = value
        End Set
    End Property


    <System.ComponentModel.Category("Appearance")> _
    Public Property BorderColor() As Color
        Get
            BorderColor = m_BorderColor
        End Get
        Set(ByVal value As Color)
            m_BorderColor = value
        End Set
    End Property

    <System.ComponentModel.Category("Appearance")> _
    Public Property PolyPoints() As Point()
        Get
            PolyPoints = m_PolyPoints
        End Get
        Set(ByVal value As Point())
            ReDim m_PolyPoints(UBound(value))
            Array.Copy(value, m_PolyPoints, UBound(value) + 1)
        End Set
    End Property

    <System.ComponentModel.Category("Appearance")> _
    Public Property Text() As String
        Get
            Text = m_Text
        End Get
        Set(ByVal value As String)
            m_Text = value
        End Set
    End Property

    <System.ComponentModel.Category("Appearance")> _
    Public ReadOnly Property CurrentFontColor() As Color
        Get
            'If (Me.IsBlinking And Me.BlinkState) Then
            '    Debug.Assert(False)
            'End If
            If Me.IsSelected Or (Me.IsBlinking And Me.BlinkState) Then
                CurrentFontColor = m_FontColor_Selected
            Else
                CurrentFontColor = m_FontColor
            End If
        End Get
    End Property

    <System.ComponentModel.Category("Appearance")> _
    Public Property FontColor_Selected() As Color
        Get
            FontColor_Selected = m_FontColor_Selected
        End Get
        Set(ByVal value As Color)
            m_FontColor_Selected = value
        End Set
    End Property

    <System.ComponentModel.Category("Appearance")> _
    Public Property FontColor() As Color
        Get
            FontColor = m_FontColor
        End Get
        Set(ByVal value As Color)
            m_FontColor = value
        End Set
    End Property

    <System.ComponentModel.Category("Appearance")> _
    Public Property IsBlinking() As Boolean
        Get
            IsBlinking = m_IsBlinking
        End Get
        Set(ByVal value As Boolean)
            m_IsBlinking = value
        End Set
    End Property

    <System.ComponentModel.Category("Appearance")> _
    Public ReadOnly Property CurrentFont() As Font
        Get
            'If Me.IsSelected Or (Me.IsBlinking And Me.BlinkState) Then
            If Me.IsSelected Then
                CurrentFont = m_Font_Selected
            Else
                CurrentFont = m_Font
            End If
        End Get
    End Property

    <System.ComponentModel.Category("Appearance")> _
    Public Property Font_Selected() As Font
        Get
            Font_Selected = m_Font_Selected
        End Get
        Set(ByVal value As Font)
            m_Font_Selected = value
        End Set
    End Property

    <System.ComponentModel.Category("Appearance")> _
    Public Property Font() As Font
        Get
            Font = m_Font
        End Get
        Set(ByVal value As Font)
            m_Font = value
        End Set
    End Property

    <System.ComponentModel.Category("Appearance")> _
    Public ReadOnly Property CurrentBackColor() As Color
        Get
            If Me.IsSelected Or (Me.IsBlinking And Me.BlinkState) Then
                CurrentBackColor = m_BackColor_Selected
            Else
                CurrentBackColor = m_BackColor
            End If
        End Get
    End Property

    <System.ComponentModel.Category("Appearance")> _
    Public Property BackColor_Selected() As Color
        Get
            BackColor_Selected = m_BackColor_Selected
        End Get
        Set(ByVal value As Color)
            m_BackColor_Selected = value
        End Set
    End Property

    <System.ComponentModel.Category("Appearance")> _
    Public Property BackColor() As Color
        Get
            BackColor = m_BackColor
        End Get
        Set(ByVal value As Color)
            m_BackColor = value
        End Set
    End Property

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Public Sub New()

    End Sub
End Class
