﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FAbilitaOp
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim OperatoreLabel As System.Windows.Forms.Label
        Dim PasswordLabel As System.Windows.Forms.Label
        Me.Collaudi02DataSet = New CollaudoPeriferiche.Collaudi02DataSet
        Me.OperatoriBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.OperatoriTableAdapter = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.OperatoriTableAdapter
        Me.TableAdapterManager = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.TableAdapterManager
        Me.Button1 = New System.Windows.Forms.Button
        Me.PasswordTextBox = New System.Windows.Forms.TextBox
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.UscitaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        OperatoreLabel = New System.Windows.Forms.Label
        PasswordLabel = New System.Windows.Forms.Label
        CType(Me.Collaudi02DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OperatoriBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'OperatoreLabel
        '
        OperatoreLabel.AutoSize = True
        OperatoreLabel.Location = New System.Drawing.Point(35, 27)
        OperatoreLabel.Name = "OperatoreLabel"
        OperatoreLabel.Size = New System.Drawing.Size(57, 13)
        OperatoreLabel.TabIndex = 1
        OperatoreLabel.Text = "Operatore:"
        OperatoreLabel.Visible = False
        '
        'PasswordLabel
        '
        PasswordLabel.AutoSize = True
        PasswordLabel.Location = New System.Drawing.Point(36, 67)
        PasswordLabel.Name = "PasswordLabel"
        PasswordLabel.Size = New System.Drawing.Size(56, 13)
        PasswordLabel.TabIndex = 3
        PasswordLabel.Text = "Password:"
        '
        'Collaudi02DataSet
        '
        Me.Collaudi02DataSet.DataSetName = "Collaudi02DataSet"
        Me.Collaudi02DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'OperatoriBindingSource
        '
        Me.OperatoriBindingSource.DataMember = "Operatori"
        Me.OperatoriBindingSource.DataSource = Me.Collaudi02DataSet
        '
        'OperatoriTableAdapter
        '
        Me.OperatoriTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AutenticazioneTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.CategorieOpzioniTableAdapter = Nothing
        Me.TableAdapterManager.DettaglioOrdineTableAdapter = Nothing
        Me.TableAdapterManager.OperatoriTableAdapter = Me.OperatoriTableAdapter
        Me.TableAdapterManager.SequenzaCollaudoTableAdapter = Nothing
        Me.TableAdapterManager.SequenzaTableAdapter = Nothing
        Me.TableAdapterManager.SequOperaTableAdapter = Nothing
        Me.TableAdapterManager.T_Coll_FaseTableAdapter = Nothing
        Me.TableAdapterManager.T_CollaudoTableAdapter = Nothing
        Me.TableAdapterManager.T_DatiFineCollaudoTableAdapter = Nothing
        Me.TableAdapterManager.T_FasicollaudoTableAdapter = Nothing
        Me.TableAdapterManager.T_FirmwareTableAdapter = Nothing
        Me.TableAdapterManager.T_ImmaginiTableAdapter = Nothing
        Me.TableAdapterManager.T_Mess_PerifTableAdapter = Nothing
        Me.TableAdapterManager.T_MessaggiTableAdapter = Nothing
        Me.TableAdapterManager.T_OrdineTableAdapter = Nothing
        Me.TableAdapterManager.T_PerifericaTableAdapter = Nothing
        Me.TableAdapterManager.T_Staz_CollTableAdapter = Nothing
        Me.TableAdapterManager.T_StoricoTableAdapter = Nothing
        Me.TableAdapterManager.T_TipoDatoTableAdapter = Nothing
        Me.TableAdapterManager.T_VersioniTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = CollaudoPeriferiche.Collaudi02DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.ValoriOpzioniTableAdapter = Nothing
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Button1.Location = New System.Drawing.Point(89, 101)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 46)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Abilita"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'PasswordTextBox
        '
        Me.PasswordTextBox.Location = New System.Drawing.Point(98, 64)
        Me.PasswordTextBox.Name = "PasswordTextBox"
        Me.PasswordTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.PasswordTextBox.Size = New System.Drawing.Size(121, 20)
        Me.PasswordTextBox.TabIndex = 4
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UscitaToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(257, 24)
        Me.MenuStrip1.TabIndex = 5
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'UscitaToolStripMenuItem
        '
        Me.UscitaToolStripMenuItem.Name = "UscitaToolStripMenuItem"
        Me.UscitaToolStripMenuItem.Size = New System.Drawing.Size(51, 20)
        Me.UscitaToolStripMenuItem.Text = "Uscita"
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.OperatoriBindingSource
        Me.ComboBox1.DisplayMember = "Operatore"
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(98, 28)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(121, 21)
        Me.ComboBox1.TabIndex = 6
        Me.ComboBox1.ValueMember = "Id_Operatore"
        Me.ComboBox1.Visible = False
        '
        'FAbilitaOp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(257, 169)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(PasswordLabel)
        Me.Controls.Add(Me.PasswordTextBox)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(OperatoreLabel)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "FAbilitaOp"
        Me.Text = "Abilita Operatore"
        CType(Me.Collaudi02DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OperatoriBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Collaudi02DataSet As CollaudoPeriferiche.Collaudi02DataSet
    Friend WithEvents OperatoriBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents OperatoriTableAdapter As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.OperatoriTableAdapter
    Friend WithEvents TableAdapterManager As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.TableAdapterManager
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents PasswordTextBox As System.Windows.Forms.TextBox
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents UscitaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
End Class
