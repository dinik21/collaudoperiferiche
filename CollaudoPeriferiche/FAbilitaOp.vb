﻿Public Class FAbilitaOp
    Public OperatoreSelezionato As DataRowView
    Private Sub FAbilitaOp_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Form1.fServerPresente = True Then

            'TODO: questa riga di codice carica i dati nella tabella 'Collaudi02DataSet.Operatori'. È possibile spostarla o rimuoverla se necessario.
            Me.OperatoriTableAdapter.FillByused(Me.Collaudi02DataSet.Operatori)
        Else
            Console.WriteLine("Prendo i dati da locale")

            Collaudi02DataSet.Operatori.ReadXml(Application.StartupPath + Form1.LOCALDB + Form1.T_OPERATORI)
        End If
    End Sub

    Private Sub OperatoriBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
     
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim r As Int16
        If Form1.fServerPresente = True Then

            Try
                OperatoreSelezionato = Me.OperatoriBindingSource.Current
                If OperatoreSelezionato("Password") = PasswordTextBox.Text Then
                    ' ho messo la psw corretta , alzo il flag di operatore utilizzato
                    Me.OperatoriBindingSource.Current("Used") = False

                    r = Me.OperatoriTableAdapter.Update(Collaudi02DataSet.Operatori)

                    Me.OperatoriBindingSource.EndEdit()
                    Me.TableAdapterManager.UpdateAll(Me.Collaudi02DataSet)
                    Me.Close()
                Else
                    MessageBox.Show("Password ERRATA")
                End If
            Catch ex As Exception
                MessageBox.Show("Attenzione Connessione di Rete non Presente !!! RIAVVIARE IL PROGRAMMA", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        Else
            Me.Close()
        End If

    End Sub

    Private Sub UscitaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UscitaToolStripMenuItem.Click
        Me.Close()
    End Sub
End Class
