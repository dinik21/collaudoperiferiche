﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FBraSile
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.C_Files = New System.Windows.Forms.ComboBox
        Me.LNom = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.C_Firmware = New System.Windows.Forms.ComboBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.T_Operatore = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.L_Linea = New System.Windows.Forms.Label
        Me.L_Or = New System.Windows.Forms.Label
        Me.L_Ori = New System.Windows.Forms.Label
        Me.L_Produttore = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.L_Modello = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'C_Files
        '
        Me.C_Files.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.C_Files.FormattingEnabled = True
        Me.C_Files.Location = New System.Drawing.Point(12, 46)
        Me.C_Files.Name = "C_Files"
        Me.C_Files.Size = New System.Drawing.Size(455, 28)
        Me.C_Files.TabIndex = 0
        '
        'LNom
        '
        Me.LNom.AutoSize = True
        Me.LNom.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LNom.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.LNom.Location = New System.Drawing.Point(192, 9)
        Me.LNom.Name = "LNom"
        Me.LNom.Size = New System.Drawing.Size(85, 25)
        Me.LNom.TabIndex = 1
        Me.LNom.Text = "LNome"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label1.Location = New System.Drawing.Point(192, 85)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(108, 25)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Firmware"
        '
        'C_Firmware
        '
        Me.C_Firmware.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.C_Firmware.FormattingEnabled = True
        Me.C_Firmware.Location = New System.Drawing.Point(12, 113)
        Me.C_Firmware.Name = "C_Firmware"
        Me.C_Firmware.Size = New System.Drawing.Size(455, 28)
        Me.C_Firmware.TabIndex = 3
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.LightGreen
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(163, 281)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(137, 52)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Inizia Collaudo"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label2.Location = New System.Drawing.Point(26, 251)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 18)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Operatore"
        '
        'T_Operatore
        '
        Me.T_Operatore.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.T_Operatore.Location = New System.Drawing.Point(207, 251)
        Me.T_Operatore.Name = "T_Operatore"
        Me.T_Operatore.Size = New System.Drawing.Size(195, 24)
        Me.T_Operatore.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label3.Location = New System.Drawing.Point(25, 159)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(157, 18)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Linea di Produzione"
        '
        'L_Linea
        '
        Me.L_Linea.AutoSize = True
        Me.L_Linea.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.L_Linea.Location = New System.Drawing.Point(204, 161)
        Me.L_Linea.Name = "L_Linea"
        Me.L_Linea.Size = New System.Drawing.Size(57, 18)
        Me.L_Linea.TabIndex = 8
        Me.L_Linea.Text = "Label4"
        '
        'L_Or
        '
        Me.L_Or.AutoSize = True
        Me.L_Or.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.L_Or.Location = New System.Drawing.Point(204, 183)
        Me.L_Or.Name = "L_Or"
        Me.L_Or.Size = New System.Drawing.Size(57, 18)
        Me.L_Or.TabIndex = 10
        Me.L_Or.Text = "Label4"
        '
        'L_Ori
        '
        Me.L_Ori.AutoSize = True
        Me.L_Ori.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.L_Ori.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.L_Ori.Location = New System.Drawing.Point(25, 181)
        Me.L_Ori.Name = "L_Ori"
        Me.L_Ori.Size = New System.Drawing.Size(58, 18)
        Me.L_Ori.TabIndex = 9
        Me.L_Ori.Text = "Ordine"
        '
        'L_Produttore
        '
        Me.L_Produttore.AutoSize = True
        Me.L_Produttore.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.L_Produttore.Location = New System.Drawing.Point(204, 205)
        Me.L_Produttore.Name = "L_Produttore"
        Me.L_Produttore.Size = New System.Drawing.Size(57, 18)
        Me.L_Produttore.TabIndex = 12
        Me.L_Produttore.Text = "Label4"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label6.Location = New System.Drawing.Point(25, 203)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(88, 18)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Produttore"
        '
        'L_Modello
        '
        Me.L_Modello.AutoSize = True
        Me.L_Modello.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.L_Modello.Location = New System.Drawing.Point(204, 230)
        Me.L_Modello.Name = "L_Modello"
        Me.L_Modello.Size = New System.Drawing.Size(57, 18)
        Me.L_Modello.TabIndex = 14
        Me.L_Modello.Text = "Label4"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label7.Location = New System.Drawing.Point(25, 228)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(68, 18)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "Modello"
        '
        'FBraSile
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(479, 336)
        Me.Controls.Add(Me.L_Modello)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.L_Produttore)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.L_Or)
        Me.Controls.Add(Me.L_Ori)
        Me.Controls.Add(Me.L_Linea)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.T_Operatore)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.C_Firmware)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.LNom)
        Me.Controls.Add(Me.C_Files)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FBraSile"
        Me.ShowInTaskbar = False
        Me.Text = "Impostazione Collaudo Per Brasile"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents C_Files As System.Windows.Forms.ComboBox
    Friend WithEvents LNom As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents C_Firmware As System.Windows.Forms.ComboBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents T_Operatore As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents L_Linea As System.Windows.Forms.Label
    Friend WithEvents L_Or As System.Windows.Forms.Label
    Friend WithEvents L_Ori As System.Windows.Forms.Label
    Friend WithEvents L_Produttore As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents L_Modello As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
End Class
