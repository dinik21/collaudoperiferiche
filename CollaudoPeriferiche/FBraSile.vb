﻿Public Class FBraSile
    Public StrPath As String
    Public StrNome As String
    Public Sequenze() As String
    Public SequenzeFW() As String
    Public ListaSequ As List(Of String())
    Public nomeSeq As String
    Public OpzioniSeq() As String
    Public RigaFw As String
    Public FileSelezionato As String
    Public v(1) As Integer
    Public StrRadCfg As String
    Public VettoreStr() As String
    Public CFLinea As String
    Public CFOrdine As String
    Public CFProduttore As String
    Public CFModello As String
    Public CFOperatore As String

    Private Sub FBraSile_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        T_Operatore.Text = ""
        L_Modello.Text = ""
        L_Linea.Text = ""
        L_Or.Text = ""
        L_Produttore.Text = ""

        ListaSequ = New List(Of String())
        Sequenze = System.IO.Directory.GetFiles(StrPath, "Conf*.xml")
        LNom.Text = StrNome
        'cerco i file con le sequenze, prendo solo il nome e lo visualizzo nella combo
        For Each s As String In Sequenze
            Dim ii As Int16 = s.LastIndexOf("\")
            nomeSeq = s.Substring(ii + 1)
            ii = nomeSeq.LastIndexOf(".xml")
            nomeSeq = nomeSeq.Substring(0, ii)
            C_Files.Items.Add(nomeSeq)
        Next
        ' se trovo i files, visualizzo il primo
        If (C_Files.Items.Count() > 0) Then
            C_Files.SelectedIndex = 0
        Else
            MessageBox.Show("Files Conf*.xml non trovati", "Collaudo non possibile", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
        If (System.IO.File.Exists(StrPath + "_FW\Firmware.txt") = False) Then
            MessageBox.Show("Errore: Manca il file Firmware.txt sotto la cartella LS40_FW", "Collaudo non possibile", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Close()
        Else
            Dim filefirm As System.IO.TextReader
            filefirm = New System.IO.StreamReader(StrPath + "_FW\Firmware.txt")
            ' cerco il file con i firmware lo scorro aggiungo le righe alla combobox
            Do
                RigaFw = filefirm.ReadLine()
                If RigaFw Is Nothing Then
                Else
                    SequenzeFW = RigaFw.Split(";")
                    C_Firmware.Items.Add(SequenzeFW(0))
                    ListaSequ.Add(SequenzeFW)
                End If


            Loop Until RigaFw Is Nothing
            C_Firmware.SelectedIndex = 0
            filefirm.Close()

            ' carico il file report, contiene le informazioni 

            Dim fileConf As System.IO.TextReader
            If (System.IO.File.Exists(StrPath + "\Report.txt") = False) Then
                MessageBox.Show("Errore: Manca il file Report.txt sotto la cartella LS40", "Collaudo non possibile", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Me.Close()
            Else
                fileConf = New System.IO.StreamReader(StrPath + "\Report.txt")
                StrRadCfg = fileConf.ReadLine()
                If (Not (StrRadCfg Is Nothing)) Then
                    SetStr(CFLinea)
                End If
                StrRadCfg = fileConf.ReadLine()
                If (Not (StrRadCfg Is Nothing)) Then
                    SetStr(CFOrdine)
                End If
                StrRadCfg = fileConf.ReadLine()
                If (Not (StrRadCfg Is Nothing)) Then
                    SetStr(CFProduttore)
                End If
                StrRadCfg = fileConf.ReadLine()
                If (Not (StrRadCfg Is Nothing)) Then
                    SetStr(CFModello)
                End If
                StrRadCfg = fileConf.ReadLine()
                If (Not (StrRadCfg Is Nothing)) Then
                    SetStr(CFOperatore)
                End If

                StrRadCfg = fileConf.ReadLine()
                If (Not (StrRadCfg Is Nothing)) Then
                    VettoreStr = StrRadCfg.Split(":")
                    If VettoreStr.Count >= 2 Then
                        C_Files.SelectedIndex = System.Convert.ToInt16(VettoreStr(1)) - 1
                    End If
                End If

                StrRadCfg = fileConf.ReadLine()
                If (Not (StrRadCfg Is Nothing)) Then
                    VettoreStr = StrRadCfg.Split(":")
                    If VettoreStr.Count >= 2 Then
                        C_Firmware.SelectedIndex = System.Convert.ToInt16(VettoreStr(1)) - 1
                    End If
                End If
                fileConf.Close()

                T_Operatore.Text = CFOperatore
                L_Modello.Text = CFModello
                L_Linea.Text = CFLinea
                L_Or.Text = CFOrdine
                L_Produttore.Text = CFProduttore
            End If
        End If




    End Sub
    Public Sub SetStr(ByRef str As String)
        VettoreStr = StrRadCfg.Split(":")
        If VettoreStr.Count() >= 2 Then
            str = VettoreStr(1)
        End If
    End Sub


    Private Sub C_Firmware_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles C_Firmware.SelectedIndexChanged

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim indiceopz As Integer

        indiceopz = 0
        FileSelezionato = C_Files.SelectedItem

        ' creo il vettore di interi con le opzioni di collaudo
        OpzioniSeq = FileSelezionato.Split("_")
        Select Case StrNome
            Case "Ls40"
            Case "Ls40UVCOLORE"
                For Each ss As String In OpzioniSeq

                    Select Case ss
                        Case "Feed"
                            If indiceopz >= 2 Then
                                Array.Resize(v, v.Count + 1)
                            End If
                            v(indiceopz) = 2
                            indiceopz += 1
                            v(indiceopz) = 4
                            indiceopz += 1
                        Case "Micr"
                            If indiceopz >= 2 Then
                                Array.Resize(v, v.Count + 1)
                            End If
                            v(indiceopz) = 8
                            indiceopz += 1
                        Case "Scan"
                            If indiceopz >= 2 Then
                                Array.Resize(v, v.Count + 1)
                            End If
                            v(indiceopz) = 12
                            indiceopz += 1
                        Case "Timbro"
                            If indiceopz >= 2 Then
                                Array.Resize(v, v.Count + 1)
                            End If
                            v(indiceopz) = 14
                            indiceopz += 1
                        Case "Ink"
                            If indiceopz >= 2 Then
                                Array.Resize(v, v.Count + 1)
                            End If
                            v(indiceopz) = 18
                            indiceopz += 1
                        Case "Badge"
                            If indiceopz >= 2 Then
                                Array.Resize(v, v.Count + 1)
                            End If
                            v(indiceopz) = 48
                            indiceopz += 1
                        Case "Id"
                            If indiceopz >= 2 Then
                                Array.Resize(v, v.Count + 1)
                            End If
                            v(indiceopz) = 4
                            indiceopz += 1
                        Case "Iqa"
                            If indiceopz >= 2 Then
                                Array.Resize(v, v.Count + 1)
                            End If
                            v(indiceopz) = 76 'IQA ABILITATO
                            indiceopz += 1
                    End Select

                Next
                Array.Resize(v, v.Count + 1)
                v(indiceopz) = 48
            Case "Ls150"
                For Each ss As String In OpzioniSeq
                    Select Case ss
                        Case "Feed50"
                            If indiceopz >= 2 Then
                                Array.Resize(v, v.Count + 1)
                            End If
                            v(indiceopz) = 65
                            indiceopz += 1
                            v(indiceopz) = 4
                            indiceopz += 1
                        Case "Feed"
                            If indiceopz >= 2 Then
                                Array.Resize(v, v.Count + 1)
                            End If
                            v(indiceopz) = 2
                            indiceopz += 1
                            v(indiceopz) = 4
                            indiceopz += 1
                        Case "Micr"
                            If indiceopz >= 2 Then
                                Array.Resize(v, v.Count + 1)
                            End If
                            v(indiceopz) = 8
                            indiceopz += 1
                        Case "Scan"
                            If indiceopz >= 2 Then
                                Array.Resize(v, v.Count + 1)
                            End If
                            v(indiceopz) = 12
                            indiceopz += 1
                        Case "Ink"
                            If indiceopz >= 2 Then
                                Array.Resize(v, v.Count + 1)
                            End If
                            v(indiceopz) = 18
                            indiceopz += 1                        
                    End Select

                Next
                Array.Resize(v, v.Count + 1)
                v(indiceopz) = 47
            Case "Ls100"
                For Each ss As String In OpzioniSeq

                    Select Case ss
                        Case "Feed"
                            If indiceopz >= 2 Then
                                Array.Resize(v, v.Count + 1)
                            End If
                            v(indiceopz) = 2
                            indiceopz += 1
                            v(indiceopz) = 4
                            indiceopz += 1
                        Case "Micr"
                            If indiceopz >= 2 Then
                                Array.Resize(v, v.Count + 1)
                            End If
                            v(indiceopz) = 8
                            indiceopz += 1
                        Case "Scan"
                            If indiceopz >= 2 Then
                                Array.Resize(v, v.Count + 1)
                            End If
                            v(indiceopz) = 12
                            indiceopz += 1
                        Case "Timbro"
                            If indiceopz >= 2 Then
                                Array.Resize(v, v.Count + 1)
                            End If
                            v(indiceopz) = 14
                            indiceopz += 1
                        Case "Ink"
                            If indiceopz >= 2 Then
                                Array.Resize(v, v.Count + 1)
                            End If
                            v(indiceopz) = 18
                            indiceopz += 1
                        Case "Badge"
                            If indiceopz >= 2 Then
                                Array.Resize(v, v.Count + 1)
                            End If
                            v(indiceopz) = 48
                            indiceopz += 1
                    End Select

                Next
                Array.Resize(v, v.Count + 1)
                v(indiceopz) = 30
        End Select
        SequenzeFW = ListaSequ(C_Firmware.SelectedIndex)

        ' scrivo il file di configurazione Firmware.txt
        CFOperatore = T_Operatore.Text
        CFModello = L_Modello.Text
        CFLinea = L_Linea.Text
        CFOrdine = L_Or.Text
        CFProduttore = L_Produttore.Text

        Dim fileConf As System.IO.TextWriter
        fileConf = New System.IO.StreamWriter(StrPath + "\Report.txt")
        fileConf.WriteLine("Linea :" + CFLinea)
        fileConf.WriteLine("Ordine :" + CFOrdine)
        fileConf.WriteLine("Produttore :" + CFProduttore)
        fileConf.WriteLine("Modello :" + CFModello)
        fileConf.WriteLine("Operatore :" + CFOperatore)
        fileConf.WriteLine("Conf :" + (C_Files.SelectedIndex + 1).ToString())
        fileConf.WriteLine("Firm :" + (C_Firmware.SelectedIndex + 1).ToString())

        fileConf.Close()
        ' chiudo la dialog
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub L_Ori_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles L_Ori.Click

    End Sub
End Class