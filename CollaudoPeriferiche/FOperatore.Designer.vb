﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FOperatore
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim OperatoreLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FOperatore))
        Me.Label1 = New System.Windows.Forms.Label
        Me.BOpOK = New System.Windows.Forms.Button
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.UscitaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.TPassword = New System.Windows.Forms.TextBox
        Me.Collaudi02DataSet = New CollaudoPeriferiche.Collaudi02DataSet
        Me.OperatoriBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.OperatoriTableAdapter = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.OperatoriTableAdapter
        Me.TableAdapterManager = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.TableAdapterManager
        Me.OperatoreComboBox = New System.Windows.Forms.ComboBox
        Me.AutenticazioneBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AutenticazioneTableAdapter = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.AutenticazioneTableAdapter
        OperatoreLabel = New System.Windows.Forms.Label
        Me.MenuStrip1.SuspendLayout()
        CType(Me.Collaudi02DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OperatoriBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AutenticazioneBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'OperatoreLabel
        '
        OperatoreLabel.AutoSize = True
        OperatoreLabel.ForeColor = System.Drawing.SystemColors.ActiveCaption
        OperatoreLabel.Location = New System.Drawing.Point(28, 30)
        OperatoreLabel.Name = "OperatoreLabel"
        OperatoreLabel.Size = New System.Drawing.Size(57, 13)
        OperatoreLabel.TabIndex = 6
        OperatoreLabel.Text = "Operatore:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label1.Location = New System.Drawing.Point(31, 69)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Password"
        '
        'BOpOK
        '
        Me.BOpOK.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.BOpOK.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.BOpOK.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BOpOK.Location = New System.Drawing.Point(74, 95)
        Me.BOpOK.Name = "BOpOK"
        Me.BOpOK.Size = New System.Drawing.Size(92, 58)
        Me.BOpOK.TabIndex = 5
        Me.BOpOK.Text = "OK"
        Me.BOpOK.UseVisualStyleBackColor = False
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UscitaToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(234, 24)
        Me.MenuStrip1.TabIndex = 6
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'UscitaToolStripMenuItem
        '
        Me.UscitaToolStripMenuItem.Name = "UscitaToolStripMenuItem"
        Me.UscitaToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F12
        Me.UscitaToolStripMenuItem.Size = New System.Drawing.Size(51, 20)
        Me.UscitaToolStripMenuItem.Text = "Uscita"
        '
        'TPassword
        '
        Me.TPassword.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.CollaudoPeriferiche.My.MySettings.Default, "PassWordUtente", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.TPassword.Location = New System.Drawing.Point(91, 69)
        Me.TPassword.Name = "TPassword"
        Me.TPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TPassword.Size = New System.Drawing.Size(121, 20)
        Me.TPassword.TabIndex = 3
        Me.TPassword.Text = Global.CollaudoPeriferiche.My.MySettings.Default.PassWordUtente
        '
        'Collaudi02DataSet
        '
        Me.Collaudi02DataSet.DataSetName = "Collaudi02DataSet"
        Me.Collaudi02DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'OperatoriBindingSource
        '
        Me.OperatoriBindingSource.DataMember = "Operatori"
        Me.OperatoriBindingSource.DataSource = Me.Collaudi02DataSet
        '
        'OperatoriTableAdapter
        '
        Me.OperatoriTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AutenticazioneTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.CategorieOpzioniTableAdapter = Nothing
        Me.TableAdapterManager.DettaglioOrdineTableAdapter = Nothing
        Me.TableAdapterManager.OperatoriTableAdapter = Me.OperatoriTableAdapter
        Me.TableAdapterManager.SequenzaCollaudoTableAdapter = Nothing
        Me.TableAdapterManager.SequenzaTableAdapter = Nothing
        Me.TableAdapterManager.SequOperaTableAdapter = Nothing
        Me.TableAdapterManager.T_Coll_FaseTableAdapter = Nothing
        Me.TableAdapterManager.T_CollaudoTableAdapter = Nothing
        Me.TableAdapterManager.T_DatiFineCollaudoTableAdapter = Nothing
        Me.TableAdapterManager.T_FasicollaudoTableAdapter = Nothing
        Me.TableAdapterManager.T_FirmwareTableAdapter = Nothing
        Me.TableAdapterManager.T_ImmaginiTableAdapter = Nothing
        Me.TableAdapterManager.T_Mess_PerifTableAdapter = Nothing
        Me.TableAdapterManager.T_MessaggiTableAdapter = Nothing
        Me.TableAdapterManager.T_OrdineTableAdapter = Nothing
        Me.TableAdapterManager.T_PerifericaTableAdapter = Nothing
        Me.TableAdapterManager.T_Staz_CollTableAdapter = Nothing
        Me.TableAdapterManager.T_StoricoTableAdapter = Nothing
        Me.TableAdapterManager.T_TipoDatoTableAdapter = Nothing
        Me.TableAdapterManager.T_VersioniTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = CollaudoPeriferiche.Collaudi02DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.ValoriOpzioniTableAdapter = Nothing
        '
        'OperatoreComboBox
        '
        Me.OperatoreComboBox.DataSource = Me.OperatoriBindingSource
        Me.OperatoreComboBox.DisplayMember = "Operatore"
        Me.OperatoreComboBox.FormattingEnabled = True
        Me.OperatoreComboBox.Location = New System.Drawing.Point(91, 27)
        Me.OperatoreComboBox.Name = "OperatoreComboBox"
        Me.OperatoreComboBox.Size = New System.Drawing.Size(121, 21)
        Me.OperatoreComboBox.TabIndex = 7
        '
        'AutenticazioneBindingSource
        '
        Me.AutenticazioneBindingSource.DataMember = "Autenticazione"
        Me.AutenticazioneBindingSource.DataSource = Me.Collaudi02DataSet
        '
        'AutenticazioneTableAdapter
        '
        Me.AutenticazioneTableAdapter.ClearBeforeFill = True
        '
        'FOperatore
        '
        Me.AcceptButton = Me.BOpOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(234, 171)
        Me.ControlBox = False
        Me.Controls.Add(OperatoreLabel)
        Me.Controls.Add(Me.OperatoreComboBox)
        Me.Controls.Add(Me.BOpOK)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TPassword)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FOperatore"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Selezione Operatore"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.Collaudi02DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OperatoriBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AutenticazioneBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

   
    Friend WithEvents TPassword As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents BOpOK As System.Windows.Forms.Button
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents UscitaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Collaudi02DataSet As CollaudoPeriferiche.Collaudi02DataSet
    Friend WithEvents OperatoriBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents OperatoriTableAdapter As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.OperatoriTableAdapter
    Friend WithEvents TableAdapterManager As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.TableAdapterManager
    Friend WithEvents OperatoreComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents AutenticazioneBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AutenticazioneTableAdapter As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.AutenticazioneTableAdapter
End Class
