﻿Public Class FOperatore
    Public OperatoreSelezionato As DataRowView
    Public Function Autentica(ByVal par As Integer) As Integer
        Dim ret As Integer
        Dim riga As Collaudi02DataSet.AutenticazioneRow
        riga = Me.Collaudi02DataSet.Autenticazione.Rows(0)
        If par = 0 Then
            If riga.Autenticazione = 0 Then
                riga.Autenticazione = 1
                Me.AutenticazioneTableAdapter.Update(riga)
                ret = 1
            Else
                ret = 0
            End If
        Else
            If riga.Autenticazione = 1 Then
                riga.Autenticazione = 0
                Me.AutenticazioneTableAdapter.Update(riga)
            End If
        End If
        ' Ale modifica del 23-10-2013
        ' cambiato il return ! torna sempre 1 così il collaudo va avanti !
        Return 1
        'Return ret
    End Function

    Private Sub FOperatore_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: questa riga di codice carica i dati nella tabella 'Collaudi02DataSet.Autenticazione'. È possibile spostarla o rimuoverla se necessario.

        'TODO: questa riga di codice carica i dati nella tabella 'Collaudi02DataSet.Operatori'. È possibile spostarla o rimuoverla se necessario.
        'Dim rr As Integer
        Dim s As SByte
        s = 1
        OperatoreSelezionato = Nothing

        If Form1.fServerPresente = True Then
            Try
                Me.AutenticazioneTableAdapter.FillByID(Me.Collaudi02DataSet.Autenticazione)
                ' se non c'è nessuno che si sta autenticando abilito la videata di autenticazione
                If Autentica(0) = 1 Then

                    ' se mi ero già loggago come un altro utenteo tolgo il LOG
                    If OperatoreSelezionato Is Nothing Then
                    Else
                        ' Tolgo il flag USED all operatore 
                        OperatoreSelezionato("Used") = 0
                        OperatoriTableAdapter.Update(OperatoreSelezionato("Operatore").ToString, OperatoreSelezionato("Password").ToString, 0, OperatoreSelezionato("Id_Operatore"), OperatoreSelezionato("Operatore").ToString, OperatoreSelezionato("Password").ToString, 1)
                    End If


                    'TODO: questa riga di codice carica i dati nella tabella 'CollaudiDataSet.Operatori'. È possibile spostarla o rimuoverla se necessario.
                    Me.OperatoriTableAdapter.FillByNotUsed(Me.Collaudi02DataSet.Operatori)

                    Collaudi02DataSet.Operatori.WriteXml(Application.StartupPath + Form1.LOCALDB + Form1.T_OPERATORI)

                    TPassword.Text = ""
                Else
                    MessageBox.Show("Non è Possibile Autenticarsi, attendere quanche secondo e riprovare", "Attezione", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Me.Close()
                End If
            Catch ex As Exception
                'System.Windows.Forms.MessageBox.Show(ex.Message)
                'MessageBox.Show("Attenzione Connessione di Rete non Presente !!! RIAVVIARE IL PROGRAMMA", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error)
                MessageBox.Show("Mancata Comunicazione con il Database o con alcune sue tabelle", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Me.Close()
            End Try
            
        Else
            ' server NON presente
            Dim r As Short
            Collaudi02DataSet.Operatori.Clear()

            r = Collaudi02DataSet.Operatori.ReadXml(Application.StartupPath + Form1.LOCALDB + Form1.T_OPERATORI)
        End If

    End Sub

    Private Sub BOpOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BOpOK.Click
        Dim r As Int16
        OperatoreSelezionato = Me.OperatoriBindingSource.Current
        If Form1.fServerPresente = True Then
            If OperatoreSelezionato("Password") = TPassword.Text Then
                ' ho messo la psw corretta , alzo il flag di operatore utilizzato
                Me.OperatoriBindingSource.Current("Used") = True
                Me.OperatoriBindingSource.EndEdit()
                r = Me.OperatoriTableAdapter.Update(Collaudi02DataSet.Operatori)
                Me.OperatoriTableAdapter.Update(Me.OperatoriBindingSource.Current("Operatore").ToString, Me.OperatoriBindingSource.Current("Password").ToString, 1, Me.OperatoriBindingSource.Current("Id_Operatore"), Me.OperatoriBindingSource.Current("Operatore").ToString, Me.OperatoriBindingSource.Current("Password").ToString, 0)

                ' tolgo il flag di autenticazione e lascio che altri si possano autenticare
                Autentica(1)

                Me.Close()
            Else
                TPassword.Focus()
                MessageBox.Show("Password ERRATA")
            End If
        Else
            If OperatoreSelezionato("Password") = TPassword.Text Then
                Me.Close()
            Else
                TPassword.Focus()
                MessageBox.Show("Password ERRATA")
            End If
        End If
        
    End Sub

   
    Private Sub UscitaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UscitaToolStripMenuItem.Click
        ' tolgo il flag di autenticazione e lascio che altri si possano autenticare
        If Form1.fServerPresente Then
            Autentica(1)
        End If

        Me.Close()
    End Sub

    Private Sub OperatoreComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub OperatoriBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.OperatoriBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Collaudi02DataSet)

    End Sub


End Class