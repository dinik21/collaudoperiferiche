﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FOrdini
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Id_OrdineLabel As System.Windows.Forms.Label
        Dim FKPerifericaLabel As System.Windows.Forms.Label
        Dim DescrizioneClienteLabel As System.Windows.Forms.Label
        Dim ArticoloLabel As System.Windows.Forms.Label
        Dim DataInsLabel As System.Windows.Forms.Label
        Dim N_OrdineLabel1 As System.Windows.Forms.Label
        Dim Firmware1Label As System.Windows.Forms.Label
        Dim Firmware2Label As System.Windows.Forms.Label
        Dim Firmware3Label As System.Windows.Forms.Label
        Dim Firmware4Label As System.Windows.Forms.Label
        Dim Firmware5Label As System.Windows.Forms.Label
        Dim Firmware6Label As System.Windows.Forms.Label
        Dim Firmware7Label As System.Windows.Forms.Label
        Dim Firmware8Label As System.Windows.Forms.Label
        Dim Firmware9Label As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FOrdini))
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.DOpzioni = New System.Windows.Forms.DataGridView
        Me.Catogoria = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Valore = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Button1 = New System.Windows.Forms.Button
        Me.T_OrdineBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.T_OrdineBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Collaudi02DataSet = New CollaudoPeriferiche.Collaudi02DataSet
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.FKPerifericaTextBox = New System.Windows.Forms.TextBox
        Me.DataInsDateTimePicker = New System.Windows.Forms.DateTimePicker
        Me.Id_OrdineLabel2 = New System.Windows.Forms.Label
        Me.N_OrdineLabel2 = New System.Windows.Forms.Label
        Me.ArticoloLabel2 = New System.Windows.Forms.Label
        Me.Firmware1Label1 = New System.Windows.Forms.Label
        Me.Firmware2Label1 = New System.Windows.Forms.Label
        Me.Firmware3Label1 = New System.Windows.Forms.Label
        Me.Firmware4Label1 = New System.Windows.Forms.Label
        Me.Firmware5Label1 = New System.Windows.Forms.Label
        Me.Firmware6Label1 = New System.Windows.Forms.Label
        Me.Firmware7Label1 = New System.Windows.Forms.Label
        Me.Firmware8Label1 = New System.Windows.Forms.Label
        Me.Firmware9Label1 = New System.Windows.Forms.Label
        Me.TNomeperiferica = New System.Windows.Forms.Label
        Me.TDescrizioneperiferica = New System.Windows.Forms.Label
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.labelDescrizioneCliente = New System.Windows.Forms.Label
        Me.MaskedTextBox1 = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Button2 = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.RLS40LC = New System.Windows.Forms.RadioButton
        Me.RNewCod = New System.Windows.Forms.RadioButton
        Me.RLS40UV = New System.Windows.Forms.RadioButton
        Me.RadioButton1 = New System.Windows.Forms.RadioButton
        Me.RALL = New System.Windows.Forms.RadioButton
        Me.RLS40 = New System.Windows.Forms.RadioButton
        Me.RLS515 = New System.Windows.Forms.RadioButton
        Me.RLS150COL = New System.Windows.Forms.RadioButton
        Me.RLS150UV = New System.Windows.Forms.RadioButton
        Me.RLS150 = New System.Windows.Forms.RadioButton
        Me.RLS100 = New System.Windows.Forms.RadioButton
        Me.DettaglioOrdineBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.T_OrdineTableAdapter = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_OrdineTableAdapter
        Me.TableAdapterManager = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.TableAdapterManager
        Me.CategorieOpzioniTableAdapter = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.CategorieOpzioniTableAdapter
        Me.DettaglioOrdineTableAdapter = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.DettaglioOrdineTableAdapter
        Me.T_PerifericaTableAdapter = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_PerifericaTableAdapter
        Me.ValoriOpzioniTableAdapter = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.ValoriOpzioniTableAdapter
        Me.CategorieOpzioniBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ValoriOpzioniBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.T_PerifericaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.btEsci = New System.Windows.Forms.Button
        Id_OrdineLabel = New System.Windows.Forms.Label
        FKPerifericaLabel = New System.Windows.Forms.Label
        DescrizioneClienteLabel = New System.Windows.Forms.Label
        ArticoloLabel = New System.Windows.Forms.Label
        DataInsLabel = New System.Windows.Forms.Label
        N_OrdineLabel1 = New System.Windows.Forms.Label
        Firmware1Label = New System.Windows.Forms.Label
        Firmware2Label = New System.Windows.Forms.Label
        Firmware3Label = New System.Windows.Forms.Label
        Firmware4Label = New System.Windows.Forms.Label
        Firmware5Label = New System.Windows.Forms.Label
        Firmware6Label = New System.Windows.Forms.Label
        Firmware7Label = New System.Windows.Forms.Label
        Firmware8Label = New System.Windows.Forms.Label
        Firmware9Label = New System.Windows.Forms.Label
        Me.GroupBox3.SuspendLayout()
        CType(Me.DOpzioni, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.T_OrdineBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.T_OrdineBindingNavigator.SuspendLayout()
        CType(Me.T_OrdineBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Collaudi02DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DettaglioOrdineBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CategorieOpzioniBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ValoriOpzioniBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.T_PerifericaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Id_OrdineLabel
        '
        Id_OrdineLabel.AutoSize = True
        Id_OrdineLabel.ForeColor = System.Drawing.Color.Blue
        Id_OrdineLabel.Location = New System.Drawing.Point(9, 45)
        Id_OrdineLabel.Name = "Id_OrdineLabel"
        Id_OrdineLabel.Size = New System.Drawing.Size(53, 13)
        Id_OrdineLabel.TabIndex = 43
        Id_OrdineLabel.Text = "Id Ordine:"
        '
        'FKPerifericaLabel
        '
        FKPerifericaLabel.AutoSize = True
        FKPerifericaLabel.ForeColor = System.Drawing.Color.Blue
        FKPerifericaLabel.Location = New System.Drawing.Point(9, 71)
        FKPerifericaLabel.Name = "FKPerifericaLabel"
        FKPerifericaLabel.Size = New System.Drawing.Size(67, 13)
        FKPerifericaLabel.TabIndex = 47
        FKPerifericaLabel.Text = "FKPeriferica:"
        '
        'DescrizioneClienteLabel
        '
        DescrizioneClienteLabel.AutoSize = True
        DescrizioneClienteLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DescrizioneClienteLabel.ForeColor = System.Drawing.Color.Red
        DescrizioneClienteLabel.Location = New System.Drawing.Point(21, 123)
        DescrizioneClienteLabel.Name = "DescrizioneClienteLabel"
        DescrizioneClienteLabel.Size = New System.Drawing.Size(248, 29)
        DescrizioneClienteLabel.TabIndex = 53
        DescrizioneClienteLabel.Text = "Descrizione Cliente:"
        '
        'ArticoloLabel
        '
        ArticoloLabel.AutoSize = True
        ArticoloLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ArticoloLabel.ForeColor = System.Drawing.Color.Red
        ArticoloLabel.Location = New System.Drawing.Point(21, 156)
        ArticoloLabel.Name = "ArticoloLabel"
        ArticoloLabel.Size = New System.Drawing.Size(109, 29)
        ArticoloLabel.TabIndex = 55
        ArticoloLabel.Text = "Articolo:"
        '
        'DataInsLabel
        '
        DataInsLabel.AutoSize = True
        DataInsLabel.Enabled = False
        DataInsLabel.ForeColor = System.Drawing.Color.Blue
        DataInsLabel.Location = New System.Drawing.Point(430, 312)
        DataInsLabel.Name = "DataInsLabel"
        DataInsLabel.Size = New System.Drawing.Size(50, 13)
        DataInsLabel.TabIndex = 75
        DataInsLabel.Text = "Data Ins:"
        DataInsLabel.Visible = False
        '
        'N_OrdineLabel1
        '
        N_OrdineLabel1.AutoSize = True
        N_OrdineLabel1.ForeColor = System.Drawing.Color.Blue
        N_OrdineLabel1.Location = New System.Drawing.Point(311, 45)
        N_OrdineLabel1.Name = "N_OrdineLabel1"
        N_OrdineLabel1.Size = New System.Drawing.Size(52, 13)
        N_OrdineLabel1.TabIndex = 79
        N_OrdineLabel1.Text = "N Ordine:"
        N_OrdineLabel1.Visible = False
        '
        'Firmware1Label
        '
        Firmware1Label.AutoSize = True
        Firmware1Label.ForeColor = System.Drawing.Color.Blue
        Firmware1Label.Location = New System.Drawing.Point(6, 236)
        Firmware1Label.Name = "Firmware1Label"
        Firmware1Label.Size = New System.Drawing.Size(58, 13)
        Firmware1Label.TabIndex = 84
        Firmware1Label.Text = "Firmware1:"
        '
        'Firmware2Label
        '
        Firmware2Label.AutoSize = True
        Firmware2Label.ForeColor = System.Drawing.Color.Blue
        Firmware2Label.Location = New System.Drawing.Point(244, 236)
        Firmware2Label.Name = "Firmware2Label"
        Firmware2Label.Size = New System.Drawing.Size(58, 13)
        Firmware2Label.TabIndex = 85
        Firmware2Label.Text = "Firmware2:"
        '
        'Firmware3Label
        '
        Firmware3Label.AutoSize = True
        Firmware3Label.ForeColor = System.Drawing.Color.Blue
        Firmware3Label.Location = New System.Drawing.Point(480, 236)
        Firmware3Label.Name = "Firmware3Label"
        Firmware3Label.Size = New System.Drawing.Size(58, 13)
        Firmware3Label.TabIndex = 86
        Firmware3Label.Text = "Firmware3:"
        '
        'Firmware4Label
        '
        Firmware4Label.AutoSize = True
        Firmware4Label.ForeColor = System.Drawing.Color.Blue
        Firmware4Label.Location = New System.Drawing.Point(6, 267)
        Firmware4Label.Name = "Firmware4Label"
        Firmware4Label.Size = New System.Drawing.Size(58, 13)
        Firmware4Label.TabIndex = 87
        Firmware4Label.Text = "Firmware4:"
        '
        'Firmware5Label
        '
        Firmware5Label.AutoSize = True
        Firmware5Label.ForeColor = System.Drawing.Color.Blue
        Firmware5Label.Location = New System.Drawing.Point(244, 267)
        Firmware5Label.Name = "Firmware5Label"
        Firmware5Label.Size = New System.Drawing.Size(58, 13)
        Firmware5Label.TabIndex = 88
        Firmware5Label.Text = "Firmware5:"
        '
        'Firmware6Label
        '
        Firmware6Label.AutoSize = True
        Firmware6Label.ForeColor = System.Drawing.Color.Blue
        Firmware6Label.Location = New System.Drawing.Point(6, 312)
        Firmware6Label.Name = "Firmware6Label"
        Firmware6Label.Size = New System.Drawing.Size(58, 13)
        Firmware6Label.TabIndex = 89
        Firmware6Label.Text = "Firmware6:"
        Firmware6Label.Visible = False
        '
        'Firmware7Label
        '
        Firmware7Label.AutoSize = True
        Firmware7Label.ForeColor = System.Drawing.Color.Blue
        Firmware7Label.Location = New System.Drawing.Point(123, 312)
        Firmware7Label.Name = "Firmware7Label"
        Firmware7Label.Size = New System.Drawing.Size(58, 13)
        Firmware7Label.TabIndex = 90
        Firmware7Label.Text = "Firmware7:"
        Firmware7Label.Visible = False
        '
        'Firmware8Label
        '
        Firmware8Label.AutoSize = True
        Firmware8Label.ForeColor = System.Drawing.Color.Blue
        Firmware8Label.Location = New System.Drawing.Point(247, 310)
        Firmware8Label.Name = "Firmware8Label"
        Firmware8Label.Size = New System.Drawing.Size(58, 13)
        Firmware8Label.TabIndex = 91
        Firmware8Label.Text = "Firmware8:"
        Firmware8Label.Visible = False
        '
        'Firmware9Label
        '
        Firmware9Label.AutoSize = True
        Firmware9Label.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold)
        Firmware9Label.ForeColor = System.Drawing.Color.Red
        Firmware9Label.Location = New System.Drawing.Point(21, 185)
        Firmware9Label.Name = "Firmware9Label"
        Firmware9Label.Size = New System.Drawing.Size(87, 29)
        Firmware9Label.TabIndex = 92
        Firmware9Label.Text = "Suite :"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.DOpzioni)
        Me.GroupBox3.Location = New System.Drawing.Point(123, 330)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(461, 246)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Opzioni Collaudo"
        '
        'DOpzioni
        '
        Me.DOpzioni.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DOpzioni.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Catogoria, Me.Valore})
        Me.DOpzioni.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DOpzioni.Location = New System.Drawing.Point(3, 16)
        Me.DOpzioni.Name = "DOpzioni"
        Me.DOpzioni.ReadOnly = True
        Me.DOpzioni.Size = New System.Drawing.Size(455, 227)
        Me.DOpzioni.TabIndex = 0
        '
        'Catogoria
        '
        Me.Catogoria.HeaderText = "Opzione"
        Me.Catogoria.Name = "Catogoria"
        Me.Catogoria.ReadOnly = True
        Me.Catogoria.Width = 200
        '
        'Valore
        '
        Me.Valore.HeaderText = "Valore"
        Me.Valore.Name = "Valore"
        Me.Valore.ReadOnly = True
        Me.Valore.Width = 200
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(326, 755)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(83, 56)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "OK"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'T_OrdineBindingNavigator
        '
        Me.T_OrdineBindingNavigator.AddNewItem = Nothing
        Me.T_OrdineBindingNavigator.BindingSource = Me.T_OrdineBindingSource
        Me.T_OrdineBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.T_OrdineBindingNavigator.DeleteItem = Nothing
        Me.T_OrdineBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2})
        Me.T_OrdineBindingNavigator.Location = New System.Drawing.Point(3, 3)
        Me.T_OrdineBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.T_OrdineBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.T_OrdineBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.T_OrdineBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.T_OrdineBindingNavigator.Name = "T_OrdineBindingNavigator"
        Me.T_OrdineBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.T_OrdineBindingNavigator.Size = New System.Drawing.Size(718, 25)
        Me.T_OrdineBindingNavigator.TabIndex = 2
        Me.T_OrdineBindingNavigator.Text = "BindingNavigator1"
        '
        'T_OrdineBindingSource
        '
        Me.T_OrdineBindingSource.DataMember = "T_Ordine"
        Me.T_OrdineBindingSource.DataSource = Me.Collaudi02DataSet
        '
        'Collaudi02DataSet
        '
        Me.Collaudi02DataSet.DataSetName = "Collaudi02DataSet"
        Me.Collaudi02DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(34, 22)
        Me.BindingNavigatorCountItem.Text = "di {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Numero totale di elementi"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Sposta in prima posizione"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Sposta indietro"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AcceptsTab = True
        Me.BindingNavigatorPositionItem.AccessibleName = "Posizione"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 21)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Posizione corrente"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Sposta avanti"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Sposta in ultima posizione"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'FKPerifericaTextBox
        '
        Me.FKPerifericaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_OrdineBindingSource, "FKPeriferica", True))
        Me.FKPerifericaTextBox.Enabled = False
        Me.FKPerifericaTextBox.Location = New System.Drawing.Point(117, 68)
        Me.FKPerifericaTextBox.Name = "FKPerifericaTextBox"
        Me.FKPerifericaTextBox.Size = New System.Drawing.Size(52, 20)
        Me.FKPerifericaTextBox.TabIndex = 0
        '
        'DataInsDateTimePicker
        '
        Me.DataInsDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.T_OrdineBindingSource, "DataIns", True))
        Me.DataInsDateTimePicker.Enabled = False
        Me.DataInsDateTimePicker.Location = New System.Drawing.Point(486, 304)
        Me.DataInsDateTimePicker.Name = "DataInsDateTimePicker"
        Me.DataInsDateTimePicker.Size = New System.Drawing.Size(200, 20)
        Me.DataInsDateTimePicker.TabIndex = 76
        Me.DataInsDateTimePicker.Visible = False
        '
        'Id_OrdineLabel2
        '
        Me.Id_OrdineLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_OrdineBindingSource, "Id_Ordine", True))
        Me.Id_OrdineLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Id_OrdineLabel2.Location = New System.Drawing.Point(114, 45)
        Me.Id_OrdineLabel2.Name = "Id_OrdineLabel2"
        Me.Id_OrdineLabel2.Size = New System.Drawing.Size(78, 17)
        Me.Id_OrdineLabel2.TabIndex = 1
        Me.Id_OrdineLabel2.Text = "Id_ordine"
        '
        'N_OrdineLabel2
        '
        Me.N_OrdineLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_OrdineBindingSource, "N_Ordine", True))
        Me.N_OrdineLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.N_OrdineLabel2.Location = New System.Drawing.Point(369, 45)
        Me.N_OrdineLabel2.Name = "N_OrdineLabel2"
        Me.N_OrdineLabel2.Size = New System.Drawing.Size(215, 17)
        Me.N_OrdineLabel2.TabIndex = 80
        Me.N_OrdineLabel2.Text = "N_Ordine"
        Me.N_OrdineLabel2.Visible = False
        '
        'ArticoloLabel2
        '
        Me.ArticoloLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_OrdineBindingSource, "Articolo", True))
        Me.ArticoloLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.ArticoloLabel2.Location = New System.Drawing.Point(300, 166)
        Me.ArticoloLabel2.Name = "ArticoloLabel2"
        Me.ArticoloLabel2.Size = New System.Drawing.Size(260, 29)
        Me.ArticoloLabel2.TabIndex = 84
        Me.ArticoloLabel2.Text = "Articolo"
        '
        'Firmware1Label1
        '
        Me.Firmware1Label1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_OrdineBindingSource, "Firmware1", True))
        Me.Firmware1Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Firmware1Label1.Location = New System.Drawing.Point(70, 236)
        Me.Firmware1Label1.Name = "Firmware1Label1"
        Me.Firmware1Label1.Size = New System.Drawing.Size(151, 17)
        Me.Firmware1Label1.TabIndex = 85
        Me.Firmware1Label1.Text = "FW1"
        '
        'Firmware2Label1
        '
        Me.Firmware2Label1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_OrdineBindingSource, "Firmware2", True))
        Me.Firmware2Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Firmware2Label1.Location = New System.Drawing.Point(308, 236)
        Me.Firmware2Label1.Name = "Firmware2Label1"
        Me.Firmware2Label1.Size = New System.Drawing.Size(151, 17)
        Me.Firmware2Label1.TabIndex = 86
        Me.Firmware2Label1.Text = "FW2"
        '
        'Firmware3Label1
        '
        Me.Firmware3Label1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_OrdineBindingSource, "Firmware3", True))
        Me.Firmware3Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Firmware3Label1.Location = New System.Drawing.Point(544, 236)
        Me.Firmware3Label1.Name = "Firmware3Label1"
        Me.Firmware3Label1.Size = New System.Drawing.Size(151, 17)
        Me.Firmware3Label1.TabIndex = 87
        Me.Firmware3Label1.Text = "FW3"
        '
        'Firmware4Label1
        '
        Me.Firmware4Label1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_OrdineBindingSource, "Firmware4", True))
        Me.Firmware4Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Firmware4Label1.Location = New System.Drawing.Point(70, 267)
        Me.Firmware4Label1.Name = "Firmware4Label1"
        Me.Firmware4Label1.Size = New System.Drawing.Size(151, 17)
        Me.Firmware4Label1.TabIndex = 88
        Me.Firmware4Label1.Text = "FW4"
        '
        'Firmware5Label1
        '
        Me.Firmware5Label1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_OrdineBindingSource, "Firmware5", True))
        Me.Firmware5Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Firmware5Label1.Location = New System.Drawing.Point(308, 267)
        Me.Firmware5Label1.Name = "Firmware5Label1"
        Me.Firmware5Label1.Size = New System.Drawing.Size(151, 17)
        Me.Firmware5Label1.TabIndex = 89
        Me.Firmware5Label1.Text = "FW5"
        '
        'Firmware6Label1
        '
        Me.Firmware6Label1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_OrdineBindingSource, "Firmware6", True))
        Me.Firmware6Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Firmware6Label1.Location = New System.Drawing.Point(63, 310)
        Me.Firmware6Label1.Name = "Firmware6Label1"
        Me.Firmware6Label1.Size = New System.Drawing.Size(54, 17)
        Me.Firmware6Label1.TabIndex = 90
        Me.Firmware6Label1.Text = "FW6"
        Me.Firmware6Label1.Visible = False
        '
        'Firmware7Label1
        '
        Me.Firmware7Label1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_OrdineBindingSource, "Firmware7", True))
        Me.Firmware7Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Firmware7Label1.Location = New System.Drawing.Point(183, 306)
        Me.Firmware7Label1.Name = "Firmware7Label1"
        Me.Firmware7Label1.Size = New System.Drawing.Size(58, 17)
        Me.Firmware7Label1.TabIndex = 91
        Me.Firmware7Label1.Text = "FW7"
        Me.Firmware7Label1.Visible = False
        '
        'Firmware8Label1
        '
        Me.Firmware8Label1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_OrdineBindingSource, "Firmware8", True))
        Me.Firmware8Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Firmware8Label1.Location = New System.Drawing.Point(311, 310)
        Me.Firmware8Label1.Name = "Firmware8Label1"
        Me.Firmware8Label1.Size = New System.Drawing.Size(151, 17)
        Me.Firmware8Label1.TabIndex = 92
        Me.Firmware8Label1.Text = "FW8"
        Me.Firmware8Label1.Visible = False
        '
        'Firmware9Label1
        '
        Me.Firmware9Label1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_OrdineBindingSource, "Firmware9", True))
        Me.Firmware9Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Firmware9Label1.Location = New System.Drawing.Point(300, 195)
        Me.Firmware9Label1.Name = "Firmware9Label1"
        Me.Firmware9Label1.Size = New System.Drawing.Size(151, 17)
        Me.Firmware9Label1.TabIndex = 93
        Me.Firmware9Label1.Text = "FW9"
        '
        'TNomeperiferica
        '
        Me.TNomeperiferica.AutoSize = True
        Me.TNomeperiferica.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TNomeperiferica.Location = New System.Drawing.Point(218, 69)
        Me.TNomeperiferica.Name = "TNomeperiferica"
        Me.TNomeperiferica.Size = New System.Drawing.Size(77, 16)
        Me.TNomeperiferica.TabIndex = 94
        Me.TNomeperiferica.Text = "Nome Per"
        '
        'TDescrizioneperiferica
        '
        Me.TDescrizioneperiferica.AutoSize = True
        Me.TDescrizioneperiferica.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TDescrizioneperiferica.Location = New System.Drawing.Point(390, 68)
        Me.TDescrizioneperiferica.Name = "TDescrizioneperiferica"
        Me.TDescrizioneperiferica.Size = New System.Drawing.Size(77, 16)
        Me.TDescrizioneperiferica.TabIndex = 95
        Me.TDescrizioneperiferica.Text = "Descr Per"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Location = New System.Drawing.Point(0, 146)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(732, 587)
        Me.TabControl1.TabIndex = 96
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.labelDescrizioneCliente)
        Me.TabPage1.Controls.Add(Me.MaskedTextBox1)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.Button2)
        Me.TabPage1.Controls.Add(Me.T_OrdineBindingNavigator)
        Me.TabPage1.Controls.Add(Me.TDescrizioneperiferica)
        Me.TabPage1.Controls.Add(Me.Id_OrdineLabel2)
        Me.TabPage1.Controls.Add(Me.TNomeperiferica)
        Me.TabPage1.Controls.Add(Me.GroupBox3)
        Me.TabPage1.Controls.Add(Firmware9Label)
        Me.TabPage1.Controls.Add(Me.DataInsDateTimePicker)
        Me.TabPage1.Controls.Add(Me.Firmware9Label1)
        Me.TabPage1.Controls.Add(DataInsLabel)
        Me.TabPage1.Controls.Add(Firmware8Label)
        Me.TabPage1.Controls.Add(ArticoloLabel)
        Me.TabPage1.Controls.Add(Me.Firmware8Label1)
        Me.TabPage1.Controls.Add(DescrizioneClienteLabel)
        Me.TabPage1.Controls.Add(Firmware7Label)
        Me.TabPage1.Controls.Add(Me.FKPerifericaTextBox)
        Me.TabPage1.Controls.Add(Me.Firmware7Label1)
        Me.TabPage1.Controls.Add(FKPerifericaLabel)
        Me.TabPage1.Controls.Add(Firmware6Label)
        Me.TabPage1.Controls.Add(Id_OrdineLabel)
        Me.TabPage1.Controls.Add(Me.Firmware6Label1)
        Me.TabPage1.Controls.Add(Me.N_OrdineLabel2)
        Me.TabPage1.Controls.Add(Firmware5Label)
        Me.TabPage1.Controls.Add(N_OrdineLabel1)
        Me.TabPage1.Controls.Add(Me.Firmware5Label1)
        Me.TabPage1.Controls.Add(Firmware4Label)
        Me.TabPage1.Controls.Add(Me.Firmware4Label1)
        Me.TabPage1.Controls.Add(Firmware3Label)
        Me.TabPage1.Controls.Add(Me.Firmware3Label1)
        Me.TabPage1.Controls.Add(Me.ArticoloLabel2)
        Me.TabPage1.Controls.Add(Firmware2Label)
        Me.TabPage1.Controls.Add(Me.Firmware1Label1)
        Me.TabPage1.Controls.Add(Me.Firmware2Label1)
        Me.TabPage1.Controls.Add(Firmware1Label)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(724, 561)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Scorri Configurazioni"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'labelDescrizioneCliente
        '
        Me.labelDescrizioneCliente.AutoSize = True
        Me.labelDescrizioneCliente.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_OrdineBindingSource, "DescrizioneCliente", True))
        Me.labelDescrizioneCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold)
        Me.labelDescrizioneCliente.Location = New System.Drawing.Point(300, 133)
        Me.labelDescrizioneCliente.Name = "labelDescrizioneCliente"
        Me.labelDescrizioneCliente.Size = New System.Drawing.Size(142, 16)
        Me.labelDescrizioneCliente.TabIndex = 100
        Me.labelDescrizioneCliente.Text = "Descrizione Cliente"
        '
        'MaskedTextBox1
        '
        Me.MaskedTextBox1.Location = New System.Drawing.Point(294, 4)
        Me.MaskedTextBox1.Name = "MaskedTextBox1"
        Me.MaskedTextBox1.Size = New System.Drawing.Size(98, 20)
        Me.MaskedTextBox1.TabIndex = 99
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(235, 7)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 13)
        Me.Label2.TabIndex = 97
        Me.Label2.Text = "Articolo"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.LightCyan
        Me.Button2.Location = New System.Drawing.Point(398, -1)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 29)
        Me.Button2.TabIndex = 98
        Me.Button2.Text = "Cerca"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.RLS40LC)
        Me.GroupBox1.Controls.Add(Me.RNewCod)
        Me.GroupBox1.Controls.Add(Me.RLS40UV)
        Me.GroupBox1.Controls.Add(Me.RadioButton1)
        Me.GroupBox1.Controls.Add(Me.RALL)
        Me.GroupBox1.Controls.Add(Me.RLS40)
        Me.GroupBox1.Controls.Add(Me.RLS515)
        Me.GroupBox1.Controls.Add(Me.RLS150COL)
        Me.GroupBox1.Controls.Add(Me.RLS150UV)
        Me.GroupBox1.Controls.Add(Me.RLS150)
        Me.GroupBox1.Controls.Add(Me.RLS100)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(741, 140)
        Me.GroupBox1.TabIndex = 97
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Seleziona Periferica"
        '
        'RLS40LC
        '
        Me.RLS40LC.AutoSize = True
        Me.RLS40LC.Location = New System.Drawing.Point(18, 32)
        Me.RLS40LC.Name = "RLS40LC"
        Me.RLS40LC.Size = New System.Drawing.Size(50, 17)
        Me.RLS40LC.TabIndex = 13
        Me.RLS40LC.TabStop = True
        Me.RLS40LC.Text = "LS40"
        Me.RLS40LC.UseVisualStyleBackColor = True
        '
        'RNewCod
        '
        Me.RNewCod.AutoSize = True
        Me.RNewCod.Location = New System.Drawing.Point(201, 32)
        Me.RNewCod.Name = "RNewCod"
        Me.RNewCod.Size = New System.Drawing.Size(56, 17)
        Me.RNewCod.TabIndex = 12
        Me.RNewCod.TabStop = True
        Me.RNewCod.Text = "LS150"
        Me.RNewCod.UseVisualStyleBackColor = True
        '
        'RLS40UV
        '
        Me.RLS40UV.AutoSize = True
        Me.RLS40UV.Location = New System.Drawing.Point(558, 80)
        Me.RLS40UV.Name = "RLS40UV"
        Me.RLS40UV.Size = New System.Drawing.Size(139, 17)
        Me.RLS40UV.TabIndex = 10
        Me.RLS40UV.TabStop = True
        Me.RLS40UV.Text = "LS40UV/COLOR/MyT2"
        Me.RLS40UV.UseVisualStyleBackColor = True
        Me.RLS40UV.Visible = False
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Location = New System.Drawing.Point(201, 66)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(74, 17)
        Me.RadioButton1.TabIndex = 9
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "LS515 UV"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'RALL
        '
        Me.RALL.AutoSize = True
        Me.RALL.Location = New System.Drawing.Point(353, 66)
        Me.RALL.Name = "RALL"
        Me.RALL.Size = New System.Drawing.Size(151, 17)
        Me.RALL.TabIndex = 8
        Me.RALL.TabStop = True
        Me.RALL.Text = "TUTTE LE PERIFERICHE"
        Me.RALL.UseVisualStyleBackColor = True
        '
        'RLS40
        '
        Me.RLS40.AutoSize = True
        Me.RLS40.Location = New System.Drawing.Point(558, 34)
        Me.RLS40.Name = "RLS40"
        Me.RLS40.Size = New System.Drawing.Size(67, 17)
        Me.RLS40.TabIndex = 6
        Me.RLS40.TabStop = True
        Me.RLS40.Text = "LS40 old"
        Me.RLS40.UseVisualStyleBackColor = True
        Me.RLS40.Visible = False
        '
        'RLS515
        '
        Me.RLS515.AutoSize = True
        Me.RLS515.Location = New System.Drawing.Point(18, 66)
        Me.RLS515.Name = "RLS515"
        Me.RLS515.Size = New System.Drawing.Size(56, 17)
        Me.RLS515.TabIndex = 4
        Me.RLS515.TabStop = True
        Me.RLS515.Text = "LS515"
        Me.RLS515.UseVisualStyleBackColor = True
        '
        'RLS150COL
        '
        Me.RLS150COL.AutoSize = True
        Me.RLS150COL.Location = New System.Drawing.Point(657, 57)
        Me.RLS150COL.Name = "RLS150COL"
        Me.RLS150COL.Size = New System.Drawing.Size(72, 17)
        Me.RLS150COL.TabIndex = 3
        Me.RLS150COL.TabStop = True
        Me.RLS150COL.Text = "LS150 CL"
        Me.RLS150COL.UseVisualStyleBackColor = True
        Me.RLS150COL.Visible = False
        '
        'RLS150UV
        '
        Me.RLS150UV.AutoSize = True
        Me.RLS150UV.Location = New System.Drawing.Point(558, 57)
        Me.RLS150UV.Name = "RLS150UV"
        Me.RLS150UV.Size = New System.Drawing.Size(74, 17)
        Me.RLS150UV.TabIndex = 2
        Me.RLS150UV.TabStop = True
        Me.RLS150UV.Text = "LS150 UV"
        Me.RLS150UV.UseVisualStyleBackColor = True
        Me.RLS150UV.Visible = False
        '
        'RLS150
        '
        Me.RLS150.AutoSize = True
        Me.RLS150.Location = New System.Drawing.Point(657, 32)
        Me.RLS150.Name = "RLS150"
        Me.RLS150.Size = New System.Drawing.Size(75, 17)
        Me.RLS150.TabIndex = 1
        Me.RLS150.TabStop = True
        Me.RLS150.Text = "LS150 GR"
        Me.RLS150.UseVisualStyleBackColor = True
        Me.RLS150.Visible = False
        '
        'RLS100
        '
        Me.RLS100.AutoSize = True
        Me.RLS100.Location = New System.Drawing.Point(353, 32)
        Me.RLS100.Name = "RLS100"
        Me.RLS100.Size = New System.Drawing.Size(56, 17)
        Me.RLS100.TabIndex = 0
        Me.RLS100.TabStop = True
        Me.RLS100.Text = "LS100"
        Me.RLS100.UseVisualStyleBackColor = True
        '
        'DettaglioOrdineBindingSource
        '
        Me.DettaglioOrdineBindingSource.DataMember = "DettaglioOrdine"
        Me.DettaglioOrdineBindingSource.DataSource = Me.Collaudi02DataSet
        '
        'T_OrdineTableAdapter
        '
        Me.T_OrdineTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AutenticazioneTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.CategorieOpzioniTableAdapter = Me.CategorieOpzioniTableAdapter
        Me.TableAdapterManager.DettaglioOrdineTableAdapter = Me.DettaglioOrdineTableAdapter
        Me.TableAdapterManager.OperatoriTableAdapter = Nothing
        Me.TableAdapterManager.SequenzaCollaudoTableAdapter = Nothing
        Me.TableAdapterManager.SequenzaTableAdapter = Nothing
        Me.TableAdapterManager.SequOperaTableAdapter = Nothing
        Me.TableAdapterManager.T_Coll_FaseTableAdapter = Nothing
        Me.TableAdapterManager.T_CollaudoTableAdapter = Nothing
        Me.TableAdapterManager.T_DatiFineCollaudoTableAdapter = Nothing
        Me.TableAdapterManager.T_FasicollaudoTableAdapter = Nothing
        Me.TableAdapterManager.T_FirmwareTableAdapter = Nothing
        Me.TableAdapterManager.T_ImmaginiTableAdapter = Nothing
        Me.TableAdapterManager.T_Mess_PerifTableAdapter = Nothing
        Me.TableAdapterManager.T_MessaggiTableAdapter = Nothing
        Me.TableAdapterManager.T_OrdineTableAdapter = Me.T_OrdineTableAdapter
        Me.TableAdapterManager.T_PerifericaTableAdapter = Me.T_PerifericaTableAdapter
        Me.TableAdapterManager.T_Staz_CollTableAdapter = Nothing
        Me.TableAdapterManager.T_StoricoTableAdapter = Nothing
        Me.TableAdapterManager.T_TipoDatoTableAdapter = Nothing
        Me.TableAdapterManager.T_VersioniTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = CollaudoPeriferiche.Collaudi02DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.ValoriOpzioniTableAdapter = Me.ValoriOpzioniTableAdapter
        '
        'CategorieOpzioniTableAdapter
        '
        Me.CategorieOpzioniTableAdapter.ClearBeforeFill = True
        '
        'DettaglioOrdineTableAdapter
        '
        Me.DettaglioOrdineTableAdapter.ClearBeforeFill = True
        '
        'T_PerifericaTableAdapter
        '
        Me.T_PerifericaTableAdapter.ClearBeforeFill = True
        '
        'ValoriOpzioniTableAdapter
        '
        Me.ValoriOpzioniTableAdapter.ClearBeforeFill = True
        '
        'CategorieOpzioniBindingSource
        '
        Me.CategorieOpzioniBindingSource.DataMember = "CategorieOpzioni"
        Me.CategorieOpzioniBindingSource.DataSource = Me.Collaudi02DataSet
        '
        'ValoriOpzioniBindingSource
        '
        Me.ValoriOpzioniBindingSource.DataMember = "ValoriOpzioni"
        Me.ValoriOpzioniBindingSource.DataSource = Me.Collaudi02DataSet
        '
        'T_PerifericaBindingSource
        '
        Me.T_PerifericaBindingSource.DataMember = "T_Periferica"
        Me.T_PerifericaBindingSource.DataSource = Me.Collaudi02DataSet
        '
        'btEsci
        '
        Me.btEsci.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btEsci.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btEsci.Location = New System.Drawing.Point(652, 747)
        Me.btEsci.Name = "btEsci"
        Me.btEsci.Size = New System.Drawing.Size(83, 56)
        Me.btEsci.TabIndex = 98
        Me.btEsci.Text = "Esci"
        Me.btEsci.UseVisualStyleBackColor = False
        Me.btEsci.Visible = False
        '
        'FOrdini
        '
        Me.AcceptButton = Me.Button1
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(741, 815)
        Me.ControlBox = False
        Me.Controls.Add(Me.btEsci)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FOrdini"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Scelta Configurazione"
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.DOpzioni, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.T_OrdineBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.T_OrdineBindingNavigator.ResumeLayout(False)
        Me.T_OrdineBindingNavigator.PerformLayout()
        CType(Me.T_OrdineBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Collaudi02DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DettaglioOrdineBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CategorieOpzioniBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ValoriOpzioniBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.T_PerifericaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Collaudi02DataSet As CollaudoPeriferiche.Collaudi02DataSet
    Friend WithEvents T_OrdineBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_OrdineTableAdapter As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_OrdineTableAdapter
    Friend WithEvents TableAdapterManager As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.TableAdapterManager
    Friend WithEvents T_OrdineBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents FKPerifericaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DataInsDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents DettaglioOrdineTableAdapter As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.DettaglioOrdineTableAdapter
    Friend WithEvents DettaglioOrdineBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CategorieOpzioniTableAdapter As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.CategorieOpzioniTableAdapter
    Friend WithEvents CategorieOpzioniBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ValoriOpzioniTableAdapter As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.ValoriOpzioniTableAdapter
    Friend WithEvents ValoriOpzioniBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_PerifericaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_PerifericaTableAdapter As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_PerifericaTableAdapter
    Friend WithEvents DOpzioni As System.Windows.Forms.DataGridView
    Friend WithEvents Catogoria As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Valore As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Id_OrdineLabel2 As System.Windows.Forms.Label
    Friend WithEvents N_OrdineLabel2 As System.Windows.Forms.Label
    Friend WithEvents ArticoloLabel2 As System.Windows.Forms.Label
    Friend WithEvents Firmware1Label1 As System.Windows.Forms.Label
    Friend WithEvents Firmware2Label1 As System.Windows.Forms.Label
    Friend WithEvents Firmware3Label1 As System.Windows.Forms.Label
    Friend WithEvents Firmware4Label1 As System.Windows.Forms.Label
    Friend WithEvents Firmware5Label1 As System.Windows.Forms.Label
    Friend WithEvents Firmware6Label1 As System.Windows.Forms.Label
    Friend WithEvents Firmware7Label1 As System.Windows.Forms.Label
    Friend WithEvents Firmware8Label1 As System.Windows.Forms.Label
    Friend WithEvents Firmware9Label1 As System.Windows.Forms.Label
    Friend WithEvents TNomeperiferica As System.Windows.Forms.Label
    Friend WithEvents TDescrizioneperiferica As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents MaskedTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents RLS150 As System.Windows.Forms.RadioButton
    Friend WithEvents RLS100 As System.Windows.Forms.RadioButton
    Friend WithEvents RLS150COL As System.Windows.Forms.RadioButton
    Friend WithEvents RLS150UV As System.Windows.Forms.RadioButton
    Friend WithEvents RLS515 As System.Windows.Forms.RadioButton
    Friend WithEvents RALL As System.Windows.Forms.RadioButton
    Friend WithEvents RLS40 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents RLS40UV As System.Windows.Forms.RadioButton
    Friend WithEvents labelDescrizioneCliente As System.Windows.Forms.Label
    Friend WithEvents RNewCod As System.Windows.Forms.RadioButton
    Friend WithEvents btEsci As System.Windows.Forms.Button
    Friend WithEvents RLS40LC As System.Windows.Forms.RadioButton
End Class
