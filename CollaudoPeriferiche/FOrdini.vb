﻿Imports CollaudoPeriferiche.Collaudi02DataSet

Public Class FOrdini
    Public OrdineSelezionato As DataRowView
    Public rigaperif() As Collaudi02DataSet.T_PerifericaRow
    Public rigafirm() As Collaudi02DataSet.T_FirmwareRow
    Public righeopzione() As Collaudi02DataSet.DettaglioOrdineRow
    Public rigaValore() As Collaudi02DataSet.ValoriOpzioniRow
    Public rigaCategoria() As Collaudi02DataSet.CategorieOpzioniRow
    Public ordScelto() As Collaudi02DataSet.T_OrdineRow
    Public Lfw1, Lfw2, Lfw3, Lfw4, Lfw5, Lfw6, Lfw7, Lfw8, Lfw9 As String

    Public VettValoriOpzioni As Array
    Dim bf As Boolean

    Public rigaordine As DataRowView

    Private Sub UscitaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub FOrdini_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        Select Case (e.KeyValue)
            Case Keys.Right
                T_OrdineBindingSource.MoveNext()
            Case Keys.Left
                T_OrdineBindingSource.MovePrevious()
            Case Keys.Enter
                Button1_Click(sender, e)
            


        End Select
    End Sub


    Private Sub FOrdini_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Dim fail As Boolean
        'fail = False
        bf = False
        OrdineSelezionato = Nothing
        MaskedTextBox1.Text = ""

        If Form1.fServerPresente = True Then
            Try
                'Collaudi02DataSet.CategorieOpzioni.Clear()
                'Collaudi02DataSet.ValoriOpzioni.Clear()
                'Collaudi02DataSet.DettaglioOrdine.Clear()
                'Collaudi02DataSet.T_Periferica.Clear()
                'Collaudi02DataSet.T_Ordine.Clear()
                'TODO: questa riga di codice carica i dati nella tabella 'CollaudiDataSet.CategorieOpzioni'. È possibile spostarla o rimuoverla se necessario.
                Me.CategorieOpzioniTableAdapter.Fill(Me.Collaudi02DataSet.CategorieOpzioni)
                'TODO: questa riga di codice carica i dati nella tabella 'CollaudiDataSet.ValoriOpzioni'. È possibile spostarla o rimuoverla se necessario.
                Me.ValoriOpzioniTableAdapter.Fill(Me.Collaudi02DataSet.ValoriOpzioni)
                'TODO: questa riga di codice carica i dati nella tabella 'CollaudiDataSet.DettaglioOrdine'. È possibile spostarla o rimuoverla se necessario.
                Me.DettaglioOrdineTableAdapter.Fill(Me.Collaudi02DataSet.DettaglioOrdine)
                'TODO: questa riga di codice carica i dati nella tabella 'CollaudiDataSet.T_Firmware'. È possibile spostarla o rimuoverla se necessario.


                'TODO: questa riga di codice carica i dati nella tabella 'CollaudiDataSet.T_Periferica'. È possibile spostarla o rimuoverla se necessario.
                Me.T_PerifericaTableAdapter.Fill(Me.Collaudi02DataSet.T_Periferica)
                'TODO: questa riga di codice carica i dati nella tabella 'CollaudiDataSet.T_Ordine'. È possibile spostarla o rimuoverla se necessario.
                Me.T_OrdineTableAdapter.Fill(Me.Collaudi02DataSet.T_Ordine)


                BindingNavigatorPositionItem.Focus()
            Catch ex As Exception
                MessageBox.Show("Attenzione Connessione di Rete non Presente !!! RIAVVIARE IL PROGRAMMA", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Me.Close()
                'fail = True
            End Try
        Else
            Collaudi02DataSet.CategorieOpzioni.Clear()
            Collaudi02DataSet.ValoriOpzioni.Clear()
            Collaudi02DataSet.DettaglioOrdine.Clear()
            Collaudi02DataSet.T_Periferica.Clear()
            Collaudi02DataSet.T_Ordine.Clear()

            Collaudi02DataSet.CategorieOpzioni.ReadXml(Application.StartupPath + Form1.LOCALDB + Form1.CATEGORIEOPZIONI)
            Collaudi02DataSet.ValoriOpzioni.ReadXml(Application.StartupPath + Form1.LOCALDB + Form1.VALORIOPZIONI)
            Collaudi02DataSet.DettaglioOrdine.ReadXml(Application.StartupPath + Form1.LOCALDB + Form1.DETTAGLIOORDINE)

            Collaudi02DataSet.T_Periferica.ReadXml(Application.StartupPath + Form1.LOCALDB + Form1.T_PERIFERICA)
            Collaudi02DataSet.T_Ordine.ReadXml(Application.StartupPath + Form1.LOCALDB + Form1.T_ORDINE)
        End If

        'If fail = True Then
        '    Try
        '        'TODO: questa riga di codice carica i dati nella tabella 'CollaudiDataSet.CategorieOpzioni'. È possibile spostarla o rimuoverla se necessario.
        '        Me.CategorieOpzioniTableAdapter.Fill(Me.Collaudi02DataSet.CategorieOpzioni)
        '        'TODO: questa riga di codice carica i dati nella tabella 'CollaudiDataSet.ValoriOpzioni'. È possibile spostarla o rimuoverla se necessario.
        '        Me.ValoriOpzioniTableAdapter.Fill(Me.Collaudi02DataSet.ValoriOpzioni)
        '        'TODO: questa riga di codice carica i dati nella tabella 'CollaudiDataSet.DettaglioOrdine'. È possibile spostarla o rimuoverla se necessario.
        '        Me.DettaglioOrdineTableAdapter.Fill(Me.Collaudi02DataSet.DettaglioOrdine)
        '        'TODO: questa riga di codice carica i dati nella tabella 'CollaudiDataSet.T_Firmware'. È possibile spostarla o rimuoverla se necessario.


        '        'TODO: questa riga di codice carica i dati nella tabella 'CollaudiDataSet.T_Periferica'. È possibile spostarla o rimuoverla se necessario.
        '        Me.T_PerifericaTableAdapter.Fill(Me.Collaudi02DataSet.T_Periferica)
        '        'TODO: questa riga di codice carica i dati nella tabella 'CollaudiDataSet.T_Ordine'. È possibile spostarla o rimuoverla se necessario.
        '        Me.T_OrdineTableAdapter.Fill(Me.Collaudi02DataSet.T_Ordine)


        '        BindingNavigatorPositionItem.Focus()
        '    Catch ex As Exception
        '        MessageBox.Show("Attenzione Connessione di Rete non Presente !!! RIAVVIARE IL PROGRAMMA", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '        Me.Close()
        '    End Try
        'End If
        

    End Sub

    Private Sub FOrdini_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown


        PopolaVideata()

        bf = True
    End Sub

    Private Sub T_OrdineBindingSource_CurrentChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles T_OrdineBindingSource.CurrentChanged
        If bf = True Then
            PopolaVideata()
        End If

    End Sub


    Public Sub Reset()
        DOpzioni.Rows.Clear()
   
    End Sub
    Public Sub PopolaVideata()
        ' compilo a video i capi delle tabelle associate
        Dim Ricercaperiferica As String

        Dim RicercaOpzione As String
        Dim RicercaCateg As String
        Dim RicercaDescr As String

        ' Pulisco la videata
        Reset()

        ' PERIFERICA

        If FKPerifericaTextBox.Text <> "" Then
            Ricercaperiferica = " ID_Periferica = " + FKPerifericaTextBox.Text
            rigaperif = Collaudi02DataSet.T_Periferica.Select(Ricercaperiferica)

            TNomeperiferica.Text = rigaperif(0)("Nome")
            TDescrizioneperiferica.Text = rigaperif(0)("Descrizione")

        End If
       
        ' FIRMWARE





        ' OPZIONI COLLAUDO
        If (Id_OrdineLabel2.Text <> "") Then
            RicercaOpzione = "Fkordine = " + Id_OrdineLabel2.Text
            righeopzione = Collaudi02DataSet.DettaglioOrdine.Select(RicercaOpzione)
            VettValoriOpzioni = Array.CreateInstance(GetType(System.Int32), righeopzione.Count + 1)
            Try
                For ii As Integer = 0 To righeopzione.Count - 1

                    RicercaCateg = "ID_Catogoria = " + righeopzione(ii)("FK_Categoria").ToString
                    rigaCategoria = Collaudi02DataSet.CategorieOpzioni.Select(RicercaCateg)

                    RicercaDescr = "ID_Valore = " + righeopzione(ii)("FKValoreOpzione").ToString
                    rigaValore = Collaudi02DataSet.ValoriOpzioni.Select(RicercaDescr)

                    DOpzioni.Rows.Add(rigaCategoria(0)("NomeCategoria"), rigaValore(0)("Descrizione"))
                    VettValoriOpzioni(ii) = righeopzione(ii)("FKValoreOpzione")
                Next
                VettValoriOpzioni(righeopzione.Count()) = System.Convert.ToInt32(rigaperif(0)("ID_Periferica"))
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
      
    End Sub


    ' serve per la ricerca da numero di ordine

  
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        OrdineSelezionato = Me.T_OrdineBindingSource.Current

        Me.Close()
    End Sub


   

    Private Sub T_OrdineBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.T_OrdineBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Collaudi02DataSet)

    End Sub

  
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
      
    End Sub

    Private Sub MenuStrip1_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs)

    End Sub

    Private Sub TabControl1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TabControl1.SelectedIndexChanged
      

    End Sub

    
    Private Sub FOrdini_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        bf = False
    End Sub

    Private Sub Button2_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Dim r As Integer
        Dim RicercaOrdine As String
        Reset()
        Dim Ordin As DataRowView
        Dim fcicla As Boolean = True
        Dim trovato As Boolean
        trovato = False

        If MaskedTextBox1.Text = "" Then
        Else
            RicercaOrdine = " N_Ordine = " + MaskedTextBox1.Text
            ' ordScelto = Me.Collaudi02DataSet.T_Ordine.Select(RicercaOrdine)
            T_OrdineBindingSource.MoveFirst()

            While (fcicla)
                Ordin = T_OrdineBindingSource.Current
                If Ordin("N_Ordine") = MaskedTextBox1.Text Then
                    fcicla = False
                    trovato = True
                Else
                    T_OrdineBindingSource.MoveNext()
                    If T_OrdineBindingSource.Position = T_OrdineBindingSource.Count - 1 Then
                        fcicla = False
                        trovato = False
                    End If
                End If


            End While
            If trovato = True Then
                PopolaVideata()
                Button1.Enabled = True
            Else
                MaskedTextBox1.Focus()
                'Reset()
                'Button1.Enabled = False
                MessageBox.Show("Errore... Articolo NON trovato", "Attenzione!!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                btEsci.Visible = True
            End If
            ' r = Me.T_OrdineTableAdapter.FillBynordine(Me.Collaudi02DataSet.T_Ordine, MaskedTextBox1.Text)

        End If
        
    End Sub

    Private Sub BindingNavigatorMoveNextItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorMoveNextItem.Click
        MaskedTextBox1.Text = ""
    End Sub

    Private Sub BindingNavigatorMoveLastItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorMoveLastItem.Click
        MaskedTextBox1.Text = ""
    End Sub

    Private Sub BindingNavigatorMovePreviousItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorMovePreviousItem.Click
        MaskedTextBox1.Text = ""
    End Sub

    Private Sub BindingNavigatorMoveFirstItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorMoveFirstItem.Click
        MaskedTextBox1.Text = ""
    End Sub

    Private Sub FillByPerifericaToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
       

    End Sub

    Private Sub T_OrdineBindingNavigator_RefreshItems(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles T_OrdineBindingNavigator.RefreshItems

    End Sub

    Private Sub RLS100_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RLS100.CheckedChanged
        FKPerifericaTextBox.Text = "30"
        FillaperPeriferica(30)
    End Sub
    Private Sub FillaperPeriferica(ByVal per As Integer)
        If Form1.fServerPresente = True Then

            Try
                Dim param1 As Object = New Object
                param1 = per
                'Me.Collaudi02DataSet.T_Ordine.Clear()
                Me.T_OrdineTableAdapter.FillByPeriferica(Me.Collaudi02DataSet.T_Ordine, per)
                T_OrdineBindingNavigator.Refresh()
                BindingNavigatorPositionItem.Focus()
            Catch ex As System.Exception
                'System.Windows.Forms.MessageBox.Show(ex.Message)
                MessageBox.Show("Attenzione Connessione di Rete non Presente !!! SELEZIONARE TUTTE LE PERIFERICHE", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
            PopolaVideata()
        Else
            Console.WriteLine("USO I DATI IN LOCALE")
        End If
    End Sub

    Private Sub RLS150_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RLS150.CheckedChanged
        FKPerifericaTextBox.Text = "47"
        FillaperPeriferica(47)
    End Sub

    Private Sub RLS515_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RLS515.CheckedChanged
        FKPerifericaTextBox.Text = "57"
        FillaperPeriferica(57)
    End Sub

    Private Sub RLS75_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        FKPerifericaTextBox.Text = "43"
        FillaperPeriferica(43)
    End Sub

    Private Sub RLS40_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RLS40.CheckedChanged
        FKPerifericaTextBox.Text = "48"
        FillaperPeriferica(48)
    End Sub

    Private Sub RSB50E_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        FKPerifericaTextBox.Text = "41"
        FillaperPeriferica(41)
    End Sub

    Private Sub RLS150UV_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RLS150UV.CheckedChanged
        FKPerifericaTextBox.Text = "53"
        FillaperPeriferica(53)
    End Sub

    Private Sub RLS150COL_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RLS150COL.CheckedChanged
        FKPerifericaTextBox.Text = "54"
        FillaperPeriferica(54)
    End Sub

    Private Sub RALL_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RALL.CheckedChanged

        If Form1.fServerPresente = True Then
            Me.T_OrdineTableAdapter.Fill(Me.Collaudi02DataSet.T_Ordine)
            BindingNavigatorPositionItem.Focus()
        Else
            Console.WriteLine("Prende i valori da locale")
        End If
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        FKPerifericaTextBox.Text = "75"
        FillaperPeriferica(75)
    End Sub

    Private Sub RLS40UV_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RLS40UV.CheckedChanged
        FKPerifericaTextBox.Text = "80"
        FillaperPeriferica(80)
    End Sub

  

    Private Sub RNewCod_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RNewCod.CheckedChanged
        'If Form1.fServerPresente = True Then
        '    Me.T_OrdineTableAdapter.Fill(Me.Collaudi02DataSet.T_Ordine)
        '    BindingNavigatorPositionItem.Focus()
        'Else
        '    Console.WriteLine("Prende i valori da locale")
        'End If
        FKPerifericaTextBox.Text = "99"
        FillaperPeriferica(99)
    End Sub

 
    Private Sub btEsci_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btEsci.Click
        Me.Close()
    End Sub

    Private Sub RLS40LC_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RLS40LC.CheckedChanged
        FKPerifericaTextBox.Text = "100"
        FillaperPeriferica(100)
    End Sub
End Class