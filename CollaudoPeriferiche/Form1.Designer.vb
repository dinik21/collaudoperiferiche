﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim lblFW8 As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Dim lblSuite As System.Windows.Forms.Label
        Dim N_OrdineLabel As System.Windows.Forms.Label
        Dim lblFw6 As System.Windows.Forms.Label
        Dim lblFW7 As System.Windows.Forms.Label
        Dim lblFW4 As System.Windows.Forms.Label
        Dim lblFW5 As System.Windows.Forms.Label
        Dim lblFW2 As System.Windows.Forms.Label
        Dim lblFW3 As System.Windows.Forms.Label
        Dim NumPerifericheLabel As System.Windows.Forms.Label
        Dim MatricolainizialeLabel As System.Windows.Forms.Label
        Dim lblDescrizione As System.Windows.Forms.Label
        Dim lblNomePerif As System.Windows.Forms.Label
        Dim lblfamiglia As System.Windows.Forms.Label
        Dim lblFW1 As System.Windows.Forms.Label
        Me.SplitForm = New System.Windows.Forms.SplitContainer
        Me.lbllocale = New System.Windows.Forms.Label
        Me.LColla = New System.Windows.Forms.Label
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.lbltempocollaudo = New System.Windows.Forms.Label
        Me.LTempoTot = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.LDb = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        Me.LTempo = New System.Windows.Forms.Label
        Me.lblTempo = New System.Windows.Forms.Label
        Me.LTentativo = New System.Windows.Forms.Label
        Me.LTentativiPossibili = New System.Windows.Forms.Label
        Me.lblTentativo = New System.Windows.Forms.Label
        Me.lblTentativi = New System.Windows.Forms.Label
        Me.GroupBox7 = New System.Windows.Forms.GroupBox
        Me.LFase = New System.Windows.Forms.Label
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.LstatisticaFasi = New System.Windows.Forms.ListBox
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.lblretro = New System.Windows.Forms.Label
        Me.lblfronte = New System.Windows.Forms.Label
        Me.Label33 = New System.Windows.Forms.Label
        Me.Label32 = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.Label35 = New System.Windows.Forms.Label
        Me.Label34 = New System.Windows.Forms.Label
        Me.LDb2 = New System.Windows.Forms.Label
        Me.lblVer4 = New System.Windows.Forms.Label
        Me.lblVer3 = New System.Windows.Forms.Label
        Me.lblVer2 = New System.Windows.Forms.Label
        Me.lblVer1 = New System.Windows.Forms.Label
        Me.GrpOrdine = New System.Windows.Forms.GroupBox
        Me.Lfw8 = New System.Windows.Forms.Label
        Me.Lfw6 = New System.Windows.Forms.Label
        Me.Lfw4 = New System.Windows.Forms.Label
        Me.Lfw3 = New System.Windows.Forms.Label
        Me.LFam = New System.Windows.Forms.Label
        Me.LNomep = New System.Windows.Forms.Label
        Me.Lfw2 = New System.Windows.Forms.Label
        Me.Lfw1 = New System.Windows.Forms.Label
        Me.LDescp = New System.Windows.Forms.Label
        Me.Lfw9 = New System.Windows.Forms.Label
        Me.Lfw7 = New System.Windows.Forms.Label
        Me.Lfw5 = New System.Windows.Forms.Label
        Me.LMatric = New System.Windows.Forms.Label
        Me.LNPer = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.LNordine = New System.Windows.Forms.Label
        Me.DOpzioni = New System.Windows.Forms.DataGridView
        Me.Catogoria = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Valore = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.LIndIP = New System.Windows.Forms.Label
        Me.lblIP = New System.Windows.Forms.Label
        Me.LAzienda = New System.Windows.Forms.Label
        Me.LNomeLinea = New System.Windows.Forms.Label
        Me.LNomeStaz = New System.Windows.Forms.Label
        Me.lblAzienda = New System.Windows.Forms.Label
        Me.lblinea = New System.Windows.Forms.Label
        Me.lblNomeStazione = New System.Windows.Forms.Label
        Me.LOperatore = New System.Windows.Forms.Label
        Me.lblOperatore = New System.Windows.Forms.Label
        Me.MMemu = New System.Windows.Forms.MenuStrip
        Me.UscitaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.OperatoreToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.StazioneDiCollaudoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.OrdiniToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.CollaudoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ProvaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.TestToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.PiastreLS40ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.Ls40GrigioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.Ls40UvCOLOToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.PiastraLs515ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.Ls515SeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.Ls515UVToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.PiastreLs150ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.PistreLs1007ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.CollaudoBrasileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.Ls40ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.Ls40UVCOLOREToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.Ls150ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.Ls100ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.CheckToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.LettoreEScannerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ChipCardToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.TestToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.FaseBar1 = New BarraFasi.FaseBar
        Me.FaseBar2 = New BarraFasi.FaseBar
        Me.Collaudi02DataSet = New CollaudoPeriferiche.Collaudi02DataSet
        Me.SequenzaCollaudoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SequenzaCollaudoTableAdapter = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.SequenzaCollaudoTableAdapter
        Me.TableAdapterManager = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.TableAdapterManager
        Me.CategorieOpzioniTableAdapter = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.CategorieOpzioniTableAdapter
        Me.DettaglioOrdineTableAdapter = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.DettaglioOrdineTableAdapter
        Me.OperatoriTableAdapter = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.OperatoriTableAdapter
        Me.SequenzaTableAdapter = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.SequenzaTableAdapter
        Me.T_Coll_FaseTableAdapter = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_Coll_FaseTableAdapter
        Me.T_CollaudoTableAdapter = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_CollaudoTableAdapter
        Me.T_DatiFineCollaudoTableAdapter = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_DatiFineCollaudoTableAdapter
        Me.T_FasicollaudoTableAdapter = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_FasicollaudoTableAdapter
        Me.T_ImmaginiTableAdapter = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_ImmaginiTableAdapter
        Me.T_MessaggiTableAdapter = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_MessaggiTableAdapter
        Me.T_OrdineTableAdapter = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_OrdineTableAdapter
        Me.T_PerifericaTableAdapter = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_PerifericaTableAdapter
        Me.T_Staz_CollTableAdapter = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_Staz_CollTableAdapter
        Me.T_StoricoTableAdapter = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_StoricoTableAdapter
        Me.T_VersioniTableAdapter = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_VersioniTableAdapter
        Me.ValoriOpzioniTableAdapter = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.ValoriOpzioniTableAdapter
        Me.T_CollaudoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.T_FasicollaudoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.T_StoricoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.T_Staz_CollBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DettaglioOrdineBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ValoriOpzioniBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CategorieOpzioniBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.T_Coll_FaseBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.T_VersioniBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.T_statisticaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.T_statisticaTableAdapter = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_statisticaTableAdapter
        Me.T_ImmaginiBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.T_OrdineBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.OperatoriBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.T_PerifericaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SequenzaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.T_MessaggiBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.T_DatiFineCollaudoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        lblFW8 = New System.Windows.Forms.Label
        lblSuite = New System.Windows.Forms.Label
        N_OrdineLabel = New System.Windows.Forms.Label
        lblFw6 = New System.Windows.Forms.Label
        lblFW7 = New System.Windows.Forms.Label
        lblFW4 = New System.Windows.Forms.Label
        lblFW5 = New System.Windows.Forms.Label
        lblFW2 = New System.Windows.Forms.Label
        lblFW3 = New System.Windows.Forms.Label
        NumPerifericheLabel = New System.Windows.Forms.Label
        MatricolainizialeLabel = New System.Windows.Forms.Label
        lblDescrizione = New System.Windows.Forms.Label
        lblNomePerif = New System.Windows.Forms.Label
        lblfamiglia = New System.Windows.Forms.Label
        lblFW1 = New System.Windows.Forms.Label
        Me.SplitForm.Panel1.SuspendLayout()
        Me.SplitForm.Panel2.SuspendLayout()
        Me.SplitForm.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GrpOrdine.SuspendLayout()
        CType(Me.DOpzioni, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.MMemu.SuspendLayout()
        CType(Me.Collaudi02DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SequenzaCollaudoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.T_CollaudoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.T_FasicollaudoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.T_StoricoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.T_Staz_CollBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DettaglioOrdineBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ValoriOpzioniBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CategorieOpzioniBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.T_Coll_FaseBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.T_VersioniBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.T_statisticaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.T_ImmaginiBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.T_OrdineBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OperatoriBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.T_PerifericaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SequenzaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.T_MessaggiBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.T_DatiFineCollaudoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblFW8
        '
        resources.ApplyResources(lblFW8, "lblFW8")
        lblFW8.ForeColor = System.Drawing.SystemColors.ActiveCaption
        lblFW8.Name = "lblFW8"
        '
        'SplitForm
        '
        resources.ApplyResources(Me.SplitForm, "SplitForm")
        Me.SplitForm.Name = "SplitForm"
        '
        'SplitForm.Panel1
        '
        resources.ApplyResources(Me.SplitForm.Panel1, "SplitForm.Panel1")
        Me.SplitForm.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.SplitForm.Panel1.Controls.Add(Me.lbllocale)
        Me.SplitForm.Panel1.Controls.Add(Me.LColla)
        Me.SplitForm.Panel1.Controls.Add(Me.GroupBox4)
        Me.SplitForm.Panel1.Controls.Add(Me.GroupBox3)
        Me.SplitForm.Panel1.Controls.Add(Me.GroupBox7)
        Me.SplitForm.Panel1.Controls.Add(Me.GroupBox6)
        Me.SplitForm.Panel1.Controls.Add(Me.GroupBox5)
        '
        'SplitForm.Panel2
        '
        resources.ApplyResources(Me.SplitForm.Panel2, "SplitForm.Panel2")
        Me.SplitForm.Panel2.Controls.Add(Me.LDb2)
        Me.SplitForm.Panel2.Controls.Add(Me.lblVer4)
        Me.SplitForm.Panel2.Controls.Add(Me.lblVer3)
        Me.SplitForm.Panel2.Controls.Add(Me.lblVer2)
        Me.SplitForm.Panel2.Controls.Add(Me.lblVer1)
        Me.SplitForm.Panel2.Controls.Add(Me.GrpOrdine)
        Me.SplitForm.Panel2.Controls.Add(Me.GroupBox1)
        Me.SplitForm.Panel2.Controls.Add(Me.LOperatore)
        Me.SplitForm.Panel2.Controls.Add(Me.lblOperatore)
        '
        'lbllocale
        '
        resources.ApplyResources(Me.lbllocale, "lbllocale")
        Me.lbllocale.ForeColor = System.Drawing.Color.Black
        Me.lbllocale.Name = "lbllocale"
        '
        'LColla
        '
        resources.ApplyResources(Me.LColla, "LColla")
        Me.LColla.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LColla.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.LColla.Name = "LColla"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.lbltempocollaudo)
        Me.GroupBox4.Controls.Add(Me.LTempoTot)
        resources.ApplyResources(Me.GroupBox4, "GroupBox4")
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.TabStop = False
        '
        'lbltempocollaudo
        '
        resources.ApplyResources(Me.lbltempocollaudo, "lbltempocollaudo")
        Me.lbltempocollaudo.ForeColor = System.Drawing.Color.Black
        Me.lbltempocollaudo.Name = "lbltempocollaudo"
        '
        'LTempoTot
        '
        resources.ApplyResources(Me.LTempoTot, "LTempoTot")
        Me.LTempoTot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LTempoTot.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.LTempoTot.Name = "LTempoTot"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.LDb)
        Me.GroupBox3.Controls.Add(Me.Label24)
        Me.GroupBox3.Controls.Add(Me.LTempo)
        Me.GroupBox3.Controls.Add(Me.lblTempo)
        Me.GroupBox3.Controls.Add(Me.LTentativo)
        Me.GroupBox3.Controls.Add(Me.LTentativiPossibili)
        Me.GroupBox3.Controls.Add(Me.lblTentativo)
        Me.GroupBox3.Controls.Add(Me.lblTentativi)
        resources.ApplyResources(Me.GroupBox3, "GroupBox3")
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.TabStop = False
        '
        'LDb
        '
        resources.ApplyResources(Me.LDb, "LDb")
        Me.LDb.Name = "LDb"
        '
        'Label24
        '
        resources.ApplyResources(Me.Label24, "Label24")
        Me.Label24.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label24.Name = "Label24"
        '
        'LTempo
        '
        resources.ApplyResources(Me.LTempo, "LTempo")
        Me.LTempo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LTempo.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.LTempo.Name = "LTempo"
        '
        'lblTempo
        '
        resources.ApplyResources(Me.lblTempo, "lblTempo")
        Me.lblTempo.ForeColor = System.Drawing.Color.Black
        Me.lblTempo.Name = "lblTempo"
        '
        'LTentativo
        '
        resources.ApplyResources(Me.LTentativo, "LTentativo")
        Me.LTentativo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LTentativo.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.LTentativo.Name = "LTentativo"
        '
        'LTentativiPossibili
        '
        resources.ApplyResources(Me.LTentativiPossibili, "LTentativiPossibili")
        Me.LTentativiPossibili.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LTentativiPossibili.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.LTentativiPossibili.Name = "LTentativiPossibili"
        '
        'lblTentativo
        '
        resources.ApplyResources(Me.lblTentativo, "lblTentativo")
        Me.lblTentativo.ForeColor = System.Drawing.Color.Black
        Me.lblTentativo.Name = "lblTentativo"
        '
        'lblTentativi
        '
        resources.ApplyResources(Me.lblTentativi, "lblTentativi")
        Me.lblTentativi.ForeColor = System.Drawing.Color.Black
        Me.lblTentativi.Name = "lblTentativi"
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.LFase)
        resources.ApplyResources(Me.GroupBox7, "GroupBox7")
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.TabStop = False
        '
        'LFase
        '
        Me.LFase.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        resources.ApplyResources(Me.LFase, "LFase")
        Me.LFase.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.LFase.Name = "LFase"
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.LstatisticaFasi)
        resources.ApplyResources(Me.GroupBox6, "GroupBox6")
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.TabStop = False
        '
        'LstatisticaFasi
        '
        resources.ApplyResources(Me.LstatisticaFasi, "LstatisticaFasi")
        Me.LstatisticaFasi.FormattingEnabled = True
        Me.LstatisticaFasi.Name = "LstatisticaFasi"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.PictureBox2)
        Me.GroupBox5.Controls.Add(Me.lblretro)
        Me.GroupBox5.Controls.Add(Me.lblfronte)
        Me.GroupBox5.Controls.Add(Me.Label33)
        Me.GroupBox5.Controls.Add(Me.Label32)
        Me.GroupBox5.Controls.Add(Me.PictureBox1)
        Me.GroupBox5.Controls.Add(Me.Label35)
        Me.GroupBox5.Controls.Add(Me.Label34)
        resources.ApplyResources(Me.GroupBox5, "GroupBox5")
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.SystemColors.AppWorkspace
        resources.ApplyResources(Me.PictureBox2, "PictureBox2")
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.TabStop = False
        '
        'lblretro
        '
        resources.ApplyResources(Me.lblretro, "lblretro")
        Me.lblretro.ForeColor = System.Drawing.Color.Black
        Me.lblretro.Name = "lblretro"
        '
        'lblfronte
        '
        resources.ApplyResources(Me.lblfronte, "lblfronte")
        Me.lblfronte.ForeColor = System.Drawing.Color.Black
        Me.lblfronte.Name = "lblfronte"
        '
        'Label33
        '
        Me.Label33.BackColor = System.Drawing.Color.Blue
        resources.ApplyResources(Me.Label33, "Label33")
        Me.Label33.Name = "Label33"
        '
        'Label32
        '
        Me.Label32.BackColor = System.Drawing.Color.Red
        resources.ApplyResources(Me.Label32, "Label32")
        Me.Label32.Name = "Label32"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.SystemColors.AppWorkspace
        resources.ApplyResources(Me.PictureBox1, "PictureBox1")
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.TabStop = False
        '
        'Label35
        '
        Me.Label35.BackColor = System.Drawing.Color.Red
        resources.ApplyResources(Me.Label35, "Label35")
        Me.Label35.Name = "Label35"
        '
        'Label34
        '
        Me.Label34.BackColor = System.Drawing.Color.Blue
        resources.ApplyResources(Me.Label34, "Label34")
        Me.Label34.Name = "Label34"
        '
        'LDb2
        '
        resources.ApplyResources(Me.LDb2, "LDb2")
        Me.LDb2.Name = "LDb2"
        '
        'lblVer4
        '
        resources.ApplyResources(Me.lblVer4, "lblVer4")
        Me.lblVer4.Name = "lblVer4"
        '
        'lblVer3
        '
        resources.ApplyResources(Me.lblVer3, "lblVer3")
        Me.lblVer3.Name = "lblVer3"
        '
        'lblVer2
        '
        resources.ApplyResources(Me.lblVer2, "lblVer2")
        Me.lblVer2.Name = "lblVer2"
        '
        'lblVer1
        '
        resources.ApplyResources(Me.lblVer1, "lblVer1")
        Me.lblVer1.Name = "lblVer1"
        '
        'GrpOrdine
        '
        Me.GrpOrdine.Controls.Add(Me.Lfw8)
        Me.GrpOrdine.Controls.Add(Me.Lfw6)
        Me.GrpOrdine.Controls.Add(Me.Lfw4)
        Me.GrpOrdine.Controls.Add(Me.Lfw3)
        Me.GrpOrdine.Controls.Add(Me.LFam)
        Me.GrpOrdine.Controls.Add(Me.LNomep)
        Me.GrpOrdine.Controls.Add(Me.Lfw2)
        Me.GrpOrdine.Controls.Add(Me.Lfw1)
        Me.GrpOrdine.Controls.Add(Me.LDescp)
        Me.GrpOrdine.Controls.Add(Me.Lfw9)
        Me.GrpOrdine.Controls.Add(Me.Lfw7)
        Me.GrpOrdine.Controls.Add(Me.Lfw5)
        Me.GrpOrdine.Controls.Add(Me.LMatric)
        Me.GrpOrdine.Controls.Add(Me.LNPer)
        Me.GrpOrdine.Controls.Add(Me.Label18)
        Me.GrpOrdine.Controls.Add(Me.Label17)
        Me.GrpOrdine.Controls.Add(Me.LNordine)
        Me.GrpOrdine.Controls.Add(lblFW8)
        Me.GrpOrdine.Controls.Add(lblSuite)
        Me.GrpOrdine.Controls.Add(N_OrdineLabel)
        Me.GrpOrdine.Controls.Add(lblFw6)
        Me.GrpOrdine.Controls.Add(lblFW7)
        Me.GrpOrdine.Controls.Add(lblFW4)
        Me.GrpOrdine.Controls.Add(lblFW5)
        Me.GrpOrdine.Controls.Add(lblFW2)
        Me.GrpOrdine.Controls.Add(lblFW3)
        Me.GrpOrdine.Controls.Add(NumPerifericheLabel)
        Me.GrpOrdine.Controls.Add(MatricolainizialeLabel)
        Me.GrpOrdine.Controls.Add(lblDescrizione)
        Me.GrpOrdine.Controls.Add(lblNomePerif)
        Me.GrpOrdine.Controls.Add(lblfamiglia)
        Me.GrpOrdine.Controls.Add(Me.DOpzioni)
        Me.GrpOrdine.Controls.Add(lblFW1)
        resources.ApplyResources(Me.GrpOrdine, "GrpOrdine")
        Me.GrpOrdine.Name = "GrpOrdine"
        Me.GrpOrdine.TabStop = False
        '
        'Lfw8
        '
        resources.ApplyResources(Me.Lfw8, "Lfw8")
        Me.Lfw8.Name = "Lfw8"
        '
        'Lfw6
        '
        resources.ApplyResources(Me.Lfw6, "Lfw6")
        Me.Lfw6.Name = "Lfw6"
        '
        'Lfw4
        '
        resources.ApplyResources(Me.Lfw4, "Lfw4")
        Me.Lfw4.Name = "Lfw4"
        '
        'Lfw3
        '
        resources.ApplyResources(Me.Lfw3, "Lfw3")
        Me.Lfw3.Name = "Lfw3"
        '
        'LFam
        '
        resources.ApplyResources(Me.LFam, "LFam")
        Me.LFam.Name = "LFam"
        '
        'LNomep
        '
        resources.ApplyResources(Me.LNomep, "LNomep")
        Me.LNomep.Name = "LNomep"
        '
        'Lfw2
        '
        resources.ApplyResources(Me.Lfw2, "Lfw2")
        Me.Lfw2.Name = "Lfw2"
        '
        'Lfw1
        '
        resources.ApplyResources(Me.Lfw1, "Lfw1")
        Me.Lfw1.Name = "Lfw1"
        '
        'LDescp
        '
        resources.ApplyResources(Me.LDescp, "LDescp")
        Me.LDescp.Name = "LDescp"
        '
        'Lfw9
        '
        resources.ApplyResources(Me.Lfw9, "Lfw9")
        Me.Lfw9.Name = "Lfw9"
        '
        'Lfw7
        '
        resources.ApplyResources(Me.Lfw7, "Lfw7")
        Me.Lfw7.Name = "Lfw7"
        '
        'Lfw5
        '
        resources.ApplyResources(Me.Lfw5, "Lfw5")
        Me.Lfw5.Name = "Lfw5"
        '
        'LMatric
        '
        resources.ApplyResources(Me.LMatric, "LMatric")
        Me.LMatric.Name = "LMatric"
        '
        'LNPer
        '
        resources.ApplyResources(Me.LNPer, "LNPer")
        Me.LNPer.Name = "LNPer"
        '
        'Label18
        '
        resources.ApplyResources(Me.Label18, "Label18")
        Me.Label18.Name = "Label18"
        '
        'Label17
        '
        resources.ApplyResources(Me.Label17, "Label17")
        Me.Label17.Name = "Label17"
        '
        'LNordine
        '
        resources.ApplyResources(Me.LNordine, "LNordine")
        Me.LNordine.Name = "LNordine"
        '
        'lblSuite
        '
        resources.ApplyResources(lblSuite, "lblSuite")
        lblSuite.ForeColor = System.Drawing.SystemColors.ActiveCaption
        lblSuite.Name = "lblSuite"
        '
        'N_OrdineLabel
        '
        resources.ApplyResources(N_OrdineLabel, "N_OrdineLabel")
        N_OrdineLabel.ForeColor = System.Drawing.SystemColors.ActiveCaption
        N_OrdineLabel.Name = "N_OrdineLabel"
        '
        'lblFw6
        '
        resources.ApplyResources(lblFw6, "lblFw6")
        lblFw6.ForeColor = System.Drawing.SystemColors.ActiveCaption
        lblFw6.Name = "lblFw6"
        '
        'lblFW7
        '
        resources.ApplyResources(lblFW7, "lblFW7")
        lblFW7.ForeColor = System.Drawing.SystemColors.ActiveCaption
        lblFW7.Name = "lblFW7"
        '
        'lblFW4
        '
        resources.ApplyResources(lblFW4, "lblFW4")
        lblFW4.ForeColor = System.Drawing.SystemColors.ActiveCaption
        lblFW4.Name = "lblFW4"
        '
        'lblFW5
        '
        resources.ApplyResources(lblFW5, "lblFW5")
        lblFW5.ForeColor = System.Drawing.SystemColors.ActiveCaption
        lblFW5.Name = "lblFW5"
        '
        'lblFW2
        '
        resources.ApplyResources(lblFW2, "lblFW2")
        lblFW2.ForeColor = System.Drawing.SystemColors.ActiveCaption
        lblFW2.Name = "lblFW2"
        '
        'lblFW3
        '
        resources.ApplyResources(lblFW3, "lblFW3")
        lblFW3.ForeColor = System.Drawing.SystemColors.ActiveCaption
        lblFW3.Name = "lblFW3"
        '
        'NumPerifericheLabel
        '
        resources.ApplyResources(NumPerifericheLabel, "NumPerifericheLabel")
        NumPerifericheLabel.ForeColor = System.Drawing.SystemColors.ActiveCaption
        NumPerifericheLabel.Name = "NumPerifericheLabel"
        '
        'MatricolainizialeLabel
        '
        resources.ApplyResources(MatricolainizialeLabel, "MatricolainizialeLabel")
        MatricolainizialeLabel.ForeColor = System.Drawing.SystemColors.ActiveCaption
        MatricolainizialeLabel.Name = "MatricolainizialeLabel"
        '
        'lblDescrizione
        '
        resources.ApplyResources(lblDescrizione, "lblDescrizione")
        lblDescrizione.ForeColor = System.Drawing.SystemColors.ActiveCaption
        lblDescrizione.Name = "lblDescrizione"
        '
        'lblNomePerif
        '
        resources.ApplyResources(lblNomePerif, "lblNomePerif")
        lblNomePerif.ForeColor = System.Drawing.SystemColors.ActiveCaption
        lblNomePerif.Name = "lblNomePerif"
        '
        'lblfamiglia
        '
        resources.ApplyResources(lblfamiglia, "lblfamiglia")
        lblfamiglia.ForeColor = System.Drawing.SystemColors.ActiveCaption
        lblfamiglia.Name = "lblfamiglia"
        '
        'DOpzioni
        '
        Me.DOpzioni.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DOpzioni.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Catogoria, Me.Valore})
        resources.ApplyResources(Me.DOpzioni, "DOpzioni")
        Me.DOpzioni.Name = "DOpzioni"
        '
        'Catogoria
        '
        resources.ApplyResources(Me.Catogoria, "Catogoria")
        Me.Catogoria.Name = "Catogoria"
        Me.Catogoria.ReadOnly = True
        '
        'Valore
        '
        resources.ApplyResources(Me.Valore, "Valore")
        Me.Valore.Name = "Valore"
        Me.Valore.ReadOnly = True
        '
        'lblFW1
        '
        resources.ApplyResources(lblFW1, "lblFW1")
        lblFW1.ForeColor = System.Drawing.SystemColors.ActiveCaption
        lblFW1.Name = "lblFW1"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.LIndIP)
        Me.GroupBox1.Controls.Add(Me.lblIP)
        Me.GroupBox1.Controls.Add(Me.LAzienda)
        Me.GroupBox1.Controls.Add(Me.LNomeLinea)
        Me.GroupBox1.Controls.Add(Me.LNomeStaz)
        Me.GroupBox1.Controls.Add(Me.lblAzienda)
        Me.GroupBox1.Controls.Add(Me.lblinea)
        Me.GroupBox1.Controls.Add(Me.lblNomeStazione)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'LIndIP
        '
        resources.ApplyResources(Me.LIndIP, "LIndIP")
        Me.LIndIP.Name = "LIndIP"
        '
        'lblIP
        '
        resources.ApplyResources(Me.lblIP, "lblIP")
        Me.lblIP.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.lblIP.Name = "lblIP"
        '
        'LAzienda
        '
        resources.ApplyResources(Me.LAzienda, "LAzienda")
        Me.LAzienda.Name = "LAzienda"
        '
        'LNomeLinea
        '
        resources.ApplyResources(Me.LNomeLinea, "LNomeLinea")
        Me.LNomeLinea.Name = "LNomeLinea"
        '
        'LNomeStaz
        '
        resources.ApplyResources(Me.LNomeStaz, "LNomeStaz")
        Me.LNomeStaz.Name = "LNomeStaz"
        '
        'lblAzienda
        '
        resources.ApplyResources(Me.lblAzienda, "lblAzienda")
        Me.lblAzienda.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.lblAzienda.Name = "lblAzienda"
        '
        'lblinea
        '
        resources.ApplyResources(Me.lblinea, "lblinea")
        Me.lblinea.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.lblinea.Name = "lblinea"
        '
        'lblNomeStazione
        '
        resources.ApplyResources(Me.lblNomeStazione, "lblNomeStazione")
        Me.lblNomeStazione.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.lblNomeStazione.Name = "lblNomeStazione"
        '
        'LOperatore
        '
        resources.ApplyResources(Me.LOperatore, "LOperatore")
        Me.LOperatore.MinimumSize = New System.Drawing.Size(100, 0)
        Me.LOperatore.Name = "LOperatore"
        '
        'lblOperatore
        '
        resources.ApplyResources(Me.lblOperatore, "lblOperatore")
        Me.lblOperatore.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.lblOperatore.Name = "lblOperatore"
        '
        'MMemu
        '
        Me.MMemu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UscitaToolStripMenuItem, Me.OperatoreToolStripMenuItem, Me.StazioneDiCollaudoToolStripMenuItem, Me.CollaudoToolStripMenuItem, Me.OrdiniToolStripMenuItem, Me.ProvaToolStripMenuItem, Me.TestToolStripMenuItem, Me.CollaudoBrasileToolStripMenuItem, Me.CheckToolStripMenuItem, Me.ChipCardToolStripMenuItem})
        resources.ApplyResources(Me.MMemu, "MMemu")
        Me.MMemu.Name = "MMemu"
        '
        'UscitaToolStripMenuItem
        '
        Me.UscitaToolStripMenuItem.Name = "UscitaToolStripMenuItem"
        resources.ApplyResources(Me.UscitaToolStripMenuItem, "UscitaToolStripMenuItem")
        '
        'OperatoreToolStripMenuItem
        '
        Me.OperatoreToolStripMenuItem.Name = "OperatoreToolStripMenuItem"
        resources.ApplyResources(Me.OperatoreToolStripMenuItem, "OperatoreToolStripMenuItem")
        '
        'StazioneDiCollaudoToolStripMenuItem
        '
        resources.ApplyResources(Me.StazioneDiCollaudoToolStripMenuItem, "StazioneDiCollaudoToolStripMenuItem")
        Me.StazioneDiCollaudoToolStripMenuItem.Name = "StazioneDiCollaudoToolStripMenuItem"
        '
        'OrdiniToolStripMenuItem
        '
        resources.ApplyResources(Me.OrdiniToolStripMenuItem, "OrdiniToolStripMenuItem")
        Me.OrdiniToolStripMenuItem.Name = "OrdiniToolStripMenuItem"
        '
        'CollaudoToolStripMenuItem
        '
        resources.ApplyResources(Me.CollaudoToolStripMenuItem, "CollaudoToolStripMenuItem")
        Me.CollaudoToolStripMenuItem.Name = "CollaudoToolStripMenuItem"
        '
        'ProvaToolStripMenuItem
        '
        Me.ProvaToolStripMenuItem.Name = "ProvaToolStripMenuItem"
        resources.ApplyResources(Me.ProvaToolStripMenuItem, "ProvaToolStripMenuItem")
        '
        'TestToolStripMenuItem
        '
        Me.TestToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PiastreLS40ToolStripMenuItem, Me.PiastraLs515ToolStripMenuItem, Me.PiastreLs150ToolStripMenuItem, Me.PistreLs1007ToolStripMenuItem})
        Me.TestToolStripMenuItem.Name = "TestToolStripMenuItem"
        resources.ApplyResources(Me.TestToolStripMenuItem, "TestToolStripMenuItem")
        '
        'PiastreLS40ToolStripMenuItem
        '
        Me.PiastreLS40ToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Ls40GrigioToolStripMenuItem, Me.Ls40UvCOLOToolStripMenuItem})
        Me.PiastreLS40ToolStripMenuItem.Name = "PiastreLS40ToolStripMenuItem"
        resources.ApplyResources(Me.PiastreLS40ToolStripMenuItem, "PiastreLS40ToolStripMenuItem")
        '
        'Ls40GrigioToolStripMenuItem
        '
        Me.Ls40GrigioToolStripMenuItem.Name = "Ls40GrigioToolStripMenuItem"
        resources.ApplyResources(Me.Ls40GrigioToolStripMenuItem, "Ls40GrigioToolStripMenuItem")
        '
        'Ls40UvCOLOToolStripMenuItem
        '
        Me.Ls40UvCOLOToolStripMenuItem.Name = "Ls40UvCOLOToolStripMenuItem"
        resources.ApplyResources(Me.Ls40UvCOLOToolStripMenuItem, "Ls40UvCOLOToolStripMenuItem")
        '
        'PiastraLs515ToolStripMenuItem
        '
        Me.PiastraLs515ToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Ls515SeToolStripMenuItem, Me.Ls515UVToolStripMenuItem})
        Me.PiastraLs515ToolStripMenuItem.Name = "PiastraLs515ToolStripMenuItem"
        resources.ApplyResources(Me.PiastraLs515ToolStripMenuItem, "PiastraLs515ToolStripMenuItem")
        '
        'Ls515SeToolStripMenuItem
        '
        Me.Ls515SeToolStripMenuItem.Name = "Ls515SeToolStripMenuItem"
        resources.ApplyResources(Me.Ls515SeToolStripMenuItem, "Ls515SeToolStripMenuItem")
        '
        'Ls515UVToolStripMenuItem
        '
        Me.Ls515UVToolStripMenuItem.Name = "Ls515UVToolStripMenuItem"
        resources.ApplyResources(Me.Ls515UVToolStripMenuItem, "Ls515UVToolStripMenuItem")
        '
        'PiastreLs150ToolStripMenuItem
        '
        Me.PiastreLs150ToolStripMenuItem.Name = "PiastreLs150ToolStripMenuItem"
        resources.ApplyResources(Me.PiastreLs150ToolStripMenuItem, "PiastreLs150ToolStripMenuItem")
        '
        'PistreLs1007ToolStripMenuItem
        '
        Me.PistreLs1007ToolStripMenuItem.Name = "PistreLs1007ToolStripMenuItem"
        resources.ApplyResources(Me.PistreLs1007ToolStripMenuItem, "PistreLs1007ToolStripMenuItem")
        '
        'CollaudoBrasileToolStripMenuItem
        '
        Me.CollaudoBrasileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Ls40ToolStripMenuItem, Me.Ls40UVCOLOREToolStripMenuItem, Me.Ls150ToolStripMenuItem, Me.Ls100ToolStripMenuItem})
        Me.CollaudoBrasileToolStripMenuItem.Name = "CollaudoBrasileToolStripMenuItem"
        resources.ApplyResources(Me.CollaudoBrasileToolStripMenuItem, "CollaudoBrasileToolStripMenuItem")
        '
        'Ls40ToolStripMenuItem
        '
        Me.Ls40ToolStripMenuItem.Name = "Ls40ToolStripMenuItem"
        resources.ApplyResources(Me.Ls40ToolStripMenuItem, "Ls40ToolStripMenuItem")
        '
        'Ls40UVCOLOREToolStripMenuItem
        '
        Me.Ls40UVCOLOREToolStripMenuItem.Name = "Ls40UVCOLOREToolStripMenuItem"
        resources.ApplyResources(Me.Ls40UVCOLOREToolStripMenuItem, "Ls40UVCOLOREToolStripMenuItem")
        '
        'Ls150ToolStripMenuItem
        '
        Me.Ls150ToolStripMenuItem.Name = "Ls150ToolStripMenuItem"
        resources.ApplyResources(Me.Ls150ToolStripMenuItem, "Ls150ToolStripMenuItem")
        '
        'Ls100ToolStripMenuItem
        '
        Me.Ls100ToolStripMenuItem.Name = "Ls100ToolStripMenuItem"
        resources.ApplyResources(Me.Ls100ToolStripMenuItem, "Ls100ToolStripMenuItem")
        '
        'CheckToolStripMenuItem
        '
        Me.CheckToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LettoreEScannerToolStripMenuItem})
        Me.CheckToolStripMenuItem.Name = "CheckToolStripMenuItem"
        resources.ApplyResources(Me.CheckToolStripMenuItem, "CheckToolStripMenuItem")
        '
        'LettoreEScannerToolStripMenuItem
        '
        Me.LettoreEScannerToolStripMenuItem.Name = "LettoreEScannerToolStripMenuItem"
        resources.ApplyResources(Me.LettoreEScannerToolStripMenuItem, "LettoreEScannerToolStripMenuItem")
        '
        'ChipCardToolStripMenuItem
        '
        Me.ChipCardToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TestToolStripMenuItem1})
        Me.ChipCardToolStripMenuItem.Name = "ChipCardToolStripMenuItem"
        resources.ApplyResources(Me.ChipCardToolStripMenuItem, "ChipCardToolStripMenuItem")
        '
        'TestToolStripMenuItem1
        '
        Me.TestToolStripMenuItem1.Name = "TestToolStripMenuItem1"
        resources.ApplyResources(Me.TestToolStripMenuItem1, "TestToolStripMenuItem1")
        '
        'FaseBar1
        '
        Me.FaseBar1.BackColor = System.Drawing.Color.Silver
        Me.FaseBar1.BorderStyle = System.Windows.Forms.Border3DStyle.Flat
        Me.FaseBar1.CycleSpeed = 500
        resources.ApplyResources(Me.FaseBar1, "FaseBar1")
        Me.FaseBar1.FDraw = False
        Me.FaseBar1.Name = "FaseBar1"
        Me.FaseBar1.Phases = CType(resources.GetObject("FaseBar1.Phases"), Microsoft.VisualBasic.Collection)
        Me.FaseBar1.PointLength = 15
        Me.FaseBar1.SelectedIndex = 0
        Me.FaseBar1.TextBuffer = 50
        '
        'FaseBar2
        '
        Me.FaseBar2.BackColor = System.Drawing.Color.Silver
        Me.FaseBar2.BorderStyle = System.Windows.Forms.Border3DStyle.Flat
        Me.FaseBar2.CycleSpeed = 500
        resources.ApplyResources(Me.FaseBar2, "FaseBar2")
        Me.FaseBar2.FDraw = False
        Me.FaseBar2.Name = "FaseBar2"
        Me.FaseBar2.Phases = CType(resources.GetObject("FaseBar2.Phases"), Microsoft.VisualBasic.Collection)
        Me.FaseBar2.PointLength = 15
        Me.FaseBar2.SelectedIndex = 0
        Me.FaseBar2.TextBuffer = 50
        '
        'Collaudi02DataSet
        '
        Me.Collaudi02DataSet.DataSetName = "Collaudi02DataSet"
        Me.Collaudi02DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SequenzaCollaudoBindingSource
        '
        Me.SequenzaCollaudoBindingSource.DataMember = "SequenzaCollaudo"
        Me.SequenzaCollaudoBindingSource.DataSource = Me.Collaudi02DataSet
        '
        'SequenzaCollaudoTableAdapter
        '
        Me.SequenzaCollaudoTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AutenticazioneTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.CategorieOpzioniTableAdapter = Me.CategorieOpzioniTableAdapter
        Me.TableAdapterManager.DettaglioOrdineTableAdapter = Me.DettaglioOrdineTableAdapter
        Me.TableAdapterManager.OperatoriTableAdapter = Me.OperatoriTableAdapter
        Me.TableAdapterManager.SequenzaCollaudoTableAdapter = Me.SequenzaCollaudoTableAdapter
        Me.TableAdapterManager.SequenzaTableAdapter = Me.SequenzaTableAdapter
        Me.TableAdapterManager.SequOperaTableAdapter = Nothing
        Me.TableAdapterManager.T_Coll_FaseTableAdapter = Me.T_Coll_FaseTableAdapter
        Me.TableAdapterManager.T_CollaudoTableAdapter = Me.T_CollaudoTableAdapter
        Me.TableAdapterManager.T_DatiFineCollaudoTableAdapter = Me.T_DatiFineCollaudoTableAdapter
        Me.TableAdapterManager.T_FasicollaudoTableAdapter = Me.T_FasicollaudoTableAdapter
        Me.TableAdapterManager.T_FirmwareTableAdapter = Nothing
        Me.TableAdapterManager.T_ImmaginiTableAdapter = Me.T_ImmaginiTableAdapter
        Me.TableAdapterManager.T_Mess_PerifTableAdapter = Nothing
        Me.TableAdapterManager.T_MessaggiTableAdapter = Me.T_MessaggiTableAdapter
        Me.TableAdapterManager.T_OrdineTableAdapter = Me.T_OrdineTableAdapter
        Me.TableAdapterManager.T_PerifericaTableAdapter = Me.T_PerifericaTableAdapter
        Me.TableAdapterManager.T_Staz_CollTableAdapter = Me.T_Staz_CollTableAdapter
        Me.TableAdapterManager.T_StoricoTableAdapter = Me.T_StoricoTableAdapter
        Me.TableAdapterManager.T_TipoDatoTableAdapter = Nothing
        Me.TableAdapterManager.T_VersioniTableAdapter = Me.T_VersioniTableAdapter
        Me.TableAdapterManager.UpdateOrder = CollaudoPeriferiche.Collaudi02DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.ValoriOpzioniTableAdapter = Me.ValoriOpzioniTableAdapter
        '
        'CategorieOpzioniTableAdapter
        '
        Me.CategorieOpzioniTableAdapter.ClearBeforeFill = True
        '
        'DettaglioOrdineTableAdapter
        '
        Me.DettaglioOrdineTableAdapter.ClearBeforeFill = True
        '
        'OperatoriTableAdapter
        '
        Me.OperatoriTableAdapter.ClearBeforeFill = True
        '
        'SequenzaTableAdapter
        '
        Me.SequenzaTableAdapter.ClearBeforeFill = True
        '
        'T_Coll_FaseTableAdapter
        '
        Me.T_Coll_FaseTableAdapter.ClearBeforeFill = True
        '
        'T_CollaudoTableAdapter
        '
        Me.T_CollaudoTableAdapter.ClearBeforeFill = True
        '
        'T_DatiFineCollaudoTableAdapter
        '
        Me.T_DatiFineCollaudoTableAdapter.ClearBeforeFill = True
        '
        'T_FasicollaudoTableAdapter
        '
        Me.T_FasicollaudoTableAdapter.ClearBeforeFill = True
        '
        'T_ImmaginiTableAdapter
        '
        Me.T_ImmaginiTableAdapter.ClearBeforeFill = True
        '
        'T_MessaggiTableAdapter
        '
        Me.T_MessaggiTableAdapter.ClearBeforeFill = True
        '
        'T_OrdineTableAdapter
        '
        Me.T_OrdineTableAdapter.ClearBeforeFill = True
        '
        'T_PerifericaTableAdapter
        '
        Me.T_PerifericaTableAdapter.ClearBeforeFill = True
        '
        'T_Staz_CollTableAdapter
        '
        Me.T_Staz_CollTableAdapter.ClearBeforeFill = True
        '
        'T_StoricoTableAdapter
        '
        Me.T_StoricoTableAdapter.ClearBeforeFill = True
        '
        'T_VersioniTableAdapter
        '
        Me.T_VersioniTableAdapter.ClearBeforeFill = True
        '
        'ValoriOpzioniTableAdapter
        '
        Me.ValoriOpzioniTableAdapter.ClearBeforeFill = True
        '
        'T_CollaudoBindingSource
        '
        Me.T_CollaudoBindingSource.DataMember = "T_Collaudo"
        Me.T_CollaudoBindingSource.DataSource = Me.Collaudi02DataSet
        '
        'T_FasicollaudoBindingSource
        '
        Me.T_FasicollaudoBindingSource.DataMember = "T_Fasicollaudo"
        Me.T_FasicollaudoBindingSource.DataSource = Me.Collaudi02DataSet
        '
        'T_StoricoBindingSource
        '
        Me.T_StoricoBindingSource.DataMember = "T_Storico"
        Me.T_StoricoBindingSource.DataSource = Me.Collaudi02DataSet
        '
        'T_Staz_CollBindingSource
        '
        Me.T_Staz_CollBindingSource.DataMember = "T_Staz_Coll"
        Me.T_Staz_CollBindingSource.DataSource = Me.Collaudi02DataSet
        '
        'DettaglioOrdineBindingSource
        '
        Me.DettaglioOrdineBindingSource.DataMember = "DettaglioOrdine"
        Me.DettaglioOrdineBindingSource.DataSource = Me.Collaudi02DataSet
        '
        'ValoriOpzioniBindingSource
        '
        Me.ValoriOpzioniBindingSource.DataMember = "ValoriOpzioni"
        Me.ValoriOpzioniBindingSource.DataSource = Me.Collaudi02DataSet
        '
        'CategorieOpzioniBindingSource
        '
        Me.CategorieOpzioniBindingSource.DataMember = "CategorieOpzioni"
        Me.CategorieOpzioniBindingSource.DataSource = Me.Collaudi02DataSet
        '
        'T_Coll_FaseBindingSource
        '
        Me.T_Coll_FaseBindingSource.DataMember = "T_Coll_Fase"
        Me.T_Coll_FaseBindingSource.DataSource = Me.Collaudi02DataSet
        '
        'T_VersioniBindingSource
        '
        Me.T_VersioniBindingSource.DataMember = "T_Versioni"
        Me.T_VersioniBindingSource.DataSource = Me.Collaudi02DataSet
        '
        'T_statisticaBindingSource
        '
        Me.T_statisticaBindingSource.DataMember = "T_statistica"
        Me.T_statisticaBindingSource.DataSource = Me.Collaudi02DataSet
        '
        'T_statisticaTableAdapter
        '
        Me.T_statisticaTableAdapter.ClearBeforeFill = True
        '
        'T_ImmaginiBindingSource
        '
        Me.T_ImmaginiBindingSource.DataMember = "T_Immagini"
        Me.T_ImmaginiBindingSource.DataSource = Me.Collaudi02DataSet
        '
        'T_OrdineBindingSource
        '
        Me.T_OrdineBindingSource.DataMember = "T_Ordine"
        Me.T_OrdineBindingSource.DataSource = Me.Collaudi02DataSet
        '
        'OperatoriBindingSource
        '
        Me.OperatoriBindingSource.DataMember = "Operatori"
        Me.OperatoriBindingSource.DataSource = Me.Collaudi02DataSet
        '
        'T_PerifericaBindingSource
        '
        Me.T_PerifericaBindingSource.DataMember = "T_Periferica"
        Me.T_PerifericaBindingSource.DataSource = Me.Collaudi02DataSet
        '
        'SequenzaBindingSource
        '
        Me.SequenzaBindingSource.DataMember = "Sequenza"
        Me.SequenzaBindingSource.DataSource = Me.Collaudi02DataSet
        '
        'T_MessaggiBindingSource
        '
        Me.T_MessaggiBindingSource.DataMember = "T_Messaggi"
        Me.T_MessaggiBindingSource.DataSource = Me.Collaudi02DataSet
        '
        'T_DatiFineCollaudoBindingSource
        '
        Me.T_DatiFineCollaudoBindingSource.DataMember = "T_DatiFineCollaudo"
        Me.T_DatiFineCollaudoBindingSource.DataSource = Me.Collaudi02DataSet
        '
        'Form1
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ControlBox = False
        Me.Controls.Add(Me.FaseBar2)
        Me.Controls.Add(Me.FaseBar1)
        Me.Controls.Add(Me.MMemu)
        Me.Controls.Add(Me.SplitForm)
        Me.KeyPreview = True
        Me.MainMenuStrip = Me.MMemu
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Form1"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.SplitForm.Panel1.ResumeLayout(False)
        Me.SplitForm.Panel1.PerformLayout()
        Me.SplitForm.Panel2.ResumeLayout(False)
        Me.SplitForm.Panel2.PerformLayout()
        Me.SplitForm.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GrpOrdine.ResumeLayout(False)
        Me.GrpOrdine.PerformLayout()
        CType(Me.DOpzioni, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.MMemu.ResumeLayout(False)
        Me.MMemu.PerformLayout()
        CType(Me.Collaudi02DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SequenzaCollaudoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.T_CollaudoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.T_FasicollaudoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.T_StoricoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.T_Staz_CollBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DettaglioOrdineBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ValoriOpzioniBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CategorieOpzioniBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.T_Coll_FaseBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.T_VersioniBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.T_statisticaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.T_ImmaginiBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.T_OrdineBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OperatoriBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.T_PerifericaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SequenzaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.T_MessaggiBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.T_DatiFineCollaudoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents FaseBar1 As BarraFasi.FaseBar
    Friend WithEvents MMemu As System.Windows.Forms.MenuStrip
    Friend WithEvents UscitaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OperatoreToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SplitForm As System.Windows.Forms.SplitContainer
    Friend WithEvents LOperatore As System.Windows.Forms.Label
    Friend WithEvents lblOperatore As System.Windows.Forms.Label
    Friend WithEvents StazioneDiCollaudoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblAzienda As System.Windows.Forms.Label
    Friend WithEvents lblinea As System.Windows.Forms.Label
    Friend WithEvents lblNomeStazione As System.Windows.Forms.Label
    Friend WithEvents LAzienda As System.Windows.Forms.Label
    Friend WithEvents LNomeLinea As System.Windows.Forms.Label
    Friend WithEvents LNomeStaz As System.Windows.Forms.Label
    Friend WithEvents OrdiniToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GrpOrdine As System.Windows.Forms.GroupBox
    Friend WithEvents DOpzioni As System.Windows.Forms.DataGridView
    Friend WithEvents Catogoria As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Valore As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents LNordine As System.Windows.Forms.Label
    Friend WithEvents LMatric As System.Windows.Forms.Label
    Friend WithEvents LNPer As System.Windows.Forms.Label
    Friend WithEvents Lfw8 As System.Windows.Forms.Label
    Friend WithEvents Lfw6 As System.Windows.Forms.Label
    Friend WithEvents Lfw4 As System.Windows.Forms.Label
    Friend WithEvents Lfw3 As System.Windows.Forms.Label
    Friend WithEvents LFam As System.Windows.Forms.Label
    Friend WithEvents LNomep As System.Windows.Forms.Label
    Friend WithEvents Lfw2 As System.Windows.Forms.Label
    Friend WithEvents Lfw1 As System.Windows.Forms.Label
    Friend WithEvents LDescp As System.Windows.Forms.Label
    Friend WithEvents Lfw9 As System.Windows.Forms.Label
    Friend WithEvents Lfw7 As System.Windows.Forms.Label
    Friend WithEvents Lfw5 As System.Windows.Forms.Label
    Friend WithEvents CollaudoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FaseBar2 As BarraFasi.FaseBar
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents lblfronte As System.Windows.Forms.Label
    Friend WithEvents lblretro As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents LTentativiPossibili As System.Windows.Forms.Label
    Friend WithEvents lblTentativo As System.Windows.Forms.Label
    Friend WithEvents lblTentativi As System.Windows.Forms.Label
    Friend WithEvents LTentativo As System.Windows.Forms.Label
    Friend WithEvents LTempo As System.Windows.Forms.Label
    Friend WithEvents lblTempo As System.Windows.Forms.Label
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents LTempoTot As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents LstatisticaFasi As System.Windows.Forms.ListBox
    Friend WithEvents lblVer4 As System.Windows.Forms.Label
    Friend WithEvents lblVer3 As System.Windows.Forms.Label
    Friend WithEvents lblVer2 As System.Windows.Forms.Label
    Public WithEvents lblVer1 As System.Windows.Forms.Label
    Friend WithEvents Collaudi02DataSet As CollaudoPeriferiche.Collaudi02DataSet
    Friend WithEvents SequenzaCollaudoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SequenzaCollaudoTableAdapter As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.SequenzaCollaudoTableAdapter
    Friend WithEvents TableAdapterManager As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.TableAdapterManager
    Friend WithEvents T_CollaudoTableAdapter As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_CollaudoTableAdapter
    Friend WithEvents T_CollaudoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_FasicollaudoTableAdapter As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_FasicollaudoTableAdapter
    Friend WithEvents T_FasicollaudoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_StoricoTableAdapter As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_StoricoTableAdapter
    Friend WithEvents T_StoricoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_Staz_CollTableAdapter As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_Staz_CollTableAdapter
    Friend WithEvents T_Staz_CollBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DettaglioOrdineTableAdapter As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.DettaglioOrdineTableAdapter
    Friend WithEvents DettaglioOrdineBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ValoriOpzioniTableAdapter As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.ValoriOpzioniTableAdapter
    Friend WithEvents ValoriOpzioniBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CategorieOpzioniTableAdapter As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.CategorieOpzioniTableAdapter
    Friend WithEvents CategorieOpzioniBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_Coll_FaseTableAdapter As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_Coll_FaseTableAdapter
    Friend WithEvents T_Coll_FaseBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_VersioniBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_VersioniTableAdapter As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_VersioniTableAdapter
    Friend WithEvents T_statisticaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_statisticaTableAdapter As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_statisticaTableAdapter
    Friend WithEvents ProvaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents T_ImmaginiBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_ImmaginiTableAdapter As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_ImmaginiTableAdapter
    Friend WithEvents LIndIP As System.Windows.Forms.Label
    Friend WithEvents lblIP As System.Windows.Forms.Label
    Friend WithEvents OperatoriTableAdapter As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.OperatoriTableAdapter
    Friend WithEvents T_OrdineBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_OrdineTableAdapter As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_OrdineTableAdapter
    Friend WithEvents OperatoriBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_PerifericaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_PerifericaTableAdapter As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_PerifericaTableAdapter
    Friend WithEvents LDb As System.Windows.Forms.Label
    Friend WithEvents LDb2 As System.Windows.Forms.Label
    Friend WithEvents LColla As System.Windows.Forms.Label
    Friend WithEvents TestToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SequenzaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SequenzaTableAdapter As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.SequenzaTableAdapter
    Friend WithEvents lbltempocollaudo As System.Windows.Forms.Label
    Friend WithEvents lbllocale As System.Windows.Forms.Label
    Friend WithEvents T_MessaggiBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_MessaggiTableAdapter As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_MessaggiTableAdapter
    Friend WithEvents PiastreLS40ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CollaudoBrasileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Ls40ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Ls100ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PiastraLs515ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PiastreLs150ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents T_DatiFineCollaudoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_DatiFineCollaudoTableAdapter As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_DatiFineCollaudoTableAdapter
    Friend WithEvents Ls150ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PistreLs1007ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents LFase As System.Windows.Forms.Label
    Friend WithEvents CheckToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LettoreEScannerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Ls515SeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Ls515UVToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Ls40UVCOLOREToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Ls40GrigioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Ls40UvCOLOToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ChipCardToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TestToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem

End Class
