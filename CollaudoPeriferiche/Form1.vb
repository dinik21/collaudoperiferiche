﻿Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Threading
Imports System.Diagnostics
Imports System.Net
Imports System.Text.RegularExpressions
Imports System.IO






Public Class Form1

    Public Const LOCALDB As String = "\LocalDB"
    Public Const LOCALDBPIASTRE As String = "\LocalDB_Piastre"
    Public Const LOCALDBBRASIL As String = "\LocalDB_Brasil"
    Public Const MESSAGGI As String = "\Messaggi"
    Public Const RESULTDB As String = "\ResultDB"
    Public Const RESULTDBPIASTRA As String = "\ResultDBPIASTRE"
    Public Const DATI As String = "\Dati"
    Public Const IMAGES As String = "\Quality"
    Public Const CATEGORIEOPZIONI As String = "\CategorieOpzioni.xml"
    Public Const DETTAGLIOORDINE As String = "\DettalioOrdine.xml"
    'Public Const OPERATORI As String = "\Operatori.xml"
    Public Const SEQUENZACOLLAUDO As String = "\SequenzaCollaudo.xml"
    Public Const DATA_SALVATAGGIO As String = "\DataSalvataggio.txt"
    Public Const T_FASICOLLAUDO As String = "\T_fasicollaudo.xml"
    Public Const T_FIRMWARE As String = "\T_firmware.xml"
    Public Const T_ORDINE As String = "\T_ordine.xml"
    Public Const T_PERIFERICA As String = "\T_periferica.xml"
    Public Const T_STAZ_COLL As String = "\T_staz_coll.xml"
    Public Const T_OPERATORI As String = "\T_Operatori.xml"
    Public Const VALORIOPZIONI As String = "\Valoriopzioni.xml"
    Public Const T_VERSIONI As String = "\T_Versioni.xml"
    Public Const T_COLLAUDO As String = "\T_Collaudo-"
    Public Const T_COLL_FASE As String = "\T_Coll_Fase-"
    Public Const T_IMMAGINI As String = "\T_Immagini-"
    Public Const T_STORICO As String = "\T_Storico-"


    Public Const NUMEROFASIBARRA As Integer = 22
    'Public Const NUMEROFASIBARRA As Integer = 50
    'Public Const SOFTWARE_VERSION As String = "C.T.S. CollaudoPeriferiche.exe Ver. 5.00 Rev. 010 date 22/01/2014"
    'Public Const SOFTWARE_VERSION As String = "C.T.S. CollaudoPeriferiche.exe Ver. 6.00 Rev. 010 date 29/07/2014"
    'Public Const SOFTWARE_VERSION As String = "C.T.S. CollaudoPeriferiche.exe Ver. 7.00 Rev. 010 date 10/09/2014"
    'Public Const SOFTWARE_VERSION As String = "C.T.S. CollaudoPeriferiche.exe Ver. 8.00 Rev. 010 date 08/10/2014"
    'Public Const SOFTWARE_VERSION As String = "C.T.S. CollaudoPeriferiche.exe Ver. 9.00 Rev. 010 date 18/11/2014"
    'Public Const SOFTWARE_VERSION As String = "C.T.S. CollaudoPeriferiche.exe Ver. 10.00 Rev. 010 date 12/12/2014"
    'Public Const SOFTWARE_VERSION As String = "C.T.S. CollaudoPeriferiche.exe Ver.  11.00 date 17/12/2014"
    'Public Const SOFTWARE_VERSION As String = "C.T.S. CollaudoPeriferiche.exe Ver.  12.00 date 26/01/2015"
    'Public Const SOFTWARE_VERSION As String = "C.T.S. CollaudoPeriferiche.exe Ver.  13.00 date 06/02/2015"
    'Public Const SOFTWARE_VERSION As String = "C.T.S. CollaudoPeriferiche.exe Ver.  14.00 date 13/02/2015"
    'Public Const SOFTWARE_VERSION As String = "C.T.S. CollaudoPeriferiche.exe Ver. 15.00 date 19/02/2015"
    'Public Const SOFTWARE_VERSION As String = "C.T.S. CollaudoPeriferiche.exe Ver. 16.00 date 24/02/2015"
    'Public Const SOFTWARE_VERSION As String = "C.T.S. CollaudoPeriferiche.exe Ver. 17.00 date 13/03/2015"
    'Public Const SOFTWARE_VERSION As String = "C.T.S. CollaudoPeriferiche.exe Ver. 18.00 date 18/05/2015"
    'Public Const SOFTWARE_VERSION As String = "C.T.S. CollaudoPeriferiche.exe Ver. 19.00 date 22/06/2015"
    'Public Const SOFTWARE_VERSION As String = "C.T.S. CollaudoPeriferiche.exe Ver. 20.00 date 05/08/2015" 'SISTEMA COLLAUDO LS40COLORE BRASILE
    'Public Const SOFTWARE_VERSION As String = "C.T.S. CollaudoPeriferiche.exe Ver. 20.00 date 02/09/2015" 'VERSIONE TEMPORANEA DI COSTA , non e' stata cambiata la versione
    'Public Const SOFTWARE_VERSION As String = "C.T.S. CollaudoPeriferiche.exe Ver. 22.00 date 23/09/2015"
    'Public Const SOFTWARE_VERSION As String = "C.T.S. CollaudoPeriferiche.exe Ver. 23.00 date 08/10/2015"
    'Public Const SOFTWARE_VERSION As String = "C.T.S. CollaudoPeriferiche.exe Ver. 24.00 date 10/11/2015"
    'Public Const SOFTWARE_VERSION As String = "C.T.S. CollaudoPeriferiche.exe Ver. 25.00 date 23/02/2016"
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 26.00 TEST date 11/03/2016"
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 26.01 date 24/03/2016"
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 26.02 TEST date 21/04/2016"
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 27.00 TEST date 16/05/2016"
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 27.04 TEST date 01/06/2016" 'DWL INKDETEC su Ls100 e MYt2
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 27.05 TEST date 22/06/2016" 'modificato stampa report TOGLIENDO CTS
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 28.01 TEST date 12/07/2016" 'LS100 InkDetector
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 29.00 TEST date 19/07/2016" 'Data Matrix Ls40 Colore
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 30.00 TEST date 02/09/2016"  'Ls40UV Corretto Baco
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 31.00 TEST date 03/10/2016"  'Ls150UV
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 32.00 TEST date 26/10/2016" '
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 33.00 date 16/11/2016" '
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 34.00 date 30/01/2017 - TEST" '
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 35.00 date 20/02/2017 - TEST" '
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 36.00 date 27/03/2017 - TEST" '
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 37.00 date 07/04/2017 - TEST" '
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 38.00 date 12/04/2017 - TEST" '
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 38.01 date 16/06/2017 - TEST" '
    'Public Const SOFTWARE_VERSION As String = "ARCA Col-+laudoPeriferiche.exe Ver. 38.02 date 21/06/2017" '
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 39.00 date 22/08/2017" '
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 40.00 date 27/09/2017" '
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 41.00 date 23/10/2017" '
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 42.00 date 06/11/2017" '
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 43.00 date 01/12/2017" '
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 44.00 date 28/12/2017 - PIASTRE" '
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 45.00 date 12/03/2018" '
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 46.00 date 09/10/2018" '  
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 47.02 date 10/04/2019" '   
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 48.0.0.2 date 17/06/2019" '   
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 48.0.0.3 date 21/06/2019" '   
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 48.0.0.4 date 12/07/2019" '   
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 48.0.0.5 date 01/08/2019" '   ls40 errore - 7  , 
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 48.0.0.6 date 18/09/2019" ' connessione al server tramite nome 
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 48.0.0.7 date 08/10/2019" ' LS150 abilitazione ScanIdCardToColor su tutte le macchine
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 48.0.0.8 date 15/10/2019" ' Eliminato test card da LS40 per MyTeller
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 49.0.0.1 date xx/xx/2019" ' Semplificazione collaudo LS40
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 49.0.0.2 date 21/11/2019" ' Allineato sw con demo per la chiamata alla funzione di regolazione double leafing
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 49.0.0.3 date 19/11/2020" ' Implementato test cinghia con documento 042-04
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 49.0.0.4 date 01/06/2021" ' Implementato test chip card con sw SCM Microsystems PC/SC Diagnostic Tool
    'Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 49.0.0.5 date 01/06/2021" ' eliminato test docutest-069
    Public Const SOFTWARE_VERSION As String = "ARCA CollaudoPeriferiche.exe Ver. 49.0.0.6 date 05/07/2021" ' unificazione FW (LS40 - con e senza inkjet / LS150 - con e senza sfogliatore automatico)

    Public Const SOFTWARE_VERSION_X_REPORT As String = "0.00"
    Public Const DLL_VERSION_X_REPORT As String = "1.00"
    Public Const STRINGA_RICERCA_DLL As String = "TestFasiCollaudo*.dll"
    Public Const NUMERO_MAX_PERIF_GESTITE As Integer = 10
    Public Const STAZIONE_NON_TROVATA As Integer = -1
    Public Const PERIF_COLLEGATA As Integer = 1
    Public Const PERIF_SCOLLEGATA As Integer = 2
    Public Const COLLAUDO_PIASTRA_OK As String = "COLLAUDO PIASTRA OK!!!"
    Public Const COLLAUDO_PIASTRA_KO As String = "COLLAUDO PIASTRA KO!!!"
    Public Const SEQ_SCANNER_OK As String = "SEQ_SCANNER OK!!!"
    Public Const SEQ_SCANNER_KO As String = "SEQ_SCANNER KO!!!"
    Public Const COLLAUDO_PERIFERICA_OK As String = "COLLAUDO PERIFERICA OK!!!"
    Public Const COLLAUDO_PERIFERICA_KO As String = "COLLAUDO PERIFERICA KO!!!"
    Public Const COLLAUDO_PERIFERICA_OK_BRA As String = "COLLAUDO PERIFERICA OK!!!"
    Public Const COLLAUDO_PERIFERICA_KO_BRA As String = "COLLAUDO PERIFERICA KO!!!"
    Public fServerPresente As Boolean
    Public fSoftwareAggiornato As Boolean
    Public IdStazione As Integer
    Public RigaDatiFineCollaudo As Collaudi02DataSet.T_DatiFineCollaudoRow
    Public configIni As New INIFile(Application.StartupPath & "\ConfigLs.ini")
    Public SaveLogPath As String = configIni.ReadValue("option", "SaveFolderPath")
    Public SaveReportCollaudiPathLS150 = configIni.ReadValue("option", "SaveLogPath150")
    Public SaveReportCollaudiPathLS100 = configIni.ReadValue("option", "SaveLogPath100")
    Public SaveReportCollaudiPathLS40 = configIni.ReadValue("option", "SaveLogPath40")
    Public SaveReportCollaudiPathLS515 = configIni.ReadValue("option", "SaveLogPath515")


    Dim MessaggioFineOk As String
    Dim MessaggioFineKo As String
    Dim fstorico As Boolean
    Dim foperatore As FOperatore
    Dim fstazioni As FStazioni
    Dim fordini As FOrdini
    Dim PassiCollaudo As System.Data.DataRowCollection
    Dim FaseEseguita As Collaudi02DataSet.T_Coll_FaseRow
    Dim RigaStorico As Collaudi02DataSet.T_StoricoRow
    Dim RigaImmagini As Collaudi02DataSet.T_ImmaginiRow
    Dim RigaTestataCollaudo As Collaudi02DataSet.T_CollaudoRow
    Dim Stazione_corrente As Collaudi02DataSet.T_Staz_CollRow
    Dim Staz() As Collaudi02DataSet.T_Staz_CollRow
    Dim UltimaOperazUSB As Integer
    Dim Messag As CtsControls.Message
    Dim IndicePerifericaSelezionata As Integer
    Dim strappoggio As String
    Dim DatiFasi() As Collaudi02DataSet.T_FasicollaudoRow
    Dim StatisticaFasi() As Collaudi02DataSet.T_statisticaRow
    ' contiene le istanze degli oggetti perif di ogni dll trovata
    Dim OggettiPerif(NUMERO_MAX_PERIF_GESTITE) As Object
    ' contiene i nomi delle classi trovate
    Dim NomiPerif(NUMERO_MAX_PERIF_GESTITE) As String
    ' contiene il tipo perifxxxxx di ogni assembly
    Dim TipiPerif(NUMERO_MAX_PERIF_GESTITE) As Type
    Dim ClassePeriferica As Type
    Dim MetodiClasse() As MethodInfo
    Dim ParametriMedotoDaChiamare() As ParameterInfo
    Dim MetodoDaChiamare As MethodInfo
    Dim RitornoMetodo As Integer
    Dim parametri() As Object
    Dim TentativiPassoCorrente As Integer
    Dim iiTentativi As Integer
    Dim fNoErrore As Boolean
    Dim FCollaudo As Boolean
    Dim VFasi As ArrayList
    Dim vettorepiastra() As Integer
    Dim StrSeqPiastre As String
    Dim StrPerifericaCollegata As String
    Dim FlagCollaudoIniziato As Boolean
    Dim StopCollaudoImposto As Boolean
    Dim PrimaMatrOrdine As Integer
    Dim UltimaMatrOrdine As Integer
    Dim MatricolainCollaudo As String
    Dim SuffisspMatricola As String
    Dim PrefissoMatricola As String


    Public Class Win32
        Public Const WM_DEVICECHANGE = &H219
        Public Const DBT_DEVICEARRIVAL = &H8000
        Public Const DBT_DEVICEREMOVECOMPLETE = &H8004
        Public Const DEVICE_NOTIFY_WINDOW_HANDLE = 0
        Public Const DEVICE_NOTIFY_SERVICE_HANDLE = 1
        Public Const DBT_DEVTYP_DEVICEINTERFACE = 5
        Public Shared GUID_IO_MEDIA_ARRIVAL As Guid = New Guid("A5DCBF10-6530-11D2-901F-00C04FB951ED")

        <StructLayout(LayoutKind.Sequential)> _
        Public Class DEV_BROADCAST_DEVICEINTERFACE
            Public dbcc_size As Integer
            Public dbcc_devicetype As Integer
            Public dbcc_reserved As Integer
            Public dbcc_classguid As Guid
            Public dbcc_name As Short
        End Class
        <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Unicode)> _
    Public Class DEV_BROADCAST_DEVICEINTERFACE1
            Public dbcc_size As Integer
            Public dbcc_devicetype As Integer
            Public dbcc_reserved As Integer
            <MarshalAs(UnmanagedType.ByValArray, ArraySubType:=UnmanagedType.U1, SizeConst:=16)> _
    Public dbcc_classguid() As Byte
            <MarshalAs(UnmanagedType.ByValArray, SizeConst:=128)> _
            Public dbcc_name() As Char
        End Class
        <StructLayout(LayoutKind.Sequential)> _
        Public Class DEV_BROADCAST_HDR
            Public dbcc_size As Integer
            Public dbcc_devicetype As Integer
            Public dbcc_reserved As Integer
        End Class

        <DllImport("user32.DLL", SetLastError:=True)> Public Shared Function RegisterDeviceNotification(ByVal IntPtr As IntPtr, ByVal NotificationFilter As IntPtr, ByVal Flags As Int32) As IntPtr
        End Function

        <DllImport("kernel32.DLL")> _
        Public Shared Function _
        GetLastError() As Integer
        End Function
    End Class

    Public Function MessageboxMy(ByVal testo As String, ByVal titolo As String, ByVal bt As MessageBoxButtons, ByVal ic As MessageBoxIcon, ByVal Img As Image) As DialogResult

        Messag = New CtsControls.Message
        Messag.Text = titolo
        Messag.testo = testo
        Messag.icona = ic
        Messag.pulsanti = bt
        Messag.Imag.Image = Img
        'If BVisualZoom = True Then
        '    ff.Bzoom.Visible = True
        '    ff.Directory = DirZoom
        '    ff.FileCorrente = FileZoomCorrente
        'End If

        '27-01-2014
        Messag.StartPosition = FormStartPosition.CenterParent

        Messag.ShowDialog()
        'If ff.fine = True Then
        '    ii = Tentativi
        '    ret = False
        'End If
        '.Refresh()
        Return Messag.result

    End Function

    Private Sub ThreadTask()
        Dim NomiDaUccidere(1) As String

        'NomiDaUccidere(0) = "LS1"
        'NomiDaUccidere(1) = "LS1"
        'NomiDaUccidere(2) = "LS5"
        NomiDaUccidere(0) = "Lsw"
        NomiDaUccidere(1) = "LSW"


        Do
            Dim processiAttivi() As Process = Process.GetProcesses(Environment.MachineName)
            For Each p As Process In processiAttivi
                For ii As Integer = 0 To NomiDaUccidere.Count() - 1

                    If p.ProcessName.Contains(NomiDaUccidere(ii)) = True Then
                        'If p.MainWindowTitle.Contains(NomiDaUccidere(ii)) = True Then
                        p.Kill()
                        p.Close()
                        'MessageBox.Show("Il programma " + p.ProcessName + "Verrà terminato perchè incompatibile con il Collaudo di Periferiche CTS", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification, False)
                        Exit For
                    End If
                Next
            Next



            Thread.Sleep(1000)
            ' Thread.Sleep(1)
        Loop
    End Sub

    Public Sub RegisterHidNotification()
        Dim dbi As Win32.DEV_BROADCAST_DEVICEINTERFACE = New Win32.DEV_BROADCAST_DEVICEINTERFACE()
        Dim size As Integer
        size = Marshal.SizeOf(dbi)
        'Dim gd As Guid
        ' MsgBox(Marshal.SizeOf(gd))
        ' MsgBox(Marshal.SizeOf(New Win32.DEV_BROADCAST_DEVICEINTERFACE1))
        dbi.dbcc_size = size
        dbi.dbcc_devicetype = Win32.DBT_DEVTYP_DEVICEINTERFACE
        dbi.dbcc_reserved = 0
        dbi.dbcc_classguid = Win32.GUID_IO_MEDIA_ARRIVAL
        Dim Buffer As IntPtr
        Buffer = Marshal.AllocHGlobal(size)
        Marshal.StructureToPtr(dbi, Buffer, True)
        Dim r As IntPtr
        r = Win32.RegisterDeviceNotification(Handle, Buffer, Win32.DEVICE_NOTIFY_WINDOW_HANDLE)
        Marshal.PtrToStructure(Buffer, dbi)
        If r.ToInt32 = IntPtr.Zero.ToInt32 Then
            MessageBox.Show(Win32.GetLastError().ToString())
        End If
    End Sub

    Protected Overrides Sub WndProc(ByRef m As Message)
        If m.Msg = Win32.WM_DEVICECHANGE Then
            OnDeviceChange(m)

        End If
        MyBase.WndProc(m)
    End Sub

    Private Sub OnDeviceChange(ByVal msg As Message)
        'Dim wParam As Integer
        'wParam = msg.WParam.ToInt32()


        ''   FlagCollaudoIniziato = True

        'If wParam = Win32.DBT_DEVICEARRIVAL Then
        '    Dim o As New Win32.DEV_BROADCAST_HDR
        '    Dim b As New Win32.DEV_BROADCAST_DEVICEINTERFACE1

        '    Marshal.PtrToStructure(msg.LParam, o)
        '    If (o.dbcc_devicetype = Win32.DBT_DEVTYP_DEVICEINTERFACE) Then
        '        Dim strsize As Integer = (o.dbcc_size - 28) / 2
        '        ReDim b.dbcc_name(strsize)
        '        Marshal.PtrToStructure(msg.LParam, b)
        '        Dim str As New String(b.dbcc_name, 0, strsize)
        '        If str.Contains(StrPerifericaCollegata) Then
        '            If FlagCollaudoIniziato = True Then
        '                PictureBox3.Image = System.Drawing.Image.FromFile(Application.StartupPath() + "\Images\31.jpg")
        '                LScollegata.Visible = False
        '            End If
        '            UltimaOperazUSB = PERIF_COLLEGATA
        '        End If
        '    End If
        'ElseIf wParam = Win32.DBT_DEVICEREMOVECOMPLETE Then
        '    Dim o As New Win32.DEV_BROADCAST_HDR
        '    Dim b As New Win32.DEV_BROADCAST_DEVICEINTERFACE1

        '    Marshal.PtrToStructure(msg.LParam, o)
        '    If (o.dbcc_devicetype = Win32.DBT_DEVTYP_DEVICEINTERFACE) Then
        '        Dim strsize As Integer = (o.dbcc_size - 28) / 2
        '        ReDim b.dbcc_name(strsize)
        '        Marshal.PtrToStructure(msg.LParam, b)
        '        Dim str As New String(b.dbcc_name, 0, strsize)
        '        If str.Contains(StrPerifericaCollegata) Then
        '            PictureBox3.Image = System.Drawing.Image.FromFile(Application.StartupPath() + "\Images\29.jpg")
        '            LScollegata.Visible = False
        '            If FlagCollaudoIniziato = True Then
        '                LScollegata.Text = "Attenzione Periferica Scollegata Durante Il collaudo"
        '                LScollegata.Visible = True


        '                If MessageboxMy("La periferica è stata scollegata durante il collaudo, si consiglia di riiniziare il collaudo!!Vuoi terminare ?", "Errore", MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing) = DialogResult.Yes Then
        '                    StopCollaudoImposto = True
        '                    LScollegata.Text = "Attenzione Periferica Scollegata e ricollegata Durante Il collaudo: il collaudo verrà terminato come richesto"
        '                    LScollegata.Visible = True
        '                Else
        '                    LScollegata.Text = "Il Collaudo Sta continuando anche se la periferica è stata scollegata.... ERRORE !!!!"

        '                    Dim MessaggioMail As String
        '                    MessaggioMail = "Attenzione : sull'oridne di produzione " + LNordine.Text + " per la periferica  " + LNomep.Text + vbCrLf
        '                    MessaggioMail += "L'operatore " + LOperatore.Text + " " + LNomeStaz.Text + " " + LNomeLinea.Text + " " + LAzienda.Text + " " + LIndIP.Text + vbCrLf
        '                    MessaggioMail += " ha scollegato e ricollegato una periferica durante il collaudo e ha continuato il collaudo" + vbCrLf
        '                    MessaggioMail += "DETTAGLI :" + vbCrLf
        '                    MessaggioMail += OggettiPerif(IndicePerifericaSelezionata).MatricolaPiastra + vbCrLf
        '                    MessaggioMail += OggettiPerif(IndicePerifericaSelezionata).SerialNumberPiastra + vbCrLf
        '                    MessaggioMail += OggettiPerif(IndicePerifericaSelezionata).MatricolaPeriferica + vbCrLf

        '                    Dim Message As System.Net.Mail.MailMessage = New System.Net.Mail.MailMessage("Collaudoperiferiche@ctsgroup.it", "progsw@ctsgroup.it", "Anomalia durante il collaudo", MessaggioMail)
        '                    Dim mailClient As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient("172.16.0.1")
        '                    mailClient.Send(Message)
        '                    LScollegata.Visible = True
        '                End If
        '            End If
        '            UltimaOperazUSB = PERIF_SCOLLEGATA



        '        End If
        '    End If



        'End If
    End Sub





#Region "MENU VIDEATA"
    Private Sub UscitaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UscitaToolStripMenuItem.Click
        Dim r As Integer
        Dim fAspetta As CtsControls.FWait

        If fServerPresente = True Then
            'salvo in locale i dati di adesso
            fAspetta = New CtsControls.FWait
            fAspetta.Label1.Text = "Attendere...salvo in locale i dati del DB"
            fAspetta.Show()
            Application.DoEvents()

            SalvaDatiServer()

            SalvaDBinLocale()

            fAspetta.Hide()

            ' se sto lavorando con il server tol used a poeratore
            If foperatore.OperatoreSelezionato Is Nothing Then
            Else
                ' Tolgo il flag USED all operatore  , non piu' usata questa sezione
                foperatore.OperatoreSelezionato("Used") = 0
                r = foperatore.OperatoriTableAdapter.Update(foperatore.OperatoreSelezionato("Operatore").ToString, foperatore.OperatoreSelezionato("Password").ToString, 0, foperatore.OperatoreSelezionato("Id_Operatore"), foperatore.OperatoreSelezionato("Operatore").ToString, foperatore.OperatoreSelezionato("Password").ToString, 1)
                If r <> 1 Then
                    MessageBox.Show("Operatore non Sloggato")
                End If
                ' MessageBox.Show(r.ToString)
            End If

            fAspetta.Close()

        Else

        End If

        Me.Close()
    End Sub
    Private Sub OperatoreToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OperatoreToolStripMenuItem.Click

        FormOperatore.ShowDialog()

        If (FormOperatore.fchiusura = True) Then
            OrdiniToolStripMenuItem.Enabled = True
            LOperatore.Text = FormOperatore.NomeOperatore
        End If



        'foperatore.ShowDialog()
        '' setto a video l'operatore selezionato
        'If foperatore.OperatoreSelezionato Is Nothing Then
        'Else
        '    If foperatore.OperatoreSelezionato("Id_Operatore") = My.Settings.IdAdmin Then
        '        StazioneDiCollaudoToolStripMenuItem.Enabled = True
        '    Else
        '        StazioneDiCollaudoToolStripMenuItem.Enabled = False
        '    End If
        '    LOperatore.Text = foperatore.OperatoreSelezionato("Operatore").ToString
        '    OrdiniToolStripMenuItem.Enabled = True

        'End If
    End Sub
    Private Sub StazioneDiCollaudoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StazioneDiCollaudoToolStripMenuItem.Click
        fstazioni.ShowDialog()

        ' Setto a video nel pannelo sotto le info della stazione selezionata
        If fstazioni.StazioneSelezionata Is Nothing Then
        Else


            Dim loip() As System.Net.IPAddress = System.Net.Dns.GetHostAddresses(System.Net.Dns.GetHostName)
            Dim ip As String = ""

            For Each ind As System.Net.IPAddress In loip
                If ind.AddressFamily = Sockets.AddressFamily.InterNetwork Then
                    ip = ind.ToString()
                End If
            Next


            Dim sip As String


            sip = fstazioni.StazioneSelezionata("IndirizzoIP").ToString
            '  sip = sip.Substring(0, 15)
            If ip = sip Then

                LNomeStaz.Text = fstazioni.StazioneSelezionata("Nome_stazione").ToString
                LNomeLinea.Text = fstazioni.StazioneSelezionata("Nome_linea").ToString
                LAzienda.Text = fstazioni.StazioneSelezionata("Azienda").ToString
                LIndIP.Text = fstazioni.StazioneSelezionata("IndirizzoIP").ToString

                IdStazione = My.Settings.ID_Stazione
            Else
                IdStazione = STAZIONE_NON_TROVATA
                MessageBox.Show("Indirizzo IP Macchina ERRATO : Richiesto " + fstazioni.StazioneSelezionata("IndirizzoIP").ToString + vbCrLf + "Trovato : " + ip + vbCrLf + "CONTATTARE L' AMMINISTRATORE DI SISTEMA", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        End If
    End Sub
    Private Sub SequenzaCollaudoBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.SequenzaCollaudoBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Collaudi02DataSet)
    End Sub
    Private Sub OrdiniToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OrdiniToolStripMenuItem.Click
        fordini.ShowDialog()
        ' setto a video nel pannello inferiore l'ordine
        If fordini.OrdineSelezionato Is Nothing Then
        Else
            ' compilo a video i capi delle tabelle associate
            Dim Ricercaperiferica As String
            'Dim RicercaFW As String
            Dim RicercaOpzione As String
            Dim RicercaCateg As String
            Dim RicercaDescr As String
            'Dim PN As String

            ' Pulisco la videata
            Reset()

            GrpOrdine.Visible = True

            'Ordine 
            LNordine.Text = fordini.OrdineSelezionato("N_Ordine").ToString

            'LNPer.Text = fordini.OrdineSelezionato("NumPeriferiche").ToString
            LNPer.Visible = False
            LMatric.Text = fordini.OrdineSelezionato("Matricolainiziale").ToString
            LMatric.Visible = False

            ' PERIFERICA
            Ricercaperiferica = " ID_Periferica = " + fordini.FKPerifericaTextBox.Text
            fordini.rigaperif = fordini.Collaudi02DataSet.T_Periferica.Select(Ricercaperiferica)

            LNomep.Text = fordini.rigaperif(0)("Nome")
            LDescp.Text = fordini.rigaperif(0)("Descrizione")

            ' FIRMWARE
            LFam.Text = "Non Disponibile"
            LFam.Visible = False
            Lfw1.Text = fordini.OrdineSelezionato("Firmware1").ToString
            Lfw2.Text = fordini.OrdineSelezionato("Firmware2").ToString
            Lfw3.Text = fordini.OrdineSelezionato("Firmware3").ToString
            Lfw4.Text = fordini.OrdineSelezionato("Firmware4").ToString
            Lfw5.Text = fordini.OrdineSelezionato("Firmware5").ToString
            Lfw6.Text = fordini.OrdineSelezionato("Firmware6").ToString
            'Lfw7.Text = fordini.OrdineSelezionato("Firmware7").ToString
            'Lfw8.Text = fordini.OrdineSelezionato("Firmware8").ToString
            Lfw7.Visible = False
            Lfw8.Visible = False
            Lfw9.Text = fordini.OrdineSelezionato("Firmware9").ToString

            ' OPZIONI COLLAUDO
            RicercaOpzione = "Fkordine = " + fordini.Id_OrdineLabel2.Text
            fordini.righeopzione = fordini.Collaudi02DataSet.DettaglioOrdine.Select(RicercaOpzione)
            DOpzioni.Rows.Clear()
            Try
                For ii As Integer = 0 To fordini.righeopzione.Count - 1

                    RicercaCateg = "ID_Catogoria = " + fordini.righeopzione(ii)("FK_Categoria").ToString
                    fordini.rigaCategoria = fordini.Collaudi02DataSet.CategorieOpzioni.Select(RicercaCateg)

                    RicercaDescr = "ID_Valore = " + fordini.righeopzione(ii)("FKValoreOpzione").ToString
                    fordini.rigaValore = fordini.Collaudi02DataSet.ValoriOpzioni.Select(RicercaDescr)

                    DOpzioni.Rows.Add(fordini.rigaCategoria(0)("NomeCategoria"), fordini.rigaValore(0)("Descrizione"))
                Next
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try

            ' Abilito il menu di collaudo
            If IdStazione <> STAZIONE_NON_TROVATA Then
                CollaudoToolStripMenuItem.Enabled = True
            Else
                MessageBox.Show("Attezione stazione di collaudo NON corretta", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If


        End If

    End Sub
#End Region

#Region "FUNZIONI DI SUPPORTO"


    Public Function TestInstallazione() As Integer

        Dim str As String
        str = Application.StartupPath + LOCALDB
        If (IO.Directory.Exists(str) = False) Then
            IO.Directory.CreateDirectory(str)
        End If
        str = Application.StartupPath + RESULTDB
        If (IO.Directory.Exists(str) = False) Then
            IO.Directory.CreateDirectory(str)
        End If

        str = Application.StartupPath + RESULTDBPIASTRA
        If (IO.Directory.Exists(str) = False) Then
            IO.Directory.CreateDirectory(str)
        End If
        str = Application.StartupPath + DATI
        If (IO.Directory.Exists(str) = False) Then
            IO.Directory.CreateDirectory(str)
        End If
        str = Application.StartupPath + IMAGES
        If (IO.Directory.Exists(str) = False) Then
            IO.Directory.CreateDirectory(str)
        End If

    End Function

    Public Function CaricaDll() As Boolean
        Dim str As String
        Dim bok As Boolean
        Dim ii As Integer
        Dim Directory() As String
        Dim Assemblys(10) As Assembly
        Dim AsseblyTmp As Assembly
        Dim TypeTmp() As Type
        Dim tipo As Type
        Dim sss() As String
        Dim s As String
        Dim m() As MethodInfo

        str = Application.StartupPath

        Directory = System.IO.Directory.GetFiles(str, STRINGA_RICERCA_DLL)
        If Directory.Length = 0 Then
            MessageBox.Show("Mancano i files TestFasiCollaudo", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return False
        Else
            For Each s In Directory

                sss = (s.Substring(s.LastIndexOf("TestFasiCollaudo") + "TestFasiCollaudo".Length)).Split(".")
                ' carico temporaneamente il file per cercare l'oggett
                ' Perifxxx che devo poi usare
                bok = False
                AsseblyTmp = Assembly.LoadFrom(s)
                TypeTmp = AsseblyTmp.GetTypes()
                tipo = TypeTmp(0)
                For index As Integer = 0 To TypeTmp.Length - 1
                    tipo = TypeTmp(index)
                    If tipo.Name.Contains("Perif" + sss(0)) Then
                        bok = True
                        Exit For
                    End If
                Next


                If bok = True Then
                    NomiPerif(ii) = sss(0)
                    Assemblys(ii) = Assembly.LoadFrom(s)
                    TipiPerif(ii) = tipo
                    ' creo un'istanza dell' oggetto perif
                    OggettiPerif(ii) = Activator.CreateInstance(TipiPerif(ii))
                    ' chiamo il costruttore 

                    m = TipiPerif(ii).GetMethods()
                    MetodoDaChiamare = tipo.GetMethod("Perif" + NomiPerif(ii))
                    MetodoDaChiamare.Invoke(OggettiPerif(ii), Nothing)
                    ii += 1
                End If
            Next
            Return True
        End If
    End Function



    Public Sub ResetVideo()
        DOpzioni.Rows.Clear()
        LFam.Text = ""
        Lfw1.Text = ""
        Lfw2.Text = ""
        Lfw3.Text = ""
        Lfw4.Text = ""
        Lfw5.Text = ""
        Lfw6.Text = ""
        Lfw7.Text = ""
        Lfw8.Text = ""
        Lfw9.Text = ""
    End Sub


    Public Sub CreaBarraFasi()
        Dim strAppoggio As String
        Dim fase As BarraFasi.PhaseProgressBarItem

        Dim DatiFasi() As Collaudi02DataSet.T_FasicollaudoRow
        FaseBar1.clearPolygons()
        FaseBar2.clearPolygons()

        FaseBar1.ResetText()
        FaseBar2.ResetText()
        ' Me.Refresh()

        'FaseBar1.FDraw = True
        FaseBar2.FDraw = True
        If PassiCollaudo.Count > NUMEROFASIBARRA Then
            FaseBar1.Visible = True
            FaseBar2.Visible = True
            FaseBar2.FDraw = True
        Else
            FaseBar1.Visible = True
        End If

        For ii As Integer = 0 To PassiCollaudo.Count - 1

            strAppoggio = "Id_fase = " + PassiCollaudo(ii)("FK_FaseCollaudo").ToString
            DatiFasi = Collaudi02DataSet.T_Fasicollaudo.Select(strAppoggio)

            ' ho le fasi del collaudo completo, mi creo la sequenza corretta in base all' ordine selezionato
            strAppoggio = ""

            fase = New BarraFasi.PhaseProgressBarItem()
            fase.Text = PassiCollaudo(ii)("FK_FaseCollaudo").ToString + "-" + DatiFasi(0).Descrizione
            fase.Text = fase.Text.Replace(" ", vbCrLf)
            fase.Font = New Font(FontFamily.GenericSansSerif, 7)
            If ii < NUMEROFASIBARRA Then
                FaseBar2.AddPhase(fase)
                'FaseBar1.AddPhase(fase)
            Else
                FaseBar1.FDraw = True
                FaseBar1.AddPhase(fase)
            End If

            Me.Refresh()
        Next
    End Sub

    Public Sub CreaBarraFasiCollaudoPiastre()
        Dim strAppoggio As String
        Dim fase As BarraFasi.PhaseProgressBarItem

        Dim DatiFasi() As Collaudi02DataSet.T_FasicollaudoRow
        FaseBar1.clearPolygons()
        FaseBar2.clearPolygons()

        FaseBar1.ResetText()
        FaseBar2.ResetText()
        ' Me.Refresh()

        FaseBar1.FDraw = True
        'FaseBar2.FDraw = True
        If PassiCollaudo.Count > NUMEROFASIBARRA Then
            FaseBar1.Visible = True
            FaseBar2.Visible = True
            FaseBar2.FDraw = True
        Else
            FaseBar1.Visible = True
        End If

        For ii As Integer = 0 To PassiCollaudo.Count - 1

            strAppoggio = "Id_fase = " + PassiCollaudo(ii)("FK_FaseCollaudo").ToString
            DatiFasi = Collaudi02DataSet.T_Fasicollaudo.Select(strAppoggio)

            ' ho le fasi del collaudo completo, mi creo la sequenza corretta in base all' ordine selezionato
            strAppoggio = ""

            fase = New BarraFasi.PhaseProgressBarItem()
            fase.Text = PassiCollaudo(ii)("FK_FaseCollaudo").ToString + "-" + DatiFasi(0).Descrizione
            fase.Text = fase.Text.Replace(" ", vbCrLf)
            fase.Font = New Font(FontFamily.GenericSansSerif, 7)
            If ii < NUMEROFASIBARRA Then
                'FaseBar2.AddPhase(fase)
                FaseBar1.AddPhase(fase)
            Else
                'FaseBar1.FDraw = True
                'FaseBar1.AddPhase(fase)
                FaseBar2.FDraw = True
                FaseBar2.AddPhase(fase)
            End If

            Me.Refresh()
        Next
    End Sub

    Public Function ImpostaMatodoDaChiamare(ByVal NumFase)

        strappoggio = "Id_fase = " + PassiCollaudo(NumFase)("FK_FaseCollaudo").ToString
        DatiFasi = Collaudi02DataSet.T_Fasicollaudo.Select(strappoggio)


        MetodoDaChiamare = ClassePeriferica.GetMethod(DatiFasi(0).Nome)


        Try
            ParametriMedotoDaChiamare = MetodoDaChiamare.GetParameters()

            ' se ci sono parametri devo compilare il vettore
            If ParametriMedotoDaChiamare.Length > 0 Then
                Dim del(2) As String
                del(0) = "Param:"
                ReDim parametri(ParametriMedotoDaChiamare.Length - 1)
                Dim vettoreparametri() As String = DatiFasi(0).Note.Split(del, StringSplitOptions.RemoveEmptyEntries)
                'converto i valori nel vettore di oggetti
                ' 


                ' se la funzione che devo chiamare a parametri opzionali li gestisco
                Dim numparametriObbigatori As Short = ParametriMedotoDaChiamare.Length
                For iii As Integer = 0 To ParametriMedotoDaChiamare.Length - 1
                    If ParametriMedotoDaChiamare(iii).IsOptional = True Then
                        numparametriObbigatori = numparametriObbigatori - 1
                    End If
                Next

                If vettoreparametri.Length < numparametriObbigatori Then
                    ' errore o nel db o nella classe !!!!!
                Else
                    'For iii As Integer = 0 To ParametriMedotoDaChiamare.Length - 1
                    For iii As Integer = 0 To vettoreparametri.Length - 1
                        'If ParametriMedotoDaChiamare(iii).IsOptional = False Then
                        Try


                            Select Case ParametriMedotoDaChiamare(iii).ParameterType.Name
                                Case "Int16"
                                    parametri(iii) = CType(vettoreparametri(iii), Short)
                                Case "Boolean"
                                    parametri(iii) = CType(vettoreparametri(iii), Boolean)
                                Case "Byte"
                                    parametri(iii) = CType(vettoreparametri(iii), Byte)
                                Case "String"
                                    parametri(iii) = CType(vettoreparametri(iii), String)
                                Case "Single"
                                    parametri(iii) = CType(vettoreparametri(iii), Single)
                            End Select
                        Catch ex As Exception
                            Return False
                        End Try
                        ' End If
                        'Case 
                    Next
                End If
            End If
        Catch ex As Exception
            'MsgBox("FASE NON PRESENTE,contattare d.loggia@ctsgroup,a.costa@ctsgroup.it")
            Return False
        End Try

        Return True
    End Function


    Public Sub FineCollaudo(ByVal str As String, ByVal fok As Boolean)
        Dim f As New FineTest



        If str.Length > 30 Then
            f.LWrite.Font = New Font(f.Font.FontFamily, 18)
        End If
        f.LWrite.Text = str

        If fok = True Then
            f.LWrite.ForeColor = Color.Green
            f.BClose.ForeColor = Color.Green

        Else

            f.LWrite.ForeColor = Color.Red
            f.BClose.ForeColor = Color.Red
        End If

        f.ShowDialog()
    End Sub


    Public Sub Statistica()
        LstatisticaFasi.Items.Clear()
        For Each a As Collaudi02DataSet.T_statisticaRow In StatisticaFasi
            LstatisticaFasi.Items.Add(a("Descrizione").ToString + " : " + a("count(FK_Fase)").ToString)
        Next
    End Sub


    Public Function CaricaDatiDb() As Boolean
        Dim fret As Boolean
        fret = False
        Try
            'TODO: questa riga di codice carica i dati nella tabella 'CollaudiDataSet.T_Collaudo'. È possibile spostarla o rimuoverla se necessario.
            Me.OperatoriTableAdapter.Fill(Me.Collaudi02DataSet.Operatori)

            Me.T_VersioniTableAdapter.Fill(Me.Collaudi02DataSet.T_Versioni)
            'r = Me.T_CollaudoTableAdapter.Fill(Me.CollaudiDataSet.T_Collaudo)
            'r = Me.T_CollaudoTableAdapter.FillByIn(Me.CollaudiDataSet.T_Collaudo)
            'TODO: questa riga di codice carica i dati nella tabella 'CollaudiDataSet.ValoriOpzioni'. È possibile spostarla o rimuoverla se necessario.
            Me.ValoriOpzioniTableAdapter.Fill(Me.Collaudi02DataSet.ValoriOpzioni)
            'TODO: questa riga di codice carica i dati nella tabella 'CollaudiDataSet.DettaglioOrdine'. È possibile spostarla o rimuoverla se necessario.
            Me.DettaglioOrdineTableAdapter.Fill(Me.Collaudi02DataSet.DettaglioOrdine)
            'TODO: questa riga di codice carica i dati nella tabella 'CollaudiDataSet.CategorieOpzioni'. È possibile spostarla o rimuoverla se necessario.
            Me.CategorieOpzioniTableAdapter.Fill(Me.Collaudi02DataSet.CategorieOpzioni)

            Me.T_StoricoTableAdapter.Fill(Me.Collaudi02DataSet.T_Storico)
            'TODO: questa riga di codice carica i dati nella tabella 'CollaudiDataSet.SequenzaCollaudo'. È possibile spostarla o rimuoverla se necessario.
            Me.SequenzaCollaudoTableAdapter.Fill(Me.Collaudi02DataSet.SequenzaCollaudo)

            Me.T_Staz_CollTableAdapter.Fill(Me.Collaudi02DataSet.T_Staz_Coll)

            'TODO: questa riga di codice carica i dati nella tabella 'Collaudi02DataSet.T_Immagini'. È possibile spostarla o rimuoverla se necessario.
            Me.T_ImmaginiTableAdapter.FillBy0(Me.Collaudi02DataSet.T_Immagini)

            Me.T_FasicollaudoTableAdapter.Fill(Me.Collaudi02DataSet.T_Fasicollaudo)


            ' imposto la stazione di collaudo
            Stazione_corrente = Me.Collaudi02DataSet.T_Staz_Coll.FindById_Stazione(My.Settings.ID_Stazione)


            'PER ORA LO METTO A TRUE SENZA FARE ULTERIORI TEST  
            fret = True


        Catch ex As Exception
            ' cerco i files 
            'MessageBox.Show("Errore Connessione Server" + ex.Message, "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error)
            MessageBox.Show("Errore TABELLE CORROTTE CONTATTARE PROGETTO" + ex.Message, "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'promemoria in caso di errore
            'il caso in cui potrebbe entrare qui dentro e' quando nella tabella DettaglioOrdine ci sono incongruenze dei dati
            'es : lo stesso FK_ordine ha due parametri uguali sulla stessa categoria
            'es 1669 81 4 ->scanner zebrato + grigio
            'es 1669 12 4 ->scanner grigio
            Dim Message As System.Net.Mail.MailMessage = New System.Net.Mail.MailMessage("Collaudoperiferiche@ctsgroup.it", "d.loggia@arca.com", "Errore TABELLE CORROTTE", "Errore TABELLE CORROTTE")
            'IVREA
            Dim mailClient As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient("172.16.0.1")
            'BOLLENGO
            'Dim mailClient As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient("192.168.221.2")
            mailClient.Send(Message)

        End Try
        Return fret
    End Function

    Public Function CaricaDatiDalocale() As Boolean
        Dim xmlread As System.Data.XmlReadMode

        Dim freturn As Boolean
        Dim strmsg As String
        Dim strdata As String

        freturn = False
        strmsg = ""
        strdata = ""
        Try
            Dim f As System.IO.TextReader
            f = New System.IO.StreamReader(Application.StartupPath + LOCALDB + DATA_SALVATAGGIO)
            strdata = f.ReadLine()
            f.Close()
        Catch ex As Exception

        End Try


        If strdata <> "" Then
            xmlread = Collaudi02DataSet.T_Ordine.ReadXml(Application.StartupPath + LOCALDB + T_ORDINE)
            If Collaudi02DataSet.T_Ordine.Rows.Count > 0 Then
                xmlread = Collaudi02DataSet.T_Periferica.ReadXml(Application.StartupPath + LOCALDB + T_PERIFERICA)
                If Collaudi02DataSet.T_Periferica.Rows.Count > 0 Then
                    xmlread = Collaudi02DataSet.T_Staz_Coll.ReadXml(Application.StartupPath + LOCALDB + T_STAZ_COLL)
                    If Collaudi02DataSet.T_Staz_Coll.Rows.Count > 0 Then
                        xmlread = Collaudi02DataSet.T_Fasicollaudo.ReadXml(Application.StartupPath + LOCALDB + T_FASICOLLAUDO)
                        If Collaudi02DataSet.T_Fasicollaudo.Rows.Count > 0 Then
                            xmlread = Collaudi02DataSet.DettaglioOrdine.ReadXml(Application.StartupPath + LOCALDB + DETTAGLIOORDINE)
                            If Collaudi02DataSet.DettaglioOrdine.Rows.Count > 0 Then
                                xmlread = Collaudi02DataSet.CategorieOpzioni.ReadXml(Application.StartupPath + LOCALDB + CATEGORIEOPZIONI)
                                If Collaudi02DataSet.CategorieOpzioni.Rows.Count > 0 Then
                                    xmlread = Collaudi02DataSet.ValoriOpzioni.ReadXml(Application.StartupPath + LOCALDB + VALORIOPZIONI)
                                    If Collaudi02DataSet.ValoriOpzioni.Rows.Count > 0 Then
                                        xmlread = Collaudi02DataSet.T_Versioni.ReadXml(Application.StartupPath + LOCALDB + T_VERSIONI)
                                        If Collaudi02DataSet.T_Versioni.Rows.Count > 0 Then
                                            freturn = True
                                        Else
                                            strmsg = T_VERSIONI
                                        End If
                                    Else
                                        strmsg = VALORIOPZIONI
                                    End If
                                Else
                                    strmsg = CATEGORIEOPZIONI
                                End If
                            Else
                                strmsg = DETTAGLIOORDINE
                            End If
                        Else
                            strmsg = T_FASICOLLAUDO
                        End If
                    Else
                        strmsg = T_STAZ_COLL
                    End If
                Else
                    strmsg = T_PERIFERICA
                End If
                strmsg = T_ORDINE
            End If
        Else
            strmsg = DATA_SALVATAGGIO
        End If

        'aggiunta 08/02/2016
        Try
            xmlread = Collaudi02DataSet.Operatori.ReadXml(Application.StartupPath + LOCALDB + T_OPERATORI)
        Catch ex As Exception
            freturn = True
        End Try



        If freturn = False Then
            MessageBox.Show("Manca il file : " + strmsg, "Errore !!! ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            MessageBox.Show("Dati caricati da Locale, salvati il :" + strdata, "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
        Return freturn
    End Function

    Public Function ContaCollaudiinLoc()
        Dim files() As String
        If Not IO.Directory.Exists(Application.StartupPath + RESULTDB) Then IO.Directory.CreateDirectory(Application.StartupPath + RESULTDB)
        files = IO.Directory.GetFiles(Application.StartupPath + RESULTDB, "T_Collaudo" + "*.xml")
        Return files.Count()
    End Function
    Public Function ContaCollaudiPiastrainLoc()
        Dim files() As String
        files = IO.Directory.GetFiles(Application.StartupPath + RESULTDBPIASTRA, "T_Collaudo" + "*.xml")
        Return files.Count()
    End Function

    Public Function SalvaCollaudoLocale() As Boolean
        Dim dataNome As Date
        Dim ritorno As Boolean
        Dim nomecollaudo As String
        ritorno = False
        LColla.Text = ContaCollaudiinLoc().ToString

        dataNome = Date.Now
        ' server NON presente SALVO i Dati su file...
        nomecollaudo = Application.StartupPath + RESULTDB + T_COLLAUDO + dataNome.ToString("dd_MM-hh_mm_ss") + ".xml"
        Collaudi02DataSet.T_Collaudo.WriteXml(nomecollaudo)
        Collaudi02DataSet.T_Coll_Fase.WriteXml(Application.StartupPath + RESULTDB + T_COLL_FASE + dataNome.ToString("dd_MM-hh_mm_ss") + ".xml")
        If fNoErrore = True Then
            Collaudi02DataSet.T_Immagini.WriteXml(Application.StartupPath + RESULTDB + T_IMMAGINI + dataNome.ToString("dd_MM-hh_mm_ss") + ".xml")
            If fstorico = True Then
                Collaudi02DataSet.T_Storico.WriteXml(Application.StartupPath + RESULTDB + T_STORICO + dataNome.ToString("dd_MM-hh_mm_ss") + ".xml")
            End If
        End If
        If System.IO.File.Exists(nomecollaudo) Then
            ritorno = True
        End If
        LColla.Text = ContaCollaudiinLoc().ToString
        Return ritorno
    End Function

    Public Function SalvaCollaudoPiastraLocale() As Boolean
        Dim dataNome As Date
        Dim ritorno As Boolean
        Dim nomecollaudo As String
        ritorno = False
        LColla.Text = ContaCollaudiPiastrainLoc().ToString

        dataNome = Date.Now
        ' server NON presente SALVO i Dati su file...
        nomecollaudo = Application.StartupPath + RESULTDBPIASTRA + T_COLLAUDO + dataNome.ToString("dd_MM-hh_mm_ss") + ".xml"
        Collaudi02DataSet.T_Collaudo.WriteXml(nomecollaudo)
        Collaudi02DataSet.T_Coll_Fase.WriteXml(Application.StartupPath + RESULTDBPIASTRA + T_COLL_FASE + dataNome.ToString("dd_MM-hh_mm_ss") + ".xml")
        If fNoErrore = True Then
            Collaudi02DataSet.T_Immagini.WriteXml(Application.StartupPath + RESULTDBPIASTRA + T_IMMAGINI + dataNome.ToString("dd_MM-hh_mm_ss") + ".xml")
            If fstorico = True Then
                Collaudi02DataSet.T_Storico.WriteXml(Application.StartupPath + RESULTDBPIASTRA + T_STORICO + dataNome.ToString("dd_MM-hh_mm_ss") + ".xml")
            End If
        End If
        If System.IO.File.Exists(nomecollaudo) Then
            ritorno = True
        End If
        LColla.Text = ContaCollaudiPiastrainLoc().ToString
        Return ritorno
    End Function


    Public Function AggiornaDB() As Boolean
        Dim FDb As New faGGIdb
        Dim files() As String
        Dim freturn As Boolean
        Dim fd() As String
        Dim fi() As String
        Dim fs() As String
        Dim fdettaglio As Boolean
        Dim fstorico As Boolean
        Dim fimmagini As Boolean
        Dim fscritturaok As Boolean
        fdettaglio = fstorico = fimmagini = False
        fscritturaok = False
        Dim NomeDett As String = ""
        Dim NomeImg As String = ""
        Dim NomeStorico As String = ""
        freturn = False
        'cerco tutti i collaudi
        FDb.Show()
        FDb.TopLevel = True
        files = IO.Directory.GetFiles(Application.StartupPath + RESULTDB, "T_Collaudo" + "*.xml")
        FDb.LCol.Text = files.Count().ToString
        For Each NomeColl As String In files
            If NomeColl.Contains("T_Collaudo") Then

                Dim key As String
                Collaudi02DataSet.T_Coll_Fase.Clear()
                Collaudi02DataSet.T_Immagini.Clear()
                Collaudi02DataSet.T_Storico.Clear()
                Collaudi02DataSet.T_Collaudo.Clear()


                ' carico la testata del collaudo
                Try
                    Collaudi02DataSet.T_Collaudo.ReadXml(NomeColl)
                Catch ex As Exception
                    'c'e' stato un errore nel salvataggio dei dati per l'esecuzione in locale...
                    If (System.IO.File.Exists(NomeColl)) Then
                        'MsgBox(ex.ToString())
                        System.IO.File.Delete(NomeColl)
                        Return True
                    End If

                End Try

                FDb.LAgg.Text = NomeColl
                FDb.ProgressBar1.Value = 0
                FDb.Refresh()

                key = NomeColl.Substring(NomeColl.Length - 18, 14)
                ' cerco dettaglio
                fd = IO.Directory.GetFiles(Application.StartupPath + RESULTDB, "T_Coll_Fase*" + key + "*.xml")
                For Each NomeDett In fd
                    Collaudi02DataSet.T_Coll_Fase.ReadXml(NomeDett)
                    fdettaglio = True
                Next
                ' cerco immagini
                fi = IO.Directory.GetFiles(Application.StartupPath + RESULTDB, "T_Immagini*" + key + "*.xml")
                For Each NomeImg In fi
                    Try
                        Collaudi02DataSet.T_Immagini.ReadXml(NomeImg)
                        If (Me.Collaudi02DataSet.T_Immagini.Rows.Count() > 0) Then
                            RigaImmagini = Me.Collaudi02DataSet.T_Immagini.Rows(0)
                            fimmagini = True
                            fNoErrore = True
                        Else

                        End If
                    Catch ex As Exception

                    End Try

                Next
                ' cerco storico
                fs = IO.Directory.GetFiles(Application.StartupPath + RESULTDB, "T_Storico*" + key + "*.xml")
                For Each NomeStorico In fs
                    Collaudi02DataSet.T_Storico.ReadXml(NomeStorico)
                    If (Me.Collaudi02DataSet.T_Storico.Rows.Count() > 0) Then
                        RigaStorico = Me.Collaudi02DataSet.T_Storico.Rows(0)
                        fstorico = True
                    Else
                    End If
                Next

                Try
                    Dim r As Integer
                    Dim Nuovo_Id_Collaudo As Integer

                    ' scrivo i dati della testata del collaudo
                    RigaTestataCollaudo = Me.Collaudi02DataSet.T_Collaudo.Rows(0)
                    Dim CollaudoEsistente = Me.T_CollaudoTableAdapter.CheckCollaudo(RigaTestataCollaudo("Data"), RigaTestataCollaudo("Durata"), RigaTestataCollaudo("FKOperatore"), RigaTestataCollaudo("FK_Ordine"), RigaTestataCollaudo("Ora"))


                    FDb.ProgressBar1.Value += 50
                    sleep(500)


                    If CollaudoEsistente Is Nothing Then
                        ' se il collaudo non esiste già allora lo scrivo
                        r = Me.T_CollaudoTableAdapter.Update(Collaudi02DataSet.T_Collaudo)

                        If (r = Collaudi02DataSet.T_Collaudo.Count()) Then
                            'se la scrittura è andata a buon fine chiedo l'id inserito e lo cambio in locale
                            Nuovo_Id_Collaudo = Me.T_CollaudoTableAdapter.UltimoIser(RigaTestataCollaudo("Data"), RigaTestataCollaudo("Durata"), RigaTestataCollaudo("FK_Stazione"), RigaTestataCollaudo("FK_Ordine"), RigaTestataCollaudo("FKOperatore"))
                            If Nuovo_Id_Collaudo < 0 Then
                            Else

                                ' aggiorno le righe in locale
                                Collaudi02DataSet.T_Collaudo.Rows(0)("Id_Collaudo") = Nuovo_Id_Collaudo
                                For ii = 0 To Collaudi02DataSet.T_Coll_Fase.Rows.Count - 1
                                    Collaudi02DataSet.T_Coll_Fase(ii)("FK_collaudo") = Nuovo_Id_Collaudo
                                Next

                                If RigaStorico Is Nothing Then
                                Else
                                    RigaStorico("FK_Collaudo") = Collaudi02DataSet.T_Collaudo.Rows(0)("Id_Collaudo")
                                End If

                                If RigaImmagini Is Nothing Then
                                Else
                                    RigaImmagini("FK_Collaudo") = RigaStorico("FK_Collaudo")
                                End If

                                ' scrivo la riga con le immagini


                                ' scrivo le righe del collaudo sul Collaudi02DataSet
                                r = Me.T_Coll_FaseTableAdapter.Update(Collaudi02DataSet.T_Coll_Fase)
                                If r = Collaudi02DataSet.T_Coll_Fase.Count() Then
                                    fscritturaok = True
                                    If fNoErrore = True Then
                                        If (Me.T_ImmaginiTableAdapter.Update(Collaudi02DataSet.T_Immagini) = 1) Then
                                        Else
                                            Me.T_Coll_FaseTableAdapter.DeleteQuery(Nuovo_Id_Collaudo)
                                            Me.T_CollaudoTableAdapter.DeleteQuery(Nuovo_Id_Collaudo)
                                            fscritturaok = False
                                        End If
                                    End If
                                    If fstorico = True Then
                                        r = Me.T_StoricoTableAdapter.Update(Collaudi02DataSet.T_Storico)
                                        If (r = Collaudi02DataSet.T_Storico.Count()) Then
                                        Else
                                            ' scrittura storico non a buon fine 
                                            ' devo cancellare le righe e l'intestazione del collaudo
                                            Me.T_ImmaginiTableAdapter.DeleteQuery(Nuovo_Id_Collaudo)
                                            Me.T_Coll_FaseTableAdapter.DeleteQuery(Nuovo_Id_Collaudo)
                                            Me.T_CollaudoTableAdapter.DeleteQuery(Nuovo_Id_Collaudo)
                                            fscritturaok = False
                                        End If
                                    End If
                                    ' ho scirtto il collaudo sul DB ora lo cancella da file
                                    If fscritturaok Then
                                        If fNoErrore Then
                                            If NomeImg Is Nothing Then
                                            Else
                                                System.IO.File.Delete(NomeImg)
                                            End If
                                            fNoErrore = False
                                        End If
                                        If fstorico Then
                                            If NomeStorico Is Nothing Then
                                            Else
                                                System.IO.File.Delete(NomeStorico)
                                            End If
                                            fstorico = False
                                        End If

                                        If NomeDett Is Nothing Then
                                        Else
                                            System.IO.File.Delete(NomeDett)
                                        End If

                                        If NomeColl Is Nothing Then
                                        Else
                                            System.IO.File.Delete(NomeColl)
                                        End If
                                    Else
                                        ' non cancello nessun file, ma lascio li il file
                                    End If

                                    freturn = True
                                Else
                                    ' non ho scritto la riga di immagii
                                    ' cancello solamente l'intestazione del collaudo
                                    Me.T_CollaudoTableAdapter.DeleteQuery(Nuovo_Id_Collaudo)
                                End If
                            End If

                        Else
                            ' la scrittura intestazione collaudo non è andata bene 
                        End If
                    Else
                        If NomeImg Is Nothing Then
                        Else
                            If (System.IO.File.Exists(NomeImg)) Then
                                System.IO.File.Delete(NomeImg)
                            End If
                        End If

                        If NomeStorico Is Nothing Then
                        Else
                            If (System.IO.File.Exists(NomeStorico)) Then
                                System.IO.File.Delete(NomeStorico)
                            End If
                        End If
                        If NomeDett Is Nothing Then
                        Else
                            If (System.IO.File.Exists(NomeDett)) Then
                                System.IO.File.Delete(NomeDett)
                            End If
                        End If
                        If NomeColl Is Nothing Then
                        Else
                            If (System.IO.File.Exists(NomeColl)) Then
                                System.IO.File.Delete(NomeColl)
                            End If
                        End If
                    End If
                Catch ex As Exception
                    'If SalvaCollaudoLocale() = True Then
                    MessageBox.Show("Connessione di Rete Persa!!! Dati Salvati in Locale", "Errore !!! Riavviare il Programma !!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    'c'e' stato un errore nel salvataggio dei dati per l'esecuzione in locale...
                    If (System.IO.File.Exists(NomeColl)) Then
                        'MsgBox(ex.ToString())
                        System.IO.File.Delete(NomeColl)
                        Return True
                    End If

                    'Me.Close()
                End Try
            Else
                MessageBox.Show(NomeColl)
            End If
            FDb.ProgressBar1.Value += 50
            sleep(500)
        Next
        FDb.Close()
        Return freturn
    End Function
    Public Sub sleep(ByVal Milli As Integer)
        Dim t As New DateTime(DateTime.Now.Ticks)
        Dim ts As TimeSpan = New TimeSpan
        ts = t - DateTime.Now
        While (ts.Milliseconds < Milli)
            ts = DateTime.Now - t
        End While
    End Sub
#End Region

#Region "MESSAGGI VIDEATA"
    Private Sub Form1_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        My.Settings.Save()
        FaseBar1.Dispose()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: questa riga di codice carica i dati nella tabella 'Collaudi02DataSet.T_DatiFineCollaudo'. È possibile spostarla o rimuoverla se necessario.

        'TODO: questa riga di codice carica i dati nella tabella 'Collaudi02DataSet.T_Messaggi'. È possibile spostarla o rimuoverla se necessario.

        'TODO: questa riga di codice carica i dati nella tabella 'Collaudi02DataSet.Sequenza'. È possibile spostarla o rimuoverla se necessario.
        '   Me.SequenzaTableAdapter.Fill(Me.Collaudi02DataSet.Sequenza)

        'Dim r As Integer
        Dim freturn As Boolean
        Dim ip As String = ""


        ' Stringa fittizia per riconoscere se la periferica viene scollegata durante il collaudo.
        StrPerifericaCollegata = "CTS"
        UltimaOperazUSB = PERIF_SCOLLEGATA

        FlagCollaudoIniziato = False
        'System.IO.File.Copy("\\progps\software-exe\Sw_Per_Produzione\Collaudo_Periferiche_Ls100\ls100.dll", "provacopia.dll", True)

        ' carico subito i dati di MT setting...
        LColla.Text = ContaCollaudiinLoc().ToString
        My.Settings.Reload()

        fServerPresente = False
        'UsbCtsMon1.Monitor = False
        'UsbCtsMon1.UserInterface = False
        'UsbCtsMon1.VendorId = "1136"
        'UsbCtsMon1.ProductId = "3232"
        'UsbCtsMon1.Visible = False


        TestInstallazione()
        If CaricaDll() = True Then

            fServerPresente = testServer()
            fSoftwareAggiornato = True
            'Me.Icon = My.Resources.LOGO_ARCA
            Me.Refresh()

            If fServerPresente = True AndAlso CaricaDatiDb() = True Then



                LDb.ForeColor = Color.Green
                LDb2.ForeColor = Color.Green
                LDb.Text = "Server Connesso"
                LDb2.Text = LDb.Text

                ' ho caricato i dati ok
                'update db
                AggiornaDB()
            Else
                ' vado a caricare i dati in locale


                LDb.ForeColor = Color.Red
                LDb2.ForeColor = Color.Red
                LDb.Text = "Server Sconnesso"
                LDb2.Text = LDb.Text
                freturn = CaricaDatiDalocale()
                If freturn = False Then
                    MessageBox.Show("Dati in locale  non corretti...", "ERRORE GRAVE", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Me.Close()
                    Return
                End If

            End If
            VFasi = New ArrayList()
        Else
            ' non ho caricato le dll
            Me.Close()
            Return
        End If
        SplitForm.Panel2Collapsed = False
        SplitForm.Panel1Collapsed = True

        foperatore = New FOperatore
        fstazioni = New FStazioni
        fordini = New FOrdini


        If fServerPresente = False Then
            ' non c'è il server cerco la stazione ????
            IdStazione = My.Settings.ID_Stazione
            Staz = Collaudi02DataSet.T_Staz_Coll.Select("ID_Stazione = " + IdStazione.ToString)

            If Staz.Count <= 0 Then
            Else

                Stazione_corrente = Staz(0)
                If Stazione_corrente Is Nothing Then
                    IdStazione = STAZIONE_NON_TROVATA
                    MessageBox.Show("Indirizzo IP Macchina NON TROVATO " + vbCrLf + "CONTATTARE L' AMMINISTRATORE DI SISTEMA", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Else


                    LNomeStaz.Text = Stazione_corrente.Nome_stazione
                    LNomeLinea.Text = Stazione_corrente.Nome_linea
                    LAzienda.Text = Stazione_corrente.Azienda
                    LIndIP.Text = Stazione_corrente.IndirizzoIP
                End If
            End If

        Else
            IdStazione = My.Settings.ID_Stazione
            '' il server è presente
            'If Stazione_corrente Is Nothing Then
            '    IdStazione = STAZIONE_NON_TROVATA
            '    MessageBox.Show("Indirizzo IP Macchina NON TROVATO " + vbCrLf + "CONTATTARE L' AMMINISTRATORE DI SISTEMA", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Else
            Dim loip() As System.Net.IPAddress = System.Net.Dns.GetHostAddresses(System.Net.Dns.GetHostName)



            For Each ind As System.Net.IPAddress In loip
                If ind.AddressFamily = Sockets.AddressFamily.InterNetwork Then
                    ip = ind.ToString()
                End If
            Next



            ' Ale
            'Staz = Collaudi02DataSet.T_Staz_Coll.Select("IndirizzoIP = " + ip)
            'Stazione_corrente = Staz(0)

            'If ip = Stazione_corrente.IndirizzoIP Then

            LNomeStaz.Text = System.Environment.MachineName  'Stazione_corrente.Nome_stazione
            LNomeLinea.Text = "ARCA Production"  'Stazione_corrente.Nome_linea"
            'LAzienda.Text = "CTS Electronics S.P.A."    'Stazione_corrente.Azienda
            LAzienda.Text = "Arca Technologies S.r.l."    'Stazione_corrente.Azienda
            LIndIP.Text = ip.ToString() 'Stazione_corrente.IndirizzoIP
            IdStazione = My.Settings.ID_Stazione

            'Me.Icon = My.Resources.LOGO_ARCA
            '    Else
            '    IdStazione = STAZIONE_NON_TROVATA
            '    MessageBox.Show("Indirizzo IP Macchina ERRATO : Richiesto " + Stazione_corrente.IndirizzoIP + vbCrLf + "Trovato : " + ip + vbCrLf + "CONTATTARE L' AMMINISTRATORE DI SISTEMA", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'End If
            'End If


        End If
        LColla.Text = ContaCollaudiinLoc().ToString


        'Me.Text = SOFTWARE_VERSION + "  IndirizzoIP:  " + ip.ToString() + "  Id TeamViewer:  " + System.Environment.MachineName.ToString()
        Me.Text = SOFTWARE_VERSION + "  Id TeamViewer:  " + System.Environment.MachineName.ToString()
        Me.Icon = My.Resources.LOGO_ARCA



    End Sub

    Private Sub Form1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Select Case (e.KeyValue)
            ' 9 per info sui collaudi
            Case Keys.NumPad9
                If SplitForm.Panel1Collapsed = True Then
                    SplitForm.Panel2Collapsed = True
                Else
                    SplitForm.Panel1Collapsed = True
                End If
            Case Keys.NumPad2
            Case Keys.Escape
        End Select
    End Sub

    Public Sub SvuotaDir(ByVal cartella As String)
        Dim di As IO.DirectoryInfo = _
                New IO.DirectoryInfo(cartella)
        For Each oFile As IO.FileInfo In di.GetFiles()
            oFile.Delete()
        Next
    End Sub

    Private Sub CollaudoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CollaudoToolStripMenuItem.Click
        Dim TempoPasso As TimeSpan
        Dim Tempocollaudo As TimeSpan
        Dim DataIniPasso As DateTime
        Dim DataIni As DateTime
        Dim DataFinale As DateTime


        Dim ii As Integer


        Dim FasEseApp() As Collaudi02DataSet.T_Coll_FaseRow
        Dim FaseErrore() As Collaudi02DataSet.T_statisticaRow

        ' riga con le versioni di SW per la periferica che è selezionata nell' ordine 
        Dim VersioniRiga() As Collaudi02DataSet.T_VersioniRow

        ' creo la nuova riga di testata collaudo
        RigaTestataCollaudo = Collaudi02DataSet.T_Collaudo.NewRow()

        ' contiene lo storico della periferica prima che venga cancellato.
        RigaStorico = Collaudi02DataSet.T_Storico.NewRow()
        RigaImmagini = Collaudi02DataSet.T_Immagini.NewRow()



        If LOperatore.Text <> "PROGETTO" Then
            'trd = New Thread(AddressOf ThreadTask)
            'trd.IsBackground = True
            'trd.Start()
        End If

        FCollaudo = True
        StopCollaudoImposto = False

        ' cerco la dll TestFasicollaudoXxxxx.dll da utilizzare rispetto all'
        'ordine selezionato
        For ii = 0 To NUMERO_MAX_PERIF_GESTITE
            If NomiPerif(ii) Is Nothing Then
            Else
                If LNomep.Text = NomiPerif(ii) Then
                    IndicePerifericaSelezionata = ii
                End If
            End If
        Next

        ' Controllo che la dll corretta sia stata caricata
        If Not (OggettiPerif(IndicePerifericaSelezionata) Is Nothing) Then
            TempoPasso = New TimeSpan
            Tempocollaudo = New TimeSpan

            ' inizializzo l'oggetto da usare
            OggettiPerif(IndicePerifericaSelezionata).formpadre = Me

            ClassePeriferica = OggettiPerif(IndicePerifericaSelezionata).GetType

            MetodiClasse = ClassePeriferica.GetMethods()
            '  StatisticaFasi = CollaudiDataSet.T_Coll_Fase

            OggettiPerif(IndicePerifericaSelezionata).Pimage = PictureBox1
            OggettiPerif(IndicePerifericaSelezionata).PImageBack = PictureBox2

            ' prendo i dati dal Database e pulisco i dati dal collaudo precedente
            Me.Collaudi02DataSet.T_Coll_Fase.Clear()
            Me.Collaudi02DataSet.T_Collaudo.Clear()
            Me.Collaudi02DataSet.T_Storico.Clear()
            Me.Collaudi02DataSet.T_statistica.Clear()
            Me.Collaudi02DataSet.T_Immagini.Clear()
            Me.Collaudi02DataSet.SequenzaCollaudo.Clear()





            ' creo la sequenza di collaudo corretta
            If fServerPresente = True Then
                'Creo la tabella delle statistiche
                Try
                    ' messe per prova per scrivere il tipo di sequenza.....
                    'Dim RigaTipoSequenza As Collaudi02DataSet.SequenzaRow
                    'Me.SequenzaTableAdapter.FillByOperatore(Me.Collaudi02DataSet.Sequenza, foperatore.OperatoreSelezionato("Id_Operatore"))
                    'RigaTipoSequenza = Me.Collaudi02DataSet.Sequenza.Rows(0)
                    'LTipoCollaudo.Text = "Eseguo : " + RigaTipoSequenza("Descrizione")
                    Me.T_DatiFineCollaudoTableAdapter.FillBymax(Me.Collaudi02DataSet.T_DatiFineCollaudo)

                    Me.T_statisticaTableAdapter.Fill(Me.Collaudi02DataSet.T_statistica, IdStazione, fordini.OrdineSelezionato("Id_Ordine"), Date.Today)

                    Me.SequenzaCollaudoTableAdapter.FillByp1p2(Me.Collaudi02DataSet.SequenzaCollaudo, fordini.OrdineSelezionato("FKPeriferica"), fordini.OrdineSelezionato("Id_Ordine"))

                    Me.T_MessaggiTableAdapter.FillByPerif(Me.Collaudi02DataSet.T_Messaggi, fordini.OrdineSelezionato("FKPeriferica"))

                    Me.Collaudi02DataSet.T_Messaggi.WriteXml(Application.StartupPath + LOCALDB + MESSAGGI + fordini.OrdineSelezionato("FKPeriferica").ToString + ".xml")

                Catch ex As Exception
                    MessageBox.Show("Attenzione Connessione di Rete non Presente !!! RIAVVIARE IL PROGRAMMA", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Return
                End Try

            Else

                'Me.Collaudi02DataSet.SequenzaCollaudo.ReadXml(Application.StartupPath + LOCALDB + "\Seq_" + fordini.OrdineSelezionato("N_Ordine") + ".xml")
                Me.Collaudi02DataSet.SequenzaCollaudo.ReadXml(Application.StartupPath + LOCALDB + "\Seq_" + fordini.OrdineSelezionato("Id_Ordine").ToString() + ".xml")
            End If


            StatisticaFasi = Collaudi02DataSet.T_statistica.Select()


            ' imposto altre proprietà per il collaudo
            OggettiPerif(IndicePerifericaSelezionata).Ver_firm1 = Lfw1.Text
            OggettiPerif(IndicePerifericaSelezionata).Ver_firm2 = Lfw2.Text
            OggettiPerif(IndicePerifericaSelezionata).Ver_firm3 = Lfw3.Text
            OggettiPerif(IndicePerifericaSelezionata).Ver_firm4 = Lfw4.Text
            OggettiPerif(IndicePerifericaSelezionata).Ver_firm5 = Lfw5.Text
            'OggettiPerif(IndicePerifericaSelezionata).Ver_firm6 = Lfw6.Text
            'OggettiPerif(IndicePerifericaSelezionata).Ver_firm7 = Lfw7.Text
            'OggettiPerif(IndicePerifericaSelezionata).Ver_firm8 = Lfw8.Text
            OggettiPerif(IndicePerifericaSelezionata).Ver_firm9 = Lfw9.Text

            ' CONTROLLO DELLE VIRSIONI SOFTWARE AGGIORNATE

            VersioniRiga = Collaudi02DataSet.T_Versioni.Select("Periferica = " + fordini.OrdineSelezionato("FKPeriferica").ToString())
            ' visualizza la versione di SW corrente
            'Label25.Text = OggettiPerif(IndicePerifericaSelezionata).VersioneDll_0
            'Label26.Text = OggettiPerif(IndicePerifericaSelezionata).VersioneDll_1
            'Label27.Text = OggettiPerif(IndicePerifericaSelezionata).VersioneTest
            lblVer1.Visible = False
            lblVer2.Visible = False
            lblVer3.Visible = False
            lblVer4.Text = SOFTWARE_VERSION


            'StrPerifericaCollegata = OggettiPerif(IndicePerifericaSelezionata).StrUsb
            'UsbCtsMon1.Visible = True
            'UsbCtsMon1.ProductId = StrPerifericaCollegata
            'UsbCtsMon1.UserInterface = True
            'UsbCtsMon1.Monitor = True
            'UsbCtsMon1.GetStatus()


            '19-12-2014 commentato 
            'If (OggettiPerif(IndicePerifericaSelezionata).VersioneDll_0 <> VersioniRiga(0)("VersioneDll_0")) Then
            '    Dim strmsg As String
            '    strmsg = "Richiesta :" + vbCrLf + VersioniRiga(0)("VersioneDll_0") + vbCrLf + "Presente :" + vbCrLf + OggettiPerif(IndicePerifericaSelezionata).VersioneDll_0
            '    If MessageBox.Show(strmsg, "Attenzione !! Software NON Aggiornato Continuare ?", MessageBoxButtons.YesNo, MessageBoxIcon.Error) = Windows.Forms.DialogResult.No Then
            '        ''uscire da preogramma
            '        fSoftwareAggiornato = False
            '    End If
            'Else
            '    fSoftwareAggiornato = True
            'End If
            'FINE DEL CONTROLLO DELLE VIRSIONI SOFTWARE AGGIORNATE

            ' Cancello le directory con i dati e le ricreo
            Dim sss As String
            'Dim ind As Integer
            'Dim zzz As String
            sss = Application.StartupPath()
            'ind = sss.LastIndexOf("\")
            'zzz = sss.Substring(0, ind)
            'zzz = "c:"



            If PictureBox1.Image Is Nothing Then
            Else
                PictureBox1.Image.Dispose()
                PictureBox1.Image = Nothing
            End If
            If PictureBox2.Image Is Nothing Then
            Else
                PictureBox2.Image.Dispose()
                PictureBox2.Image = Nothing
            End If



            'le Directory non vengono create sul Destkop , problemi di permessi
            Try
                If IO.Directory.Exists(sss + "\Dati") Then
                    'IO.Directory.Delete(sss + "\Dati", True)
                    SvuotaDir(sss + "\Dati")
                Else
                    IO.Directory.CreateDirectory(sss + "\Dati")
                End If
            Catch ex As Exception
                MessageBox.Show("Directory Dati NON creata....", ex.Message + " ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                IO.Directory.Delete(sss + "\Dati", True)
                Me.Close()
            End Try

            Try

                If IO.Directory.Exists(sss + "\Quality") Then
                    SvuotaDir(sss + "\Quality")
                    'IO.Directory.Delete(sss + "\Quality", True)
                    Console.WriteLine("Cartella Quality gia' presente")
                Else
                    IO.Directory.CreateDirectory(sss + "\Quality")
                End If

            Catch ex As Exception
                MessageBox.Show("Directory Quality NON creata.....Chiudere e Riaprire il programma", ex.Message + " ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                IO.Directory.Delete(sss + "\Quality", True)
                Me.Close()
            End Try

            Try

                If IO.Directory.Exists(sss + "\Images_Scanner") Then
                    Console.WriteLine("Cartella Images_Scanner gia' presente")
                Else
                    IO.Directory.CreateDirectory(sss + "\Images_Scanner")
                End If

                If IO.Directory.Exists(sss + "\Images_Scanner\old") Then
                    'SvuotaDir(sss + "\Quality")
                    'IO.Directory.Delete(sss + "\Quality", True)
                    Console.WriteLine("Cartella Images_Scanner\old gia' presente")
                Else
                    IO.Directory.CreateDirectory(sss + "\Images_Scanner\old")
                End If

                If IO.Directory.Exists(sss + "\Images_Scanner\Log") Then
                    Console.WriteLine("Cartella Images_Scanner\Log gia' presente")
                Else
                    IO.Directory.CreateDirectory(sss + "\Images_Scanner\Log")
                End If

            Catch ex As Exception
                MessageBox.Show("Directory Images_Scanner NON creata.....Chiudere e Riaprire il programma", ex.Message + " ", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Me.Close()
            End Try

            Try
                If IO.Directory.Exists(sss + "\Images_Fascioni") Then
                    'IO.Directory.Delete(sss + "\Dati", True)
                    SvuotaDir(sss + "\Images_Fascioni")
                Else
                    IO.Directory.CreateDirectory(sss + "\Images_Fascioni")
                End If
            Catch ex As Exception
                MessageBox.Show("Directory Images_Fascioni NON creata....", ex.Message + " ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

            Try
                If IO.Directory.Exists(sss + "\Report") Then
                    'IO.Directory.Delete(sss + "\Dati", True)
                    'SvuotaDir(sss + "\Images_Fascioni")
                Else
                    IO.Directory.CreateDirectory(sss + "\Report")
                End If
            Catch ex As Exception
                MessageBox.Show("Directory Report NON creata....", ex.Message + " ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

            Try
                If IO.Directory.Exists(sss + "\Report150") Then
                Else
                    IO.Directory.CreateDirectory(sss + "\Report150")
                End If
            Catch ex As Exception
                MessageBox.Show("Directory Report150 NON creata....", ex.Message + " ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

            Try
                If IO.Directory.Exists(sss + "\Report40") Then
                Else
                    IO.Directory.CreateDirectory(sss + "\Report40")
                End If
            Catch ex As Exception
                MessageBox.Show("Directory Report40 NON creata....", ex.Message + " ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

            Try
                If IO.Directory.Exists(sss + "\Report100") Then
                Else
                    IO.Directory.CreateDirectory(sss + "\Report100")
                End If
            Catch ex As Exception
                MessageBox.Show("Directory Report100 NON creata....", ex.Message + " ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

            Try
                If IO.Directory.Exists(sss + "\Report515") Then
                Else
                    IO.Directory.CreateDirectory(sss + "\Report515")
                End If
            Catch ex As Exception
                MessageBox.Show("Directory Report515 NON creata....", ex.Message + " ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

            ' se il SW è Aggiornato o l'utente vuole andare avanti ugualmente
            If fSoftwareAggiornato = True Then
                SplitForm.Panel2Collapsed = True
                SplitForm.Panel1Collapsed = False

                'Dim FileOLD As String
                'FileOLD = Application.StartupPath() & "\quality\old\"
                'Dim File1 As String
                'File1 = Application.StartupPath() & "\quality\"

                SalvaDatiServer()


                MetodoDaChiamare = TipiPerif(IndicePerifericaSelezionata).GetMethod("ImpostaFlagPerConfigurazione")
                Dim args() As Object = {fordini.VettValoriOpzioni}

                RitornoMetodo = MetodoDaChiamare.Invoke(OggettiPerif(IndicePerifericaSelezionata), args)

                Statistica()

                PassiCollaudo = Collaudi02DataSet.SequenzaCollaudo.Rows


                RigaTestataCollaudo("Data") = DateTime.Now
                Dim StrSoftwareToDb As String
                StrSoftwareToDb = "Richiesta:" + OggettiPerif(IndicePerifericaSelezionata).VersioneDll_0 + vbCrLf + " Trovato: " + VersioniRiga(0)("VersioneDll_0") + vbCrLf
                StrSoftwareToDb += "Richiesta:" + OggettiPerif(IndicePerifericaSelezionata).VersioneDll_1 + vbCrLf + " Trovato: " + VersioniRiga(0)("VersioneDll_1") + vbCrLf
                StrSoftwareToDb += "Richiesta:" + OggettiPerif(IndicePerifericaSelezionata).VersioneTest + vbCrLf + " Trovato: " + VersioniRiga(0)("VersioneTest") + vbCrLf
                StrSoftwareToDb += "Richiesta:" + SOFTWARE_VERSION + vbCrLf + " Trovato: " + VersioniRiga(0)("VersioneApplicativo") + vbCrLf

                'RigaTestataCollaudo("Software") = StrSoftwareToDb
                RigaTestataCollaudo("Software") = ""

                'RigaTestataCollaudo("FK_Stazione") = IdStazione
                RigaTestataCollaudo("FK_Stazione") = FormOperatore.IdNomeStazione

                'OggettiPerif(IndicePerifericaSelezionata).FileTraccia.Operatore = LOperatore.Text
                OggettiPerif(IndicePerifericaSelezionata).FileTraccia.Operatore = FormOperatore.NomeOperatore
                OggettiPerif(IndicePerifericaSelezionata).FileTraccia.NomeStazione = FormOperatore.NomeStazione

                RigaTestataCollaudo("FK_Ordine") = fordini.OrdineSelezionato("Id_Ordine")
                Try
                    'RigaTestataCollaudo("FKOperatore") = foperatore.OperatoreSelezionato("Id_Operatore")
                    RigaTestataCollaudo("FKOperatore") = FormOperatore.IdNomeOperatore
                Catch ex As Exception
                    RigaTestataCollaudo("FKOperatore") = 29
                End Try


                RigaTestataCollaudo("Ora") = DateTime.Now()

                Collaudi02DataSet.T_Collaudo.Rows.Add(RigaTestataCollaudo)

                ' imposto gli oggetti per i campi della stampa del report
                OggettiPerif(IndicePerifericaSelezionata).FileTraccia.Matricola = ""
                OggettiPerif(IndicePerifericaSelezionata).FileTraccia.VersioneDll = ""

                OggettiPerif(IndicePerifericaSelezionata).FileTraccia.Periferica = LNomep.Text
                OggettiPerif(IndicePerifericaSelezionata).FileTraccia.Ordine_prod = LNordine.Text.Trim()

                OggettiPerif(IndicePerifericaSelezionata).FileTraccia.VersioneSw = SOFTWARE_VERSION_X_REPORT
                'OggettiPerif(IndicePerifericaSelezionata).FileTraccia.VersioneDll = DLL_VERSION_X_REPORT
                OggettiPerif(IndicePerifericaSelezionata).FileTraccia.LineaProd = LNomeLinea.Text
                OggettiPerif(IndicePerifericaSelezionata).FileTraccia.Manufacter = LAzienda.Text
                OggettiPerif(IndicePerifericaSelezionata).FileTraccia.DataCollaudo = RigaTestataCollaudo("Data")

                'OggettiPerif(IndicePerifericaSelezionata).FileTraccia.Durata_test = "pippo"

                ' crea la barra delle fasi
                CreaBarraFasi()

                ' metto la data di inizio collaudo che andrò a scrivere nel BD
                DataIni = DateTime.Now()

                FasEseApp = Collaudi02DataSet.T_Coll_Fase.Select("Id_Passo = Max(Id_Passo)")

                ' Eseguo il collaudo
                ii = 0
                FlagCollaudoIniziato = True

                ' NON PIU UTILIZZATE
                'If UltimaOperazUSB = PERIF_COLLEGATA Then
                '    PictureBox3.Image = System.Drawing.Image.FromFile(Application.StartupPath + "/Images/31.jpg")
                'End If
                'If UltimaOperazUSB = PERIF_SCOLLEGATA Then
                '    PictureBox3.Image = System.Drawing.Image.FromFile(Application.StartupPath + "/Images/29.jpg")
                'End If

                ' ciclo fino a quando sono arrivato a ok o a ko
                Dim faseapp As BarraFasi.PhaseProgressBarItem
                faseapp = New BarraFasi.PhaseProgressBarItem()
                Dim result As Boolean
                Do While (ii < PassiCollaudo.Count And FCollaudo = True)

                    result = ImpostaMatodoDaChiamare(ii)
                    If (result = True) Then

                        ' attivo la porzione di barra corrente
                        'If (ii < NUMEROFASIBARRA) Then
                        '    OggettiPerif(IndicePerifericaSelezionata).fase = FaseBar1.Phases(ii + 1)
                        '    faseapp = FaseBar1.Phases(ii + 1)

                        'Else
                        '    OggettiPerif(IndicePerifericaSelezionata).fase = FaseBar2.Phases(ii + 1 - NUMEROFASIBARRA)
                        '    faseapp = FaseBar2.Phases(ii + 1 - NUMEROFASIBARRA)

                        'End If

                        If (ii < NUMEROFASIBARRA) Then
                            OggettiPerif(IndicePerifericaSelezionata).fase = FaseBar2.Phases(ii + 1)
                            faseapp = FaseBar2.Phases(ii + 1)

                        Else
                            OggettiPerif(IndicePerifericaSelezionata).fase = FaseBar1.Phases(ii + 1 - NUMEROFASIBARRA)
                            faseapp = FaseBar1.Phases(ii + 1 - NUMEROFASIBARRA)

                        End If

                        LFase.Text = faseapp.Text

                        'reset variabili e impostazione videata
                        TentativiPassoCorrente = PassiCollaudo(ii)("NTentativiPossibili")
                        LTentativiPossibili.Text = TentativiPassoCorrente.ToString
                        iiTentativi = 0
                        fNoErrore = False

                        PrimaMatrOrdine = fordini.OrdineSelezionato("Matricolainiziale")
                        UltimaMatrOrdine = fordini.OrdineSelezionato("NumPeriferiche") + PrimaMatrOrdine - 1


                        ' ciclo per il numero di tentativi massimo della fase corrente o per fase ok
                        Do While (iiTentativi < TentativiPassoCorrente And fNoErrore = False)
                            ' compilo la nuova riga del DB
                            FaseEseguita = Collaudi02DataSet.T_Coll_Fase.NewRow()
                            FaseEseguita("FK_Collaudo") = RigaTestataCollaudo("Id_Collaudo")
                            FaseEseguita("FK_Fase") = PassiCollaudo(ii)("Fk_FaseCollaudo")

                            ' imposto tentativo corrente e durata passo
                            DataIniPasso = DateTime.Now
                            iiTentativi += 1
                            LTentativo.Text = iiTentativi.ToString
                            ' chiamo il metodo
                            If ParametriMedotoDaChiamare.Length > 0 Then
                                RitornoMetodo = MetodoDaChiamare.Invoke(OggettiPerif(IndicePerifericaSelezionata), parametri)
                            Else
                                RitornoMetodo = MetodoDaChiamare.Invoke(OggettiPerif(IndicePerifericaSelezionata), Nothing)
                            End If
                            If RitornoMetodo = True Then
                                ' controllo della matricola corretta e non presente
                                ' fNoErrore = ControllaMatricola()
                                fNoErrore = True
                            Else
                                ' ritorno metodo con errore 
                                fNoErrore = False
                            End If

                            If StopCollaudoImposto = True Then
                                'LScollegata.Text = "Collaudo Annullato Per periferica Scollegata"

                                'LScollegata.Visible = True
                                fNoErrore = False
                            End If

                            ' Aggiorno i tempi e la videata
                            DataFinale = DateTime.Now
                            TempoPasso = DataFinale - DataIniPasso
                            LTempo.Text = TempoPasso.Hours.ToString + ":" + TempoPasso.Minutes.ToString + ":" + TempoPasso.Seconds.ToString
                            Tempocollaudo = DateTime.Now - DataIni
                            LTempoTot.Text = Tempocollaudo.Hours.ToString + ":" + Tempocollaudo.Minutes.ToString + ":" + Tempocollaudo.Seconds.ToString


                            OggettiPerif(IndicePerifericaSelezionata).FileTraccia.Durata_Test = Tempocollaudo
                            ' compilo i dati del passo appena eseguito 
                            FaseEseguita("Valore_Ritorno") = OggettiPerif(IndicePerifericaSelezionata).FileTraccia.UltimoRitornoFunzione
                            FaseEseguita("Descrizione") = OggettiPerif(IndicePerifericaSelezionata).FileTraccia.StrRet
                            FaseEseguita("N_Tentativo") = iiTentativi
                            FaseEseguita("TempoParziale") = TempoPasso
                            FaseEseguita("Note") = OggettiPerif(IndicePerifericaSelezionata).FileTraccia.Note

                            ' aggiungo la fase appena eseguita
                            Collaudi02DataSet.T_Coll_Fase.Rows.Add(FaseEseguita)
                            ' Se ci sono errori aggiorno la statistica
                            If fNoErrore = False Then
                                Dim KeyFase As Short
                                Dim strDescrApp As String
                                KeyFase = FaseEseguita("FK_Fase")
                                FaseErrore = Collaudi02DataSet.T_statistica.Select("FK_fase =" + KeyFase.ToString)

                                ' controllo se oggi ho già eseguito la fase corrente 
                                If FaseErrore.Count() > 0 Then
                                    FaseErrore(0)("count(FK_Fase)") += 1
                                Else

                                    ' devo creare la fase corrente come statistica.....
                                    Dim VFaseAppoggio As Collaudi02DataSet.T_statisticaRow
                                    'FaseEseguita = CollaudiDataSet.T_Coll_Fase.NewRow()
                                    VFaseAppoggio = Collaudi02DataSet.T_statistica.NewRow()
                                    VFaseAppoggio("FK_Fase") = KeyFase
                                    VFaseAppoggio("count(FK_Fase)") = 1
                                    strDescrApp = OggettiPerif(IndicePerifericaSelezionata).fase.text
                                    VFaseAppoggio("Descrizione") = strDescrApp
                                    Collaudi02DataSet.T_statistica.Rows.Add(VFaseAppoggio)
                                End If
                                Statistica()
                            End If
                        Loop

                        ' Testo il Reply Del metodo per sapere se posso continuare il collaudo o è fallito
                        If fNoErrore = True Then
                            FCollaudo = True

                        Else
                            FCollaudo = False

                        End If
                    End If
                    ii += 1
                Loop

                '                FlagCollaudoIniziato = False

                ' tolgo le immagini a video in modo che le possa salvare sul server
                If PictureBox1.Image Is Nothing Then
                Else
                    PictureBox1.Image.Dispose()
                    PictureBox1.Image = Nothing
                End If
                If PictureBox2.Image Is Nothing Then
                Else
                    PictureBox2.Image.Dispose()
                    PictureBox2.Image = Nothing
                End If

                ' fionisco di impostare i campi della testata del collaudo
                RigaTestataCollaudo("SerialNumberPiastra") = OggettiPerif(IndicePerifericaSelezionata).SerialNumberPiastra
                RigaTestataCollaudo("Matricolapiastra") = OggettiPerif(IndicePerifericaSelezionata).MatricolaPiastra
                RigaTestataCollaudo("Durata") = DateTime.Now.Subtract(DataIni)
                ' Metto il messaggio a video della fine collaudo
                If fNoErrore = True Then
                    ' nel DB scrivo la matricola solo se hofinito il collaudo con OK
                    RigaTestataCollaudo("Matricola") = OggettiPerif(IndicePerifericaSelezionata).MatricolaPeriferica


                    ' il collaudo è andato a buon fine posso salvare lo storico        
                    RigaStorico("FK_Collaudo") = RigaTestataCollaudo("Id_Collaudo")
                    RigaStorico("Doc_Timbrati") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Doc_Timbrati
                    RigaStorico("Doc_Timbrati_retro") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Doc_Timbrati_retro
                    RigaStorico("Documenti_Catturati") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Documenti_Catturati
                    RigaStorico("Documenti_Ingresso") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Documenti_Ingresso
                    RigaStorico("Documenti_Trattati") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Documenti_Trattati
                    RigaStorico("Documenti_Trattenuti") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Documenti_Trattenuti
                    RigaStorico("Doppia_Sfogliatura") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Doppia_Sfogliatura
                    RigaStorico("Errori_Barcode") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Errori_Barcode
                    RigaStorico("Errori_CMC7") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Errori_CMC7
                    RigaStorico("Errori_E13B") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Errori_E13B
                    RigaStorico("Errori_Ottici") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Errori_Ottici
                    RigaStorico("Jam_Allinea") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Jam_Allinea
                    RigaStorico("Jam_Card") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Jam_Card
                    RigaStorico("Jam_Cassetto1") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Jam_Cassetto1
                    RigaStorico("Jam_Cassetto2") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Jam_Cassetto2
                    RigaStorico("Jam_Cassetto3") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Jam_Cassetto3
                    RigaStorico("Jam_Feeder") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Jam_Feeder
                    RigaStorico("Jam_Feeder_Ret") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Jam_Feeder_Ret
                    RigaStorico("Jam_Micr") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Jam_Micr
                    RigaStorico("Jam_Micr_Ret") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Jam_Micr_Ret
                    RigaStorico("Jam_Percorso_DX") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Jam_Percorso_DX
                    RigaStorico("Jam_Percorso_SX") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Jam_Percorso_SX
                    RigaStorico("Jam_Print") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Jam_Print
                    RigaStorico("Jam_Print_Ret") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Jam_Print_Ret
                    RigaStorico("Jam_Scanner") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Jam_Scanner
                    RigaStorico("Jam_Scanner_Ret") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Jam_Scanner_Ret
                    RigaStorico("Jam_Sorter") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Jam_Sorter
                    RigaStorico("Jam_Stamp") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Jam_Stamp
                    RigaStorico("Num_Accensioni") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Num_Accensioni
                    RigaStorico("Tempo_Accenzione") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.TempoAccensione
                    RigaStorico("Trattenuti_Micr") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Trattenuti_Micr
                    RigaStorico("Trattenuti_Scan") = OggettiPerif(IndicePerifericaSelezionata).Hstorico.Trattenuti_Scan

                    Collaudi02DataSet.T_Storico.Rows.Add(RigaStorico)
                    fstorico = True



                    ''QUI SALVA LE IMMAGINI
                    'Dim ss() As String
                    'Dim filefronte As String
                    'Dim fileretro As String
                    'sss = Application.StartupPath()
                    'ss = IO.Directory.GetFiles(sss + "\Quality", OggettiPerif(IndicePerifericaSelezionata).PATHFRONTEQ + "*.bmp", IO.SearchOption.TopDirectoryOnly)
                    'filefronte = ss.Max()
                    'ss = IO.Directory.GetFiles(sss + "\Quality", OggettiPerif(IndicePerifericaSelezionata).PATHRETROQ + "*.bmp", IO.SearchOption.TopDirectoryOnly)
                    'fileretro = ss.Max()

                    'RigaImmagini("FK_Collaudo") = RigaTestataCollaudo("Id_Collaudo")
                    'Dim ff As System.IO.FileStream
                    'Dim fr As System.IO.FileStream
                    'Try
                    '    ff = New System.IO.FileStream(filefronte, IO.FileMode.Open)
                    '    fr = New System.IO.FileStream(fileretro, IO.FileMode.Open)
                    '    If ff Is Nothing Then
                    '    Else
                    '        Dim buffront(ff.Length) As Byte
                    '        ff.Read(buffront, 0, ff.Length)

                    '        ' file strem del file in jpg
                    '        SalvaJpg(ff, sss + "\quality" + "\FrontSave.jpg")

                    '        Dim fftronte As System.IO.FileStream
                    '        fftronte = New System.IO.FileStream(sss + "\quality" + "\FrontSave.jpg", IO.FileMode.Open)
                    '        If fftronte Is Nothing Then
                    '        Else
                    '            Dim buffronte(fftronte.Length) As Byte
                    '            fftronte.Read(buffronte, 0, fftronte.Length)

                    '            RigaImmagini("Image1") = buffronte
                    '            fftronte.Close()
                    '        End If
                    '        ff.Close()
                    '        If fr Is Nothing Then
                    '        Else
                    '            Dim bufback(fr.Length) As Byte
                    '            fr.Read(bufback, 0, fr.Length)
                    '            SalvaJpg(fr, sss + "\quality" + "\BackSave.jpg")

                    '            Dim ffback As System.IO.FileStream
                    '            ffback = New System.IO.FileStream(sss + "\quality" + "\BackSave.jpg", IO.FileMode.Open)
                    '            If ffback Is Nothing Then
                    '            Else
                    '                Dim buffback(ffback.Length) As Byte
                    '                ffback.Read(buffback, 0, ffback.Length)

                    '                RigaImmagini("Image2") = buffback
                    '                ffback.Close()
                    '                fr.Close()
                    '            End If
                    '        End If
                    '        Collaudi02DataSet.T_Immagini.Rows.Add(RigaImmagini)
                    '    End If

                    'Catch ex As Exception
                    '    MessageBox.Show(ex.ToString())
                    'End Try





                    FineCollaudo("Collaudo OK", True)

                Else
                    FineCollaudo("Collaudo Ko", False)

                End If

                'LScollegata.Text = "Scollegare la Periferica USB"
                'LScollegata.Visible = True


                If fServerPresente = True Then
                    'Salvo solo i collaudi Ok per non intasare i server....
                    'If fServerPresente = True And fNoErrore = True Then


                    Try
                        Dim r As Integer
                        Dim Nuovo_Id_Collaudo As Integer


                        ' scrivo i dati della testata del collaudo
                        r = Me.T_CollaudoTableAdapter.Update(Collaudi02DataSet.T_Collaudo)
                        If (r = Collaudi02DataSet.T_Collaudo.Count()) Then
                            'se la scrittura è andata a buon fine chiedo l'id inserito e lo cambio in locale
                            Nuovo_Id_Collaudo = Me.T_CollaudoTableAdapter.UltimoIser(RigaTestataCollaudo("Data"), RigaTestataCollaudo("Durata"), RigaTestataCollaudo("FK_Stazione"), RigaTestataCollaudo("FK_Ordine"), RigaTestataCollaudo("FKOperatore"))
                            If Nuovo_Id_Collaudo < 0 Then
                            Else

                                ' aggiorno le righe in locale
                                Collaudi02DataSet.T_Collaudo.Rows(0)("Id_Collaudo") = Nuovo_Id_Collaudo
                                RigaStorico("FK_Collaudo") = Collaudi02DataSet.T_Collaudo.Rows(0)("Id_Collaudo")
                                RigaImmagini("FK_Collaudo") = RigaStorico("FK_Collaudo")

                                ' scrivo la riga con le immagini

                                If fNoErrore = True Then
                                    'dati fine collaudi
                                    ' SetDatiFineCollaudo()

                                    'r = Me.T_DatiFineCollaudoTableAdapter.Update(Collaudi02DataSet.T_DatiFineCollaudo)

                                    If (r = Me.T_ImmaginiTableAdapter.Update(Collaudi02DataSet.T_Immagini) = 1) Then
                                    Else
                                    End If
                                End If

                                ' scrivo le righe del collaudo sul Collaudi02DataSet
                                r = Me.T_Coll_FaseTableAdapter.Update(Collaudi02DataSet.T_Coll_Fase)

                                If r = Collaudi02DataSet.T_Coll_Fase.Count() Then

                                    If fstorico = True Then
                                        r = Me.T_StoricoTableAdapter.Update(Collaudi02DataSet.T_Storico)
                                        If (r = Collaudi02DataSet.T_Storico.Count()) Then
                                            fstorico = False
                                        Else
                                            ' scrittura storico non a buon fine 
                                            ' devo cancellare le righe e l'intestazione del collaudo
                                            Me.T_ImmaginiTableAdapter.DeleteQuery(Nuovo_Id_Collaudo)
                                            Me.T_Coll_FaseTableAdapter.DeleteQuery(Nuovo_Id_Collaudo)
                                            Me.T_CollaudoTableAdapter.DeleteQuery(Nuovo_Id_Collaudo)
                                        End If
                                    End If
                                Else

                                    ' non ho scritto la riga di immagii
                                    ' cancello solamente l'intestazione del collaudo
                                    'Me.T_CollaudoTableAdapter.DeleteQuery(Nuovo_Id_Collaudo)
                                End If
                            End If

                        Else
                            ' la scrittura intestazione collaudo non è andata bene 
                        End If
                    Catch ex As Exception
                        If SalvaCollaudoLocale() = True Then
                            MessageBox.Show("Connessione di Rete Persa!!! Dati Salvati in Locale", "Errore !!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            Me.Close()
                        Else

                        End If
                    End Try
                Else 'non c'è il server dall' inizio

                    'Salvo solo i collaudi Ok per non intasare i server....
                    If (fNoErrore = True) Then
                        If SalvaCollaudoLocale() = True Then

                        Else

                        End If
                    End If

                End If

            Else
                ' fsoftwareaggiornato = false   
            End If
        Else
            ' l'oggetto caricato è null
            MessageBox.Show("Manca il file TestFasicollaudoXXXXX.dll per la periferica selezionata !!! ", "Errore !!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

        FlagCollaudoIniziato = False
        'If Not (trd Is Nothing) Then
        '    trd.Abort()
        'End If
        'UsbCtsMon1.Monitor = False
        'UsbCtsMon1.Visible = False
    End Sub

    Public Function ControllaMatricola() As Boolean
        Dim retbool As Boolean

        If MetodoDaChiamare.Name = "TestSNPerStampa" And fServerPresente = True Then
            MatricolainCollaudo = OggettiPerif(IndicePerifericaSelezionata).SerialNumberDaStampare
            SuffisspMatricola = MatricolainCollaudo.Substring(6)
            PrefissoMatricola = MatricolainCollaudo.Substring(0, 6)
            If PrefissoMatricola = "123456" Then
                ' non faccio nulla perchè sono in un collaudo di prova
                retbool = True

            Else
                ' sonotrolla che la matricola sia numerica !!!
                If Regex.IsMatch(SuffisspMatricola, "^\d{0,9}$") Then
                    ' matricola numerica, la converto in numero e
                    ' controllo che stia nel range e non sia presente in altri ordini

                    If SuffisspMatricola >= PrimaMatrOrdine And MatricolainCollaudo <= UltimaMatrOrdine Then
                        ' matricola in range, controllo che non sia presente i n altri collaudi ....
                        If (Me.T_CollaudoTableAdapter.ContaMatricoleEsistenti(SuffisspMatricola, RigaTestataCollaudo("FK_Ordine")) > 0) Then

                            ' ci sono altre matricole presenti in altri ordini
                            retbool = False

                        Else
                            retbool = True
                        End If



                    Else
                        'matricola non in range
                        retbool = False

                    End If

                Else
                    'matricola non numerica
                    retbool = False

                End If
            End If
        Else
            ' non sono nella fase di inserimento della matricola...
            retbool = True
        End If
        ControllaMatricola = retbool
    End Function
#End Region

    ' scorro la lista di dati salvati per il fine collaudo,
    ' e li salvo nel DB
    Public Sub SetDatiFineCollaudo()
        Dim buf(1) As Byte


        Dim aa As Integer = OggettiPerif(IndicePerifericaSelezionata).DatiFine.lista.Count()

        For iii As Integer = 0 To aa - 1
            RigaDatiFineCollaudo = Collaudi02DataSet.T_DatiFineCollaudo.NewRow()
            RigaDatiFineCollaudo("FK_Collaudo") = RigaTestataCollaudo("Id_Collaudo")
            RigaDatiFineCollaudo("FK_TipoDato") = OggettiPerif(IndicePerifericaSelezionata).DatiFine.lista(iii).Tipo
            RigaDatiFineCollaudo("Valore") = OggettiPerif(IndicePerifericaSelezionata).DatiFine.lista(iii).Buffer
            Collaudi02DataSet.T_DatiFineCollaudo.Rows.Add(RigaDatiFineCollaudo)

        Next

    End Sub

    Private Function GetEncoderInfo(ByVal mimeType As String) As Imaging.ImageCodecInfo
        Dim encoders() As Imaging.ImageCodecInfo
        encoders = Imaging.ImageCodecInfo.GetImageEncoders()
        For ii = 0 To encoders.Length
            If encoders(ii).MimeType = mimeType Then
                Return encoders(ii)
            End If
        Next
        Return Nothing
    End Function



    Public Function SalvaDBinLocale() As Boolean

        Try

            T_OrdineTableAdapter.Fill(Me.Collaudi02DataSet.T_Ordine)
            OperatoriTableAdapter.Fill(Me.Collaudi02DataSet.Operatori)

            T_PerifericaTableAdapter.Fill(Me.Collaudi02DataSet.T_Periferica)



            Collaudi02DataSet.T_Ordine.WriteXml(Application.StartupPath + LOCALDB + T_ORDINE)
            Collaudi02DataSet.T_Periferica.WriteXml(Application.StartupPath + LOCALDB + T_PERIFERICA)
            Collaudi02DataSet.T_Staz_Coll.WriteXml(Application.StartupPath + LOCALDB + T_STAZ_COLL)

            Collaudi02DataSet.T_Fasicollaudo.WriteXml(Application.StartupPath + LOCALDB + T_FASICOLLAUDO)
            Collaudi02DataSet.DettaglioOrdine.WriteXml(Application.StartupPath + LOCALDB + DETTAGLIOORDINE)
            Collaudi02DataSet.CategorieOpzioni.WriteXml(Application.StartupPath + LOCALDB + CATEGORIEOPZIONI)
            Collaudi02DataSet.ValoriOpzioni.WriteXml(Application.StartupPath + LOCALDB + VALORIOPZIONI)
            Collaudi02DataSet.T_Versioni.WriteXml(Application.StartupPath + LOCALDB + T_VERSIONI)
            Collaudi02DataSet.Operatori.WriteXml(Application.StartupPath + LOCALDB + T_OPERATORI)
            Collaudi02DataSet.T_Periferica.WriteXml(Application.StartupPath + LOCALDB + T_PERIFERICA)



            For Each r In Me.Collaudi02DataSet.T_Ordine
                If (r.Id_Ordine > 1500) Then 'non salvo piu' gli ordini di anni fa'.....
                    Me.SequenzaCollaudoTableAdapter.FillByp1p2(Me.Collaudi02DataSet.SequenzaCollaudo, r.FKPeriferica, r.Id_Ordine)
                    'Me.Collaudi02DataSet.SequenzaCollaudo.WriteXml(Application.StartupPath + LOCALDB + "\Seq_" + r.N_Ordine + ".xml")
                    'il N_ordine non viene piu' utilizzato ...sara' a NULL sempre
                    Me.Collaudi02DataSet.SequenzaCollaudo.WriteXml(Application.StartupPath + LOCALDB + "\Seq_" + r.Id_Ordine.ToString() + ".xml")
                End If
            Next
            Me.T_OrdineTableAdapter.Dispose()
            Me.SequenzaCollaudoTableAdapter.Dispose()

            Dim f As System.IO.TextWriter
            f = New System.IO.StreamWriter(Application.StartupPath + LOCALDB + DATA_SALVATAGGIO)
            f.WriteLine(Date.Now.ToString())
            f.Close()
        Catch ex As Exception
            MessageBox.Show("Dati per collaudare senza rete non Salvati Correttamente", "ERRORE", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'T_OrdineTableAdapter.Fill(Me.Collaudi02DataSet.T_Ordine)
            'OperatoriTableAdapter.Fill(Me.Collaudi02DataSet.Operatori)

            'T_PerifericaTableAdapter.Fill(Me.Collaudi02DataSet.T_Periferica)



            'Collaudi02DataSet.T_Ordine.WriteXml(Application.StartupPath + LOCALDB + T_ORDINE)
            'Collaudi02DataSet.T_Periferica.WriteXml(Application.StartupPath + LOCALDB + T_PERIFERICA)
            'Collaudi02DataSet.T_Staz_Coll.WriteXml(Application.StartupPath + LOCALDB + T_STAZ_COLL)

            'Collaudi02DataSet.T_Fasicollaudo.WriteXml(Application.StartupPath + LOCALDB + T_FASICOLLAUDO)
            'Collaudi02DataSet.DettaglioOrdine.WriteXml(Application.StartupPath + LOCALDB + DETTAGLIOORDINE)
            'Collaudi02DataSet.CategorieOpzioni.WriteXml(Application.StartupPath + LOCALDB + CATEGORIEOPZIONI)
            'Collaudi02DataSet.ValoriOpzioni.WriteXml(Application.StartupPath + LOCALDB + VALORIOPZIONI)
            'Collaudi02DataSet.T_Versioni.WriteXml(Application.StartupPath + LOCALDB + T_VERSIONI)
            'Collaudi02DataSet.Operatori.WriteXml(Application.StartupPath + LOCALDB + T_OPERATORI)
            'Collaudi02DataSet.T_Periferica.WriteXml(Application.StartupPath + LOCALDB + T_PERIFERICA)



            'For Each r In Me.Collaudi02DataSet.T_Ordine
            '    If (r.Id_Ordine > 1500) Then 'non salvo piu' gli ordini di anni fa'.....
            '        Me.SequenzaCollaudoTableAdapter.FillByp1p2(Me.Collaudi02DataSet.SequenzaCollaudo, r.FKPeriferica, r.Id_Ordine)
            '        'Me.Collaudi02DataSet.SequenzaCollaudo.WriteXml(Application.StartupPath + LOCALDB + "\Seq_" + r.N_Ordine + ".xml")
            '        'il N_ordine non viene piu' utilizzato ...sara' a NULL sempre
            '        Me.Collaudi02DataSet.SequenzaCollaudo.WriteXml(Application.StartupPath + LOCALDB + "\Seq_" + r.Id_Ordine.ToString() + ".xml")
            '    End If
            'Next
            'Me.T_OrdineTableAdapter.Dispose()
            'Me.SequenzaCollaudoTableAdapter.Dispose()

            'Dim f As System.IO.TextWriter
            'f = New System.IO.StreamWriter(Application.StartupPath + LOCALDB + DATA_SALVATAGGIO)
            'f.WriteLine(Date.Now.ToString())
            'f.Close()
        End Try

    End Function

    Public Function SalvaDatiServer() As Boolean

        '23/01/2017 le immagini vengono salvate su server e non piu' dentro il DataBase
        Try
            'Save Report File on Server
            ' Loop through each file in the directory
            For Each fileD As IO.FileInfo In New IO.DirectoryInfo(Application.StartupPath() + "\Images_Scanner").GetFiles

                If fileD.Name <> "Thumbs.db" Then
                    ' copy resource to users local directory
                    If (File.Exists(SaveLogPath & " \ " & fileD.Name) = False) Then
                        fileD.CopyTo(SaveLogPath & " \ " & fileD.Name)
                        '
                        'prove...debug
                        'fileD.CopyTo(Application.StartupPath() & "\Images_Scanner\old\" & fileD.Name)
                        '
                        File.Delete(Application.StartupPath() + "\Images_Scanner\" & fileD.Name)
                    Else
                        Console.WriteLine("FileGiaPresentesulServer")
                        File.Delete(Application.StartupPath() + "\Images_Scanner\" & fileD.Name)
                    End If

                End If
            Next
        Catch ex As Exception
            'verranno salvati al prossimo collaudo effettuato ...
        End Try

        '07/04/2017 / 22/08/2017
        Try
            'Save Report File on Server
            ' Loop through each file in the directory
            For Each fileD As IO.FileInfo In New IO.DirectoryInfo(Application.StartupPath() + "\Images_Scanner\Log").GetFiles

                If fileD.Name <> "Thumbs.db" Then
                    ' copy resource to users local directory
                    If (File.Exists(SaveLogPath & " \Log " & " \ " & fileD.Name) = False) Then
                        fileD.CopyTo(SaveLogPath & " \Log " & " \ " & fileD.Name)
                        File.Delete(Application.StartupPath() + "\Images_Scanner\Log\" & fileD.Name)
                    Else
                        Console.WriteLine("FileGiaPresentesulServer")
                        File.Delete(Application.StartupPath() + "\Images_Scanner\Log\" & fileD.Name)
                    End If

                End If
            Next
        Catch ex As Exception
            'verranno salvati al prossimo collaudo effettuato ...
        End Try

        '26/03/2019  REPORT LS150 SUL SERVER
        Try
            For Each fileD As IO.FileInfo In New IO.DirectoryInfo(Application.StartupPath() + "\Report150\").GetFiles
                If fileD.Name <> "Thumbs.db" Then
                    ' copy resource to users local directory
                    If (File.Exists(SaveReportCollaudiPathLS150 & " \ " & fileD.Name) = False) Then
                        fileD.CopyTo(SaveReportCollaudiPathLS150 & " \ " & fileD.Name)
                        File.Delete(Application.StartupPath() + "\Report150\" & fileD.Name)
                    Else
                        Console.WriteLine("FileGiaPresentesulServer")
                        File.Delete(Application.StartupPath() + "\Report150\" & fileD.Name)
                    End If

                End If
            Next
        Catch ex As Exception
            'verranno salvati al prossimo collaudo effettuato ...
        End Try


        '26/03/2019  REPORT LS40 SUL SERVER
        Try
            For Each fileD As IO.FileInfo In New IO.DirectoryInfo(Application.StartupPath() + "\Report40\").GetFiles
                If fileD.Name <> "Thumbs.db" Then
                    ' copy resource to users local directory
                    If (File.Exists(SaveReportCollaudiPathLS40 & " \ " & fileD.Name) = False) Then
                        fileD.CopyTo(SaveReportCollaudiPathLS40 & " \ " & fileD.Name)
                        File.Delete(Application.StartupPath() + "\Report40\" & fileD.Name)
                    Else
                        Console.WriteLine("FileGiaPresentesulServer")
                        File.Delete(Application.StartupPath() + "\Report40\" & fileD.Name)
                    End If

                End If
            Next
        Catch ex As Exception
            'verranno salvati al prossimo collaudo effettuato ...
        End Try

        '26/03/2019  REPORT LS100 SUL SERVER
        Try
            For Each fileD As IO.FileInfo In New IO.DirectoryInfo(Application.StartupPath() + "\Report100\").GetFiles
                If fileD.Name <> "Thumbs.db" Then
                    ' copy resource to users local directory
                    If (File.Exists(SaveReportCollaudiPathLS100 & " \ " & fileD.Name) = False) Then
                        fileD.CopyTo(SaveReportCollaudiPathLS100 & " \ " & fileD.Name)
                        File.Delete(Application.StartupPath() + "\Report100\" & fileD.Name)
                    Else
                        Console.WriteLine("FileGiaPresentesulServer")
                        File.Delete(Application.StartupPath() + "\Report100\" & fileD.Name)
                    End If

                End If
            Next
        Catch ex As Exception
            'verranno salvati al prossimo collaudo effettuato ...
        End Try

        '26/03/2019  REPORT LS515 SUL SERVER
        Try
            For Each fileD As IO.FileInfo In New IO.DirectoryInfo(Application.StartupPath() + "\Report515\").GetFiles
                If fileD.Name <> "Thumbs.db" Then
                    ' copy resource to users local directory
                    If (File.Exists(SaveReportCollaudiPathLS515 & " \ " & fileD.Name) = False) Then
                        fileD.CopyTo(SaveReportCollaudiPathLS515 & " \ " & fileD.Name)
                        File.Delete(Application.StartupPath() + "\Report515\" & fileD.Name)
                    Else
                        Console.WriteLine("FileGiaPresentesulServer")
                        File.Delete(Application.StartupPath() + "\Report515\" & fileD.Name)
                    End If

                End If
            Next
        Catch ex As Exception
            'verranno salvati al prossimo collaudo effettuato ...
        End Try



    End Function

    'Private Sub SequenzaCollaudoBindingNavigatorSaveItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Me.Validate()
    '    Me.SequenzaCollaudoBindingSource.EndEdit()
    '    Me.TableAdapterManager.UpdateAll(Me.Collaudi02DataSet)

    'End Sub


    Private Sub ProvaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProvaToolStripMenuItem.Click
        Dim f As FAbilitaOp
        f = New FAbilitaOp
        f.ShowDialog()
        StazioneDiCollaudoToolStripMenuItem.Enabled = False
        OrdiniToolStripMenuItem.Enabled = False
        CollaudoToolStripMenuItem.Enabled = False
        foperatore.OperatoreSelezionato = Nothing

    End Sub



    Private Sub SeqToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub


    Public Function testServer() As Boolean
        Dim myConnectionString As String
        Dim ret As Boolean
        myConnectionString = My.Settings.CollaudiConnectionString

        Try
            Dim conn As New MySql.Data.MySqlClient.MySqlConnection(myConnectionString)
            conn.Open()
            conn.Close()
            ret = True

        Catch ex As MySql.Data.MySqlClient.MySqlException
            ret = False
            Select Case ex.Number
                Case 0
                    ' LabelStateRemote.Text = "DB Remoto - Errore di connessione"
                Case 1045
                    ' LabelStateRemote.Text = "DB Remoto - Dati di connessione errati"
            End Select

        End Try
        Return ret
    End Function


    Public Sub SalvaJpg(ByVal ff As System.IO.FileStream, ByVal Nomefile As String)
        Dim myBitmap As Bitmap
        Dim myImageCodecInfo As Imaging.ImageCodecInfo
        Dim myEncoder As System.Drawing.Imaging.Encoder
        Dim myEncoderParameter As System.Drawing.Imaging.EncoderParameter
        Dim myEncoderParameters As System.Drawing.Imaging.EncoderParameters

        '// Create a Bitmap object based on a BMP file.
        myBitmap = New Bitmap(ff)

        ' Get an ImageCodecInfo object that represents the JPEG codec.
        myImageCodecInfo = GetEncoderInfo("image/jpeg")

        ' Create an Encoder object based on the GUID for the Quality 
        ' parameter category.
        myEncoder = System.Drawing.Imaging.Encoder.Quality

        '// Create an EncoderParameters object.
        '// An EncoderParameters object has an array of EncoderParameter
        '// objects. In this case, there is only one
        '// EncoderParameter object in the array.
        myEncoderParameters = New Imaging.EncoderParameters(1)

        '// Save the bitmap as a JPEG file with quality level 100.
        myEncoderParameter = New Imaging.EncoderParameter(myEncoder, 40L)
        myEncoderParameters.Param(0) = myEncoderParameter
        myBitmap.Save(Nomefile, myImageCodecInfo, myEncoderParameters)
    End Sub

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click

    End Sub


    Private Sub TestToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TestToolStripMenuItem.Click

    End Sub

    Private Sub ScorriOrdiniToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ScegliOrdineToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        MessageBox.Show("Prova")
    End Sub

    Private Sub PiastreLS40ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PiastreLS40ToolStripMenuItem.Click


    End Sub
    Public Sub EseguicollaudoPiastre(ByVal StrSeq As String, ByVal NomeP As String)

        Dim TempoPasso As TimeSpan
        Dim Tempocollaudo As TimeSpan
        Dim DataIniPasso As DateTime
        Dim DataIni As DateTime
        Dim DataFinale As DateTime
        Dim IndicePerifericaSelezionata As Integer
        Dim str As String


        Dim ii As Integer

        DataIni = DateTime.Now()
        Dim FasEseApp() As Collaudi02DataSet.T_Coll_FaseRow

        ' creo la nuova riga di testata collaudo
        RigaTestataCollaudo = Collaudi02DataSet.T_Collaudo.NewRow()

        ' contiene lo storico della periferica prima che venga cancellato.
        RigaStorico = Collaudi02DataSet.T_Storico.NewRow()
        RigaImmagini = Collaudi02DataSet.T_Immagini.NewRow()

        FCollaudo = True

        ' cerco la dll TestFasicollaudoXxxxx.dll da utilizzare rispetto all'
        'ordine selezionato
        For ii = 0 To NUMERO_MAX_PERIF_GESTITE
            If NomiPerif(ii) Is Nothing Then
            Else
                If NomiPerif(ii) = NomeP Then
                    IndicePerifericaSelezionata = ii
                End If
            End If
        Next

        ' Controllo che la dll corretta sia stata caricata
        If Not (OggettiPerif(IndicePerifericaSelezionata) Is Nothing) Then
            TempoPasso = New TimeSpan
            Tempocollaudo = New TimeSpan

            ' inizializzo l'oggetto da usare
            OggettiPerif(IndicePerifericaSelezionata).formpadre = Me

            ClassePeriferica = OggettiPerif(IndicePerifericaSelezionata).GetType

            MetodiClasse = ClassePeriferica.GetMethods()
            '  StatisticaFasi = CollaudiDataSet.T_Coll_Fase

            OggettiPerif(IndicePerifericaSelezionata).Pimage = PictureBox1
            OggettiPerif(IndicePerifericaSelezionata).PImageBack = PictureBox2

            ' prendo i dati dal Database e pulisco i dati dal collaudo precedente
            Me.Collaudi02DataSet.T_Coll_Fase.Clear()
            Me.Collaudi02DataSet.T_Collaudo.Clear()
            Me.Collaudi02DataSet.T_Storico.Clear()
            Me.Collaudi02DataSet.T_statistica.Clear()
            Me.Collaudi02DataSet.T_Immagini.Clear()
            Me.Collaudi02DataSet.SequenzaCollaudo.Clear()

            Dim sss As String


            ' creo la sequenza di collaudo corretta
            Me.Collaudi02DataSet.SequenzaCollaudo.ReadXml(StrSeq)

            'Dim sss As String
            'Dim ind As Integer
            'Dim zzz As String
            sss = Application.StartupPath()
            'ind = sss.LastIndexOf("\")
            'zzz = sss.Substring(0, ind)
            'zzz = "c:"

            If PictureBox1.Image Is Nothing Then
            Else
                PictureBox1.Image.Dispose()
                PictureBox1.Image = Nothing
            End If
            If PictureBox2.Image Is Nothing Then
            Else
                PictureBox2.Image.Dispose()
                PictureBox2.Image = Nothing
            End If



            Try
                If IO.Directory.Exists(sss + "\Dati") Then
                    IO.Directory.Delete(sss + "\Dati", True)
                End If
                IO.Directory.CreateDirectory(sss + "\Dati")

                'If IO.Directory.Exists(sss + "\Quality") Then
                '    IO.Directory.Delete(sss + "\Quality", True)

                'End If
                'IO.Directory.CreateDirectory(sss + "\Quality")
                str = Application.StartupPath + IMAGES
                If (IO.Directory.Exists(str) = False) Then
                    IO.Directory.CreateDirectory(str)
                End If
            Catch ex As Exception
                MessageBox.Show("Error....", ex.Message + " ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try



            ' se il SW è Aggiornato o l'utente vuole andare avanti ugualmente

            SplitForm.Panel2Collapsed = True
            SplitForm.Panel1Collapsed = False
            'cerco le opzioni nel Dettaglio ORD per impostare la configurazione
            'DEBUG DA IMPLEMENTARE
            '        'FasiLS150.ImpostaFlagPerConfigurazioe(fordini.VettValoriOpzioni)

            ' imposto altre proprietà per il collaudo
            OggettiPerif(IndicePerifericaSelezionata).Ver_firm1 = Lfw1.Text
            OggettiPerif(IndicePerifericaSelezionata).Ver_firm2 = Lfw2.Text
            OggettiPerif(IndicePerifericaSelezionata).Ver_firm3 = Lfw3.Text
            OggettiPerif(IndicePerifericaSelezionata).Ver_firm4 = Lfw4.Text
            OggettiPerif(IndicePerifericaSelezionata).Ver_firm5 = Lfw5.Text
            'OggettiPerif(IndicePerifericaSelezionata).Ver_firm6 = Lfw6.Text
            'OggettiPerif(IndicePerifericaSelezionata).Ver_firm7 = Lfw7.Text
            'OggettiPerif(IndicePerifericaSelezionata).Ver_firm8 = Lfw8.Text
            OggettiPerif(IndicePerifericaSelezionata).Ver_firm9 = Lfw9.Text


            ' imposto gli oggetti per i campi della stampa del report
            OggettiPerif(IndicePerifericaSelezionata).FileTraccia.Matricola = ""
            OggettiPerif(IndicePerifericaSelezionata).FileTraccia.VersioneDll = ""
            OggettiPerif(IndicePerifericaSelezionata).FileTraccia.Periferica = LNomep.Text
            OggettiPerif(IndicePerifericaSelezionata).FileTraccia.Ordine_prod = LNordine.Text
            'OggettiPerif(IndicePerifericaSelezionata).FileTraccia.Operatore = LOperatore.Text

            OggettiPerif(IndicePerifericaSelezionata).FileTraccia.Operatore = FormOperatore.NomeOperatore
            OggettiPerif(IndicePerifericaSelezionata).FileTraccia.NomeStazione = FormOperatore.NomeStazione

            OggettiPerif(IndicePerifericaSelezionata).FileTraccia.VersioneSw = SOFTWARE_VERSION_X_REPORT
            'OggettiPerif(IndicePerifericaSelezionata).FileTraccia.VersioneDll = DLL_VERSION_X_REPORT
            OggettiPerif(IndicePerifericaSelezionata).FileTraccia.LineaProd = LNomeLinea.Text
            OggettiPerif(IndicePerifericaSelezionata).FileTraccia.Manufacter = LAzienda.Text
            OggettiPerif(IndicePerifericaSelezionata).FileTraccia.DataCollaudo = New Date(Date.Now().Year, Date.Now().Month, Date.Now.Day)




            MetodoDaChiamare = TipiPerif(IndicePerifericaSelezionata).GetMethod("ImpostaFlagPerConfigurazione")
            'fordini.VettValoriOpzioni(0) = 4

            Dim valori() As Object = {vettorepiastra}

            'Dim args() As Object = valori

            RitornoMetodo = MetodoDaChiamare.Invoke(OggettiPerif(IndicePerifericaSelezionata), valori)

            PassiCollaudo = Collaudi02DataSet.SequenzaCollaudo.Rows

            RigaTestataCollaudo("Data") = DateTime.Now

            ' crea la barra delle fasi

            CreaBarraFasiCollaudoPiastre()

            FasEseApp = Collaudi02DataSet.T_Coll_Fase.Select("Id_Passo = Max(Id_Passo)")

            ' Eseguo il collaudo
            ii = 0

            ' ciclo fino a quando sono arrivato a ok o a ko
            Do While (ii < PassiCollaudo.Count And FCollaudo = True)

                ' attivo la porzione di barra corrente
                If (ii < NUMEROFASIBARRA) Then
                    OggettiPerif(IndicePerifericaSelezionata).fase = FaseBar1.Phases(ii + 1)
                Else
                    OggettiPerif(IndicePerifericaSelezionata).fase = FaseBar2.Phases(ii + 1 - NUMEROFASIBARRA)
                End If
                'If (ii < NUMEROFASIBARRA) Then
                '    OggettiPerif(IndicePerifericaSelezionata).fase = FaseBar2.Phases(ii + 1)
                'Else
                '    OggettiPerif(IndicePerifericaSelezionata).fase = FaseBar1.Phases(ii + 1 - NUMEROFASIBARRA)
                'End If

                ImpostaMatodoDaChiamare(ii)

                'reset variabili e impostazione videata
                TentativiPassoCorrente = PassiCollaudo(ii)("NTentativiPossibili")
                LTentativiPossibili.Text = TentativiPassoCorrente.ToString
                iiTentativi = 0
                fNoErrore = False

                ' ciclo per il numero di tentativi massimo della fase corrente o per fase ok
                Do While (iiTentativi < TentativiPassoCorrente And fNoErrore = False)
                    ' compilo la nuova riga del DB
                    FaseEseguita = Collaudi02DataSet.T_Coll_Fase.NewRow()
                    FaseEseguita("FK_Collaudo") = RigaTestataCollaudo("Id_Collaudo")
                    FaseEseguita("FK_Fase") = PassiCollaudo(ii)("Fk_FaseCollaudo")

                    ' imposto tentativo corrente e durata passo
                    DataIniPasso = DateTime.Now
                    iiTentativi += 1
                    LTentativo.Text = iiTentativi.ToString
                    ' chiamo il metodo
                    If ParametriMedotoDaChiamare.Length > 0 Then
                        RitornoMetodo = MetodoDaChiamare.Invoke(OggettiPerif(IndicePerifericaSelezionata), parametri)
                    Else
                        RitornoMetodo = MetodoDaChiamare.Invoke(OggettiPerif(IndicePerifericaSelezionata), Nothing)
                    End If
                    If RitornoMetodo = True Then
                        fNoErrore = True
                    Else
                        fNoErrore = False
                    End If
                    ' Aggiorno i tempi e la videata
                    DataFinale = DateTime.Now
                    TempoPasso = DataFinale - DataIniPasso
                    LTempo.Text = TempoPasso.Hours.ToString + ":" + TempoPasso.Minutes.ToString + ":" + TempoPasso.Seconds.ToString
                    Tempocollaudo = DateTime.Now - DataIni
                    LTempoTot.Text = Tempocollaudo.Hours.ToString + ":" + Tempocollaudo.Minutes.ToString + ":" + Tempocollaudo.Seconds.ToString

                Loop

                ' Testo il Reply Del metodo per sapere se posso continuare il collaudo o è fallito
                If fNoErrore = True Then
                    FCollaudo = True
                Else
                    FCollaudo = False
                End If

                ii += 1
            Loop

            ' tolgo le immagini a video in modo che le possa salvare sul server
            If PictureBox1.Image Is Nothing Then
            Else
                PictureBox1.Image.Dispose()
                PictureBox1.Image = Nothing
            End If
            If PictureBox2.Image Is Nothing Then
            Else
                PictureBox2.Image.Dispose()
                PictureBox2.Image = Nothing
            End If

            ' Metto il messaggio a video della fine collaudo
            If fNoErrore Then
                FineCollaudo(MessaggioFineOk, True)
            Else
                FineCollaudo(MessaggioFineKo, False)
            End If
        Else
            ' l'oggetto caricato è null
            MessageBox.Show("Manca il file TestFasicollaudoXXXXX.dll per la periferica selezionata !!! ", "Errore !!!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Public Sub New()

        ' Chiamata richiesta da Progettazione Windows Form.
        InitializeComponent()
        RegisterHidNotification()

        ' Aggiungere le eventuali istruzioni di inizializzazione dopo la chiamata a InitializeComponent().

    End Sub

    Private Sub CollaudoBrasileToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CollaudoBrasileToolStripMenuItem.Click

    End Sub

    Private Sub Ls40ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ls40ToolStripMenuItem.Click
        Dim fbra As FBraSile
        fbra = New FBraSile()
        fbra.StrPath = Application.StartupPath + LOCALDBBRASIL + "\Ls40"
        fbra.StrNome = "Ls40"

        If (System.IO.Directory.Exists(fbra.StrPath) = False) Then
            MessageBox.Show("Attenzione Manca la cartella LS40 ", "collaudo non possibile", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            If (System.IO.Directory.Exists(Application.StartupPath + LOCALDBBRASIL + "\Ls40_FW") = False) Then
                MessageBox.Show("Attenzione Manca la cartella LS40_FW ", "collaudo non possibile", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else


                If (fbra.ShowDialog(Me.ParentForm) = Windows.Forms.DialogResult.OK) Then
                    MessaggioFineOk = COLLAUDO_PERIFERICA_OK_BRA
                    MessaggioFineKo = COLLAUDO_PERIFERICA_KO_BRA

                    LNomeLinea.Text = fbra.CFLinea
                    LNomep.Text = fbra.CFModello
                    'LOperatore.Text = fbra.CFOperatore
                    LOperatore.Text = ""
                    LNordine.Text = fbra.CFOrdine
                    LAzienda.Text = fbra.CFProduttore

                    vettorepiastra = fbra.v
                    Lfw1.Text = ""
                    Lfw2.Text = ""
                    Lfw3.Text = ""
                    Lfw4.Text = ""
                    Lfw5.Text = ""
                    Lfw6.Text = ""
                    Lfw7.Text = ""
                    Lfw8.Text = ""
                    Lfw9.Text = ""

                    Select Case fbra.SequenzeFW.Count() - 1
                        Case 1
                            Lfw1.Text = fbra.SequenzeFW(1)
                        Case 2
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                        Case 3
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                            Lfw3.Text = fbra.SequenzeFW(3)
                        Case 4
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                            Lfw3.Text = fbra.SequenzeFW(3)
                            Lfw4.Text = fbra.SequenzeFW(4)
                        Case 5
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                            Lfw3.Text = fbra.SequenzeFW(3)
                            Lfw4.Text = fbra.SequenzeFW(4)
                            Lfw5.Text = fbra.SequenzeFW(5)
                        Case 6
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                            Lfw3.Text = fbra.SequenzeFW(3)
                            Lfw4.Text = fbra.SequenzeFW(4)
                            Lfw5.Text = fbra.SequenzeFW(5)
                            Lfw6.Text = fbra.SequenzeFW(6)
                        Case 7
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                            Lfw3.Text = fbra.SequenzeFW(3)
                            Lfw4.Text = fbra.SequenzeFW(4)
                            Lfw5.Text = fbra.SequenzeFW(5)
                            Lfw6.Text = fbra.SequenzeFW(6)
                            Lfw7.Text = fbra.SequenzeFW(7)
                        Case 8
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                            Lfw3.Text = fbra.SequenzeFW(3)
                            Lfw4.Text = fbra.SequenzeFW(4)
                            Lfw5.Text = fbra.SequenzeFW(5)
                            Lfw6.Text = fbra.SequenzeFW(6)
                            Lfw7.Text = fbra.SequenzeFW(7)
                            Lfw8.Text = fbra.SequenzeFW(8)

                        Case 9
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                            Lfw3.Text = fbra.SequenzeFW(3)
                            Lfw4.Text = fbra.SequenzeFW(4)
                            Lfw5.Text = fbra.SequenzeFW(5)
                            Lfw6.Text = fbra.SequenzeFW(6)
                            Lfw7.Text = fbra.SequenzeFW(7)
                            Lfw8.Text = fbra.SequenzeFW(8)
                            Lfw9.Text = fbra.SequenzeFW(9)

                    End Select



                    EseguicollaudoPiastre(fbra.StrPath + "\" + fbra.FileSelezionato + ".xml", "Ls40")

                End If

            End If

        End If


    End Sub

    Private Sub Ls100ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ls100ToolStripMenuItem.Click
        Dim fbra As FBraSile
        fbra = New FBraSile()
        fbra.StrPath = Application.StartupPath + LOCALDBBRASIL + "\Ls100"
        fbra.StrNome = "Ls100"

        If (System.IO.Directory.Exists(fbra.StrPath) = False) Then
            MessageBox.Show("Attenzione Manca la cartella LS100 ", "collaudo non possibile", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            If (System.IO.Directory.Exists(Application.StartupPath + LOCALDBBRASIL + "\Ls100_FW") = False) Then
                MessageBox.Show("Attenzione Manca la cartella LS100_FW ", "collaudo non possibile", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else


                If (fbra.ShowDialog(Me.ParentForm) = Windows.Forms.DialogResult.OK) Then
                    MessaggioFineOk = COLLAUDO_PERIFERICA_OK_BRA
                    MessaggioFineKo = COLLAUDO_PERIFERICA_KO_BRA

                    LNomeLinea.Text = fbra.CFLinea
                    LNomep.Text = fbra.CFModello
                    'LOperatore.Text = fbra.CFOperatore
                    LOperatore.Text = ""
                    LNordine.Text = fbra.CFOrdine
                    LAzienda.Text = fbra.CFProduttore

                    vettorepiastra = fbra.v
                    Lfw1.Text = ""
                    Lfw2.Text = ""
                    Lfw3.Text = ""
                    Lfw4.Text = ""
                    Lfw5.Text = ""
                    Lfw6.Text = ""
                    Lfw7.Text = ""
                    Lfw8.Text = ""
                    Lfw9.Text = ""

                    Select Case fbra.SequenzeFW.Count() - 1
                        Case 1
                            Lfw1.Text = fbra.SequenzeFW(1)
                        Case 2
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                        Case 3
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                            Lfw3.Text = fbra.SequenzeFW(3)
                        Case 4
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                            Lfw3.Text = fbra.SequenzeFW(3)
                            Lfw4.Text = fbra.SequenzeFW(4)
                        Case 5
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                            Lfw3.Text = fbra.SequenzeFW(3)
                            Lfw4.Text = fbra.SequenzeFW(4)
                            Lfw5.Text = fbra.SequenzeFW(5)
                        Case 6
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                            Lfw3.Text = fbra.SequenzeFW(3)
                            Lfw4.Text = fbra.SequenzeFW(4)
                            Lfw5.Text = fbra.SequenzeFW(5)
                            Lfw6.Text = fbra.SequenzeFW(6)
                        Case 7
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                            Lfw3.Text = fbra.SequenzeFW(3)
                            Lfw4.Text = fbra.SequenzeFW(4)
                            Lfw5.Text = fbra.SequenzeFW(5)
                            Lfw6.Text = fbra.SequenzeFW(6)
                            Lfw7.Text = fbra.SequenzeFW(7)
                        Case 8
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                            Lfw3.Text = fbra.SequenzeFW(3)
                            Lfw4.Text = fbra.SequenzeFW(4)
                            Lfw5.Text = fbra.SequenzeFW(5)
                            Lfw6.Text = fbra.SequenzeFW(6)
                            Lfw7.Text = fbra.SequenzeFW(7)
                            Lfw8.Text = fbra.SequenzeFW(8)

                        Case 9
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                            Lfw3.Text = fbra.SequenzeFW(3)
                            Lfw4.Text = fbra.SequenzeFW(4)
                            Lfw5.Text = fbra.SequenzeFW(5)
                            Lfw6.Text = fbra.SequenzeFW(6)
                            Lfw7.Text = fbra.SequenzeFW(7)
                            Lfw8.Text = fbra.SequenzeFW(8)
                            Lfw9.Text = fbra.SequenzeFW(9)

                    End Select



                    EseguicollaudoPiastre(fbra.StrPath + "\" + fbra.FileSelezionato + ".xml", "Ls100")

                End If

            End If

        End If




    End Sub

    Private Sub PiastraLS150ColoriToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)



    End Sub

    Private Sub SplitForm_Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles SplitForm.Panel1.Paint

    End Sub

    Private Sub UsbCtsMon1_UsbEvent(ByVal sender As Object, ByVal e As System.EventArgs)
        '01/05/2016 commentato per ora
        'If UsbCtsMon1.GetStatus() = False Then
        '    If MessageboxMy("La periferica è stata scollegata durante il collaudo, si consiglia di riiniziare il collaudo!!Vuoi terminare ?", "Errore", MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing) = DialogResult.Yes Then
        '        StopCollaudoImposto = True
        '    Else
        '        'LScollegata.Text = "Il Collaudo Sta continuando anche se la periferica è stata scollegata.... ERRORE !!!!"
        '        Dim MessaggioMail As String
        '        MessaggioMail = "Attenzione : sull'ordine di produzione " + LNordine.Text + " per la periferica  " + LNomep.Text + vbCrLf
        '        MessaggioMail += "L'operatore " + LOperatore.Text + " " + LNomeStaz.Text + " " + LNomeLinea.Text + " " + LAzienda.Text + " " + LIndIP.Text + vbCrLf
        '        MessaggioMail += " ha scollegato e ricollegato una periferica durante il collaudo e ha continuato il collaudo" + vbCrLf
        '        MessaggioMail += "DETTAGLI :" + vbCrLf
        '        MessaggioMail += OggettiPerif(IndicePerifericaSelezionata).MatricolaPiastra + vbCrLf
        '        MessaggioMail += OggettiPerif(IndicePerifericaSelezionata).SerialNumberPiastra + vbCrLf
        '        MessaggioMail += OggettiPerif(IndicePerifericaSelezionata).MatricolaPeriferica + vbCrLf
        '        MessaggioMail += vbCrLf + " Durante la Fase " + FaseEseguita("FK_Fase").ToString()
        '        MessaggioMail += FaseEseguita("Valore_Ritorno").ToString() + vbCrLf
        '        MessaggioMail += FaseEseguita("Descrizione").ToString() + vbCrLf
        '        MessaggioMail += FaseEseguita("N_Tentativo").ToString() + vbCrLf
        '        MessaggioMail += FaseEseguita("TempoParziale").ToString() + vbCrLf
        '        MessaggioMail += FaseEseguita("Note").ToString()

        '        'Dim Message As System.Net.Mail.MailMessage = New System.Net.Mail.MailMessage("Collaudoperiferiche@ctsgroup.it", "c.roffinott@ctsgroup.it", "Anomalia durante il collaudo", MessaggioMail)
        '        'Dim mailClient As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient("172.16.0.1")
        '        'mailClient.Send(Message)
        '        ' MessageBox.Show("evento usb")
        '    End If
        'End If

    End Sub

    Private Sub PiastraL75ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub PiastraL75MICRToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim v() As Integer = {8, 10, 50, 43}
        Dim sss As String

        vettorepiastra = v
        StrSeqPiastre = "\Seq_PiastreL75.xml"
        MessaggioFineOk = COLLAUDO_PIASTRA_OK
        MessaggioFineKo = COLLAUDO_PIASTRA_KO
        sss = Application.StartupPath + LOCALDBPIASTRE + StrSeqPiastre

        EseguicollaudoPiastre(sss, "L75")
    End Sub

    Private Sub PiastraL75NOMICRToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim v() As Integer = {10, 50, 43} 'vettore opzioni
        Dim sss As String

        vettorepiastra = v
        StrSeqPiastre = "\Seq_PiastreL75_NOMICR.xml"
        MessaggioFineOk = COLLAUDO_PIASTRA_OK
        MessaggioFineKo = COLLAUDO_PIASTRA_KO
        sss = Application.StartupPath + LOCALDBPIASTRE + StrSeqPiastre

        EseguicollaudoPiastre(sss, "L75")
    End Sub

    Private Sub PiastraLs515ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PiastraLs515ToolStripMenuItem.Click

    End Sub

    Private Sub Ls150UVToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim v() As Integer = {2, 4, 8, 12, 14, 18, 52, 51} 'vettore opzioni
        Dim sss As String

        vettorepiastra = v
        StrSeqPiastre = "\Seq_PiastreLS150UVGL.xml"
        MessaggioFineOk = COLLAUDO_PIASTRA_OK
        MessaggioFineKo = COLLAUDO_PIASTRA_KO
        sss = Application.StartupPath + LOCALDBPIASTRE + StrSeqPiastre

        EseguicollaudoPiastre(sss, "Ls150")
    End Sub

    Private Sub Ls150ColoriToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim v() As Integer = {2, 4, 8, 12, 14, 18, 52, 47} 'vettore opzioni
        Dim sss As String

        vettorepiastra = v
        StrSeqPiastre = "\Seq_PiastreLS150c.xml"
        MessaggioFineOk = COLLAUDO_PIASTRA_OK
        MessaggioFineKo = COLLAUDO_PIASTRA_KO
        sss = Application.StartupPath + LOCALDBPIASTRE + StrSeqPiastre

        EseguicollaudoPiastre(sss, "Ls150")
    End Sub

    Private Sub Ls150GrigiToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Ls150ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ls150ToolStripMenuItem.Click
        Dim fbra As FBraSile
        fbra = New FBraSile()
        fbra.StrPath = Application.StartupPath + LOCALDBBRASIL + "\Ls150"
        fbra.StrNome = "Ls150"

        If (System.IO.Directory.Exists(fbra.StrPath) = False) Then
            MessageBox.Show("Attenzione Manca la cartella LS150 ", "collaudo non possibile", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            If (System.IO.Directory.Exists(Application.StartupPath + LOCALDBBRASIL + "\Ls150_FW") = False) Then
                MessageBox.Show("Attenzione Manca la cartella LS150_FW ", "collaudo non possibile", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else


                If (fbra.ShowDialog(Me.ParentForm) = Windows.Forms.DialogResult.OK) Then
                    MessaggioFineOk = COLLAUDO_PERIFERICA_OK_BRA
                    MessaggioFineKo = COLLAUDO_PERIFERICA_KO_BRA

                    LNomeLinea.Text = fbra.CFLinea
                    LNomep.Text = fbra.CFModello
                    'L3.Text = fbra.CFOperatore
                    LOperatore.Text = ""
                    LNordine.Text = fbra.CFOrdine
                    LAzienda.Text = fbra.CFProduttore

                    vettorepiastra = fbra.v
                    Lfw1.Text = ""
                    Lfw2.Text = ""
                    Lfw3.Text = ""
                    Lfw4.Text = ""
                    Lfw5.Text = ""
                    Lfw6.Text = ""
                    Lfw7.Text = ""
                    Lfw8.Text = ""
                    Lfw9.Text = ""

                    Select Case fbra.SequenzeFW.Count() - 1
                        Case 1
                            Lfw1.Text = fbra.SequenzeFW(1)
                        Case 2
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                        Case 3
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                            Lfw3.Text = fbra.SequenzeFW(3)
                        Case 4
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                            Lfw3.Text = fbra.SequenzeFW(3)
                            Lfw4.Text = fbra.SequenzeFW(4)
                        Case 5
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                            Lfw3.Text = fbra.SequenzeFW(3)
                            Lfw4.Text = fbra.SequenzeFW(4)
                            Lfw5.Text = fbra.SequenzeFW(5)
                        Case 6
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                            Lfw3.Text = fbra.SequenzeFW(3)
                            Lfw4.Text = fbra.SequenzeFW(4)
                            Lfw5.Text = fbra.SequenzeFW(5)
                            Lfw6.Text = fbra.SequenzeFW(6)
                        Case 7
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                            Lfw3.Text = fbra.SequenzeFW(3)
                            Lfw4.Text = fbra.SequenzeFW(4)
                            Lfw5.Text = fbra.SequenzeFW(5)
                            Lfw6.Text = fbra.SequenzeFW(6)
                            Lfw7.Text = fbra.SequenzeFW(7)
                        Case 8
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                            Lfw3.Text = fbra.SequenzeFW(3)
                            Lfw4.Text = fbra.SequenzeFW(4)
                            Lfw5.Text = fbra.SequenzeFW(5)
                            Lfw6.Text = fbra.SequenzeFW(6)
                            Lfw7.Text = fbra.SequenzeFW(7)
                            Lfw8.Text = fbra.SequenzeFW(8)

                        Case 9
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                            Lfw3.Text = fbra.SequenzeFW(3)
                            Lfw4.Text = fbra.SequenzeFW(4)
                            Lfw5.Text = fbra.SequenzeFW(5)
                            Lfw6.Text = fbra.SequenzeFW(6)
                            Lfw7.Text = fbra.SequenzeFW(7)
                            Lfw8.Text = fbra.SequenzeFW(8)
                            Lfw9.Text = fbra.SequenzeFW(9)

                    End Select



                    EseguicollaudoPiastre(fbra.StrPath + "\" + fbra.FileSelezionato + ".xml", "Ls150")

                End If

            End If

        End If

    End Sub

    Private Sub FillBymaxToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Me.T_DatiFineCollaudoTableAdapter.FillBymax(Me.Collaudi02DataSet.T_DatiFineCollaudo)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Ls150JPMToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim v() As Integer = {2, 4, 8, 12, 14, 18, 52, 70, 47} 'vettore opzioni
        Dim sss As String

        vettorepiastra = v

        StrSeqPiastre = "\Seq_PiastreLS150JPM.xml"
        MessaggioFineOk = COLLAUDO_PIASTRA_OK
        MessaggioFineKo = COLLAUDO_PIASTRA_KO
        sss = Application.StartupPath + LOCALDBPIASTRE + StrSeqPiastre

        EseguicollaudoPiastre(sss, "Ls150")
    End Sub

    Private Sub MMemu_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles MMemu.ItemClicked

    End Sub

    Private Sub PistreLs1007ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PistreLs1007ToolStripMenuItem.Click
        Dim v() As Integer = {2, 4, 8, 12, 18, 30} 'vettore opzioni
        Dim sss As String

        vettorepiastra = v
        StrSeqPiastre = "\Seq_PiastreLS100.xml"
        MessaggioFineOk = COLLAUDO_PIASTRA_OK
        MessaggioFineKo = COLLAUDO_PIASTRA_KO
        sss = Application.StartupPath + LOCALDBPIASTRE + StrSeqPiastre

        EseguicollaudoPiastre(sss, "Ls100")
    End Sub

    Private Sub LettoreEScannerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LettoreEScannerToolStripMenuItem.Click
        Dim v() As Integer = {2, 4, 8, 12, 18, 47} 'vettore opzioni
        Dim sss As String

        vettorepiastra = v
        StrSeqPiastre = "\Seq_vetrini.xml"
        MessaggioFineOk = SEQ_SCANNER_OK
        MessaggioFineKo = SEQ_SCANNER_KO
        sss = Application.StartupPath + LOCALDBPIASTRE + StrSeqPiastre

        EseguicollaudoPiastre(sss, "Ls150")
    End Sub

    Private Sub T_OrdineBindingSource_CurrentChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles T_OrdineBindingSource.CurrentChanged

    End Sub

    Private Sub ToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim v() As Integer = {2, 4, 8, 12, 14, 18, 52, 51} 'vettore opzioni
        Dim sss As String

        vettorepiastra = v
        StrSeqPiastre = "\Seq_PiastreLS150UV.xml"
        MessaggioFineOk = COLLAUDO_PIASTRA_OK
        MessaggioFineKo = COLLAUDO_PIASTRA_KO
        sss = Application.StartupPath + LOCALDBPIASTRE + StrSeqPiastre

        EseguicollaudoPiastre(sss, "Ls150")
    End Sub

    Private Sub FaseBar2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FaseBar2.Load

    End Sub

    Private Sub PiastreLs150ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PiastreLs150ToolStripMenuItem.Click
        Dim v() As Integer = {2, 4, 8, 12, 14, 18, 52, 47} 'vettore opzioni
        Dim sss As String

        vettorepiastra = v
        StrSeqPiastre = "\Seq_PiastreLS150.xml"
        MessaggioFineOk = COLLAUDO_PIASTRA_OK
        MessaggioFineKo = COLLAUDO_PIASTRA_KO
        sss = Application.StartupPath + LOCALDBPIASTRE + StrSeqPiastre

        'le Directory non vengono create sul Destkop , problemi di permessi
        Try
            If IO.Directory.Exists(Application.StartupPath + "\Dati") Then
                'IO.Directory.Delete(sss + "\Dati", True)
                SvuotaDir(Application.StartupPath + "\Dati")
            Else
                IO.Directory.CreateDirectory(Application.StartupPath + "\Dati")
            End If
        Catch ex As Exception
            MessageBox.Show("Directory Dati NON creata....", ex.Message + " ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            IO.Directory.Delete(Application.StartupPath + "\Dati", True)
            Me.Close()
        End Try

        Try

            If IO.Directory.Exists(Application.StartupPath + "\Quality") Then
                SvuotaDir(Application.StartupPath + "\Quality")
                'IO.Directory.Delete(sss + "\Quality", True)
                Console.WriteLine("Cartella Quality gia' presente")
            Else
                IO.Directory.CreateDirectory(Application.StartupPath + "\Quality")
            End If

        Catch ex As Exception
            MessageBox.Show("Directory Quality NON creata.....Chiudere e Riaprire il programma", ex.Message + " ", MessageBoxButtons.OK, MessageBoxIcon.Error)
            IO.Directory.Delete(sss + "\Quality", True)
            Me.Close()
        End Try


        EseguicollaudoPiastre(sss, "Ls150")
    End Sub

    Private Sub Ls515SeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ls515SeToolStripMenuItem.Click
        Dim v() As Integer = {2, 4, 8, 12, 14, 18, 52, 57} 'vettore opzioni
        Dim sss As String

        vettorepiastra = v
        StrSeqPiastre = "\Seq_PiastreLS515.xml"
        MessaggioFineOk = COLLAUDO_PIASTRA_OK
        MessaggioFineKo = COLLAUDO_PIASTRA_KO
        sss = Application.StartupPath + LOCALDBPIASTRE + StrSeqPiastre

        EseguicollaudoPiastre(sss, "Ls515")
    End Sub

    Private Sub Ls515UVToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ls515UVToolStripMenuItem.Click
        'ultimo parametro e' l'ID_PERIFERICA
        Dim v() As Integer = {2, 4, 8, 12, 14, 18, 52, 75} 'vettore opzioni
        Dim sss As String

        vettorepiastra = v
        StrSeqPiastre = "\Seq_PiastreLS515UV.xml"
        MessaggioFineOk = COLLAUDO_PIASTRA_OK
        MessaggioFineKo = COLLAUDO_PIASTRA_KO
        sss = Application.StartupPath + LOCALDBPIASTRE + StrSeqPiastre

        EseguicollaudoPiastre(sss, "Ls515")
    End Sub

    Private Sub Ls40UVCOLOREToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ls40UVCOLOREToolStripMenuItem.Click
        Dim fbra As FBraSile
        fbra = New FBraSile()
        fbra.StrPath = Application.StartupPath + LOCALDBBRASIL + "\Ls40UVCOLORE"
        fbra.StrNome = "Ls40UVCOLORE"

        If (System.IO.Directory.Exists(fbra.StrPath) = False) Then
            MessageBox.Show("Attenzione Manca la cartella LS40 UV-COLORE", "collaudo non possibile", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            If (System.IO.Directory.Exists(Application.StartupPath + LOCALDBBRASIL + "\Ls40UVCOLORE_FW") = False) Then
                MessageBox.Show("Attenzione Manca la cartella LS40_FW UVCOLORE", "collaudo non possibile", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else


                If (fbra.ShowDialog(Me.ParentForm) = Windows.Forms.DialogResult.OK) Then
                    MessaggioFineOk = COLLAUDO_PERIFERICA_OK_BRA
                    MessaggioFineKo = COLLAUDO_PERIFERICA_KO_BRA

                    LNomeLinea.Text = fbra.CFLinea
                    LNomep.Text = fbra.CFModello
                    'LOperatore.Text = fbra.CFOperatore
                    LOperatore.Text = ""
                    LNordine.Text = fbra.CFOrdine
                    LAzienda.Text = fbra.CFProduttore

                    vettorepiastra = fbra.v
                    Lfw1.Text = ""
                    Lfw2.Text = ""
                    Lfw3.Text = ""
                    Lfw4.Text = ""
                    Lfw5.Text = ""
                    Lfw6.Text = ""
                    Lfw7.Text = ""
                    Lfw8.Text = ""
                    Lfw9.Text = ""

                    Select Case fbra.SequenzeFW.Count() - 1
                        Case 1
                            Lfw1.Text = fbra.SequenzeFW(1)
                        Case 2
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                        Case 3
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                            Lfw3.Text = fbra.SequenzeFW(3)
                        Case 4
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                            Lfw3.Text = fbra.SequenzeFW(3)
                            Lfw4.Text = fbra.SequenzeFW(4)
                        Case 5
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                            Lfw3.Text = fbra.SequenzeFW(3)
                            Lfw4.Text = fbra.SequenzeFW(4)
                            Lfw5.Text = fbra.SequenzeFW(5)
                        Case 6
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                            Lfw3.Text = fbra.SequenzeFW(3)
                            Lfw4.Text = fbra.SequenzeFW(4)
                            Lfw5.Text = fbra.SequenzeFW(5)
                            Lfw6.Text = fbra.SequenzeFW(6)
                        Case 7
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                            Lfw3.Text = fbra.SequenzeFW(3)
                            Lfw4.Text = fbra.SequenzeFW(4)
                            Lfw5.Text = fbra.SequenzeFW(5)
                            Lfw6.Text = fbra.SequenzeFW(6)
                            Lfw7.Text = fbra.SequenzeFW(7)
                        Case 8
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                            Lfw3.Text = fbra.SequenzeFW(3)
                            Lfw4.Text = fbra.SequenzeFW(4)
                            Lfw5.Text = fbra.SequenzeFW(5)
                            Lfw6.Text = fbra.SequenzeFW(6)
                            Lfw7.Text = fbra.SequenzeFW(7)
                            Lfw8.Text = fbra.SequenzeFW(8)

                        Case 9
                            Lfw1.Text = fbra.SequenzeFW(1)
                            Lfw2.Text = fbra.SequenzeFW(2)
                            Lfw3.Text = fbra.SequenzeFW(3)
                            Lfw4.Text = fbra.SequenzeFW(4)
                            Lfw5.Text = fbra.SequenzeFW(5)
                            Lfw6.Text = fbra.SequenzeFW(6)
                            Lfw7.Text = fbra.SequenzeFW(7)
                            Lfw8.Text = fbra.SequenzeFW(8)
                            Lfw9.Text = fbra.SequenzeFW(9)

                    End Select



                    EseguicollaudoPiastre(fbra.StrPath + "\" + fbra.FileSelezionato + ".xml", "Ls40")

                End If

            End If

        End If

    End Sub

    Private Sub PiastreCsExtraToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim v() As Integer = {76} 'vettore opzioni
        Dim sss As String

        vettorepiastra = v
        StrSeqPiastre = "\Seq_PiastreCsExtra.xml"
        MessaggioFineOk = COLLAUDO_PIASTRA_OK
        MessaggioFineKo = COLLAUDO_PIASTRA_KO
        sss = Application.StartupPath + LOCALDBPIASTRE + StrSeqPiastre

        EseguicollaudoPiastre(sss, "CsExtra")
    End Sub

    Private Sub Ls40GrigioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ls40GrigioToolStripMenuItem.Click
        Dim v() As Integer = {8, 12, 14, 18, 52, 48}
        'N_B  : queste sono le opzioni che vengono settate di default nel Collaudo Piastre
        '8 : LETTURA CMC7 + E13B
        '12 :SCANNER FRONTE + RETRO
        '14 :TIMBRO FRONTE
        '18 :INK JET
        '48 : Hub serve ? 
        '52 :Badge Traccia 1/2/3
        '22-06-2015
        'aggiunta calibrazione Sensore Double Leafing .......
        'NB : 62 grigio 63 colori 64 grigio+UV 75 colore+UV
        


        Dim sss As String

        vettorepiastra = v
        StrSeqPiastre = "\Seq_PiastreLS40.xml"
        MessaggioFineOk = COLLAUDO_PIASTRA_OK
        MessaggioFineKo = COLLAUDO_PIASTRA_KO
        sss = Application.StartupPath + LOCALDBPIASTRE + StrSeqPiastre
        EseguicollaudoPiastre(sss, "Ls40")
    End Sub

    Private Sub Ls40UvCOLOToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ls40UvCOLOToolStripMenuItem.Click
        Dim v() As Integer = {8, 12, 14, 18, 52, 64, 80}
        'Dim v() As Integer = {8, 12, 14, 18, 52, 63, 80} 'TEST INTERNI
        'N_B  : queste sono le opzioni che vengono settate di default nel Collaudo Piastre
        '8 : LETTURA CMC7 + E13B
        '12 :SCANNER FRONTE + RETRO
        '14 :TIMBRO FRONTE
        '18 :INK JET
        '48 : Hub serve ? 
        '52 :Badge Traccia 1/2/3
        '22-06-2015
        'aggiunta calibrazione Sensore Double Leafing .......
        '64 setting scanner UV + GRIGIO

        'NB : 62 grigio 63 colori 64 grigio+UV 75 colore+UV 
        Dim sss As String

        vettorepiastra = v
        StrSeqPiastre = "\Seq_PiastreLS40UVCOLORE.xml"
        MessaggioFineOk = COLLAUDO_PIASTRA_OK
        MessaggioFineKo = COLLAUDO_PIASTRA_KO
        sss = Application.StartupPath + LOCALDBPIASTRE + StrSeqPiastre

        '23/01/2017 le immagini vengono salvate su server e non piu' dentro il DataBase
        Try
            'Save Report File on Server
            ' Loop through each file in the directory
            For Each fileD As IO.FileInfo In New IO.DirectoryInfo(Application.StartupPath() + "\Images_Scanner").GetFiles

                If fileD.Name <> "Thumbs.db" Then
                    ' copy resource to users local directory
                    'If (File.Exists(SaveLogPath & " \ " & fileD.Name) = False) Then
                    'fileD.CopyTo(SaveLogPath & " \ " & fileD.Name)
                    '
                    'fileD.CopyTo(Application.StartupPath() & "\Images_Scanner\old\" & fileD.Name)
                    '
                    File.Delete(Application.StartupPath() + "\Images_Scanner\" & fileD.Name)
                    'Else
                    '    Console.WriteLine("FileGiaPresentesulServer")
                    'End If

                End If
            Next
        Catch ex As Exception
            'verranno salvati al prossimo collaudo effettuato ...
        End Try

        EseguicollaudoPiastre(sss, "Ls40")
    End Sub

    Private Sub Form1_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

    End Sub

    Private Sub FaseBar1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FaseBar1.Load

    End Sub

    Private Sub TestToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TestToolStripMenuItem1.Click
        'ClassePeriferica = OggettiPerif(4).GetType
        'MetodiClasse = ClassePeriferica.GetMethods()
        Dim myProcess As New Process()
        myProcess.StartInfo.FileName = Application.StartupPath & "\SmartCardReader\SmartPCSCDiag.exe"
        myProcess.StartInfo.WorkingDirectory = Application.StartupPath & "\SmartCardReader"
        'dres = MessageboxMy("Premere OK per avviare il sw SmartPCSCDiag e verificare il funzionamento della Smart Card", "TEST SMART CARD", MessageBoxButtons.OK, MessageBoxIcon.Information, Nothing)
        myProcess.Start()
        myProcess.WaitForExit()
    End Sub
End Class
