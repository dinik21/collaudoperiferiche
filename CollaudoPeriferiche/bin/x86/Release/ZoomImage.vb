Imports System.Windows.Forms
Imports System.Drawing
Public Class ZoomImage
    Inherits System.Windows.Forms.Form
    Public DirectoryImage As String
    Public FileCorrente As String
    Public Result As Windows.Forms.DialogResult
    Private Files(1000) As String
    Dim ii As Short
    Friend WithEvents picture As System.Windows.Forms.PictureBox
    Dim nFile As Short
    Dim bbool As Boolean
    Dim conta As Integer = 1
    Dim bytez As Byte
    Dim Array1(20) As Byte


    Dim trovato As Boolean = False
    Private currentImage As Image = Nothing
    Private zoomFactor As Double = 1
    Dim xi, yi, xf, yf, difx, dify, difxold, difyold As Short

#Region " Codice generato da Progettazione Windows Form "

    Public Sub New()
        MyBase.New()

        'Chiamata richiesta da Progettazione Windows Form.
        InitializeComponent()

        'Aggiungere le eventuali istruzioni di inizializzazione dopo la chiamata a InitializeComponent()

    End Sub

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue � richiesta da Progettazione Windows Form.
    'Pu� essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.


    Friend WithEvents BEsci As System.Windows.Forms.Button

    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button


    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.BEsci = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.picture = New System.Windows.Forms.PictureBox
        CType(Me.picture, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BEsci
        '
        Me.BEsci.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BEsci.ForeColor = System.Drawing.Color.Blue
        Me.BEsci.Location = New System.Drawing.Point(632, 7)
        Me.BEsci.Name = "BEsci"
        Me.BEsci.Size = New System.Drawing.Size(88, 24)
        Me.BEsci.TabIndex = 2
        Me.BEsci.Text = "Esci"
        Me.BEsci.Visible = False
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button1.BackColor = System.Drawing.Color.Lime
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(645, 804)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(112, 80)
        Me.Button1.TabIndex = 9
        Me.Button1.Text = "Qualita' OK"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button2.BackColor = System.Drawing.Color.Red
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(517, 804)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(112, 80)
        Me.Button2.TabIndex = 10
        Me.Button2.Text = "Qualita' KO"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'picture
        '
        Me.picture.Location = New System.Drawing.Point(32, 56)
        Me.picture.Name = "picture"
        Me.picture.Size = New System.Drawing.Size(1230, 830)
        Me.picture.TabIndex = 11
        Me.picture.TabStop = False
        '
        'ZoomImage
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(1274, 887)
        Me.ControlBox = False
        Me.Controls.Add(Me.picture)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.BEsci)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.Name = "ZoomImage"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "ZoomImage"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.picture, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Public Function Start() As Integer
        'MessageBox.Show("ok")
        Dim ret As Integer
        Dim passidafare As Integer
        passidafare = 0

        If (picture.Image.Size.Height > picture.Size.Height) Then
            passidafare += 2
        End If

        If (picture.Image.Size.Width > picture.Size.Width) Then
            passidafare += 2
        End If
        If conta >= passidafare Then
            ret = 1
        Else
            ret = 0
            bbool = True

        End If

        If (picture.Image.Size.Height < picture.Size.Height) And _
          (picture.Image.Size.Width > picture.Size.Width) And (conta = 1) Then
            conta = 2
        End If

        Select Case conta
            Case 1
                dify = -(picture.Image.Size.Height / 2)
                difyold = dify
            Case 2
                difx = -(picture.Image.Size.Width / 2)
                dify = 0
            Case 3
                dify = -(picture.Image.Size.Height / 2)
                difyold = dify

                difx = -(picture.Image.Size.Width / 2)
            Case 4

                '    difyold = difx
        End Select
        conta += 1



        'If (picture.Image.Size.Height < picture.Size.Height) Then
        '    ret = 1
        'Else
        '    If dify < 0 Then

        '        ret = 1
        '    Else
        '        ret = 0
        '    End If

        '    dify = -(picture.Image.Size.Height / 2)
        '    difyold = dify
        '    'ddy = yf - yi + difyold

        '    ' difx += MainImage.Size.Width
        '    ' difxold += MainImage.Size.Width
        '    bbool = True
        '    picture.Refresh()
        'End If

        'If (picture.Image.Size.Width < picture.Size.Width) Then
        '    ret = 1
        'Else
        '    If difx < 0 Then
        '        ret = 1
        '    Else
        '        ret = 0
        '    End If

        '    difx = -(picture.Image.Size.Width / 2)
        '    difyold = difx
        '    'ddy = yf - yi + difyold

        '    ' difx += MainImage.Size.Width
        '    ' difxold += MainImage.Size.Width
        '    bbool = True
        '    picture.Refresh()
        'End If
        picture.Refresh()
        Return ret
    End Function
    Public Sub Zoom()

        ' Save the factor in global variable.
        If bbool Then
            zoomFactor = 1

            ' Get the resized image.
            'Dim sourceBitmap As New Bitmap(currentImage)
           
            Dim destBitmap As New Bitmap(CInt(currentImage.Width * zoomFactor), _
            CInt(currentImage.Height * zoomFactor))

            Dim destGraphic As Graphics = Graphics.FromImage(destBitmap)

            destGraphic.DrawImage(currentImage, difx, dify, destBitmap.Width + 1, _
                destBitmap.Height + 1)

            ' Save the size of the image on the screen in globals.


            picture.Image = destBitmap


            bbool = False
        End If

        'Preview.Refresh()




    End Sub

    Private Sub ZoomImage_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ' setto la size del controllo in automatico

        'Dim s As New Size(Me.Width, ZoomImage.ActiveForm.Height * 0.8)
        Dim s As New Drawing.Size(Me.Width, Me.Height)
        'UserControl12.Size = s

        ' mi creo un vettore con tutti i nomi dei file delle immagini 



        difxold = 0
        difyold = 0
        Dim img As Image = Image.FromFile(FileCorrente)
        currentImage = img
        picture.Image = img




        ii = nFile
        Button1.Focus()


    End Sub

    Private Sub BEsci_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BEsci.Click
        If currentImage Is Nothing Then
        Else
            currentImage.Dispose()
            currentImage = Nothing
        End If

        If picture.Image Is Nothing Then
        Else
            picture.Image.Dispose()
            picture.Image = Nothing
        End If
        Me.Close()
    End Sub

    Private Sub Bnext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If (ii < nFile) Then
            ii += 1
        End If


        'Lfile.Text = Files(ii)
        'Lii.Text = ii.ToString + 1
    End Sub

    Private Sub BPrecedente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If (ii >= 1) Then
            ii -= 1
        End If


        'Lfile.Text = Files(ii)
        'Lii.Text = ii.ToString + 1
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim retf As Integer
        retf = Start()
        If retf Then
            Result = Windows.Forms.DialogResult.OK
            If picture.Image Is Nothing Then
            Else
                picture.Image.Dispose()
                picture.Image = Nothing
            End If
            Me.Close()
        Else

        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Result = Windows.Forms.DialogResult.Abort
        If picture.Image Is Nothing Then
        Else
            picture.Image.Dispose()
            picture.Image = Nothing
        End If
        Me.Close()
    End Sub

    Private Sub ZoomImage_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Select Case (e.KeyValue)
            Case Windows.Forms.Keys.Back
                Button2_Click(sender, e)
            Case Windows.Forms.Keys.Escape
                Button2_Click(sender, e)
        End Select
    End Sub


    Private Sub picture_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles picture.Paint
        If picture.Image Is Nothing Then
        Else
            Zoom()
        End If
    End Sub

    Private Sub picture_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles picture.Click

    End Sub
End Class
