﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FStazioni
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Nome_stazioneLabel As System.Windows.Forms.Label
        Dim Nome_lineaLabel As System.Windows.Forms.Label
        Dim AziendaLabel As System.Windows.Forms.Label
        Dim IndirizzoIPLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FStazioni))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.UscitaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.BStOK = New System.Windows.Forms.Button
        Me.Collaudi02DataSet = New CollaudoPeriferiche.Collaudi02DataSet
        Me.T_Staz_CollBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.T_Staz_CollTableAdapter = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_Staz_CollTableAdapter
        Me.TableAdapterManager = New CollaudoPeriferiche.Collaudi02DataSetTableAdapters.TableAdapterManager
        Me.T_Staz_CollBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.Nome_stazioneTextBox = New System.Windows.Forms.TextBox
        Me.Nome_lineaTextBox = New System.Windows.Forms.TextBox
        Me.AziendaTextBox = New System.Windows.Forms.TextBox
        Me.IndirizzoIPTextBox = New System.Windows.Forms.TextBox
        Nome_stazioneLabel = New System.Windows.Forms.Label
        Nome_lineaLabel = New System.Windows.Forms.Label
        AziendaLabel = New System.Windows.Forms.Label
        IndirizzoIPLabel = New System.Windows.Forms.Label
        Me.MenuStrip1.SuspendLayout()
        CType(Me.Collaudi02DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.T_Staz_CollBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.T_Staz_CollBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.T_Staz_CollBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'Nome_stazioneLabel
        '
        Nome_stazioneLabel.AutoSize = True
        Nome_stazioneLabel.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Nome_stazioneLabel.Location = New System.Drawing.Point(66, 94)
        Nome_stazioneLabel.Name = "Nome_stazioneLabel"
        Nome_stazioneLabel.Size = New System.Drawing.Size(80, 13)
        Nome_stazioneLabel.TabIndex = 11
        Nome_stazioneLabel.Text = "Nome stazione:"
        '
        'Nome_lineaLabel
        '
        Nome_lineaLabel.AutoSize = True
        Nome_lineaLabel.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Nome_lineaLabel.Location = New System.Drawing.Point(83, 131)
        Nome_lineaLabel.Name = "Nome_lineaLabel"
        Nome_lineaLabel.Size = New System.Drawing.Size(63, 13)
        Nome_lineaLabel.TabIndex = 12
        Nome_lineaLabel.Text = "Nome linea:"
        '
        'AziendaLabel
        '
        AziendaLabel.AutoSize = True
        AziendaLabel.ForeColor = System.Drawing.SystemColors.ActiveCaption
        AziendaLabel.Location = New System.Drawing.Point(99, 170)
        AziendaLabel.Name = "AziendaLabel"
        AziendaLabel.Size = New System.Drawing.Size(48, 13)
        AziendaLabel.TabIndex = 13
        AziendaLabel.Text = "Azienda:"
        '
        'IndirizzoIPLabel
        '
        IndirizzoIPLabel.AutoSize = True
        IndirizzoIPLabel.Location = New System.Drawing.Point(86, 211)
        IndirizzoIPLabel.Name = "IndirizzoIPLabel"
        IndirizzoIPLabel.Size = New System.Drawing.Size(61, 13)
        IndirizzoIPLabel.TabIndex = 14
        IndirizzoIPLabel.Text = "Indirizzo IP:"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UscitaToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(408, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'UscitaToolStripMenuItem
        '
        Me.UscitaToolStripMenuItem.Name = "UscitaToolStripMenuItem"
        Me.UscitaToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F12
        Me.UscitaToolStripMenuItem.Size = New System.Drawing.Size(48, 20)
        Me.UscitaToolStripMenuItem.Text = "Uscita"
        '
        'BStOK
        '
        Me.BStOK.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.BStOK.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BStOK.Location = New System.Drawing.Point(167, 270)
        Me.BStOK.Name = "BStOK"
        Me.BStOK.Size = New System.Drawing.Size(75, 53)
        Me.BStOK.TabIndex = 10
        Me.BStOK.Text = "OK"
        Me.BStOK.UseVisualStyleBackColor = False
        '
        'Collaudi02DataSet
        '
        Me.Collaudi02DataSet.DataSetName = "Collaudi02DataSet"
        Me.Collaudi02DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'T_Staz_CollBindingSource
        '
        Me.T_Staz_CollBindingSource.DataMember = "T_Staz_Coll"
        Me.T_Staz_CollBindingSource.DataSource = Me.Collaudi02DataSet
        '
        'T_Staz_CollTableAdapter
        '
        Me.T_Staz_CollTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AutenticazioneTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.CategorieOpzioniTableAdapter = Nothing
        Me.TableAdapterManager.DettaglioOrdineTableAdapter = Nothing
        Me.TableAdapterManager.OperatoriTableAdapter = Nothing
        Me.TableAdapterManager.SequenzaCollaudoTableAdapter = Nothing
        Me.TableAdapterManager.SequenzaTableAdapter = Nothing
        Me.TableAdapterManager.SequOperaTableAdapter = Nothing
        Me.TableAdapterManager.T_Coll_FaseTableAdapter = Nothing
        Me.TableAdapterManager.T_CollaudoTableAdapter = Nothing
        Me.TableAdapterManager.T_FasicollaudoTableAdapter = Nothing
        Me.TableAdapterManager.T_FirmwareTableAdapter = Nothing
        Me.TableAdapterManager.T_ImmaginiTableAdapter = Nothing
        Me.TableAdapterManager.T_Mess_PerifTableAdapter = Nothing
        Me.TableAdapterManager.T_MessaggiTableAdapter = Nothing
        Me.TableAdapterManager.T_OrdineTableAdapter = Nothing
        Me.TableAdapterManager.T_PerifericaTableAdapter = Nothing
        Me.TableAdapterManager.T_Staz_CollTableAdapter = Me.T_Staz_CollTableAdapter
        Me.TableAdapterManager.T_StoricoTableAdapter = Nothing
        Me.TableAdapterManager.T_VersioniTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = CollaudoPeriferiche.Collaudi02DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        Me.TableAdapterManager.ValoriOpzioniTableAdapter = Nothing
        '
        'T_Staz_CollBindingNavigator
        '
        Me.T_Staz_CollBindingNavigator.AddNewItem = Nothing
        Me.T_Staz_CollBindingNavigator.BindingSource = Me.T_Staz_CollBindingSource
        Me.T_Staz_CollBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.T_Staz_CollBindingNavigator.DeleteItem = Nothing
        Me.T_Staz_CollBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2})
        Me.T_Staz_CollBindingNavigator.Location = New System.Drawing.Point(0, 24)
        Me.T_Staz_CollBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.T_Staz_CollBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.T_Staz_CollBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.T_Staz_CollBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.T_Staz_CollBindingNavigator.Name = "T_Staz_CollBindingNavigator"
        Me.T_Staz_CollBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.T_Staz_CollBindingNavigator.Size = New System.Drawing.Size(408, 25)
        Me.T_Staz_CollBindingNavigator.TabIndex = 11
        Me.T_Staz_CollBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(34, 22)
        Me.BindingNavigatorCountItem.Text = "di {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Numero totale di elementi"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Sposta in prima posizione"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Sposta indietro"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Posizione"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 21)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Posizione corrente"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Sposta avanti"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Sposta in ultima posizione"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'Nome_stazioneTextBox
        '
        Me.Nome_stazioneTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_Staz_CollBindingSource, "Nome_stazione", True))
        Me.Nome_stazioneTextBox.Location = New System.Drawing.Point(152, 91)
        Me.Nome_stazioneTextBox.Name = "Nome_stazioneTextBox"
        Me.Nome_stazioneTextBox.Size = New System.Drawing.Size(190, 20)
        Me.Nome_stazioneTextBox.TabIndex = 12
        '
        'Nome_lineaTextBox
        '
        Me.Nome_lineaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_Staz_CollBindingSource, "Nome_linea", True))
        Me.Nome_lineaTextBox.Location = New System.Drawing.Point(152, 128)
        Me.Nome_lineaTextBox.Name = "Nome_lineaTextBox"
        Me.Nome_lineaTextBox.Size = New System.Drawing.Size(190, 20)
        Me.Nome_lineaTextBox.TabIndex = 13
        '
        'AziendaTextBox
        '
        Me.AziendaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_Staz_CollBindingSource, "Azienda", True))
        Me.AziendaTextBox.Location = New System.Drawing.Point(153, 167)
        Me.AziendaTextBox.Name = "AziendaTextBox"
        Me.AziendaTextBox.Size = New System.Drawing.Size(190, 20)
        Me.AziendaTextBox.TabIndex = 14
        '
        'IndirizzoIPTextBox
        '
        Me.IndirizzoIPTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.T_Staz_CollBindingSource, "IndirizzoIP", True))
        Me.IndirizzoIPTextBox.Location = New System.Drawing.Point(153, 208)
        Me.IndirizzoIPTextBox.Name = "IndirizzoIPTextBox"
        Me.IndirizzoIPTextBox.Size = New System.Drawing.Size(190, 20)
        Me.IndirizzoIPTextBox.TabIndex = 15
        '
        'FStazioni
        '
        Me.AcceptButton = Me.BStOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(408, 343)
        Me.ControlBox = False
        Me.Controls.Add(IndirizzoIPLabel)
        Me.Controls.Add(Me.IndirizzoIPTextBox)
        Me.Controls.Add(AziendaLabel)
        Me.Controls.Add(Me.AziendaTextBox)
        Me.Controls.Add(Nome_lineaLabel)
        Me.Controls.Add(Me.Nome_lineaTextBox)
        Me.Controls.Add(Nome_stazioneLabel)
        Me.Controls.Add(Me.Nome_stazioneTextBox)
        Me.Controls.Add(Me.T_Staz_CollBindingNavigator)
        Me.Controls.Add(Me.BStOK)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FStazioni"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Stazioni di Collaudo"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.Collaudi02DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.T_Staz_CollBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.T_Staz_CollBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.T_Staz_CollBindingNavigator.ResumeLayout(False)
        Me.T_Staz_CollBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents UscitaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BStOK As System.Windows.Forms.Button
    Friend WithEvents Collaudi02DataSet As CollaudoPeriferiche.Collaudi02DataSet
    Friend WithEvents T_Staz_CollBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents T_Staz_CollTableAdapter As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.T_Staz_CollTableAdapter
    Friend WithEvents TableAdapterManager As CollaudoPeriferiche.Collaudi02DataSetTableAdapters.TableAdapterManager
    Friend WithEvents T_Staz_CollBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Nome_stazioneTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Nome_lineaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents AziendaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents IndirizzoIPTextBox As System.Windows.Forms.TextBox
End Class
