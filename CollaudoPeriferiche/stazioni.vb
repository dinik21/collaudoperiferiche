﻿Public Class FStazioni
    Public StazioneSelezionata As DataRowView

    Private Sub UscitaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UscitaToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub T_Staz_CollBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.T_Staz_CollBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.Collaudi02DataSet)

    End Sub

    Private Sub FStazioni_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Select Case (e.KeyValue)
            Case Keys.Right
                T_Staz_CollBindingSource.MoveNext()
            Case Keys.Left
                T_Staz_CollBindingSource.MovePrevious()
        End Select
    End Sub

    Private Sub FStazioni_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: questa riga di codice carica i dati nella tabella 'Collaudi02DataSet.T_Staz_Coll'. È possibile spostarla o rimuoverla se necessario.


        If Form1.fServerPresente = True Then
            'TODO: questa riga di codice carica i dati nella tabella 'CollaudiDataSet.T_Staz_Coll'. È possibile spostarla o rimuoverla se necessario.
            Me.T_Staz_CollTableAdapter.Fill(Me.Collaudi02DataSet.T_Staz_Coll)
            Dim r As Integer = Me.T_Staz_CollBindingSource.Find("Id_Stazione", My.Settings.ID_Stazione)
            'Me.BindingNavigatorPositionItem.Text = My.Settings.ID_Stazione
            Me.T_Staz_CollBindingSource.Position = r


        Else
            Collaudi02DataSet.T_Staz_Coll.Clear()
            Collaudi02DataSet.T_Staz_Coll.ReadXml(Application.StartupPath + Form1.LOCALDB + Form1.T_STAZ_COLL)
            Dim r As Integer = Me.T_Staz_CollBindingSource.Find("Id_Stazione", My.Settings.ID_Stazione)
            'Me.BindingNavigatorPositionItem.Text = My.Settings.ID_Stazione
            Me.T_Staz_CollBindingSource.Position = r

        End If
    End Sub

    Private Sub BStOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BStOK.Click
        StazioneSelezionata = Me.T_Staz_CollBindingSource.Current

        My.Settings.ID_Stazione = StazioneSelezionata("Id_Stazione")
        
        Me.Close()
    End Sub

    Private Sub T_Staz_CollBindingSource_CurrentChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles T_Staz_CollBindingSource.CurrentChanged

    End Sub
End Class