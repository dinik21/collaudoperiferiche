﻿Public Class FHistogram

      
    Public lunghezza, altezza As Integer
    Public Titolo As String
    Public LarghezzaBarra As Integer
    Public PosTitX, PosTitY As Integer

    Public yaxis(10) As Integer
    Public xaxis(10) As String
    Public Property pippo() As Integer
        Get
            Return yaxis(0)
        End Get
        Set(ByVal value As Integer)
            yaxis(0) = value
        End Set
    End Property


    Public Sub FHistogram()
        lunghezza = 800
        altezza = 400
        Titolo = "Grafico a Barre"
        PosTitX = 10
        PosTitY = 5
        LarghezzaBarra = 30



    

    End Sub
        

    
    Public Sub Disegna()
        Dim bm As New Bitmap(lunghezza, altezza)
        Dim g As Graphics
        g = Graphics.FromImage(bm)
        g.Clear(Color.Snow)
        g.DrawString(Titolo, New Font("Verdana", 14), Brushes.Black, New PointF(PosTitX, PosTitY))

        ' Imposto la legenda del grafico e la descrizione della legenda
          dim symbolLeg = new PointF(lunghezza-100, 20)
        Dim descLeg = New PointF(lunghezza - 80, 16)

        For i As Integer = 1 To i < xaxis.Length
            g.FillRectangle(New SolidBrush(GetColor(i)), symbolLeg.X, symbolLeg.Y, 20, 10)
            g.DrawRectangle(Pens.Black, symbolLeg.X, symbolLeg.Y, 20, 10)
            g.DrawString(xaxis(i).ToString(), New Font("Arial", 10), Brushes.Black, descLeg)
            symbolLeg.Y += 15
            descLeg.Y += 15
        Next

          
        ' Disegno il grafico   
        Dim spacebetbars As Integer = LarghezzaBarra + 10
        Dim scale As Integer = 1
        For i As Integer = 1 To i < yaxis.Length
            g.FillRectangle(New SolidBrush(GetColor(i)), (i * spacebetbars) + 15, altezza - (yaxis(i) * scale), LarghezzaBarra, (yaxis(i) * scale) + 5)
            g.DrawRectangle(Pens.Black, (i * spacebetbars) + 15, altezza - (yaxis(i) * scale), LarghezzaBarra, (yaxis(i) * scale) + 5)
        Next
        Dim p = New Pen(Color.Black, 2)
        g.DrawRectangle(p, 2, 2, lunghezza - 2, altezza - 2)
        PictureBox1.Image = bm

    End Sub
        

    Private Function GetColor(ByVal itemIndex As Int16) As Color
        Dim objColor As Color
        Select itemIndex
            Case 0 : objColor = Color.Blue
            Case 1 : objColor = Color.Red
            Case 2 : objColor = Color.Yellow
            Case 3 : objColor = Color.Peru
            Case 4 : objColor = Color.Orange
            Case 5 : objColor = Color.Coral
            Case 6 : objColor = Color.Gray
            Case 7 : objColor = Color.Maroon
            Case Else : objColor = Color.Green
        End Select
        Return objColor
    End Function
    Private Sub FHistogram_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      
    End Sub

    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.Click

    End Sub
End Class