﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FSeriale
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Bok = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.CPorta = New System.Windows.Forms.ComboBox
        Me.Cbaud = New System.Windows.Forms.ComboBox
        Me.CParita = New System.Windows.Forms.ComboBox
        Me.CSize = New System.Windows.Forms.ComboBox
        Me.CStop = New System.Windows.Forms.ComboBox
        Me.SuspendLayout()
        '
        'Bok
        '
        Me.Bok.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Bok.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Bok.Location = New System.Drawing.Point(130, 259)
        Me.Bok.Name = "Bok"
        Me.Bok.Size = New System.Drawing.Size(64, 43)
        Me.Bok.TabIndex = 8
        Me.Bok.Text = "OK"
        Me.Bok.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(19, 27)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(116, 24)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Porta Seriale"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(19, 66)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(97, 24)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Baud Rate"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(19, 107)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 24)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "Parità"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(19, 148)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(87, 24)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Byte Size"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(19, 194)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(73, 24)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "Bit Stop"
        '
        'CPorta
        '
        Me.CPorta.FormattingEnabled = True
        Me.CPorta.Items.AddRange(New Object() {"Com1", "Com2", "Com3", "Com4", "Com5", "Com6"})
        Me.CPorta.Location = New System.Drawing.Point(176, 30)
        Me.CPorta.Name = "CPorta"
        Me.CPorta.Size = New System.Drawing.Size(121, 21)
        Me.CPorta.TabIndex = 15
        '
        'Cbaud
        '
        Me.Cbaud.FormattingEnabled = True
        Me.Cbaud.Items.AddRange(New Object() {"9600", "19200", "57600", "115200"})
        Me.Cbaud.Location = New System.Drawing.Point(176, 68)
        Me.Cbaud.Name = "Cbaud"
        Me.Cbaud.Size = New System.Drawing.Size(121, 21)
        Me.Cbaud.TabIndex = 16
        '
        'CParita
        '
        Me.CParita.FormattingEnabled = True
        Me.CParita.Items.AddRange(New Object() {"No Parity", "Odd Parity", "Even Parity"})
        Me.CParita.Location = New System.Drawing.Point(176, 109)
        Me.CParita.Name = "CParita"
        Me.CParita.Size = New System.Drawing.Size(121, 21)
        Me.CParita.TabIndex = 17
        '
        'CSize
        '
        Me.CSize.FormattingEnabled = True
        Me.CSize.Items.AddRange(New Object() {"7", "8"})
        Me.CSize.Location = New System.Drawing.Point(176, 148)
        Me.CSize.Name = "CSize"
        Me.CSize.Size = New System.Drawing.Size(121, 21)
        Me.CSize.TabIndex = 18
        '
        'CStop
        '
        Me.CStop.FormattingEnabled = True
        Me.CStop.Items.AddRange(New Object() {"1"})
        Me.CStop.Location = New System.Drawing.Point(176, 196)
        Me.CStop.Name = "CStop"
        Me.CStop.Size = New System.Drawing.Size(121, 21)
        Me.CStop.TabIndex = 19
        '
        'FSeriale
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(316, 314)
        Me.Controls.Add(Me.CStop)
        Me.Controls.Add(Me.CSize)
        Me.Controls.Add(Me.CParita)
        Me.Controls.Add(Me.Cbaud)
        Me.Controls.Add(Me.CPorta)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Bok)
        Me.Name = "FSeriale"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Impostazioni Seriale"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Bok As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents CPorta As System.Windows.Forms.ComboBox
    Friend WithEvents Cbaud As System.Windows.Forms.ComboBox
    Friend WithEvents CParita As System.Windows.Forms.ComboBox
    Friend WithEvents CSize As System.Windows.Forms.ComboBox
    Friend WithEvents CStop As System.Windows.Forms.ComboBox
End Class
