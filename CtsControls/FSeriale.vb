﻿Public Class FSeriale
    Public Porta As Short
    Public Baud As Short
    Public parit As Short
    Public SizeByte As Short
    Public StopBit As Short


    Private Sub FSeriale_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CPorta.SelectedIndex = Porta
        Cbaud.SelectedIndex = Baud
        CParita.SelectedIndex = parit
        CSize.SelectedIndex = SizeByte
        CStop.SelectedIndex = StopBit
    End Sub

    Private Sub Bok_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bok.Click
        Porta = CPorta.SelectedIndex
        Baud = Cbaud.SelectedIndex
        parit = CParita.SelectedIndex
        SizeByte = CSize.SelectedIndex
        StopBit = CStop.SelectedIndex
        Me.Close()
    End Sub
End Class