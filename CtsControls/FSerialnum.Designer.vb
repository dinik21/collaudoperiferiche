﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FSerialnum
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TSerial1 = New System.Windows.Forms.TextBox
        Me.TSerial2 = New System.Windows.Forms.TextBox
        Me.BOk = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'TSerial1
        '
        Me.TSerial1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TSerial1.Location = New System.Drawing.Point(32, 21)
        Me.TSerial1.MaxLength = 20
        Me.TSerial1.Name = "TSerial1"
        Me.TSerial1.Size = New System.Drawing.Size(276, 26)
        Me.TSerial1.TabIndex = 0
        '
        'TSerial2
        '
        Me.TSerial2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TSerial2.Location = New System.Drawing.Point(32, 74)
        Me.TSerial2.MaxLength = 20
        Me.TSerial2.Name = "TSerial2"
        Me.TSerial2.Size = New System.Drawing.Size(276, 26)
        Me.TSerial2.TabIndex = 1
        '
        'BOk
        '
        Me.BOk.Enabled = False
        Me.BOk.Location = New System.Drawing.Point(102, 148)
        Me.BOk.Name = "BOk"
        Me.BOk.Size = New System.Drawing.Size(121, 44)
        Me.BOk.TabIndex = 2
        Me.BOk.Text = "Ok"
        Me.BOk.UseVisualStyleBackColor = True
        '
        'FSerialnum
        '
        Me.AcceptButton = Me.BOk
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(331, 204)
        Me.ControlBox = False
        Me.Controls.Add(Me.BOk)
        Me.Controls.Add(Me.TSerial2)
        Me.Controls.Add(Me.TSerial1)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FSerialnum"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Inserire Serial Number Periferica"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TSerial1 As System.Windows.Forms.TextBox
    Friend WithEvents TSerial2 As System.Windows.Forms.TextBox
    Friend WithEvents BOk As System.Windows.Forms.Button
End Class
