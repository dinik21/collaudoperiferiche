﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormInfoTest
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Linfo = New System.Windows.Forms.ListBox
        Me.SuspendLayout()
        '
        'Linfo
        '
        Me.Linfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Linfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Linfo.FormattingEnabled = True
        Me.Linfo.ItemHeight = 18
        Me.Linfo.Location = New System.Drawing.Point(0, 0)
        Me.Linfo.Name = "Linfo"
        Me.Linfo.Size = New System.Drawing.Size(635, 184)
        Me.Linfo.TabIndex = 0
        '
        'FormInfoTest
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(635, 194)
        Me.ControlBox = False
        Me.Controls.Add(Me.Linfo)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FormInfoTest"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Informazioni Test in esecuzione"
        Me.ResumeLayout(False)

    End Sub
    Public WithEvents Linfo As System.Windows.Forms.ListBox
End Class
