﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormInkDetectorLS100
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblDoc1SH1TargetValue = New System.Windows.Forms.Label
        Me.lblUltimatabellaPresetDatacaricata = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.lblOffsetFlu = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.lblCorrente = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.lblOffsetRef = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.lblTargetRef = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.VettoreSH1 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.lblUltimoDoc1SH1Avg = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.lblUltimoDoc1SH1Max = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.UltimoDoc1SH1Min = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.lblUltimoDoc1SH1StDev = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.lblNumerociclicorrezzionedinamica = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        'Me.Graph1 = New ZedGraph.ZedGraphControl
        'Me.Graph2 = New ZedGraph.ZedGraphControl
        Me.Bok = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(54, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(241, 21)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Doc1SH1TargetValue"
        '
        'lblDoc1SH1TargetValue
        '
        Me.lblDoc1SH1TargetValue.AutoSize = True
        Me.lblDoc1SH1TargetValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDoc1SH1TargetValue.Location = New System.Drawing.Point(301, 33)
        Me.lblDoc1SH1TargetValue.Name = "lblDoc1SH1TargetValue"
        Me.lblDoc1SH1TargetValue.Size = New System.Drawing.Size(63, 20)
        Me.lblDoc1SH1TargetValue.TabIndex = 1
        Me.lblDoc1SH1TargetValue.Text = "Label2"
        '
        'lblUltimatabellaPresetDatacaricata
        '
        Me.lblUltimatabellaPresetDatacaricata.AutoSize = True
        Me.lblUltimatabellaPresetDatacaricata.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUltimatabellaPresetDatacaricata.Location = New System.Drawing.Point(301, 74)
        Me.lblUltimatabellaPresetDatacaricata.Name = "lblUltimatabellaPresetDatacaricata"
        Me.lblUltimatabellaPresetDatacaricata.Size = New System.Drawing.Size(63, 20)
        Me.lblUltimatabellaPresetDatacaricata.TabIndex = 3
        Me.lblUltimatabellaPresetDatacaricata.Text = "Label2"
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(54, 73)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(241, 21)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "UltimatabellaPresetDatacaricata"
        '
        'lblOffsetFlu
        '
        Me.lblOffsetFlu.AutoSize = True
        Me.lblOffsetFlu.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOffsetFlu.Location = New System.Drawing.Point(301, 115)
        Me.lblOffsetFlu.Name = "lblOffsetFlu"
        Me.lblOffsetFlu.Size = New System.Drawing.Size(63, 20)
        Me.lblOffsetFlu.TabIndex = 5
        Me.lblOffsetFlu.Text = "Label2"
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(54, 114)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(241, 21)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "OffsetFlu 6.1"
        '
        'lblCorrente
        '
        Me.lblCorrente.AutoSize = True
        Me.lblCorrente.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCorrente.Location = New System.Drawing.Point(301, 156)
        Me.lblCorrente.Name = "lblCorrente"
        Me.lblCorrente.Size = New System.Drawing.Size(63, 20)
        Me.lblCorrente.TabIndex = 7
        Me.lblCorrente.Text = "Label2"
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(54, 155)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(241, 21)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Corrente 6.2"
        '
        'lblOffsetRef
        '
        Me.lblOffsetRef.AutoSize = True
        Me.lblOffsetRef.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOffsetRef.Location = New System.Drawing.Point(301, 197)
        Me.lblOffsetRef.Name = "lblOffsetRef"
        Me.lblOffsetRef.Size = New System.Drawing.Size(63, 20)
        Me.lblOffsetRef.TabIndex = 9
        Me.lblOffsetRef.Text = "Label2"
        '
        'Label9
        '
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(54, 196)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(241, 21)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "OffsetRef 10.1"
        '
        'lblTargetRef
        '
        Me.lblTargetRef.AutoSize = True
        Me.lblTargetRef.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTargetRef.Location = New System.Drawing.Point(301, 238)
        Me.lblTargetRef.Name = "lblTargetRef"
        Me.lblTargetRef.Size = New System.Drawing.Size(63, 20)
        Me.lblTargetRef.TabIndex = 11
        Me.lblTargetRef.Text = "Label2"
        '
        'Label11
        '
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(54, 237)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(241, 21)
        Me.Label11.TabIndex = 10
        Me.Label11.Text = "TargetRef 10.2"
        '
        'VettoreSH1
        '
        Me.VettoreSH1.AutoSize = True
        Me.VettoreSH1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VettoreSH1.Location = New System.Drawing.Point(301, 279)
        Me.VettoreSH1.Name = "VettoreSH1"
        Me.VettoreSH1.Size = New System.Drawing.Size(63, 20)
        Me.VettoreSH1.TabIndex = 13
        Me.VettoreSH1.Text = "Label2"
        '
        'Label13
        '
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(54, 278)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(241, 21)
        Me.Label13.TabIndex = 12
        Me.Label13.Text = "Vettore SH1 11.3"
        '
        'lblUltimoDoc1SH1Avg
        '
        Me.lblUltimoDoc1SH1Avg.AutoSize = True
        Me.lblUltimoDoc1SH1Avg.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUltimoDoc1SH1Avg.Location = New System.Drawing.Point(301, 320)
        Me.lblUltimoDoc1SH1Avg.Name = "lblUltimoDoc1SH1Avg"
        Me.lblUltimoDoc1SH1Avg.Size = New System.Drawing.Size(63, 20)
        Me.lblUltimoDoc1SH1Avg.TabIndex = 15
        Me.lblUltimoDoc1SH1Avg.Text = "Label2"
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(54, 319)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(241, 21)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "UltimoDoc1SH1Avg 11.7"
        '
        'lblUltimoDoc1SH1Max
        '
        Me.lblUltimoDoc1SH1Max.AutoSize = True
        Me.lblUltimoDoc1SH1Max.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUltimoDoc1SH1Max.Location = New System.Drawing.Point(301, 361)
        Me.lblUltimoDoc1SH1Max.Name = "lblUltimoDoc1SH1Max"
        Me.lblUltimoDoc1SH1Max.Size = New System.Drawing.Size(63, 20)
        Me.lblUltimoDoc1SH1Max.TabIndex = 17
        Me.lblUltimoDoc1SH1Max.Text = "Label2"
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(54, 360)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(241, 21)
        Me.Label6.TabIndex = 16
        Me.Label6.Text = "UltimoDoc1SH1Max"
        '
        'UltimoDoc1SH1Min
        '
        Me.UltimoDoc1SH1Min.AutoSize = True
        Me.UltimoDoc1SH1Min.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UltimoDoc1SH1Min.Location = New System.Drawing.Point(301, 402)
        Me.UltimoDoc1SH1Min.Name = "UltimoDoc1SH1Min"
        Me.UltimoDoc1SH1Min.Size = New System.Drawing.Size(63, 20)
        Me.UltimoDoc1SH1Min.TabIndex = 19
        Me.UltimoDoc1SH1Min.Text = "Label2"
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(54, 401)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(241, 21)
        Me.Label8.TabIndex = 18
        Me.Label8.Text = "UltimoDoc1SH1Min"
        '
        'lblUltimoDoc1SH1StDev
        '
        Me.lblUltimoDoc1SH1StDev.AutoSize = True
        Me.lblUltimoDoc1SH1StDev.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUltimoDoc1SH1StDev.Location = New System.Drawing.Point(301, 443)
        Me.lblUltimoDoc1SH1StDev.Name = "lblUltimoDoc1SH1StDev"
        Me.lblUltimoDoc1SH1StDev.Size = New System.Drawing.Size(63, 20)
        Me.lblUltimoDoc1SH1StDev.TabIndex = 21
        Me.lblUltimoDoc1SH1StDev.Text = "Label2"
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(54, 442)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(241, 21)
        Me.Label10.TabIndex = 20
        Me.Label10.Text = "UltimoDoc1SH1StDev 11.7"
        '
        'lblNumerociclicorrezzionedinamica
        '
        Me.lblNumerociclicorrezzionedinamica.AutoSize = True
        Me.lblNumerociclicorrezzionedinamica.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumerociclicorrezzionedinamica.Location = New System.Drawing.Point(301, 484)
        Me.lblNumerociclicorrezzionedinamica.Name = "lblNumerociclicorrezzionedinamica"
        Me.lblNumerociclicorrezzionedinamica.Size = New System.Drawing.Size(63, 20)
        Me.lblNumerociclicorrezzionedinamica.TabIndex = 23
        Me.lblNumerociclicorrezzionedinamica.Text = "Label2"
        Me.lblNumerociclicorrezzionedinamica.Visible = False
        '
        'Label12
        '
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(54, 483)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(241, 21)
        Me.Label12.TabIndex = 22
        Me.Label12.Text = "Numerociclicorrezzionedinamica"
        Me.Label12.Visible = False
        '
        'Graph1
        '
        ''Me.Graph1.EditButtons = System.Windows.Forms.MouseButtons.None
        'Me.Graph1.IsEnableVPan = False
        'Me.Graph1.IsShowVScrollBar = True
        'Me.Graph1.Location = New System.Drawing.Point(410, 32)
        'Me.Graph1.Name = "Graph1"
        'Me.Graph1.ScrollGrace = 0
        'Me.Graph1.ScrollMaxX = 2000
        'Me.Graph1.ScrollMaxY = 255
        'Me.Graph1.ScrollMaxY2 = 255
        'Me.Graph1.ScrollMinX = 0
        'Me.Graph1.ScrollMinY = 0
        'Me.Graph1.ScrollMinY2 = 0
        'Me.Graph1.Size = New System.Drawing.Size(577, 330)
        'Me.Graph1.TabIndex = 24
        '
        'Graph2
        '
        'Me.Graph2.EditButtons = System.Windows.Forms.MouseButtons.None
        'Me.Graph2.IsEnableVPan = False
        'Me.Graph2.IsShowVScrollBar = True
        'Me.Graph2.Location = New System.Drawing.Point(410, 378)
        'Me.Graph2.Name = "Graph2"
        'Me.Graph2.ScrollGrace = 0
        'Me.Graph2.ScrollMaxX = 2000
        'Me.Graph2.ScrollMaxY = 255
        'Me.Graph2.ScrollMaxY2 = 255
        'Me.Graph2.ScrollMinX = 0
        'Me.Graph2.ScrollMinY = 0
        'Me.Graph2.ScrollMinY2 = 0
        'Me.Graph2.Size = New System.Drawing.Size(577, 330)
        'Me.Graph2.TabIndex = 25
        '
        'Bok
        '
        Me.Bok.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Bok.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Bok.Location = New System.Drawing.Point(410, 723)
        Me.Bok.Name = "Bok"
        Me.Bok.Size = New System.Drawing.Size(133, 43)
        Me.Bok.TabIndex = 0
        Me.Bok.Text = "OK"
        Me.Bok.UseVisualStyleBackColor = False
        '
        'FormInkDetectorLS100
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1037, 767)
        Me.Controls.Add(Me.Bok)
        'Me.Controls.Add(Me.Graph2)
        'Me.Controls.Add(Me.Graph1)
        Me.Controls.Add(Me.lblNumerociclicorrezzionedinamica)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.lblUltimoDoc1SH1StDev)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.UltimoDoc1SH1Min)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.lblUltimoDoc1SH1Max)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.lblUltimoDoc1SH1Avg)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.VettoreSH1)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.lblTargetRef)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.lblOffsetRef)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.lblCorrente)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.lblOffsetFlu)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lblUltimatabellaPresetDatacaricata)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lblDoc1SH1TargetValue)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FormInkDetectorLS100"
        Me.Text = "FormInkDetectorLS100"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblDoc1SH1TargetValue As System.Windows.Forms.Label
    Friend WithEvents lblUltimatabellaPresetDatacaricata As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblOffsetFlu As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblCorrente As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lblOffsetRef As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblTargetRef As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents VettoreSH1 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents lblUltimoDoc1SH1Avg As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblUltimoDoc1SH1Max As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents UltimoDoc1SH1Min As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblUltimoDoc1SH1StDev As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents lblNumerociclicorrezzionedinamica As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    'Private WithEvents Graph1 As ZedGraph.ZedGraphControl
    'Private WithEvents Graph2 As ZedGraph.ZedGraphControl
    Friend WithEvents Bok As System.Windows.Forms.Button
End Class
