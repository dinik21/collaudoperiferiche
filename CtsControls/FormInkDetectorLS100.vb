﻿'Imports ZedGraph

Public Class FormInkDetectorLS100

    'Private Sub CreateGraph(ByVal zgc As ZedGraphControl, ByVal point As String())
    '    Dim myPane As GraphPane = zgc.GraphPane

    '    ' Set the titles and axis labels
    '    myPane.Title.Text = "Doc1SH1 Array"
    '    myPane.XAxis.Title.Text = "X Value"
    '    myPane.YAxis.Title.Text = "Y Axis"

    '    myPane.XAxis.Scale.Min = 0
    '    myPane.XAxis.Scale.Max = 40
    '    myPane.YAxis.Scale.Min = 0
    '    myPane.YAxis.Scale.Max = 4000

    '    ' Make up some data points from the Sine function
    '    Dim list = New PointPairList()
    '    Dim x As Double ', y As Double
    '    Dim cont As Double
    '    cont = 0
    '    For x = 53 To 90
    '        'y = Math.Sin(x * Math.PI / 15.0)
    '        If (point(x) = "") Then
    '            point(x) = 0
    '            list.Add(cont, point(x))
    '        Else
    '            list.Add(cont, point(x))
    '        End If
    '        cont = cont + 1
    '    Next x

    '    ' Generate a blue curve with circle symbols, and "My Curve 2" in the legend
    '    Dim myCurve As LineItem = myPane.AddCurve("My Curve", list, Color.Blue, SymbolType.Triangle)

    '    ' Fill the area under the curve with a white-red gradient at 45 degrees
    '    'myCurve.Line.Fill = New Fill(Color.White, Color.Red, 45.0F)
    '    ' Make the symbols opaque by filling them with white
    '    myCurve.Symbol.Fill = New Fill(Color.Blue)
    '    myCurve.Line.Width = 2.0
    '    ' Fill the axis background with a color gradient
    '    'myPane.Chart.Fill = New Fill(Color.White, Color.LightGoldenrodYellow, 45.0F)

    '    ' Fill the pane background with a color gradient
    '    'myPane.Fill = New Fill(Color.White, Color.FromArgb(220, 220, 255), 45.0F)
    '    'myPane.Fill = New Fill(Color.White, Color.White, 45.0F)


    '    ' Calculate the Axis Scale Ranges
    '    zgc.AxisChange()
    'End Sub

    'Private Sub CreateGraph2(ByVal zgc As ZedGraphControl, ByVal point As String())
    '    Dim myPane As GraphPane = zgc.GraphPane

    '    ' Set the titles and axis labels
    '    myPane.Title.Text = "Doc2SH1 Array"
    '    myPane.XAxis.Title.Text = "X Value"
    '    myPane.YAxis.Title.Text = "My Y Axis"

    '    myPane.XAxis.Scale.Min = 0
    '    myPane.XAxis.Scale.Max = 40
    '    myPane.YAxis.Scale.Min = 0
    '    myPane.YAxis.Scale.Max = 1000

    '    ' Make up some data points from the Sine function
    '    Dim list = New PointPairList()
    '    Dim x As Double ', y As Double
    '    Dim cont As Double
    '    cont = 0
    '    For x = 156 To 193
    '        'y = Math.Sin(x * Math.PI / 15.0)
    '        If (point(x) = "") Then
    '            point(x) = 0
    '            list.Add(cont, point(x))
    '        Else
    '            list.Add(cont, point(x))
    '        End If
    '        cont = cont + 1
    '    Next x

    '    ' Generate a blue curve with circle symbols, and "My Curve 2" in the legend
    '    Dim myCurve As LineItem = myPane.AddCurve("My Curve", list, Color.Yellow, SymbolType.Triangle)

    '    ' Fill the area under the curve with a white-red gradient at 45 degrees
    '    'myCurve.Line.Fill = New Fill(Color.White, Color.Red, 45.0F)
    '    ' Make the symbols opaque by filling them with white
    '    myCurve.Symbol.Fill = New Fill(Color.White)
    '    myCurve.Line.Width = 2.0

    '    ' Fill the axis background with a color gradient
    '    'myPane.Chart.Fill = New Fill(Color.White, Color.LightGoldenrodYellow, 45.0F)

    '    ' Fill the pane background with a color gradient
    '    'myPane.Fill = New Fill(Color.White, Color.FromArgb(220, 220, 255), 45.0F)

    '    ' Calculate the Axis Scale Ranges
    '    zgc.AxisChange()
    'End Sub

    Private Sub FormInkDetectorLS100_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim FileName As String



        FileName = Application.StartupPath + "\Reports" + "\InkDetectorCalibration.txt"
        Dim lettura As New IO.StreamReader(FileName)
        'Una stringa che rappresenta ogni singola riga del file
        Dim Riga As String
        Dim point As String()

        Riga = "a;b"
        point = Riga.Split(";")

        While Not lettura.EndOfStream


            'lettura.ReadToEnd()
            'Leggiamo una linea di file (S)
            Riga = lettura.ReadLine

            'Se la linea è diversa da una riga vuota
            If (Riga Is Nothing) Or (Riga = "") Then
                'Se la riga è vuota la salta.
            Else
                point = Riga.Split(";")
            End If
        End While

        lblDoc1SH1TargetValue.Text = point(11).ToString()
        lblUltimatabellaPresetDatacaricata.Text = point(12).ToString()
        lblOffsetFlu.Text = point(42).ToString()
        lblCorrente.Text = point(43).ToString()
        lblOffsetRef.Text = point(44).ToString()
        lblTargetRef.Text = point(47).ToString()
        VettoreSH1.Text = point(50).ToString()


        lblUltimoDoc1SH1Avg.Text = point(95).ToString()
        lblUltimoDoc1SH1Max.Text = point(97).ToString()
        UltimoDoc1SH1Min.Text = point(99).ToString()
        lblUltimoDoc1SH1StDev.Text = point(101).ToString()
        lblNumerociclicorrezzionedinamica.Text = point(112).ToString()

        'CreateGraph(Graph1, point)
        'CreateGraph2(Graph2, point)


        Bok.Focus()

    End Sub


   

    Private Sub Form1_Resize(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Resize
        'SetSize()
    End Sub

    Private Sub SetSize()
        'Graph1.Location = New Point(10, 10)
        ' Leave a small margin around the outside of the control
        'Graph1.Size = New Size(ClientRectangle.Width - 20, ClientRectangle.Height - 20)
    End Sub

    Private Sub Bok_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bok.Click

        Me.Close()

    End Sub

    Private Sub Message_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        Select Case (e.KeyValue)
            Case Keys.Enter
                If Bok.Text = "OK" Then
                    'result = DialogResult.OK
                    Me.Close()
                Else
                    'result = DialogResult.Yes
                    Me.Close()
                End If
            Case Keys.Escape
                'result = DialogResult.Abort
                Me.Close()
            Case Keys.Back
                'result = DialogResult.No
                Me.Close()
            Case Else
                Me.Close()
                '   Me.ParentForm.ReflectMessage(Me.ParentForm.Handle, )
        End Select


    End Sub
End Class