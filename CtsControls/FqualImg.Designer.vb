﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FqualImg
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BCalibra = New LBSoft.IndustrialCtrls.Buttons.LBButton
        Me.BCheck = New LBSoft.IndustrialCtrls.Buttons.LBButton
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Bannulla = New LBSoft.IndustrialCtrls.Buttons.LBButton
        Me.Label3 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'BCalibra
        '
        Me.BCalibra.BackColor = System.Drawing.Color.Transparent
        Me.BCalibra.ButtonColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.BCalibra.Label = ""
        Me.BCalibra.Location = New System.Drawing.Point(12, 7)
        Me.BCalibra.Name = "BCalibra"
        Me.BCalibra.Renderer = Nothing
        Me.BCalibra.Size = New System.Drawing.Size(60, 60)
        Me.BCalibra.State = LBSoft.IndustrialCtrls.Buttons.LBButton.ButtonState.Normal
        Me.BCalibra.Style = LBSoft.IndustrialCtrls.Buttons.LBButton.ButtonStyle.Circular
        Me.BCalibra.TabIndex = 0
        '
        'BCheck
        '
        Me.BCheck.BackColor = System.Drawing.Color.Transparent
        Me.BCheck.ButtonColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.BCheck.Label = ""
        Me.BCheck.Location = New System.Drawing.Point(12, 72)
        Me.BCheck.Name = "BCheck"
        Me.BCheck.Renderer = Nothing
        Me.BCheck.Size = New System.Drawing.Size(60, 60)
        Me.BCheck.State = LBSoft.IndustrialCtrls.Buttons.LBButton.ButtonState.Normal
        Me.BCheck.Style = LBSoft.IndustrialCtrls.Buttons.LBButton.ButtonStyle.Circular
        Me.BCheck.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(98, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(376, 24)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Calibrazione Scanner E Controllo Immagine"
        Me.Label1.UseMnemonic = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(98, 94)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(173, 24)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Controllo Immagine"
        '
        'Bannulla
        '
        Me.Bannulla.BackColor = System.Drawing.Color.Transparent
        Me.Bannulla.ButtonColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Bannulla.Label = ""
        Me.Bannulla.Location = New System.Drawing.Point(12, 138)
        Me.Bannulla.Name = "Bannulla"
        Me.Bannulla.Renderer = Nothing
        Me.Bannulla.Size = New System.Drawing.Size(60, 60)
        Me.Bannulla.State = LBSoft.IndustrialCtrls.Buttons.LBButton.ButtonState.Normal
        Me.Bannulla.Style = LBSoft.IndustrialCtrls.Buttons.LBButton.ButtonStyle.Circular
        Me.Bannulla.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(98, 157)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(74, 24)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Annulla"
        '
        'FqualImg
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(474, 206)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Bannulla)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.BCheck)
        Me.Controls.Add(Me.BCalibra)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FqualImg"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BCalibra As LBSoft.IndustrialCtrls.Buttons.LBButton
    Friend WithEvents BCheck As LBSoft.IndustrialCtrls.Buttons.LBButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Bannulla As LBSoft.IndustrialCtrls.Buttons.LBButton
    Friend WithEvents Label3 As System.Windows.Forms.Label
End Class
