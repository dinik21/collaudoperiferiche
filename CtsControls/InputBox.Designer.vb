﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class InputBox
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(InputBox))
        Me.TTesto = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.BAnnulla = New System.Windows.Forms.Button
        Me.Bok = New System.Windows.Forms.Button
        Me.MTValore = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'TTesto
        '
        Me.TTesto.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TTesto.Location = New System.Drawing.Point(12, 19)
        Me.TTesto.Name = "TTesto"
        Me.TTesto.Size = New System.Drawing.Size(586, 72)
        Me.TTesto.TabIndex = 7
        Me.TTesto.Text = "Label1"
        Me.TTesto.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(215, 120)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 24)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Valore"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.Location = New System.Drawing.Point(515, 70)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(64, 40)
        Me.Button1.TabIndex = 12
        Me.Button1.Text = "Annulla Collaudo"
        Me.Button1.UseVisualStyleBackColor = False
        Me.Button1.Visible = False
        '
        'BAnnulla
        '
        Me.BAnnulla.Location = New System.Drawing.Point(515, 120)
        Me.BAnnulla.Name = "BAnnulla"
        Me.BAnnulla.Size = New System.Drawing.Size(64, 38)
        Me.BAnnulla.TabIndex = 11
        Me.BAnnulla.Text = "Annulla"
        Me.BAnnulla.Visible = False
        '
        'Bok
        '
        Me.Bok.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Bok.Location = New System.Drawing.Point(286, 167)
        Me.Bok.Name = "Bok"
        Me.Bok.Size = New System.Drawing.Size(69, 40)
        Me.Bok.TabIndex = 10
        Me.Bok.Text = "OK"
        Me.Bok.UseVisualStyleBackColor = False
        '
        'MTValore
        '
        Me.MTValore.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.MTValore.Location = New System.Drawing.Point(286, 115)
        Me.MTValore.MaxLength = 9999
        Me.MTValore.Name = "MTValore"
        Me.MTValore.Size = New System.Drawing.Size(69, 29)
        Me.MTValore.TabIndex = 14
        '
        'InputBox
        '
        Me.AcceptButton = Me.Bok
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(610, 219)
        Me.Controls.Add(Me.MTValore)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.BAnnulla)
        Me.Controls.Add(Me.Bok)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TTesto)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "InputBox"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "InputBox"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TTesto As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents BAnnulla As System.Windows.Forms.Button
    Friend WithEvents Bok As System.Windows.Forms.Button
    Friend WithEvents MTValore As System.Windows.Forms.TextBox
End Class
