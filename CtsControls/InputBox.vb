﻿Imports System.Windows.Forms
Imports System.Drawing

Public Class InputBox
    Inherits System.Windows.Forms.Form


    Public Titolo As String
    Public Messaggio As String
    Public result As Windows.Forms.DialogResult
    Public fine As Boolean
    Public Valore As Integer
    Public Check As Boolean
    Public valoreMinimo As Integer
    Public valoreMassimo As Integer
  

    Private Sub InputBox_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Text = Titolo
        Me.TTesto.Text = Messaggio
        Me.MTValore.Focus()

    End Sub

    Private Sub Bok_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bok.Click
        Dim str As String
inizio:
        If MTValore.Text <> "" Then
            Valore = CInt(MTValore.Text)
            'If Valore < 110 And Valore > 65 Then
            If (valoreMinimo = 0 And valoreMassimo = 0) Then
            Else
                If (Valore < valoreMinimo Or Valore > valoreMassimo) Then
                    MTValore.Focus()
                    MTValore.Text = ""
                    MessageboxMy("Valore Inserito NON corretto", "Attenzione!!!", MessageBoxButtons.OK, MessageBoxIcon.Question, Nothing)
                    GoTo inizio
                End If
            End If

            If Check = True Then
                str = "Il valore inserito è : " + Valore.ToString
                If MessageboxMy(str, "Attenzione!!!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing) = DialogResult.Yes Then
                    If Bok.Text = "OK" Then
                        result = Windows.Forms.DialogResult.OK
                    Else
                        result = Windows.Forms.DialogResult.Yes
                    End If
                    Me.Close()
                Else
                    MTValore.Focus()
                End If
            Else
                If Bok.Text = "OK" Then
                    result = Windows.Forms.DialogResult.OK
                Else
                    result = Windows.Forms.DialogResult.Yes
                End If
                Me.Close()
            End If
            'Else
            '    MTValore.Focus()
            'End If
        Else
            MTValore.Focus()
        End If


    End Sub

    Private Sub BAnnulla_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BAnnulla.Click
        'If BAnnulla.Text = "Annulla" Then
        '    result = Windows.Forms.DialogResult.Abort
        'Else
        '    result = Windows.Forms.DialogResult.Cancel
        'End If
        'Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'fine = True
        'result = Windows.Forms.DialogResult.Abort
        'Me.Close()
    End Sub

    Public Function MessageboxMy(ByVal testo As String, ByVal titolo As String, ByVal bt As MessageBoxButtons, ByVal ic As MessageBoxIcon, ByVal Img As Image) As DialogResult
        Dim ff As New Message


        ff.Text = titolo
        ff.testo = testo
        ff.icona = ic
        ff.pulsanti = bt
        ff.Imag.Image = Img
        'If BVisualZoom = True Then
        '    ff.Bzoom.Visible = True
        '    ff.Directory = DirZoom
        '    ff.FileCorrente = FileZoomCorrente
        'End If


        ff.ShowDialog(Me)
        'If ff.fine = True Then
        '    ii = Tentativi
        '    ret = False
        'End If

        Return ff.result

    End Function

   
  
End Class