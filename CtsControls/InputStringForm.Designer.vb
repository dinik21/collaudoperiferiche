﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class InputStringForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(InputStringForm))
        Me.MTValore = New System.Windows.Forms.TextBox
        Me.BAnnulla = New System.Windows.Forms.Button
        Me.Bok = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.TTesto = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'MTValore
        '
        Me.MTValore.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!)
        Me.MTValore.Location = New System.Drawing.Point(226, 88)
        Me.MTValore.MaxLength = 9999
        Me.MTValore.Name = "MTValore"
        Me.MTValore.Size = New System.Drawing.Size(155, 29)
        Me.MTValore.TabIndex = 14
        '
        'BAnnulla
        '
        Me.BAnnulla.Location = New System.Drawing.Point(325, 142)
        Me.BAnnulla.Name = "BAnnulla"
        Me.BAnnulla.Size = New System.Drawing.Size(56, 38)
        Me.BAnnulla.TabIndex = 11
        Me.BAnnulla.Text = "Annulla"
        '
        'Bok
        '
        Me.Bok.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Bok.Location = New System.Drawing.Point(226, 142)
        Me.Bok.Name = "Bok"
        Me.Bok.Size = New System.Drawing.Size(54, 40)
        Me.Bok.TabIndex = 10
        Me.Bok.Text = "OK"
        Me.Bok.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(155, 93)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 24)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Valore"
        '
        'TTesto
        '
        Me.TTesto.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TTesto.Location = New System.Drawing.Point(12, 9)
        Me.TTesto.Name = "TTesto"
        Me.TTesto.Size = New System.Drawing.Size(586, 72)
        Me.TTesto.TabIndex = 7
        Me.TTesto.Text = "Label1"
        Me.TTesto.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'InputStringForm
        '
        Me.AcceptButton = Me.Bok
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(631, 227)
        Me.Controls.Add(Me.MTValore)
        Me.Controls.Add(Me.BAnnulla)
        Me.Controls.Add(Me.Bok)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TTesto)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "InputStringForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "InputStringForm"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MTValore As System.Windows.Forms.TextBox
    Friend WithEvents BAnnulla As System.Windows.Forms.Button
    Friend WithEvents Bok As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TTesto As System.Windows.Forms.Label
End Class
