﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Led
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LMessaggio = New System.Windows.Forms.Label
        Me.LbLedSeBin = New LBSoft.IndustrialCtrls.Leds.LBLed
        Me.Label1 = New System.Windows.Forms.Label
        Me.LSe1 = New System.Windows.Forms.Label
        Me.Lse2 = New System.Windows.Forms.Label
        Me.LSe3 = New System.Windows.Forms.Label
        Me.LbLedSe3 = New LBSoft.IndustrialCtrls.Leds.LBLed
        Me.LbLedSe2 = New LBSoft.IndustrialCtrls.Leds.LBLed
        Me.LbLedSe1 = New LBSoft.IndustrialCtrls.Leds.LBLed
        Me.Button1 = New System.Windows.Forms.Button
        Me.LbLedSe5 = New LBSoft.IndustrialCtrls.Leds.LBLed
        Me.LSe5 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'LMessaggio
        '
        Me.LMessaggio.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LMessaggio.Location = New System.Drawing.Point(10, 9)
        Me.LMessaggio.Name = "LMessaggio"
        Me.LMessaggio.Size = New System.Drawing.Size(562, 86)
        Me.LMessaggio.TabIndex = 0
        Me.LMessaggio.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LbLedSeBin
        '
        Me.LbLedSeBin.BackColor = System.Drawing.Color.Transparent
        Me.LbLedSeBin.BlinkInterval = 50
        Me.LbLedSeBin.Label = ""
        Me.LbLedSeBin.LabelPosition = LBSoft.IndustrialCtrls.Leds.LBLed.LedLabelPosition.Top
        Me.LbLedSeBin.LedColor = System.Drawing.Color.Red
        Me.LbLedSeBin.LedSize = New System.Drawing.SizeF(35.0!, 35.0!)
        Me.LbLedSeBin.Location = New System.Drawing.Point(408, 174)
        Me.LbLedSeBin.Name = "LbLedSeBin"
        Me.LbLedSeBin.Renderer = Nothing
        Me.LbLedSeBin.Size = New System.Drawing.Size(40, 49)
        Me.LbLedSeBin.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.Off
        Me.LbLedSeBin.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(221, 187)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(152, 24)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Sensore 4 PWM:"
        '
        'LSe1
        '
        Me.LSe1.AutoSize = True
        Me.LSe1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LSe1.Location = New System.Drawing.Point(-1, 110)
        Me.LSe1.Name = "LSe1"
        Me.LSe1.Size = New System.Drawing.Size(157, 24)
        Me.LSe1.TabIndex = 3
        Me.LSe1.Text = "Sensore 1 PWM: "
        '
        'Lse2
        '
        Me.Lse2.AutoSize = True
        Me.Lse2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lse2.Location = New System.Drawing.Point(221, 110)
        Me.Lse2.Name = "Lse2"
        Me.Lse2.Size = New System.Drawing.Size(157, 24)
        Me.Lse2.TabIndex = 4
        Me.Lse2.Text = "Sensore 2 PWM: "
        '
        'LSe3
        '
        Me.LSe3.AutoSize = True
        Me.LSe3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LSe3.Location = New System.Drawing.Point(-1, 187)
        Me.LSe3.Name = "LSe3"
        Me.LSe3.Size = New System.Drawing.Size(157, 24)
        Me.LSe3.TabIndex = 5
        Me.LSe3.Text = "Sensore 3 PWM: "
        '
        'LbLedSe3
        '
        Me.LbLedSe3.BackColor = System.Drawing.Color.Transparent
        Me.LbLedSe3.BlinkInterval = 50
        Me.LbLedSe3.Label = ""
        Me.LbLedSe3.LabelPosition = LBSoft.IndustrialCtrls.Leds.LBLed.LedLabelPosition.Top
        Me.LbLedSe3.LedColor = System.Drawing.Color.Red
        Me.LbLedSe3.LedSize = New System.Drawing.SizeF(35.0!, 35.0!)
        Me.LbLedSe3.Location = New System.Drawing.Point(176, 174)
        Me.LbLedSe3.Name = "LbLedSe3"
        Me.LbLedSe3.Renderer = Nothing
        Me.LbLedSe3.Size = New System.Drawing.Size(43, 49)
        Me.LbLedSe3.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.Off
        Me.LbLedSe3.TabIndex = 6
        '
        'LbLedSe2
        '
        Me.LbLedSe2.BackColor = System.Drawing.Color.Transparent
        Me.LbLedSe2.BlinkInterval = 50
        Me.LbLedSe2.Label = ""
        Me.LbLedSe2.LabelPosition = LBSoft.IndustrialCtrls.Leds.LBLed.LedLabelPosition.Top
        Me.LbLedSe2.LedColor = System.Drawing.Color.Red
        Me.LbLedSe2.LedSize = New System.Drawing.SizeF(35.0!, 35.0!)
        Me.LbLedSe2.Location = New System.Drawing.Point(408, 93)
        Me.LbLedSe2.Name = "LbLedSe2"
        Me.LbLedSe2.Renderer = Nothing
        Me.LbLedSe2.Size = New System.Drawing.Size(40, 49)
        Me.LbLedSe2.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.Off
        Me.LbLedSe2.TabIndex = 7
        '
        'LbLedSe1
        '
        Me.LbLedSe1.BackColor = System.Drawing.Color.Transparent
        Me.LbLedSe1.BlinkInterval = 50
        Me.LbLedSe1.Label = ""
        Me.LbLedSe1.LabelPosition = LBSoft.IndustrialCtrls.Leds.LBLed.LedLabelPosition.Top
        Me.LbLedSe1.LedColor = System.Drawing.Color.Red
        Me.LbLedSe1.LedSize = New System.Drawing.SizeF(35.0!, 35.0!)
        Me.LbLedSe1.Location = New System.Drawing.Point(176, 93)
        Me.LbLedSe1.Name = "LbLedSe1"
        Me.LbLedSe1.Renderer = Nothing
        Me.LbLedSe1.Size = New System.Drawing.Size(43, 49)
        Me.LbLedSe1.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.Off
        Me.LbLedSe1.TabIndex = 8
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(499, 128)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(73, 49)
        Me.Button1.TabIndex = 9
        Me.Button1.Text = "Esci"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'LbLedSe5
        '
        Me.LbLedSe5.BackColor = System.Drawing.Color.Transparent
        Me.LbLedSe5.BlinkInterval = 50
        Me.LbLedSe5.Label = ""
        Me.LbLedSe5.LabelPosition = LBSoft.IndustrialCtrls.Leds.LBLed.LedLabelPosition.Top
        Me.LbLedSe5.LedColor = System.Drawing.Color.Red
        Me.LbLedSe5.LedSize = New System.Drawing.SizeF(35.0!, 35.0!)
        Me.LbLedSe5.Location = New System.Drawing.Point(543, 174)
        Me.LbLedSe5.Name = "LbLedSe5"
        Me.LbLedSe5.Renderer = Nothing
        Me.LbLedSe5.Size = New System.Drawing.Size(43, 49)
        Me.LbLedSe5.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.Off
        Me.LbLedSe5.TabIndex = 11
        Me.LbLedSe5.Visible = False
        '
        'LSe5
        '
        Me.LSe5.AutoSize = True
        Me.LSe5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LSe5.Location = New System.Drawing.Point(429, 226)
        Me.LSe5.Name = "LSe5"
        Me.LSe5.Size = New System.Drawing.Size(157, 24)
        Me.LSe5.TabIndex = 10
        Me.LSe5.Text = "Sensore 5 PWM: "
        Me.LSe5.Visible = False
        '
        'Led
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(584, 256)
        Me.ControlBox = False
        Me.Controls.Add(Me.LbLedSe5)
        Me.Controls.Add(Me.LSe5)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.LbLedSe1)
        Me.Controls.Add(Me.LbLedSe2)
        Me.Controls.Add(Me.LbLedSe3)
        Me.Controls.Add(Me.LSe3)
        Me.Controls.Add(Me.Lse2)
        Me.Controls.Add(Me.LSe1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.LbLedSeBin)
        Me.Controls.Add(Me.LMessaggio)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Led"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Led"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents LbLedSeBin As LBSoft.IndustrialCtrls.Leds.LBLed
    Public WithEvents LbLedSe3 As LBSoft.IndustrialCtrls.Leds.LBLed
    Public WithEvents LbLedSe2 As LBSoft.IndustrialCtrls.Leds.LBLed
    Public WithEvents LbLedSe1 As LBSoft.IndustrialCtrls.Leds.LBLed
    Public WithEvents Label1 As System.Windows.Forms.Label
    Public WithEvents LSe1 As System.Windows.Forms.Label
    Public WithEvents Lse2 As System.Windows.Forms.Label
    Public WithEvents LSe3 As System.Windows.Forms.Label
    Public WithEvents LMessaggio As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Public WithEvents LbLedSe5 As LBSoft.IndustrialCtrls.Leds.LBLed
    Public WithEvents LSe5 As System.Windows.Forms.Label
End Class
