﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Led5xx
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LMessaggio = New System.Windows.Forms.Label
        Me.LbLedSe4 = New LBSoft.IndustrialCtrls.Leds.LBLed
        Me.LSe4 = New System.Windows.Forms.Label
        Me.LSe1 = New System.Windows.Forms.Label
        Me.Lse2 = New System.Windows.Forms.Label
        Me.LSe3 = New System.Windows.Forms.Label
        Me.LbLedSe3 = New LBSoft.IndustrialCtrls.Leds.LBLed
        Me.LbLedSe2 = New LBSoft.IndustrialCtrls.Leds.LBLed
        Me.LbLedSe1 = New LBSoft.IndustrialCtrls.Leds.LBLed
        Me.LbLedSe5 = New LBSoft.IndustrialCtrls.Leds.LBLed
        Me.LSe5 = New System.Windows.Forms.Label
        Me.LSe6 = New System.Windows.Forms.Label
        Me.LbLedSe6 = New LBSoft.IndustrialCtrls.Leds.LBLed
        Me.LbLedSeBin = New LBSoft.IndustrialCtrls.Leds.LBLed
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.LbLedSeBin2 = New LBSoft.IndustrialCtrls.Leds.LBLed
        Me.SuspendLayout()
        '
        'LMessaggio
        '
        Me.LMessaggio.AutoSize = True
        Me.LMessaggio.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LMessaggio.Location = New System.Drawing.Point(21, 17)
        Me.LMessaggio.Name = "LMessaggio"
        Me.LMessaggio.Size = New System.Drawing.Size(0, 24)
        Me.LMessaggio.TabIndex = 0
        '
        'LbLedSe4
        '
        Me.LbLedSe4.BackColor = System.Drawing.Color.Transparent
        Me.LbLedSe4.BlinkInterval = 50
        Me.LbLedSe4.Label = ""
        Me.LbLedSe4.LabelPosition = LBSoft.IndustrialCtrls.Leds.LBLed.LedLabelPosition.Top
        Me.LbLedSe4.LedColor = System.Drawing.Color.Red
        Me.LbLedSe4.LedSize = New System.Drawing.SizeF(35.0!, 35.0!)
        Me.LbLedSe4.Location = New System.Drawing.Point(408, 132)
        Me.LbLedSe4.Name = "LbLedSe4"
        Me.LbLedSe4.Renderer = Nothing
        Me.LbLedSe4.Size = New System.Drawing.Size(40, 49)
        Me.LbLedSe4.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.Off
        Me.LbLedSe4.TabIndex = 1
        '
        'LSe4
        '
        Me.LSe4.AutoSize = True
        Me.LSe4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LSe4.Location = New System.Drawing.Point(221, 145)
        Me.LSe4.Name = "LSe4"
        Me.LSe4.Size = New System.Drawing.Size(152, 24)
        Me.LSe4.TabIndex = 2
        Me.LSe4.Text = "Sensore 4 PWM:"
        '
        'LSe1
        '
        Me.LSe1.AutoSize = True
        Me.LSe1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LSe1.Location = New System.Drawing.Point(-1, 68)
        Me.LSe1.Name = "LSe1"
        Me.LSe1.Size = New System.Drawing.Size(157, 24)
        Me.LSe1.TabIndex = 3
        Me.LSe1.Text = "Sensore 1 PWM: "
        '
        'Lse2
        '
        Me.Lse2.AutoSize = True
        Me.Lse2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lse2.Location = New System.Drawing.Point(221, 68)
        Me.Lse2.Name = "Lse2"
        Me.Lse2.Size = New System.Drawing.Size(157, 24)
        Me.Lse2.TabIndex = 4
        Me.Lse2.Text = "Sensore 2 PWM: "
        '
        'LSe3
        '
        Me.LSe3.AutoSize = True
        Me.LSe3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LSe3.Location = New System.Drawing.Point(-1, 145)
        Me.LSe3.Name = "LSe3"
        Me.LSe3.Size = New System.Drawing.Size(157, 24)
        Me.LSe3.TabIndex = 5
        Me.LSe3.Text = "Sensore 3 PWM: "
        '
        'LbLedSe3
        '
        Me.LbLedSe3.BackColor = System.Drawing.Color.Transparent
        Me.LbLedSe3.BlinkInterval = 50
        Me.LbLedSe3.Label = ""
        Me.LbLedSe3.LabelPosition = LBSoft.IndustrialCtrls.Leds.LBLed.LedLabelPosition.Top
        Me.LbLedSe3.LedColor = System.Drawing.Color.Red
        Me.LbLedSe3.LedSize = New System.Drawing.SizeF(35.0!, 35.0!)
        Me.LbLedSe3.Location = New System.Drawing.Point(176, 132)
        Me.LbLedSe3.Name = "LbLedSe3"
        Me.LbLedSe3.Renderer = Nothing
        Me.LbLedSe3.Size = New System.Drawing.Size(43, 49)
        Me.LbLedSe3.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.Off
        Me.LbLedSe3.TabIndex = 6
        '
        'LbLedSe2
        '
        Me.LbLedSe2.BackColor = System.Drawing.Color.Transparent
        Me.LbLedSe2.BlinkInterval = 50
        Me.LbLedSe2.Label = ""
        Me.LbLedSe2.LabelPosition = LBSoft.IndustrialCtrls.Leds.LBLed.LedLabelPosition.Top
        Me.LbLedSe2.LedColor = System.Drawing.Color.Red
        Me.LbLedSe2.LedSize = New System.Drawing.SizeF(35.0!, 35.0!)
        Me.LbLedSe2.Location = New System.Drawing.Point(408, 51)
        Me.LbLedSe2.Name = "LbLedSe2"
        Me.LbLedSe2.Renderer = Nothing
        Me.LbLedSe2.Size = New System.Drawing.Size(40, 49)
        Me.LbLedSe2.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.Off
        Me.LbLedSe2.TabIndex = 7
        '
        'LbLedSe1
        '
        Me.LbLedSe1.BackColor = System.Drawing.Color.Transparent
        Me.LbLedSe1.BlinkInterval = 50
        Me.LbLedSe1.Label = ""
        Me.LbLedSe1.LabelPosition = LBSoft.IndustrialCtrls.Leds.LBLed.LedLabelPosition.Top
        Me.LbLedSe1.LedColor = System.Drawing.Color.Red
        Me.LbLedSe1.LedSize = New System.Drawing.SizeF(35.0!, 35.0!)
        Me.LbLedSe1.Location = New System.Drawing.Point(176, 51)
        Me.LbLedSe1.Name = "LbLedSe1"
        Me.LbLedSe1.Renderer = Nothing
        Me.LbLedSe1.Size = New System.Drawing.Size(43, 49)
        Me.LbLedSe1.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.Off
        Me.LbLedSe1.TabIndex = 8
        '
        'LbLedSe5
        '
        Me.LbLedSe5.BackColor = System.Drawing.Color.Transparent
        Me.LbLedSe5.BlinkInterval = 50
        Me.LbLedSe5.Label = ""
        Me.LbLedSe5.LabelPosition = LBSoft.IndustrialCtrls.Leds.LBLed.LedLabelPosition.Top
        Me.LbLedSe5.LedColor = System.Drawing.Color.Red
        Me.LbLedSe5.LedSize = New System.Drawing.SizeF(35.0!, 35.0!)
        Me.LbLedSe5.Location = New System.Drawing.Point(176, 207)
        Me.LbLedSe5.Name = "LbLedSe5"
        Me.LbLedSe5.Renderer = Nothing
        Me.LbLedSe5.Size = New System.Drawing.Size(43, 49)
        Me.LbLedSe5.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.Off
        Me.LbLedSe5.TabIndex = 12
        '
        'LSe5
        '
        Me.LSe5.AutoSize = True
        Me.LSe5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LSe5.Location = New System.Drawing.Point(-1, 220)
        Me.LSe5.Name = "LSe5"
        Me.LSe5.Size = New System.Drawing.Size(157, 24)
        Me.LSe5.TabIndex = 11
        Me.LSe5.Text = "Sensore 5 PWM: "
        '
        'LSe6
        '
        Me.LSe6.AutoSize = True
        Me.LSe6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LSe6.Location = New System.Drawing.Point(221, 220)
        Me.LSe6.Name = "LSe6"
        Me.LSe6.Size = New System.Drawing.Size(152, 24)
        Me.LSe6.TabIndex = 10
        Me.LSe6.Text = "Sensore 6 PWM:"
        '
        'LbLedSe6
        '
        Me.LbLedSe6.BackColor = System.Drawing.Color.Transparent
        Me.LbLedSe6.BlinkInterval = 50
        Me.LbLedSe6.Label = ""
        Me.LbLedSe6.LabelPosition = LBSoft.IndustrialCtrls.Leds.LBLed.LedLabelPosition.Top
        Me.LbLedSe6.LedColor = System.Drawing.Color.Red
        Me.LbLedSe6.LedSize = New System.Drawing.SizeF(35.0!, 35.0!)
        Me.LbLedSe6.Location = New System.Drawing.Point(408, 207)
        Me.LbLedSe6.Name = "LbLedSe6"
        Me.LbLedSe6.Renderer = Nothing
        Me.LbLedSe6.Size = New System.Drawing.Size(40, 49)
        Me.LbLedSe6.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.Off
        Me.LbLedSe6.TabIndex = 9
        '
        'LbLedSeBin
        '
        Me.LbLedSeBin.BackColor = System.Drawing.Color.Transparent
        Me.LbLedSeBin.BlinkInterval = 50
        Me.LbLedSeBin.Label = ""
        Me.LbLedSeBin.LabelPosition = LBSoft.IndustrialCtrls.Leds.LBLed.LedLabelPosition.Top
        Me.LbLedSeBin.LedColor = System.Drawing.Color.Red
        Me.LbLedSeBin.LedSize = New System.Drawing.SizeF(35.0!, 35.0!)
        Me.LbLedSeBin.Location = New System.Drawing.Point(176, 278)
        Me.LbLedSeBin.Name = "LbLedSeBin"
        Me.LbLedSeBin.Renderer = Nothing
        Me.LbLedSeBin.Size = New System.Drawing.Size(43, 49)
        Me.LbLedSeBin.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.Off
        Me.LbLedSeBin.TabIndex = 16
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(-1, 291)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(187, 24)
        Me.Label4.TabIndex = 15
        Me.Label4.Text = "Sensore BIN1 PWM: "
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(221, 291)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(182, 24)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "Sensore BIN2 PWM:"
        '
        'LbLedSeBin2
        '
        Me.LbLedSeBin2.BackColor = System.Drawing.Color.Transparent
        Me.LbLedSeBin2.BlinkInterval = 50
        Me.LbLedSeBin2.Label = ""
        Me.LbLedSeBin2.LabelPosition = LBSoft.IndustrialCtrls.Leds.LBLed.LedLabelPosition.Top
        Me.LbLedSeBin2.LedColor = System.Drawing.Color.Red
        Me.LbLedSeBin2.LedSize = New System.Drawing.SizeF(35.0!, 35.0!)
        Me.LbLedSeBin2.Location = New System.Drawing.Point(408, 278)
        Me.LbLedSeBin2.Name = "LbLedSeBin2"
        Me.LbLedSeBin2.Renderer = Nothing
        Me.LbLedSeBin2.Size = New System.Drawing.Size(40, 49)
        Me.LbLedSeBin2.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.Off
        Me.LbLedSeBin2.TabIndex = 13
        '
        'Led5xx
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(452, 336)
        Me.ControlBox = False
        Me.Controls.Add(Me.LbLedSeBin)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.LbLedSeBin2)
        Me.Controls.Add(Me.LbLedSe5)
        Me.Controls.Add(Me.LSe5)
        Me.Controls.Add(Me.LSe6)
        Me.Controls.Add(Me.LbLedSe6)
        Me.Controls.Add(Me.LbLedSe1)
        Me.Controls.Add(Me.LbLedSe2)
        Me.Controls.Add(Me.LbLedSe3)
        Me.Controls.Add(Me.LSe3)
        Me.Controls.Add(Me.Lse2)
        Me.Controls.Add(Me.LSe1)
        Me.Controls.Add(Me.LSe4)
        Me.Controls.Add(Me.LbLedSe4)
        Me.Controls.Add(Me.LMessaggio)
        Me.Name = "Led5xx"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Led"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents LbLedSe4 As LBSoft.IndustrialCtrls.Leds.LBLed
    Public WithEvents LbLedSe3 As LBSoft.IndustrialCtrls.Leds.LBLed
    Public WithEvents LbLedSe2 As LBSoft.IndustrialCtrls.Leds.LBLed
    Public WithEvents LbLedSe1 As LBSoft.IndustrialCtrls.Leds.LBLed
    Public WithEvents LSe4 As System.Windows.Forms.Label
    Public WithEvents LSe1 As System.Windows.Forms.Label
    Public WithEvents Lse2 As System.Windows.Forms.Label
    Public WithEvents LSe3 As System.Windows.Forms.Label
    Public WithEvents LMessaggio As System.Windows.Forms.Label
    Public WithEvents LbLedSe5 As LBSoft.IndustrialCtrls.Leds.LBLed
    Public WithEvents LSe5 As System.Windows.Forms.Label
    Public WithEvents LSe6 As System.Windows.Forms.Label
    Public WithEvents LbLedSe6 As LBSoft.IndustrialCtrls.Leds.LBLed
    Public WithEvents LbLedSeBin As LBSoft.IndustrialCtrls.Leds.LBLed
    Public WithEvents Label4 As System.Windows.Forms.Label
    Public WithEvents Label5 As System.Windows.Forms.Label
    Public WithEvents LbLedSeBin2 As LBSoft.IndustrialCtrls.Leds.LBLed
End Class
