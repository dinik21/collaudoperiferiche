Imports System.Windows.Forms
Imports System.Drawing

Public Class Message
    Inherits System.Windows.Forms.Form

    Dim ImgVet(10) As Image
    Public icona As MessageBoxIcon
    Public pulsanti As MessageBoxButtons
    Public testo As String
    Public result As DialogResult
    Public fine As Boolean
    Public Directory As String
    Public FileCorrente As String


#Region " Codice generato da Progettazione Windows Form "

    Public Sub New()
        MyBase.New()

        'Chiamata richiesta da Progettazione Windows Form.
        InitializeComponent()

        'Aggiungere le eventuali istruzioni di inizializzazione dopo la chiamata a InitializeComponent()

    End Sub

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue � richiesta da Progettazione Windows Form.
    'Pu� essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    Friend WithEvents PIcona As System.Windows.Forms.PictureBox
    Public WithEvents Imag As System.Windows.Forms.PictureBox
    Friend WithEvents Bok As System.Windows.Forms.Button
    Friend WithEvents BAnnulla As System.Windows.Forms.Button
    Friend WithEvents TTesto As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Bzoom As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Message))
        Me.Bok = New System.Windows.Forms.Button
        Me.BAnnulla = New System.Windows.Forms.Button
        Me.TTesto = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.Bzoom = New System.Windows.Forms.Button
        Me.Imag = New System.Windows.Forms.PictureBox
        Me.PIcona = New System.Windows.Forms.PictureBox
        CType(Me.Imag, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PIcona, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Bok
        '
        Me.Bok.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Bok.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Bok.Location = New System.Drawing.Point(564, 148)
        Me.Bok.Name = "Bok"
        Me.Bok.Size = New System.Drawing.Size(64, 43)
        Me.Bok.TabIndex = 3
        Me.Bok.Text = "OK"
        Me.Bok.UseVisualStyleBackColor = False
        Me.Bok.Visible = False
        '
        'BAnnulla
        '
        Me.BAnnulla.Location = New System.Drawing.Point(11, 145)
        Me.BAnnulla.Name = "BAnnulla"
        Me.BAnnulla.Size = New System.Drawing.Size(51, 32)
        Me.BAnnulla.TabIndex = 4
        Me.BAnnulla.Text = "Annulla"
        Me.BAnnulla.Visible = False
        '
        'TTesto
        '
        Me.TTesto.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TTesto.Location = New System.Drawing.Point(144, 8)
        Me.TTesto.Name = "TTesto"
        Me.TTesto.Size = New System.Drawing.Size(414, 130)
        Me.TTesto.TabIndex = 6
        Me.TTesto.Text = "Label1"
        Me.TTesto.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.Location = New System.Drawing.Point(8, 326)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(64, 43)
        Me.Button1.TabIndex = 7
        Me.Button1.Text = "Annulla Collaudo"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Bzoom
        '
        Me.Bzoom.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Bzoom.Location = New System.Drawing.Point(11, 183)
        Me.Bzoom.Name = "Bzoom"
        Me.Bzoom.Size = New System.Drawing.Size(51, 32)
        Me.Bzoom.TabIndex = 8
        Me.Bzoom.Text = "Zoom Immagine"
        Me.Bzoom.Visible = False
        '
        'Imag
        '
        Me.Imag.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Imag.Location = New System.Drawing.Point(148, 145)
        Me.Imag.Name = "Imag"
        Me.Imag.Size = New System.Drawing.Size(410, 224)
        Me.Imag.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Imag.TabIndex = 2
        Me.Imag.TabStop = False
        '
        'PIcona
        '
        Me.PIcona.Location = New System.Drawing.Point(8, 8)
        Me.PIcona.Name = "PIcona"
        Me.PIcona.Size = New System.Drawing.Size(130, 130)
        Me.PIcona.TabIndex = 0
        Me.PIcona.TabStop = False
        '
        'Message
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(632, 381)
        Me.Controls.Add(Me.Bzoom)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.TTesto)
        Me.Controls.Add(Me.BAnnulla)
        Me.Controls.Add(Me.Bok)
        Me.Controls.Add(Me.Imag)
        Me.Controls.Add(Me.PIcona)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Message"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.Imag, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PIcona, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Message_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed


        'Application.DoEvents()
    End Sub


    Public Shared Sub CenterForm(ByVal frm As Form, Optional ByVal parent As Form = Nothing)
        '' Note: call this from frm's Load event!
        Dim r As Rectangle
        If parent IsNot Nothing Then
            r = parent.RectangleToScreen(parent.ClientRectangle)
        Else
            r = Screen.FromPoint(frm.Location).WorkingArea
        End If

        Dim x = r.Left + (r.Width - frm.Width) \ 2
        Dim y = r.Top + (r.Height - frm.Height) \ 2
        frm.Location = New Point(x, y)
    End Sub

    Private Sub Message_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim rsxr As Resources.ResXResourceReader
        Dim a As Resources.ResourceSet
        Dim punto As New Point
        Dim path As String
        Dim en As IDictionaryEnumerator
        'Dim Screen As System.Drawing.Rectangle

        'Screen = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea()
        'Me.Top = (Screen.Height \ 2) - (Me.Height \ 2)
        'Me.Left = (Screen.Width \ 2) - (Me.Width \ 2)
        'Me.Location = New Point(50, 50)
        'CenterForm(Me)

        path = My.Application.Info.DirectoryPath + "\img.resx"


        fine = False
        rsxr = New Resources.ResXResourceReader(path)
        rsxr.BasePath = path
        Try
            a = New Resources.ResourceSet(rsxr)
            en = a.GetEnumerator()


            Dim id As IDictionaryEnumerator = rsxr.GetEnumerator()

            ' Iterate through the resources and display the contents to the console.
            Dim d As DictionaryEntry
            For Each d In rsxr
                Console.WriteLine(d.Key.ToString() + ":" + ControlChars.Tab + d.Value.ToString())
            Next d



            punto = Me.Location
            punto.Y += 280
            'Me.Location = punto
            Me.StartPosition = FormStartPosition.CenterScreen
            ' Goes through the enumerator, printing out the key and value pairs.
            Dim ii As Short
            ' finestra di massaggio in esecuzione
            result = DialogResult.None
            ii = 0
            While en.MoveNext()
                ImgVet(ii) = en.Value
                ii += 1
                Me.Refresh()
            End While
            rsxr.Close()

            Select Case (icona)
                Case MessageBoxIcon.Question
                    PIcona.Image = ImgVet(0)
                Case MessageBoxIcon.Information
                    PIcona.Image = ImgVet(0)
                Case MessageBoxIcon.Stop
                    PIcona.Image = ImgVet(2)

                Case MessageBoxIcon.Exclamation
                    PIcona.Image = ImgVet(1)
                Case MessageBoxIcon.Warning
                    PIcona.Image = ImgVet(1)
            End Select

            TTesto.Text = testo

            Select Case testo.Length
                Case 2 To 100
                Case 101 To 120
                    TTesto.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                Case Else
                    TTesto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            End Select


            If pulsanti = MessageBoxButtons.OK Then
                Bok.Visible = True

            End If

            If pulsanti = MessageBoxButtons.OKCancel Then
                Bok.Visible = True
                BAnnulla.Visible = True

            End If
            If pulsanti = MessageBoxButtons.YesNo Then
                Bok.Text = "S�"
                BAnnulla.Text = "No"
                Bok.Visible = True
                BAnnulla.Visible = True

            End If
            If (Imag.Image Is Nothing) Then

                Dim ss As New Size(Me.Size.Width, Me.Size.Height - 100)
                Me.Size = ss
                Dim sizet As New Size(TTesto.Size.Width, Me.Size.Height - 10)
                TTesto.Size = sizet
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub Bok_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bok.Click
        If Bok.Text = "OK" Then
            result = DialogResult.OK
        Else
            result = DialogResult.Yes
        End If
        Me.Close()
    End Sub

    Private Sub BAnnulla_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BAnnulla.Click
        If BAnnulla.Text = "Annulla" Then
            result = DialogResult.Abort
        Else
            result = DialogResult.Cancel
        End If
        Me.Close()
    End Sub

    Private Sub Message_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
      
        If result = DialogResult.None Then
            result = DialogResult.OK
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        fine = True
        result = DialogResult.Abort
        Me.Close()
    End Sub

    Private Sub Bzoom_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bzoom.Click
        'Dim f As New ZoomImage
        'f.FileCorrente = FileCorrente
        'f.DirectoryImage = Directory
        'f.ShowDialog()
    End Sub
    Private Sub Message_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        Select Case (e.KeyValue)
            Case Keys.Enter
                If Bok.Text = "OK" Then
                    result = DialogResult.OK
                    Me.Close()
                Else
                    result = DialogResult.Yes
                    Me.Close()
                End If
            Case Keys.Escape
                result = DialogResult.Abort
                Me.Close()
            Case Keys.Back
                result = DialogResult.No
                Me.Close()
            Case Keys.NumPad9
                '   Me.ParentForm.ReflectMessage(Me.ParentForm.Handle, )
        End Select


    End Sub

    Private Sub Message_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Application.DoEvents()
    End Sub

    Private Sub TTesto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TTesto.Click

    End Sub
End Class
