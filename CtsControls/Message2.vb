Imports System.Windows.Forms
Imports System.Drawing

Public Class Message2
    Inherits System.Windows.Forms.Form

    Dim ImgVet(10) As Image
    Public icona As MessageBoxIcon
    Public pulsanti As MessageBoxButtons
    Public testo As String
    Public result As DialogResult
    Public fine As Boolean
    Public Directory As String
    Public DurataFinestra As Integer
    Public WithEvents LId As System.Windows.Forms.Label
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents TTesto As System.Windows.Forms.TextBox
    Public WithEvents PUser As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Public WithEvents LbLedSe1 As LBSoft.IndustrialCtrls.Leds.LBLed
    Public WithEvents Label2 As System.Windows.Forms.Label
    Public FileCorrente As String


#Region " Codice generato da Progettazione Windows Form "

    Public Sub New()
        MyBase.New()

        'Chiamata richiesta da Progettazione Windows Form.
        InitializeComponent()

        'Aggiungere le eventuali istruzioni di inizializzazione dopo la chiamata a InitializeComponent()

    End Sub

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue � richiesta da Progettazione Windows Form.
    'Pu� essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    Friend WithEvents PIcona As System.Windows.Forms.PictureBox
    Public WithEvents Imag As System.Windows.Forms.PictureBox
    Friend WithEvents Bok As System.Windows.Forms.Button
    Friend WithEvents BAnnulla As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Bzoom As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Message2))
        Me.Bok = New System.Windows.Forms.Button
        Me.BAnnulla = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.Bzoom = New System.Windows.Forms.Button
        Me.LId = New System.Windows.Forms.Label
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.TTesto = New System.Windows.Forms.TextBox
        Me.PUser = New System.Windows.Forms.PictureBox
        Me.Imag = New System.Windows.Forms.PictureBox
        Me.PIcona = New System.Windows.Forms.PictureBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.LbLedSe1 = New LBSoft.IndustrialCtrls.Leds.LBLed
        Me.Label2 = New System.Windows.Forms.Label
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.PUser, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Imag, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PIcona, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Bok
        '
        Me.Bok.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Bok.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Bok.Location = New System.Drawing.Point(601, 317)
        Me.Bok.Name = "Bok"
        Me.Bok.Size = New System.Drawing.Size(64, 43)
        Me.Bok.TabIndex = 3
        Me.Bok.Text = "OK"
        Me.Bok.UseVisualStyleBackColor = False
        Me.Bok.Visible = False
        '
        'BAnnulla
        '
        Me.BAnnulla.Location = New System.Drawing.Point(7, 177)
        Me.BAnnulla.Name = "BAnnulla"
        Me.BAnnulla.Size = New System.Drawing.Size(51, 32)
        Me.BAnnulla.TabIndex = 4
        Me.BAnnulla.Text = "Annulla"
        Me.BAnnulla.Visible = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.Location = New System.Drawing.Point(8, 314)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(64, 43)
        Me.Button1.TabIndex = 7
        Me.Button1.Text = "Annulla Collaudo"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Bzoom
        '
        Me.Bzoom.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Bzoom.Location = New System.Drawing.Point(64, 177)
        Me.Bzoom.Name = "Bzoom"
        Me.Bzoom.Size = New System.Drawing.Size(51, 32)
        Me.Bzoom.TabIndex = 8
        Me.Bzoom.Text = "Zoom Immagine"
        Me.Bzoom.Visible = False
        '
        'LId
        '
        Me.LId.AutoSize = True
        Me.LId.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LId.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.LId.Location = New System.Drawing.Point(626, 268)
        Me.LId.Name = "LId"
        Me.LId.Size = New System.Drawing.Size(39, 20)
        Me.LId.TabIndex = 10
        Me.LId.Text = "000"
        Me.LId.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.LId.Visible = False
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(141, 8)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.TTesto)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.PUser)
        Me.SplitContainer1.Size = New System.Drawing.Size(432, 352)
        Me.SplitContainer1.SplitterDistance = 191
        Me.SplitContainer1.TabIndex = 11
        '
        'TTesto
        '
        Me.TTesto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TTesto.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TTesto.Location = New System.Drawing.Point(0, 0)
        Me.TTesto.Multiline = True
        Me.TTesto.Name = "TTesto"
        Me.TTesto.ReadOnly = True
        Me.TTesto.Size = New System.Drawing.Size(432, 191)
        Me.TTesto.TabIndex = 10
        Me.TTesto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'PUser
        '
        Me.PUser.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PUser.Location = New System.Drawing.Point(0, 0)
        Me.PUser.Name = "PUser"
        Me.PUser.Size = New System.Drawing.Size(432, 157)
        Me.PUser.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PUser.TabIndex = 0
        Me.PUser.TabStop = False
        '
        'Imag
        '
        Me.Imag.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Imag.Location = New System.Drawing.Point(64, 221)
        Me.Imag.Name = "Imag"
        Me.Imag.Size = New System.Drawing.Size(124, 136)
        Me.Imag.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Imag.TabIndex = 2
        Me.Imag.TabStop = False
        '
        'PIcona
        '
        Me.PIcona.Location = New System.Drawing.Point(8, 8)
        Me.PIcona.Name = "PIcona"
        Me.PIcona.Size = New System.Drawing.Size(127, 130)
        Me.PIcona.TabIndex = 0
        Me.PIcona.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(594, 273)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(27, 13)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "Msg"
        Me.Label1.Visible = False
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'LbLedSe1
        '
        Me.LbLedSe1.BackColor = System.Drawing.Color.Transparent
        Me.LbLedSe1.BlinkInterval = 500
        Me.LbLedSe1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbLedSe1.Label = "0000"
        Me.LbLedSe1.LabelPosition = LBSoft.IndustrialCtrls.Leds.LBLed.LedLabelPosition.Bottom
        Me.LbLedSe1.LedColor = System.Drawing.Color.Chartreuse
        Me.LbLedSe1.LedSize = New System.Drawing.SizeF(50.0!, 50.0!)
        Me.LbLedSe1.Location = New System.Drawing.Point(591, 81)
        Me.LbLedSe1.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.LbLedSe1.Name = "LbLedSe1"
        Me.LbLedSe1.Renderer = Nothing
        Me.LbLedSe1.Size = New System.Drawing.Size(95, 110)
        Me.LbLedSe1.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.Off
        Me.LbLedSe1.TabIndex = 13
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label2.Location = New System.Drawing.Point(587, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(93, 66)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Tempo Rimasto"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Message2
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(692, 366)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.LbLedSe1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.LId)
        Me.Controls.Add(Me.Bzoom)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.BAnnulla)
        Me.Controls.Add(Me.Bok)
        Me.Controls.Add(Me.Imag)
        Me.Controls.Add(Me.PIcona)
        Me.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Message2"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.PUser, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Imag, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PIcona, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub Message_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed


        'Application.DoEvents()
    End Sub

    Private Sub Message_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim rsxr As Resources.ResXResourceReader
        Dim a As Resources.ResourceSet
        Dim punto As New Point
        Dim path As String
        Dim en As IDictionaryEnumerator
        path = My.Application.Info.DirectoryPath + "\img.resx"

        '' Dim fof As Font

        fine = False
        rsxr = New Resources.ResXResourceReader(path)
        rsxr.BasePath = path
        Try
            a = New Resources.ResourceSet(rsxr)
            en = a.GetEnumerator()

            Dim id As IDictionaryEnumerator = rsxr.GetEnumerator()

            ' Iterate through the resources and display the contents to the console.
            Dim d As DictionaryEntry
            For Each d In rsxr
                Console.WriteLine(d.Key.ToString() + ":" + ControlChars.Tab + d.Value.ToString())
            Next d



            punto = Me.Location
            punto.Y += 280
            'Me.Location = punto
            Me.StartPosition = FormStartPosition.CenterScreen
            ' Goes through the enumerator, printing out the key and value pairs.
            Dim ii As Short
            ' finestra di massaggio in esecuzione
            result = DialogResult.None
            ii = 0
            While en.MoveNext()
                ImgVet(ii) = en.Value
                ii += 1
                Me.Refresh()
            End While
            rsxr.Close()

            Select Case (icona)
                Case MessageBoxIcon.Question
                    PIcona.Image = ImgVet(0)
                Case MessageBoxIcon.Information
                    PIcona.Image = ImgVet(0)
                Case MessageBoxIcon.Stop
                    PIcona.Image = ImgVet(2)

                Case MessageBoxIcon.Exclamation
                    PIcona.Image = ImgVet(1)
                Case MessageBoxIcon.Warning
                    PIcona.Image = ImgVet(1)
            End Select

            TTesto.Text = testo

            Select Case testo.Length
                Case 2 To 100
                    TTesto.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                Case 101 To 120
                    TTesto.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                Case Else
                    'TTesto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                    TTesto.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            End Select


            If pulsanti = MessageBoxButtons.OK Then
                Bok.Visible = True

            End If

            If pulsanti = MessageBoxButtons.OKCancel Then
                Bok.Visible = True
                BAnnulla.Visible = True

            End If
            If pulsanti = MessageBoxButtons.YesNo Then
                Bok.Text = "S�"
                BAnnulla.Text = "No"
                Bok.Visible = True
                BAnnulla.Visible = True

            End If
            If (Imag.Image Is Nothing) Then

                'Dim ss As New Size(Me.Size.Width, Me.Size.Height - 100)
                'Me.Size = ss
                'Dim sizet As New Size(TTesto.Size.Width, Me.Size.Height - 10)
                'TTesto.Size = sizet
            End If

            If (PUser.Image Is Nothing) Then
                SplitContainer1.Panel2Collapsed = True
            Else
                SplitContainer1.Panel2Collapsed = False
            End If

            LbLedSe1.LedColor = Color.Chartreuse
            LbLedSe1.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.Blink
            LbLedSe1.BlinkInterval = 500
            DurataFinestra = 30
            Timer1.Start()

        Catch ex As Exception

        End Try
    End Sub

    Private Sub Bok_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bok.Click
        If Bok.Text = "OK" Then
            result = DialogResult.OK
        Else
            result = DialogResult.Yes
        End If
        Me.Close()
    End Sub

    Private Sub BAnnulla_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BAnnulla.Click
        If BAnnulla.Text = "Annulla" Then
            result = DialogResult.Abort
        Else
            result = DialogResult.Cancel
        End If
        Me.Close()
    End Sub

    Private Sub Message_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        If result = DialogResult.None Then
            result = DialogResult.OK
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        fine = True
        result = DialogResult.Abort
        Me.Close()
    End Sub

    Private Sub Bzoom_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Bzoom.Click
        'Dim f As New ZoomImage
        'f.FileCorrente = FileCorrente
        'f.DirectoryImage = Directory
        'f.ShowDialog()
    End Sub
    Private Sub Message_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        Select Case (e.KeyValue)
            Case Keys.Enter
                If Bok.Text = "OK" Then
                    result = DialogResult.OK
                    Me.Close()
                Else
                    result = DialogResult.Yes
                    Me.Close()
                End If
            Case Keys.Escape
                result = DialogResult.Abort
                Me.Close()
            Case Keys.Back
                result = DialogResult.No
                Me.Close()
            Case Keys.NumPad9
                '   Me.ParentForm.ReflectMessage(Me.ParentForm.Handle, )
        End Select


    End Sub

    Private Sub Message_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Application.DoEvents()
    End Sub

    Private Sub LId_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LId.Click

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        DurataFinestra = DurataFinestra - 1
        If DurataFinestra < 20 Then
            LbLedSe1.LedColor = Color.Yellow
            LbLedSe1.BlinkInterval = 150
        End If
        If DurataFinestra < 10 Then
            LbLedSe1.LedColor = Color.Red
            LbLedSe1.BlinkInterval = 100
        End If
        If DurataFinestra <= 0 Then
            Timer1.Stop()
            result = DialogResult.Abort
            Me.Close()
        End If
        LbLedSe1.Label = DurataFinestra.ToString()
    End Sub
End Class
