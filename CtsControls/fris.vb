Imports System.Drawing
Public Class Frisul
    Inherits System.Windows.Forms.Form

#Region " Codice generato da Progettazione Windows Form "

    Public Sub New()
        MyBase.New()

        'Chiamata richiesta da Progettazione Windows Form.
        InitializeComponent()

        'Aggiungere le eventuali istruzioni di inizializzazione dopo la chiamata a InitializeComponent()

    End Sub

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer
    Public WithEvents L1 As System.Windows.Forms.Label
    Public WithEvents L2 As System.Windows.Forms.Label
    Public WithEvents L3 As System.Windows.Forms.Label
    Public WithEvents L4 As System.Windows.Forms.Label
    Public WithEvents L5 As System.Windows.Forms.Label
    Public WithEvents L6 As System.Windows.Forms.Label

    'NOTA: la procedura che segue � richiesta da Progettazione Windows Form.
    'Pu� essere modificata in Progettazione Windows Form.  
    'Non modificarla nell'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.L1 = New System.Windows.Forms.Label
        Me.L2 = New System.Windows.Forms.Label
        Me.L3 = New System.Windows.Forms.Label
        Me.L5 = New System.Windows.Forms.Label
        Me.L4 = New System.Windows.Forms.Label
        Me.L6 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'L1
        '
        Me.L1.BackColor = System.Drawing.Color.Transparent
        Me.L1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.L1.ForeColor = System.Drawing.Color.Red
        Me.L1.Location = New System.Drawing.Point(16, 16)
        Me.L1.Name = "L1"
        Me.L1.Size = New System.Drawing.Size(248, 32)
        Me.L1.TabIndex = 0
        Me.L1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'L2
        '
        Me.L2.BackColor = System.Drawing.Color.Transparent
        Me.L2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.L2.ForeColor = System.Drawing.Color.Red
        Me.L2.Location = New System.Drawing.Point(16, 56)
        Me.L2.Name = "L2"
        Me.L2.Size = New System.Drawing.Size(248, 32)
        Me.L2.TabIndex = 1
        Me.L2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'L3
        '
        Me.L3.BackColor = System.Drawing.Color.Transparent
        Me.L3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.L3.ForeColor = System.Drawing.Color.Red
        Me.L3.Location = New System.Drawing.Point(16, 96)
        Me.L3.Name = "L3"
        Me.L3.Size = New System.Drawing.Size(248, 32)
        Me.L3.TabIndex = 2
        Me.L3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'L5
        '
        Me.L5.BackColor = System.Drawing.Color.Transparent
        Me.L5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.L5.ForeColor = System.Drawing.Color.Red
        Me.L5.Location = New System.Drawing.Point(16, 176)
        Me.L5.Name = "L5"
        Me.L5.Size = New System.Drawing.Size(248, 32)
        Me.L5.TabIndex = 3
        Me.L5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'L4
        '
        Me.L4.BackColor = System.Drawing.Color.Transparent
        Me.L4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.L4.ForeColor = System.Drawing.Color.Red
        Me.L4.Location = New System.Drawing.Point(16, 136)
        Me.L4.Name = "L4"
        Me.L4.Size = New System.Drawing.Size(248, 32)
        Me.L4.TabIndex = 4
        Me.L4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'L6
        '
        Me.L6.BackColor = System.Drawing.Color.Transparent
        Me.L6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.L6.ForeColor = System.Drawing.Color.Red
        Me.L6.Location = New System.Drawing.Point(16, 216)
        Me.L6.Name = "L6"
        Me.L6.Size = New System.Drawing.Size(248, 32)
        Me.L6.TabIndex = 5
        Me.L6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Frisul
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(280, 264)
        Me.Controls.Add(Me.L6)
        Me.Controls.Add(Me.L4)
        Me.Controls.Add(Me.L5)
        Me.Controls.Add(Me.L3)
        Me.Controls.Add(Me.L2)
        Me.Controls.Add(Me.L1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Frisul"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Risultati letture"
        Me.TopMost = True
        Me.ResumeLayout(False)

    End Sub

#End Region

    Public TipoControllo As Short


    Public Sub Reset()
        L1.ForeColor = Color.Red
        L2.ForeColor = Color.Red
        L3.ForeColor = Color.Red
        L4.ForeColor = Color.Red
        L5.ForeColor = Color.Red
        L6.ForeColor = Color.Red
    End Sub



    Private Sub Frisul_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim p As New Point(50, 450)
        Me.Location = p

        Dim Strview As String

        If (TipoControllo = 16) Then
            Strview = "Letti DOCUTEST  n� "
            L1.Text = Strview
            Strview = "Letti DOCUTEST  n� "
            L2.Text = Strview
            L3.Text = ""
            L3.Visible = False
            L4.Text = ""
            L4.Visible = False
            L5.Text = ""
            L5.Visible = False
            L6.Text = ""
            L6.Visible = False
        Else
            Strview = "Letti DOCUTEST  n� "
            L1.Text = Strview
            Strview = "Letti DOCUTEST  n� "
            L2.Text = Strview
            Strview = "Letti DOCUTEST  n� "
            L3.Text = Strview
            Strview = "Letti DOCUTEST  n� "
            L4.Text = Strview
            Strview = "Letti DOCUTEST  n� "
            L5.Text = Strview
            Strview = "Letti DOCUTEST  n� "
            L6.Text = Strview
        End If
       

    End Sub

    Private Sub L1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles L1.Click

    End Sub
End Class
