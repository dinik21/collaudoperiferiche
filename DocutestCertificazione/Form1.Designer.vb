﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.BtDocSecondari = New System.Windows.Forms.Button
        Me.BtDocTest = New System.Windows.Forms.Button
        Me.gpValori = New System.Windows.Forms.GroupBox
        Me.txtcomLevelBI = New System.Windows.Forms.TextBox
        Me.lblcomLevelBI = New System.Windows.Forms.Label
        Me.txtcomLevelGr = New System.Windows.Forms.TextBox
        Me.lblcomLevelGr = New System.Windows.Forms.Label
        Me.txtcomLevelRd = New System.Windows.Forms.TextBox
        Me.lblcomLevelRd = New System.Windows.Forms.Label
        Me.txtcomLevelGray = New System.Windows.Forms.TextBox
        Me.lblcomLevelGray = New System.Windows.Forms.Label
        Me.LBLtesto = New System.Windows.Forms.Label
        Me.grpUtente = New System.Windows.Forms.GroupBox
        Me.lblGRAY = New System.Windows.Forms.Label
        Me.lblBL = New System.Windows.Forms.Label
        Me.lblGR = New System.Windows.Forms.Label
        Me.lblRD = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.GbResult = New System.Windows.Forms.GroupBox
        Me.gbLottoResult = New System.Windows.Forms.GroupBox
        Me.LbLottoLotto = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.LbLottoGray = New System.Windows.Forms.Label
        Me.LbLottoBL = New System.Windows.Forms.Label
        Me.LbLottoGR = New System.Windows.Forms.Label
        Me.LbLottoRD = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.gpValori.SuspendLayout()
        Me.grpUtente.SuspendLayout()
        Me.gbLottoResult.SuspendLayout()
        Me.SuspendLayout()
        '
        'BtDocSecondari
        '
        Me.BtDocSecondari.Location = New System.Drawing.Point(46, 24)
        Me.BtDocSecondari.Name = "BtDocSecondari"
        Me.BtDocSecondari.Size = New System.Drawing.Size(103, 53)
        Me.BtDocSecondari.TabIndex = 1
        Me.BtDocSecondari.Text = "Certificazione Doc. Secondari"
        Me.BtDocSecondari.UseVisualStyleBackColor = True
        '
        'BtDocTest
        '
        Me.BtDocTest.Location = New System.Drawing.Point(280, 24)
        Me.BtDocTest.Name = "BtDocTest"
        Me.BtDocTest.Size = New System.Drawing.Size(103, 53)
        Me.BtDocTest.TabIndex = 0
        Me.BtDocTest.Text = "Assegnazione Lotto DocuTest"
        Me.BtDocTest.UseVisualStyleBackColor = True
        '
        'gpValori
        '
        Me.gpValori.Controls.Add(Me.txtcomLevelBI)
        Me.gpValori.Controls.Add(Me.lblcomLevelBI)
        Me.gpValori.Controls.Add(Me.txtcomLevelGr)
        Me.gpValori.Controls.Add(Me.lblcomLevelGr)
        Me.gpValori.Controls.Add(Me.txtcomLevelRd)
        Me.gpValori.Controls.Add(Me.lblcomLevelRd)
        Me.gpValori.Controls.Add(Me.txtcomLevelGray)
        Me.gpValori.Controls.Add(Me.lblcomLevelGray)
        Me.gpValori.Location = New System.Drawing.Point(54, 468)
        Me.gpValori.Name = "gpValori"
        Me.gpValori.Size = New System.Drawing.Size(320, 168)
        Me.gpValori.TabIndex = 2
        Me.gpValori.TabStop = False
        Me.gpValori.Visible = False
        '
        'txtcomLevelBI
        '
        Me.txtcomLevelBI.ForeColor = System.Drawing.SystemColors.GrayText
        Me.txtcomLevelBI.Location = New System.Drawing.Point(182, 132)
        Me.txtcomLevelBI.MaxLength = 3
        Me.txtcomLevelBI.Name = "txtcomLevelBI"
        Me.txtcomLevelBI.Size = New System.Drawing.Size(117, 20)
        Me.txtcomLevelBI.TabIndex = 7
        Me.txtcomLevelBI.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblcomLevelBI
        '
        Me.lblcomLevelBI.AutoSize = True
        Me.lblcomLevelBI.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblcomLevelBI.ForeColor = System.Drawing.SystemColors.GrayText
        Me.lblcomLevelBI.Location = New System.Drawing.Point(21, 127)
        Me.lblcomLevelBI.Name = "lblcomLevelBI"
        Me.lblcomLevelBI.Size = New System.Drawing.Size(133, 25)
        Me.lblcomLevelBI.TabIndex = 6
        Me.lblcomLevelBI.Text = "comLevelBI"
        '
        'txtcomLevelGr
        '
        Me.txtcomLevelGr.ForeColor = System.Drawing.SystemColors.GrayText
        Me.txtcomLevelGr.Location = New System.Drawing.Point(182, 98)
        Me.txtcomLevelGr.MaxLength = 3
        Me.txtcomLevelGr.Name = "txtcomLevelGr"
        Me.txtcomLevelGr.Size = New System.Drawing.Size(117, 20)
        Me.txtcomLevelGr.TabIndex = 5
        Me.txtcomLevelGr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblcomLevelGr
        '
        Me.lblcomLevelGr.AutoSize = True
        Me.lblcomLevelGr.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblcomLevelGr.ForeColor = System.Drawing.SystemColors.GrayText
        Me.lblcomLevelGr.Location = New System.Drawing.Point(25, 93)
        Me.lblcomLevelGr.Name = "lblcomLevelGr"
        Me.lblcomLevelGr.Size = New System.Drawing.Size(137, 25)
        Me.lblcomLevelGr.TabIndex = 4
        Me.lblcomLevelGr.Text = "comLevelGr"
        '
        'txtcomLevelRd
        '
        Me.txtcomLevelRd.ForeColor = System.Drawing.SystemColors.GrayText
        Me.txtcomLevelRd.Location = New System.Drawing.Point(182, 64)
        Me.txtcomLevelRd.MaxLength = 3
        Me.txtcomLevelRd.Name = "txtcomLevelRd"
        Me.txtcomLevelRd.Size = New System.Drawing.Size(117, 20)
        Me.txtcomLevelRd.TabIndex = 3
        Me.txtcomLevelRd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblcomLevelRd
        '
        Me.lblcomLevelRd.AutoSize = True
        Me.lblcomLevelRd.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblcomLevelRd.ForeColor = System.Drawing.SystemColors.GrayText
        Me.lblcomLevelRd.Location = New System.Drawing.Point(21, 59)
        Me.lblcomLevelRd.Name = "lblcomLevelRd"
        Me.lblcomLevelRd.Size = New System.Drawing.Size(141, 25)
        Me.lblcomLevelRd.TabIndex = 2
        Me.lblcomLevelRd.Text = "comLevelRd"
        '
        'txtcomLevelGray
        '
        Me.txtcomLevelGray.ForeColor = System.Drawing.SystemColors.GrayText
        Me.txtcomLevelGray.Location = New System.Drawing.Point(182, 30)
        Me.txtcomLevelGray.MaxLength = 3
        Me.txtcomLevelGray.Name = "txtcomLevelGray"
        Me.txtcomLevelGray.Size = New System.Drawing.Size(117, 20)
        Me.txtcomLevelGray.TabIndex = 1
        Me.txtcomLevelGray.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblcomLevelGray
        '
        Me.lblcomLevelGray.AutoSize = True
        Me.lblcomLevelGray.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblcomLevelGray.ForeColor = System.Drawing.SystemColors.GrayText
        Me.lblcomLevelGray.Location = New System.Drawing.Point(21, 25)
        Me.lblcomLevelGray.Name = "lblcomLevelGray"
        Me.lblcomLevelGray.Size = New System.Drawing.Size(162, 25)
        Me.lblcomLevelGray.TabIndex = 0
        Me.lblcomLevelGray.Text = "comLevelGray"
        '
        'LBLtesto
        '
        Me.LBLtesto.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LBLtesto.ForeColor = System.Drawing.SystemColors.GrayText
        Me.LBLtesto.Location = New System.Drawing.Point(29, 97)
        Me.LBLtesto.Name = "LBLtesto"
        Me.LBLtesto.Size = New System.Drawing.Size(371, 171)
        Me.LBLtesto.TabIndex = 9
        '
        'grpUtente
        '
        Me.grpUtente.Controls.Add(Me.lblGRAY)
        Me.grpUtente.Controls.Add(Me.lblBL)
        Me.grpUtente.Controls.Add(Me.lblGR)
        Me.grpUtente.Controls.Add(Me.lblRD)
        Me.grpUtente.Controls.Add(Me.Label1)
        Me.grpUtente.Controls.Add(Me.Label3)
        Me.grpUtente.Controls.Add(Me.Label4)
        Me.grpUtente.Controls.Add(Me.Label5)
        Me.grpUtente.Location = New System.Drawing.Point(116, 313)
        Me.grpUtente.Name = "grpUtente"
        Me.grpUtente.Size = New System.Drawing.Size(196, 149)
        Me.grpUtente.TabIndex = 14
        Me.grpUtente.TabStop = False
        Me.grpUtente.Visible = False
        '
        'lblGRAY
        '
        Me.lblGRAY.AutoSize = True
        Me.lblGRAY.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGRAY.ForeColor = System.Drawing.SystemColors.GrayText
        Me.lblGRAY.Location = New System.Drawing.Point(130, 22)
        Me.lblGRAY.Name = "lblGRAY"
        Me.lblGRAY.Size = New System.Drawing.Size(25, 25)
        Me.lblGRAY.TabIndex = 13
        Me.lblGRAY.Text = "0"
        '
        'lblBL
        '
        Me.lblBL.AutoSize = True
        Me.lblBL.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBL.ForeColor = System.Drawing.SystemColors.GrayText
        Me.lblBL.Location = New System.Drawing.Point(130, 112)
        Me.lblBL.Name = "lblBL"
        Me.lblBL.Size = New System.Drawing.Size(25, 25)
        Me.lblBL.TabIndex = 12
        Me.lblBL.Text = "0"
        '
        'lblGR
        '
        Me.lblGR.AutoSize = True
        Me.lblGR.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGR.ForeColor = System.Drawing.SystemColors.GrayText
        Me.lblGR.Location = New System.Drawing.Point(130, 82)
        Me.lblGR.Name = "lblGR"
        Me.lblGR.Size = New System.Drawing.Size(25, 25)
        Me.lblGR.TabIndex = 11
        Me.lblGR.Text = "0"
        '
        'lblRD
        '
        Me.lblRD.AutoSize = True
        Me.lblRD.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRD.ForeColor = System.Drawing.SystemColors.GrayText
        Me.lblRD.Location = New System.Drawing.Point(130, 52)
        Me.lblRD.Name = "lblRD"
        Me.lblRD.Size = New System.Drawing.Size(25, 25)
        Me.lblRD.TabIndex = 10
        Me.lblRD.Text = "0"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.GrayText
        Me.Label1.Location = New System.Drawing.Point(42, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(76, 25)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "GRAY"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.GrayText
        Me.Label3.Location = New System.Drawing.Point(42, 112)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 25)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "BL"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.GrayText
        Me.Label4.Location = New System.Drawing.Point(42, 82)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(45, 25)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "GR"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.GrayText
        Me.Label5.Location = New System.Drawing.Point(43, 52)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(44, 25)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "RD"
        '
        'GbResult
        '
        Me.GbResult.Location = New System.Drawing.Point(163, 24)
        Me.GbResult.Name = "GbResult"
        Me.GbResult.Size = New System.Drawing.Size(103, 53)
        Me.GbResult.TabIndex = 11
        Me.GbResult.TabStop = False
        Me.GbResult.Text = "Result"
        '
        'gbLottoResult
        '
        Me.gbLottoResult.Controls.Add(Me.LbLottoLotto)
        Me.gbLottoResult.Controls.Add(Me.Label14)
        Me.gbLottoResult.Controls.Add(Me.LbLottoGray)
        Me.gbLottoResult.Controls.Add(Me.LbLottoBL)
        Me.gbLottoResult.Controls.Add(Me.LbLottoGR)
        Me.gbLottoResult.Controls.Add(Me.LbLottoRD)
        Me.gbLottoResult.Controls.Add(Me.Label9)
        Me.gbLottoResult.Controls.Add(Me.Label10)
        Me.gbLottoResult.Controls.Add(Me.Label11)
        Me.gbLottoResult.Controls.Add(Me.Label12)
        Me.gbLottoResult.Location = New System.Drawing.Point(116, 280)
        Me.gbLottoResult.Name = "gbLottoResult"
        Me.gbLottoResult.Size = New System.Drawing.Size(196, 182)
        Me.gbLottoResult.TabIndex = 10
        Me.gbLottoResult.TabStop = False
        Me.gbLottoResult.Visible = False
        '
        'LbLottoLotto
        '
        Me.LbLottoLotto.AutoSize = True
        Me.LbLottoLotto.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbLottoLotto.ForeColor = System.Drawing.SystemColors.GrayText
        Me.LbLottoLotto.Location = New System.Drawing.Point(130, 142)
        Me.LbLottoLotto.Name = "LbLottoLotto"
        Me.LbLottoLotto.Size = New System.Drawing.Size(25, 25)
        Me.LbLottoLotto.TabIndex = 15
        Me.LbLottoLotto.Text = "0"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.SystemColors.GrayText
        Me.Label14.Location = New System.Drawing.Point(42, 142)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(65, 25)
        Me.Label14.TabIndex = 14
        Me.Label14.Text = "Lotto"
        '
        'LbLottoGray
        '
        Me.LbLottoGray.AutoSize = True
        Me.LbLottoGray.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbLottoGray.ForeColor = System.Drawing.SystemColors.GrayText
        Me.LbLottoGray.Location = New System.Drawing.Point(130, 22)
        Me.LbLottoGray.Name = "LbLottoGray"
        Me.LbLottoGray.Size = New System.Drawing.Size(25, 25)
        Me.LbLottoGray.TabIndex = 13
        Me.LbLottoGray.Text = "0"
        '
        'LbLottoBL
        '
        Me.LbLottoBL.AutoSize = True
        Me.LbLottoBL.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbLottoBL.ForeColor = System.Drawing.SystemColors.GrayText
        Me.LbLottoBL.Location = New System.Drawing.Point(130, 112)
        Me.LbLottoBL.Name = "LbLottoBL"
        Me.LbLottoBL.Size = New System.Drawing.Size(25, 25)
        Me.LbLottoBL.TabIndex = 12
        Me.LbLottoBL.Text = "0"
        '
        'LbLottoGR
        '
        Me.LbLottoGR.AutoSize = True
        Me.LbLottoGR.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbLottoGR.ForeColor = System.Drawing.SystemColors.GrayText
        Me.LbLottoGR.Location = New System.Drawing.Point(130, 82)
        Me.LbLottoGR.Name = "LbLottoGR"
        Me.LbLottoGR.Size = New System.Drawing.Size(25, 25)
        Me.LbLottoGR.TabIndex = 11
        Me.LbLottoGR.Text = "0"
        '
        'LbLottoRD
        '
        Me.LbLottoRD.AutoSize = True
        Me.LbLottoRD.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbLottoRD.ForeColor = System.Drawing.SystemColors.GrayText
        Me.LbLottoRD.Location = New System.Drawing.Point(130, 52)
        Me.LbLottoRD.Name = "LbLottoRD"
        Me.LbLottoRD.Size = New System.Drawing.Size(25, 25)
        Me.LbLottoRD.TabIndex = 10
        Me.LbLottoRD.Text = "0"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.GrayText
        Me.Label9.Location = New System.Drawing.Point(42, 22)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(76, 25)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = "GRAY"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.GrayText
        Me.Label10.Location = New System.Drawing.Point(42, 112)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(40, 25)
        Me.Label10.TabIndex = 4
        Me.Label10.Text = "BL"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.GrayText
        Me.Label11.Location = New System.Drawing.Point(42, 82)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(45, 25)
        Me.Label11.TabIndex = 2
        Me.Label11.Text = "GR"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.SystemColors.GrayText
        Me.Label12.Location = New System.Drawing.Point(43, 52)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(44, 25)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "RD"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(429, 650)
        Me.Controls.Add(Me.gbLottoResult)
        Me.Controls.Add(Me.GbResult)
        Me.Controls.Add(Me.grpUtente)
        Me.Controls.Add(Me.gpValori)
        Me.Controls.Add(Me.BtDocTest)
        Me.Controls.Add(Me.BtDocSecondari)
        Me.Controls.Add(Me.LBLtesto)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Calibrazione Docutest 309   13-02-2019"
        Me.gpValori.ResumeLayout(False)
        Me.gpValori.PerformLayout()
        Me.grpUtente.ResumeLayout(False)
        Me.grpUtente.PerformLayout()
        Me.gbLottoResult.ResumeLayout(False)
        Me.gbLottoResult.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents BtDocSecondari As Button
    Friend WithEvents BtDocTest As Button
    Friend WithEvents gpValori As GroupBox
    Friend WithEvents txtcomLevelBI As TextBox
    Friend WithEvents lblcomLevelBI As Label
    Friend WithEvents txtcomLevelGr As TextBox
    Friend WithEvents lblcomLevelGr As Label
    Friend WithEvents txtcomLevelRd As TextBox
    Friend WithEvents lblcomLevelRd As Label
    Friend WithEvents txtcomLevelGray As TextBox
    Friend WithEvents lblcomLevelGray As Label
    Friend WithEvents LBLtesto As Label
    Friend WithEvents grpUtente As GroupBox
    Friend WithEvents lblGRAY As Label
    Friend WithEvents lblBL As Label
    Friend WithEvents lblGR As Label
    Friend WithEvents lblRD As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents GbResult As GroupBox
    Friend WithEvents gbLottoResult As GroupBox
    Friend WithEvents LbLottoLotto As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents LbLottoGray As Label
    Friend WithEvents LbLottoBL As Label
    Friend WithEvents LbLottoGR As Label
    Friend WithEvents LbLottoRD As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
End Class
