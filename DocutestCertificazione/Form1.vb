﻿Imports System.IO
Imports System.Text

Public Class Form1


    Dim LsInterface As Ls150Class.Periferica

    Dim RDfr_1 As Integer
    Dim RDfr_2 As Integer
    Dim GRfr_1 As Integer
    Dim GRfr_2 As Integer
    Dim BLfr_1 As Integer
    Dim BLfr_2 As Integer
    Dim RDre_1 As Integer
    Dim RDre_2 As Integer
    Dim GRre_1 As Integer
    Dim GRre_2 As Integer
    Dim BLre_1 As Integer
    Dim BLre_2 As Integer

    Dim areaSelectionCoords(4) As selectionCoordsStruct

    'Valori Caratteristici Documenti 
    Dim RD As Integer
    Dim GR As Integer
    Dim BL As Integer
    Dim GRAY As Integer

    Dim fStop As Boolean
    Dim fDocLotto As Boolean

    Dim ColorControl As Color

    'CERTIFICAZIONE DOC TEST / 'Valori Caratteristici Documenti 
    Dim RDfr As Integer
    Dim GRfr As Integer
    Dim BLfr As Integer
    Dim RDre As Integer
    Dim GRre As Integer
    Dim BLre As Integer
    Dim GrayColFR As Integer
    Dim GrayColRE As Integer
    Dim GrayCOLini As Integer
    Dim compLevelGraydoc As Integer
    Dim compLevelRDdoc As Integer
    Dim compLevelGRdoc As Integer
    Dim compLevelBLdoc As Integer
    Dim colorLevelThresholdINI As Integer

    Dim DELTArdfr As Integer
    Dim DELTAgrfr As Integer
    Dim DELTAblfr As Integer
    Dim DELTArdre As Integer
    Dim DELTAgrre As Integer
    Dim DELTAblre As Integer

    Dim WBerrfr As Integer
    Dim WBerrre As Integer

    Dim whiteBalanceThresholdINI As Integer
    Dim MaxDistanzaFronteRetro As Integer

    Dim KeyPressed As Integer
    Dim nomeFile As String
    Dim nomefileLang As String

    Dim compLevelRDIni As Integer
    Dim compLevelGRIni As Integer
    Dim compLevelBLIni As Integer
    Dim compGrayColIni As Integer

    Dim compLevelRDIni_Loti As Integer
    Dim compLevelGRIni_Loti As Integer
    Dim compLevelBLIni_Loti As Integer
    Dim compGrayColIni_Loti As Integer

    Dim NrCertificazioniSecondarieFatte As Integer
    Dim NrMaxCertificazioniSecondarie As Integer
    Dim NrAssegnazioneLottoFatte As Integer
    Dim NrMaxAssegnazioneLotto As Integer
    Dim TempoAttesaNuovaCertificazione As Integer
    Dim NoCalibrazione As Integer



#Region "Costanti ed Enum"

    Private Const HEADER_LENGHT As Integer = 54                     'DIMENSIONE HEADER (SENZA I COLORI)
    Private Const LOOKUPCOLOR_LENGHT As Integer = 1024              'DIMENSIONE TABELLA COLORI
    Private Const INI_FILENAME As String = "checkBMPAnalyzer.ini"   'Nome del file ini con la configurazione da caricare
    Private Const FILTERED_PATH As String = "\Filtered"
    Public Const NCOL As Integer = 3
    Public Enum COL
        RD = 0
        GR = 1
        BL = 2
    End Enum
    Public COL_NAME() As String = {"RD", "GR", "BL"}
    Public COL_COLOR() As Color = {Color.Red, Color.Green, Color.Blue}
#End Region

#Region "Variabili"

    Private matrixHeader(0 To HEADER_LENGHT - 1) As Byte                    'Header del file caricato
    Private lookupColors(0 To 3, 0 To LOOKUPCOLOR_LENGHT / 4 - 1) As Byte   'Tabella di transcodifica colori del file caricato

    Private checkGrayImage As Boolean                                       'Se True l'immagine è bianco e nero, se False a colori

    Private completeImage(,) As Byte                                        'Immagine caricata R,G,B sempre (codice da transcodificare)
    Private imgWidth, imgHeight As Integer                                  'Dimensioni dell'immagine caricata
    Private scaleFactor As Double                                           'Fattore di scala dell'immagine (sia altezza che larghezza)

    Private myBmp As Bitmap = Nothing                                       'Bitmap visualizzata

    Private checkAreaSelection As Boolean = False                           'Indica se è stata o meno selezionata un'area

    Private refreshRectangleAsynch As Boolean = False
    Private mouseClickIntoPictureBox As Boolean                             'Indica se sto cliccando o meno
    Private tempPosStart(0 To 1), tempPosStop(0 To 1) As Integer            'Posizione del rettangolo di selezione (idx=0->asseX, idx=1->asseY)
    Private RectSel As Rectangle = Nothing                                  'Rettangoli di selezione

    Private idxSelArea As Integer = 0                                       'Index dell'area da selezionare/selezionata
    Private numOfSelArea As Integer = 0                                     'Numero di aree totali da gestire
    Private radioButtonArea() As RadioButton = Nothing                      'RadioButtons per scegliere quali aree selezionare

    Private avgScanline(,) As Double                                         'Scanline media RGB dell'area selezionata
    Private compensationScanline(,) As Double                                'Scanline con i coefficienti moltiplicativi RGB di compensazione immediata

    Private checkLoadNewImage As Boolean = False                            'Controllo di caricamento di nuova immagine

    Private checkCompensationRequest As Boolean = False                     'Richiesta di compensazione dell'immagine caricata (con i coefficienti in compensationScanline)
    Private checkSWCompensationRequest As Boolean = False                   'ValNew = (ValOld - offsetX_coeffCorrection) + kCorrect_coeffCorrection + offsetY_coeffCorrection

    Private checkButterworthRequest As Boolean = False                      'Richiesta di caricare l'immagine con il filtro di Butterworth applicato

    Private offsetX_coeffCorrection(,) As Byte                               'ValNew = (ValOld - offsetX_coeffCorrection) + kCorrect_coeffCorrection + offsetY_coeffCorrection - RGB
    Private offsetY_coeffCorrection(,) As Byte                               'ValNew = (ValOld - offsetX_coeffCorrection) + kCorrect_coeffCorrection + offsetY_coeffCorrection - RGB
    Private kCorrect_coeffCorrection(,) As Double                            'ValNew = (ValOld - offsetX_coeffCorrection) + kCorrect_coeffCorrection + offsetY_coeffCorrection - RGB

    Private Structure selectionCoordsStruct
        Dim xStart As Integer
        Dim xStop As Integer
        Dim yStart As Integer
        Dim yStop As Integer
    End Structure



    'Private areaSelectionCoords() As selectionCoordsStruct                  'Coordinate delle aree selezionate
    'Private avgScanlineSelectionCoords As selectionCoordsStruct             'Coordinate dell'area di cui ottenere la scanline media
    'Private avgBlackScanlineSelectionCoords As selectionCoordsStruct        'Coordinate dell'area di cui ottenere la scanline media per il black
    'Private avgWhiteScanlineSelectionCoords As selectionCoordsStruct        'Coordinate dell'area di cui ottenere la scanline media per il white

    ' Private myFormCoeffTypeRequest As FormCoeffTypeRequest                  'Form di scelta dei coefficienti su cui agire
    'Private myFormResult As FormResults                                     'Form con i risultati dell'analisi in frequenza

    Private checkSaveBmp_FreqAnalysis As Boolean = False                    'Richiesta di salvataggio dei risultati dell'analisi in frequenza

    Private myStartupPath As String                                         'Cartella del programma (letta all'avvio)
#End Region

    Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
      "GetPrivateProfileStringA" (ByVal lpApplicationName As String, _
          ByVal lpKeyName As String, _
          ByVal lpDefault As String, _
          ByVal lpReturnedString As System.Text.StringBuilder, _
          ByVal nSize As Integer, ByVal lpFileName As String) As Integer

    Private Declare Ansi Function WritePrivateProfileString Lib "kernel32.dll" Alias "WritePrivateProfileStringA" _
        (ByVal lpApplicationName As String, _
         ByVal lpKeyName As String, _
         ByVal lpString As String, _
         ByVal lpFileName As String) _
    As Integer


    Public Function ReadValue(ByVal section As String, ByVal key As String, ByVal fileName As String) As String
        Dim sb As New StringBuilder
        sb = New StringBuilder(255)
        Dim i = GetPrivateProfileString(section, key, "0", sb, sb.Capacity, fileName)
        Return sb.ToString()
    End Function

    Public Sub WriteValue(ByVal section As String, ByVal key As String, ByVal value As String, ByVal fileName As String)
        WritePrivateProfileString(section, key, value, fileName)
    End Sub

    Public Sub ViewError(ByVal ReplyCode As Integer) 'BarraFasi.PhaseProgressBarItem)
        Dim sss As String
        Dim ccostante As New Ls150Class.Costanti
        sss = ""
        sss = ccostante.GetDesc(ReplyCode)
        MessageBox.Show(sss)
        'davide
        'Me.BackColor = Color.Red
        LsInterface.Reset(Ls150Class.Costanti.S_RESET_FREE_PATH)
        'MessageboxMy(ls150.ReturnC.FunzioneChiamante + " " + ls150.ReturnC.ReturnCode.ToString + "--> " + sss, "Attenzione !!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
    End Sub



    '**
    '* Calcolo base di media e deviazione standard di una bitmap passata come array di byte
    '*
    '* bitmapToAnalyze()    Bitmap in array di byte
    '* minResult            Minimo calcolato
    '* maxResult            Massimo calcolato
    '* avgResult            Media calcolata
    '* devStdResult         Deviazione standard calcolata
    '* entropyResult        Entropia calcolata
    '* histogramResult()    Istogramma della bitmap (nella selezione)
    '* coord                Coordinate di selezione
    '**
    Private Sub basicStatisticalRoutine(ByVal bitmapToAnalyze As Byte(), ByRef avgResult As Double, ByVal coord As selectionCoordsStruct)
        'Private Sub basicStatisticalRoutine(ByVal bitmapToAnalyze As Byte(), ByRef minResult As Double, ByRef maxResult As Double, ByRef avgResult As Double, ByRef devStdResult As Double, ByRef entropyResult As Double, ByRef nBlockKO As Integer, ByRef nBlockTOT As Integer, ByRef histogramResult() As Double, ByVal coord As selectionCoordsStruct)
        Dim nItemAvgDevStd As Integer = 0

        'minResult = Integer.MaxValue
        'maxResult = Integer.MinValue

        'Calcolo le medie
        avgResult = 0
        For j = 0 To imgHeight - 1
            For i As Integer = 0 To imgWidth - 1
                If (i >= coord.xStart And i <= coord.xStop) And (j >= coord.yStart And j <= coord.yStop) Then
                    'Calcolo massimo e minimo
                    'If bitmapToAnalyze(j * imgWidth + i) > maxResult Then maxResult = bitmapToAnalyze(j * imgWidth + i)
                    'If bitmapToAnalyze(j * imgWidth + i) < minResult Then minResult = bitmapToAnalyze(j * imgWidth + i)

                    'Calcolo la media
                    avgResult = avgResult + bitmapToAnalyze(j * imgWidth + i)
                    nItemAvgDevStd = nItemAvgDevStd + 1
                End If
            Next
        Next
        avgResult = avgResult / nItemAvgDevStd

        ''Calcolo le deviazioni standard
        'devStdResult = 0
        'For j = 0 To imgHeight - 1
        '    For i As Integer = 0 To imgWidth - 1
        '        If (i >= coord.xStart And i <= coord.xStop) And (j >= coord.yStart And j <= coord.yStop) Then
        '            devStdResult = devStdResult + ((bitmapToAnalyze(j * imgWidth + i) - avgResult) ^ 2)
        '        End If
        '    Next
        'Next
        'devStdResult = Math.Sqrt(devStdResult / nItemAvgDevStd)

        ''Calcolo l'istogramma e lo normalizzo per il numero di elementi della bitmap analizzati
        'For j = 0 To imgHeight - 1
        '    For i As Integer = 0 To imgWidth - 1
        '        If (i >= coord.xStart And i <= coord.xStop) And (j >= coord.yStart And j <= coord.yStop) Then
        '            histogramResult(bitmapToAnalyze(j * imgWidth + i)) += 1
        '        End If
        '    Next
        'Next
        'For i = 0 To histogramResult.Length - 1
        '    histogramResult(i) = histogramResult(i) / nItemAvgDevStd
        'Next

        ''Calcolo l'entropia
        'entropyResult = 0
        'For i = 0 To histogramResult.Length - 1
        '    If histogramResult(i) > 0 Then
        '        entropyResult = entropyResult + (histogramResult(i) * (Math.Log(1 / histogramResult(i)) / Math.Log(2)))
        '    End If
        'Next

        'Calcolo il numero di blocchi KO
        'Dim stepCounting As Integer = CInt(TextBoxBlockCountingFilterStep.Text)
        'Dim limitMin As Integer = CInt(TextBoxBlockCountingMin.Text)
        'Dim limitMax As Integer = CInt(TextBoxBlockCountingMax.Text)
        'nBlockKO = 0
        'nBlockTOT = 0
        'For j = coord.yStart To coord.yStop Step stepCounting
        'For i As Integer = coord.xStart To coord.xStop Step stepCounting
        'nBlockTOT = nBlockTOT + 1
        'If (bitmapToAnalyze(j * imgWidth + i) < limitMin) Or (bitmapToAnalyze(j * imgWidth + i) > limitMax) Then nBlockKO = nBlockKO + 1
        'Next
        'Next
    End Sub

    Private Function mergeHeader(ByVal matrixHeader() As Byte, ByVal offsetHead As Integer, ByVal nByteToMerge As Integer) As Integer
        If nByteToMerge = 2 Then
            Return CInt(matrixHeader(offsetHead)) + (CInt(matrixHeader(offsetHead + 1)) << 8)
        ElseIf nByteToMerge = 4 Then
            Return CInt(matrixHeader(offsetHead)) + (CInt(matrixHeader(offsetHead + 1)) << 8) + (CInt(matrixHeader(offsetHead + 2)) << 16) + (CInt(matrixHeader(offsetHead + 3)) << 24)
        End If

        Return 0
    End Function

    Private Function LeggiImmagini(ByVal NRpassata As Short) As Integer
        Dim tempImageRead() As Byte

        Dim completeImage_Gray As Byte()
        Dim completeImage_RD As Byte()
        Dim completeImage_GR As Byte()
        Dim completeImage_BL As Byte()

        Dim offsetPixel As Integer
        Dim nBitPixel As Integer
        Dim bitmapOffset As Integer
        Dim colorMode As Integer
        Dim rowIncrementByte As Byte

        LeggiImmagini = 0

        'Carico velocemente tutta la bitmap
        tempImageRead = File.ReadAllBytes(LsInterface.NomeFileImmagine)
        'Leggo l'header della bitmap
        For i = 0 To HEADER_LENGHT - 1
            matrixHeader(i) = tempImageRead(i)
        Next
        completeImage_Gray = Nothing
        completeImage_RD = Nothing : completeImage_GR = Nothing : completeImage_BL = Nothing
        'Leggo i dati che mi interessano dall'header
        offsetPixel = mergeHeader(matrixHeader, 10, 4)
        imgWidth = mergeHeader(matrixHeader, 18, 4)
        imgHeight = mergeHeader(matrixHeader, 22, 4)
        nBitPixel = mergeHeader(matrixHeader, 28, 2)
        bitmapOffset = mergeHeader(matrixHeader, 10, 4)

        'Controllo la modalità di colori dell'imamgine
        colorMode = mergeHeader(matrixHeader, 28, 2)
        If colorMode = 8 Then
            checkGrayImage = True
        Else
            checkGrayImage = False
        End If

        'Ricalcolo la dimensione reale in byte (deve essere un numero divisibile per 4)... o quanti byte scartare ogni riga
        If checkGrayImage Then
            rowIncrementByte = imgWidth Mod 4
            If rowIncrementByte > 0 Then
                rowIncrementByte = 4 - rowIncrementByte
            End If
        Else
            rowIncrementByte = (imgWidth * 3) Mod 4
            If rowIncrementByte > 0 Then
                rowIncrementByte = 4 - rowIncrementByte
            End If
        End If

        'Creo l'immagine girata e ridimensionata correttamente
        ReDim completeImage_Gray(0 To imgWidth * imgHeight - 1)
        ReDim completeImage_RD(0 To imgWidth * imgHeight - 1)
        ReDim completeImage_GR(0 To imgWidth * imgHeight - 1)
        ReDim completeImage_BL(0 To imgWidth * imgHeight - 1)
        Try
            If checkGrayImage Then
                Dim iIdx As Integer = 0
                For iH = 0 To imgHeight - 1
                    For iW = 0 To imgWidth - 1
                        completeImage_Gray(iW + imgWidth * (imgHeight - iH - 1)) = tempImageRead(bitmapOffset + iIdx)
                        completeImage_BL(iW + imgWidth * (imgHeight - iH - 1)) = completeImage_Gray(iW + imgWidth * (imgHeight - iH - 1))
                        completeImage_GR(iW + imgWidth * (imgHeight - iH - 1)) = completeImage_Gray(iW + imgWidth * (imgHeight - iH - 1))
                        completeImage_RD(iW + imgWidth * (imgHeight - iH - 1)) = completeImage_Gray(iW + imgWidth * (imgHeight - iH - 1))
                        iIdx = iIdx + 1
                    Next
                    iIdx = iIdx + rowIncrementByte
                Next
            Else
                Dim iIdx As Integer = 0
                For iH = 0 To imgHeight - 1
                    For iW = 0 To imgWidth - 1
                        'Creo anche l'immagini a grigi che mi serve finchè non metto tutto a posto (dovrei sviluppare la gestione dei grigi come se avessi 3 componenti)
                        completeImage_BL(iW + imgWidth * (imgHeight - iH - 1)) = tempImageRead(bitmapOffset + iIdx)
                        completeImage_GR(iW + imgWidth * (imgHeight - iH - 1)) = tempImageRead(bitmapOffset + iIdx + 1)
                        completeImage_RD(iW + imgWidth * (imgHeight - iH - 1)) = tempImageRead(bitmapOffset + iIdx + 2)
                        'If RadioButtonRDcomp.Checked Then
                        'completeImage_Gray(iW + imgWidth * (imgHeight - iH - 1)) = completeImage_RD(iW + imgWidth * (imgHeight - iH - 1))
                        'ElseIf RadioButtonGRcomp.Checked Then
                        'completeImage_Gray(iW + imgWidth * (imgHeight - iH - 1)) = completeImage_GR(iW + imgWidth * (imgHeight - iH - 1))
                        'ElseIf RadioButtonBLcomp.Checked Then
                        'completeImage_Gray(iW + imgWidth * (imgHeight - iH - 1)) = completeImage_BL(iW + imgWidth * (imgHeight - iH - 1))
                        'End If
                        iIdx = iIdx + 3
                    Next
                    iIdx = iIdx + rowIncrementByte
                Next
            End If
        Catch ex As Exception
            MsgBox("Error. Bitmap with less data than expected")
            LeggiImmagini = 1
        End Try

        Try
            If (NRpassata = 1) Then
                basicStatisticalRoutine(completeImage_RD, RDfr_1, areaSelectionCoords(0))
                basicStatisticalRoutine(completeImage_GR, GRfr_1, areaSelectionCoords(0))
                basicStatisticalRoutine(completeImage_BL, BLfr_1, areaSelectionCoords(0))
            ElseIf (NRpassata = 2) Then
                basicStatisticalRoutine(completeImage_RD, RDfr_2, areaSelectionCoords(0))
                basicStatisticalRoutine(completeImage_GR, GRfr_2, areaSelectionCoords(0))
                basicStatisticalRoutine(completeImage_BL, BLfr_2, areaSelectionCoords(0))
            Else 'nrPassata 3 
                basicStatisticalRoutine(completeImage_RD, RDfr, areaSelectionCoords(0))
                basicStatisticalRoutine(completeImage_GR, GRfr, areaSelectionCoords(0))
                basicStatisticalRoutine(completeImage_BL, BLfr, areaSelectionCoords(0))
            End If
        Catch ex As Exception
            Dim str As String
            str = "Error" + ex.ToString()
            MsgBox(str)
        End Try


        'Carico velocemente tutta la bitmap
        tempImageRead = File.ReadAllBytes(LsInterface.NomeFileImmagineRetro)
        'Leggo l'header della bitmap
        For i = 0 To HEADER_LENGHT - 1
            matrixHeader(i) = tempImageRead(i)
        Next
        completeImage_Gray = Nothing
        completeImage_RD = Nothing : completeImage_GR = Nothing : completeImage_BL = Nothing
        'Leggo i dati che mi interessano dall'header
        offsetPixel = mergeHeader(matrixHeader, 10, 4)
        imgWidth = mergeHeader(matrixHeader, 18, 4)
        imgHeight = mergeHeader(matrixHeader, 22, 4)
        nBitPixel = mergeHeader(matrixHeader, 28, 2)
        bitmapOffset = mergeHeader(matrixHeader, 10, 4)

        'Controllo la modalità di colori dell'imamgine
        colorMode = mergeHeader(matrixHeader, 28, 2)
        If colorMode = 8 Then
            checkGrayImage = True
        Else
            checkGrayImage = False
        End If

        'Ricalcolo la dimensione reale in byte (deve essere un numero divisibile per 4)... o quanti byte scartare ogni riga
        If checkGrayImage Then
            rowIncrementByte = imgWidth Mod 4
            If rowIncrementByte > 0 Then
                rowIncrementByte = 4 - rowIncrementByte
            End If
        Else
            rowIncrementByte = (imgWidth * 3) Mod 4
            If rowIncrementByte > 0 Then
                rowIncrementByte = 4 - rowIncrementByte
            End If
        End If

        'Creo l'immagine girata e ridimensionata correttamente
        ReDim completeImage_Gray(0 To imgWidth * imgHeight - 1)
        ReDim completeImage_RD(0 To imgWidth * imgHeight - 1)
        ReDim completeImage_GR(0 To imgWidth * imgHeight - 1)
        ReDim completeImage_BL(0 To imgWidth * imgHeight - 1)
        Try
            If checkGrayImage Then
                Dim iIdx As Integer = 0
                For iH = 0 To imgHeight - 1
                    For iW = 0 To imgWidth - 1
                        completeImage_Gray(iW + imgWidth * (imgHeight - iH - 1)) = tempImageRead(bitmapOffset + iIdx)
                        completeImage_BL(iW + imgWidth * (imgHeight - iH - 1)) = completeImage_Gray(iW + imgWidth * (imgHeight - iH - 1))
                        completeImage_GR(iW + imgWidth * (imgHeight - iH - 1)) = completeImage_Gray(iW + imgWidth * (imgHeight - iH - 1))
                        completeImage_RD(iW + imgWidth * (imgHeight - iH - 1)) = completeImage_Gray(iW + imgWidth * (imgHeight - iH - 1))
                        iIdx = iIdx + 1
                    Next
                    iIdx = iIdx + rowIncrementByte
                Next
            Else
                Dim iIdx As Integer = 0
                For iH = 0 To imgHeight - 1
                    For iW = 0 To imgWidth - 1
                        'Creo anche l'immagini a grigi che mi serve finchè non metto tutto a posto (dovrei sviluppare la gestione dei grigi come se avessi 3 componenti)
                        completeImage_BL(iW + imgWidth * (imgHeight - iH - 1)) = tempImageRead(bitmapOffset + iIdx)
                        completeImage_GR(iW + imgWidth * (imgHeight - iH - 1)) = tempImageRead(bitmapOffset + iIdx + 1)
                        completeImage_RD(iW + imgWidth * (imgHeight - iH - 1)) = tempImageRead(bitmapOffset + iIdx + 2)
                        'If RadioButtonRDcomp.Checked Then
                        'completeImage_Gray(iW + imgWidth * (imgHeight - iH - 1)) = completeImage_RD(iW + imgWidth * (imgHeight - iH - 1))
                        'ElseIf RadioButtonGRcomp.Checked Then
                        'completeImage_Gray(iW + imgWidth * (imgHeight - iH - 1)) = completeImage_GR(iW + imgWidth * (imgHeight - iH - 1))
                        'ElseIf RadioButtonBLcomp.Checked Then
                        'completeImage_Gray(iW + imgWidth * (imgHeight - iH - 1)) = completeImage_BL(iW + imgWidth * (imgHeight - iH - 1))
                        'End If
                        iIdx = iIdx + 3
                    Next
                    iIdx = iIdx + rowIncrementByte
                Next
            End If
        Catch ex As Exception
            MsgBox("Error. Bitmap with less data than expected")
        End Try
        Try
            If (NRpassata = 1) Then
                'Retro
                basicStatisticalRoutine(completeImage_RD, RDre_1, areaSelectionCoords(0))
                basicStatisticalRoutine(completeImage_GR, GRre_1, areaSelectionCoords(0))
                basicStatisticalRoutine(completeImage_BL, BLre_1, areaSelectionCoords(0))
            ElseIf (NRpassata = 2) Then
                'Retro
                basicStatisticalRoutine(completeImage_RD, RDre_2, areaSelectionCoords(0))
                basicStatisticalRoutine(completeImage_GR, GRre_2, areaSelectionCoords(0))
                basicStatisticalRoutine(completeImage_BL, BLre_2, areaSelectionCoords(0))
            Else 'Nr passata 3 
                'Retro
                basicStatisticalRoutine(completeImage_RD, RDre, areaSelectionCoords(0))
                basicStatisticalRoutine(completeImage_GR, GRre, areaSelectionCoords(0))
                basicStatisticalRoutine(completeImage_BL, BLre, areaSelectionCoords(0))
            End If
        Catch ex As Exception
            Dim str As String
            str = "Error" + ex.ToString()
            MsgBox(str)
            LeggiImmagini = 1
        End Try

    End Function

    Public Sub SvuotaDir(ByVal cartella As String)
        Dim di As IO.DirectoryInfo = _
                New IO.DirectoryInfo(cartella)
        For Each oFile As IO.FileInfo In di.GetFiles()
            oFile.Delete()
        Next
    End Sub

    Private Sub txtcomLevelGray_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtcomLevelGray.KeyPress

        If (Not IsNumeric(e.KeyChar)) And (Asc(e.KeyChar) <> 8) Then
            e.Handled = True
        End If
    End Sub


    Private Sub txtcomLevelRd_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtcomLevelRd.KeyPress
        If (Not IsNumeric(e.KeyChar)) And (Asc(e.KeyChar) <> 8) Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtcomLevelGr_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtcomLevelGr.KeyPress
        If (Not IsNumeric(e.KeyChar)) And (Asc(e.KeyChar) <> 8) Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtcomLevelBI_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtcomLevelBI.KeyPress
        If (Not IsNumeric(e.KeyChar)) And (Asc(e.KeyChar) <> 8) Then
            e.Handled = True
        End If
    End Sub

    Private Sub Form1_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress

    End Sub

    Private Sub Form1_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        KeyPressed = e.KeyValue
    End Sub

    Private Sub txtcomLevelGray_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtcomLevelGray.KeyUp, txtcomLevelBI.KeyUp, txtcomLevelGr.KeyUp, txtcomLevelRd.KeyUp, MyBase.KeyUp
        KeyPressed = e.KeyValue
    End Sub

    Private Sub BtDocSecondari_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles BtDocSecondari.KeyUp, BtDocTest.KeyDown
        KeyPressed = e.KeyValue
    End Sub



    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        LsInterface = New Ls150Class.Periferica()
        Dim ValoreLetto As String = New String(" "c, 12)

        RDfr_1 = 0
        RDfr_2 = 0
        GRfr_1 = 0
        GRfr_2 = 0
        BLfr_1 = 0
        BLfr_2 = 0
        RDre_1 = 0
        RDre_2 = 0
        GRre_1 = 0
        GRre_2 = 0
        BLre_1 = 0
        BLre_2 = 0

        imgHeight = 0
        imgWidth = 0

        RD = 0
        GR = 0
        BL = 0
        GRAY = 0

        RDfr = 0
        GRfr = 0
        BLfr = 0
        RDre = 0
        GRre = 0
        BLre = 0

        GrayColFR = 0
        GrayColRE = 0
        GrayCOLini = 0

        compLevelGraydoc = 0
        compLevelRDdoc = 0
        compLevelGRdoc = 0
        compLevelBLdoc = 0
        colorLevelThresholdINI = 0

        DELTArdfr = 0
        DELTAgrfr = 0
        DELTAblfr = 0
        DELTArdre = 0
        DELTAgrre = 0
        DELTAblre = 0

        WBerrfr = 0
        WBerrre = 0
        whiteBalanceThresholdINI = 0
        MaxDistanzaFronteRetro = 0

        ColorControl = GbResult.BackColor

        nomeFile = Application.StartupPath + "\LS150_CIScalibration.ini"
        nomefileLang = Application.StartupPath + "\CIScalibrationLang.ini"

        ' Lettura valori inizializzazione dal file .ini

        compGrayColIni = CInt(ReadValue("DOCUTEST--309", "compLevelGray", nomeFile))
        compLevelRDIni = CInt(ReadValue("DOCUTEST--309", "compLevelRd", nomeFile))
        compLevelGRIni = CInt(ReadValue("DOCUTEST--309", "compLevelGr", nomeFile))
        compLevelBLIni = CInt(ReadValue("DOCUTEST--309", "compLevelBl", nomeFile))

        areaSelectionCoords(0).xStart = CInt(ReadValue("DOCUTEST--309", "xStartAvgArea", nomeFile))
        areaSelectionCoords(0).xStop = CInt(ReadValue("DOCUTEST--309", "xStopAvgArea", nomeFile))
        areaSelectionCoords(0).yStart = CInt(ReadValue("DOCUTEST--309", "yStartAvgArea", nomeFile))
        areaSelectionCoords(0).yStop = CInt(ReadValue("DOCUTEST--309", "yStopAvgArea", nomeFile))

        whiteBalanceThresholdINI = CInt(ReadValue("DOCUTEST--309", "whiteBalanceThreshold", nomeFile))
        colorLevelThresholdINI = CInt(ReadValue("DOCUTEST--309", "colorLevelThreshold", nomeFile))
        MaxDistanzaFronteRetro = CInt(ReadValue("DOCUTEST--309", "MaxDistanzaFronteRetro", nomeFile))



        TempoAttesaNuovaCertificazione = CInt(ReadValue("Application", "TempoAttesaCertificazione", nomeFile))
        NrMaxCertificazioniSecondarie = CInt(ReadValue("Application", "NrVolteRitaratura", nomeFile))

        NrMaxAssegnazioneLotto = CInt(ReadValue("Application", "NrVolteRitaraturaTest", nomeFile))

        NoCalibrazione = CInt(ReadValue("Application", "NoCalibrazione", nomeFile))


        NrCertificazioniSecondarieFatte = 0
        NrAssegnazioneLottoFatte = 0

        Try
            If IO.Directory.Exists(Application.StartupPath + "\IMG") Then
                'IO.Directory.Delete(sss + "\Dati", True)
                SvuotaDir(Application.StartupPath + "\IMG")
            Else
                IO.Directory.CreateDirectory(Application.StartupPath + "\IMG")
            End If
        Catch ex As Exception
            MessageBox.Show("Directory Dati NON creata....", ex.Message + " ", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        'ValoreLetto = ReadValue("ITA", "Msg18", nomefileLang) 'Inserire i Valori Letti sul DocutestCertificato
        'LBLtesto.Text = ValoreLetto

    End Sub


    Private Sub BtDocSecondari_Click(sender As Object, e As EventArgs) Handles BtDocSecondari.Click

        Dim length As Integer = 12
        Dim ValoreLetto As String = New String(" "c, length)

        Dim Reply As Short
        Dim Reply_IMG As Short
        Dim fuscita As Boolean
        Dim PresenzaAssegno As Integer
        Dim stt As String
        Dim nrSecondi As Integer


        fuscita = False
        fStop = False
        'cmdStop.Visible = True
        BtDocSecondari.Enabled = False
        BtDocTest.Enabled = False

        ' Se ho Processato dei Documenti per assegnare il lotto, richiedo di inserire i valori del documento Primario
        If (NrAssegnazioneLottoFatte <> 0) Then
            NrCertificazioniSecondarieFatte = 0
        End If


        Try
            GbResult.BackColor = ColorControl

            Do
                ' Controllo se devo rifare la calibrazione del CIS
                If ((NrCertificazioniSecondarieFatte = 0 Or NrCertificazioniSecondarieFatte >= NrMaxCertificazioniSecondarie) And (NoCalibrazione <> 1)) Then

                    GbResult.BackColor = Color.Yellow

                    ' Riazzero le variabili
                    NrCertificazioniSecondarieFatte = 0
                    txtcomLevelGray.Text = ""
                    txtcomLevelRd.Text = ""
                    txtcomLevelGr.Text = ""
                    txtcomLevelBI.Text = ""

                    'taratura del letture necessaria con documento di calibrazione primario
                    ValoreLetto = ReadValue("ITA", "Msg3", nomefileLang)
                    LBLtesto.Text = ValoreLetto
                    Me.Refresh()
                    MessageBox.Show(ValoreLetto)

                    Reply = LsInterface.TestFotoPresenza(PresenzaAssegno)

                    If (Reply = Ls150Class.Costanti.S_LS150_OKAY) Then

                        PresenzaAssegno = 0
                        Do
                            Reply = LsInterface.TestFotoPresenza(PresenzaAssegno)

                        Loop While (PresenzaAssegno = 0)

                        ValoreLetto = ReadValue("ITA", "Msg4", nomefileLang)
                        LBLtesto.Text = ValoreLetto
                        Me.Refresh()
                        Reply = LsInterface.CalibraScannerOld()


                        If (Reply = Ls150Class.Costanti.S_LS150_OKAY) Then
                            'ValoreLetto = ReadValue("ITA", "Msg1", nomefileLang)
                            'LBLtesto.Text = ValoreLetto

                            ValoreLetto = ReadValue("ITA", "Msg2", nomefileLang) 'Inserire i Valori Letti sul DocutestCertificato
                            LBLtesto.Text = ValoreLetto
                            'lblTesto2.BackColor = Color.White

                            gpValori.Visible = True
                            Application.DoEvents()
                            txtcomLevelGray.Focus()
                            fuscita = False
                            Me.Refresh()

                            Do
                                If (txtcomLevelGray.Text <> "" And txtcomLevelRd.Text <> "" And txtcomLevelGr.Text <> "" And txtcomLevelBI.Text <> "" And _
                                    txtcomLevelGray.Text.Length = 3 And txtcomLevelRd.Text.Length = 3 And txtcomLevelGr.Text.Length = 3 And txtcomLevelBI.Text.Length = 3) Then

                                    compLevelGraydoc = CInt(txtcomLevelGray.Text)
                                    compLevelRDdoc = CInt(txtcomLevelRd.Text)
                                    compLevelGRdoc = CInt(txtcomLevelGr.Text)
                                    compLevelBLdoc = CInt(txtcomLevelBI.Text)

                                    fuscita = True
                                    'Else
                                    '    ValoreLetto = ReadValue("ITA", "Msg2", nomefileLang)
                                    '    LBLtesto.Text = ValoreLetto
                                    '    'LBLtesto.BackColor = Color.Red
                                    '    fuscita = False
                                    '    gpValori.Visible = True
                                End If
                                'Me.Refresh()
                                Application.DoEvents()
                            Loop While (fuscita = False)

                            gpValori.Visible = False

                            'passaggio docutest 309 in modalita' grigia
                            ValoreLetto = ReadValue("ITA", "Msg5", nomefileLang)
                            LBLtesto.Text = ValoreLetto
                            Me.Refresh()
                            Do
                                Reply = LsInterface.TestFotoPresenza(PresenzaAssegno)
                            Loop While (PresenzaAssegno = 0)

                            Reply = LsInterface.TestNormalization(Ls150Class.Costanti.S_SCAN_MODE_256GR300, 1, compLevelGraydoc, compLevelRDdoc, compLevelGRdoc, compLevelBLdoc, -2, 1, 1)
                            If (Reply = Ls150Class.Costanti.S_LS150_OKAY) Then
                                ValoreLetto = ReadValue("ITA", "Msg12", nomefileLang) 'Inserire Docutest309 per effettuare il calcolo dei coefficienti moltiplicativi(COLORE)
                                LBLtesto.Text = ValoreLetto
                                Me.Refresh()

                                Reply = LsInterface.TestNormalization(Ls150Class.Costanti.S_SCAN_MODE_COLOR_300, 1, compLevelGraydoc, compLevelRDdoc, compLevelGRdoc, compLevelBLdoc, -2, 1, 1)

                                If (Reply = Ls150Class.Costanti.S_LS150_OKAY) Then

                                    stt = ReadValue("ITA", "Msg16", nomefileLang)
                                    MessageBox.Show(stt, "CERIFICAZIONE", MessageBoxButtons.OK)

                                Else
                                    ViewError(Reply)
                                    fStop = True
                                    GbResult.BackColor = Color.Red
                                End If
                            Else
                                ViewError(Reply)
                                fStop = True
                                GbResult.BackColor = Color.Red
                            End If
                        Else
                            ViewError(Reply)
                            fStop = True
                            GbResult.BackColor = Color.Red
                        End If
                    Else
                        ViewError(Reply)
                        fStop = True
                        GbResult.BackColor = Color.Red
                    End If
                End If

                'Dim IndImgF As Integer
                If (fStop = False) Then

                    ' Azzeramento deriva termica CIS
                    nrSecondi = TempoAttesaNuovaCertificazione
                    While (nrSecondi)
                        LBLtesto.Text = "Prego attendere " + nrSecondi.ToString + " secondi ..."
                        Application.DoEvents()
                        Me.Refresh()
                        Threading.Thread.Sleep(888)
                        nrSecondi -= 1
                    End While

                    Dim Data As String
                    Data = DateTime.Now.ToString("yyyyMMdd_HHmmss")

                    grpUtente.Visible = False
                    Application.DoEvents()
                    Me.Refresh()
                    'IndImgF = 0
                    LsInterface.NomeFileImmagine = Application.StartupPath + "\\IMG\\" + Data + "_" + NrCertificazioniSecondarieFatte.ToString("0000") + "Front.bmp"
                    LsInterface.NomeFileImmagineRetro = Application.StartupPath + "\\IMG\\" + Data + "_" + NrCertificazioniSecondarieFatte.ToString("0000") + "Back.bmp"

                    ValoreLetto = ReadValue("ITA", "Msg13", nomefileLang)
                    LBLtesto.Text = ValoreLetto
                    Me.Refresh()
                    PresenzaAssegno = 0
                    Do
                        Reply = LsInterface.TestFotoPresenza(PresenzaAssegno)
                    Loop While (PresenzaAssegno = 0 And Reply <> Ls150Class.Costanti.S_LS150_PERIFNOTFOUND)

                    If Reply = Ls150Class.Costanti.S_LS150_PERIFNOTFOUND Then
                        ViewError(Reply)
                        fStop = True
                    Else
                        Reply = LsInterface.Docutest309(Ls150Class.Costanti.S_SCAN_MODE_COLOR_300)
                        If (Reply = Ls150Class.Costanti.S_LS150_OKAY) Then
                            Reply_IMG = LeggiImmagini(1)
                            ValoreLetto = ReadValue("ITA", "Msg7", nomefileLang) 'nserire Docutest309 per effettuare il calcolo dei coefficienti moltiplicativi(2)
                            LBLtesto.Text = ValoreLetto
                            Me.Refresh()

                            PresenzaAssegno = 0
                            Do
                                Reply = LsInterface.TestFotoPresenza(PresenzaAssegno)
                            Loop While (PresenzaAssegno = 0)

                            Reply = LsInterface.Docutest309(Ls150Class.Costanti.S_SCAN_MODE_COLOR_300)
                            If (Reply = Ls150Class.Costanti.S_LS150_OKAY) Then
                                Reply_IMG = LeggiImmagini(2)

                                RD = (RDfr_1 + RDfr_2 + RDre_1 + RDre_2) / 4
                                GR = (GRfr_1 + GRfr_2 + GRre_1 + GRre_2) / 4
                                BL = (BLfr_1 + BLfr_2 + BLre_1 + BLre_2) / 4
                                GRAY = (RD + GR + BL) / 3

                                grpUtente.Visible = True
                                lblRD.Text = RD.ToString()
                                lblGR.Text = GR.ToString()
                                lblBL.Text = BL.ToString()
                                lblGRAY.Text = GRAY.ToString()

                                GbResult.BackColor = Color.Green
                                ValoreLetto = ReadValue("ITA", "Msg6", nomefileLang)
                                LBLtesto.Text = ValoreLetto
                                Me.Refresh()

                                ' Incremento il numero di certificazioni
                                NrCertificazioniSecondarieFatte = NrCertificazioniSecondarieFatte + 1

                                KeyPressed = 0
                                PresenzaAssegno = 0
                                Do
                                    Reply = LsInterface.TestFotoPresenza(PresenzaAssegno)
                                    Application.DoEvents()
                                    If (KeyPressed > 0) Then
                                        PresenzaAssegno = 1
                                    End If
                                Loop While (PresenzaAssegno = 0)
                                'fStop = True

                                If (KeyPressed > 0) Then
                                    fStop = True
                                    grpUtente.Visible = False
                                    ValoreLetto = ReadValue("ITA", "Msg18", nomefileLang) 'Inserire i Valori Letti sul DocutestCertificato
                                    LBLtesto.Text = ValoreLetto
                                End If

                            Else
                                ViewError(Reply)
                                fStop = True
                                GbResult.BackColor = Color.Red
                            End If
                        Else
                            ViewError(Reply)
                            fStop = True
                            GbResult.BackColor = Color.Red
                        End If
                    End If
                End If
            Loop While (fStop = False)

        Catch ex As Exception
            MessageBox.Show(ex.ToString())
            fStop = True
        End Try

        'If (fStop = True) Then
        '    LBLtesto.Text = ""
        'End If

        BtDocSecondari.Enabled = True
        BtDocTest.Enabled = True

    End Sub

    Private Sub BtDocTest_Click(sender As Object, e As EventArgs) Handles BtDocTest.Click

        Dim length As Integer = 12
        Dim ValoreLetto As String = New String(" "c, length)
        Dim str As String
        Dim Reply As Short
        Dim Reply_IMG As Short
        Dim fuscita As Boolean
        Dim PresenzaAssegno As Integer
        Dim stt As String
        Dim nrSecondi As Integer


        fuscita = False
        fStop = False

        BtDocSecondari.Enabled = False
        BtDocTest.Enabled = False

        LbLottoGray.Text = "0"
        LbLottoRD.Text = "0"
        LbLottoGR.Text = "0"
        LbLottoBL.Text = "0"
        LbLottoLotto.Text = "0"

        ' Se ho Processato dei Documenti Secondari, richiedo di inserire i valori del documento Primario
        If (NrCertificazioniSecondarieFatte <> 0) Then
            NrAssegnazioneLottoFatte = 0
        End If


        Try
            GbResult.BackColor = ColorControl

            Do
                ' Controllo se devo rifare la calibrazione del CIS
                If ((NrAssegnazioneLottoFatte = 0 Or NrAssegnazioneLottoFatte = NrMaxAssegnazioneLotto) And (NoCalibrazione <> 1)) Then


                    GbResult.BackColor = Color.Yellow

                    ' Riazzero le variabili
                    NrAssegnazioneLottoFatte = 0
                    txtcomLevelGray.Text = ""
                    txtcomLevelRd.Text = ""
                    txtcomLevelGr.Text = ""
                    txtcomLevelBI.Text = ""

                    'taratura del letture necessaria con documento di calibrazione primario
                    ValoreLetto = ReadValue("ITA", "Msg3", nomefileLang)
                    LBLtesto.Text = ValoreLetto
                    Me.Refresh()
                    MessageBox.Show(ValoreLetto)

                    Reply = LsInterface.TestFotoPresenza(PresenzaAssegno)

                    If (Reply = Ls150Class.Costanti.S_LS150_OKAY) Then

                        PresenzaAssegno = 0
                        Do
                            Reply = LsInterface.TestFotoPresenza(PresenzaAssegno)

                        Loop While (PresenzaAssegno = 0)

                        ValoreLetto = ReadValue("ITA", "Msg4", nomefileLang)
                        LBLtesto.Text = ValoreLetto
                        Me.Refresh()
                        Reply = LsInterface.CalibraScannerOld()


                        If (Reply = Ls150Class.Costanti.S_LS150_OKAY) Then
                            'ValoreLetto = ReadValue("ITA", "Msg1", nomefileLang)
                            'LBLtesto.Text = ValoreLetto

                            ValoreLetto = ReadValue("ITA", "Msg17", nomefileLang)
                            LBLtesto.Text = ValoreLetto
                            'lblTesto2.BackColor = Color.White

                            gpValori.Visible = True
                            txtcomLevelGray.Focus()
                            fuscita = False
                            Me.Refresh()

                            Do
                                If (txtcomLevelGray.Text <> "" And txtcomLevelRd.Text <> "" And txtcomLevelGr.Text <> "" And txtcomLevelBI.Text <> "" And _
                                    txtcomLevelGray.Text.Length = 3 And txtcomLevelRd.Text.Length = 3 And txtcomLevelGr.Text.Length = 3 And txtcomLevelBI.Text.Length = 3) Then

                                    compLevelGraydoc = CInt(txtcomLevelGray.Text)
                                    compLevelRDdoc = CInt(txtcomLevelRd.Text)
                                    compLevelGRdoc = CInt(txtcomLevelGr.Text)
                                    compLevelBLdoc = CInt(txtcomLevelBI.Text)

                                    fuscita = True
                                    'Else
                                    '    ValoreLetto = ReadValue("ITA", "Msg17", nomefileLang)
                                    '    LBLtesto.Text = ValoreLetto
                                    '    'LBLtesto.BackColor = Color.Red
                                    '    fuscita = False
                                End If

                                Application.DoEvents()
                            Loop While (fuscita = False)

                            gpValori.Visible = False

                            'passaggio docutest 309 in modalita' grigia
                            ValoreLetto = ReadValue("ITA", "Msg5", nomefileLang)
                            LBLtesto.Text = ValoreLetto
                            Me.Refresh()
                            Do
                                Reply = LsInterface.TestFotoPresenza(PresenzaAssegno)
                            Loop While (PresenzaAssegno = 0)

                            Reply = LsInterface.TestNormalization(Ls150Class.Costanti.S_SCAN_MODE_256GR300, 1, compLevelGraydoc, compLevelRDdoc, compLevelGRdoc, compLevelBLdoc, -2, 1, 1)
                            If (Reply = Ls150Class.Costanti.S_LS150_OKAY) Then

                                ValoreLetto = ReadValue("ITA", "Msg12", nomefileLang) 'Inserire Docutest309 per effettuare il calcolo dei coefficienti moltiplicativi(COLORE)
                                LBLtesto.Text = ValoreLetto
                                Me.Refresh()

                                Reply = LsInterface.TestNormalization(Ls150Class.Costanti.S_SCAN_MODE_COLOR_300, 1, compLevelGraydoc, compLevelRDdoc, compLevelGRdoc, compLevelBLdoc, -2, 1, 1)

                                If (Reply = Ls150Class.Costanti.S_LS150_OKAY) Then

                                    stt = ReadValue("ITA", "Msg16", nomefileLang)
                                    MessageBox.Show(stt, "CERIFICAZIONE", MessageBoxButtons.OK)
                                Else
                                    ViewError(Reply)
                                    GbResult.BackColor = Color.Red
                                    fStop = True
                                End If
                            Else
                                ViewError(Reply)
                                GbResult.BackColor = Color.Red
                                fStop = True
                            End If
                        Else
                            ViewError(Reply)
                            GbResult.BackColor = Color.Red
                            fStop = True
                        End If
                    Else
                        ViewError(Reply)
                        GbResult.BackColor = Color.Red
                        fStop = True
                    End If
                End If


                If fStop = False Then

                    ' Azzeramento deriva termica CIS
                    nrSecondi = TempoAttesaNuovaCertificazione
                    While (nrSecondi)
                        LBLtesto.Text = "Prego attendere " + nrSecondi.ToString + " secondi ..."
                        Application.DoEvents()
                        Me.Refresh()
                        Threading.Thread.Sleep(888)
                        nrSecondi -= 1
                    End While

                    Dim Data As String
                    Data = DateTime.Now.ToString("yyyyMMdd_HHmmss")

                    GbResult.BackColor = Color.Yellow
                    gbLottoResult.Visible = True

                    'Passaggio del DOCUTEST--309 in modalita' colori, con Salvataggio delle immagini 

                    'IndImgF = 0
                    LsInterface.NomeFileImmagine = Application.StartupPath + "\\IMG\\" + Data + "_" + NrAssegnazioneLottoFatte.ToString("0000") + "Front.bmp"
                    LsInterface.NomeFileImmagineRetro = Application.StartupPath + "\\IMG\\" + Data + "_" + NrAssegnazioneLottoFatte.ToString("0000") + "Back.bmp"
                    gpValori.Visible = False
                    ValoreLetto = ReadValue("ITA", "Msg8", nomefileLang)
                    LBLtesto.Text = ValoreLetto
                    Me.Refresh()
                    PresenzaAssegno = 0
                    Do
                        Reply = LsInterface.TestFotoPresenza(PresenzaAssegno)
                    Loop While (PresenzaAssegno = 0)

                    Reply = LsInterface.Docutest309(Ls150Class.Costanti.S_SCAN_MODE_COLOR_300)
                    If (Reply = Ls150Class.Costanti.S_LS150_OKAY) Then
                        Reply_IMG = LeggiImmagini(3)
                        If (Reply = Ls150Class.Costanti.S_LS150_OKAY) Then

                            '02/05/2018
                            'Identificazione del lotto di appartenza tramite verifica del bilanciamento bianco ...

                            Dim Lettura As String
                            Dim indice As Integer
                            Dim lottoDoc As Integer
                            Dim VarLottoStr As String
                            Dim LetturaSplit() As String
                            Dim MAXWBerrfr As Integer
                            Dim MINWBerrfr As Integer
                            Dim MAXWBerrre As Integer
                            Dim MINWBerrre As Integer
                            Dim NumeroLottiFileIni As Integer
                            Dim media() As Integer = Nothing
                            Dim lotto() As Integer = Nothing
                            Dim cont As Integer
                            cont = 0


                            NumeroLottiFileIni = CInt(ReadValue("Application", "NrLotti", nomeFile))
                            fDocLotto = False

                            'For indice = 0 To 26
                            For indice = 0 To NumeroLottiFileIni - 1
                                VarLottoStr = "lotto" + indice.ToString() + "_309"
                                Lettura = ReadValue("Lotti DOCUTEST--309", VarLottoStr, nomeFile)
                                LetturaSplit = Lettura.Split(",")
                                compGrayColIni_Loti = LetturaSplit(0)
                                compLevelRDIni_Loti = LetturaSplit(1)
                                compLevelGRIni_Loti = LetturaSplit(2)
                                compLevelBLIni_Loti = LetturaSplit(3)

                                ' Verifica bilanciamento del bianco
                                DELTArdfr = RDfr - compLevelRDIni_Loti
                                'If (DELTArdfr < 0) Then
                                '    DELTArdfr = 0
                                'End If
                                DELTAgrfr = GRfr - compLevelGRIni_Loti
                                'If (DELTAgrfr < 0) Then
                                '    DELTAgrfr = 0
                                'End If
                                DELTAblfr = BLfr - compLevelBLIni_Loti
                                'If (DELTAblfr < 0) Then
                                '    DELTAblfr = 0
                                'End If

                                DELTArdre = RDre - compLevelRDIni_Loti
                                'If (DELTArdre < 0) Then
                                '    DELTArdre = 0
                                'End If

                                DELTAgrre = GRre - compLevelGRIni_Loti
                                'If (DELTAgrre < 0) Then
                                '    DELTAgrre = 0
                                'End If

                                DELTAblre = BLre - compLevelBLIni_Loti
                                'If (DELTAblre < 0) Then
                                '    DELTAblre = 0
                                'End If

                                MAXWBerrfr = DELTArdfr
                                If (DELTAgrfr > MAXWBerrfr) Then
                                    MAXWBerrfr = DELTAgrfr
                                End If

                                If (DELTAblfr > MAXWBerrfr) Then
                                    MAXWBerrfr = DELTAblfr
                                End If

                                MINWBerrfr = DELTArdfr
                                If (DELTAgrfr < MINWBerrfr) Then
                                    MINWBerrfr = DELTAgrfr
                                End If
                                If (DELTAblfr < MINWBerrfr) Then
                                    MINWBerrfr = DELTAblfr
                                End If

                                MAXWBerrre = DELTArdre
                                If (DELTAgrre > MAXWBerrre) Then
                                    MAXWBerrre = DELTAgrre
                                End If
                                If (DELTAblre > MAXWBerrre) Then
                                    MAXWBerrre = DELTAblre
                                End If

                                MINWBerrre = DELTArdre
                                If (DELTAgrre < MINWBerrre) Then
                                    MINWBerrre = DELTAgrre
                                End If
                                If (DELTAblre < MINWBerrre) Then
                                    MINWBerrre = DELTAblre
                                End If

                                ' Controllo se le distanze sono comprese nel range del lotto
                                If ((MAXWBerrfr < whiteBalanceThresholdINI) And (MINWBerrfr > (-1 * whiteBalanceThresholdINI))) Then

                                    'If ((MAXWBerrre < whiteBalanceThresholdINI) And (MINWBerrre > (-1 * whiteBalanceThresholdINI)) And
                                    '    (RDre < (compLevelRDIni_Loti + MaxDistanzaFronteRetro)) And (RDre > (compLevelRDIni_Loti - MaxDistanzaFronteRetro)) And
                                    '    (GRre < (compLevelGRIni_Loti + MaxDistanzaFronteRetro)) And (GRre > (compLevelGRIni_Loti - MaxDistanzaFronteRetro)) And
                                    '    (BLre < (compLevelBLIni_Loti + MaxDistanzaFronteRetro)) And (BLre > (compLevelBLIni_Loti - MaxDistanzaFronteRetro))) Then

                                    If ((RDre < (compLevelRDIni_Loti + MaxDistanzaFronteRetro)) And (RDre > (compLevelRDIni_Loti - MaxDistanzaFronteRetro)) And _
                                        (GRre < (compLevelGRIni_Loti + MaxDistanzaFronteRetro)) And (GRre > (compLevelGRIni_Loti - MaxDistanzaFronteRetro)) And _
                                        (BLre < (compLevelBLIni_Loti + MaxDistanzaFronteRetro)) And (BLre > (compLevelBLIni_Loti - MaxDistanzaFronteRetro))) Then

                                        WBerrfr = MAXWBerrfr - MINWBerrfr
                                        WBerrre = MAXWBerrre - MINWBerrre

                                        ' Verifica bilanciamento del bianco
                                        'If ((WBerrfr < (whiteBalanceThresholdINI)) And (WBerrre < whiteBalanceThresholdINI)) Then
                                        'OK
                                        fDocLotto = True
                                        ReDim Preserve media(0 To cont)
                                        ReDim Preserve lotto(0 To cont)
                                        media(cont) = (RDfr + RDre + GRfr + GRre + BLfr + BLre) / 6
                                        media(cont) = Math.Abs(media(cont) - compGrayColIni_Loti)
                                        lotto(cont) = indice
                                        cont = cont + 1
                                        'Exit For
                                        Dim grayLevel As Integer
                                        grayLevel = ((RDfr + RDre + GRfr) / 3).ToString
                                        LbLottoGray.Text = grayLevel.ToString
                                        LbLottoRD.Text = RDfr.ToString
                                        LbLottoGR.Text = GRfr.ToString
                                        LbLottoBL.Text = BLfr.ToString
                                        LbLottoLotto.Text = indice.ToString
                                        'End If
                                    End If
                                End If

                            Next

                            ' Incremento il numero di certificazioni
                            NrAssegnazioneLottoFatte = NrAssegnazioneLottoFatte + 1

                            If (fDocLotto = True) Then
                                Dim min_media As Integer
                                min_media = media.Min()
                                Dim indice_lotto As Integer
                                indice_lotto = Array.IndexOf(media, min_media)

                                lottoDoc = lotto(indice_lotto)
                                LBLtesto.Text = "LottoDoc = " + lottoDoc.ToString() + vbNewLine

                                GbResult.BackColor = Color.Green
                            End If

                            If (fDocLotto = False) Then

                                LBLtesto.Text = "Lotto non Trovato !" + vbNewLine + vbNewLine
                                LBLtesto.Text = LBLtesto.Text + "RDfr = " + RDfr.ToString() + "  -  RDre = " + RDre.ToString() + vbNewLine
                                LBLtesto.Text = LBLtesto.Text + "GRfr = " + GRfr.ToString() + "  -  Grre = " + GRre.ToString() + vbNewLine
                                LBLtesto.Text = LBLtesto.Text + "BLfr = " + BLfr.ToString() + "  -  BLre = " + BLre.ToString() + vbNewLine

                                GbResult.BackColor = Color.Red
                                Me.Refresh()
                                Exit Do
                                'Else
                                '    Me.BackColor = Color.FromArgb(85, 22, 110)
                                '    Me.Refresh()
                                '    'LBLtesto.Text = LBLtesto.Text + vbNewLine
                                '    'LBLtesto.Text = LBLtesto.Text + "LottoDoc = " + indice.ToString() + vbNewLine
                            End If



                            'Verifica del livello globale dei colori (livello di grigio)
                            GrayColFR = (RDfr + GRfr + BLfr) / 3
                            GrayColRE = (RDre + GRre + BLre) / 3

                            ' Ri-Leggo il valore di grigio
                            VarLottoStr = "lotto" + lottoDoc.ToString() + "_309"
                            Lettura = ReadValue("Lotti DOCUTEST--309", VarLottoStr, nomeFile)
                            LetturaSplit = Lettura.Split(",")
                            compGrayColIni_Loti = LetturaSplit(0)
                            compLevelRDIni_Loti = LetturaSplit(1)
                            compLevelGRIni_Loti = LetturaSplit(2)
                            compLevelBLIni_Loti = LetturaSplit(3)

                            'GrayCOLini = (compLevelRDini + compLevelGrIni + compLevelBLini) / 3
                            GrayCOLini = (compLevelRDIni_Loti + compLevelBLIni_Loti + compLevelGRIni_Loti) / 3

                            fStop = False
                            'verifica livello globale colori
                            If (GrayColFR > (GrayCOLini - colorLevelThresholdINI)) And (GrayColFR < (GrayCOLini + colorLevelThresholdINI)) Then
                                'OK
                                'fStop = False
                                str = "OK"
                            Else
                                fStop = True
                                ValoreLetto = ReadValue("ITA", "Msg9", nomefileLang)
                                LBLtesto.Text = ValoreLetto + vbNewLine
                                GbResult.BackColor = Color.Red
                            End If


                            If (GrayColRE > (GrayCOLini - colorLevelThresholdINI)) And (GrayColRE < (GrayCOLini + colorLevelThresholdINI)) Then
                                'OK
                                str = "OK"
                            Else
                                fStop = True
                                ValoreLetto = ReadValue("ITA", "Msg9", nomefileLang)
                                LBLtesto.Text = ValoreLetto + vbNewLine
                                GbResult.BackColor = Color.Green
                            End If


                            ValoreLetto = ReadValue("ITA", "Msg14", nomefileLang) 'Docutest CERTIFICATO , Inserire un nuovo Docutest309 per continuare o premere un Tasto per Terminare
                            'LBLtesto.Text = "LottoDoc = " + indice.ToString() + vbNewLine
                            LBLtesto.Text = LBLtesto.Text + ValoreLetto
                            Me.Refresh()


                            KeyPressed = 0
                            PresenzaAssegno = 0
                            Do
                                Reply = LsInterface.TestFotoPresenza(PresenzaAssegno)
                                Application.DoEvents()
                                If (KeyPressed > 0) Then
                                    PresenzaAssegno = 1
                                End If
                            Loop While (PresenzaAssegno = 0)

                            If (KeyPressed > 0) Then
                                fStop = True
                                gpValori.Visible = False
                                ValoreLetto = ReadValue("ITA", "Msg18", nomefileLang) 'Inserire i Valori Letti sul DocutestCertificato
                                LBLtesto.Text = ValoreLetto
                            End If

                        Else
                            ViewError(Reply)
                            fStop = True
                            GbResult.BackColor = Color.Red
                        End If
                    Else
                        ViewError(Reply)
                        fStop = True
                        GbResult.BackColor = Color.Red
                    End If
                Else
                    LBLtesto.Text = ""
                End If
            Loop While (fStop = False)


        Catch ex As Exception
            MessageBox.Show(ex.ToString())
            fStop = True
        End Try

        gbLottoResult.Visible = False

        BtDocSecondari.Enabled = True
        BtDocTest.Enabled = True

    End Sub


End Class

