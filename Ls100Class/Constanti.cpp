#include "StdAfx.h"
#include "Constanti.h"

ls100Class::Costanti::Costanti()
{
	
}



String ^ls100Class::Costanti::GetDesc(int ReturnCode)
{
			String ^Description;
			switch( ReturnCode)
			{
			case S_LS100_SERVERSYSERR:
			//case S_LS100_SYSTEM_ERROR:	
				Description = "LS100_SERVERSYSERR";
					break;
			case S_LS100_USBERR:
					Description = "LS100_USBERR";
					break;
			case S_LS100_PERIFNOTFOUND:
					Description = "LS100_PERIFNOTFOUND";
					break;
			case S_LS100_HARDERR:
					Description = "LS100_HARDERR";
					break;	
			case S_LS100_PERIFOFFON:
					Description = "LS100_PERIFOFFON";
					break;	
			case S_LS100_CODELINE_ERROR:
					Description = "LS100_CODELINE_ERROR";
					break;
				
				case S_LS100_PAPER_JAM:
					Description = "LS100_PAPER_JAM";
					break;
				case S_LS100_TARGET_BUSY:
					Description = "LS100_TARGET_BUSY";
					break;
				case S_LS100_INVALIDCMD:
					Description = "LS100_INVALIDCMD";
					break;
				case S_LS100_DATALOST:
					Description = "LS100_DATALOST";
					break;
				case S_LS100_EXECCMD:
					Description = "LS100_EXECCMD";
					break;
				case S_LS100_JPEGERROR:
					Description = "LS100_JPEGERROR";
					break;
				case S_LS100_CMDSEQUENCEERROR:
					Description = "LS100_CMDSEQUENCEERROR";
					break;
				case S_LS100_NOTUSED:
					Description = "LS100_NOTUSED";
					break;
				case S_LS100_IMAGEOVERWRITE:
					Description = "LS100_IMAGEOVERWRITE";
					break;
				case S_LS100_INVALID_HANDLE:
					Description = "LS100_INVALID_HANDLE";
					break;
				case S_LS100_NO_LIBRARY_LOAD:
					Description = "LS100_NO_LIBRARY_LOAD";
					break;
				case S_LS100_BMP_ERROR:
					Description = "LS100_BMP_ERROR";
					break;
				case S_LS100_TIFFERROR:
					Description = "LS100_TIFFERROR";
					break;
				case S_LS100_IMAGE_NO_MORE_AVAILABLE:
					Description = "LS100_IMAGE_NO_MORE_AVAILABLE";
					break;
				case S_LS100_IMAGE_NO_FILMED:
					Description = "LS100_IMAGE_NO_FILMED";
					break;
				case S_LS100_IMAGE_NOT_PRESENT:
					Description = "LS100_IMAGE_NOT_PRESENT";
					break;
				case S_LS100_FUNCTION_NOT_AVAILABLE:
					Description = "LS100_FUNCTION_NOT_AVAILABLE";
					break;
				case S_LS100_DOCUMENT_NOT_SUPPORTED:
					Description = "LS100_DOCUMENT_NOT_SUPPORTED";
					break;
				case S_LS100_BARCODE_ERROR:
					Description = "LS100_BARCODE_ERROR";
					break;
				case S_LS100_INVALID_LIBRARY:
					Description = "LS100_INVALID_LIBRARY";
					break;
				case S_LS100_INVALID_IMAGE:
					Description = "LS100_INVALID_IMAGE";
					break;
				case S_LS100_INVALID_IMAGE_FORMAT:
					Description = "LS100_INVALID_IMAGE";
					break;
				case S_LS100_INVALID_BARCODE_TYPE:
					Description = "LS100_INVALID_BARCODE_TYPE";
					break;
				case S_LS100_OPEN_NOT_DONE:
					Description = "LS100_OPEN_NOT_DONE";
					break;
				case S_LS100_INVALID_TYPE_CMD:
					Description = "LS100_INVALID_TYPE_CMD";
					break;
				case S_LS100_INVALID_CLEARBLACK:
					Description = "LS100_INVALID_CLEARBLACK";
					break;
				case S_LS100_INVALID_SIDE:
					Description = "LS100_INVALID_SIDE";
					break;
				case S_LS100_MISSING_IMAGE:
					Description = "LS100_MISSING_IMAGE";
					break;
				case S_LS100_INVALID_TYPE:
					Description = "LS100_INVALID_TYPE";
					break;
				case S_LS100_INVALID_SAVEMODE:
					Description = "LS100_INVALID_SAVEMODE";
					break;
				case S_LS100_INVALID_PAGE_NUMBER:
					Description = "LS100_INVALID_PAGE_NUMBER";
					break;
				case S_LS100_INVALID_NRIMAGE:
					Description = "LS100_INVALID_NRIMAGE";
					break;
				case S_LS100_INVALID_FRONTSTAMP:
					Description = "LS100_INVALID_FRONTSTAMP";
					break;
				case S_LS100_INVALID_WAITTIMEOUT:
					Description = "LS100_INVALID_WAITTIMEOUT";
					break;
				case S_LS100_INVALID_VALIDATE:
					Description = "LS100_INVALID_VALIDATE";
					break;
				case S_LS100_INVALID_CODELINE_TYPE:
					Description = "LS100_INVALID_CODELINE_TYPE";
					break;
				case S_LS100_MISSING_NRIMAGE:
					Description = "LS100_MISSING_NRIMAGE";
					break;
				case S_LS100_INVALID_SCANMODE:
					Description = "LS100_INVALID_SCANMODE";
					break;
				case S_LS100_INVALID_BEEP_PARAM:
					Description = "LS100_INVALID_BEEP_PARAM";
					break;
				case S_LS100_INVALID_FEEDER_PARAM:
					Description = "LS100_INVALID_FEEDER_PARAM";
					break;
				case S_LS100_INVALID_SORTER_PARAM:
					Description = "LS100_INVALID_SORTER_PARAM";
					break;
				case S_LS100_INVALID_BADGE_TRACK:
					Description = "LS100_INVALID_BADGE_TRACK";
					break;
				case S_LS100_MISSING_FILENAME:
					Description = "LS100_MISSING_FILENAME";
					break;
				case S_LS100_INVALID_QUALITY:
					Description = "LS100_INVALID_QUALITY";
					break;
				case S_LS100_INVALID_FILEFORMAT:
					Description = "LS100_INVALID_FILEFORMAT";
					break;
				case S_LS100_INVALID_COORDINATE:
					Description = "LS100_INVALID_COORDINATE";
					break;
				case S_LS100_MISSING_HANDLE_VARIABLE:
					Description = "LS100_MISSING_HANDLE_VARIABLE";
					break;
				case S_LS100_INVALID_POLO_FILTER:
					Description = "LS100_INVALID_POLO_FILTER";
					break;
				case S_LS100_INVALID_ORIGIN_MEASURES:
					Description = "LS100_INVALID_ORIGIN_MEASURES";
					break;
				case S_LS100_INVALID_SIZEH_VALUE:
					Description = "LS100_INVALID_SIZEH_VALUE";
					break;
				case S_LS100_INVALID_FORMAT:
					Description = "LS100_INVALID_FORMAT";
					break;
				case S_LS100_STRINGS_TOO_LONGS:
					Description = "LS100_STRINGS_TOO_LONGS";
					break;
				case S_LS100_READ_IMAGE_FAILED:
					Description = "LS100_READ_IMAGE_FAILED";
					break;
				
				case S_LS100_MISSING_BUFFER_HISTORY:
					Description = "LS100_MISSING_BUFFER_HISTORY";
					break;
				case S_LS100_INVALID_ANSWER:
					Description = "LS100_INVALID_ANSWER";
					break;
				case S_LS100_OPEN_FILE_ERROR_OR_NOT_FOUND:
					Description = "LS100_OPEN_FILE_ERROR_OR_NOT_FOUND";
					break;
				case S_LS100_READ_TIMEOUT_EXPIRED:
					Description = "LS100_READ_TIMEOUT_EXPIRED";
					break;
				case S_LS100_INVALID_METHOD:
					Description = "LS100_INVALID_METHOD";
					break;
				case S_LS100_CALIBRATION_FAILED:
					Description = "LS100_CALIBRATION_FAILED";
					break;
				case S_LS100_INVALID_SAVEIMAGE:
					Description = "LS100_INVALID_SAVEIMAGE";
					break;
				case S_LS100_UNIT_PARAM:
					Description = "LS100_UNIT_PARAM";
					break;
				case S_LS100_INVALID_NRWINDOWS:
					Description = "LS100_INVALID_NRWINDOWS";
					break;
				case S_LS100_INVALID_VALUE:
					Description = "LS100_INVALID_VALUE";
					break;
				case S_LS100_INVALID_DEGREE:
					Description = "LS100_INVALID_DEGREE";
					break;
				case S_LS100_R0TATE_ERROR:
					Description = "LS100_R0TATE_ERROR";
					break;
				case S_LS100_PERIPHERAL_RESERVED:
					Description = "LS100_PERIPHERAL_RESERVED";
					break;
				case S_LS100_INVALID_NCHANGE:
					Description = "LS100_INVALID_NCHANGE";
					break;
				case S_LS100_BRIGHTNESS_ERROR:
					Description = "LS100_BRIGHTNESS_ERROR";
					break;
				case S_LS100_CONTRAST_ERROR:
					Description = "LS100_CONTRAST_ERROR";
					break;
				case S_LS100_DOUBLE_LEAFING_ERROR:
					Description = "LS100_DOUBLE_LEAFING_ERROR";
					break;
				case S_LS100_INVALID_BADGE_TIMEOUT:
					Description = "LS100_INVALID_BADGE_TIMEOUT";
					break;
				case S_LS100_INVALID_RESET_TYPE:
					Description = "LS100_INVALID_RESET_TYPE";
					break;
				case S_LS100_IMAGE_NOT_200_DPI:
					Description = "LS100_IMAGE_NOT_200_DPI";
					break;

				case S_LS100_DECODE_FONT_NOT_PRESENT:
					Description = "LS100_DECODE_FONT_NOT_PRESENT";
					break;
				case S_LS100_DECODE_INVALID_COORDINATE:
					Description = "LS100_DECODE_INVALID_COORDINATE";
					break;
				case S_LS100_DECODE_INVALID_OPTION:
					Description = "LS100_DECODE_INVALID_OPTION";
					break;
				case S_LS100_DECODE_INVALID_CODELINE_TYPE:
					Description = "LS100_DECODE_INVALID_CODELINE_TYPE";
					break;
				case S_LS100_DECODE_SYSTEM_ERROR:
					Description = "LS100_DECODE_SYSTEM_ERROR";
					break;
				case S_LS100_DECODE_DATA_TRUNC:
					Description = "LS100_DECODE_DATA_TRUNC";
					break;
				case S_LS100_DECODE_INVALID_BITMAP:
					Description = "LS100_DECODE_INVALID_BITMAP";
					break;
				case S_LS100_DECODE_ILLEGAL_USE:
					Description = "LS100_DECODE_ILLEGAL_USE";
					break;
				case S_LS100_BARCODE_GENERIC_ERROR:
					Description = "LS100_BARCODE_GENERIC_ERROR";
					break;
				case S_LS100_BARCODE_NOT_DECODABLE:
					Description = "LS100_BARCODE_NOT_DECODABLE";
					break;
				case S_LS100_BARCODE_OPENFILE_ERROR:
					Description = "LS100_BARCODE_OPENFILE_ERROR";
					break;
				case S_LS100_BARCODE_READBMP_ERROR:
					Description = "LS100_BARCODE_READBMP_ERROR";
					break;
				case S_LS100_BARCODE_MEMORY_ERROR:
					Description = "LS100_BARCODE_MEMORY_ERROR";
					break;
				case S_LS100_BARCODE_START_NOTFOUND:
					Description = "LS100_BARCODE_START_NOTFOUND";
					break;
				case S_LS100_BARCODE_STOP_NOTFOUND:
					Description = "LS100_BARCODE_STOP_NOTFOUND";
					break;
				case S_LS100_PDF_NOT_DECODABLE:
					Description = "LS100_PDF_NOT_DECODABLE";
					break;
				case S_LS100_PDF_READBMP_ERROR:
					Description = "LS100_PDF_READBMP_ERROR";
					break;
				case S_LS100_PDF_BITMAP_FORMAT_ERROR:
					Description = "LS100_PDF_BITMAP_FORMAT_ERROR";
					break;
				case S_LS100_PDF_MEMORY_ERROR:
					Description = "LS100_PDF_MEMORY_ERROR";
					break;
				case S_LS100_PDF_START_NOTFOUND:
					Description = "LS100_PDF_START_NOTFOUND";
					break;
				case S_LS100_PDF_STOP_NOTFOUND:
					Description = "LS100_PDF_STOP_NOTFOUND";
					break;
				case S_LS100_PDF_LEFTIND_ERROR:
					Description = "LS100_PDF_LEFTIND_ERROR";
					break;
				case S_LS100_PDF_RIGHTIND_ERROR:
					Description = "LS100_PDF_RIGHTIND_ERROR";
					break;
				case S_LS100_PDF_OPENFILE_ERROR:
					Description = "LS100_PDF_OPENFILE_ERROR";
					break;
				case S_LS100_USER_ABORT:
					Description = "S_LS100_USER_ABORT";
					break;
				

				case S_LS100_FPGA_NOT_OK:			
					Description = "S_LS100_FPGA_NOT_OK";
					break;
				case S_LS100_BOARD_NOT_OK:
					Description = "S_LS100_BOARD_NOT_OK";
					break;
				case S_LS100_DATA_NOT_OK:
					Description = "S_LS100_DATA_NOT_OK";
					break;
				case S_LS100_FIRMWARE_NOT_OK:
					Description = "S_LS100_FIRMWARE_NOT_OK";
					break;


				// ------------------------------------------------------------------------
				//                  WARNINGS
				// ------------------------------------------------------------------------
				case S_LS100_FEEDEREMPTY:
					Description = "LS100_FEEDEREMPTY";
					break;
				case S_LS100_DATATRUNC:
					Description = "LS100_DATATRUNC";
					break;
				case S_LS100_DOCPRESENT:
					Description = "LS100_DOCPRESENT";
					break;
				case S_LS100_BADGETIMEOUT:
					Description = "LS100_BADGETIMEOUT";
					break;
				case S_LS100_PERIF_BUSY:
					Description = "LS100_PERIF_BUSY";
					break;
				case S_LS100_DOUBLE_LEAFING_WARNING:
					Description = "Double Leafing Warning";
					break ;
				case S_LS100_NO_ENDCMD:
					Description = "LS100_NO_ENDCMD";
					break;
				case S_LS100_RETRY:
					Description = "LS100_RETRY";
					break;
				case S_LS100_NO_OTHER_DOCUMENT:
					Description = "LS100_NO_OTHER_DOCUMENT";
					break;
				case S_LS100_QUEUEFULL:
					Description = "LS100_QUEUEFULL";
					break;
				case S_LS100_NOSENSE:
					Description = "LS100_NOSENSE";
					break;
				case S_LS100_TRY_TO_RESET:
					Description = "LS100_TRY_TO_RESET";
					break;
				//case S_LS100_STRINGTRUNCATED:
				//	Description = "LS100_STRINGTRUNCATED";
				//	break;

				case S_LS_STRINGTRUNCATED : 
					Description = "Nr Catteri S/N NON CORRETTO";
					break;

				case S_LS100_COMMAND_NOT_SUPPORTED:
					Description = "LS100_COMMAND_NOT_SUPPORTED";
					break;
				case S_LS100_LOOP_INTERRUPTED:
					Description = "LS100_LOOP_INTERRUPTED";
					break;

				//19/02/2016
				case S_LS_ERROR_ID_OFFSET_FLU_2:
					Description = "S_LS_ERROR_ID_OFFSET_FLU_2";
					break;

				case S_LS_ERROR_ID_OFFSET_FLU_3:
					Description = "S_LS_ERROR_ID_OFFSET_FLU_3";
					break;

				case S_LS_ERROR_ID_LED_CURRENT_2:
					Description = "S_LS_ERROR_ID_LED_CURRENT_2";
					break;

				case S_LS_ERROR_ID_LED_CURRENT_3:
					Description = "S_LS_ERROR_ID_LED_CURRENT_3";
					break;

				case S_LS_ERROR_ID_OFFSET_REF:
					Description = "S_LS_ERROR_ID_OFFSET_REF";
					break;

				case S_LS_ERROR_ID_TARGET_REF_2:
					Description = "S_LS_ERROR_ID_TARGET_REF_2";
					break;

				case S_LS_ERROR_ID_TARGET_REF_3:
					Description = "S_LS_ERROR_ID_TARGET_REF_3";
					break;

				case S_LS_ERROR_ID_OUT_TOLERANCE:
					Description = "S_LS_ERROR_ID_OUT_TOLERANCE";
					break;

				case S_LS_ERROR_ID_INK_DETECTED:
					Description = "S_LS_ERROR_ID_INK_DETECTED";
					break;

				case S_LS_ERROR_ID_VERIFY_CALIBRATION:
					Description = "S_LS_ERROR_ID_VERIFY_CALIBRATION";
					break;

				case S_LS_ERROR_SN_PIASTRA_NOT_CORRECT :
					Description = "Numero di Caratteri del SerialNumber Piastra NON Corretto";
					break;

				case S_LS_BACK_BACKGROUND :
					Description = "REAR background KO - One or more row up 64 level";
					break;

				case S_LS_BACK_BACKGROUND_1 :
					Description = "REAR background KO - More then 10 row contiguous over the level of 32 !!!";
					break;
				
				case S_LS_BACK_BACKGROUND_2 :
					Description = "REAR background KO - More then 80 row over the level of 80 !!!";
					break;

				case S_LS_FRONT_BACKGROUND :
					Description = "FRONT background KO - One or more row up 64 level";
					break;

				case S_LS_FRONT_BACKGROUND_1 :
					Description = "FRONT background KO - More then 10 row contiguous over the level of 32 !!!";
					break;
				
				case S_LS_FRONT_BACKGROUND_2 :
					Description = "FRONT background KO - More then 80 row over the level of 80 !!!";
					break;
			}
			return Description;
		}