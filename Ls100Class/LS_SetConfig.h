//
// CTS Electronics
// Corso Vercelli, 332 - 10015 Ivrea (TORINO) Italy
// January 2001
//
// Phone ++39 125 235611
// Telefax ++39 125 235623
//
// www.ctsgroup.it		techsupp@ctsgroup.it
//
//
// All rights reserved
//
// We reserve the right to vary technical specification
//
//  MODULE:   LSSETCONFIGURATION.H
//
//  PURPOSE:  LS Include Interface
#ifdef __cplusplus
extern "C" {
#endif






int LS40_WriteConfiguration(HWND hWnd, char *BytesCfg);

int LS100_WriteConfiguration(HWND hWnd, char *BytesCfg);

int LS150_WriteConfiguration(HWND hWnd, char *BytesCfg);

int LS515_WriteConfiguration(HWND hWnd, char *BytesCfg);




#ifdef __cplusplus
}
#endif

