//
// CTS Electronics
// Corso Vercelli, 332 - 10015 Ivrea (TORINO) Italy
// January 2001
//
// Phone ++39 125 235611
// Telefax ++39 125 235623
//
// www.ctsgroup.it		techsupp@ctsgroup.it
//
//
// All rights reserved
//
// We reserve the right to vary technical specification
//
//  MODULE:   LS100.H
//
//  PURPOSE:  LS100 Include Interface
#include<windows.h>

#ifndef LS100_H
#define LS100_H     1



// ------------------------------------------------------------------------
//                     DEFINES
// ------------------------------------------------------------------------

// Parameter Type_com
#define SUSPENSIVE_MODE					'S'
#define NOT_SUSPENSIVE_MODE				'T'


// Parameter FrontStamp
#define NO_FRONT_STAMP					0
#define FRONT_STAMP						1


// Parameter Validate
#define NO_PRINT_VALIDATE				0
#define PRINT_VALIDATE					1


// Parameter Feed
#define AUTOFEED						0
#define PATHFEED						1


// Parameter Sorter
#define HOLD_DOCUMENT					0
#define SORTER_BAY1						1
#define EJECT_DOCUMENT					5


// Parameter Codeline
#define NO_READ_CODELINE				0
#define READ_CODELINE_MICR				1
#define READ_BARCODE_PDF417				2
#define READ_CODELINE_OPTIC				21
#define READ_MICR_AND_OPTIC				22

#define READ_CODELINE_HW_OCRA			0x41	//'A'
#define READ_CODELINE_HW_OCRB_NUM		0x42	//'B'
#define READ_CODELINE_HW_OCRB_ALFANUM	0x43	//'C'
#define READ_CODELINE_HW_E13B			0x45	//'E'
#define READ_CODELINE_HW_OCRB_ITALY		0x46	//'F'
#define READ_CODELINE_HW_E13B_X_OCRB	0x58	//'X'
#define READ_CODELINE_HW_MULTI_READ		0x4d	//'M'

#define READ_BARCODE_2_OF_5				50
#define READ_BARCODE_CODE39				51
#define READ_BARCODE_CODE128			52
#define READ_BARCODE_EAN13				53

#define READ_CODELINE_SW_OCRA			0x41	//'A'
#define READ_CODELINE_SW_OCRB_NUM		0x42	//'B'
#define READ_CODELINE_SW_OCRB_ALFANUM	0x43	//'C'
#define READ_CODELINE_SW_OCRB_ITALY		0x46	//'F'
#define READ_CODELINE_SW_E13B			0x45	//'E'

#define READ_ONE_CODELINE_TYPE			0x4e	//'N'
#define READ_CODELINE_SW_MULTI_READ		0x4d	//'M'
#define READ_CODELINE_SW_E13B_X_OCRB	0x58	//'X'


// Parameter OriginMeasureDoc
#define BOTTOM_LEFT_PIXEL				0
#define BOTTOM_RIGHT_MM					10
#define BOTTOM_RIGHT_INCH				20


// Parameter Unit
#define UNIT_MM							0
#define UNIT_INCH						1


// OCR Hardware configuration
#define OCRH_BALNKS_NO					0x00
#define OCRH_BLANKS_YES					0x01


// Value of height to decode a software Codeline
#define MAX_PIXEL_HEIGHT				42
#define OCR_VALUE_IN_MM					10.5
#define OCR_VALUE_IN_INCH				0.41


// Parameter ResetType
#define RESET_ERROR						0x30
#define RESET_FREE_PATH					0x31


// Parameter ScanMode
#define SCAN_MODE_BW					1
#define SCAN_MODE_16GR100				2
#define SCAN_MODE_16GR200				3
#define SCAN_MODE_256GR100				4
#define SCAN_MODE_256GR200				5
#define SCAN_MODE_COLOR_100				10
#define SCAN_MODE_COLOR_200				11
#define SCAN_MODE_COLOR_AND_RED_100		12
#define SCAN_MODE_COLOR_AND_RED_200		13

// Old value leave for compatibility
#define SCAN_MODE_BW_TIFF				6


// Parameter ClearBlack
#define NO_CLEAR_BLACK					0
#define CLEAR_ALL_BLACK 				1

// Parameter Threshold
#define DEFAULT_BLACK_THRESHOLD			0x44


// Parameter Side
#define SIDE_NONE_IMAGE					'N'
#define SIDE_FRONT_IMAGE				'F'
#define SIDE_BACK_IMAGE					'B'
#define SIDE_ALL_IMAGE					'X'
#define SIDE_FRONT_BLUE_IMAGE			'G'
#define SIDE_BACK_BLUE_IMAGE			'C'
#define SIDE_ALL_BLUE_IMAGE				'Y'
#define SIDE_FRONT_GREEN_IMAGE			'H'
#define SIDE_BACK_GREEN_IMAGE			'D'
#define SIDE_ALL_GREEN_IMAGE			'W'
#define SIDE_FRONT_RED_IMAGE			'I'
#define SIDE_BACK_RED_IMAGE				'E'
#define SIDE_ALL_RED_IMAGE				'Z'


// Parameter Image Coordinate
#define IMAGE_MAX_WIDTH					1720
#define IMAGE_MAX_HEIGHT				848


// Parameter Method
#define ALGORITHM_CTS						4
#define ALGORITHM_CTS_2						5
#define ALGORITHM_NODITHERING				0x10
#define ALGORITHM_FLOYDSTEINDITHERING		0x11
#define ALGORITHM_STUCKIDITHERING			0x12
#define ALGORITHM_BURKESDITHERING			0x13
#define ALGORITHM_SIERRADITHERING			0x14
#define ALGORITHM_STEVENSONARCEDITHERING	0x15
#define ALGORITHM_JARVISDITHERING			0x16

#define DEFAULT_POLO_FILTER					450


// Parameter Format Printer
#define FORMAT_NORMAL						'N'
#define FORMAT_BOLD							'B'
#define FORMAT_NORMAL_15_CHAR				'A'

#define PRINT_UP_FORMAT_NORMAL				'n'
#define PRINT_UP_FORMAT_BOLD				'b'
#define PRINT_UP_FORMAT_NORMAL_15_CHAR		'a'


// Parameter Format Badge
#define FORMAT_IATA						0x20	// Badge Track 1
#define FORMAT_ABA						0x40	// Badge Track 2
#define FORMAT_MINTS					0x80	// Badge Track 3
#define FORMAT_IATA_ABA					0x60	// Badge Track 1 and 2
#define FORMAT_ABA_MINTS				0xc0	// Badge Track 2 and 3
#define FORMAT_IATA_ABA_MINTS			0xe0	// Badge Track 1, 2 and 3


// Parameter Timeout
#define MIN_TIMEOUT						500


// Parameter Wait_com
#define WAIT_NO							'G'
#define WAIT_YES						'W'


//Parameter Beep
#define	NO_BEEP							0
#define	BEEP							1


//Parameter SaveMode
#define	IMAGE_SAVE_ON_FILE				4
#define	IMAGE_SAVE_HANDLE				5
#define	IMAGE_SAVE_BOTH					6
#define	IMAGE_SAVE_NONE					7


//Parameter FileFormat
#define	SAVE_JPEG						10
#define	SAVE_BMP						11


// Parameter: Tiff type
#define FILE_TIF						3		// Tagged Image File Format
#define FILE_CCITT						25		// TIFF  CCITT
#define FILE_CCITT_GROUP3_1DIM			27		// CCITT Group3 one dimension
#define FILE_CCITT_GROUP3_2DIM			28		// CCITT Group3 two dimensions
#define FILE_CCITT_GROUP4				29		// CCITT Group4 two dimensions

// Parameter: uSaveMulti
#define SAVE_OVERWRITE					0
#define SAVE_APPEND						1
#define SAVE_REPLACE					2
#define SAVE_INSERT						3

// Parameter: dlouble leafing
#define DOUBLE_LEAFING_LEVEL2			2
#define DOUBLE_LEAFING_LEVEL3			3
#define DOUBLE_LEAFING_DEFAULT			4
#define DOUBLE_LEAFING_LEVEL4			5
#define DOUBLE_LEAFING_DISABLE			7


// History choise
#define CMD_READ_HISTORY				1
#define CMD_ERASE_HISTORY				2


#define	CODE_LINE_LENGTH				256		// Max length of returned codeline

#define MAX_OPTICAL_WINDOWS				1		// Nr. window * 5 bytes per window

#define PERIPHERAL_SIZE_MEMORY			36 * 1024	//Total memory of the peripheral


// String for identify the periferal connected
#define MODEL_LS100_1					"LS100USB"
#define MODEL_LS100_2					"LS100RS_"
#define MODEL_LS100_3					"LS100/3_"
#define MODEL_LS100_4					"LS100/4_"
#define MODEL_LS100_5					"LS100/5_"
#define MODEL_LS100_7					"LS100/7_"
#define MODEL_LS100_ETH					"LS100IP"

#define LS100_CALIBRATION_INK_DETECTOR_SENSOR		3



// ------------------------------------------------------------------------
//                          REPLY-CODE
// ------------------------------------------------------------------------

#define		LS_OKAY							0

// ------------------------------------------------------------------------
//                  ERRORS
// ------------------------------------------------------------------------
#define		LS100_SERVERSYSERR					-1
#define		LS100_USBERR						-2
#define		LS100_NET_ERR						-2
#define		LS100_PERIFNOTFOUND					-3
#define		LS100_HARDERR						-4
#define		LS100_PERIFOFFON					-5
#define		LS100_CODELINE_ERROR				-6
#define		LS100_BOURRAGE						-7
#define		LS100_PAPER_JAM						-7
#define		LS100_TARGET_BUSY					-8
#define		LS100_INVALIDCMD					-9
#define		LS100_DATALOST						-10
#define		LS100_EXECCMD						-11
#define		LS100_JPEGERROR						-12
#define		LS100_CMDSEQUENCEERROR				-13
#define		LS100_NOTUSED						-14
#define		LS100_IMAGEOVERWRITE				-15
#define		LS100_INVALID_HANDLE				-16
#define		LS100_NO_LIBRARY_LOAD				-17
#define		LS100_BMP_ERROR						-18
#define		LS100_TIFFERROR						-19
#define		LS100_IMAGE_NO_MORE_AVAILABLE		-20
#define		LS100_IMAGE_NO_FILMED				-21
#define		LS100_IMAGE_NOT_PRESENT				-22
#define		LS100_FUNCTION_NOT_AVAILABLE		-23
#define		LS100_DOCUMENT_NOT_SUPPORTED		-24
#define		LS100_BARCODE_ERROR					-25
#define		LS100_INVALID_LIBRARY				-26
#define		LS100_INVALID_IMAGE					-27
#define		LS100_INVALID_IMAGE_FORMAT			-28
#define		LS100_INVALID_BARCODE_TYPE			-29
#define		LS100_OPEN_NOT_DONE					-30
#define		LS100_INVALID_TYPE_CMD				-31
#define		LS100_INVALID_CLEARBLACK			-32
#define		LS100_INVALID_SIDE					-33
#define		LS100_MISSING_IMAGE					-34
#define		LS100_INVALID_TYPE					-35
#define		LS100_INVALID_SAVEMODE				-36
#define		LS100_INVALID_PAGE_NUMBER			-37
#define		LS100_INVALID_NRIMAGE				-38
#define		LS100_INVALID_FRONTSTAMP			-39
#define		LS100_INVALID_WAITTIMEOUT			-40
#define		LS100_INVALID_VALIDATE				-41
#define		LS100_INVALID_CODELINE_TYPE			-42
#define		LS100_MISSING_NRIMAGE				-43
#define		LS100_INVALID_SCANMODE				-44
#define		LS100_INVALID_BEEP_PARAM			-45
#define		LS100_INVALID_FEEDER_PARAM			-46
#define		LS100_INVALID_SORTER_PARAM			-47
#define		LS100_INVALID_BADGE_TRACK			-48
#define		LS100_MISSING_FILENAME				-49
#define		LS100_INVALID_QUALITY				-50
#define		LS100_INVALID_FILEFORMAT			-51
#define		LS100_INVALID_COORDINATE			-52
#define		LS100_MISSING_HANDLE_VARIABLE		-53
#define		LS100_INVALID_POLO_FILTER			-54
#define		LS100_INVALID_ORIGIN_MEASURES		-55
#define		LS100_INVALID_SIZEH_VALUE			-56
#define		LS100_INVALID_FORMAT				-57
#define		LS100_STRINGS_TOO_LONGS				-58
#define		LS100_READ_IMAGE_FAILED				-59
#define		LS100_INVALID_CMD_HISTORY			-60
#define		LS100_MISSING_BUFFER_HISTORY		-61
#define		LS100_INVALID_ANSWER				-62
#define		LS100_OPEN_FILE_ERROR_OR_NOT_FOUND	-63
#define		LS100_READ_TIMEOUT_EXPIRED			-64
#define		LS100_INVALID_METHOD				-65
#define		LS100_CALIBRATION_FAILED			-66
#define		LS100_INVALID_SAVEIMAGE				-67
#define		LS100_UNIT_PARAM					-68
#define		LS100_INVALID_NRWINDOWS				-71
#define		LS100_INVALID_VALUE					-72
#define		LS100_INVALID_DEGREE				-77
#define		LS100_R0TATE_ERROR					-78
#define		LS100_PERIPHERAL_RESERVED			-80
#define		LS100_INVALID_NCHANGE				-81
#define		LS100_BRIGHTNESS_ERROR				-82
#define		LS100_CONTRAST_ERROR				-83
#define		LS100_DOUBLE_LEAFING_ERROR			-85
#define		LS100_INVALID_BADGE_TIMEOUT			-86
#define		LS100_INVALID_RESET_TYPE			-87
#define		LS100_IMAGE_NOT_200_DPI				-89

#define		LS_ERROR_OFFSET_FRONT				-348
#define		LS_ERROR_OFFSET_REAR				-349
#define		LS_ERROR_SCANNER_PWM				-350
#define		LS_ERROR_GAIN_FRONT					-351
#define		LS_ERROR_GAIN_REAR					-352
#define		LS_ERROR_COEFF_FRONT				-353
#define		LS_ERROR_COEFF_REAR					-354
#define		LS_ERROR_SCANNER_GENERICO			-363

#define		LS100_DECODE_FONT_NOT_PRESENT		-1101
#define		LS100_DECODE_INVALID_COORDINATE		-1102
#define		LS100_DECODE_INVALID_OPTION			-1103
#define		LS100_DECODE_INVALID_CODELINE_TYPE	-1104
#define		LS100_DECODE_SYSTEM_ERROR			-1105
#define		LS100_DECODE_DATA_TRUNC				-1106
#define		LS100_DECODE_INVALID_BITMAP			-1107
#define		LS100_DECODE_ILLEGAL_USE			-1108

#define		LS100_BARCODE_GENERIC_ERROR			-1201
#define		LS100_BARCODE_NOT_DECODABLE			-1202
#define		LS100_BARCODE_OPENFILE_ERROR		-1203
#define		LS100_BARCODE_READBMP_ERROR			-1204
#define		LS100_BARCODE_MEMORY_ERROR			-1205
#define		LS100_BARCODE_START_NOTFOUND		-1206
#define		LS100_BARCODE_STOP_NOTFOUND			-1207

#define		LS100_PDF_NOT_DECODABLE				-1301
#define		LS100_PDF_READBMP_ERROR				-1302
#define		LS100_PDF_BITMAP_FORMAT_ERROR		-1303
#define		LS100_PDF_MEMORY_ERROR				-1304
#define		LS100_PDF_START_NOTFOUND			-1305
#define		LS100_PDF_STOP_NOTFOUND				-1306
#define		LS100_PDF_LEFTIND_ERROR				-1307
#define		LS100_PDF_RIGHTIND_ERROR			-1308
#define		LS100_PDF_OPENFILE_ERROR			-1309

#define		LS100_USER_ABORT					-9999


// ------------------------------------------------------------------------
//                  WARNINGS
// ------------------------------------------------------------------------
#define		LS100_FEEDEREMPTY					1
#define		LS100_DATATRUNC						2
#define		LS100_DOCPRESENT					3
#define		LS100_BADGETIMEOUT					4
#define		LS_ALREADY_OPEN					5
#define		LS100_PERIF_BUSY					6
#define		LS100_NO_ENDCMD						8
#define		LS100_RETRY							9
#define		LS100_NO_OTHER_DOCUMENT				10
#define		LS100_QUEUEFULL						11
#define		LS100_NOSENSE						12
#define		LS100_TRY_TO_RESET					14
#define		LS100_STRINGTRUNCATED				15
#define		LS100_COMMAND_NOT_SUPPORTED			19
#define		LS100_LOOP_INTERRUPTED				40
#define		RETRY_IMAGE_CALIBRATION				1000


// ------------------------------------------------------------------------
//					DEFINES STRUTTURES
// ------------------------------------------------------------------------
// structure for configure the read codeline from bitmap
typedef struct _ReadOption
{
	BOOL	PutBlanks;
	char	TypeRead;
}READOPTIONS, *LPREADOPTIONS;


typedef struct _DATAOPTICALWINDOW
{
	unsigned char	TypeRead;			// Type of read choise
	unsigned char	Config;				// Configuration
	short			XRightBottom;		// X1 coordinates
	short			YRightBottom;		// Y1 coordinates
	short			Size;				// size
	short			Height;				// height
} DATAOPTICALWINDOW, *PDATAOPTICALWINDOW;


// Structure with information about the just stored codeline
typedef struct _S_CODELINE_INFO
{
	short			Size;							// Size of the struct
	unsigned long	NrDoc;							// Progessive document number
	char			CodelineRead[CODE_LINE_LENGTH];	// Codeline returned
	short			NrBytes;						// Length of the codeline
} S_CODELINE_INFO, *LPS_CODELINE_INFO;


// Structure with information about the just stored image
typedef struct _S_IMAGE_INFO
{
	short			Size;				// Size of the struct
	unsigned long	NrDoc;				// Progressive document number
	HANDLE			hImage;				// Image handle
	int				ImageSize;			// Image size bytes
	int				Width;				// Image width
	int				Height;				// Image height
	int				Resolution;			// Image resolution
	int				BitCount;			// Image bit count (level of grey)
} S_IMAGE_INFO, *LPS_IMAGE_INFO;


// Structure with information about the just stored codeline
typedef struct _S_CODELINE_INFO_VB
{
	short			Size;							// Size of the struct
	unsigned long	NrDoc;							// Progessive document number
	WCHAR			CodelineRead[CODE_LINE_LENGTH];	// Codeline returned
	short			NrBytes;						// Length of the codeline
} S_CODELINE_INFO_VB, *LPS_CODELINE_INFO_VB;


// Structure with information about the just stored image
typedef struct _S_IMAGE_INFO_VB
{
	short			Size;				// Size of the struct
	unsigned long	NrDoc;				// Progressive document number
	HANDLE			hImage;				// Image handle
	int				ImageSize;			// Image size bytes
	int				Width;				// Image width
	int				Height;				// Image height
	int				Resolution;			// Image resolution
	int				BitCount;			// Image bit count (level of grey)
} S_IMAGE_INFO_VB, *LPS_IMAGE_INFO_VB;


// structure for read usefull information about the peripheral life
typedef struct _History
{
	short			Size;					// Size of the struct
	unsigned long	doc_sorted;				// Document sortered
	unsigned long	bourrage_feeder;		// Jam in the feeder
	unsigned long	bourrage_micr;			// Jam during the MICR reading
	unsigned long	doc_retain;				// Nr. of document retained
	unsigned long	bourrage_exit;			// Jam after the film
	unsigned long	doc_cmc7_err;			// Nr. of document CMC7, read with error
	unsigned long	doc_e13b_err;			// Nr. of document E13B, read with error
	unsigned long	time_peripheral_on;		// Minutes peripheral time life
	unsigned long	num_turn_on;			// Nr. of power on
	unsigned long	doc_ink_jet;			// Nr. of document printed
	unsigned long	doc_stamp;				// Nr. of document stamped

} S_HISTORY, *LPS_HISTORY;



// ------------------------------------------------------------------------
//                  EXPORT FUNCTIONS
// ------------------------------------------------------------------------
#ifdef __cplusplus
extern "C" {
#endif

extern int APIENTRY LS100_Open(HWND hWnd,
							   HANDLE Hinst,
							   char Type_com);

extern int APIENTRY LS100_Close(HWND hWnd,
								char Type_com);

extern int APIENTRY LS100_SetIPAddress(char			*Eth_IpAddress,
									   unsigned short Net_Port);

extern int APIENTRY LS100_IdentifyEx(HWND hWnd,
									 char Type_com,
									 LPSTR lpCfg,
									 LPSTR ProductVersion,
									 LPSTR Date_Fw,
									 LPSTR DecoderExpVersion,
									 LPSTR InkJetVersion,
									 LPSTR SerialNumber,
									 LPSTR Reserved);

extern int APIENTRY LS100_DisableWaitDocument(HWND hWnd,
											  char Type_com,
											  BOOL fDo);

extern int APIENTRY LS100_DocHandle(HWND			hWnd,
									char			Type_com,
									short			FrontStamp,
									short			Validate,
									short			CodeLine,
									char			Side,
									short			ScanMode,
									short			Feeder,
									short			Sorter,
									short			WaitTimeout,
									short			Beep,
									unsigned long	*NrDoc,
									short			Reserved1,
									long			Reserved2);

extern int APIENTRY LS100_DocHandleOverlapped(HWND		hWnd,
											  char		Type_com,
											  short		FrontStamp,
											  short		Validate,
											  short		CodeLine,
											  char		Side,
											  short		ScanMode,
											  short		ClearBlack,
											  short		WaitTimeout,
											  short		Beep,
											  short		NumDocument,
											  short		Reserved1,
											  long		Reserved2,
										      int (__stdcall *userfunc1)(S_CODELINE_INFO *CodelineInfo),
										      int (__stdcall *userfunc2)(S_IMAGE_INFO *ImageInfo),
										      int (__stdcall *userfunc3)(S_IMAGE_INFO *ImageInfo),
											  void		*Reserved3);

extern int APIENTRY LS100_DocHandleOverlappedVB(HWND		hWnd,
												char		Type_com,
												short		FrontStamp,
												short		Validate,
												short		CodeLine,
												char		Side,
												short		ScanMode,
												short		ClearBlack,
												short		WaitTimeout,
												short		Beep,
												short		NumDocument,
												short		Reserved1,
												long		Reserved2,
												int (__stdcall *userfunc1)(S_CODELINE_INFO_VB *CodelineInfo),
												int (__stdcall *userfunc2)(S_IMAGE_INFO_VB *ImageInfo),
												int (__stdcall *userfunc3)(S_IMAGE_INFO_VB *ImageInfo),
												void		*Reserved3);

extern int APIENTRY LS100_DocHandleAndGetData(HWND			hWnd,
											  char			Type_com,
											  short			FrontStamp,
											  short			Validate,
											  short			CodeLine,
											  short			ScanMode,
											  short			ClearBlack,
											  char			Side,
											  short			SaveImage,
											  char			*DirectoryFile,
											  char			*BaseFilename,
											  int			pos_x,
											  int			pos_y,
											  int			sizeW,
											  int			sizeH,
											  int			Quality,
											  short			FileFormat,
											  short			WaitTimeout,
											  short			Beep,
											  int			SaveMode,
											  int			PageNumber,
											  short			Reserved1,
											  LPVOID		Reserved2,

											  unsigned long	*NrDoc,
											  LPSTR			FilenameFront,
											  LPSTR			FilenameBack,
											  LPSTR			Reserved3,		// immagine Fronte NETTO
											  LPSTR			Reserved4,		// immagine Retro NETTO
							  				  LPLONG		*FrontImage,
									  		  LPLONG		*BackImage,
									  		  LPLONG		*Reserved5,		// immagine Fronte NETTO
									  		  LPLONG		*Reserved6,		// immagine Retro NETTO
									  		  LPSTR			CodelineSW,
							  				  LPSTR			CodelineHW,
									  		  LPSTR			BarCode,
									  		  LPSTR			CodelinesOptical,
											  LPVOID		Reserved7,		// not used must be NULL
											  LPVOID		Reserved8);		// not used must be NULL

extern int APIENTRY LS100_ReadImage(HWND			hWnd,
									char			Type_com,
									short			ClearBlack,
									char			Side,
									unsigned long	NrDoc,
									LPHANDLE		FrontImage,
									LPHANDLE		BackImage,
									LPHANDLE		Reserved1,
									LPHANDLE		Reserved2);

extern int APIENTRY LS100_ReadImagePiece(HWND			Hwnd,
										 char			Type_com,
										 char			Side,
										 unsigned long	NrDoc,
										 int			pos_x,
										 int			pos_y,
										 int			sizeW,
										 int			sizeH,
										 LPHANDLE		hImage);

extern int APIENTRY LS100_SetThresholdClearBlack(HWND			hWnd,
												 char			Type_com,
												 unsigned char	Threshold);

extern int APIENTRY LS100_SaveJPEG(	HWND	hWnd, 
									char	Type_com, 
									HANDLE	Face, 
									int		quality, 
									LPSTR	filename);

extern int APIENTRY LS100_SaveDIB(HWND		hWnd,
								  char		Type_com,
								  HANDLE	hImage,
								  LPSTR		filename);

extern int APIENTRY LS100_SaveTIFF(HWND		hWnd,
								   char		Type_com,
								   HANDLE	hImage,
								   LPSTR	filename,
								   int		Type,
								   int		SaveMode,
								   int		PageNumber);

extern int APIENTRY LS100_RotateImage(HWND		hWnd,
									  char		Type_com,
									  HANDLE	hImage,
									  int		degree,
									  HANDLE	*pImage);

extern int APIENTRY LS100_ImageBrightness(HWND		hWnd,
										  char		Type_com,
										  HANDLE	hImage,
										  int		nChange,
										  HANDLE	*pImage);

extern int APIENTRY LS100_ImageContrast(HWND	hWnd,
										char	Type_com,
										HANDLE	hImage,
										int		nChange,
										HANDLE *pImage);

extern int APIENTRY LS100_CutImage(HWND		hWnd,
								   char		Type_com,
								   HANDLE	hImage,
								   short	Unit,
								   float	pos_x,
								   float	pos_y,
								   float	sizeW,
								   float	sizeH,
								   HANDLE  *pImage);

extern int APIENTRY LS100_ConvertImage200To100dpi(HWND		hWnd,
												  char		Type_com,
												  HANDLE	hImage,
												  HANDLE	*pImage);

extern int APIENTRY LS100_SetOpticalWindows(HWND hWnd,
											char Type_com,
											PDATAOPTICALWINDOW pDimWindows,
											short NrWindows);

extern int APIENTRY LS100_ReadCodeline(HWND hWnd,
									   char Type_com,
									   LPSTR Codeline,
									   short *Length_Codeline,
									   LPSTR Barcode,
									   short *Length_Barcode);

extern int APIENTRY LS100_CodelineReadFromBitmap(HWND hWnd,
												 char Type_com,
												 HANDLE hImage,
												 char *codelineType,
												 short UnitMeasure,
												 float x,
												 float y,
												 float sizeW,
												 float sizeH,
												 READOPTIONS *Option,
												 LPSTR Codeline,
												 UINT *Length);

extern int APIENTRY LS100_ReadPdf417FromBitmap(HWND		hWnd,
											   char		Type_com,
											   HANDLE	hImage,
											   LPSTR	Codeline,
											   UINT		*Length,
											   char		*ErrorRate,
											   int		Reserved1,
											   int		Reserved2,
											   int		Reserved3,
											   int		Reserved4);

extern int APIENTRY LS100_ReadBarcodeFromBitmap(HWND	hWnd,
												char	Type_com,
												HANDLE	hImage,
												char	TypeBarcode,
												int		pos_x,
												int		pos_y,
												int		sizeW,
												int		sizeH,
												LPSTR	Codeline,
												UINT	*Length);

extern int APIENTRY LS100_ConvertImageToBWEx(HWND hWnd,
											 char Type_com,
											 short Method, 
											 HANDLE GrayImage,
											 LPHANDLE BWImage,
											 short PoloFilter,
											 float Reserved);

extern int APIENTRY LS100_FreeImage(HWND hWnd,
									LPHANDLE hImage);

extern int APIENTRY LS100_LoadString(HWND hWnd,
									 char Type_com,
									 char Format, 
									 short Length,
									 LPSTR String);

extern int APIENTRY LS100_LoadStringWithCounter(HWND hWnd,
												char Type_com,
												char Format,
												LPSTR String,
												short Length,
												unsigned long StartNumber,
												short Step);

extern int APIENTRY LS100_ReadBadge(HWND hWnd,
									char Type_com,
									unsigned char Format,
									short MaxLength, 
									char *String,
									short *length);

extern int APIENTRY LS100_ReadBadgeWithTimeout(HWND hWnd, 
											char Type_com, 
											unsigned char Format,
											short MaxLength, 
											char *String, 
											short *length, 
											long timeout);

extern int APIENTRY LS100_ResetEx(HWND	hWnd,
								 char	Type_com,
								 char	ResetType);

extern int APIENTRY LS100_Wait(HWND hWnd,
							   int  *ReplyCmd,
							   void *Reserved);

extern int APIENTRY LS100_Test(HWND hWnd,
							   int  *ReplyCmd,
							   void *Reserved);

extern int APIENTRY LS100_DisplayImage(HWND hWnd,
									   HANDLE hInstance,
									   char TypeCom,
									   char *FilenameFront,
									   char *FilenameBack,
									   int XFront,
									   int YFront,
									   int XBack,
									   int YBack,
									   int FrontWidth,
									   int FrontHeight,
									   int BackWidth,
									   int BackHeight,
									   HWND *RetHwndFront,
									   HWND *RetHwndBack);

extern int APIENTRY LS100_UpdateImage(HWND hWnd,
									  char TypeCom,
									  char *FilenameFront,
									  char *FilenameBack,
									  HWND hWndFront,
									  HWND hWndBack);

extern int APIENTRY LS100_AutoDocHandleEx(HWND		hWnd,
										  char		Type_com,
										  short		FrontStamp,
										  short		Validate,
										  short		CodeLine,
										  short		ScanMode,
										  short		NumDocument,
										  short		ClearBlack,
										  char		Side,
										  short		SaveImage,
										  char		*DirectoryFile,
										  char		*BaseFilename,
										  float		pos_x,
										  float		pos_y,
										  float		sizeW,
										  float		sizeH,
										  int		Quality,
										  short		FileFormat,
										  short		WaitTimeout,
										  short		Beep,
										  int		SaveMode,
										  int		PageNumber,
										  short		OriginMeasureDoc,
										  LPVOID	Reserved1,
										  LPVOID	Reserved2,
										  LPVOID	Reserved3);

extern int APIENTRY LS100_AutoDocHandle(HWND	hWnd,
										char	Type_com,
										short	FrontStamp,
										short	Validate,
										short	CodeLine,
										short	ScanMode,
										short	NumDocument,
										short	ClearBlack,
										char	Side,
										short	SaveImage,
										char	*DirectoryFile,
										char	*BaseFilename,
										int		pos_x,
										int		pos_y,
										int		sizeW,
										int		sizeH,
										int		Quality,
										short	FileFormat,
										short	WaitTimeout,
										short	Beep,
										int		SaveMode,
										int		PageNumber,
										short	OriginMeasureDoc,
										LPVOID	Reserved1);

extern int APIENTRY LS100_GetDocData(HWND			hWnd,
									 char			Wait_com,
									 unsigned long	*NrDoc,
									 LPSTR			FilenameFront,
									 LPSTR			FilenameBack,
									 LPSTR			Reserved1,		// not used must be NULL
									 LPSTR			Reserved2,		// not used must be NULL
									 LPLONG			*FrontImage,
									 LPLONG			*BackImage,
									 LPLONG			*Reserved3,		// not used must be NULL
									 LPLONG			*Reserved4,		// not used must be NULL
									 LPSTR			CodelineSW,
									 LPSTR			CodelineHW,
									 LPSTR			BarCode,
									 LPSTR			CodelinesOptical,
									 short			*DocToRead,
									 LONG			*ErrorType,		// set to 0
									 LPVOID			Reserved5,		// not used must be NULL
									 LPVOID			Reserved6);		// not used must be NULL

extern int APIENTRY LS100_StopAutoDocHandle(HWND hWnd,
											char Type_com);

extern int APIENTRY LS100_PeripheralStatus(HWND			 hWnd,
										   unsigned char *SenseKey,
										   unsigned char *StatusByte);

extern int APIENTRY LS100_GetVersion(char *, short);

extern int APIENTRY LS100_SetThresholdBW(HWND			hWnd,
										 unsigned char	*Soglia);

extern int APIENTRY LS100_ViewOCRRectangle(HWND hWnd,
										   BOOL fView);

extern int APIENTRY LS100_HistoryCommand(HWND		hWnd,
										 short		Cmd,
										 S_HISTORY	*sHistory);

extern int APIENTRY LS100_DownloadFirmware(HWND		hWnd,
										   char		*FileFw,
										   int		(__stdcall *userfunc1)(char *Item));

extern int APIENTRY LS100_DoubleLeafingSensibility(HWND hWnd,
												   unsigned char Value);


// -------------------------------------------------------------
// Old Functions leave for compatibility
// -------------------------------------------------------------
extern int APIENTRY LS100_Identify(HWND hWnd,
								   CHAR Type_com,
								   LPSTR lpldPnt, 
								   LPSTR vendorModelPar,
								   LPSTR productVersionPar,
								   LPSTR DecoderExpVersion,
								   LPSTR InkJetVersion);

extern int APIENTRY LS100_Reset(HWND hWnd,
								char Type_com);

extern int APIENTRY LS100_RS232Configuration(HWND, char *, DWORD, BYTE, BYTE, BYTE);

extern int APIENTRY LS100_PeriphStatus(HWND			 hWnd,
									   unsigned char *SenseKey,
									   unsigned char *StatusByte);

extern int APIENTRY LS100_OpenDownload(HWND		hWnd,
									   HANDLE	Hinst);

extern int APIENTRY LS100_ReadCodelineFromBitmap(HWND hWnd,
												 char Type_com,
												 HANDLE hImage,
												 char *codelineType,
												 int x,
												 int y,
												 int sizeW,
												 int sizeH,
												 READOPTIONS *Option,
												 LPSTR Codeline,
												 UINT *Length);

extern int APIENTRY LS100_ConvertImageToBW(HWND hWnd,
										   char Type_com,
										   short Method, 
										   HANDLE GrayImage,
										   LPHANDLE BWImage);


// -------------------------------------------------------------
// Functions for LS100 IP version
// -------------------------------------------------------------
extern int APIENTRY LS100Eth_Open(HWND			hWnd,
								  HANDLE		hInst,
								  char			*New_Eth_IpAddress,
								  unsigned short New_Net_Port,
								  short			*hConnect);

extern int APIENTRY LS100Eth_Close(short	hConnect,
								   HWND		hWnd);

extern int APIENTRY LS100Eth_ReserveUnit(short hConnect,
										 HWND  hWnd,
										 long  Reserved);

extern int APIENTRY LS100Eth_ReleaseUnit(short hConnect,
										 HWND  hWnd,
										 long  Reserved);

extern int APIENTRY LS100Eth_Identify(short	hConnect,
									  HWND	hWnd,
									  LPSTR lpCfg,
									  LPSTR ProductVersion,
									  LPSTR Date_Fw,
									  LPSTR DecoderExpVersion,
									  LPSTR InkJetVersion,
									  LPSTR SerialNumber,
									  LPSTR BoardAndFPGA,
									  LPSTR Reserved);

extern int APIENTRY LS100Eth_DisableWaitDocument(short hConnect,
												 HWND  hWnd,
												 BOOL  fDo);

extern int APIENTRY LS100Eth_DoubleLeafingSensibility(short			hConnect,
													  HWND			hWnd,
													  unsigned char Value);

extern int APIENTRY LS100Eth_DocHandle(short hConnect,
									   HWND  hWnd,
									   short FrontStamp,
									   short Validate,
									   short CodeLine,
									   char  Side,
									   short ScanMode,
									   short Feeder,
									   short Sorter,
									   short WaitTimeout,
									   short Beep,
									   short Reserved1,
									   long  Reserved2);

extern int APIENTRY LS100Eth_ReadCodeline(short hConnect,
										  HWND  hWnd,
										  LPSTR Codeline,
										  short *Length_Codeline,
										  LPSTR Reserved1,
										  short *Reserved2);

extern int APIENTRY LS100Eth_ReadImage(short		hConnect,
									   HWND			hWnd,
									   short		ClearBlack,
									   char			Side,
									   LPHANDLE		FrontImage,
									   LPHANDLE		BackImage,
									   LPHANDLE		Reserved1,
									   LPHANDLE		Reserved2);

extern int APIENTRY LS100Eth_LoadString(short hConnect,
										HWND  hWnd,
										char  Format,
										short Length,
										LPSTR String);

extern int APIENTRY LS100Eth_AutoDocHandle(short	hConnect,
											HWND	hWnd,
											short	FrontStamp,
											short	Validate,
											short	CodeLine,
											short	ScanMode,
											short	NumDocument,
											short	ClearBlack,
											char	Side,
											short	SaveImage,
											char	*DirectoryFile,
											char	*BaseFilename,
											float	pos_x,
											float	pos_y,
											float	sizeW,
											float	sizeH,
											int		Quality,
											short	FileFormat,
											short	WaitTimeout,
											short	Beep,
											int		SaveMode,
											int		PageNumber,
											short	OriginMeasureDoc,
											LPVOID	Reserved1,
											LPVOID	Reserved2,
											LPVOID	Reserved3);

extern int APIENTRY LS100Eth_GetDocData(short		hConnect,
									  HWND			hWnd,
									  LPSTR			FilenameFront,
									  LPSTR			FilenameBack,
									  LPSTR			FilenameFrontNetto,
									  LPSTR			FilenameBackNetto,
									  LPLONG		*FrontImage,
									  LPLONG		*BackImage,
									  LPLONG		*FrontImageNetto,
									  LPLONG		*BackImageNetto,
									  LPSTR			CodelineSW,
									  LPSTR			CodelineHW,
									  LPSTR			BarCode,
									  LPSTR			CodelinesOptical,
									  short			*DocToRead,
									  long			*ErrorType,
									  LPVOID		Reserved5,
									  LPVOID		Reserved6);

extern int APIENTRY LS100Eth_Reset(short hConnect,
								   HWND  hWnd,
								   char  ResetType);

extern int APIENTRY LS100Eth_PeriphStatus(short			hConnect,
										  HWND			hWnd,
										  unsigned char *SenseKey,
										  unsigned char *StatusByte);

extern int APIENTRY LS100Eth_ReadBadge(short		hConnect,
									   HWND			hWnd,
									   unsigned char Format,
									   short		MaxLength,
									   char			*String,
									   short		*length);

extern int APIENTRY LS100Eth_ReadBadgeWithTimeout(short			hConnect,
												  HWND			hWnd,
												  unsigned char Format,
												  short			MaxLength,
												  char			*String,
												  short			*length,
												  long			timeout);

extern int APIENTRY LS100Eth_SetThresholdClearBlack(short		  hConnect,
													HWND		  hWnd,
													unsigned char Threshold);

extern int APIENTRY LS100Eth_HistoryCommand(short	  hConnect,
											HWND	  hWnd,
											short	  Cmd,
											S_HISTORY *sHistory);

extern int APIENTRY LS100Eth_DownloadFirmware(short hConnect,
											  HWND  hWnd,
											  char *FileFw,
											  int (*userfunc1)(char *Item));


// -------------------------------------------------------------
// CTS Reserved function
// -------------------------------------------------------------
extern int APIENTRY LS100_OpenDiagnostica(HWND, HANDLE, char);

extern int APIENTRY LS100_ReadE13BSignal(HWND, unsigned char *, long *);

extern int APIENTRY LS100_ScannerCalibration(HWND, short);

extern int APIENTRY LS100_PhotosCalibration(HWND, short);

extern int APIENTRY LS100_DumpPeripheralMemory(HWND, unsigned char *, long, long);

extern int APIENTRY LS100_CalibrationMICR(HWND, unsigned char *, short);

extern int APIENTRY LS100_BuilderSetting(HWND, void *);

extern int APIENTRY LS100_SetDiagnosticMode(HWND, long);

extern int APIENTRY LS100_SetConfiguration(HWND, char *);

extern int APIENTRY LS100_SetTrimmerMICR(HWND, short);

extern int APIENTRY LS100_ImageCalibration(HWND, BOOL, short *);

extern int APIENTRY LS100_SetInImageCalibration(HWND hWnd);

extern int APIENTRY LS100_SetChoiceReadMICRLowLevel(HWND, unsigned char);

extern int APIENTRY LS100_SetSerialNumber(HWND, unsigned char *, short);

extern int APIENTRY LS100_SetScannerLight(HWND, short);

extern int APIENTRY LS100_TestCinghia(HWND, unsigned char *, long *);

extern int APIENTRY LS100_ReadTimeSampleMICR(HWND, char, unsigned char *, short);

extern int APIENTRY LS100_SetTimeSampleMICR(HWND, char, short);

#ifdef __cplusplus
}
#endif

/////////////////////////////////////////////////////////////////////////

#endif