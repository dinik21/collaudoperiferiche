// File DLL principale.

#include "stdafx.h"


#include "Ls100Class.h"
#include "ErrWar.h"
//#include "ls100.h"
//15/02/2015
#include "LsApi.h"
#include "LsApi_Calibr.h"
//17/11/2016
#include "LS_SetConfig.h"

#include <stdio.h>

#define OFF_PHOTO_1					0x0002
#define OFF_PHOTO_2					0x0003
#define OFF_PHOTO_3					0x0004
#define OFF_PHOTO_4					0x0005
#define PHOTO_INI_DUMP				0x8d50
#define OFF_PHOTO_LAN_1				0x0000
#define OFF_PHOTO_LAN_2				0x0002
#define OFF_PHOTO_LAN_3				0x0004
#define OFF_PHOTO_LAN_4				0x0006
#define PHOTO_INI_LAN_DUMP			0x0000
#define PHOTO_DUMP_SIZE_MEMORY		0x10


#define UNITA_VOLT						0.01953125//0.019042676
#define TRIMMER_PRE_READ				3
#define TRIMMER_MIN_VALUE				0x00
#define TRIMMER_MAX_VALUE				0xff
#define TRIMMER_MIN_VALUE_3				0x00
#define TRIMMER_MAX_VALUE_3				0x1f
#define NR_VALORI_LETTI					512
#define OFFSET_INZIO_CALCOLO			250
#define NR_VALORI_PER_MEDIA				200
#define OFFSET_PRIMO_PICCO				72

#define SCANNER_BACK					0
#define SCANNER_FRONT					1

#define LIMITE_16_GRIGI				0x06
#define LIMITE_256_GRIGI			0x66
#define NR_VALORI_OK				228
#define E13B_FINE_LLC				0x0d		// Terminatore dati Larghezza char
#define DIM_DESCR_CHAR				5
#define HEIGHT_BAR					50


#define NR_VALORI_PER_MEDIA				200
#define ZERO_TEORICO					0x80
// Defines taratura MICR
#define TENSIONE_VCC_LS100ETH				3.3		// Tensione di alimentazione del circuito MICR
#define MOLTIPL_RIFERIMENTO_VOLT_LS100ETH	0.4		// Moltiplicatore per riferimento in Volt
#define NR_VALORI_CONVERTITORE_LS100ETH		256


#define VALORE_MAX_PICCO_MIN			0x60
#define NR_PICCHI						50
#define MAX_READ_RETRY					3
#define PICCO_DISTANZA_MINIMA			100
#define NR_BYTE_BUFFER_E13B				0x4000

#define SOGLIA_INIZIO_NERO			0x80 - 0x10	// Valore per inizio barretta nera

#define SAMPLE_VALUE_MIN				107.0f
#define SAMPLE_VALUE_MAX				116.0f
#define SAMPLE_VALUE_MEDIO				112.0f

#define SPEED_GRAY						0x0001
#define SPEED_COLOR						0x0101

#define SPEED_200_DPI					0x00
#define SPEED_300_DPI					0x01


#define TYPE_LS100_1					101
#define TYPE_LS100_2					102
#define TYPE_LS100_3					103
#define TYPE_LS100_4					104
#define TYPE_LS100_5					105
#define TYPE_LS100_7					107
#define TYPE_LS100_8					108
#define TYPE_LS100_ETH					110

#define RETRY_IMAGE_CALIBRATION			1000

unsigned char Limite16Grigi = LIMITE_16_GRIGI;			// Soglia nera per Clear Black
unsigned char Limite256Grigi = LIMITE_256_GRIGI;		// Soglia nera per Clear Black

void ClearFrontAndRearBlack(BITMAPINFOHEADER *Bitmap, short *ColNereAnt);


Ls100Class::Periferica::Periferica(void)
{
	
	ReturnC = gcnew ErrWar();


	S_Storico = gcnew Storico();
	


	MICR_TolleranceMore_3 = 7;
	MICR_TolleranceLess_3 = 7;
	MICR_Valore100_3 = 2.00;
	
	
	hDlg = 0;
	//hInst = 0;
	GetVersione();

	TipoPeriferica = LS_100_USB;

	Connessione = 0;

	

	
	
}
int Ls100Class::Periferica::GetVersione()
{
	char Versione[120];
	
	VersioneDll_1 = "C.T.S. Ls100Class.dll -- Ver. 1.00, Rev 002,  date 25/11/2009";
	

	//Reply = LS100_Open((HWND)hDlg,(HANDLE)hInst,SUSPENSIVE_MODE);
	// Chiamo la funzione per via documento
	//if ((Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN))
	{
		//Reply = LS100_GetVersion(Versione,80);
		Reply = LSGetVersion(Versione,80);
		//if(Reply == LS_OKAY)
		if(Reply == LS_OKAY)
		{
			VersioneDll_0 = Marshal::PtrToStringAnsi((IntPtr) (char *)Versione);
			
		}
	}
	//LS100_Close((HWND)hDlg,SUSPENSIVE_MODE);
	return 0;
}


short Ls100Class::Periferica::GetTypeLS(String ^Version)
{

	String ^mm;
	mm = MODEL_LS100_1;

	TypeLS = TYPE_LS100_1;

	//Identifico la periferica se LS100 USB only or USB/RS232
	if ( String::Compare(Version,0,MODEL_LS100_1,0,mm->Length) == 0)
		TypeLS = TYPE_LS100_1;
	else 
		mm =MODEL_LS100_2;
		if( String::Compare(Version,0,MODEL_LS100_2,0,mm->Length) == 0)
			TypeLS = TYPE_LS100_2;
	else
		mm = MODEL_LS100_3;
		if( String::Compare(Version,0,MODEL_LS100_3,0,mm->Length) == 0)
			TypeLS = TYPE_LS100_3;
	else
		mm = MODEL_LS100_4;
		if( String::Compare(Version,0,MODEL_LS100_4,0,mm->Length) == 0)
			TypeLS = TYPE_LS100_4;
	else 
		mm = MODEL_LS100_5;
		if( String::Compare(Version,0,MODEL_LS100_5,0,mm->Length) == 0)
		TypeLS = TYPE_LS100_5;
	else 
		mm = MODEL_LS100_7;
		if( String::Compare(Version,0,MODEL_LS100_7,0,mm->Length) == 0)
		TypeLS = TYPE_LS100_7;
	else 
		mm = MODEL_LS100_8;
		if( String::Compare(Version,0,MODEL_LS100_8,0,mm->Length) == 0)
		TypeLS = TYPE_LS100_8;
	else 
		mm = MODEL_LS100_ETH;
		if( String::Compare(Version,0,MODEL_LS100_ETH,0,mm->Length) == 0)
		TypeLS = TYPE_LS100_ETH;

	return TypeLS;
} // GetTypeLS




int Ls100Class::Periferica::Identificativo()
{
	char IdentStr[64],Version[64],Date_Fw[64],InkJet[64],SerialNumber[64],LsName[64] ,FeederVersion[64];
	char   BoardNr[8]; 
	short Board,Fpga;
	short Connessione;

	ReturnC->FunzioneChiamante = "Test Identificativo";
	
	//Reply = LS100_Open((HWND)hDlg,(HANDLE)hInst,SUSPENSIVE_MODE);
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	// Chiamo la funzione per via documento
	//if ((Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN))
	if (Reply == LS_OKAY)
	{
		//Reply = LS100_IdentifyEx((HWND)hDlg, SUSPENSIVE_MODE, IdentStr, Version, Date_Fw, Lema, InkJet, SerialNumber, BoardAndFPGANr);
		Reply =	LSUnitIdentify(Connessione,(HWND)0, IdentStr,LsName,Version,Date_Fw,SerialNumber,BoardNr, NULL, InkJet, FeederVersion, NULL, NULL, NULL, NULL);

		//if (Reply == LS_OKAY)
		if (Reply == LS_OKAY)
		{
			SIdentStr = Marshal::PtrToStringAnsi((IntPtr) (char *)IdentStr);
			SVersion = Marshal::PtrToStringAnsi((IntPtr) (char *)Version);
			SDateFW = Marshal::PtrToStringAnsi((IntPtr) (char *)Date_Fw);
			//SLema = Marshal::PtrToStringAnsi((IntPtr) (char *)Lema);
			SInkJet = Marshal::PtrToStringAnsi((IntPtr) (char *)InkJet);
			//Board = BoardAndFPGANr[0];
			Board = BoardNr[0];
			if(Board > 0)
			{
				SBoard = Board.ToString();
			}
			else
				SBoard = "";
			Fpga = BoardNr[1];
			if(Fpga > 0)
			{
				Fpga-= 48;
				SFPGANr = Fpga.ToString();
			}
			else
				SFPGANr = "";
			SSerialNumber = Marshal::PtrToStringAnsi((IntPtr) (char *)SerialNumber);
			
			
			GetTypeLS(SVersion);
		}
		else
		{
			ReturnC->FunzioneChiamante= "LSUnitIdentify";
		}
		//LS100_Close((HWND)hDlg,SUSPENSIVE_MODE);
		LSDisconnect(Connessione,(HWND)hDlg);
	}
	else
	{
		ReturnC->FunzioneChiamante= "LSOpen";
	}
	ReturnC->ReturnCode = Reply;
	return Reply;
}


int Ls100Class::Periferica::CheckReply(HWND hDlg,int Rep,char *s)
{
	ReturnC->FunzioneChiamante = Marshal::PtrToStringAnsi((IntPtr) (char *)s);
	return TRUE;
}



int Ls100Class::Periferica::LeggiCodelineOCR(short Type)
{
	//short ret;
	unsigned long NrDoc;
	short lenCodeline=80;
	char szCodeline[256];
	char *StrNome=NULL;
	DATAOPTICALWINDOW pDimWindows;
	HANDLE HFrontGray = 0;
	short Connessione;

	ReturnC->FunzioneChiamante = "LeggiCodelineOCR";
	//Reply = LS100_Open((HWND)hDlg,(HANDLE)hInst,SUSPENSIVE_MODE);
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	// Chiamo la funzione per via documento
	if ((Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN))
	{
		pDimWindows.TypeRead = (unsigned char)Type;
		//pDimWindows.Config = OCRH_BALNKS_NO;
		pDimWindows.XRightBottom = (short)0;
		pDimWindows.YRightBottom = (short)4;
		pDimWindows.Size = (short)-1;
		pDimWindows.Height = (short)(14);

		/*Reply = LS100_SetOpticalWindows((HWND)hDlg,
										SUSPENSIVE_MODE,
										&pDimWindows,
										1);*/
		Reply = LSSetOpticalWindows(Connessione, (HWND)hDlg, &pDimWindows, 1);

		/*Reply = LS100_DocHandle((HWND)hDlg,
								SUSPENSIVE_MODE,
								NO_FRONT_STAMP,
								NO_PRINT_VALIDATE,
								READ_CODELINE_OPTIC,
								SIDE_FRONT_IMAGE,
								SCAN_MODE_256GR200,
								AUTO_FEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								&NrDoc,
								0,*/
		Reply = LSDocHandle(Connessione, (HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								READ_CODELINE_OPTIC,
								SIDE_FRONT_IMAGE,
								SCAN_MODE_256GR200 , 
								AUTO_FEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								&NrDoc,
								0, 0);

		if (Reply == LS_OKAY)
		{
			//Reply = LS100_ReadCodeline((HWND)hDlg,'S',NULL,&ret,Code,&Length_Codeline);
			Reply = LSReadCodeline(Connessione, (HWND)hDlg, szCodeline, &lenCodeline, NULL, NULL, NULL, NULL);

			if (Reply == LS_OKAY)
			{
				/*Reply = LS100_ReadImage((HWND)hDlg,
									SUSPENSIVE_MODE,
									CLEAR_ALL_BLACK , 
									SIDE_FRONT_IMAGE,
									NrDoc,
									(LPHANDLE)&HFrontGray,
									NULL,
									NULL,
									NULL);*/
				Reply = LSReadImage(Connessione, (HWND)hDlg,
										CLEAR_ALL_BLACK,
										SIDE_FRONT_IMAGE,
										0, NrDoc,
										(LPHANDLE)&HFrontGray,
										NULL,
										NULL,
										NULL);

				CodelineLetta = Marshal::PtrToStringAnsi((IntPtr) (char *)szCodeline);
				//CodelineLetta = Code;
				// salvo le immagini
				
					//NomeFileImmagine = "..\\Dati\\FrontImage.bmp";
				
					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
					//Reply = LS100_SaveDIB((HWND)hDlg, SUSPENSIVE_MODE, HFrontGray,StrNome);
					Reply = LSSaveDIB((HWND)hDlg,HFrontGray,StrNome);

					// libero le immagini
					if (HFrontGray)
						//LS100_FreeImage((HWND)hDlg, &HFrontGray);
						LSFreeImage((HWND)hDlg, &HFrontGray);
					
				//LS100_Close((HWND)hDlg,SUSPENSIVE_MODE);
				LSDisconnect(Connessione,(HWND)hDlg);
			}
			else
			{	
				ReturnC->FunzioneChiamante= "LS100_ReadCodeline";
			}
		}
		else
		{
			ReturnC->FunzioneChiamante= "LS100_DocHandle";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante= "LS100_Open";
	}

		
	ReturnC->ReturnCode = Reply;
	return Reply;
}



int Ls100Class::Periferica::CalibrazioneFoto()
{
	unsigned char pd[16];
	int llBuffer;
	long	iniAddr = 0 ; 
	short Connessione;

	ReturnC->FunzioneChiamante = "CalibrazioneFoto";
	//Reply = LS100_Open((HWND)hDlg,(HANDLE)hInst,SUSPENSIVE_MODE);
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	// Chiamo la funzione per via documento
	//if ((Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN))
	if (Reply == LS_OKAY)
	{
		//Reply = LS100_PhotosCalibration((HWND)hDlg, 0);
		Reply = LSPhotosCalibration(Connessione,(HWND)hDlg,0);
		//if (Reply == LS_OKAY)
		if (Reply == LS_OKAY)
		{
			if (TypeLS == 0 ) 
				TypeLS = TYPE_LS100_7 ; 
			else
				GetTypeLS( SVersion );
			
			if( TypeLS == TYPE_LS100_ETH || TypeLS == TYPE_LS100_7 || TypeLS == TYPE_LS100_8 )
				iniAddr = 0;
			else
				iniAddr = PHOTO_INI_DUMP;
			
			llBuffer = 16;
			
			//LS100_DumpPeripheralMemory((HWND)hDlg, (unsigned char *)pd, llBuffer, iniAddr);
			Reply = LSDumpPeripheralMemory(Connessione, (HWND)hDlg, (unsigned char *)pd, llBuffer, iniAddr, 0);

			//if( Reply == LS_OKAY )
			{

				// Leggo il valore dei photo
				if( TypeLS == TYPE_LS100_ETH || TypeLS == TYPE_LS100_7 || TypeLS == TYPE_LS100_8 )
				{
					PhotoValue[0] = *(unsigned short *)(pd + OFF_PHOTO_LAN_1);
					PhotoValue[1] = *(unsigned short *)(pd + OFF_PHOTO_LAN_2);
					PhotoValue[2] = *(unsigned short *)(pd + OFF_PHOTO_LAN_3);
					PhotoValue[3] = *(unsigned short *)(pd + OFF_PHOTO_LAN_4);
				}
				else
				{
					PhotoValue[0] = *(pd + OFF_PHOTO_1);
					PhotoValue[1] = *(pd + OFF_PHOTO_2);
					PhotoValue[2] = *(pd + OFF_PHOTO_3);
					PhotoValue[3] = *(pd + OFF_PHOTO_4);
				}
				
			}
		}
		else
		{
			ReturnC->FunzioneChiamante= "LSPhotosCalibration";
		}
		//LS100_Close((HWND)hDlg,SUSPENSIVE_MODE);
		LSDisconnect(Connessione,(HWND)hDlg);
	}
	else
	{
		ReturnC->FunzioneChiamante= "LsOpen";
	}
	ReturnC->ReturnCode = Reply;
	
	return Reply;
}

int Ls100Class::Periferica::CalSfogliatura()
{
	unsigned short pDump[24],Threshold[24];
	int llBuffer;
	short Connessione;

	//ReturnC->FunzioneChiamante = "CalSfogliatura";
	//Reply = LS100_Open((HWND)hDlg,(HANDLE)hInst,SUSPENSIVE_MODE);
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	// Chiamo la funzione per via documento
	//if ((Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN))
	if( Reply == LS_OKAY )
	{
		// double leafing 
		//Reply = LS100_PhotosCalibration((HWND)hDlg, 1);
		Reply = LSPhotosCalibration(Connessione,(HWND)hDlg,1);
		//if (Reply == LS_OKAY)
		if( Reply == LS_OKAY )
		{
			llBuffer = 32;
			LSReadPhotosValue(Connessione,(HWND)0,pDump,Threshold);
			if( Reply == LS_OKAY )
			{
				// Leggo il valore dei photo
				PhotoValue[0] = pDump[0];
				PhotoValue[1] = pDump[1];
				PhotoValue[2] = pDump[2];
				// Doppia sfogliatura..... 
				PhotoValueDP[0] = (unsigned char)pDump[3];
				PhotoValueDP[1] = (unsigned char)pDump[4];
				PhotoValueDP[2] = (unsigned char)pDump[5];

				PhotoValueDP[3] = (unsigned char)Threshold[0];
				PhotoValueDP[4] = (unsigned char)Threshold[1];
				PhotoValueDP[5] = (unsigned char)Threshold[2];
				PhotoValueDP[6] = (unsigned char)Threshold[3];
				PhotoValueDP[7] = (unsigned char)Threshold[4];
				PhotoValueDP[8] = (unsigned char)Threshold[5];
			}
			else
			{
				ReturnC->FunzioneChiamante= "LSReadPhotosValue";
			}

			
		}
		else
		{
			ReturnC->FunzioneChiamante= "LSPhotosCalibration";
		}
		
		

		//LS100_Close((HWND)hDlg,SUSPENSIVE_MODE);
		LSDisconnect(Connessione,(HWND)hDlg);	
	}

	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Calibrazione Doppia Sfogl.";
	return Reply;
}



//NON USATA why ?
//int Ls100Class::Periferica::DoMICRCalibration3(int MICR_SignalValue,long NrReadGood,  long TypeSpeed)
//{
//	short	Connessione;
//	int ii, jj;
//	short	Reply = LS_OKAY;
//	short	Retry;
//	float	TotValRif, MinValue;
//	float	vTotValRif[16], vMinValue[16], vPercento[16];
//	float	RangeMore, RangeLess;
//	unsigned char	TrimmerValue, NewTrimmerValue;
//	float	MediaPre;
//	unsigned char buffDati[NR_VALORI_LETTI];		// Dati da chiedere
//	unsigned long NrDoc;
//
//	ReturnC->FunzioneChiamante = "DoMICRCalibration3";
//
//	MinValue = TotValRif = 0;
//	Retry = 1;
//	nrLettura = 0;
//	Percentile = 0;
//
//	// Setto i parametri per LS100 seriale
//	MICR_SignalValue_3 = MICR_SignalValue;
//
//	//Reply = LS100_Open((HWND)hDlg,(HANDLE)hInst,SUSPENSIVE_MODE);
//	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
//
//	if ((Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN))
//	{
//		//LS100_Reset((HWND)hDlg, SUSPENSIVE_MODE);
//		
//		//----------- Set attesa introduzione documento -----------
//	    Reply = LSDisableWaitDocument(Connessione, (HWND)hDlg, FALSE);
//		// Setto il diagnostic mode
//		//Reply = LS100_SetDiagnosticMode((HWND)hDlg, TypeSpeed);
//		Reply = LSSetDiagnosticMode(Connessione, (HWND)hDlg, TypeSpeed);
//	
//		if (Reply == LS_OKAY)
//		{
//			// Lettura segnale
//			//Reply = LS100_CalibrationMICR((HWND)hDlg, buffDati, NR_VALORI_LETTI);
//			Reply = LSCalibrationMICR(Connessione, (HWND)hDlg, buffDati, NR_VALORI_LETTI);
//			
//			if( Reply == LS_OKAY)
//			{
//				// Salvo il valore del trimmer
//				TrimmerValue = buffDati[0];
//
//				// Eseguo il primo loop di misurazione di 3 letture
//				for( ii = 0; ii < (TRIMMER_PRE_READ + NrReadGood); ii++ )
//				{
//					nrLettura ++;
//
//					//// Chiamo la funzione per via documento
//					//Reply = LS100_DocHandle((HWND)hDlg,
//					//						SUSPENSIVE_MODE,
//					//						NO_FRONT_STAMP,
//					//						NO_PRINT_VALIDATE,
//					//						READ_CODELINE_MICR,
//					//						SIDE_NONE_IMAGE,
//					//						SCAN_MODE_256GR200,
//					//						AUTO_FEED,
//					//						SORTER_BAY1,
//					//						WAIT_YES,
//					//						NO_BEEP,
//					//						&NrDoc,
//					//						0,
//					//						0);
//					
//					// Chiamo la funzione per via documento
//					Reply = LSDocHandle(Connessione, (HWND)hDlg,
//										NO_STAMP,
//										NO_PRINT_VALIDATE,
//										READ_CODELINE_MICR,
//										SIDE_NONE_IMAGE,
//										SCAN_MODE_256GR200,
//										AUTO_FEED,
//										SORTER_BAY1,
//										WAIT_YES,
//										NO_BEEP,
//										&NrDoc,
//										0, 0);
//
//					if( Reply == LS_OKAY )
//					{
//						// Lettura segnale
//						//Reply = LS100_CalibrationMICR((HWND)hDlg, buffDati, NR_VALORI_LETTI);
//						Reply = LSCalibrationMICR(Connessione, (HWND)hDlg, buffDati, NR_VALORI_LETTI);
//						if( Reply != LS_OKAY)
//						{		
//							Reply = LS_CALIBRATION_FAILED;
//							break;
//						}
//
//						// Estraggo un valore di riferimento
//						vPercento[ii] = CalcoloUnValoreRiferimento( buffDati,MICR_Valore100_3, &vTotValRif[ii], &vMinValue[ii]);
//
//						// Controllo le pre letture se sono OK
//						if( ii == (TRIMMER_PRE_READ - 1) )
//						{
//							// Calcolo la media delle pre-letture
//							MediaPre = 0;
//							for( jj = 0; jj < TRIMMER_PRE_READ; jj++)
//								MediaPre += vPercento[jj];
//							MediaPre /= TRIMMER_PRE_READ;
//
//							// Testo se i valori stanno nel + o - 5% della media pre letture
////							RangeMore = MediaPre * ((float)MICR_TolleranceMore_3 / 100);
////							RangeLess = MediaPre * ((float)MICR_TolleranceLess_3 / 100);
//							RangeMore = MediaPre * ((float)5 / 100);
//							RangeLess = MediaPre * ((float)5 / 100);
//
//							for( jj = 0; jj < TRIMMER_PRE_READ; jj++)
//							{
//								if( (vPercento[jj] < (MediaPre - RangeLess)) ||
//									(vPercento[jj] > (MediaPre + RangeMore)) )
//								{
//									if( Retry < 3 )
//									{
//										ii = -1;	// Cos� ricomincia da inizio pre letture
//										MinValue = TotValRif = 0;
//										Retry ++;
//									}
//									else
//									{
//										Reply = LS_CALIBRATION_FAILED;
//										break;
//									}
//								}
//							}
//
//							// Se le pre-letture sono OK
//							if( ii == (jj - 1) )
//							{
//								// Controllo se la lettura sta nel + o - 2% della tolleranza
//								RangeMore = (float)MICR_SignalValue_3 * ((float)2 / 100);
//								RangeLess = (float)MICR_SignalValue_3 * ((float)2 / 100);
//								if( (MediaPre < (MICR_SignalValue_3 - RangeLess)) ||
//									(MediaPre > (MICR_SignalValue_3 + RangeMore)) )
//								{
//									// Calcolo il nuovo valore del trimmer ...
//									NewTrimmerValue = Calc_Trimmer_Position(MediaPre, TrimmerValue);
//
//									// Testo se sono nel range accettato dal trimmer
//									if( (NewTrimmerValue <= TRIMMER_MIN_VALUE_3) ||
//										(NewTrimmerValue >= TRIMMER_MAX_VALUE_3) )
//									{
//										Reply = LS100_CALIBRATION_FAILED;
//										Percentile = NewTrimmerValue;
//										break;
//									}
//
//									// ... e setto il nuovo valore del trimmer
//									TrimmerValue = NewTrimmerValue;
//									//Reply = LS100_SetTrimmerMICR((HWND)hDlg, NewTrimmerValue);
//									Reply = LSSetTrimmerMICR(Connessione, (HWND)hDlg, 0, NewTrimmerValue);
//
//									if( Reply != LS_OKAY )
//									{
//										Reply = LS_CALIBRATION_FAILED;
//										break;
//									}
//								}
//							}
//						}
//						else if( (ii >= TRIMMER_PRE_READ) && (ii < (TRIMMER_PRE_READ + NrReadGood)) )
//						{
//							// Controllo se la lettura sta nel + o - 7% della tolleranza
//							RangeMore = (float)MICR_SignalValue_3 * ((float)MICR_TolleranceMore_3 / 100);
//							RangeLess = (float)MICR_SignalValue_3 * ((float)MICR_TolleranceLess_3 / 100);
//							if( (vPercento[ii] >= (MICR_SignalValue_3 - RangeLess)) &&
//								(vPercento[ii] <= (MICR_SignalValue_3 + RangeMore)) )
//							{
//								// Salvo valore di riferimento e ...
//								TotValRif += vTotValRif[ii];
//
//								// ... valore picco negativo
//								MinValue +=  vMinValue[ii];
//							}
//							else
//							{
//								// se no controllo se ho gi� fatto 3 tentativi ... o esco !
//								if( Retry < 3 )
//								{
//									ii = -1;	// Cos� ricomincia da inizio pre letture
//									MinValue = TotValRif = 0;
//									Retry ++;
//								}
//								else
//								{
//								
//									Reply = LS_CALIBRATION_FAILED;
//									break;
//								}
//							}
//						}
//					}
//					else
//						break;
//				} // fine for
//
//				// Tolgo il diagnostic mode
//				//LS100_SetDiagnosticMode((HWND)hDlg, FALSE);
//				LSSetDiagnosticMode(Connessione, (HWND)hDlg, FALSE);
//			}
//			else
//			{
//				Reply = LS_CALIBRATION_FAILED;
//			}
//		}
//		else
//		{
//			Reply = LS_CALIBRATION_FAILED;
//		}
//
//		// Calcolo il valore da ritornare, se ho letto tutti i documenti
//		if( ii == (TRIMMER_PRE_READ + NrReadGood) )
//		{
//			TotValRif /= NrReadGood;
//			MinValue /= NrReadGood;
//			MinValue = TotValRif - MinValue;
//			MinValue *= (float)UNITA_VOLT;		// Converto in Volt
//			Percentile = MinValue * 100 / MICR_Valore100_3;
//		}
//	}
//	else
//	{
//		Reply = LS_CALIBRATION_FAILED;
//	}
//
//	ReturnC->ReturnCode = Reply;
//	ReturnC->FunzioneChiamante = "Calibrazione MICR";
//	return Reply;
//} // DoMICRCalibration3
//
//

//
//      Esecuzione lettura documento per calcolo media valori testina MICR
//
int Ls100Class::Periferica::DoMICRCalibration50Barrette( int MICR_SignalValue,long NrReadGood, long TypeSpeed,ListBox^ list)
{
	int ii;
	short	Reply;
	short	fRetryLetture, fRetryCalibration;
	short	NrPicchi, NrPicchiMin, TotPicchi;
	float	RangeMore, RangeLess;
	float	Percento;

	unsigned char	TrimmerValue, NewTrimmerValue;
	unsigned char	buffDati[NR_BYTE_BUFFER_E13B];	// Dati da chiedere
	long    llDati;									// Dati da chiedere
	long	ValMin;
	float	SommaMin, TotSommeMin;

	char	BufCodelineHW[CODE_LINE_LENGTH];
	short	len_codeline;
	
	unsigned long NrDoc;
	float Valore_100_in_Volt_Ls100eth;			// Corrispondenza del 100% in Volt
	double Unita_Volt_Ls100eth;					// Da calcolarsi in base alla tensione di alimentazione

	
	ReturnC->FunzioneChiamante = "DoMICRCalibration50Barrette";
	// Setto i valori iniziali
	fRetryLetture = fRetryCalibration = 0;
	nrLettura = 0;
	Percentile = Percento = 0;
	TotPicchi = 0;
	TotSommeMin = 0;
	TrimmerValue = 0;
	NrReadGood = 3;
	MICR_TolleranceLess_3 = 4;
	MICR_TolleranceMore_3 = 4;
	MICR_SignalValue_3 =  MICR_SignalValue;

	// Calcolo tensione di riferimento ed unita' di Volt !!!
	Valore_100_in_Volt_Ls100eth = (float)(TENSIONE_VCC_LS100ETH * MOLTIPL_RIFERIMENTO_VOLT_LS100ETH);
	Unita_Volt_Ls100eth = TENSIONE_VCC_LS100ETH / NR_VALORI_CONVERTITORE_LS100ETH;

	//Reply = LS100_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	short Connessione;
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	if ((Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN))
	{
		// Setto il diagnostic mode
		//Reply = LS100_SetDiagnosticMode((HWND)hDlg, TypeSpeed);
		Reply = LSSetDiagnosticMode(Connessione, (HWND)hDlg, TypeSpeed);


		if( Reply == LS_OKAY )
		{
			//----------- Set attesa introduzione documento -----------
			Reply = LSDisableWaitDocument(Connessione, (HWND)hDlg, FALSE);

			//----------- Disable Block Document if double leafing -----------
			//LS100_DoubleLeafingSensibility((HWND)hDlg, DOUBLE_LEAFING_DISABLE);
			LSDoubleLeafingSensibility(Connessione, (HWND)hDlg, 0, DOUBLE_LEAFING_DISABLE);

			// Setto il valore del trimmer a meta' (16)
//			TrimmerValue = 16;
//			Reply = LS100_SetTrimmerMICR(hDlg, TrimmerValue);

			if( Reply == LS_OKAY)
			{
				// Eseguo il primo loop di misurazione di 3 letture
				for( ii = 0; ii < (TRIMMER_PRE_READ + NrReadGood); ii++ )
				{
					nrLettura ++;

					// Chiamo la funzione per via documento
					/*Reply = LS100_DocHandle((HWND)hDlg,
											SUSPENSIVE_MODE,
											NO_FRONT_STAMP,
											NO_PRINT_VALIDATE,
											READ_CODELINE_MICR,
											SIDE_NONE_IMAGE,
											SCAN_MODE_256GR200,
											AUTO_FEED,
											SORTER_BAY1,
											WAIT_YES,
											NO_BEEP,
											&NrDoc,
											0,
											0);*/
					Reply = LSDocHandle(Connessione, (HWND)hDlg,
										NO_STAMP,
										NO_PRINT_VALIDATE,
										READ_CODELINE_MICR,
										SIDE_NONE_IMAGE,
										SCAN_MODE_256GR200,
										AUTO_FEED,
										SORTER_BAY1,
										WAIT_YES,
										NO_BEEP,
										&NrDoc,
										0, 0);

					if( Reply == LS_OKAY )
					{
						// Dopo il primo via leggo il valore del trimmer
						if( !TrimmerValue )
						{
							//Reply = LS100_CalibrationMICR((HWND)hDlg, buffDati, NR_VALORI_LETTI);
							Reply = LSReadTrimmerMICR(Connessione, (HWND)hDlg, 0, buffDati, NR_VALORI_LETTI);

							if( Reply == LS_OKAY)
							{
								// Salvo il valore del trimmer
								TrimmerValue = buffDati[0];
							}
						}

						// Lettura segnale
						llDati = NR_BYTE_BUFFER_E13B;		// 16 Kb.
						//Reply = LS100_ReadE13BSignal((HWND)hDlg, buffDati, (long *)&llDati);
						Reply = LSReadE13BSignal(Connessione, (HWND)hDlg, buffDati, &llDati);
						if (Reply != LS_OKAY)
						{
							//CheckReply(hDlg, Reply, "LS100_ReadE13BSignal");
							//LS100_Close(hDlg, SUSPENSIVE_MODE);		// Faccio la close
							return LS_CALIBRATION_FAILED;
						}
						

						// Calcolo la somma dei valori minimi ...
						Media50Barrette(buffDati, llDati, &NrPicchi, &SommaMin, &ValMin, &NrPicchiMin);

						// Controllo se ho trovato tutti i picchi
						if( NrPicchi == NR_PICCHI )
						{
							TotSommeMin += SommaMin;

							// elimino i picchi a zero !!!
							if( ValMin == 0 )
								NrPicchi -= NrPicchiMin;
							TotPicchi += NrPicchi;

							// Calcolo il valore da visualizzare in bitmap
							// SOLO se NrPicchi diverso da zero
							if( NrPicchi )
							{
								SommaMin /= NrPicchi;
								SommaMin = ZERO_TEORICO - SommaMin;
								SommaMin *= (float)Unita_Volt_Ls100eth;			// Converto in Volt
								Percento = SommaMin * 100 / Valore_100_in_Volt_Ls100eth;
							}

							//// Disegna segnale e visualizza la bitmap
							//ShowMICRSignal(hInst, hDlg, buffDati, llDati, Percento, TrimmerValue);
							int conta ;
							list->Items->Add("Valore Letto: " + Percento + " " + "Trimmer Value: "+ TrimmerValue );
							
							conta = list->Items->Count;
							list->SelectedIndex = conta-1;
							list->Refresh();
							
							
							if( ii == (TRIMMER_PRE_READ - 1) )
							{
								// Calcolo il valore per regolare il trimmer
								// SOLO se TotPicchi diverso da zero ...
								if( TotPicchi )
								{
									TotSommeMin /= TotPicchi;
									TotSommeMin = ZERO_TEORICO - TotSommeMin;
									TotSommeMin *= (float)Unita_Volt_Ls100eth;	// Converto in Volt
									Percento = TotSommeMin * 100 / Valore_100_in_Volt_Ls100eth;

									// Calcolo il nuovo valore del trimmer ...
									NewTrimmerValue = Calc_Trimmer_Position(Percento, TrimmerValue);
								}
								else
								{
									// Il segnale � in SATURAZIONE !!!
									// Decremento il valore del trimmer di 11
									NewTrimmerValue = TrimmerValue - 11;

									ii = -1;			// Cos� ricomincia da inizio pre letture
									TotSommeMin = 0;	// Riazzero il totale delle somme
									TotPicchi = 0;		// Riazzero il totale picchi
								}

								// Testo se sono nel range accettato dal trimmer
								if( NewTrimmerValue <= TRIMMER_MIN_VALUE )
								{
									NewTrimmerValue = TRIMMER_MIN_VALUE;
									ii = -1;			// Cos� ricomincia da inizio pre letture
								}

								if( NewTrimmerValue > TRIMMER_MAX_VALUE_3 )
								{
									NewTrimmerValue = TRIMMER_MAX_VALUE_3;
									ii = -1;			// Cos� ricomincia da inizio pre letture
								}

//								if( (NewTrimmerValue <= TRIMMER_MIN_VALUE) ||
//									(NewTrimmerValue >= TRIMMER_MAX_VALUE) )
//								{
//									Reply = LS100_CALIBRATION_FAILED;
//									Percentile = NewTrimmerValue;
//									break;
//								}


								// ... e setto il nuovo valore del trimmer
								TrimmerValue = NewTrimmerValue;
								//Reply = LS100_SetTrimmerMICR((HWND)hDlg, NewTrimmerValue);//, TypeSpeed);
								Reply = LSSetTrimmerMICR(Connessione, (HWND)hDlg, 0, NewTrimmerValue);//, TypeSpeed);

								if( Reply != LS_OKAY )
								{
									Reply = LS_CALIBRATION_FAILED;
								///CheckReply(hDlg, Reply, "LS100_SetTrimmerMICR");
									break;
								}

								// Riazzero il totale delle somme e dei picchi
								TotSommeMin = 0;
								TotPicchi = 0;
							}
							else if( ii == ((TRIMMER_PRE_READ + NrReadGood) - 1) )
							{
								// Calcolo il valore delle POST LETTURE
								TotSommeMin /= TotPicchi;
								TotSommeMin = ZERO_TEORICO - TotSommeMin;
								TotSommeMin *= (float)Unita_Volt_Ls100eth;	// Converto in Volt
								Percento = TotSommeMin * 100 / Valore_100_in_Volt_Ls100eth;

								// Controllo se la lettura sta nel + o - 4% della tolleranza
								RangeMore = (float)MICR_SignalValue_3 * ((float)MICR_TolleranceMore_3 / 100);
								RangeLess = (float)MICR_SignalValue_3 * ((float)MICR_TolleranceLess_3 / 100);
								if( (Percento >= (MICR_SignalValue_3 - RangeLess)) &&
									(Percento <= (MICR_SignalValue_3 + RangeMore)) )
								{
									// Salvo il valore da ritornare, se ho letto tutti i documenti
									Percentile = Percento;
								}
								else
								{
									// se no controllo se ho gi� fatto 3 tentativi ... o esco !
									if( fRetryCalibration < 3 )
									{
										ii = -1;			// Cos� ricomincia da inizio pre letture
										TotSommeMin = 0;	// Riazzero il totale delle somme
										TotPicchi = 0;		// Riazzero il totale picchi
										fRetryCalibration ++;
									}
									else
									{
										Reply = LS_CALIBRATION_FAILED;
										break;
									}
								}
							}
						} //if( NrPicchi == NR_PICCHI )
						else
						{
							//// Disegna segnale e visualizza la bitmap
							//ShowMICRSignal(hInst, hDlg, buffDati, llDati, 0, TrimmerValue);
													
							int conta ;
							list->Items->Add("Valore Letto: " + Percento + " " + "Trimmer Value: "+ TrimmerValue );
							
							conta = list->Items->Count;
							list->SelectedIndex = conta-1;
							list->Refresh();
							// Decremento ii per ripetere l'acquisizione !!!
							ii --;

							fRetryLetture ++;
							if( fRetryLetture == MAX_READ_RETRY )
							{
								//MessageBox(hDlg, "Document loose !!!\n\nChange Document !", TITLE_ERROR, MB_OK | MB_ICONERROR);
								Reply = LS_CALIBRATION_FAILED;
								break;
							}
						}

						// Leggo la codeline per cancellare il doppio buffer !
						len_codeline = CODE_LINE_LENGTH;
						/*Reply = LS100_ReadCodeline((HWND)hDlg,
												   SUSPENSIVE_MODE, 
												   BufCodelineHW,
												   &len_codeline,
												   NULL,
												   0);*/
						Reply = LSReadCodeline(Connessione, (HWND)hDlg,
											   BufCodelineHW,
											   &len_codeline,
											   NULL, NULL,
											   NULL, NULL);

					}
					else
					{
						//CheckReply(hDlg, Reply, "LS100_DocHandle");
						Reply = LS_CALIBRATION_FAILED;
						break;
					}
				} // fine for
			}
			else
			{
				//CheckReply(hDlg, Reply, "LS100_CalibrationMICR");
				Reply = LS_CALIBRATION_FAILED;
			}

			// Tolgo il diagnostic mode
			//LS100_SetDiagnosticMode((HWND)hDlg, FALSE);
			LSSetDiagnosticMode(Connessione, (HWND)hDlg, FALSE);
		}
		else
		{
			//CheckReply(hDlg, Reply, "LS100_SetDiagnosticMode");
			Reply = LS_CALIBRATION_FAILED;
		}
	}
	
	//LS100_Close((HWND)hDlg, SUSPENSIVE_MODE);
	LSDisconnect(Connessione, (HWND)hDlg);
	

	return Reply;
} // DoMICRCalibration50Barrette

//
// Calcolo il valore di riferimento su una lettura e ritorno il valore di percentuale 100
//
float Ls100Class::Periferica::CalcoloUnValoreRiferimento(unsigned char *pDati, 
								float MICR_Valore100, float *TotValRif, float *MinValue)
{
	int jj;
	float	SommaPerMedia;
	float	ValMinore;
	float	Percentile;


	// Calcolo valore medio di riferimento e ...
	SommaPerMedia = 0;
	for( jj = OFFSET_INZIO_CALCOLO; jj < (OFFSET_INZIO_CALCOLO + NR_VALORI_PER_MEDIA); jj ++)
	{
		SommaPerMedia += *(pDati + jj);
	}
	*TotValRif = (SommaPerMedia / NR_VALORI_PER_MEDIA);

	// ... salvo secondo picco negativo
	ValMinore = *(pDati + OFFSET_PRIMO_PICCO);
	for( jj = OFFSET_PRIMO_PICCO; jj < NR_VALORI_LETTI; jj ++)
		if( ValMinore > *(pDati + jj) )
			ValMinore = *(pDati + jj);
	*MinValue = ValMinore;

	// Calcolo il valore da visualizzare in bitmap
	SommaPerMedia /= NR_VALORI_PER_MEDIA;
	ValMinore = SommaPerMedia - ValMinore;
	ValMinore *= (float)UNITA_VOLT;		// Converto in Volt
	Percentile = ValMinore * 100 / MICR_Valore100;

	// Disegna segnale e visualizza la bitmap
//	ShowMICRSignal(hInst, hDlg, pDati, llDati, Percentile);

	return Percentile;
} // CalcoloUnValoreRiferimento


// ******************************************************************* //
// Calcolo della posizione del trimmer in base alla variazione         //
// percentuale del segnale della media rispetto al valore del segnale  //
// magnetico indicato sul documento di test                            //
// ******************************************************************* //
unsigned char Ls100Class::Periferica::Calc_Trimmer_Position(float ValoreLetto, unsigned char PosTrimmer)
{
    #define R_SERIE_TRIMMER_3		39000	// Valore di restistenza presente
											// in serie al trimmer digitale
	 #define R_SERIE_TRIMMER_ETH		18000

    #define R_X_STEP_TRIMMER_3		1562	// Valore resistivo corrispondente a ogni
											// step del trimmer digitale per LS100/3
    #define R_SERIE_TRIMMER_4		47000	// Valore di restistenza presente
											// in serie al trimmer digitale
    #define R_X_STEP_TRIMMER_4		390		// Valore resistivo corrispondente a ogni
											// step del trimmer digitale per LS100/4
    float Var_Per_Sign;
    float tmp;
    unsigned long  R_trimmer;
	short R_x_step_trimmer;
	long  R_serie_trimmer;


	// Di default setto il valore per LS100/3 e LS100/5
	if( TypeLS == TYPE_LS100_ETH || TypeLS == TYPE_LS100_7 || TypeLS == TYPE_LS100_8)
	{
		R_serie_trimmer = R_SERIE_TRIMMER_ETH;
		R_x_step_trimmer = R_X_STEP_TRIMMER_3;
	}
	else if( TypeLS == TYPE_LS100_4 )
	{
		R_serie_trimmer = R_SERIE_TRIMMER_4;
		R_x_step_trimmer = R_X_STEP_TRIMMER_4;
	}
	else
	{
		R_serie_trimmer = R_SERIE_TRIMMER_3;
		R_x_step_trimmer = R_X_STEP_TRIMMER_3;
	}


	

	//Calcolo la variazione percentuale del segnale della media rispetto
	//al valore del segnale magnetico indicato sul documento di test in
	//valore assoluto
	
	tmp = (float)Math::Abs((int)ValoreLetto - MICR_SignalValue_3);
	Var_Per_Sign = tmp * 100 / ValoreLetto;

	//Calcolo il valore resistivo fornito dal trimmer nella sua posizione
	//attulale e gli sommo la resistenza presente in serie
	R_trimmer = (PosTrimmer * R_x_step_trimmer);
//	R_trimmer &= 0xFFFF;
	R_trimmer += R_serie_trimmer;

	//Determino se bisogna incrementare o decrementare l'ampiezza del segnale
	if( ValoreLetto > MICR_SignalValue_3 )
		tmp = (100 - Var_Per_Sign);
	else
		tmp = (Var_Per_Sign + 100);


	//Calcolo la variazione in percentuale da effettuare alla resistenza
	//complessiva presente sulla reazione negativa e di conseguenza
	//la nuova posizione del trimmer
//	R_trimmer = (R_trimmer * (unsigned long)tmp) / 100;
	R_trimmer = (R_trimmer / 100) * (unsigned long)tmp;
	PosTrimmer = (unsigned char)((R_trimmer - R_serie_trimmer) / R_x_step_trimmer);

	return PosTrimmer;
} // Calc_Trimmer_Position




//*******************************************************************
// FUNCTION  : Media50Barrette(unsigned char *pd, char *pb, short NrChar)
//
// PARAMETER :	pd		(I) Ptr buffer dati.
//
// PURPOSE   : Calcola la somma dei valori minimi.
//*******************************************************************
BOOL Ls100Class::Periferica::Media50Barrette(unsigned char *pd, long llDati,
						 short *NrPicchi, float *SommaValori, long *ValMin, short *NrPicchiMin)
{
	BOOL ret;
	short ii, Save_ii;
	short NrCampioni;
	short NrChar;
	unsigned char *pdct;

//	FILE *fh;


	// Conto i bytes larghezza char
	NrChar = 0;
	while( (*(pd + NrChar) != E13B_FINE_LLC) && (NrChar < llDati) )
		NrChar ++;

	if( NrChar != llDati )
	{
		NrCampioni = *(short *)(pd + NrChar + 1);

		pdct = (unsigned char *)(pd + NrChar + 1 + 2);		// Vado ad inizio dati segnale
		pdct = (unsigned char *)(((unsigned char *)pdct) + (DIM_DESCR_CHAR * NrChar));	// Vado ad inizio dati segnale

		// Tolgo i descrittori dal numero bytes
		NrCampioni -= (DIM_DESCR_CHAR * NrChar);


		// Cerco i picchi e calcolo il valore medio
		*NrPicchi = *NrPicchiMin = 0;
		*ValMin = 0xff;		// setto il massimo
		*SommaValori = 0;
		Save_ii = 0;
		// Elimino una parte iniziale e finale
		for( ii = 20; ii <= (NrCampioni - 20); ii ++)
		{
			// Controllo se � un picco,
			// per essere un picco deve avere 7 valori prima e dopo di valore inferiore
			if( (pdct[ii] <= VALORE_MAX_PICCO_MIN) &&
//			if( (pdct[ii]) && (pdct[ii] <= VALORE_MAX_PICCO_MIN) &&
				((pdct[ii] <= pdct[ii-1]) && (pdct[ii] < pdct[ii+1])) &&
				((pdct[ii] <= pdct[ii-2]) && (pdct[ii] < pdct[ii+2])) &&
				((pdct[ii] <= pdct[ii-3]) && (pdct[ii] < pdct[ii+3])) &&
				((pdct[ii] <= pdct[ii-4]) && (pdct[ii] < pdct[ii+4])) &&
				((pdct[ii] <= pdct[ii-5]) && (pdct[ii] < pdct[ii+5])) &&
				((pdct[ii] <= pdct[ii-6]) && (pdct[ii] < pdct[ii+6])) &&
				((pdct[ii] <= pdct[ii-7]) && (pdct[ii] < pdct[ii+7])) )
			{
				// Controllo se il picco � ad una distanza minima
				if( (ii - Save_ii) >= PICCO_DISTANZA_MINIMA )
				{
					// Media del picco
					*SommaValori += pdct[ii];

//					if( fh = fopen("Trace_Calibrazione_MICR.txt", "at") )
//					{
//						fprintf(fh, "%d ", pdct[ii]);
//						fclose( fh );
//					}

					// Salvo il valore minimo
					if( *ValMin == pdct[ii] )
						*NrPicchiMin = *NrPicchiMin + 1;

					if( *ValMin > pdct[ii] )
					{
						*ValMin = pdct[ii];
						*NrPicchiMin = 1;
					}

					*NrPicchi = *NrPicchi + 1;
					Save_ii = ii;
				}
			}
		}

		// Calcolo media
//		*llMedia = *llMedia / *NrPicchi;

		ret = TRUE;
	}
	else
	{
		// C'� un baco che qualche volta non ritorna i dati corretti,
		// setto tutto a zero e butto l'acquisizione !!!
		*NrPicchi = *NrPicchiMin = 0;
		*ValMin = 0;
		*SommaValori = 0;
		ret = FALSE;
	}


//	if( fh = fopen("Trace_Calibrazione_MICR.txt", "at") )
//	{
//		fprintf(fh, "\n ");
//		fclose( fh );
//	}

	return ret;
}  // Media50Barrette





//int Ls100Class::Periferica::CalibraScanner(int Side)
//{
//	ReturnC->FunzioneChiamante = "CalibraScanner";
//	Reply = LS100_Open((HWND)hDlg,(HANDLE)hInst,SUSPENSIVE_MODE);
//	if ((Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN))
//	{
//		Reply = LS100_ScannerCalibration((HWND)hDlg, Side);
//		switch (Reply)
//		{
//			case LS_ERROR_OFFSET_FRONT:
//			case LS_ERROR_GAIN_FRONT:
//			case LS_ERROR_COEFF_FRONT:
//				ReturnC->FunzioneChiamante= "Calibrazione Scanner Fronte ";
//				break;
//			case LS_ERROR_OFFSET_REAR:
//			case LS_ERROR_GAIN_REAR:
//			case LS_ERROR_COEFF_REAR:
//				ReturnC->FunzioneChiamante= "Calibrazione Scanner Retro ";
//				break;
//		};
//		LS100_Close((HWND)hDlg,SUSPENSIVE_MODE);
//	}
//	else
//	{
//		ReturnC->FunzioneChiamante= "LS100_Open";
//	}
//	
//	ReturnC->ReturnCode = Reply;
//
//	return Reply;
//}

int Ls100Class::Periferica::CalibraScanner(int ScannerType)
{
	short Connessione;
	ReturnC->FunzioneChiamante = "Calibrazione Scanner";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	
	if( Reply == LS_OKAY )
	{
		Reply = LSScannerCalibration(Connessione,(HWND)hDlg, ScannerType);
		LSDisconnect(Connessione,(HWND)hDlg);
	}

	ReturnC->ReturnCode = Reply;

	return Reply;
}


int Ls100Class::Periferica::LeggiImmagini(char Side)
{
	unsigned long NrDoc;
	HANDLE HBackGray = 0;
	HANDLE HFrontGray = 0;
	char *StrNome=NULL;
	short floop = false;
	short Connessione ; 

	ReturnC->FunzioneChiamante = "LeggiImmagini";
	//Reply = LS100_Open((HWND)hDlg,(HANDLE)hInst,SUSPENSIVE_MODE);
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	if ((Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN))
	{
		// Chiamo la funzione di via documento
		do
		{
			/*Reply = LS100_DocHandle((HWND)hDlg,
								SUSPENSIVE_MODE,
								NO_FRONT_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								Side,
								SCAN_MODE_256GR200,
								AUTO_FEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								&NrDoc,
								0,
								0);*/
			Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								//READ_CODELINE_MICR,
								NO_READ_CODELINE,
								Side,
								SCAN_MODE_256GR200,
								AUTO_FEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								&NrDoc,
								SCAN_PAPER_DOCUMENT,
								0);

			//if (Reply == LS100_FEEDEREMPTY)
			if (Reply == LS_FEEDER_EMPTY)
			{

				floop = TRUE;
				//LS100_Close((HWND)hDlg,SUSPENSIVE_MODE);
				//LS100_Open((HWND)hDlg,(HANDLE)hInst,SUSPENSIVE_MODE);
			}
			else
				floop = FALSE;
		}while(floop);

		if( Reply == LS_OKAY )
		{
			// Leggo l'immagine filmata
			/*Reply = LS100_ReadImage((HWND)hDlg,
									SUSPENSIVE_MODE,
									CLEAR_ALL_BLACK,
									Side,
									NrDoc,
									(LPHANDLE)&HFrontGray,
									(LPHANDLE)&HBackGray,
									NULL,
									NULL);*/
			Reply = LSReadImage(Connessione, (HWND)hDlg,
										CLEAR_ALL_BLACK,
										Side,
										0, NrDoc,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);
			if(Reply == LS_OKAY)
			{
				// salvo le immagini
				if (Side == SIDE_FRONT_IMAGE)
				{
					//NomeFileImmagine = "..\\Dati\\FrontImage.bmp";
					
					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
					//Reply = LS100_SaveDIB((HWND)hDlg, SUSPENSIVE_MODE, HFrontGray,StrNome);
					Reply = LSSaveDIB((HWND)hDlg,HFrontGray,StrNome);

				}
				else if (Side == SIDE_BACK_IMAGE)
				{
					//NomeFileImmagine = "..\\Dati\\BackImage.bmp";	
					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();
					//Reply = LS100_SaveDIB((HWND)hDlg, SUSPENSIVE_MODE, HBackGray,StrNome);
					Reply = LSSaveDIB((HWND)hDlg,HBackGray,StrNome);
				}	
			}
			//LS100_Close((HWND)hDlg,SUSPENSIVE_MODE);
			LSDisconnect(Connessione,(HWND)hDlg);	

		}
	}
	// libero le immagini
	if (HFrontGray)
		//LS100_FreeImage((HWND)hDlg, &HFrontGray);
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		//LS100_FreeImage((HWND)hDlg, &HBackGray);
		LSFreeImage((HWND)hDlg, &HBackGray);

	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Lettura Immagini";
	return Reply;
}

int Ls100Class::Periferica::LeggiCodeline(short DocHandling)
{
	char	BufCodeLine[CODE_LINE_LENGTH];
	short	llBufCodeLine = 256;
	//unsigned char SByte[16];
	//unsigned char SKey;
	unsigned long NrDoc;
	//short ret;
	short Connessione;


	ReturnC->FunzioneChiamante = "LeggiCodeline";
	//-----------Open--------------------------------------------
	//Reply = LS100_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
		//Reply = LS100_DoubleLeafingSensibility((HWND)hDlg,DOUBLE_LEAFING_DEFAULT);
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_WARNING,50,150,215);

		/*Reply = LS100_DocHandle((HWND)hDlg,
							SUSPENSIVE_MODE,
							NO_FRONT_STAMP,
							NO_PRINT_VALIDATE,
							READ_CODELINE_MICR,
							SIDE_NONE_IMAGE,
							SCAN_MODE_256GR200,
							AUTO_FEED,
							SORTER_BAY1,
							WAIT_YES,
							NO_BEEP,
							&NrDoc,
							0,
							0);*/
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								READ_CODELINE_MICR,
								SIDE_NONE_IMAGE,
								SCAN_MODE_256GR200,
								AUTO_FEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								&NrDoc,
								SCAN_PAPER_DOCUMENT,
								0);

		if (Reply == LS_OKAY)
		{
			//-----------ReadCodeLine--------------------------------------------
			/*Reply = LS100_ReadCodeline((HWND)hDlg,
										'S',
										BufCodeLine,
										&llBufCodeLine,
										NULL,
										&ret);*/
			Reply = LSReadCodeline(Connessione, (HWND)hDlg,
										   BufCodeLine,
										   &llBufCodeLine,
										   NULL, 0,
										   NULL, 0);
			
			if (Reply == LS_OKAY)
			{
				// setto la codeline di ritorno
				CodelineLetta = Marshal::PtrToStringAnsi((IntPtr) (char *)BufCodeLine);
			}
			else
			{
				ReturnC->FunzioneChiamante =  "LSReadCodeline";
	
			}
			//BufCodeLine[llBufCodeLine] = '\0';
		}
		else
		{
			ReturnC->FunzioneChiamante =  "LSDocHandle";
		}			
	}
	else
	{
		ReturnC->FunzioneChiamante =  "LSOpen";
	}
	//LS100_DoubleLeafingSensibility((HWND)hDlg,DOUBLE_LEAFING_DISABLE);
	LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,150,215);
	//-----------Close-------------------------------------------
	//LS100_Close((HWND)hDlg, SUSPENSIVE_MODE);	
	LSDisconnect(Connessione,(HWND)hDlg);	

	ReturnC->ReturnCode = Reply;
	return Reply;
}



int Ls100Class::Periferica::LeggiCodelineImmagini(short DocHandling,short Side,short Doppia)
{
	char	szCodeline[CODE_LINE_LENGTH];
	short	lenCodeline = 256;
	HANDLE HBackGray;
	HANDLE HFrontGray;
	char *StrNome=NULL;
	short Connessione ; 

//	unsigned char SByte[16];
	//unsigned char SKey;
	unsigned long NrDoc;
	short ret;


	
	//-----------Open--------------------------------------------
	//Reply = LS100_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
		//Reply = LS100_DoubleLeafingSensibility((HWND)hDlg, (unsigned char)Doppia);
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_WARNING,50,150,215);
		// do via documento
		/*Reply = LS100_DocHandle((HWND)hDlg,
							SUSPENSIVE_MODE,
							NO_FRONT_STAMP,
							NO_PRINT_VALIDATE,
							READ_CODELINE_MICR,
							(char)Side,
							SCAN_MODE_256GR200,
							AUTO_FEED,
							SORTER_BAY1,
							WAIT_YES,
							NO_BEEP,
							&NrDoc,
							0,
							0);*/
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								READ_CODELINE_MICR,
								SIDE_ALL_IMAGE,
								SCAN_MODE_256GR200,
								AUTO_FEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								&NrDoc,
								SCAN_PAPER_DOCUMENT,
								0);
		if (Reply == LS_OKAY)
		{
			//-----------ReadCodeLine--------------------------------------------
			/*Reply = LS100_ReadCodeline((HWND)hDlg,
										'S',
										BufCodeLine,
										&llBufCodeLine,
										NULL,
										&ret);*/
			Reply = LSReadCodeline(Connessione, (HWND)hDlg, szCodeline, &lenCodeline, NULL, NULL, NULL, NULL);
			
			if (Reply == LS_OKAY)
			{
				// setto la codeline di ritorno
				CodelineLetta = Marshal::PtrToStringAnsi((IntPtr) (char *)szCodeline);
				
				
				// Leggo l'immagine filmata
				/*Reply = LS100_ReadImage((HWND)hDlg,
										SUSPENSIVE_MODE,
										CLEAR_ALL_BLACK,
										(char)Side,
										NrDoc,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);*/
				Reply = LSReadImage(Connessione, (HWND)hDlg,
										CLEAR_ALL_BLACK,
										SIDE_ALL_IMAGE,
										0, NrDoc,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);
				if (Reply == LS_OKAY)
				{
					if(Side == SIDE_ALL_IMAGE || Side == SIDE_FRONT_IMAGE)
					{
						
						StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
						//LS100_SaveDIB((HWND)hDlg, SUSPENSIVE_MODE, HFrontGray,StrNome);
						Reply = LSSaveDIB((HWND)hDlg,HFrontGray,StrNome);
					}
						// libero le immagini
					//if (HFrontGray)
					{
						//LS100_FreeImage((HWND)hDlg, &HFrontGray);
						LSFreeImage((HWND)hDlg, &HFrontGray);
						HFrontGray = 0;
					}
				}
				if (Reply == LS_OKAY)
				{
					if(Side == SIDE_ALL_IMAGE || Side == SIDE_BACK_IMAGE)
					{
						//NomeFileImmagine = "..\\Dati\\BackImage.bmp";	
						
						StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro).ToPointer();
						//LS100_SaveDIB((HWND)hDlg, SUSPENSIVE_MODE, HBackGray,StrNome);
						Reply = LSSaveDIB((HWND)hDlg,HBackGray,StrNome);
						//if (HBackGray)
						{
							//LS100_FreeImage((HWND)hDlg, &HBackGray);
							LSFreeImage((HWND)hDlg, &HBackGray);
							HBackGray =0;
										
						}				
					}		
					else
					{
						ReturnC->FunzioneChiamante =  "LS100_SaveimageFronte";
					}
				}
				else
				{
					ReturnC->FunzioneChiamante =  "LS100_ReadImage";
				}

			}
			else
			{
				ReturnC->FunzioneChiamante =  "LS100_ReadCodeline";
	//			Reply = LS5xx_Reset((HWND)hDlg, SUSPENSIVE_MODE, RESET_PERIPHERAL_FREE_PATH);
			}
			//BufCodeLine[llBufCodeLine] = '\0';
		}
		else
		{

			ReturnC->FunzioneChiamante =  "LS100_DocHandle";
	//			Reply = LS5xx_Reset((HWND)hDlg, SUSPENSIVE_MODE, RESET_PERIPHERAL_FREE_PATH);
		}			
	}
	else
	{
		ReturnC->FunzioneChiamante =  "LS100_Open";
	}


	
	//ret = LS100_DoubleLeafingSensibility((HWND)hDlg,DOUBLE_LEAFING_DISABLE);
	ret  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,150,215);	
	//-----------Close-------------------------------------------
	//LS100_Close((HWND)hDlg, SUSPENSIVE_MODE);	
	LSDisconnect(Connessione,(HWND)hDlg);
	
	ReturnC->ReturnCode = Reply;
	return Reply;
}

int Ls100Class::Periferica::Timbra(short DocHandling)
{
	
	DateTime iniTime,currTime;
	TimeSpan t;
	unsigned long NrDoc;
	short Connessione;

	ReturnC->FunzioneChiamante = "Timbra";
	//-----------Open--------------------------------------------
	//Reply = LS100_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
		// Do il via al documento
		/*Reply = LS100_DocHandle((HWND)hDlg,
							SUSPENSIVE_MODE,
							FRONT_STAMP,
							NO_PRINT_VALIDATE,
							READ_CODELINE_MICR,
							SIDE_NONE_IMAGE,
							SCAN_MODE_256GR200,
							AUTO_FEED,
							SORTER_BAY1,
							WAIT_YES,
							NO_BEEP,
							&NrDoc,
							0,
							0);*/
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
								FRONT_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								SIDE_ALL_IMAGE,
								SCAN_MODE_256GR200,
								AUTO_FEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								&NrDoc,
								SCAN_PAPER_DOCUMENT,
								0);
			
		if (Reply == LS_OKAY)
		{
		
		}
		else
		{
			ReturnC->FunzioneChiamante =  "LSDocHandle";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}
	ReturnC->ReturnCode = Reply;
	//LS100_Close((HWND)hDlg, SUSPENSIVE_MODE);
	LSDisconnect(Connessione,(HWND)hDlg);
	
	return Reply;
}

int Ls100Class::Periferica::TestStampa(short Side)
{
	String  ^ strstp;
	char *str;
//	int ii;
	DateTime iniTime,currTime;
	TimeSpan t;
	HANDLE HBackGray;
	HANDLE HFrontGray;
	char *StrNome=NULL;
	unsigned long NrDoc;
	short Connessione ;
	
	ReturnC->FunzioneChiamante = "TestStampa";


	strstp = "TEST INKJET 888888888888 HHHHHHHHHHHHHHHH 0000000000000000";
	//Reply = LS100_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
	

		str = (char*) Marshal::StringToHGlobalAnsi(strstp ).ToPointer();	
		
		//Reply = LS100_LoadString((HWND)hDlg,
		//							SUSPENSIVE_MODE,
		//							FORMAT_BOLD,// TOLTO IL 25-11-2009    FORMAT_NORMAL,//: ) FORMAT_BOLD 
		//							strstp->Length,
		//							str);
		Reply = LSLoadString(Connessione,(HWND)hDlg,
								 FORMAT_BOLD ,//FORMAT_NORMAL,//: ) FORMAT_BOLD 
								 strstp->Length,
								 str);	
		
		if (Reply == LS_OKAY)
		{
			// via doc
			/*Reply = LS100_DocHandle((HWND)hDlg,
						SUSPENSIVE_MODE,
						NO_FRONT_STAMP,
						PRINT_VALIDATE,
						NO_READ_CODELINE,
						(char)Side,
						SCAN_MODE_256GR200,
						AUTO_FEED,
						SORTER_BAY1,
						WAIT_YES,
						NO_BEEP,
						&NrDoc,
						0,
						0);*/
			Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								PRINT_VALIDATE,
								NO_READ_CODELINE,
								SIDE_ALL_IMAGE,
								SCAN_MODE_256GR200,
								AUTO_FEED,
								SORTER_BAY1,
								WAIT_YES,
								BEEP,
								&NrDoc,
								SCAN_PAPER_DOCUMENT,
								0);

			if (Reply == LS_OKAY)
			{
				/*Reply = LS100_ReadImage((HWND)hDlg,
										SUSPENSIVE_MODE,
										CLEAR_ALL_BLACK,
										(char)Side,
										NrDoc,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);
										NULL);*/
				Reply = LSReadImage(Connessione, (HWND)hDlg,
										CLEAR_ALL_BLACK,
										SIDE_ALL_IMAGE,
										0, NrDoc,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);

				if (Reply == LS_OKAY)
				{
					if(Side == SIDE_ALL_IMAGE || Side == SIDE_FRONT_IMAGE)
					{
						StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
						//Reply = LS100_SaveDIB((HWND)hDlg, SUSPENSIVE_MODE, HFrontGray,StrNome);
						Reply = LSSaveDIB((HWND)hDlg,HFrontGray,StrNome);
					}
					if (Reply == LS_OKAY)
					{
						if(Side == SIDE_ALL_IMAGE || Side == SIDE_BACK_IMAGE)
						{
							StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();
							//Reply = LS100_SaveDIB((HWND)hDlg, SUSPENSIVE_MODE, HBackGray,StrNome);
							Reply = LSSaveDIB((HWND)hDlg,HBackGray,StrNome);
						}
					}	
					else
					{
						ReturnC->FunzioneChiamante =  "LSSaveimageFronte";
					}
				}
				else
				{
						ReturnC->FunzioneChiamante =  "LSReadImage";
				}
			}
			else
			{
				ReturnC->FunzioneChiamante = "LSDocHandle";
			}
		}
		else
		{
			ReturnC->FunzioneChiamante = "LSLoadString";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}
	ReturnC->ReturnCode = Reply;

	// libero le immagini
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);


	//LS100_Close((HWND)hDlg, SUSPENSIVE_MODE);
	LSDisconnect(Connessione,(HWND)hDlg);

	return Reply;
}


int Ls100Class::Periferica::TestStampaAlto(short Side)
{
	String  ^ strstp;
	char *str;
//	int ii;
	DateTime iniTime,currTime;
	TimeSpan t;
	HANDLE HBackGray;
	HANDLE HFrontGray;
	char *StrNome=NULL;
	unsigned long NrDoc;
	short Connessione ; 
	
	ReturnC->FunzioneChiamante = "TestStampaAlto";

	strstp = "TEST INKJET 888888888888 HHHHHHHHHHHHHHHH 0000000000000000";
	//Reply = LS100_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
		

		str = (char*) Marshal::StringToHGlobalAnsi(strstp ).ToPointer();	
		
		//Reply = LS100_LoadString((HWND)hDlg,
		//							SUSPENSIVE_MODE,
		//							PRINT_UP_FORMAT_BOLD,// tolta il 25-11-09 PRINT_UP_FORMAT_NORMAL,//: ) FORMAT_BOLD 
		//							strstp->Length,
		//							str);
		Reply = LSLoadString(Connessione,(HWND)hDlg,
								 PRINT_UP_FORMAT_BOLD ,//FORMAT_NORMAL,//: ) FORMAT_BOLD 
								 strstp->Length,
								 str);	
		
		if (Reply == LS_OKAY)
		{
			// via doc
			/*Reply = LS100_DocHandle((HWND)hDlg,
						SUSPENSIVE_MODE,
						NO_FRONT_STAMP,
						PRINT_VALIDATE,
						NO_READ_CODELINE,
						(char)Side,
						SCAN_MODE_256GR200,
						AUTO_FEED,
						SORTER_BAY1,
						WAIT_YES,
						NO_BEEP,
						&NrDoc,
						0,
						0);*/

			Reply = LSDocHandle(Connessione, (HWND)hDlg,
								NO_STAMP,
								PRINT_VALIDATE,
								NO_READ_CODELINE,
								(char)Side,
								SCAN_MODE_256GR200 , 
								AUTO_FEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								&NrDoc,
								0, 0);

			if (Reply == LS_OKAY)
			{
				/*Reply = LS100_ReadImage((HWND)hDlg,
										SUSPENSIVE_MODE,
										CLEAR_ALL_BLACK,
										(char)Side,
										NrDoc,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);*/
				Reply = LSReadImage(Connessione, (HWND)hDlg,
										CLEAR_ALL_BLACK,
										(char)Side,
										0, NrDoc,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);
				if (Reply == LS_OKAY)
				{
					if(Side == SIDE_ALL_IMAGE || Side == SIDE_FRONT_IMAGE)
					{
						StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
						//Reply = LS100_SaveDIB((HWND)hDlg, SUSPENSIVE_MODE, HFrontGray,StrNome);
						Reply = LSSaveDIB((HWND)hDlg,HFrontGray,StrNome);
					}
					if (Reply == LS_OKAY)
					{
						if(Side == SIDE_ALL_IMAGE || Side == SIDE_BACK_IMAGE)
						{
							//NomeFileImmagine = "..\\Dati\\BackImage.bmp";	
							StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();
							//Reply = LS100_SaveDIB((HWND)hDlg, SUSPENSIVE_MODE, HBackGray,StrNome);

						}
					}	
					else
					{
						ReturnC->FunzioneChiamante =  "LS100_SaveimageFronte";
					}
				}
				else
				{
						ReturnC->FunzioneChiamante =  "LS100_ReadImage";
				}
			}
			else
			{
				ReturnC->FunzioneChiamante = "LS100_DocHandle";
			}
		}
		else
		{
			ReturnC->FunzioneChiamante = "LS100_LoadString";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LS100_Open";
	}
	ReturnC->ReturnCode = Reply;
	//LS100_Close((HWND)hDlg, SUSPENSIVE_MODE);
	LSDisconnect(Connessione,(HWND)hDlg);

	return Reply;
}

//int Ls100Class::Periferica::LeggiBadge(short traccia)
//{
//	short Length = 256;
//	char strBadge[256];
//	ReturnC->FunzioneChiamante = "LeggiBadge";
//	Reply = LS100_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
//	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
//	{
//		if (traccia == 1)
//			Reply = LS100_ReadBadgeWithTimeout((HWND)hDlg,'S',FORMAT_IATA_ABA ,256,
//				strBadge,&Length,15000);
//		if (traccia == 2)
//			Reply = LS100_ReadBadgeWithTimeout((HWND)hDlg,'S',FORMAT_ABA_MINTS ,256,
//				strBadge,&Length,15000);
//		if ( traccia == 3)
//			Reply = LS100_ReadBadgeWithTimeout((HWND)hDlg,'S',FORMAT_IATA_ABA_MINTS ,256,
//				strBadge,&Length,15000);
//
//		if (Reply == LS_OKAY)
//		{
//			BadgeLetto = Marshal::PtrToStringAnsi((IntPtr) (char *)strBadge);
//		}
//		else
//		{
//			ReturnC->FunzioneChiamante = "LS100_ReadBadgeWithTimeout";
//		}
//	}
//	else
//	{
//		ReturnC->FunzioneChiamante = "LS100_Open";
//	}
//	ReturnC->ReturnCode = Reply;
//	LS100_Close((HWND)hDlg, SUSPENSIVE_MODE);
//	return Reply;
//}

int Ls100Class::Periferica::LeggiBadge(short traccia)
{
	short Length = 256;
	char strBadge[256];
	short Connessione;

	ReturnC->FunzioneChiamante = "LeggiBadge";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	if( Reply == LS_OKAY )
	{
		if (traccia == 1)
			Reply = LSReadBadgeWithTimeout(Connessione,(HWND)hDlg,FORMAT_IATA_ABA ,256,
				strBadge,&Length,15000);
		if (traccia == 2)
			Reply = LSReadBadgeWithTimeout(Connessione,(HWND)hDlg,FORMAT_ABA_MINTS ,256,
				strBadge,&Length,15000);
		if ( traccia == 3)
			Reply = LSReadBadgeWithTimeout(Connessione,(HWND)hDlg,FORMAT_IATA_ABA_MINTS ,256,
				strBadge,&Length,4000);

		
		BadgeLetto = Marshal::PtrToStringAnsi((IntPtr) (char *)strBadge);

	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}

	ReturnC->ReturnCode = Reply;
	
	LSDisconnect(Connessione,(HWND)hDlg);
	return Reply;
}




//int Ls100Class::Periferica::SerialNumber()
//{
//	short Length = 256;
//	char *str;
//	char Version[64],SerialNumber[64];
//	short ii=0;
//	String ^sss;
//
//	ReturnC->FunzioneChiamante = "SerialNumber";
//	str = (char*) Marshal::StringToHGlobalAnsi(SSerialNumber ).ToPointer();
//
//	Reply = LS100_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
//	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
//	{
//		Reply = LS100_SetSerialNumber((HWND)hDlg, (unsigned char *)str,1962);
//		
//		if (Reply == LS_OKAY)
//		{
//			Reply = LS100_IdentifyEx((HWND)hDlg, SUSPENSIVE_MODE,Version, Version, Version, Version, Version, SerialNumber, Version);
//			if (Reply == LS_OKAY)
//			{
//				//sss = SerialNumber;
//				sss = Marshal::PtrToStringAnsi((IntPtr) (char *)SerialNumber);
//				if( String::Compare(sss, SSerialNumber) == 0)
//				{
//					Reply = LS_OKAY;
//				}
//				else
//				{
//					Reply =LS100_USER_ABORT;
//					ReturnC->FunzioneChiamante = "Compare Write to Read";
//				}
//			}
//			else
//			{
//				ReturnC->FunzioneChiamante = "LS100_IdentifyEx";
//			}
//		}
//		else
//		{
//				ReturnC->FunzioneChiamante = "LS100_SetSerialNumber";
//		}
//		
//		ReturnC->ReturnCode = Reply;
//		LS100_Close((HWND)hDlg, SUSPENSIVE_MODE);
//	}
//	else
//	{
//		ReturnC->FunzioneChiamante = "LS100_Open";
//	}
//
//	return Reply;
//}

int Ls100Class::Periferica::SerialNumber()
{

	short Length = 256;
	char *str;
	short ii=0;
	short Connessione;	

	str = (char*) Marshal::StringToHGlobalAnsi(SSerialNumber ).ToPointer();
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	if( Reply == LS_OKAY )
	{
		Reply = LSSetSerialNumber(Connessione,(HWND)hDlg, (unsigned char *)str,1962);
	}
	ReturnC->ReturnCode = Reply;
	LSDisconnect(Connessione,(HWND)hDlg);

	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Serial Number";

	return Reply;
}


int Ls100Class::Periferica::TestCinghia()
{
	unsigned char pDati[50*1024];
	long lunDati= 50*1024;
	int ii, max, min;
	float media;
	int somma;
	int MinScarto,MaxScarto;
	short Connessione ; 
	
	ReturnC->FunzioneChiamante = "TestCinghia";

	MinScarto = 110;
	MaxScarto = 145;
	
	//Reply = LS100_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	//if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	if( (Reply == LS_OKAY) ) 
	{
		
		//Reply = LS100_DoubleLeafingSensibility((HWND)hDlg,DOUBLE_LEAFING_DISABLE);
		//Reply = LS100_TestCinghia((HWND)hDlg,pDati,&lunDati);
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,150,215);	
		Reply = LSTestCinghia(Connessione,(HWND)hDlg,pDati,&lunDati);
		if(Reply == LS_OKAY)
		{
			max = 0;
			min = 255;
			media = 0;
			somma = 0;
			if (lunDati >= 400)
			{
				
				for(ii = 0; ii < lunDati-400;ii++)
				{	
					if (pDati[ii] < min )
						min = pDati[ii];
					if (pDati[ii] > max )
						max = pDati[ii];
					somma += pDati[ii];
				}
				media  = (float)(somma / (lunDati-400));
				if((min < MinScarto) || (max > MaxScarto))
				{
					Reply = -6000;

				}
				else
				{

				}
				ValMinCinghia = min;
				ValMaxCinghia = max;
				NumDati = lunDati - 400;
			}
			else
			{
				// i dati non sono arrivati
				Reply = -10000;
			}
		}
		else
		{
			ReturnC->FunzioneChiamante = "LSTestCinghia";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}
	ReturnC->ReturnCode = Reply;
	
	//LS100_Close((HWND)hDlg, SUSPENSIVE_MODE);
	LSDisconnect(Connessione,(HWND)hDlg);

	return Reply;
}

int Ls100Class::Periferica::SetConfigurazione(int E13B,int CMC7,int timbro,int badge12,int badge23,int badge123,int scannerfronte, int scannerretro,int sfogliatore,int SwAbilitato,int InkDetector)
{
	char BytesCfg[4];
	short Connessione;

	ReturnC->FunzioneChiamante = "SetConfigurazione";
	// setto configurazione periferica da file
	// Azzero la variabile
	//memset(BytesCfg, 0x40, sizeof(BytesCfg));
	BytesCfg[0] = 0x40;
	BytesCfg[1] = 0x40;
	BytesCfg[2] = 0x40;
	BytesCfg[3] = 0x40;

	if( E13B  || CMC7  )
	{
		BytesCfg[0] |= 0x01;
	}
	if( timbro)
	{
		BytesCfg[0] |= 0x20;
	}
	
	if( badge12)
	{
		BytesCfg[1] |= 0x08;
	}
	if( badge123)
	{
		BytesCfg[1] |= 0x10;
	}

	
	if( scannerfronte && scannerretro )
	{
		BytesCfg[1] |= 0x03;		
	}
	else if( scannerretro )
	{
		BytesCfg[1] |= 0x02;
	}
	else if(scannerfronte )
	{
		BytesCfg[1] |= 0x01;	
	}
	if( sfogliatore)
	{
		BytesCfg[0] |= 0x10;
	}

	if(SwAbilitato >0)
	{
		BytesCfg[3] |= SwAbilitato;
	}

	if (InkDetector )
	{
		BytesCfg[1] |= 0x20;
	}

	// open
	//Reply = LS100_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	//if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	if( (Reply == LS_OKAY) )
	{

		// Setto la configurazione corrente
		//Reply = LS100_SetConfiguration((HWND)hDlg,BytesCfg );
		Reply = LS100_WriteConfiguration((HWND)hDlg,BytesCfg );
		
		if (Reply == LS_OKAY)
		{
		}
		else
		{
			ReturnC->FunzioneChiamante = "LS100_WriteConfiguration";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}
	// close
	ReturnC->ReturnCode = Reply;
	
	//LS100_Close((HWND)hDlg, SUSPENSIVE_MODE);
	LSDisconnect(Connessione,(HWND)hDlg);

	return Reply;
}
int Ls100Class::Periferica::CalibraImmagine()
{
	int ii;
	//short ret;
	short Connessione ;
	BOOL fCalibrationDone;

		// open
	//Reply = LS100_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
		ii = 0;
		fCalibrationDone = FALSE ; 
		do
		{
			fCalibrationDone = DoImageCalibration((HWND)hDlg, 1654,Connessione);

			if( fCalibrationDone == RETRY_IMAGE_CALIBRATION )//RETRY_IMAGE_CALIBRATION )
				Sleep( 20 );	
			ii ++;
			//ret = LS100_Reset((HWND)hDlg, SUSPENSIVE_MODE);
			//ret = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);

		//} while( ((Reply == 1000) || (Reply == LS100_FEEDEREMPTY) ||(Reply == LS100_INVALIDCMD)) && (ii <= 8));
		//} while( ((Reply == 1000) || (Reply == LS_FEEDER_EMPTY) ||(Reply == LS_INVALID_COMMAND)) && (ii <= 8));
		} while( (fCalibrationDone == RETRY_IMAGE_CALIBRATION) && (ii <= 8));

		if( fCalibrationDone == LS_OKAY )
			Reply = LS_OKAY ; 

	}	
	else
	{
		ReturnC->FunzioneChiamante = "LS100_Open";
	}

	// close
	ReturnC->ReturnCode = Reply;
	
	//LS100_Close((HWND)hDlg, SUSPENSIVE_MODE);
	LSDisconnect(Connessione,(HWND)hDlg);

	return Reply;

}

////********************************************************************
//// Controlla l'uniformita' dello sfondo
////
//// Il test fallisce se trovo una riga che supera il valore di 0x40
//// oppure se trovo 10 righe contigue che stanno tra 0x20 e 0x40
//// oppure se trovo 80 righe in totale che stanno tra 0x20 e 0x40
////
//#define NR_PIXEL_BACKGROUND				7
//#define GR256_BMPHEADER					1064
////********************************************************************
//int CheckBackground(HWND hDlg, HANDLE hImage, short *Row)
//{
//	int 	rc;
//	long	height, width, diff;
//	long	width_bytes;
//	short	cc, rr;
//	unsigned char 	*pData;
//	short	Pixel;
//	short	CounterContigue, CounterTotale;
//
//	rc = 0;
//	*Row = -1;
//	CounterContigue = CounterTotale = 0;
//
//	height = ((BITMAPINFOHEADER *)hImage)->biHeight;
//	width = ((BITMAPINFOHEADER *)hImage)->biWidth;
//
//	// Inizializzo i parametri per copiare i dati
//	pData = (unsigned char *)hImage + GR256_BMPHEADER;
//	width_bytes = width;
//	if( (diff = width_bytes % 4) )
//		width_bytes += (4 - diff);
//
//	for( rr = 0; rr < height; rr ++)
//	{
//		Pixel = 0;
//		for( cc = 0; cc < NR_PIXEL_BACKGROUND; cc ++)
//		{
//			Pixel += *(pData + cc);
//		}
//
//		Pixel /= NR_PIXEL_BACKGROUND;
//
//		if( rc == 0 )
//		{
//			// Controllo se la media sta sopra il valore 0x40
//			if( Pixel >= 0x40 )
//			{
//				if( *Row == -1 )
//					*Row = rr;
//				rc = 1;		// Ho trovato una riga fuori valore !!!
//			}
//
//			// Controllo se la media sta sopra il valore 0x30
//			if( Pixel > 0x20 )
//			{
//				CounterContigue ++;
//				CounterTotale ++;
//
//				if( CounterContigue >= 10 )
//				{
//					if( *Row == -1 )
//						*Row = rr;
//					rc = 2;		// Ho trovato una riga fuori valore !!!
//				}
//
//				if( CounterTotale > 80 )
//				{
//					if( *Row == -1 )
//						*Row = rr;
//					rc = 3;		// Ho trovato una riga fuori valore !!!
//				}
//			}
//			else
//				CounterContigue = 0;	// Riazzero il counter delle righe contigue
//		}
//
//		// Passo alla riga successiva
//		pData += width_bytes;
//	}
//
//	return rc;
//} // CheckBackground




//***************************************************************************
// FUNCTION  : DoImageCalibration
//
// PURPOSE   : 
//
// PARAMETER : 
//***************************************************************************
int Ls100Class::Periferica::DoImageCalibration(HWND hDlg, float TeoricValue,short Connessione)
{
	int		 Reply2;
	unsigned long NrDoc;
	long	*BufFrontImage, *BWImage;
	float	RealValue, DiffPercentuale;
//	char	sShowValue[20];
	short	OldValue, NewValue;
	short	ColNereAnt;
	short	ScanMode;
	ReturnC->FunzioneChiamante = "DoImageCalibration";
//	MSG		msg;


	//-----------Open--------------------------------------------
//	Reply = LS100_Open(hDlg, hInst, SUSPENSIVE_MODE);
//	if (Reply != LS_OKAY)
//	{
//		if( CheckReply(hDlg, Reply, "LS100_Open"))
//		    return Reply;
//	}

	// Spedisco in comando che sono in calibrazione
	//Reply = LS100_SetInImageCalibration(hDlg);
	Reply = LSSetInImageCalibration(Connessione, (HWND)hDlg);
// Non testo il reply xch� i vecchi ls100 non hanno questo comando !
//	if( Reply != LS_OKAY )
//	{
//		if( CheckReply(hDlg, Reply, "LS100_SetInImageCalibration"))
//		{
//			LS100_Close(hDlg, SUSPENSIVE_MODE);
//		    return Reply;
//		}
//	}

//	if( TypeLS == TYPE_LS100_5 )
//		ScanMode = SCAN_MODE_COLOR_200;
//	else if( TypeLS == TYPE_LS100_1 || TypeLS == TYPE_LS100_3 )
	//if( TypeLS == TYPE_LS100_1 || TypeLS == TYPE_LS100_3 || TypeLS == TYPE_LS100_5 )
		ScanMode = SCAN_MODE_256GR200;
	//else
	//	ScanMode = SCAN_MODE_BW;

	// Chiamo la funzione di via documento
	/*Reply = LS100_DocHandle((HWND)hDlg,
							SUSPENSIVE_MODE,
							NO_FRONT_STAMP,
							NO_PRINT_VALIDATE,
							NO_READ_CODELINE,
							SIDE_FRONT_IMAGE,
							ScanMode,
							AUTO_FEED,
							SORTER_BAY1,
							WAIT_YES,
							NO_BEEP,
							&NrDoc,
							0,
							0);*/
	Reply = LSDocHandle(Connessione, (HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								SIDE_FRONT_IMAGE,
								ScanMode , 
								AUTO_FEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								&NrDoc,
								0, 0);

	if( Reply != LS_OKAY )
	{
		if( CheckReply(hDlg, Reply, "LS100_DocHandle"))
		{
//			LS100_Close(hDlg, SUSPENSIVE_MODE);
			ReturnC->FunzioneChiamante = "LS100_DocHandle";
		    return Reply;
		}
	}

	// Leggo l'immagine filmata
	/*Reply = LS100_ReadImage((HWND)hDlg,
							SUSPENSIVE_MODE,
							NO_CLEAR_BLACK,
							SIDE_FRONT_IMAGE,
							NrDoc,
							(LPHANDLE)&BufFrontImage,
							NULL,
							NULL,
							NULL);*/
	Reply = LSReadImage(Connessione, (HWND)hDlg,
										NO_CLEAR_BLACK,
										SIDE_FRONT_IMAGE,
										READMODE_BRUTTO, NrDoc,
										(LPHANDLE)&BufFrontImage,
										NULL,
										NULL,
										NULL);

	if( Reply != LS_OKAY )
	{
		if( CheckReply(hDlg, Reply, "LS100_ReadImage"))
		{
//			LS100_Close(hDlg, SUSPENSIVE_MODE);
			ReturnC->FunzioneChiamante = "LS100_ReadImage";
		    return Reply;
		}
	}


	if( Reply == LS_OKAY )
	{
		// Tolgo il nero davanti e dietro e NON sopra per avere 848 righe
		ColNereAnt = 0;
		ClearFrontAndRearBlack( (BITMAPINFOHEADER *)BufFrontImage, &ColNereAnt);

		// Se calibro LS100/3 converto l'immagine in BW
		//if( TypeLS == TYPE_LS100_1 || TypeLS == TYPE_LS100_3 || TypeLS == TYPE_LS100_5)
		{
			/*Reply = LS100_ConvertImageToBW((HWND)hDlg,
										   SUSPENSIVE_MODE,
										   ALGORITHM_CTS,
										   BufFrontImage,
										   (LPHANDLE)&BWImage);*/

			Reply = LSConvertImageToBW((HWND)hDlg,
									   ALGORITHM_CTS,
									   BufFrontImage,
									   (LPHANDLE)&BWImage,
									   DEFAULT_POLO_FILTER,
									   0);

			if( Reply == LS_OKAY )
			{
				//LS100_FreeImage((HWND)hDlg, (LPHANDLE)&BufFrontImage);
				LSFreeImage((HWND)hDlg, (LPHANDLE)&BufFrontImage);
				BufFrontImage = BWImage;
			}
			else
			{
				ReturnC->FunzioneChiamante = "LS100_ConvertImageToBW";
				return Reply;
			}
		}

		

		
		// Leggo lunghezza image
		RealValue = (float)((BITMAPINFOHEADER *)BufFrontImage)->biWidth;
		// Tolgo il nr. di colonne nere anteriori
		RealValue -= ColNereAnt;

		

		// Leggo il valore settato nella periferica
		//Reply = LS100_ImageCalibration((HWND)hDlg, FALSE, &OldValue);
		Reply = LSImageCalibration(Connessione, (HWND)hDlg, FALSE, (short *)&OldValue);
		if( Reply != LS_OKAY )
		{
			CheckReply(hDlg, Reply, "LS100_ImageCalibration");
			ReturnC->FunzioneChiamante = "LS100_ImageCalibration";
//			LS100_Close(hDlg, SUSPENSIVE_MODE);
			return Reply;
		}


		// Controllo lunghezza image
		//DiffPercentuale = TeoricValue * (float)0.004;		// 0.4%
		DiffPercentuale = TeoricValue * (float)0.005;		// 0.5%

		if(	RealValue > (TeoricValue + DiffPercentuale) )
		{
			// Calcolo il valore in percentuale da sommare a quello presente nella perifeica
			DiffPercentuale = RealValue - TeoricValue;
			DiffPercentuale = DiffPercentuale * (float)100 / TeoricValue;
			NewValue = (unsigned short)((float)OldValue * DiffPercentuale) / 100;

			// La periferica Ethernet cambia il tempo di colonna come gli altri LS !!!
			if( TypeLS == TYPE_LS100_ETH || TypeLS == TYPE_LS100_7 || TypeLS == TYPE_LS100_8 )
			{
				NewValue += OldValue;

				// Controllo che non sia inferiore a 1000
				if( NewValue < 1000 )
					NewValue = 1000;
			}
			else
			{
				NewValue = OldValue - NewValue;

				// Controllo che non sia inferiore a 425 (forzo il default)
				if( NewValue < 425 )
					NewValue = 440;		//425;
			}

			Reply = RETRY_IMAGE_CALIBRATION;	// Devo riprovare la taratura
		}


		// Controllo lunghezza image
		DiffPercentuale = TeoricValue * (float)0.005;		// 0.5%

		if( RealValue < (TeoricValue - DiffPercentuale) )
		{
			// Calcolo il valore in percentuale da sommare a quello presente nella perifeica
			DiffPercentuale = TeoricValue - RealValue;
			DiffPercentuale = DiffPercentuale * (float)100 / TeoricValue;
			NewValue = (unsigned short)((float)OldValue * DiffPercentuale) / 100;


			// La periferica Ethernet cambia il tempo di colonna come gli altri LS !!!
			if( TypeLS == TYPE_LS100_ETH || TypeLS == TYPE_LS100_7 || TypeLS == TYPE_LS100_8 )
			{
				NewValue = OldValue - NewValue;

				// Controllo che non sia superiore al 10% del default 1100
				if( NewValue > 1100 )
					NewValue = 1100;
			}
			else
			{
				NewValue += OldValue;

				// Controllo che non sia superiore a 453 (forzo il default)
				if( NewValue > 453 )
					NewValue = 440;		//453;
			}

			Reply = RETRY_IMAGE_CALIBRATION;	// Devo riprovare la taratura
		}


		// Se Reply = RETRY_IMAGE_CALIBRATION ho variato i valori,
		// perci� vado a scrivere quelli nuovi
		if( Reply == RETRY_IMAGE_CALIBRATION )
		{
			//Reply2 = LS100_ImageCalibration((HWND)hDlg, TRUE, &NewValue);
			Reply2 = LSImageCalibration(Connessione, (HWND)hDlg, TRUE, (short *)&NewValue);
			if( Reply2 != LS_OKAY )
			{
				CheckReply(hDlg, Reply2, "LS100_ImageCalibration");
//				LS100_Close(hDlg, SUSPENSIVE_MODE);
				return Reply2;
			}
		}

		// Libero le aree bitmap ritornate
		if( BufFrontImage )
			GlobalFree( BufFrontImage );
	}

//	LS100_Close(hDlg, SUSPENSIVE_MODE);

	return Reply;
} // DoImageCalibration



//static const int		S_LS_FRONT_BACKGROUND				= -5012;
//static const int		S_LS_FRONT_BACKGROUND_1			= -5013;
//static const int		S_LS_FRONT_BACKGROUND_2			= -5014;
//
//static const int		S_LS_BACK_BACKGROUND				= -5015;
//static const int		S_LS_BACK_BACKGROUND_1				= -5016;
//static const int		S_LS_BACK_BACKGROUND_2				= -5017;
//
////********************************************************************
//// Controlla l'uniformita' dello sfondo
////unsigned long	NrDocTest;
////********************************************************************
//int Ls100Class::Periferica::TestBackground(int  fZebrato)
//{
//	int		Reply, rc;
//	HANDLE	pFront, pRear;
//	short	row;
//	//char	sMsg[128];
//	int		SavefOptionFitImage;
//	short Connessione;
//	BOOL fOptionFitImage = FALSE ;
//	char *StrNome = NULL; 
//	FILE *fh ; 
//	unsigned long NrDoc ; 
//
//	ReturnC->FunzioneChiamante = "TestBackground";
//	
//	//effettuo la Connect Diagnostica per ricaricare i dati di compensazione su Ls40 Colore...
//    Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
//
//	if( Reply == LS_OKAY || Reply == LS_ALREADY_OPEN )
//	{
//		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);
//
//		
//		// Leggo il documento
//		Reply = LSDocHandle(Connessione, (HWND)hDlg,
//							NO_STAMP,
//							NO_PRINT_VALIDATE,
//							NO_READ_CODELINE,
//							(char)SIDE_ALL_IMAGE,
//							SCAN_MODE_256GR200,
//							AUTO_FEED,
//							SORTER_BAY1,
//							WAIT_YES,
//							NO_BEEP,
//							&NrDoc , //NULL,
//							0, 0);
//		if( fh = fopen("Trace_BackGroundLS100.txt", "at") )
//					{
//						fprintf(fh, "Reply DOC HANDLE= %d ", Reply);
//						fclose( fh );
//					}
//		if( Reply == LS_OKAY)
//		{
//			// Leggo l'immagine senza clear
//			Reply = LSReadImage(Connessione, (HWND)hDlg,
//								NO_CLEAR_BLACK,
//								SIDE_ALL_IMAGE,
//								0, NrDoc,
//								&pFront,
//								&pRear,
//								NULL,
//								NULL);
//			if( fh = fopen("Trace_BackGroundLS100.txt", "at") )
//					{
//						fprintf(fh, "Reply READ IMAGE= %d ", Reply);
//						fclose( fh );
//					}
//			if( Reply == LS_OKAY )
//			{
//				// Setto FitImage = FALSE per avere la window normal
//				SavefOptionFitImage = fOptionFitImage;	// Salvo il valore
//				fOptionFitImage = FALSE;
//				fOptionFitImage = SavefOptionFitImage;	// Ripristino il vecchio valore
//
//				StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
//				Reply = LSSaveDIB((HWND)hDlg,  pFront,StrNome);
//
//				 // Always free trhe unmanaged string.
//				 Marshal::FreeHGlobal(IntPtr(StrNome));
//
//			  	StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();
//				Reply = LSSaveDIB((HWND)hDlg,pRear,StrNome);
//
//				 // Always free the unmanaged string.
//				 Marshal::FreeHGlobal(IntPtr(StrNome));
//			
//				
//				//if ( fZebrato == 1 ) 
//				//{
//				//	//rc  = 0 ;
//				//	rc = CheckBackground((HWND)hDlg, pRear, &row);
//				//	if( fh = fopen("Trace_BackGround.txt", "at") )
//				//	{
//				//		fprintf(fh, "Reply REAR= %d ", rc);
//				//		fclose( fh );
//				//	}
//				//	switch( rc )
//				//	{
//				//	case 1:
//				//		sprintf(sMsg, "REAR background row = %d NOT adequate !\n\nOne or more row up 64 level !!!", row);
//				//		rc  = S_L40_BACK_BACKGROUND; 
//				//		//MessageBox(hDlg, sMsg, TITLE_POPUP, MB_OK | MB_ICONERROR);
//				//		break;
//				//	case 2:
//				//		sprintf(sMsg, "REAR background row = %d NOT adequate !\n\nMore then 10 row contiguous over the level of 32 !!!", row);
//				//		//MessageBox(hDlg, sMsg, TITLE_POPUP, MB_OK | MB_ICONERROR);
//				//		rc  = S_L40_BACK_BACKGROUND_1; 
//				//		break;
//				//	case 3:
//				//		sprintf(sMsg, "REAR background row = %d NOT adequate !\n\nMore then 80 row over the level of 32 !!!", row);
//				//		rc  = S_L40_BACK_BACKGROUND_2 ; 
//				//		//MessageBox(hDlg, sMsg, TITLE_POPUP, MB_OK | MB_ICONERROR);
//				//		break;
//				//	}
//				//}
//				//else
//				{
//					
//					// Controllo lo sfondo delle 2 immagini
//					rc = CheckBackground((HWND)hDlg, pFront, &row);
//					if( fh = fopen("Trace_BackGroundLS100.txt", "at") )
//					{
//						fprintf(fh, "Reply FRONT= %d ", rc);
//						fclose( fh );
//					}
//					if( rc == 0  )
//					{
//						rc = CheckBackground((HWND)hDlg, pRear, &row);
//						if( fh = fopen("Trace_BackGroundLS100.txt", "at") )
//						{
//							fprintf(fh, "Reply REAR= %d ", rc);
//							fclose( fh );
//						}
//						if( rc )
//						{
//							switch( rc )
//							{
//							case 1:
//								//sprintf(sMsg, "REAR background row = %d NOT adequate !\n\nOne or more row up 64 level !!!", row);
//								rc  = S_LS_BACK_BACKGROUND; 
//								break;
//							case 2:
//								//sprintf(sMsg, "REAR background row = %d NOT adequate !\n\nMore then 10 row contiguous over the level of 32 !!!", row);
//								rc  = S_LS_BACK_BACKGROUND_1; 
//								break;
//							case 3:
//								//sprintf(sMsg, "REAR background row = %d NOT adequate !\n\nMore then 80 row over the level of 32 !!!", row);
//								rc  = S_LS_BACK_BACKGROUND_2 ; 
//								break;
//							}
//						}
//						else
//						{
//							//MessageBox(hDlg, "Check background PASSED !", TITLE_POPUP, MB_OK | MB_ICONINFORMATION);
//							rc = 0 ; 
//						}
//					}
//					else
//					{
//						switch( rc )
//						{
//						case 1:
//							//sprintf(sMsg, "FRONT background row = %d NOT adequate !\n\nOne or more row up 64 level !!!", row);
//							rc  = S_LS_FRONT_BACKGROUND; 
//							break;
//						case 2:
//							//sprintf(sMsg, "FRONT background row = %d NOT adequate !\n\nMore then 8 row contiguous over the level of 32 !!!", row);
//							rc  = S_LS_FRONT_BACKGROUND_1; 
//							break;
//						case 3:
//							//sprintf(sMsg, "FRONT background row = %d NOT adequate !\n\nMore then 80 row over the level of 32 !!!", row);
//							rc = S_LS_FRONT_BACKGROUND_2; 
//							break;
//						}
//					}
//				}
//
//				LSFreeImage((HWND)hDlg, &pFront);
//				LSFreeImage((HWND)hDlg, &pRear);
//			}
//			else
//			{
//				//CheckReply(hDlg, Reply, "LS150_ReadImage");
//				ReturnC->FunzioneChiamante = "LSReadImage";
//			}
//		}
//		else
//		{
//			//CheckReply(hDlg, Reply, "LS150_DocHandle");
//			ReturnC->FunzioneChiamante = "LSDocHandle";
//		}
//
//		LSDisconnect(Connessione,(HWND)hDlg);
//	}
//	else
//	{
//		//CheckReply(hDlg, Reply, "LS150_Open");
//		ReturnC->FunzioneChiamante = "LSOpen";
//	}
//
//	return rc;
//} // TestBackground





//void Ls100Class::Periferica::ClearFrontAndRearBlack(BITMAPINFOHEADER *Bitmap, short *ColNereAnt)
//{
//	unsigned long ii, jj;
//	long	CondDib;
//	short	ValoriOk, NrColonneNere;
//	long	NrByte;
//	BOOL	ContinueClear;
//	BOOL	HalfByteHigh;
//	unsigned char *pbm, *pbmt, *pbmtm;
//	unsigned char HighValue,MaskBit;
//	long	RealWidth, OldWidth, DiffWidth;
//	long	nr_col_bmp, nr_row_bmp;
//	
//
//	// test se la bitmap � in bianco nero o a livelli di grigio
//	if( Bitmap->biBitCount == 1 )
//	{
//		// Bitmap a B/N
////	SalvaDIBBitmap(Bitmap, "g:\\Progetti\\bmp\\front2.bmp");
//
//		// Inizio dati bitmap
//		pbm = (unsigned char *)Bitmap + sizeof(BITMAPINFOHEADER) + (2 * sizeof(RGBQUAD));
//		NrByte = Bitmap->biWidth / 8;
//		if(CondDib = (Bitmap->biWidth % 32) )
//			nr_col_bmp = (Bitmap->biWidth + 32 - CondDib) / 8;
//		else
//			nr_col_bmp = NrByte;
//		nr_row_bmp = Bitmap->biHeight;
//		pbmt = pbm;
//		ContinueClear = TRUE;
//		HalfByteHigh = TRUE;
//		ValoriOk = 0;
//		NrColonneNere = 0;
//		MaskBit = 0x80;
//
//		// Tolgo le colonne nere davanti
//		do
//		{
//			for( ii = 0; ii < (unsigned long)nr_row_bmp; ii ++, pbmt += nr_col_bmp)
//			{
//				// Controllo tutti bit
//				if( *pbmt & MaskBit )
//					ValoriOk ++;
//			}
//
//			//Incremento il counter nr. colonne nere
//			if( ValoriOk <= NR_VALORI_OK )
//			{
//				NrColonneNere ++;
//				ValoriOk = 0;
//				if( MaskBit & 0x01 )
//					MaskBit = 0x80;
//				else
//					MaskBit >>= 1;
//			}
//			else
//				ContinueClear = FALSE;
//
//			pbmt = pbm;
//			pbmt += (NrColonneNere / 8);
//
//		} while( ContinueClear && (NrColonneNere < (nr_col_bmp * 8)) );
//
//		// SALVO il nr. di colonne nere anteriori
//		*ColNereAnt = (NrColonneNere % 8);
//
//		//Controllo se shiftare la bitmap
//		if( NrColonneNere && (NrColonneNere < (nr_col_bmp * 8)) )
//		{
//			// Inizializzo i pointer
//			pbmt = pbmtm = pbm;
//			NrByte = nr_col_bmp;
//
//			// Calcolo il NUOVO numero di colonne e lo salvo
//			nr_col_bmp *= 8;
//			nr_col_bmp -= NrColonneNere;
//			Bitmap->biWidth = nr_col_bmp;
//
//			if( CondDib = (nr_col_bmp % 32) )
//				nr_col_bmp = (nr_col_bmp + 32 - CondDib);
//
//			// Trasformo in nr. di byte
//			NrColonneNere /= 8;
//
//			// Sposto la bitmap
//			for( ii = 0; ii < (unsigned long)Bitmap->biHeight; ii ++)
//			{
//				pbmt = pbm + (NrByte * ii) + NrColonneNere;
////				for( jj = NrColonneNere; jj < (unsigned long)NrByte; jj ++)
//				for( jj = 0; jj < (unsigned long)(Bitmap->biWidth / 8); jj ++)
//					*(pbmtm++) = *(pbmt++);
//
//				// Aggiungo il nero
////				for( jj = 0; jj < (unsigned long)((nr_col_bmp - Bitmap->biWidth) / 8); jj ++)
//				for( ; jj < (unsigned long)(nr_col_bmp / 8); jj ++)
//					*(pbmtm++) = 0x00;	//NERO;
//			}
//
//			// Aggiorno il nr. di bytes della bitmap
//			Bitmap->biSizeImage = Bitmap->biHeight * nr_col_bmp / 8;
//		}
//
//
//
////	SalvaDIBBitmap(Bitmap, "\\Progetti\\bmp\\front3.bmp");
//
//		// Inizio dati bitmap
//		pbm = (unsigned char *)Bitmap + sizeof(BITMAPINFOHEADER) + (2 * sizeof(RGBQUAD));
//		NrByte = Bitmap->biWidth / 8;
//		if(CondDib = (Bitmap->biWidth % 32) )
//			nr_col_bmp = (Bitmap->biWidth + 32 - CondDib) / 8;
//		else
//			nr_col_bmp = NrByte;
//		nr_row_bmp = Bitmap->biHeight;
//		pbmt = pbm + (nr_col_bmp - 1);
//		ContinueClear = TRUE;
//		ValoriOk = 0;
//		NrColonneNere = 0;
//		MaskBit = 0x01;
//
//		// Tolgo le colonne nere dietro
//		do
//		{
//			for( ii = 0; ii < (unsigned long)nr_row_bmp; ii ++, pbmt += nr_col_bmp)
//			{
//				// Controllo tutti bit
//				if( *pbmt & MaskBit )
//					ValoriOk ++;
//			}
//
//			//Incremento il counter nr. colonne nere
//			if( ValoriOk <= NR_VALORI_OK )
//			{
//				NrColonneNere ++;
//				ValoriOk = 0;
//				if( MaskBit & 0x80 )
//					MaskBit = 0x01;
//				else
//					MaskBit <<= 1;
//			}
//			else
//				ContinueClear = FALSE;
//
//			// Reinizializzo il pointer ad inizio bitmap
//			pbmt = pbm + (nr_col_bmp - 1) - (NrColonneNere / 8);
//
//		} while( ContinueClear && (NrColonneNere < (nr_col_bmp * 8)) );
//
//		if( NrColonneNere && (NrColonneNere < (nr_col_bmp * 8)) )
//		{
//			// Sottraggo dal nr. di colonne nere trovate il nr di colonne di arrotondamento
//			if( CondDib )
//				NrColonneNere -= (short)(32 - CondDib);
//
//			OldWidth = Bitmap->biWidth;
//
//			// Aggiorno l'header della bitmap
//			Bitmap->biWidth -= NrColonneNere;
//
//			RealWidth = Bitmap->biWidth;
//
//			// Calcolo la larghezza reale della bitmap
//			if( CondDib = OldWidth % 32 )
//				OldWidth += (32 - CondDib);
//
//			if( CondDib = RealWidth % 32 )
//				RealWidth += (32 - CondDib);
//
//			// Aggiorno la size image
//			Bitmap->biSizeImage = (Bitmap->biHeight * RealWidth) / 8;
//
//			DiffWidth = OldWidth - RealWidth;
//
//			// Arrotondo al byte
//			DiffWidth /= 8;
//
//			//Controllo se shiftare la bitmap
//			if( DiffWidth >= 4 )
//			{
//				// Porto le misure in bytes
//				OldWidth /= 8;
//				RealWidth /= 8;
//
//				// Sposto la bitmap
//				for( ii = 1; ii < (unsigned long)Bitmap->biHeight; ii ++)
//				{
//					pbmtm = pbm + (RealWidth * ii);
//					pbmt = pbmtm + (DiffWidth * ii);
//					for( jj = 0; jj < (unsigned long)RealWidth; jj ++)
//						*(pbmtm++) = *(pbmt++);
//				}
//			}
//		}
//
////	SalvaDIBBitmap(Bitmap, "\\Progetti\\bmp\\front_BW.bmp");
//
//	}
//	//Test se � una bitmap a 16 o 256 livelli di grigio
//	else if( Bitmap->biBitCount == 4 )
//	{
//		// Bitmap a 16 grigi
//
////	SalvaDIBBitmap(Bitmap, "e:\\bmp\\front.bmp");
//
//		// Inizio dati bitmap
//		pbm = (unsigned char *)Bitmap + sizeof(BITMAPINFOHEADER) + (16 * sizeof(RGBQUAD));
//		NrByte = Bitmap->biWidth / 2;
//		if(CondDib = (Bitmap->biWidth % 8) )
//			nr_col_bmp = (Bitmap->biWidth + 8 - CondDib) / 2;
//		else
//			nr_col_bmp = NrByte;
//		nr_row_bmp = Bitmap->biHeight;
//		pbmt = pbm;
//		ContinueClear = TRUE;
//		HalfByteHigh = TRUE;
//		ValoriOk = 0;
//		NrColonneNere = 0;
//
//		// Tolgo le colonne nere davanti
//		do
//		{
//			for( ii = 0; ii < (unsigned long)nr_row_bmp; ii ++, pbmt += nr_col_bmp)
//			{
//				// Prendo il valore di grigio parte alta se colonna pari, altrimenti parte bassa
//				if( HalfByteHigh )
//					HighValue = *pbmt >> 4;
//				else
//					HighValue = *pbmt & 0x0f;
//
//				if( HighValue > LIMITE_16_GRIGI )
//					ValoriOk ++;
//			}
//
//			//Incremento il counter nr. colonne nere
//			if( ValoriOk <= NR_VALORI_OK )
//			{
//				NrColonneNere ++;
//				ValoriOk = 0;
//			}
//			else
//				ContinueClear = FALSE;
//
//			// Reinizializzo il pointer ad inizio bitmap
//			pbmt = pbm + (NrColonneNere / 2);
//
//			HalfByteHigh = !HalfByteHigh;
//
//		} while( ContinueClear && (NrColonneNere < (nr_col_bmp * 2)) );
//
//
//		//Controllo se shiftare la bitmap
//		if( NrColonneNere && (NrColonneNere < (nr_col_bmp * 2)) )
//		{
//			// Inizializzo i pointer
//			pbmt = pbmtm = pbm;
//			NrByte = nr_col_bmp;
//
//			// Calcolo il NUOVO numero di colonne e lo salvo
//			nr_col_bmp *= 2;
//			nr_col_bmp -= NrColonneNere;
//			Bitmap->biWidth = nr_col_bmp;
//
//			if( CondDib = (nr_col_bmp % 8) )
//				nr_col_bmp = (nr_col_bmp + 8 - CondDib);
//
//			// Trasformo in nr. di byte
//			NrColonneNere /= 2;
//
//			// Sposto la bitmap
//			for( ii = 0; ii < (unsigned long)Bitmap->biHeight; ii ++)
//			{
//				pbmt += NrColonneNere;
//				for( jj = NrColonneNere; jj < (unsigned long)NrByte; jj ++)
//					*(pbmtm++) = *(pbmt++);
//
//				// Aggiungo il nero
//				for( jj = 0; jj < (unsigned long)((nr_col_bmp - Bitmap->biWidth) / 2); jj ++)
//					*(pbmtm++) = 0x00;	//NERO;
//			}
//
//			// Aggiorno il nr. di bytes della bitmap
//			Bitmap->biSizeImage = Bitmap->biHeight * nr_col_bmp / 2;
//		}
//
////	SalvaDIBBitmap(Bitmap, "g:\\Progetti\\bmp\\front3.bmp");
//
//
//		// Inizio dati bitmap
//		pbm = (unsigned char *)Bitmap + sizeof(BITMAPINFOHEADER) + (16 * sizeof(RGBQUAD));
//		NrByte = Bitmap->biWidth / 2;
//		if(CondDib = (Bitmap->biWidth % 8) )
//			nr_col_bmp = (Bitmap->biWidth + 8 - CondDib) / 2;
//		else
//			nr_col_bmp = NrByte;
//		nr_row_bmp = Bitmap->biHeight;
//		pbmt = pbm + (nr_col_bmp - 1);
//		ContinueClear = TRUE;
//		HalfByteHigh = FALSE;
//		ValoriOk = 0;
//		NrColonneNere = 0;
//
//// --- Secondo me sono inutili
////		NrByte /= ALLIGN_COLONNE;
////		NrByte *= ALLIGN_COLONNE;
//
//		// Tolgo le colonne nere dietro
//		do
//		{
//			for( ii = 0; ii < (unsigned long)nr_row_bmp; ii ++, pbmt += nr_col_bmp)
//			{
//				// Prendo il valore di grigio parte alta se colonna pari, altrimenti parte bassa
//				if( HalfByteHigh )
//					HighValue = *pbmt >> 4;
//				else
//					HighValue = *pbmt & 0x0f;
//
//				if( HighValue > LIMITE_16_GRIGI )
//					ValoriOk ++;
//			}
//
//			//Incremento il counter nr. colonne nere
//			if( ValoriOk <= NR_VALORI_OK )
//			{
//				NrColonneNere ++;
//				ValoriOk = 0;
//			}
//			else
//				ContinueClear = FALSE;
//
//			// Reinizializzo il pointer ad inizio bitmap
//			pbmt = pbm + (nr_col_bmp - 1) - (NrColonneNere / 2);
//
//			HalfByteHigh = !HalfByteHigh;
//
//		} while( ContinueClear && (NrColonneNere < (nr_col_bmp * 2)) );
//
//		if( NrColonneNere && (NrColonneNere < (nr_col_bmp * 2)) )
//		{
//			DiffWidth = Bitmap->biWidth;
//
//			// Arrotondo il numero di colonne nere ad un nr. pari
//			if( NrColonneNere % 2 )
//				NrColonneNere --;
//
//			// Aggiorno l'header della bitmap
//			Bitmap->biWidth -= NrColonneNere;
//
//			RealWidth = Bitmap->biWidth;
//
//			// Calcolo la larghezza reale della bitmap
//			if( CondDib = DiffWidth % 8 )
//				DiffWidth += (8 - CondDib);
//
//			if( CondDib = RealWidth % 8 )
//				RealWidth += (8 - CondDib);
//
//			DiffWidth -= RealWidth;
//
//			Bitmap->biSizeImage = (Bitmap->biHeight * RealWidth) / 2;
//
//			// Arrotondo al byte
//			RealWidth /= 2;
//			DiffWidth /= 2;
//
//			//Controllo se shiftare la bitmap
//			if( NrColonneNere )
//			{
//				pbm += RealWidth;
//				pbmt = pbm;
//
//				// Sposto la bitmap
//				for( ii = 1; ii < (unsigned long)Bitmap->biHeight; ii ++)
//				{
//					pbmt += DiffWidth;
//					for( jj = 0; jj < (unsigned long)RealWidth; jj ++)
//						*(pbm++) = *(pbmt++);
//				}
//			}
//		}
//
////	SalvaDIBBitmap(Bitmap, "g:\\Progetti\\bmp\\front4.bmp");
//
//	}
//	else
//	{
//		// Bitmap a 256 grigi
//
////	SalvaDIBBitmap(Bitmap, "e:\\bmp\\front.bmp");
//
//		// Inizio dati bitmap
//		pbm = (unsigned char *)Bitmap + sizeof(BITMAPINFOHEADER) + (256 * sizeof(RGBQUAD));
//		if(CondDib = (Bitmap->biWidth % 4) )
//			nr_col_bmp = (Bitmap->biWidth + 4 - CondDib);
//		else
//			nr_col_bmp = Bitmap->biWidth;
//		nr_row_bmp = Bitmap->biHeight;
//		ContinueClear = TRUE;
//		ValoriOk = 0;
//		NrColonneNere = 0;
//
//		// Tolgo le colonne nere davanti
//		pbmt = pbm;
//		do
//		{
//			for( ii = 0; ii < (unsigned long)nr_row_bmp; ii ++, pbmt += nr_col_bmp)
//				if( *pbmt > LIMITE_256_GRIGI )
//					ValoriOk ++;
//
//			//Controllo se eliminare la colonna
//			if( ValoriOk <= NR_VALORI_OK )
//			{
//				NrColonneNere ++;
//				ValoriOk = 0;
//			}
//			else
//				ContinueClear = FALSE;
//
//			// Reinizializzo il pointer ad inizio bitmap
//			pbmt = pbm + NrColonneNere;
//
//		} while( ContinueClear && (NrColonneNere < nr_col_bmp) );
//
//
//		//Controllo se shiftare la bitmap
//		if( NrColonneNere && (NrColonneNere < nr_col_bmp) )
//		{
//			// Inizializzo i pointer
//			pbmt = pbmtm = pbm;
//			NrByte = nr_col_bmp;
//
//			// Calcolo il NUOVO numero di colonne e lo salvo
//			nr_col_bmp -= NrColonneNere;
//			Bitmap->biWidth = nr_col_bmp;
//
//			if( CondDib = (nr_col_bmp % 4) )
//				nr_col_bmp = (nr_col_bmp + 4 - CondDib);
//
//			// Sposto la bitmap
//			for( ii = 0; ii < (unsigned long)Bitmap->biHeight; ii ++)
//			{
//				pbmt += NrColonneNere;
//				for( jj = NrColonneNere; jj < (unsigned long)NrByte; jj ++)
//					*(pbmtm++) = *(pbmt++);
//
//				// Aggiungo il nero
//				for( jj = 0; jj < (unsigned long)(nr_col_bmp - Bitmap->biWidth); jj ++)
//					*(pbmtm++) = 0x00;	//NERO;
//			}
//
//			// Aggiorno il nr. di bytes della bitmap
//			Bitmap->biSizeImage = Bitmap->biHeight * nr_col_bmp;
//		}
//
////	SalvaDIBBitmap(Bitmap, "g:\\Progetti\\bmp\\front3.bmp");
//
//
//		// Inizio dati bitmap
//		pbm = (unsigned char *)Bitmap + sizeof(BITMAPINFOHEADER) + (256 * sizeof(RGBQUAD));
//		if(CondDib = (Bitmap->biWidth % 4) )
//			nr_col_bmp = (Bitmap->biWidth + 4 - CondDib);
//		else
//			nr_col_bmp = Bitmap->biWidth;
//		nr_row_bmp = Bitmap->biHeight;
//		pbmt = pbm + nr_col_bmp - 1;
//		ContinueClear = TRUE;
//		ValoriOk = 0;
//		NrColonneNere = 0;
//
//		// Tolgo le colonne nere dietro
//		do
//		{
//			for( ii = 0; ii < (unsigned long)nr_row_bmp; ii ++, pbmt += nr_col_bmp)
//				if( *pbmt > LIMITE_256_GRIGI )
//					ValoriOk ++;
//
//			//Incremento il counter nr. colonne nere
//			if( ValoriOk <= NR_VALORI_OK )
//			{
//				NrColonneNere ++;
//				ValoriOk = 0;
//			}
//			else
//				ContinueClear = FALSE;
//
//			// Reinizializzo il pointer ad inizio bitmap
//			pbmt = pbm + (nr_col_bmp - 1) - NrColonneNere;
//
//		} while( ContinueClear && (NrColonneNere < nr_col_bmp) );
//
//		if( NrColonneNere && (NrColonneNere < nr_col_bmp) )
//		{
//			DiffWidth = Bitmap->biWidth;
//
//			// Aggiorno l'header della bitmap
//			Bitmap->biWidth -= NrColonneNere;
//
//			RealWidth = Bitmap->biWidth;
//
//			// Calcolo la larghezza reale della bitmap
//			if( CondDib = DiffWidth % 4 )
//				DiffWidth += (4 - CondDib);
//
//			if( CondDib = RealWidth % 4 )
//				RealWidth += (4 - CondDib);
//
//			DiffWidth -= RealWidth;
//
//			Bitmap->biSizeImage = Bitmap->biHeight * RealWidth;
//
//
//			//Controllo se shiftare la bitmap
//			if( NrColonneNere )
//			{
//				pbm += RealWidth;
//				pbmt = pbm;
//
//				// Sposto la bitmap
//				for( ii = 1; ii < (unsigned long)Bitmap->biHeight; ii ++)
//				{
//					pbmt += DiffWidth;
//					for( jj = 0; jj < (unsigned long)RealWidth; jj ++)
//						*(pbm++) = *(pbmt++);
//				}
//			}
//		}
//
////	SalvaDIBBitmap(Bitmap, "g:\\Progetti\\bmp\\front4.bmp");
//
//	} // Fine else
//
//} // ClearFrontAndRearBlack

//********************************************************************
// FUNCTION  : ClearFrontAndRearBlack
//
// PURPOSE   : Salva il documento in formato DIB
//
// PARAMETER : 
//********************************************************************
void ClearFrontAndRearBlack(BITMAPINFOHEADER *Bitmap, short *ColNereAnt)
{
	unsigned long ii, jj;
	long	CondDib;
	short	ValoriOk, NrColonneNere;
	long	NrByteCol;
	BOOL	ContinueClear;
	BOOL	HalfByteHigh;
	unsigned char *pbm, *pbmt, *pbmtm;
	unsigned char HighValue,MaskBit;
	long	RealWidth, OldWidth, DiffWidth;
	long	nr_col_bmp, nr_row_bmp;
	

	// test se la bitmap � in bianco nero o a livelli di grigio
	if( Bitmap->biBitCount == 1 )
	{
		// Bitmap a B/N
//	SalvaDIBBitmap(Bitmap, "g:\\Progetti\\bmp\\front2.bmp");

		// Inizio dati bitmap
		pbm = (unsigned char *)Bitmap + sizeof(BITMAPINFOHEADER) + (2 * sizeof(RGBQUAD));
		NrByteCol = Bitmap->biWidth / 8;
		if(CondDib = (Bitmap->biWidth % 32) )
			nr_col_bmp = (Bitmap->biWidth + 32 - CondDib) / 8;
		else
			nr_col_bmp = NrByteCol;
		nr_row_bmp = Bitmap->biHeight;
		pbmt = pbm;
		ContinueClear = TRUE;
		HalfByteHigh = TRUE;
		ValoriOk = 0;
		NrColonneNere = 0;
		MaskBit = 0x80;

		// Tolgo le colonne nere davanti
		do
		{
			for( ii = 0; ii < (unsigned long)nr_row_bmp; ii ++, pbmt += nr_col_bmp)
			{
				// Controllo tutti bit
				if( *pbmt & MaskBit )
					ValoriOk ++;
			}

			//Incremento il counter nr. colonne nere
			if( ValoriOk <= NR_VALORI_OK )
			{
				NrColonneNere ++;
				ValoriOk = 0;
				if( MaskBit & 0x01 )
					MaskBit = 0x80;
				else
					MaskBit >>= 1;
			}
			else
				ContinueClear = FALSE;

			pbmt = pbm;
			pbmt += (NrColonneNere / 8);

		} while( ContinueClear && (NrColonneNere < (nr_col_bmp * 8)) );

		// SALVO il nr. di colonne nere anteriori
		*ColNereAnt = (NrColonneNere % 8);

		//Controllo se shiftare la bitmap
		if( NrColonneNere && (NrColonneNere < (nr_col_bmp * 8)) )
		{
			// Inizializzo i pointer
			pbmt = pbmtm = pbm;
			NrByteCol = nr_col_bmp;

			// Calcolo il NUOVO numero di colonne e lo salvo
			nr_col_bmp *= 8;
			nr_col_bmp -= NrColonneNere;
			Bitmap->biWidth = nr_col_bmp;

			if( CondDib = (nr_col_bmp % 32) )
				nr_col_bmp = (nr_col_bmp + 32 - CondDib);

			// Trasformo in nr. di byte
			NrColonneNere /= 8;

			// Sposto la bitmap
			for( ii = 0; ii < (unsigned long)Bitmap->biHeight; ii ++)
			{
				pbmt = pbm + (NrByteCol * ii) + NrColonneNere;
				for( jj = 0; jj < (unsigned long)(Bitmap->biWidth / 8); jj ++)
					*(pbmtm++) = *(pbmt++);

				// Aggiungo il nero
				for( ; jj < (unsigned long)(nr_col_bmp / 8); jj ++)
					*(pbmtm++) = 0x00;	//NERO;
			}

			// Aggiorno il nr. di bytes della bitmap
			Bitmap->biSizeImage = Bitmap->biHeight * nr_col_bmp / 8;
		}



//	SalvaDIBBitmap(Bitmap, "\\Progetti\\bmp\\front3.bmp");

		// Inizio dati bitmap
		pbm = (unsigned char *)Bitmap + sizeof(BITMAPINFOHEADER) + (2 * sizeof(RGBQUAD));
		NrByteCol = Bitmap->biWidth / 8;
		if(CondDib = (Bitmap->biWidth % 32) )
			nr_col_bmp = (Bitmap->biWidth + 32 - CondDib) / 8;
		else
			nr_col_bmp = NrByteCol;
		nr_row_bmp = Bitmap->biHeight;
		pbmt = pbm + (nr_col_bmp - 1);
		ContinueClear = TRUE;
		ValoriOk = 0;
		NrColonneNere = 0;
		MaskBit = 0x01;

		// Tolgo le colonne nere dietro
		do
		{
			for( ii = 0; ii < (unsigned long)nr_row_bmp; ii ++, pbmt += nr_col_bmp)
			{
				// Controllo tutti bit
				if( *pbmt & MaskBit )
					ValoriOk ++;
			}

			//Incremento il counter nr. colonne nere
			if( ValoriOk <= NR_VALORI_OK )
			{
				NrColonneNere ++;
				ValoriOk = 0;
				if( MaskBit & 0x80 )
					MaskBit = 0x01;
				else
					MaskBit <<= 1;
			}
			else
				ContinueClear = FALSE;

			// Reinizializzo il pointer ad inizio bitmap
			pbmt = pbm + (nr_col_bmp - 1) - (NrColonneNere / 8);

		} while( ContinueClear && (NrColonneNere < (nr_col_bmp * 8)) );

		if( NrColonneNere && (NrColonneNere < (nr_col_bmp * 8)) )
		{
			// Sottraggo dal nr. di colonne nere trovate il nr di colonne di arrotondamento
			if( CondDib )
				NrColonneNere -= (short)(32 - CondDib);

			OldWidth = Bitmap->biWidth;

			// Aggiorno l'header della bitmap
			Bitmap->biWidth -= NrColonneNere;

			RealWidth = Bitmap->biWidth;

			// Calcolo la larghezza reale della bitmap
			if( CondDib = OldWidth % 32 )
				OldWidth += (32 - CondDib);

			if( CondDib = RealWidth % 32 )
				RealWidth += (32 - CondDib);

			// Aggiorno la size image
			Bitmap->biSizeImage = (Bitmap->biHeight * RealWidth) / 8;

			DiffWidth = OldWidth - RealWidth;

			// Arrotondo al byte
			DiffWidth /= 8;

			//Controllo se shiftare la bitmap
			if( DiffWidth >= 4 )
			{
				// Porto le misure in bytes
				OldWidth /= 8;
				RealWidth /= 8;

				// Sposto la bitmap
				for( ii = 1; ii < (unsigned long)Bitmap->biHeight; ii ++)
				{
					pbmtm = pbm + (RealWidth * ii);
					pbmt = pbmtm + (DiffWidth * ii);
					for( jj = 0; jj < (unsigned long)RealWidth; jj ++)
						*(pbmtm++) = *(pbmt++);
				}
			}
		}

//	SalvaDIBBitmap(Bitmap, "\\Progetti\\bmp\\front_BW.bmp");

	}
	//Test se � una bitmap a 16 o 256 livelli di grigio
	else if( Bitmap->biBitCount == 4 )
	{
		// Bitmap a 16 grigi

//	SalvaDIBBitmap(Bitmap, "e:\\bmp\\front.bmp");

		// Inizio dati bitmap
		pbm = (unsigned char *)Bitmap + sizeof(BITMAPINFOHEADER) + (16 * sizeof(RGBQUAD));
		NrByteCol = Bitmap->biWidth / 2;
		if(CondDib = (Bitmap->biWidth % 8) )
			nr_col_bmp = (Bitmap->biWidth + 8 - CondDib) / 2;
		else
			nr_col_bmp = NrByteCol;
		nr_row_bmp = Bitmap->biHeight;
		pbmt = pbm;
		ContinueClear = TRUE;
		HalfByteHigh = TRUE;
		ValoriOk = 0;
		NrColonneNere = 0;

		// Tolgo le colonne nere davanti
		do
		{
			for( ii = 0; ii < (unsigned long)nr_row_bmp; ii ++, pbmt += nr_col_bmp)
			{
				// Prendo il valore di grigio parte alta se colonna pari, altrimenti parte bassa
				if( HalfByteHigh )
					HighValue = *pbmt >> 4;
				else
					HighValue = *pbmt & 0x0f;

				if( HighValue > Limite16Grigi )
					ValoriOk ++;
			}

			//Incremento il counter nr. colonne nere
			if( ValoriOk <= NR_VALORI_OK )
			{
				NrColonneNere ++;
				ValoriOk = 0;
			}
			else
				ContinueClear = FALSE;

			// Reinizializzo il pointer ad inizio bitmap
			pbmt = pbm + (NrColonneNere / 2);

			HalfByteHigh = !HalfByteHigh;

		} while( ContinueClear && (NrColonneNere < (nr_col_bmp * 2)) );


		//Controllo se shiftare la bitmap
		if( NrColonneNere && (NrColonneNere < (nr_col_bmp * 2)) )
		{
			// Inizializzo i pointer
			pbmt = pbmtm = pbm;
			NrByteCol = nr_col_bmp;

			// Calcolo il NUOVO numero di colonne e lo salvo
			nr_col_bmp *= 2;
			nr_col_bmp -= NrColonneNere;
			Bitmap->biWidth = nr_col_bmp;

			if( CondDib = (nr_col_bmp % 8) )
				nr_col_bmp = (nr_col_bmp + 8 - CondDib);

			// Trasformo in nr. di byte
			NrColonneNere /= 2;

			// Sposto la bitmap
			for( ii = 0; ii < (unsigned long)Bitmap->biHeight; ii ++)
			{
				pbmt += NrColonneNere;
				for( jj = NrColonneNere; jj < (unsigned long)NrByteCol; jj ++)
					*(pbmtm++) = *(pbmt++);

				// Aggiungo il nero
				for( jj = 0; jj < (unsigned long)((nr_col_bmp - Bitmap->biWidth) / 2); jj ++)
					*(pbmtm++) = 0x00;	//NERO;
			}

			// Aggiorno il nr. di bytes della bitmap
			Bitmap->biSizeImage = Bitmap->biHeight * nr_col_bmp / 2;
		}

//	SalvaDIBBitmap(Bitmap, "g:\\Progetti\\bmp\\front3.bmp");


		// Inizio dati bitmap
		pbm = (unsigned char *)Bitmap + sizeof(BITMAPINFOHEADER) + (16 * sizeof(RGBQUAD));
		NrByteCol = Bitmap->biWidth / 2;
		if(CondDib = (Bitmap->biWidth % 8) )
			nr_col_bmp = (Bitmap->biWidth + 8 - CondDib) / 2;
		else
			nr_col_bmp = NrByteCol;
		nr_row_bmp = Bitmap->biHeight;
		pbmt = pbm + (nr_col_bmp - 1);
		ContinueClear = TRUE;
		HalfByteHigh = FALSE;
		ValoriOk = 0;
		NrColonneNere = 0;

// --- Secondo me sono inutili
//		NrByteCol /= ALLIGN_COLONNE;
//		NrByteCol *= ALLIGN_COLONNE;

		// Tolgo le colonne nere dietro
		do
		{
			for( ii = 0; ii < (unsigned long)nr_row_bmp; ii ++, pbmt += nr_col_bmp)
			{
				// Prendo il valore di grigio parte alta se colonna pari, altrimenti parte bassa
				if( HalfByteHigh )
					HighValue = *pbmt >> 4;
				else
					HighValue = *pbmt & 0x0f;

				if( HighValue > Limite16Grigi )
					ValoriOk ++;
			}

			//Incremento il counter nr. colonne nere
			if( ValoriOk <= NR_VALORI_OK )
			{
				NrColonneNere ++;
				ValoriOk = 0;
			}
			else
				ContinueClear = FALSE;

			// Reinizializzo il pointer ad inizio bitmap
			pbmt = pbm + (nr_col_bmp - 1) - (NrColonneNere / 2);

			HalfByteHigh = !HalfByteHigh;

		} while( ContinueClear && (NrColonneNere < (nr_col_bmp * 2)) );

		if( NrColonneNere && (NrColonneNere < (nr_col_bmp * 2)) )
		{
			DiffWidth = Bitmap->biWidth;

			// Arrotondo il numero di colonne nere ad un nr. pari
			if( NrColonneNere % 2 )
				NrColonneNere --;

			// Aggiorno l'header della bitmap
			Bitmap->biWidth -= NrColonneNere;

			RealWidth = Bitmap->biWidth;

			// Calcolo la larghezza reale della bitmap
			if( CondDib = DiffWidth % 8 )
				DiffWidth += (8 - CondDib);

			if( CondDib = RealWidth % 8 )
				RealWidth += (8 - CondDib);

			DiffWidth -= RealWidth;

			Bitmap->biSizeImage = (Bitmap->biHeight * RealWidth) / 2;

			// Arrotondo al byte
			RealWidth /= 2;
			DiffWidth /= 2;

			//Controllo se shiftare la bitmap
			if( NrColonneNere )
			{
				pbm += RealWidth;
				pbmt = pbm;

				// Sposto la bitmap
				for( ii = 1; ii < (unsigned long)Bitmap->biHeight; ii ++)
				{
					pbmt += DiffWidth;
					for( jj = 0; jj < (unsigned long)RealWidth; jj ++)
						*(pbm++) = *(pbmt++);
				}
			}
		}

//	SalvaDIBBitmap(Bitmap, "g:\\Progetti\\bmp\\front4.bmp");

	}
	else
	{
		// Bitmap a 256 grigi

//	SalvaDIBBitmap(Bitmap, "e:\\bmp\\front.bmp");

		// Inizio dati bitmap
		pbm = (unsigned char *)Bitmap + sizeof(BITMAPINFOHEADER) + (256 * sizeof(RGBQUAD));
		nr_col_bmp = Bitmap->biWidth;
		if(CondDib = (nr_col_bmp % 4) )
			nr_col_bmp = (Bitmap->biWidth + 4 - CondDib);
		nr_row_bmp = Bitmap->biHeight;
		ContinueClear = TRUE;
		ValoriOk = 0;
		NrColonneNere = 0;

		// Tolgo le colonne nere davanti
		pbmt = pbm;
		do
		{
			for( ii = 0; ii < (unsigned long)nr_row_bmp; ii ++, pbmt += nr_col_bmp)
				if( *pbmt > Limite256Grigi )
					ValoriOk ++;

			//Controllo se eliminare la colonna
			if( ValoriOk <= NR_VALORI_OK )
			{
				NrColonneNere ++;
				ValoriOk = 0;
			}
			else
				ContinueClear = FALSE;

			// Reinizializzo il pointer ad inizio bitmap
			pbmt = pbm + NrColonneNere;

		} while( ContinueClear && (NrColonneNere < nr_col_bmp) );


		//Controllo se shiftare la bitmap
		if( NrColonneNere && (NrColonneNere < nr_col_bmp) )
		{
			// Inizializzo i pointer
			pbmt = pbmtm = pbm;
			NrByteCol = nr_col_bmp;

			// Calcolo il NUOVO numero di colonne e lo salvo
			nr_col_bmp -= NrColonneNere;
			Bitmap->biWidth = nr_col_bmp;

			if( CondDib = (nr_col_bmp % 4) )
				nr_col_bmp = (nr_col_bmp + 4 - CondDib);

			// Sposto la bitmap
			for( ii = 0; ii < (unsigned long)Bitmap->biHeight; ii ++)
			{
				pbmt += NrColonneNere;
				for( jj = NrColonneNere; jj < (unsigned long)NrByteCol; jj ++)
					*(pbmtm++) = *(pbmt++);

				// Aggiungo il nero
				for( jj = 0; jj < (unsigned long)(nr_col_bmp - Bitmap->biWidth); jj ++)
					*(pbmtm++) = 0x00;	//NERO;
			}

			// Aggiorno il nr. di bytes della bitmap
			Bitmap->biSizeImage = Bitmap->biHeight * nr_col_bmp;
		}

//	SalvaDIBBitmap(Bitmap, "g:\\Progetti\\bmp\\front3.bmp");


		// Inizio dati bitmap
		pbm = (unsigned char *)Bitmap + sizeof(BITMAPINFOHEADER) + (256 * sizeof(RGBQUAD));
		nr_col_bmp = Bitmap->biWidth;
		if(CondDib = (nr_col_bmp % 4) )
			nr_col_bmp = (Bitmap->biWidth + 4 - CondDib);
		nr_row_bmp = Bitmap->biHeight;
		pbmt = pbm + nr_col_bmp - 1;
		ContinueClear = TRUE;
		ValoriOk = 0;
		NrColonneNere = 0;

		// Tolgo le colonne nere dietro
		do
		{
			for( ii = 0; ii < (unsigned long)nr_row_bmp; ii ++, pbmt += nr_col_bmp)
				if( *pbmt > Limite256Grigi )
					ValoriOk ++;

			//Incremento il counter nr. colonne nere
			if( ValoriOk <= NR_VALORI_OK )
			{
				NrColonneNere ++;
				ValoriOk = 0;
			}
			else
				ContinueClear = FALSE;

			// Reinizializzo il pointer ad inizio bitmap
			pbmt = pbm + (nr_col_bmp - 1) - NrColonneNere;

		} while( ContinueClear && (NrColonneNere < nr_col_bmp) );

		if( NrColonneNere && (NrColonneNere < nr_col_bmp) )
		{
			DiffWidth = Bitmap->biWidth;

			// Aggiorno l'header della bitmap
			Bitmap->biWidth -= NrColonneNere;

			RealWidth = Bitmap->biWidth;

			// Calcolo la larghezza reale della bitmap
			if( CondDib = DiffWidth % 4 )
				DiffWidth += (4 - CondDib);

			if( CondDib = RealWidth % 4 )
				RealWidth += (4 - CondDib);

			DiffWidth -= RealWidth;

			Bitmap->biSizeImage = Bitmap->biHeight * RealWidth;


			//Controllo se shiftare la bitmap
			if( NrColonneNere )
			{
				pbm += RealWidth;
				pbmt = pbm;

				// Sposto la bitmap
				for( ii = 1; ii < (unsigned long)Bitmap->biHeight; ii ++)
				{
					pbmt += DiffWidth;
					for( jj = 0; jj < (unsigned long)RealWidth; jj ++)
						*(pbm++) = *(pbmt++);
				}
			}
		}

//	SalvaDIBBitmap(Bitmap, "g:\\Progetti\\bmp\\front4.bmp");

	} // Fine else

} // ClearFrontAndRearBlack




int Ls100Class::Periferica::ViewE13BSignal(int fView)
{
	char BufCodelineHW[256];
	short lenCodeline;
	long llDati;
	unsigned char pDati[32*1024];
	//int NrDoc;
	unsigned long NrDoc;
	short NrChar;
	short NrCampioni;
	short index;
	short Connessione ;

	 llDati = 32*1024;
	//Reply = LS100_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	 Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{

		// Setto il diagnostic mode
		//Reply = LS100_SetDiagnosticMode((HWND)hDlg, TRUE);
		LSSetDiagnosticMode(Connessione, (HWND)hDlg, TRUE);
		if (Reply == LS_OKAY) 
		{
			// Leggo il documento
			/*Reply = LS100_DocHandle((HWND)hDlg,
									SUSPENSIVE_MODE,
									NO_FRONT_STAMP,
									NO_PRINT_VALIDATE,
									READ_CODELINE_MICR,
									SIDE_NONE_IMAGE,
									SCAN_MODE_256GR200,
									AUTO_FEED,
									SORTER_BAY1,
									WAIT_YES,
									NO_BEEP,
									(unsigned long*)&NrDoc,
									0,
									0);*/
			Reply = LSDocHandle(Connessione, (HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								READ_CODELINE_MICR,
								SIDE_NONE_IMAGE,
								SCAN_MODE_256GR200 , 
								AUTO_FEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								&NrDoc,
								0, 0);

			if (Reply == LS_OKAY) 
			{
				// Leggo la codeline
				lenCodeline = 256;
				/*Reply = LS100_ReadCodeline((HWND)hDlg,
										SUSPENSIVE_MODE, 
										BufCodelineHW,
										(short *)&len_codeline,
										NULL,
										0);*/
				Reply = LSReadCodeline(Connessione, (HWND)hDlg, BufCodelineHW, &lenCodeline, NULL, NULL, NULL, NULL);
				
				//Reply = LS100_ReadE13BSignal((HWND)hDlg, pDati, (long *)&llDati);
				Reply = LSReadE13BSignal(Connessione, (HWND)hDlg, pDati, &llDati);
				if (Reply == LS_OKAY)
				{
					// salvo nella variabile pubblica i dati 


					if (fView)
					{
						NrChar = 0;
						while( (pDati[NrChar]) != E13B_FINE_LLC )
							NrChar ++;
						NrCampioni =(((pDati[NrChar + 2])<<8) + pDati[NrChar + 1]);
						index = NrChar + 1 ;		// Vado ad inizio dati segnale
					}
					else
					{
						NrCampioni	= (short)(llDati-2);
						index = 0;
					}
					
					for ( long ii = 0 ; ii<NrCampioni;ii++)
					{
						E13BSignal[ii] = pDati[ii+index];
					}
				}
				// Tolgo il diagnostic mode
				//LS100_SetDiagnosticMode((HWND)hDlg, FALSE);
				LSSetDiagnosticMode(Connessione, (HWND)hDlg, FALSE);

				//LS100_Close((HWND)hDlg, SUSPENSIVE_MODE);
				LSDisconnect(Connessione,(HWND)hDlg);
			}


		}
	}
	
	return Reply ;
}
int Ls100Class::Periferica::DisegnaEstrectLenBar()
{
		
//	unsigned char *pd;
	int  xx;//, yy;
	short ii, Save_ii;
	BOOL fFine;
	short NrLenghtBar = 7;				// Nr. raggruppamento barre
	short NrCampioni;
	short NrChar, NrValori;
	short NrBarre;
	//unsigned char *pbt, *pbt2;			// Ptr. temp a bitmap
	short Save_pdct;	// Ptr. temp a caratteri
	//unsigned char *pdct2;				// Ptr. temp a caratteri
	
	short index,index2;
//	FILE *fh;
//	char *ptmp;
//	char FileOut[_MAX_FNAME];

	


	// Apro il file di save caratteri
//	ptmp = strrchr(szFileLenghtBar, '.');
//	ptmp -= 4;		// punto a nr. file
//	sprintf(FileOut, ".\\ValoriPer_%d.txt", NrLenghtBar);
//	fh = fopen(FileOut, "w");

	// Disegno grafico
	// Conto i bytes larghezza char
	NrChar = 0;
	while( (E13BSignal[NrChar]) != E13B_FINE_LLC )
		NrChar ++;
	NrCampioni =(E13BSignal[NrChar +1]);
	NrCampioni +=(E13BSignal[NrChar +2])<<8;
	//NrCampioni 	+=(E13BSignal[NrChar +2]);
	index = NrChar + 3;		// Vado ad inizio dati segnale
	index = index + (DIM_DESCR_CHAR * NrChar);	// Vado ad inizio dati segnale

	// Da lasciare o da togliere ? Bah !!!   Solo il destino lo dir� !
	NrCampioni -= (DIM_DESCR_CHAR * NrChar);

	NrCampioni -=2;
	// Cerco l'ultimo valore valido
	index2 = index + NrCampioni;
	while( E13BSignal[index2] < SOGLIA_INIZIO_NERO )
	{
		index2 --;
		NrCampioni --;
	}
	// Ne tengo uno dei non validi come tappo
	NrCampioni ++;

	for( ii = 0; ii < 3; ii++)
	{
		// Elimino la prima mezza barra, un'eventuale parte nera ...
		while( E13BSignal[index2] < SOGLIA_INIZIO_NERO )
		{
			index2 --;
			NrCampioni --;
		}

		// ... e la parte bianca, cos� parto sempre da un nero
		while( E13BSignal[index2] >= SOGLIA_INIZIO_NERO )
		{
			index2 ++;
			NrCampioni --;
		}
	}
	// Elimino tre barre iniziali
	for( ii = 0; ii < 3; ii++)
	{
		// Elimino la prima mezza barra, un'eventuale parte nera ...
		while( E13BSignal[index] < SOGLIA_INIZIO_NERO )
		{
			index ++;
			NrCampioni --;
		}
		// ... e la parte bianca, cos� parto sempre da un nero
		while( E13BSignal[index] >= SOGLIA_INIZIO_NERO )
		{
			index ++;
			NrCampioni --;
		}

	}

	// Conto i valori di una oscillazione
	
	fFine = FALSE;
	NrValori = 0;
	NrBarre = 1;
	ValMax = 0;
	ValMin = NrLenghtBar * 0xff;	// setto il massimo
	xx = 0;
	for( ii = 1; ii <= NrCampioni; ii ++)
	{
		//Sono arrivato alla fine di una barra ?
		//if( (fFine == TRUE ) && ((*pdct < SOGLIA_INIZIO_NERO) && (*(pdct + 1) < SOGLIA_INIZIO_NERO)) )
		// 29-7-2008 
		if( (fFine == TRUE ) && (E13BSignal[index] < SOGLIA_INIZIO_NERO) && (E13BSignal[index+1] < SOGLIA_INIZIO_NERO))
		{
			if( NrBarre == 1 )
			{
				Save_pdct = index;
				Save_ii = ii;
			}

			if( NrBarre < NrLenghtBar )
				NrBarre ++;
			else
			{
				xx++;

				// Se il valore esce dal grafico ... lo forzo al valore massimo
				if( NrValori > (HEIGHT_BAR * NrLenghtBar) )
					NrValori = (HEIGHT_BAR * NrLenghtBar) - 1;

				// Setto il pixel
//				*(pbt + xx + ((NrValori + OFFSET_SCALA_X) * Width) + OFFSET_SCALE_Y) = Colore;

				// Prima di settare il pixel controllo se sono fuori dalla memoria bitmap
				// Se sono dentro l'area scrivo il valore, altrimenti no !
				
				


//				fprintf(fh, "%d\n", NrValori);

				// Salvo il minimo ed il massimo
				if( NrValori < ValMin )
					ValMin = NrValori;
				if( NrValori > ValMax )
					ValMax = NrValori;

				// Riazzero le variabili
				NrValori = 0;
				NrBarre = 1;
				index = Save_pdct;
				ii = Save_ii;
			}

			fFine = FALSE;
		}

		// Il nero lo somma per FALSE, il bianco per TRUE
		if( (fFine == FALSE) && (E13BSignal[index] >= SOGLIA_INIZIO_NERO) && (E13BSignal[index+1] >= SOGLIA_INIZIO_NERO) )
			fFine = TRUE;

		// Sposto il puntatore
		index++;
		NrValori ++;
	}

//	if( fh )
//		fclose( fh );
return 1;
}

//int Ls100Class::Periferica::Docutest04()
//{
//	unsigned long NrDoc;
//	ReturnC->FunzioneChiamante = "Docutest04";
//	Reply = LS100_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
//	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
//	{
//		Reply = LS100_DoubleLeafingSensibility((HWND)hDlg,DOUBLE_LEAFING_DEFAULT);
//		Reply = LS100_DocHandle((HWND)hDlg,SUSPENSIVE_MODE,NO_FRONT_STAMP,NO_PRINT_VALIDATE,
//						NO_READ_CODELINE,SIDE_NONE_IMAGE,SCAN_MODE_256GR200,
//						AUTO_FEED,SORTER_BAY1,WAIT_YES,NO_BEEP,&NrDoc,0,0);
//	
//		Reply = LS100_DoubleLeafingSensibility((HWND)hDlg,DOUBLE_LEAFING_DISABLE);
//		LS100_Close((HWND)hDlg, SUSPENSIVE_MODE);
//	}
//	else
//	{
//		ReturnC->FunzioneChiamante = "LS100_Open";
//	}
//	return Reply;	
//}

int Ls100Class::Periferica::Docutest04()
{
	short Connessione;
	unsigned long NrDoc;


	ReturnC->FunzioneChiamante = "Test Doppia sfogliatura";
	

	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	if( Reply == LS_OKAY )
	{
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_ERROR,50,100,215);
		
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								SIDE_NONE_IMAGE,
								SCAN_MODE_256GR200,
								AUTO_FEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								&NrDoc,
								SCAN_PAPER_DOCUMENT,
								0);
		
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,150,215);
		LSDisconnect(Connessione,(HWND)hDlg);
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}

	ReturnC->ReturnCode = Reply;
	

	return Reply;	//ret
}

int Ls100Class::Periferica::Reset(short TypeReet)
{
	short Connessione;
//	int ret;
//	unsigned long NrDoc;
	//ReturnC->FunzioneChiamante = "Reset";
	//Reply = LS100_OpenDiagnostica((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	//if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	if( Reply == LS_OKAY )
	{
		//Reply = LS100_ResetEx((HWND)hDlg,SUSPENSIVE_MODE,(char)TypeReet);
		Reply = LSReset(Connessione,(HWND)0,(char)TypeReet);

		//LS100_Close((HWND)hDlg, SUSPENSIVE_MODE);
		LSDisconnect(Connessione,(HWND)hDlg);

	}

	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Reset";

	//else
	//{
	//	ReturnC->FunzioneChiamante = "LS100_Open";
	//}
	return Reply;	

}


int Ls100Class::Periferica::DoTimeSampleMICRCalibration(int nouse1, int nouse2)
{
	int		Reply;
	BOOL	nRet;
	float	DiffPercentuale;
	unsigned short	NewValue, OldValue; //CurrValue
	long valMedio;
	short	nrC, nrS;
	char TypeSpeed;
	long nColorData ;
	unsigned char	*pDati;
	unsigned char buffDati[NR_BYTE_BUFFER_E13B];		// Dati da chiedere
	long   llDati; 					// Dati da chiedere
	unsigned long NrDoc;
	short Connessione ; 
	
	// Setto i valori iniziali
	nRet = TRUE;
	Percentile = 0;
	nColorData = 256;
	ReturnC->FunzioneChiamante = "DoTimeSampleCalibration";
	TypeSpeed = 1;

	//Ad oggi gestito solo questo !
	TypeLS = TYPE_LS100_7 ; 
	
	//-----------Open--------------------------------------------
	//Reply = LS100_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	if ((Reply != LS_OKAY)&& (Reply != LS_ALREADY_OPEN))
	{
		return Reply;
	}
	else
	{
		//Reply = LS100_Reset((HWND)hDlg,SUSPENSIVE_MODE);
		//----------- Set attesa introduzione documento -----------
		Reply = LSDisableWaitDocument(Connessione, (HWND)hDlg, FALSE);

		//Reply = LS100_DoubleLeafingSensibility((HWND)hDlg, DOUBLE_LEAFING_DISABLE);
		Reply = LSDoubleLeafingSensibility(Connessione, (HWND)hDlg, 0, DOUBLE_LEAFING_DISABLE);
	
		// Setto il diagnostic mode
		//Reply = LS100_SetDiagnosticMode((HWND)hDlg, SPEED_GRAY);//TypeSpeed);
		Reply = LSSetDiagnosticMode(Connessione, (HWND)hDlg, TypeSpeed);

		if( Reply == LS_OKAY )
		{
			// Chiamo la funzione passando l'ultima configurazione settata
			/*Reply = LS100_DocHandle((HWND)hDlg,
									SUSPENSIVE_MODE,
									NO_FRONT_STAMP,
									NO_PRINT_VALIDATE,
									READ_CODELINE_MICR,
									SIDE_NONE_IMAGE,
									SCAN_MODE_256GR200,
									AUTO_FEED,
									SORTER_BAY1,
									WAIT_YES,
									NO_BEEP,
									&NrDoc,
									0,
									0);*/
			// Chiamo la funzione passando l'ultima configurazione settata
			Reply = LSDocHandle(Connessione, (HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								READ_CODELINE_MICR,
								SIDE_NONE_IMAGE,
								SCAN_MODE_256GR200 , 
								AUTO_FEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								&NrDoc,
								0, 0);


			if( Reply == LS_OKAY )
			{
				llDati = NR_BYTE_BUFFER_E13B;
				//Reply = LS100_ReadE13BSignal((HWND)hDlg, buffDati, (long *)&llDati);
				Reply = LSReadE13BSignal(Connessione, (HWND)hDlg, buffDati, &llDati);
				if( Reply != LS_OKAY )
				{
					

					//LS100_SetDiagnosticMode((HWND)hDlg, FALSE);
					//LS100_Close((HWND)hDlg, SUSPENSIVE_MODE);		// Faccio la close
					LSSetDiagnosticMode(Connessione, (HWND)hDlg, FALSE);
					LSDisconnect(Connessione, (HWND)hDlg);		// Faccio la close
					NrLettureOk = 0;
					return Reply;
				}

				// salvo nella variabile pubblica i dati 
				for ( long ii = 0 ; ii<llDati;ii++)
				{
					E13BSignal[ii] = buffDati[ii];
				}
				
				// Visualizzo la bitmap del segnale letto
				// Conto i bytes larghezza char
				nrC = 0;
				pDati = buffDati;
				while( *(pDati + nrC) != E13B_FINE_LLC )
					nrC ++;

				// Controllo se ho trovato il fine lunghezza caratteri
				if( nrC != llDati )
				{
					// Estraggo il numero di segnali
					nrS = *(short *)(pDati + nrC + 1);		// 1 per il CR
					nrS -= 2;	// Il valore comprende anche i 2 bytes di nr. campioni segnale
					DisegnaEstrectLenBar();
					llMax = ValMax;
					llMin = ValMin;
				} // end if( nrC == llDati )

				
				

				// Controllo differenza minimo massimo
				if( (llMax - llMin) > 11 )
				{
	//				MessageBox(hDlg, "Bad assembly !", TITLE_ERROR, MB_ICONERROR | MB_OK);
					//LS100_SetDiagnosticMode((HWND)hDlg, FALSE);
					//LS100_Close((HWND)hDlg, SUSPENSIVE_MODE);
					LSSetDiagnosticMode(Connessione, (HWND)hDlg, FALSE);
					LSDisconnect(Connessione, (HWND)hDlg);		// Faccio la close

					return LS_CALIBRATION_FAILED;
				}


				OldValue = 0;
				// Leggo il valore settato nella periferica
				//Reply = LS100_ReadTimeSampleMICR((HWND)hDlg, TypeSpeed, (unsigned char *)&OldValue, 1);
				Reply = LSReadTimeSampleMICR(Connessione, (HWND)hDlg, TypeSpeed, &OldValue, 1);

				if( Reply != LS_OKAY )
				{
					nRet = Reply;
					//LS100_SetDiagnosticMode((HWND)hDlg, FALSE);
					//LS100_Close((HWND)hDlg, SUSPENSIVE_MODE);
					LSSetDiagnosticMode(Connessione, (HWND)hDlg, FALSE);
					LSDisconnect(Connessione, (HWND) hDlg);		// Faccio la close
					return Reply;
				}


				// Controllo il valore massimo
				if( llMax > SAMPLE_VALUE_MAX )
				{
					// Calcolo il valore in percentuale da sommare a quello presente nella perifeica
					DiffPercentuale = (llMax * OldValue) / SAMPLE_VALUE_MAX;
					if( (DiffPercentuale - (long)DiffPercentuale) >= 0.3 )
						NewValue = (unsigned short)(DiffPercentuale + 1);
					else
						NewValue = (unsigned short)DiffPercentuale;

					// Aggiorno il valore di letture da fare
					NrLettureOk = 0;
					
					nRet = FALSE;	// Devo riprovare la calibrazione
				}


				else if(	llMin < SAMPLE_VALUE_MIN )
				{
					// Calcolo il valore in percentuale da sommare a quello presente nella perifeica
					DiffPercentuale = (llMin * OldValue) / SAMPLE_VALUE_MIN;
					if( (DiffPercentuale - (long)DiffPercentuale) >= 0.5 )
						NewValue = (unsigned short)(DiffPercentuale + 1);
					else
						NewValue = (unsigned short)DiffPercentuale;
					NrLettureOk = 0;				
					nRet = FALSE;	// Devo riprovare la calibrazione
				}

				// Serve per alzare il valore che sta nel range !!!
				// Boh !
				else if(	(llMax <= SAMPLE_VALUE_MAX) && (llMin >= (SAMPLE_VALUE_MIN )) )
				{
					valMedio = (llMax + llMin) / 2;
					// Sommo dei valori a seconda della distanza
					if( (valMedio - SAMPLE_VALUE_MEDIO) > 1 )

						NewValue = OldValue - 1;
					else if( (valMedio - SAMPLE_VALUE_MEDIO) < -1 )
						NewValue = OldValue + 1;
					else
						NewValue = OldValue;

					// Aggiorno il valore di letture da fare
					NrLettureOk ++;
					nRet = FALSE;	// Devo riprovare la calibrazione
				}


				// Se nRet = FALSE ho variato i valori, perci� vado a scrivere quelli nuovi
				if( nRet == FALSE )
				{
					// Controllo che non sia inferiore a default - 10%
					if( TypeLS == TYPE_LS100_ETH || TypeLS == TYPE_LS100_7 || TypeLS == TYPE_LS100_8 )
					{
						// Ls100E ritorna il valore moltiplicato per 4
						if( NewValue < (168 * 0.90) )
							NewValue = (unsigned short)(168 * 0.90);	// forzo il minimo

						if( NewValue > (168 * 1.10) )
							NewValue = (unsigned short)(168 * 1.10);	// forzo il massimo
					}
					else
					{
						if( NewValue < (42 * 0.90) )
							NewValue = (unsigned short)(42 * 0.90);	// forzo il minimo

						if( NewValue > (42 * 1.10) )
							NewValue = (unsigned short)(42 * 1.10);	// forzo il massimo
					}

					//Reply = LS100_SetTimeSampleMICR((HWND)hDlg, TypeSpeed, NewValue);
					Reply = LSSetTimeSampleMICR(Connessione, (HWND)hDlg, TypeSpeed, NewValue);
		
					if( Reply != LS_OKAY )
					{
						nRet = TRUE;
					}
				}
			}
			else
			{
				nRet = Reply;
			}

			// Tolgo il diagnostic mode
			//LS100_SetDiagnosticMode((HWND)hDlg, FALSE);
			LSSetDiagnosticMode(Connessione, (HWND)hDlg, FALSE);
		}
	}
   // LS100_Close((HWND)hDlg, SUSPENSIVE_MODE);
	LSDisconnect(Connessione, (HWND)hDlg);

	return nRet;
} // DoTimeSampleMICRCalibration





int Ls100Class::Periferica::EraseHistory()
{
	short Connessione;

	//S_HISTORY History;
	ReturnC->FunzioneChiamante = "EraseHistory";
	//Reply = LS100_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	//if( Reply == LS_OKAY )
	if( Reply == LS_OKAY )
	{
		//Reply = LS100_HistoryCommand((HWND)hDlg,CMD_READ_HISTORY, &History);
		//if( Reply == LS_OKAY )
		//{	
			/*S_Storico->Documenti_Trattati = History.doc_sorted;
			S_Storico->Documenti_Trattenuti = History.doc_retain;
			S_Storico->Errori_CMC7 = History.doc_cmc7_err;
			S_Storico->Errori_E13B = History.doc_e13b_err;
			S_Storico->Jam_Feeder = History.bourrage_feeder;
			S_Storico->Jam_Micr = History.bourrage_micr;
			S_Storico->Jam_Sorter = History.bourrage_exit;
			S_Storico->Num_Accensioni = History.num_turn_on;
			S_Storico->Doc_Timbrati = History.doc_stamp;
			S_Storico->TempoAccensione = History.time_peripheral_on;
			S_Storico->Documenti_Trattati = History.doc_sorted;*/
			
		
			//Reply = LS100_HistoryCommand((HWND)hDlg, CMD_ERASE_HISTORY, NULL);
			Reply = LSHistoryCommand(Connessione,(HWND)hDlg, CMD_ERASE_HISTORY, NULL);
			if( Reply != LS_OKAY )
			{
				ReturnC->FunzioneChiamante = "LSHistoryCommand";
			}

			//LS100_Close((HWND)hDlg, SUSPENSIVE_MODE);
			LSDisconnect(Connessione,(HWND)hDlg);
		//}
		//else
		//{	
		//	CheckReply(0, Reply, "LS100_HistoryCommand");
		//}
	}
	else
	{
		CheckReply(0, Reply, "LSOpen");
		
	}

	return Reply;

	
}

//09-02-2016 ->Calibrazione Nuovo Sensore
int Ls100Class::Periferica::InkDetectorCalibration(bool fPhotoCalibration , int DocValue , short step )
{
	short Connessione;
	//short	hLS;
	char	ParIni[128];

	ReturnC->FunzioneChiamante = "InkDetectorCalibration";

	memset(ParIni, 0, sizeof(ParIni));
	
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	if( Reply == LS_OKAY )
	{
		if (fPhotoCalibration == TRUE )
		{
			Reply = LSPhotosCalibration(Connessione, (HWND)0, LS100_CALIBRATION_INK_DETECTOR_SENSOR);
		}
		
		
		if( Reply == LS_OKAY )
		{	

			Reply = LSInkDetectorCalibration(Connessione, step, DocValue, ParIni);  //INKD_CALIBRATION_STEP_1
			
			if( Reply != LS_OKAY )
			{
				ReturnC->FunzioneChiamante = "InkDetectorCalibration";
			}

			LSDisconnect(Connessione,(HWND)hDlg);
		}
		else
		{

			CheckReply(0, Reply, "InkDetectorCalibration");
		}
	}
	else
	{
		CheckReply(0, Reply, "InkDetectorCalibration");
		
	}
	
	ReturnC->ReturnCode = Reply ; 
	return Reply;

	
}



int Ls100Class::Periferica::TestDownload()
{
	//short ret;
	short Connessione=0;
	char *StrNome = NULL ; 

	ReturnC->FunzioneChiamante = "TestDownload";
	
	//-----------Open--------------------------------------------
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione);
	
	if (Reply == LS_OKAY )
	{
		StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileInkDetector ).ToPointer();
		Reply =  LSDownloadFirmware(Connessione , 
									   (HWND)hDlg,
									   StrNome , //char		*FileFw,
									   NULL ) ; //int		(__stdcall *userfunc1)(char *Item));
		
		ReturnC->FunzioneChiamante =  "LSDownloadFirmware";
	}
	else
	{
		ReturnC->FunzioneChiamante =  "TestDownload";
	}
	

	//-----------Close-------------------------------------------
	LSDisconnect(Connessione,(HWND)hDlg);
	ReturnC->ReturnCode = Reply;
	
	return Reply;


}

int Ls100Class::Periferica::InkDetectorLoadCmds()
{
	short Connessione;
	char *StrNome=NULL;
	unsigned char pBuff[2] ;


	ReturnC->FunzioneChiamante = "LSInkDetectorLoadCmds";

	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	if( Reply == LS_OKAY )
	{
		//Inizializzazione SPT con comando SptInit , 0x00
		memset(pBuff , 0x00 , sizeof(pBuff));
		pBuff[0] = 0x4e;
		pBuff[1] = 0x00 ;
		Reply =LSInkDetectionSendCmd(Connessione, pBuff,2);
		memset(pBuff , 0x00 , sizeof(pBuff));
		Reply =LSInkDetectionReadBytes(Connessione,pBuff, 1, 500 ) ;
		memset(pBuff , 0x00 , sizeof(pBuff));
		pBuff[0] = 0xaa ; 
		Reply =LSInkDetectionSendCmd(Connessione, pBuff,2);


		
		StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileSPT ).ToPointer();
		Reply = LSInkDetectorLoadCmds(Connessione,StrNome );
	}
	else
	{
		CheckReply(0, Reply, "LSInkDetectorLoadCmds");
		
	}
	
	ReturnC->ReturnCode = Reply ; 
	return Reply;

	
}




int Ls100Class::Periferica::CollaudoLSConnect(HWND hDlg,HANDLE hInst,short TipoPeriferica,short *Connessione)
{
	int Reply;


	do
	{
		Reply = LSConnectDiagnostica((HWND)0,(HANDLE)hInst,TipoPeriferica,Connessione);

		// Siccome MANCANO un sacco di connect, in caso di LS_ALREADY_OPEN
		// Mi disconnetto e mi riconnetto fino a reply == LS_OKAY
		// per risolvere problemi di applicativo collaudo !!!
		if( Reply == LS_ALREADY_OPEN )
		{
			int rc;
			rc = LSDisconnect(*Connessione, (HWND)0);	// Per la connect di adesso
			rc = LSDisconnect(*Connessione, (HWND)0);	// Per la connect che c'era !!
		}

	} while( Reply == LS_ALREADY_OPEN );


	// Connect alla lsapi
//	Reply = LSConnectDiagnostica((HWND)0,(HANDLE)hInst,TipoPeriferica,Connessione);
	//if(( Reply == LS_OKAY )||( Reply == LS100_TRY_TO_RESET )||( Reply == LS_ALREADY_OPEN ))
	if(( Reply == LS_OKAY )||( Reply == LS_TRY_TO_RESET )||( Reply == LS_ALREADY_OPEN ))
	//if( Reply == LS_TRY_TO_RESET)
	{
		Reply = LSReset(*Connessione,(HWND)0,RESET_ERROR);

		if( Reply != LS_OKAY )
			Reply = LSReset(*Connessione,(HWND)0,RESET_FREE_PATH);
	}
	//else if (Reply == LS_ALREADY_OPEN ) 
	//	Reply = LS_OKAY ;


	return Reply;
}

