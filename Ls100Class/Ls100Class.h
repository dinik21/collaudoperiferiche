// Ls100Class.h
#include <windows.h>
#include "ErrWar.h"
#include "Constanti.h"

#pragma once

using namespace System;
using namespace System::IO;
using namespace System::Runtime::InteropServices;
using namespace System::Runtime::Serialization;
using namespace System::Reflection;
using namespace System::Windows::Forms;

namespace Ls100Class {


	public ref class Storico
	{
	public:
		// TODO: aggiungere qui i metodi per la classe.
		int Documenti_Catturati;
		int Documenti_Ingresso;
		int Documenti_Trattati;
		int Documenti_Trattenuti;
		int Doppia_Sfogliatura;
		int Errori_Barcode;
		int Errori_CMC7;
		int Errori_E13B;
		int Errori_Ottici;
		int Jam_Allinea;
		int Jam_Card;
		int Jam_Cassetto1;
		int Jam_Cassetto2;
		int Jam_Cassetto3;
		int Jam_Feeder;
		int Jam_Feeder_Ret;
		int Jam_Micr;
		int Jam_Micr_Ret;
		int Jam_Percorso_DX;
		int Jam_Percorso_SX;
		int Jam_Print;
		int Jam_Print_Ret;
		int Jam_Scanner;
		int Jam_Scanner_Ret;
		int Jam_Stamp;
		int Jam_Sorter;
		int Num_Accensioni;
		int Doc_Timbrati;
		int Doc_Timbrati_retro;
		int Trattenuti_Micr;
		int Trattenuti_Scan;
		int TempoAccensione;
	};


	public ref class Periferica
	{
	private:
			short GetTypeLS(String ^Version);
			int CheckReply(HWND hDlg,int Rep,char *s);
/*			
			void EstrazioneDatiPhoto(unsigned char *pd);
			void EstrazioneDatiPhotoSfogliatore(unsigned char *pd);
			
			float CalcoloUnValoreRiferimento(HWND hDlg, unsigned char *pDati, short llDati,
									float MICR_Valore100, float *TotValRif, float *MinValue);
			unsigned char Calc_Trimmer_Position(float ValoreLetto, unsigned char PosTrimmer);
			int EstraiDatiSpeedE13B(unsigned char *pd, long llDati,
							short *NrPicchi, float *SommaValori, long *ValMin, short *NrPicchiMin);
*/			
			short MICR_TolleranceMore_3;
			short MICR_TolleranceLess_3;
			float MICR_Valore100_3 ;
			short	nrLettura;
			int MICR_SignalValue_3;


	public:
			// valori fotosensori doppia SF
			static cli::array<Byte> ^ PhotoValueDP = gcnew cli::array<Byte>(16);
			
			
			ErrWar ^  ReturnC ;
			Storico ^S_Storico;


			String ^  VersioneDll_0;
			String ^ VersioneDll_1;

			String ^  CodelineLetta;
			String ^  BadgeLetto;
			
			// stringhe ritornate dall identificativo
			String ^  SIdentStr;
			String ^  SModel;
			String ^  SDateFW;
			String ^  SVersion;
			String ^  SLema;
			String ^  SInkJet;
			String ^  SSerialNumber;
			String ^  SBoard;
			String ^  SFPGANr;
			


			String ^  SStampa;

			String ^  FileNameDownload;
			String ^  NomeFileImmagine;
			String ^  NomeFileSPT ; 
			String ^  NomeFileInkDetector; 
			
			// Reply delle funzioni
			short NrLettureOk;
			int Reply;
			static int hDlg;
			static int hInst;
			static HANDLE IDPort;
			// tipo di periferica
			short TypeLS;
			// MICR lettura
			float Percentile;
			short ValMax,ValMin;

			long llMax, llMin;
			float llMedia;
			double DevStd;

			int ValMinCinghia;
			int ValMaxCinghia;
			long NumDati;
			
			/*
			Questa variabile la uso SOLAMENTE nella leggiCodeline e immagini,
			dalle altre parte uso solo NomeFileImmagine
			*/
			String ^  NomeFileImmagineRetro;
			static cli::array<int> ^ PhotoValue = gcnew cli::array<int>(8);
		
			static cli::array<Byte> ^ E13BSignal  = gcnew cli::array<Byte>(16000);
			
			static cli::array<Byte> ^ ViewSignal  = gcnew cli::array<Byte>(10000);

			short TipoPeriferica;
			short Connessione;
			


		Periferica();
		int Identificativo();
		
		// legge le codeline OCR
		int LeggiCodelineOCR(short Type);
		// Calibra i fotosensori e ritorna i valori
		int CalibrazioneFoto();
		int CalSfogliatura();


		//int DoMICRCalibration3(int MICR_SignalValue,long NrReadGood,  long TypeSpeed);
		int DoMICRCalibration50Barrette( int MICR_SignalValue,long NrReadGood, long TypeSpeed,ListBox^ list);
		float CalcoloUnValoreRiferimento(unsigned char *pDati, 
								float MICR_Valore100, float *TotValRif, float *MinValue);
		unsigned char Calc_Trimmer_Position(float ValoreLetto, unsigned char PosTrimmer);
		int Media50Barrette(unsigned char *pd, long llDati,
						 short *NrPicchi, float *SommaValori, long *ValMin, short *NrPicchiMin);

		int CalibraScanner(int Side);
		int LeggiImmagini(char Side);
		int LeggiCodeline(short DocHandling);
		int EraseHistory();
		int DoTimeSampleMICRCalibration(int nouse1,int nouse2);
		int Reset(short TypeReet);
		int Docutest04();
		int DisegnaEstrectLenBar();
		int ViewE13BSignal(int fView);
		//void ClearFrontAndRearBlack(BITMAPINFOHEADER *Bitmap, short *ColNereAnt);
		int DoImageCalibration(HWND hDlg, float TeoricValue,short );
		int CalibraImmagine();
		int SetConfigurazione(int E13B,int CMC7,int timbro,int badge12,int badge23,int badge123,int scannerfronte, int scannerretro,int sfogliatore,int SwAbilitato,int InkDetector);
		int TestCinghia();
		int SerialNumber();
		int LeggiBadge(short traccia);
		int TestStampaAlto(short Side);
		int TestStampa(short Side);
		int Timbra(short DocHandling);
		int LeggiCodelineImmagini(short DocHandling,short Side,short Doppia);
		int GetVersione();
		int InkDetectorCalibration(bool fPhotoCalibration , int Value , short step );
		int InkDetectorLoadCmds();
		//da qui funzioni che integrano gia' LASPI
		int CollaudoLSConnect(HWND hDlg,HANDLE hInst,short TipoPeriferica,short *Connessione);
		int TestDownload();
		//int TestBackground(int  fZebrato);



	};
}

