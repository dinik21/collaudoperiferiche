//
// ARCA Technologies s.r.l.
// Corso Vercelli, 332 - 10015 Ivrea (TORINO) Italy
// January 2001
//
// Phone ++39 125 235611
// Telefax ++39 125 235623
//
// www.arca.com		techsupp.it@arca.com
//
//
// All rights reserved
//
// We reserve the right to vary technical specification
//
//  MODULE:   LSAPI_CALIBR.H
//
//  PURPOSE:  LS Include Interface


#ifndef LSAPI_CALIBR_H
#define LSAPI_CALIBR_H     1


// Defines maschere Software
#define MASK_TOP_IMAGE					0x01
	

#define PRINT_FORMAT_HEAD_TEST			0x20	// Stampa una predefinita riga di test


// Scanner choise
#define LS150_SCANNER_FRONT				0
#define LS150_SCANNER_REAR				1

#define LS40_SCANNER_GRAY				0
#define LS40_SCANNER_UV					2
#define LS40_SCANNER_UVGR				3
#define LS40_SCANNER_COLOR				4

#define LS40_CALIBR_INSERT_DOC_069		0
#define LS40_CALIBR_INSERT_DOC_055		1


#define SIDE_FRONT_RGB_IMAGE			'f'
#define SIDE_REAR_RGB_IMAGE				'r'


#define LS515_LED_ON					0x02
#define LS515_LED_BADGE					0x01
#define LS515_LED_PROCESS				0x04


#define LS150_ELECTROMAGNET_POCKET		1
#define LS150_ELECTROMAGNET_LEAFER		2
#define LS150_ELECTROMAGNET_INKJET		3
#define LS150_ELECTROMAGNET_STAMP		4


#define LS515_ELECTROMAGNET_RELEASE		0
#define LS515_ELECTROMAGNET_FEEDER		1
#define LS515_ELECTROMAGNET_LINEAR		2
#define LS515_ELECTROMAGNET_STAMP		3
#define LS515_ELECTROMAGNET_POCKET		4


// Parametri per calibrazione foto
#define LS150_CALIBRATION_PHOTO_PATH				0
#define LS150_CALIBRATION_PHOTO_DOUBLE_LEAFING		1
#define LS150_CALIBRATION_VERIFY_DOUBLE_LEAFING		2
#define LS100_CALIBRATION_INK_DETECTOR_SENSOR		3

#define LS150_READ_DATA_DOUBLE_LEAFING		0x01	// Lettura dati sensori Doppia Sfogliatura
#define LS150_READ_DATA_VERIFICATION_DL		0x02	// Lettura dati di verifica sensori Doppia Sfogliatura

// Nr. PWM Sensor Double Leafing
#define LS150_NR_PWM_DOUBLE_LEAFING			3

// Defines for Ink Detector sensor
#define INKD_CALIBRATION_STEP_1				1	// Preparazione alla calibrazione
#define INKD_CALIBRATION_STEP_2				2	// Verifica valori di riferimento
#define INKD_CALIBRATION_STEP_3				3	// Calibrazione sensore
#define INKD_CALIBRATION_STEP_4				4	// Verifica finale e Salvataggio dati

#define INKD_FILE_INI						"InkDetector.ini"
#define INKD_FILES_SECTION					"Files"
#define INKD_CALIBRATION_SECTION			"Calibration"
#define INKD_DOC_SECTION					"DocStats"

#define INKD_REPLY_PowerFault				0x14
#define INKD_REPLY_SptWriteFault			0x15
#define INKD_REPLY_SptChecksumError			0x16
#define INKD_REPLY_DownloadError			0x19
#define INKD_REPLY_FirmwareCrcError			0x1a
#define INKD_REPLY_DarkRefError				0x20
#define INKD_REPLY_DarkFluError				0x21
#define INKD_REPLY_NoiseError				0x22
#define INKD_REPLY_CalibrationError			0x23
#define INKD_REPLY_SpiTimeout				0x30
#define INKD_REPLY_SyntaxError				0x31
#define INKD_REPLY_Ready					0x40
#define INKD_REPLY_Busy						0x41



// Tabella per lettura dati PWM e AD della periferica
enum
{
	DATA_SCANNER_GRAY,
	DATA_SCANNER_BLUE,
	DATA_SCANNER_GREEN,
	DATA_SCANNER_RED,
	DATA_SCANNER_UV_LOW_CONTRAST,
	DATA_SCANNER_UV_HIGH_CONTRAST,
	DATA_SCANNER_GRAY_AND_UV,
};


typedef struct _BUILDERPARAM
{
	BOOL	Beep;						// Passo il Beep per LS100, LS150
	BOOL	StopLoopOnErrorInCodeline;	// Stop loop di DocHandle su erroe in codeline
	BOOL	SaveE13BSignal;				// Save E13B Signal
	BOOL	fApplDoCheckCodeline;		// L'applicativo segnala errore in codeline
	char	CheckCodeline[CODE_LINE_LENGTH];
	BOOL	fViewUVbackground;			// Non cancella il background dell'immagine UV
	BOOL	f100dpiFromUnit;			// Filma con la risoluzione a 100 dpi, esclude la conversione da 200
	BOOL	fDebugImageColorUV;			// Se 1 Ls150 UV filma l'immagine a colori completa anziche' il modalita' Bayer
} BUILDERPARAM, *LPBUILDERPARAM;




// ------------------------------------------------------------------------
//                 REPLY-CODE ERROR FOR CALIBRATION FUNCTION
// ------------------------------------------------------------------------
#define LS_MICR_TRIMMER_VALUE_NEGATIVE	-300
#define LS_ERROR_OFFSET_FRONT			-348
#define LS_ERROR_OFFSET_REAR			-349
#define LS_ERROR_SCANNER_PWM			-350
#define LS_ERROR_GAIN_FRONT				-351
#define LS_ERROR_GAIN_REAR				-352
#define LS_ERROR_COEFF_FRONT			-353
#define LS_ERROR_COEFF_REAR				-354
#define LS_ERROR_SCANNER_GENERICO		-363

#define LS_ERROR_SCANNER_PWM_FRONT		-368
#define LS_ERROR_SCANNER_PWM_REAR		-369
#define LS_CALIBR_ERR_PHOTO_SCANNER		-370

#define LS_ERROR_ID_OFFSET_FLU_2		-380
#define LS_ERROR_ID_OFFSET_FLU_3		-381
#define LS_ERROR_ID_LED_CURRENT_2		-382
#define LS_ERROR_ID_LED_CURRENT_3		-383
#define LS_ERROR_ID_OFFSET_REF			-384
#define LS_ERROR_ID_TARGET_REF_2		-385
#define LS_ERROR_ID_TARGET_REF_3		-386
#define LS_ERROR_ID_OUT_TOLERANCE		-387
#define LS_ERROR_ID_INK_DETECTED		-388
#define LS_ERROR_ID_VERIFY_CALIBRATION	-389
#define LS_ID_INDK_DETECTED				300


// --------------------------------------------------------------
// CTS Reserved function
// --------------------------------------------------------------
#ifdef __cplusplus
extern "C" {
#endif

extern int APIENTRY LSConnectDiagnostica(HWND, HANDLE, short, short *);

extern int APIENTRY LSConnectDownload(HWND hWnd, HANDLE hInstAppl, short Peripheral, short *hConnect);

extern int APIENTRY LSReadCodelineWhitOCR(short hConnect, HWND hWnd, LPSTR Codeline, short *Length_Codeline, LPSTR Optic, short *Length_Optic);

extern int APIENTRY CTS_F1(short hConnect, HWND hWnd, char *BytesCfg);

extern int APIENTRY LSBuilderSetting(short, HWND, void *);

extern int APIENTRY LSSetSerialNumber(short, HWND, unsigned char *, short);

extern int APIENTRY LSReadE13BSignal(short, HWND, unsigned char *, long *);

extern int APIENTRY LSReadCMC7Signal(short hConnect, HWND hWnd, unsigned char *pBuff, long llBuff);

extern int APIENTRY LSReadImageOCR(short hConnect, HWND hWnd, void **pBuff);

extern int APIENTRY LSScannerCalibration2(short, HWND, short, short);

extern int APIENTRY LSScannerCalibration(short, HWND, short);

extern int APIENTRY LSScannerCalibrationOld(short, HWND, short);

extern int APIENTRY LSPhotosCalibrationEx(short, HWND, short, char);

extern int APIENTRY LSPhotosCalibration(short, HWND, short);

extern int APIENTRY LSReadBytesDoubleleafing(short, HWND, unsigned char, unsigned short, unsigned char *);

extern int APIENTRY LSSetLeaferDeltaThrust(short hConnect, HWND hWnd, short Value);

extern int APIENTRY LSReadPhotosValue(short, HWND, unsigned short *, unsigned short *);

extern int APIENTRY LSDumpPeripheralMemory(short, HWND, unsigned char *, long, long, char);

extern int APIENTRY LSCalibrationMICR(short, HWND, unsigned char *, short);

extern int APIENTRY LSSetDiagnosticMode(short, HWND, BOOL);

extern int APIENTRY LSSetTrimmerMICR(short, HWND, short, short);

extern int APIENTRY LSReadTrimmerMICR(short, HWND, char, unsigned char *, short);

extern int APIENTRY LSReadTimeSampleMICR(short, HWND, char, unsigned short *, short);

extern int APIENTRY LSSetTimeSampleMICR(short, HWND, char, short);

extern int APIENTRY LSSetInImageCalibration(short, HWND);

extern int APIENTRY LSImageCalibration(short, HWND, BOOL, short *);

extern int APIENTRY LSMoveMotorFeeder(short, HWND, short);

extern int APIENTRY LSTestElectromagnet(short hConnect, HWND hWnd, short nrElectromagnet);

extern int APIENTRY LSTestElectromagnet2(short hConnect, HWND hWnd, short nrElectromagnet, short PWMvalue);

extern int APIENTRY LSReadFeederMotorAD(short hConnect, HWND hWnd, unsigned short *pDati);

extern int APIENTRY LSSetScannerLight(short hConnect, HWND hWnd, short Light);

extern int APIENTRY LSReloadImage(short	hConnect, HWND hWnd, short ClearBlack, char Side, short nrBank, long ImageLen, unsigned long NrDoc, LPHANDLE FrontImage, LPHANDLE BackImage, LPHANDLE FrontImageNetto, LPHANDLE BackImageNetto);

extern int APIENTRY LSTestCinghia(short hConnect, HWND hWnd, unsigned char *pBuff, long *llBuff);

extern int APIENTRY LSGetPWMValues(short hConnect, char Side, long fReadData, long nrBytes, unsigned char *pData);

extern int APIENTRY LSReadScannerData(short hConnect, char Side, short Color, long fReadData, long nrBytes, unsigned char *pData);

extern int APIENTRY LS800_PhotosCalibration(HWND, char, unsigned char *, short);

extern int APIENTRY LS800_ReadPhotosValue(HWND, char, unsigned char *, short);

extern int APIENTRY LS515usbSetdefaulADvalue(short Reserved);

extern int APIENTRY LS150usbSetPWMDoubleLeafing(unsigned char *PWMvalues, short nrPWM);

extern int APIENTRY LSSendBytes(short, unsigned char *, short);

extern int APIENTRY LSReceiveBytes(short, unsigned char *, short, long);

extern int APIENTRY ARCATestPWMScanner(short hConnect, HWND hWnd, short Value);

extern int APIENTRY LSInkDetectionSendCmd(short, unsigned char *, short);

extern int APIENTRY LSInkDetectionReadBytes(short, unsigned char *, short, long);

extern int APIENTRY LSInkDetectorCalibration(short hLS, short Step, short DocValue, char *ParIni);

extern int APIENTRY LSInkDetectorLoadCmds(short hConnect, char *pFname);

#ifdef __cplusplus
}
#endif

////////////////////////////////////////////////////////////////////

#endif
