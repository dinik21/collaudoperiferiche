#include "StdAfx.h"
#include "Constanti.h"

Ls150Class::Costanti::Costanti()
{
	
}



String ^Ls150Class::Costanti::GetDesc(int ReturnCode)
{
			String ^Description;
			switch( ReturnCode)
			{
			case S_LS150_SERVERSYSERR:
			//case S_LS150_SYSTEM_ERROR:	
				Description = "LS150_SERVERSYSERR";
					break;
			case S_LS150_USBERR:
					Description = "LS150_USBERR";
					break;
			case S_LS150_PERIFNOTFOUND:
					Description = "Periferica NON Connessa";
					break;
			case S_LS150_HARDERR:
					Description = "LS150_HARDERR";
					break;	
			
			case S_LS150_CODELINE_ERROR:
					Description = "LS150_CODELINE_ERROR";
					break;
				
				case S_LS150_PAPER_JAM:
					Description = "LS150_PAPER_JAM";
					break;
				case S_LS150_TARGET_BUSY:
					Description = "LS150_TARGET_BUSY";
					break;
				case S_LS150_INVALIDCMD:
					Description = "LS150_INVALIDCMD";
					break;
				case S_LS150_DATALOST:
					Description = "LS150_DATALOST";
					break;
				case S_LS150_EXECCMD:
					Description = "LS150_EXECCMD";
					break;
				case S_LS150_JPEGERROR:
					Description = "LS150_JPEGERROR";
					break;
				case S_LS150_CMDSEQUENCEERROR:
					Description = "LS150_CMDSEQUENCEERROR";
					break;
				case S_LS150_NOTUSED:
					Description = "LS150_NOTUSED";
					break;
				case S_LS150_IMAGEOVERWRITE:
					Description = "LS_IMAGEOVERWRITE,Spegnere e riaccendere il computer....";
					break;
				case S_LS150_INVALID_HANDLE:
					Description = "LS150_INVALID_HANDLE";
					break;
				case S_LS150_NO_LIBRARY_LOAD:
					Description = "LS150_NO_LIBRARY_LOAD";
					break;
				case S_LS150_BMP_ERROR:
					Description = "LS150_BMP_ERROR";
					break;
				case S_LS150_TIFFERROR:
					Description = "LS150_TIFFERROR";
					break;
				case S_LS150_IMAGE_NO_MORE_AVAILABLE:
					Description = "LS150_IMAGE_NO_MORE_AVAILABLE";
					break;
				case S_LS150_IMAGE_NO_FILMED:
					Description = "LS150_IMAGE_NO_FILMED";
					break;
				case S_LS150_IMAGE_NOT_PRESENT:
					Description = "LS150_IMAGE_NOT_PRESENT";
					break;
				case S_LS150_FUNCTION_NOT_AVAILABLE:
					Description = "LS150_FUNCTION_NOT_AVAILABLE";
					break;
				case S_LS150_DOCUMENT_NOT_SUPPORTED:
					Description = "LS150_DOCUMENT_NOT_SUPPORTED";
					break;
				case S_LS150_BARCODE_ERROR:
					Description = "LS150_BARCODE_ERROR";
					break;
				case S_LS150_INVALID_LIBRARY:
					Description = "LS150_INVALID_LIBRARY";
					break;
				case S_LS150_INVALID_IMAGE:
					Description = "LS150_INVALID_IMAGE";
					break;
				case S_LS150_INVALID_IMAGE_FORMAT:
					Description = "LS150_INVALID_IMAGE";
					break;
				case S_LS150_INVALID_BARCODE_TYPE:
					Description = "LS150_INVALID_BARCODE_TYPE";
					break;
				case S_LS150_OPEN_NOT_DONE:
					Description = "LS150_OPEN_NOT_DONE";
					break;
				case S_LS150_INVALID_TYPE_CMD:
					Description = "LS150_INVALID_TYPE_CMD";
					break;
				case S_LS150_INVALID_CLEARBLACK:
					Description = "LS150_INVALID_CLEARBLACK";
					break;
				case S_LS150_INVALID_SIDE:
					Description = "LS150_INVALID_SIDE";
					break;
				case S_LS150_MISSING_IMAGE:
					Description = "LS150_MISSING_IMAGE";
					break;
				case S_LS150_INVALID_TYPE:
					Description = "LS150_INVALID_TYPE";
					break;
				case S_LS150_INVALID_SAVEMODE:
					Description = "LS150_INVALID_SAVEMODE";
					break;
				case S_LS150_INVALID_PAGE_NUMBER:
					Description = "LS150_INVALID_PAGE_NUMBER";
					break;
				case S_LS150_INVALID_NRIMAGE:
					Description = "LS150_INVALID_NRIMAGE";
					break;
				case S_LS150_INVALID_FRONTSTAMP:
					Description = "LS150_INVALID_FRONTSTAMP";
					break;
				case S_LS150_INVALID_WAITTIMEOUT:
					Description = "LS150_INVALID_WAITTIMEOUT";
					break;
				case S_LS150_INVALID_VALIDATE:
					Description = "LS150_INVALID_VALIDATE";
					break;
				case S_LS150_INVALID_CODELINE_TYPE:
					Description = "LS150_INVALID_CODELINE_TYPE";
					break;
				case S_LS150_MISSING_NRIMAGE:
					Description = "LS150_MISSING_NRIMAGE";
					break;
				case S_LS150_INVALID_SCANMODE:
					Description = "LS150_INVALID_SCANMODE";
					break;
				case S_LS150_INVALID_BEEP_PARAM:
					Description = "LS150_INVALID_BEEP_PARAM";
					break;
				case S_LS150_INVALID_FEEDER_PARAM:
					Description = "LS150_INVALID_FEEDER_PARAM";
					break;
				case S_LS150_INVALID_SORTER_PARAM:
					Description = "LS150_INVALID_SORTER_PARAM";
					break;
				case S_LS150_INVALID_BADGE_TRACK:
					Description = "LS150_INVALID_BADGE_TRACK";
					break;
				case S_LS150_MISSING_FILENAME:
					Description = "LS150_MISSING_FILENAME";
					break;
				case S_LS150_INVALID_QUALITY:
					Description = "LS150_INVALID_QUALITY";
					break;
				case S_LS150_INVALID_FILEFORMAT:
					Description = "LS150_INVALID_FILEFORMAT";
					break;
				case S_LS150_INVALID_COORDINATE:
					Description = "LS150_INVALID_COORDINATE";
					break;
				case S_LS150_MISSING_HANDLE_VARIABLE:
					Description = "LS150_MISSING_HANDLE_VARIABLE";
					break;
				case S_LS150_INVALID_POLO_FILTER:
					Description = "LS150_INVALID_POLO_FILTER";
					break;
				case S_LS150_INVALID_ORIGIN_MEASURES:
					Description = "LS150_INVALID_ORIGIN_MEASURES";
					break;
				case S_LS150_INVALID_SIZEH_VALUE:
					Description = "LS150_INVALID_SIZEH_VALUE";
					break;
				case S_LS150_INVALID_FORMAT:
					Description = "LS150_INVALID_FORMAT";
					break;
				case S_LS150_STRINGS_TOO_LONGS:
					Description = "LS150_STRINGS_TOO_LONGS";
					break;
				case S_LS150_READ_IMAGE_FAILED:
					Description = "LS150_READ_IMAGE_FAILED";
					break;
				
				case S_LS150_MISSING_BUFFER_HISTORY:
					Description = "LS150_MISSING_BUFFER_HISTORY";
					break;
				case S_LS150_INVALID_ANSWER:
					Description = "LS150_INVALID_ANSWER";
					break;
				case S_LS150_OPEN_FILE_ERROR_OR_NOT_FOUND:
					Description = "LS150_OPEN_FILE_ERROR_OR_NOT_FOUND";
					break;
				case S_LS150_READ_TIMEOUT_EXPIRED:
					Description = "LS150_READ_TIMEOUT_EXPIRED";
					break;
				case S_LS150_INVALID_METHOD:
					Description = "LS150_INVALID_METHOD";
					break;
				case S_LS150_CALIBRATION_FAILED:
					Description = "LS150_CALIBRATION_FAILED";
					break;
				case S_LS150_INVALID_SAVEIMAGE:
					Description = "LS150_INVALID_SAVEIMAGE";
					break;
				case S_LS150_UNIT_PARAM:
					Description = "LS150_UNIT_PARAM";
					break;
				case S_LS150_INVALID_NRWINDOWS:
					Description = "LS150_INVALID_NRWINDOWS";
					break;
				case S_LS150_INVALID_VALUE:
					Description = "LS150_INVALID_VALUE";
					break;
				case S_LS150_INVALID_DEGREE:
					Description = "LS150_INVALID_DEGREE";
					break;
				case S_LS150_R0TATE_ERROR:
					Description = "LS150_R0TATE_ERROR";
					break;
				case S_LS150_PERIPHERAL_RESERVED:
					Description = "LS150_PERIPHERAL_RESERVED";
					break;
				case S_LS150_INVALID_NCHANGE:
					Description = "LS150_INVALID_NCHANGE";
					break;
				case S_LS150_BRIGHTNESS_ERROR:
					Description = "LS150_BRIGHTNESS_ERROR";
					break;
				case S_LS150_CONTRAST_ERROR:
					Description = "LS150_CONTRAST_ERROR";
					break;
				case S_LS150_DOUBLE_LEAFING_ERROR:
					Description = "LS150_DOUBLE_LEAFING_ERROR";
					break;
				case S_LS150_INVALID_BADGE_TIMEOUT:
					Description = "LS150_INVALID_BADGE_TIMEOUT";
					break;
				case S_LS150_INVALID_RESET_TYPE:
					Description = "LS150_INVALID_RESET_TYPE";
					break;
				case S_LS150_IMAGE_NOT_200_DPI:
					Description = "LS150_IMAGE_NOT_200_DPI";
					break;

				case S_LS150_DECODE_FONT_NOT_PRESENT:
					Description = "LS150_DECODE_FONT_NOT_PRESENT";
					break;
				case S_LS150_DECODE_INVALID_COORDINATE:
					Description = "LS150_DECODE_INVALID_COORDINATE";
					break;
				case S_LS150_DECODE_INVALID_OPTION:
					Description = "LS150_DECODE_INVALID_OPTION";
					break;
				case S_LS150_DECODE_INVALID_CODELINE_TYPE:
					Description = "LS150_DECODE_INVALID_CODELINE_TYPE";
					break;
				case S_LS150_DECODE_SYSTEM_ERROR:
					Description = "LS150_DECODE_SYSTEM_ERROR";
					break;
				case S_LS150_DECODE_DATA_TRUNC:
					Description = "LS150_DECODE_DATA_TRUNC";
					break;
				case S_LS150_DECODE_INVALID_BITMAP:
					Description = "LS150_DECODE_INVALID_BITMAP";
					break;
				case S_LS150_DECODE_ILLEGAL_USE:
					Description = "LS150_DECODE_ILLEGAL_USE";
					break;
				case S_LS150_BARCODE_GENERIC_ERROR:
					Description = "Barcode Generic Error";
					break;
				case S_LS150_BARCODE_NOT_DECODABLE:
					Description = "Barcode Not Decodable";
					break;
				case S_LS150_BARCODE_OPENFILE_ERROR:
					Description = "Barcode OpenFile Error";
					break;
				case S_LS150_BARCODE_READBMP_ERROR:
					Description = "Barcode Read Error";
					break;
				case S_LS150_BARCODE_MEMORY_ERROR:
					Description = "Barcode Memory Error";
					break;
				case S_LS150_BARCODE_START_NOTFOUND:
					Description = "Barcode Start not found";
					break;
				case S_LS150_BARCODE_STOP_NOTFOUND:
					Description = "Barcode Stop not found";
					break;
				case S_LS150_PDF_NOT_DECODABLE:
					Description = "LS150_PDF_NOT_DECODABLE";
					break;
				case S_LS150_PDF_READBMP_ERROR:
					Description = "LS150_PDF_READBMP_ERROR";
					break;
				case S_LS150_PDF_BITMAP_FORMAT_ERROR:
					Description = "LS150_PDF_BITMAP_FORMAT_ERROR";
					break;
				case S_LS150_PDF_MEMORY_ERROR:
					Description = "LS150_PDF_MEMORY_ERROR";
					break;
				case S_LS150_PDF_START_NOTFOUND:
					Description = "LS150_PDF_START_NOTFOUND";
					break;
				case S_LS150_PDF_STOP_NOTFOUND:
					Description = "LS150_PDF_STOP_NOTFOUND";
					break;
				case S_LS150_PDF_LEFTIND_ERROR:
					Description = "LS150_PDF_LEFTIND_ERROR";
					break;
				case S_LS150_PDF_RIGHTIND_ERROR:
					Description = "LS150_PDF_RIGHTIND_ERROR";
					break;
				case S_LS150_PDF_OPENFILE_ERROR:
					Description = "LS150_PDF_OPENFILE_ERROR";
					break;
				case S_LS150_USER_ABORT:
					Description = "S_LS150_USER_ABORT";
					break;
				case S_LS150_JAM_AT_MICR_PHOTO:
					Description = "S_LS150_JAM_AT_MICR_PHOTO";
					break;
				case S_LS150_JAM_DOC_TOOLONG:
					Description = "S_LS150_JAM_DOC_TOO_LONG";
					break;
				case S_LS150_JAM_AT_SCANNERPHOTO:
					Description = "S_LS150_JAM_AT_SCANNER_PHOTO";
					break;

				case S_LS150_ERROR_OFFSET_FRONT:
					Description = "S_LS150_ERROR_OFFSET_FRONT";
					break;
				case S_LS150_ERROR_OFFSET_REAR:
					Description = "S_LS150_ERROR_OFFSET_REAR";
					break;
				case S_LS150_ERROR_SCANNER_PWM:
					Description = "S_LS150_ERROR_SCANNER_PWM";
					break;
				case S_LS150_ERROR_GAIN_FRONT:
					Description = "S_LS150_ERROR_GAIN_FRONT";
					break;
				case S_LS150_ERROR_GAIN_REAR:
					Description = "S_LS150_ERROR_GAIN_REAR";
					break;
				case S_LS150_ERROR_COEFF_FRONT:
					Description = "S_LS150_ERROR_COEFF_FRONT";
					break;
				case S_LS150_ERROR_COEFF_REAR:
					Description = "S_LS150_ERROR_COEFF_REAR";
					break;
				case S_LS150_ERROR_SCANNER_GENERICO:
					Description = "S_LS150_ERROR_SCANNER_GENERICO";
					break ;


				case S_LS150_FPGA_NOT_OK:			
					Description = "S_LS150_FPGA_NOT_OK";
					break;
				case S_LS150_BOARD_NOT_OK:
					Description = "S_LS150_BOARD_NOT_OK";
					break;
				case S_LS150_DATA_NOT_OK:
					Description = "S_LS150_DATA_NOT_OK";
					break;
				case S_LS150_FIRMWARE_NOT_OK:
					Description = "S_LS150_FIRMWARE_NOT_OK";
					break;
				case S_LS150_FEEDER_NOT_OK:
					Description = "S_LS150_FEEDER_NOT_OK";
					break;


				// ------------------------------------------------------------------------
				//                  WARNINGS
				// ------------------------------------------------------------------------
				case S_LS150_FEEDEREMPTY:
					Description = "LS150_FEEDEREMPTY";
					break;
				case S_LS150_DATATRUNC:
					Description = "LS150_DATATRUNC";
					break;
				case S_LS150_DOCPRESENT:
					Description = "LS150_DOCPRESENT";
					break;
				case S_LS150_BADGETIMEOUT:
					Description = "LS150_BADGETIMEOUT";
					break;
				case S_LS150_PERIF_BUSY:
					Description = "LS150_PERIF_BUSY";
					break;
				case S_LS150_NO_ENDCMD:
					Description = "LS150_NO_ENDCMD";
					break;
				case S_LS150_RETRY:
					Description = "LS150_RETRY";
					break;
				case S_LS150_NO_OTHER_DOCUMENT:
					Description = "LS150_NO_OTHER_DOCUMENT";
					break;
				case S_LS150_QUEUEFULL:
					Description = "LS150_COMMAND_NOT_SUPPORTED";
					break;
				case S_LS150_NOSENSE:
					Description = "LS150_NOSENSE";
					break;
				case S_LS150_TRY_TO_RESET:
					Description = "LS150_TRY_TO_RESET";
					break;
				case S_LS150_STRINGTRUNCATED:
					Description = "LS150_STRINGTRUNCATED";
					break;
				case S_LS150_COMMAND_NOT_SUPPORTED:
					Description = "LS150_COMMAND_NOT_SUPPORTED";
					break;
				case S_LS150_LOOP_INTERRUPTED:
					Description = "LS150_LOOP_INTERRUPTED";
					break;
				case S_LS150_SORTER_FULL:
					Description = "S_LS150_SORTER_FULL";
					break ; 

				case S_LS150_BACK_BACKGROUND :
					Description = "REAR background KO - One or more row up 64 level";
					break;

				case S_LS150_BACK_BACKGROUND_1 :
					Description = "REAR background KO - More then 10 row contiguous over the level of 32 !!!";
					break;
				
				case S_LS150_BACK_BACKGROUND_2 :
					Description = "REAR background KO - More then 80 row over the level of 80 !!!";
					break;

				case S_LS150_FRONT_BACKGROUND :
					Description = "FRONT background KO - One or more row up 64 level";
					break;

				case S_LS150_FRONT_BACKGROUND_1 :
					Description = "FRONT background KO - More then 10 row contiguous over the level of 32 !!!";
					break;
				
				case S_LS150_FRONT_BACKGROUND_2 :
					Description = "FRONT background KO - More then 80 row over the level of 80 !!!";
					break;
				
				case S_LS150_CCI_TEST_FAILED : 
					Description = "Controllo Fascioni - TestFallito"; 
					break ;

				case S_LS150_LS_SHORT_PAPER :
					Description = "Docutest Troppo Corto Inserito"; 
					break ; 

				case S_LS150_DOCUTEST_NOT_CORRECT : 
					Description = "Barcode Docutest NON trovato o Barcode NON Corretto"; 
					break ; 

				case S_LS150_CALIBATRION_MICR_NEW : 
					Description = "Collaudo annullato per scarto calibrazione"; 
					break ; 

				case S_LS150_TEST_CINGHIA_KO :
					Description = "Test Cinghia KO";
					break;

			}
			return Description;
		}