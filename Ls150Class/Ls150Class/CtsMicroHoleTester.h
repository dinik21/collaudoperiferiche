/// \file
#ifndef __CTSMICROHOLETESTER_H
#define __CTSMICROHOLETESTER_H

// ****************************************************************************

#define MHTESTER_PRODUCT_NAME_LEN   64                                  ///< Product name maximum character length
#define MHTESTER_PRODUCT_NAME_SIZE  ( MHTESTER_PRODUCT_NAME_LEN + 1 )   ///< Product name buffer size

#define MHTESTER_OK              0  ///< No error
#define MHTESTER_INVALID_HANDLE  -1 ///< Invalid handle
#define MHTESTER_INVALID_BITMAP  -2 ///< Invalid bitmap (not supported or bad header)
#define MHTESTER_OUT_OF_RANGE    -3 ///< Invalid index range
#define MHTESTER_NOT_FOUND       -4 ///< Database or product not found
#define MHTESTER_SYSTEM_ERROR    -5 ///< System error (should be reported)

// Error flags
#define MHTESTER_MASK_BAR_COUNT_ERR    ( ( unsigned short ) 0x0001 ) ///< Invalid number of bars
#define MHTESTER_MASK_BAR_COLOR_ERR    ( ( unsigned short ) 0x0002 ) ///< Invalid bar color
#define MHTESTER_MASK_DIM_BORDER_ERR   ( ( unsigned short ) 0x0004 ) ///< Invalid border bar dimension
#define MHTESTER_MASK_DIM_INNER_ERR    ( ( unsigned short ) 0x0008 ) ///< Invalid bar dimimension relative to inner criteria
#define MHTESTER_MASK_DIM_OUTER_ERR    ( ( unsigned short ) 0x0010 ) ///< Invalid bar dimimension relative to outer criteria
#define MHTESTER_MASK_LEV_INNER_ERR    ( ( unsigned short ) 0x0020 ) ///< Invalid bar grey level relative to inner criteria
#define MHTESTER_MASK_LEV_OUTER_ERR    ( ( unsigned short ) 0x0040 ) ///< Invalid bar grey level relative to outer criteria

// ****************************************************************************

/// \brief     References the tester instance
typedef void * MHTESTER_HANDLE;

// ****************************************************************************

/// \brief     Structure that holds the statistical information of a distribution
struct MHTESTER_Stat
{
   double mean;   ///< Mean value of the distribution
   double stdDev; ///< Standard deviation of the distribution
};

/// \brief     Defines a data type for structure MHTESTER_Stat
typedef struct MHTESTER_Stat MHTESTER_STAT;

/// \brief     Defines a pointer data type for structure MHTESTER_Stat
typedef struct MHTESTER_Stat *PMHTESTER_STAT;

// ****************************************************************************

/// \brief     Structure that holds a numeric interval
struct MHTESTER_Interval
{
   double min; ///< Minimum value
   double max; ///< Maximum value
};

/// \brief     Defines a data type for structure MHTESTER_Interval
typedef struct MHTESTER_Interval MHTESTER_INTERVAL;

/// \brief     Defines a pointer data type for structure MHTESTER_Interval
typedef struct MHTESTER_Interval *PMHTESTER_INTERVAL;

// ****************************************************************************

/// \brief     Structure that holds the pattern verification criteria
struct MHTESTER_Criteria
{
   char szProduct[ MHTESTER_PRODUCT_NAME_SIZE ];   ///< Name of the product

   int numberOfBars;                               ///< Number of black and white bars in the pattern (extremes are included)

   struct MHTESTER_Interval bottomDim;             ///< Bottom black bar dimension range
   struct MHTESTER_Interval topDim;                ///< Top black bar dimension range

   struct MHTESTER_Interval innerDim;              ///< Inner bar dimension criteria interval
   double innerDimMinValidPerc;                    ///< Minimum percentage of bars that verify the inner dimension criteria (extremes are excluded)

   struct MHTESTER_Interval outerDim;              ///< Bar dimension interval for outer criteria
   double outerDimMinValidPerc;                    ///< Minimum percentage of bars that verify the outer dimension criteria (extremes are excluded)

   struct MHTESTER_Interval innerLev;              ///< Inner bar grey level criteria interval
   double innerLevMinValidPerc;                    ///< Minimum percentage of bars that verify the inner grey level criteria (extremes are included)

   struct MHTESTER_Interval outerLev;              ///< Outer bar grey level criteria interval
   double outerLevMinValidPerc;                    ///< Minimum percentage of bars that verify the outer grey level criteria (extremes are included)
};

/// \brief     Defines a data type for structure MHTESTER_Criteria
typedef struct MHTESTER_Criteria MHTESTER_CRITERIA;

/// \brief     Defines a pointer data type for structure MHTESTER_Criteria
typedef struct MHTESTER_Criteria *PMHTESTER_CRITERIA;

// ****************************************************************************

/// \brief     Structure that holds the information relative to a single bar of the CIS
/// \details   <b>errorFlags</b> can hold any combination of the following flags:
///            <ul>
///               <li>MHTESTER_MASK_BAR_COLOR_ERR</li>
///               <li>MHTESTER_MASK_DIM_BORDER_ERR</li>
///               <li>MHTESTER_MASK_DIM_INNER_ERR</li>
///               <li>MHTESTER_MASK_DIM_OUTER_ERR</li>
///               <li>MHTESTER_MASK_LEV_INNER_ERR</li>
///               <li>MHTESTER_MASK_LEV_OUTER_ERR</li>
///            </ul>
struct MHTESTER_BarInfo
{
   unsigned short errorFlags;    ///< Contains bar failure information

   char bWhite;                  ///< Nature of the bar: 1 if white, 0 otherwise

   int yi;                       ///< Starting y-axis coordinate (bottom to top)
   int yf;                       ///< Ending y-axis coordinate (bottom to top)

   struct MHTESTER_Stat levStat; ///< Statistical information about the grey level of the bar
};

/// \brief     Defines a data type for structure MHTESTER_BarInfo
typedef struct MHTESTER_BarInfo MHTESTER_BARINFO;

/// \brief     Defines a pointer data type for structure MHTESTER_BarInfo
typedef struct MHTESTER_BarInfo *PMHTESTER_BARINFO;

// ****************************************************************************

/// \brief     Statistical information of the CIS Black&White pattern
/// \details   <b>errorFlags</b> can hold any combination of the following flags:
///            <ul>
///               <li>MHTESTER_MASK_BAR_COUNT_ERR</li>
///               <li>MHTESTER_MASK_BAR_COLOR_ERR</li>
///               <li>MHTESTER_MASK_DIM_BORDER_ERR</li>
///               <li>MHTESTER_MASK_DIM_INNER_ERR</li>
///               <li>MHTESTER_MASK_DIM_OUTER_ERR</li>
///               <li>MHTESTER_MASK_LEV_INNER_ERR</li>
///               <li>MHTESTER_MASK_LEV_OUTER_ERR</li>
///            </ul>
struct MHTESTER_PatternInfo
{
   unsigned short errorFlags;       ///< Pattern failure information

   double px_x_mm;                  ///< Image resolution in px/mm

   int maxGreyLevel;                ///< Image maximum grey level
   int binarizationTh;              ///< Binarization threshold

   double innerDimValidPerc;        ///< Percentage of valid bars relative to inner dimension criteria
   double outerDimValidPerc;        ///< Percentage of valid bars relative to outer dimension criteria
   double innerLevValidPerc;        ///< Percentage of valid bars relative to inner grey level criteria
   double outerLevValidPerc;        ///< Percentage of valid bars relative to outer grey level criteria

   struct MHTESTER_Stat wDimStat;   ///< Statistical information about white bars dimension
   struct MHTESTER_Stat bDimStat;   ///< Statistical information about black bars dimension
   struct MHTESTER_Stat gDimStat;   ///< Statistical information about generic bars dimension
   struct MHTESTER_Stat wLevStat;   ///< Statistical information about white bars lightness (grey level)
   struct MHTESTER_Stat bLevStat;   ///< Statistical information about black bars lightness (grey level)
};

/// \brief     Defines a data type for structure MHTESTER_PatternInfo
typedef struct MHTESTER_PatternInfo MHTESTER_PATTERNINFO;

/// \brief     Defines a pointer data type for structure MHTESTER_PatternInfo
typedef struct MHTESTER_PatternInfo *PMHTESTER_PATTERNINFO;

// ****************************************************************************

/// \brief     Returns the version of the library in the format <b>X.Y.Z.W_yyyymmdd</b>
/// \details   <ul>
///               <li><b>X</b>,<b>Y</b>,<b>Z</b>,<b>W</b> version numbers</li>
///               <li><b>yyyy</b> stands for year</li>
///               <li><b>mm</b> stands for mounth</li>
///               <li><b>dd</b> stands for day</li>
///            </ul>
/// \return    Pointer to the null terminated string of characters containing the version of the library
const char * __stdcall MHTESTER_GetLibraryVersion( void );

// ****************************************************************************

/// \brief     Returns the pattern verification criteria for the specified product
/// \param     [in] szProduct the name of the product
/// \param     [out] pCriteria the pointer to the verification criteria object
/// \return    <ul>
///               <li>MHTESTER_OK</li>
///               <li>MHTESTER_NOT_FOUND</li>
///               <li>MHTESTER_SYSTEM_ERROR</li>
///            </ul>
int __stdcall MHTESTER_getCriteria( const char szProduct[], PMHTESTER_CRITERIA pCriteria );

// ****************************************************************************
// TESTER (as function)
// ****************************************************************************

/// \brief     Verifies the correctness of the CIS Black&White pattern
/// \details   Directly verifies the pattern and returns result with statistical information
/// \param     [in] pBMP the pointer to the bitmap from which verify CIS Black&White pattern (only 256 grey level bitmaps at 300dpi are accepted)
/// \param     [in] pCriteria the pointer to the verification criteria object
/// \param     [in] maxBars the maximum bars that the barInfo array can hold (if set to <b>0</b> no information will be stored in <b>pBars</b>)
/// \param     [out] pPatternInfo the statistical pattern information
/// \param     [out] pBars the number of elements in the barInfo array
/// \param     [out] barInfo the bar information array
/// \param     [out] pbPassed the result of the operation: <b>1</b> if test passed, <b>0</b> otherwise
/// \return    <ul>
///               <li>MHTESTER_OK</li>
///               <li>MHTESTER_INVALID_BITMAP</li>
///               <li>MHTESTER_SYSTEM_ERROR</li>
///            </ul>
int __stdcall MHTESTER_cisTestPassed(
   const char *pBMP,
   const PMHTESTER_CRITERIA pCriteria,
   int maxBars,
   PMHTESTER_PATTERNINFO pPatternInfo,
   int *pBars,
   MHTESTER_BARINFO barInfo[],
   char *pbPassed );

// ****************************************************************************
// TESTER (as object)
// ****************************************************************************

/// \brief     Constructor
/// \details   Creates a tester instance.<br>
///            <br>
///            In <b>pError</b> will be stored the result of the operation:
///            <ul>
///               <li>MHTESTER_OK</li>
///               <li>MHTESTER_INVALID_BITMAP</li>
///               <li>MHTESTER_SYSTEM_ERROR</li>
///            </ul>
/// \param     [in] pBMP the pointer to the bitmap object from which extract CIS Black&White pattern information
/// \param     [in] pCriteria the pointer to the verification criteria object
/// \param     [in] pError the result of the operation
/// \return    Handle to tester instance
MHTESTER_HANDLE __stdcall MHTESTER_new( const char *pBMP, const PMHTESTER_CRITERIA pCriteria, int *pError );

/// \brief     Destructor
/// \details   Destroyes the tester instance
/// \param     [in] handle the reference to the tester instance
void __stdcall MHTESTER_free( MHTESTER_HANDLE handle );

/// \brief     Returns the result of the CIS Black&White pattern verification process
/// \param     [in] handle the reference to the tester instance
/// \param     [out] pbPassed the result of the operation: <b>1</b> if test passed, <b>0</b> otherwise
/// \return    <ul>
///               <li>MHTESTER_OK</li>
///               <li>MHTESTER_INVALID_HANDLE</li>
///            </ul>
int __stdcall MHTESTER_passed( MHTESTER_HANDLE handle, char *pbPassed );

/// \brief     Returns the statistical information of the CIS Black&white pattern
/// \param     [in] handle the reference to the tester instance
/// \param     [out] pInfo the statistical pattern information
/// \return    <ul>
///               <li>MHTESTER_OK</li>
///               <li>MHTESTER_INVALID_HANDLE</li>
///            </ul>
int __stdcall MHTESTER_getPatternInfo( MHTESTER_HANDLE handle, PMHTESTER_PATTERNINFO pInfo );

/// \brief     Returns the number of bars of the CIS Black&white pattern
/// \param     [in] handle the reference to the tester instance
/// \param     [out] pSize the number of bars
/// \return    <ul>
///               <li>MHTESTER_OK</li>
///               <li>MHTESTER_INVALID_HANDLE</li>
///            </ul>
int __stdcall MHTESTER_getPatternSize( MHTESTER_HANDLE handle, int *pSize );

/// \brief     Returns the bar with the specified index
/// \param     [in] handle the reference to the tester instance
/// \param     [in] idx the position of the bar
/// \param     [out] pInfo the bar information
/// \return    <ul>
///               <li>MHTESTER_OK</li>
///               <li>MHTESTER_INVALID_HANDLE</li>
///            </ul>
int __stdcall MHTESTER_getBarInfo( MHTESTER_HANDLE handle, int idx, PMHTESTER_BARINFO pInfo );

// ****************************************************************************

#endif
