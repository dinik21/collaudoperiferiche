// Il seguente blocco ifdef rappresenta il modo standard di creare macro che semplificano 
// l'esportazione da una DLL. Tutti i file all'interno della DLL sono compilati con il simbolo CTSPDFDRIVERLICENSE_EXPORTS
// definito nella riga di comando. Tale simbolo non deve essere definito in un progetto
// che utilizza questa DLL. In questo modo qualsiasi altro progetto i cui file di origine includono questo file vedranno le funzioni 
// CTSPDFDRIVERLICENSE_API come importate da una DLL, mentre la DLL vedr� i simboli
// definiti con questa macro come esportati.
#ifdef CTSPDFDRIVERLICENSE_EXPORTS
#define CTSPDFDRIVERLICENSE_API __declspec(dllexport)
#else
#define CTSPDFDRIVERLICENSE_API __declspec(dllimport)
#endif

extern CTSPDFDRIVERLICENSE_API int ReadPDF417DriverLicense(BITMAPINFOHEADER *pImage, char *BarcodeRead, short *lenBarcodeRead, short *ErrorRate);


extern CTSPDFDRIVERLICENSE_API int CheckLicense(char *LScfg, char *SerialNr);
