

// File DLL principale.

#include "stdafx.h"
#include <stdio.h>
#include "LsApi.h"
#include "LsApi_Calibr.h"
#include "LsCFG150.h"
#include "CtsCalibrations.h"
#include "CtsPdfDriverLicense.h"
#include "Ls150Class.h"
#include "ErrWar.h"
#include "is.h"
#include "math.h"
#include "CtsMicroHoleTester.h" 
#include "libimgdiag.h"
#pragma pack(8)

extern "C" 
{
#include "gridpro.h"
}
#pragma pack(4)
#include "Constanti.h" 

//#define UNITA_VOLT						0.01953125//0.019042676
#define TRIMMER_PRE_READ				3
#define TRIMMER_MIN_VALUE				0x00
#define TRIMMER_MAX_VALUE				0xFF
#define TRIMMER_NO_VALUE				0

#define SCANNER_TIME_DEFAULT			1427
#define SCANNER_TIME_DEFAULT_VE			0x768

#define NR_VALORI_LETTI					512
//#define OFFSET_INZIO_CALCOLO			250
#define NR_VALORI_PER_MEDIA				200
#define OFFSET_PRIMO_PICCO				72

#define SCANNER_BACK					0
#define SCANNER_FRONT					1

#define LIMITE_16_GRIGI				0x06
#define LIMITE_256_GRIGI			0x66
#define NR_VALORI_OK				228
#define E13B_FINE_LLC				0x0d		// Terminatore dati Larghezza char
#define DIM_DESCR_CHAR				5
#define HEIGHT_BAR					50


#define TENSIONE_VCC					3.3		// Tensione di alimentazione del circuito MICR
#define MOLTIPL_RIFERIMENTO_VOLT		0.4		// Moltiplicatore per riferimento in Volt
#define NR_VALORI_CONVERTITORE			256
#define NR_BYTES_TRIMMER				2

#define OFFSET_INZIO_CALCOLO			300
#define NR_VALORI_PER_MEDIA				200
#define ZERO_TEORICO					0x80
// Defines taratura MICR
#define TENSIONE_VCC_LS150ETH				3.3		// Tensione di alimentazione del circuito MICR
#define MOLTIPL_RIFERIMENTO_VOLT_LS150ETH	0.4		// Moltiplicatore per riferimento in Volt
#define NR_VALORI_CONVERTITORE_LS150ETH		256



#define MASK_FEEDER_EMPTY				0x01
#define MASK_LOW_SPEED					0x02
#define MASK_PRINTER					0x08
#define MASK_OCR_HW						0x10
#define MASK_STAMP						0x20
#define MASK_SCANNER_UV					0x04
#define MASK_SCANNER_COLOR				0x20
#define MASK_PRINTER_HD					0x01



#define TEST_SPEED_200_DPI				0x00
#define TEST_SPEED_300_DPI				0x01
#define TEST_SPEED_75_DPM				0x02
#define TEST_SPEED_300_DPI_UV			0x03
#define TEST_SPEED_300_COLOR			0x04



#define VALORE_MAX_PICCO_MIN			0x60
#define NR_PICCHI						50
#define MAX_READ_RETRY					3
#define PICCO_DISTANZA_MINIMA			100
#define NR_BYTE_BUFFER_E13B				0x4000

#define SOGLIA_INIZIO_NERO			0x80 - 0x10	// Valore per inizio barretta nera
#define SPEED_GRAY						0x0001
#define SPEED_COLOR						0x0101

#define SPEED_200_DPI					0x00
#define SPEED_300_DPI					0x01


// Defines for dump memory
#define MEMORY_EEPROM					0
#define MEMORY_FLASH					1
#define MEMORY_RAM						2
#define MEMORY_EXTERN_RAM				3

#define OFF_PHOTO_1						4
#define OFF_PHOTO_2						5
#define OFF_PHOTO_3						6
#define OFF_PHOTO_4						7
#define OFF_PHOTO_5					16 //8
#define OFF_PHOTO_6					17 //9
#define OFF_PHOTO_7					18 //10


#define MASK_FEEDER_EMPTY				0x01
#define MASK_LOW_SPEED					0x02
#define MASK_PRINTER					0x08
#define MASK_OCR_HW						0x10
#define MASK_STAMP						0x20
#define MASK_SCANNER_UV					0x04
#define MASK_SCANNER_COLOR				0x20
#define MASK_PRINTER_HD					0x01


#define TYPE_LS150_DL_OLD				147
#define TYPE_LS150_UV_OLD				148
#define TYPE_LS150_VE					149
#define TYPE_LS150						150
#define TYPE_LS150_COLOR				159


#define RETRY_IMAGE_CALIBRATION			1000
#define GOOD_TIME_SAMPLE_VALUE			112.0f
#define MM_OFFSET_BORDO					12
#define SOGLIA_NERO_BIANCO				128


#define SAMPLE_VALUE_MIN_LS150_VE		107.0f
#define SAMPLE_VALUE_MAX_LS150_VE		116.0f

#define LENGH_DUMP_150					0x9000



#define SAMPLE_VALUE_MIN				107.0f
#define SAMPLE_VALUE_MAX				116.0f
#define SAMPLE_VALUE_MEDIO				112.0f

#define BARRE_GROUP					7
#define BARRE_GROUP_GRAF			14
// define per grafico velocit� doc
#define OFFSET_SCALA_X			8			// Scala X, valore dal basso

// define dei Colori 0x00rrggbb
#define NERO					0x00
#define VIOLA					0x6b
#define AZZURRO					0x7b
#define VERDINO					0x7c
#define GRANATA					0x84
#define GRIGIO					0xb6
#define ROSSO					0xe0
#define ARANCIO					0xf0
#define LILLA					0xf2
#define GIALLO					0xfe

#define OFFSET_SCALE_Y			16			// Offset scala y da inizio bitmap
#define HEIGHT_SIGNAL			300			// Altezza Bitmap Segnale E13B
#define SIZE_SCALE				8			// Pixel riservati per scale bitmap


#define BARCODE_309					"DOCUTEST-309"

short ConnessioneGlobal; 


 Ls150Class::Periferica::Periferica(void)
{
	/*PhotoValue[0]= 0;
	PhotoValue[1]= 0;
	PhotoValue[2]= 0;
	PhotoValue[3]= 0;
	PhotoValue[4]= 0;
	PhotoValue[5]= 0;
	PhotoValue[6]= 0;
	PhotoValue[7]= 0;*/
//	PhotoValue  = gcnew  array<System::Int16>{0,0,0,0,0,0,0,0};

	ReturnC = gcnew ErrWar();
	strCis = gcnew Cis();
	S_Storico = gcnew Storico();
	GetVersione();

	TipoPeriferica = LS_150_USB;

	Connessione = 0;
	SigmaFronte =0;
	SigmaRetro =0;
	fDatiScannerColore = false;
	fDatiScannerUV = false;
	fDatiScanner =  false;
	
	
}

int Ls150Class::Periferica::GetVersione()
{
	char Versione[80];

	VersioneDll_1 = "C.T.S. Ls150Class.dll -- Ver. 3.00, Rev 002,  date 06/06/2012";

	Reply = LSGetVersion(Versione,80);
	if(Reply == LS_OKAY)
	{
		VersioneDll_0 = Marshal::PtrToStringAnsi((IntPtr) (char *)Versione);
	}
		

	return 0;
}



int Ls150Class::Periferica::LeggiCodelineOCR(short Type)
{
	/*
	short ret;
	unsigned long NrDoc;
	short Length_Codeline=80;
	char Code[256];
	char StrNome[256];
	DATAOPTICALWINDOW pDimWindows;
	HANDLE HFrontGray;
	
	Reply = LS150_Open((HWND)hDlg,(HANDLE)hInst,SUSPENSIVE_MODE);
	// Chiamo la funzione per via documento
	if ((Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN))
	{
		pDimWindows.TypeRead = Type;
		pDimWindows.Config = OCRH_BALNKS_NO;
		pDimWindows.XRightBottom = (short)0;
		pDimWindows.YRightBottom = (short)4;
		pDimWindows.Size = (short)-1;
		pDimWindows.Height = (short)(14);

		Reply = LS150_SetOpticalWindows((HWND)hDlg,
										SUSPENSIVE_MODE,
										&pDimWindows,
										1);

		Reply = LS150_DocHandle((HWND)hDlg,
								SUSPENSIVE_MODE,
								NO_FRONT_STAMP,
								NO_PRINT_VALIDATE,
								READ_CODELINE_OPTIC,
								SIDE_FRONT_IMAGE,
								SCAN_MODE_256GR200,
								AUTOFEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								&NrDoc,
								0,
								0);
		if (Reply == LS_OKAY)
		{
			Reply = LS150_ReadCodeline((HWND)hDlg,'S',NULL,&ret,Code,&Length_Codeline);
			if (Reply == LS_OKAY)
			{
				if (HFrontGray)
					LSFreeImage((HWND)hDlg, &HFrontGray);
				Reply = LS150_ReadImage((HWND)hDlg,
									SUSPENSIVE_MODE,
									CLEAR_ALL_BLACK,
		//							(char)(TypeLS == TYPE_LS150_5 ? SIDE_FRONT_RED_IMAGE : SIDE_FRONT_IMAGE),
									SIDE_FRONT_IMAGE,
									NrDoc,
									(LPHANDLE)&HFrontGray,
									NULL,
									NULL,
									NULL);
				CodelineLetta= Code;
				// salvo le immagini
				
					//NomeFileImmagine = "..\\Dati\\FrontImage.bmp";
					for (int ii=0;ii<= NomeFileImmagine->Length-1;ii++)
					{
						StrNome[ii] = NomeFileImmagine->get_Chars(ii);
					}
					Reply = LSSaveDIB((HWND)hDlg,HFrontGray,StrNome);
					// libero le immagini
					if (HFrontGray)
						LSFreeImage((HWND)hDlg, &HFrontGray);
					
				LSDisconnect(Connessione,(HWND)hDlg);
			}
			else
			{	
				
			}
		}
	}
		
	*/
	return Reply;
}



int Ls150Class::Periferica::Identificativo()
{
	
	char IdentStr[64],Version[64],Date_Fw[64],LsName[64],SerialNumber[64],FeederVersion[64];
	char BoardNr[8];
	char CpldNr[8];
	short Connessione;
	

	CpldNr[1]= 0;
	CpldNr[2]= 0;
	//HWND 
	HINSTANCE hInst = 0;
	ReturnC->FunzioneChiamante = "Identificativo";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	// Chiamo la funzione per via documento
	if( Reply == LS_OKAY )
	{
		Reply =	LSUnitIdentify(Connessione,(HWND)0, IdentStr,LsName,Version,Date_Fw,SerialNumber,BoardNr, NULL, NULL, FeederVersion, NULL, NULL, NULL, NULL);
		//Reply =	LS150_Identify((HWND)0, SUSPENSIVE_MODE,IdentStr,LsName,Version,Date_Fw,BoardNr,FeederVersion,SerialNumber,NULL);
		if (Reply == LS_OKAY)
		{
			IdentStr[0] |= 0x40;
			IdentStr[1] |= 0x40;
			IdentStr[2] |= 0x40;
			IdentStr[3] |= 0x40;

			SIdentStr = Marshal::PtrToStringAnsi((IntPtr) (char *) IdentStr);
				
			SVersion = Marshal::PtrToStringAnsi((IntPtr) (char *)Version);
			SDateFW = Marshal::PtrToStringAnsi((IntPtr) (char *)Date_Fw);
			SLsName = Marshal::PtrToStringAnsi((IntPtr) (char *)LsName);
			SSerialNumber = Marshal::PtrToStringAnsi((IntPtr) (char *)SerialNumber);
			SFeederVersion = Marshal::PtrToStringAnsi((IntPtr) (char *)FeederVersion);
			SBoardNr = BoardNr[0].ToString();
			CpldNr[0] =  BoardNr[1];
			SCpldNr = Marshal::PtrToStringAnsi(static_cast<IntPtr>(CpldNr));
			
			TypeLS = GetTypeLS(LsName,Version,IdentStr);

			//ConfMicr = true;
			

			if( IdentStr[0] & 0x02 )						// 00000010
				ConfVelocitBassa = true;
			
			if( IdentStr[0] & 0x04 )						// 00000100
				ConfFeederMotorized = true;

			if( IdentStr[0] & 0x08 )						// 00001000
				ConfEndorserPrinter = true;

			if( IdentStr[0] & 0x20 )						// 00100000
				ConfVoidingStamp = true;

			if( IdentStr[1] & 0x01 )						// 00000001
				ConfScannerFront = true;

			if( IdentStr[1] & 0x02 )						// 00000010
				ConfScannerBack = true;

			ConfScannerGrigio = 1;
			if (IdentStr[1] & 0x04)							// 00000100
				ConfScannerGrigio = 0;

			if( IdentStr[1] & 0x08 )
			{
				if( IdentStr[1] & 0x10 )
					ConfBadgeReader = 3;
				else
					ConfBadgeReader = 2;
			}
			else if( IdentStr[1] & 0x10 )
					ConfBadgeReader = 1;
			
			if (IdentStr[1] & 0x20)							// 00100000
				ConfScannerGrigio = 0;
		}
		else
		{
			ReturnC->FunzioneChiamante = "LSIdentify";
		}
		LSDisconnect(Connessione,(HWND)hDlg);
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}
	ReturnC->ReturnCode = Reply;
	
	return Reply;
}







int Ls150Class::Periferica::CalibrazioneFoto()
{
	//HINSTANCE hInst = 0;
	short Connessione;
	unsigned short pDump[24],Threshold[24];
	int llBuffer=0;
	ReturnC->FunzioneChiamante= "CalibrazioneFoto";
	
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	//Reply = LS150_Open((HWND)hDlg,(HANDLE)hInst,SUSPENSIVE_MODE);
	// Chiamo la funzione per via documento
	if( Reply == LS_OKAY )
	{
		Reply = LSPhotosCalibration(Connessione,(HWND)hDlg,0);
		if (Reply == LS_OKAY)
		{
			LSReadPhotosValue(Connessione,(HWND)0,pDump,Threshold);
			if (Reply == LS_OKAY)
			{
				// Leggo il valore dei photo
				PhotoValue[0] = pDump[0];
				PhotoValue[1] = pDump[1];
				PhotoValue[2] = pDump[2];
				// Doppia sfogliatura..... 
				PhotoValueDP[0] = (unsigned char)pDump[3];
				PhotoValueDP[1] = (unsigned char)pDump[4];
				PhotoValueDP[2] = (unsigned char)pDump[5];

				PhotoValueDP[3] = (unsigned char)Threshold[0];
				PhotoValueDP[4] = (unsigned char)Threshold[1];
				PhotoValueDP[5] = (unsigned char)Threshold[2];
				PhotoValueDP[6] = (unsigned char)Threshold[3];
				PhotoValueDP[7] = (unsigned char)Threshold[4];
				PhotoValueDP[8] = (unsigned char)Threshold[5];

			}
			else
			{
				ReturnC->FunzioneChiamante= "LSReadPhotosValue";
			}
		}
		else
		{
			ReturnC->FunzioneChiamante= "LSPhotosCalibration";
		}
		LSDisconnect(Connessione,(HWND)hDlg);
	}
	else
	{
		ReturnC->FunzioneChiamante= "LSOpen";
	}
	ReturnC->ReturnCode = Reply;
	
	return Reply;
}


int Ls150Class::Periferica::CalSfogliatura()
{
	unsigned short pDump[24],Threshold[24];
	int llBuffer;
	HINSTANCE hInst = 0;
	short Connessione;
	char DLpercentage;
	DLpercentage = 0;

	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	//Reply = LS150_Open((HWND)0,(HANDLE)hInst,SUSPENSIVE_MODE);
	// Chiamo la funzione per via documento
	if( Reply == LS_OKAY )
	{
		// double leafing 
		//Reply = LSPhotosCalibration(Connessione,(HWND)hDlg,1);
		Reply = LSPhotosCalibrationEx(Connessione,(HWND)hDlg, 1, DLpercentage);
		if (Reply == LS_OKAY)
		{
			llBuffer = 32;
			LSReadPhotosValue(Connessione,(HWND)0,pDump,Threshold);
			//Reply = LS150_DumpPeripheralMemory((HWND)hDlg, (unsigned char *)pDump, llBuffer, 0, MEMORY_EEPROM);
			if( Reply == LS_OKAY )
			{
				// Leggo il valore dei photo
				PhotoValue[0] = pDump[0];
				PhotoValue[1] = pDump[1];
				PhotoValue[2] = pDump[2];
				// Doppia sfogliatura..... 
				PhotoValueDP[0] = (unsigned char)pDump[3];
				PhotoValueDP[1] = (unsigned char)pDump[4];
				PhotoValueDP[2] = (unsigned char)pDump[5];

				PhotoValueDP[3] = (unsigned char)Threshold[0];
				PhotoValueDP[4] = (unsigned char)Threshold[1];
				PhotoValueDP[5] = (unsigned char)Threshold[2];
				PhotoValueDP[6] = (unsigned char)Threshold[3];
				PhotoValueDP[7] = (unsigned char)Threshold[4];
				PhotoValueDP[8] = (unsigned char)Threshold[5];

			}
			else
			{
				ReturnC->FunzioneChiamante= "LSReadPhotosValue";
			}
		}
		else
		{
			ReturnC->FunzioneChiamante= "LSPhotosCalibration";
		}
		LSDisconnect(Connessione,(HWND)hDlg);	
	}
	else
	{
		ReturnC->FunzioneChiamante= "LSOpen";
	}
	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Calibrazione Doppia Sfogl.";
	return Reply;
}


int Ls150Class::Periferica::TestMicroForatura()
{

	HANDLE HBackGray=0;
	HANDLE HFrontGray=0;
	short Connessione;
	struct MHTESTER_Criteria criteria;
	struct MHTESTER_PatternInfo patternInfo;
    char bPassed;
	MHTESTER_HANDLE handle;
	int error;
	//char *StrNome=NULL;

 
	ReturnC->FunzioneChiamante = "TestMicroForatura";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	if( Reply == LS_OKAY )
	{
		
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);
		
	
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								SIDE_ALL_IMAGE,
								SCAN_MODE_256GR300 , //obbligatorio
								AUTOFEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);
	

		if( Reply == LS_OKAY )
		{
	
		 Reply = LSReadImage(Connessione, (HWND)hDlg,
										NO_CLEAR_BLACK,
										SIDE_ALL_IMAGE,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);

			//StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
			//Reply = LSSaveDIB((HWND)hDlg,  HFrontGray,StrNome);


			if(Reply == LS_OKAY)
			{
				
				Reply = MHTESTER_getCriteria( "LS150", &criteria );
				if ( Reply == MHTESTER_OK )
				{
					

					handle = MHTESTER_new( ( const char * ) HFrontGray, &criteria, &error );
					if ( handle != NULL )
					{
						
						MHTESTER_getPatternInfo( handle, &patternInfo );
						MHTESTER_passed( handle, &bPassed );
						MHTESTER_free( handle );
						
						if (bPassed == 0x00 )
							Reply = patternInfo.errorFlags ;
						else
							Reply = LS_OKAY ;

						errorFlags = patternInfo.errorFlags  ;
						innerDimValidPerc = patternInfo.innerDimValidPerc ; 

						outerDimValidPerc = patternInfo.outerDimValidPerc ; 
						innerLevValidPerc = patternInfo.innerLevValidPerc ; 
						outerLevValidPerc = patternInfo.outerLevValidPerc ; 
						wDimStatmean = patternInfo.wDimStat.mean;
					    wDimStatStdDev = patternInfo.wDimStat.stdDev;
						bDimStatmean = patternInfo.bDimStat.mean ;
						bDimStatStdDev = patternInfo.bDimStat.stdDev; 
						gDimStatmean = patternInfo.gDimStat.mean ;
						gDimStatDev =patternInfo.gDimStat.stdDev ;
						wLevStatmean =patternInfo.wLevStat.mean;
						wLevStatStdDev =patternInfo.wLevStat.stdDev; 
						bLevStatmean =patternInfo.bLevStat.mean ;
						bLevStatStdDev =patternInfo.bLevStat.stdDev ;

					}
				}
             }
			
			LSDisconnect(Connessione,(HWND)hDlg);
		}
		else
		{
			ReturnC->FunzioneChiamante = "LSDocHandle";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}
	
	// libero le immagini
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	ReturnC->ReturnCode = Reply;
	
	return Reply;
}



//********************************************************************
// Controlla l'uniformita' dello sfondo
//
// Il test fallisce se trovo una riga che supera il valore di 0x40
// oppure se trovo 10 righe contigue che stanno tra 0x20 e 0x40
// oppure se trovo 80 righe in totale che stanno tra 0x20 e 0x40
//
#define NR_PIXEL_BACKGROUND				7
#define GR256_BMPHEADER					1064
//********************************************************************
int CheckBackground(HWND hDlg, HANDLE hImage, short *Row)
{
	int 	rc;
	long	height, width, diff;
	long	width_bytes;
	short	cc, rr;
	unsigned char 	*pData;
	short	Pixel;
	short	CounterContigue, CounterTotale;

	rc = 0;
	*Row = -1;
	CounterContigue = CounterTotale = 0;

	height = ((BITMAPINFOHEADER *)hImage)->biHeight;
	width = ((BITMAPINFOHEADER *)hImage)->biWidth;

	// Inizializzo i parametri per copiare i dati
	pData = (unsigned char *)hImage + GR256_BMPHEADER;
	width_bytes = width;
	if( (diff = width_bytes % 4) )
		width_bytes += (4 - diff);

	for( rr = 0; rr < height; rr ++)
	{
		Pixel = 0;
		for( cc = 0; cc < NR_PIXEL_BACKGROUND; cc ++)
		{
			Pixel += *(pData + cc);
		}

		Pixel /= NR_PIXEL_BACKGROUND;

		if( rc == 0 )
		{
			// Controllo se la media sta sopra il valore 0x40
			if( Pixel >= 0x40 )
			{
				if( *Row == -1 )
					*Row = rr;
				rc = 1;		// Ho trovato una riga fuori valore !!!
			}

			// Controllo se la media sta sopra il valore 0x30
			if( Pixel > 0x20 )
			{
				CounterContigue ++;
				CounterTotale ++;

				if( CounterContigue >= 10 )
				{
					if( *Row == -1 )
						*Row = rr;
					rc = 2;		// Ho trovato una riga fuori valore !!!
				}

				if( CounterTotale > 80 )
				{
					if( *Row == -1 )
						*Row = rr;
					rc = 3;		// Ho trovato una riga fuori valore !!!
				}
			}
			else
				CounterContigue = 0;	// Riazzero il counter delle righe contigue
		}

		// Passo alla riga successiva
		pData += width_bytes;
	}

	return rc;
} // CheckBackground




//********************************************************************
// Controlla l'uniformita' dello sfondo
//unsigned long	NrDocTest;
//********************************************************************
int Ls150Class::Periferica::TestBackground(int  fZebrato)
{
	int		Reply, rc;
	HANDLE	pFront, pRear;
	short	row;
	char	sMsg[128];
//	unsigned char	SenseKey, StatusByte[8];
	int		SavefOptionFitImage;
	short Connessione;
	BOOL fOptionFitImage = FALSE ;
	char *StrNome = NULL; 
	//FILE *fh ; 


	//-----------Open--------------------------------------------
	//SetLSConfiguration(hDlg);
	//Reply = LSConnect(hDlg, hInst, LS_Model, &hLS);
	ReturnC->FunzioneChiamante = "TestBackground";
	
	//effettuo la Connect Diagnostica per ricaricare i dati di compensazione su Ls40 Colore...
    Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	if( Reply == LS_OKAY || Reply == LS_ALREADY_OPEN )
	{
		/*Reply = LSConfigDoubleLeafingAndDocLength(hLS, hDlg,
												  DOUBLE_LEAFING_DISABLE,
												  stParDocHandle.DL_Value,
												  stParDocHandle.DL_MinDoc, stParDocHandle.DL_MaxDoc);*/
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);

		//LSPeripheralStatus(hLS, hDlg, &SenseKey, StatusByte);
		//if( ! (StatusByte[0] & MASK_FEEDER_EMPTY) )
		//{
			//Beep(700, 700);
			//MessageBox(hDlg, "Insert a document in the feeder", TITLE_POPUP, MB_OK | MB_ICONINFORMATION);
		//}

		// Leggo il documento
		Reply = LSDocHandle(Connessione, (HWND)hDlg,
							NO_STAMP,
							NO_PRINT_VALIDATE,
							NO_READ_CODELINE,
							(char)SIDE_ALL_IMAGE,
							SCAN_MODE_256GR300,
							AUTOFEED,
							SORTER_BAY1,
							WAIT_YES,
							NO_BEEP,
							NULL,
							0, 0);
		/*if( fh = fopen("Trace_BackGround.txt", "at") )
					{
						fprintf(fh, "Reply DOC HANDLE= %d ", Reply);
						fclose( fh );
					}*/
		if( Reply == LS_OKAY)
		{
			// Leggo l'immagine senza clear
			Reply = LSReadImage(Connessione, (HWND)hDlg,
								NO_CLEAR_BLACK,
								SIDE_ALL_IMAGE,
								0, 0,
								&pFront,
								&pRear,
								NULL,
								NULL);
			/*if( fh = fopen("Trace_BackGround.txt", "at") )
					{
						fprintf(fh, "Reply READ IMAGE= %d ", Reply);
						fclose( fh );
					}*/
			if( Reply == LS_OKAY )
			{
				// Setto FitImage = FALSE per avere la window normal
				SavefOptionFitImage = fOptionFitImage;	// Salvo il valore
				fOptionFitImage = FALSE;

				//// Visualizzo i dati ritornati
				//ShowCodelineAndImage(LS_OKAY,	//Reply,
				//					 0,
				//					 NrDocTest ++,
				//					 (unsigned char *)pFront,
				//					 (unsigned char *)pRear,
				//					 NULL, NULL, NULL,
				//					 NULL, NULL, NULL, NULL);

				fOptionFitImage = SavefOptionFitImage;	// Ripristino il vecchio valore

				// salvo le immagini
				//if ((Side == SIDE_ALL_IMAGE) ||(Side == SIDE_FRONT_IMAGE))
				{

					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
					Reply = LSSaveDIB((HWND)hDlg,  pFront,StrNome);

					 // Always free trhe unmanaged string.
					 Marshal::FreeHGlobal(IntPtr(StrNome));

				  	StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();
					Reply = LSSaveDIB((HWND)hDlg,pRear,StrNome);

					 // Always free the unmanaged string.
					 Marshal::FreeHGlobal(IntPtr(StrNome));
				}
				
				if ( fZebrato == 1 ) 
				{
					//rc  = 0 ;
					rc = CheckBackground((HWND)hDlg, pRear, &row);
					/*if( fh = fopen("Trace_BackGround.txt", "at") )
					{
						fprintf(fh, "Reply REAR= %d ", rc);
						fclose( fh );
					}*/
					switch( rc )
					{
					case 1:
						sprintf(sMsg, "REAR background row = %d NOT adequate !\n\nOne or more row up 64 level !!!", row);
						rc  = LS_BACK_BACKGROUND; 
						//MessageBox(hDlg, sMsg, TITLE_POPUP, MB_OK | MB_ICONERROR);
						break;
					case 2:
						sprintf(sMsg, "REAR background row = %d NOT adequate !\n\nMore then 10 row contiguous over the level of 32 !!!", row);
						//MessageBox(hDlg, sMsg, TITLE_POPUP, MB_OK | MB_ICONERROR);
						rc  = LS_BACK_BACKGROUND_1; 
						break;
					case 3:
						sprintf(sMsg, "REAR background row = %d NOT adequate !\n\nMore then 80 row over the level of 32 !!!", row);
						rc  = LS_BACK_BACKGROUND_2 ; 
						//MessageBox(hDlg, sMsg, TITLE_POPUP, MB_OK | MB_ICONERROR);
						break;
					}
				}
				else
				{
					
					// Controllo lo sfondo delle 2 immagini
					rc = CheckBackground((HWND)hDlg, pFront, &row);
					/*if( fh = fopen("Trace_BackGround.txt", "at") )
					{
						fprintf(fh, "Reply FRONT= %d ", rc);
						fclose( fh );
					}*/
					if( rc == 0  )
					{
						rc = CheckBackground((HWND)hDlg, pRear, &row);
						/*if( fh = fopen("Trace_BackGround.txt", "at") )
						{
							fprintf(fh, "Reply REAR= %d ", rc);
							fclose( fh );
						}*/
						if( rc )
						{
							switch( rc )
							{
							case 1:
								sprintf(sMsg, "REAR background row = %d NOT adequate !\n\nOne or more row up 64 level !!!", row);
								rc  = LS_BACK_BACKGROUND; 
								//MessageBox(hDlg, sMsg, TITLE_POPUP, MB_OK | MB_ICONERROR);
								break;
							case 2:
								sprintf(sMsg, "REAR background row = %d NOT adequate !\n\nMore then 10 row contiguous over the level of 32 !!!", row);
								//MessageBox(hDlg, sMsg, TITLE_POPUP, MB_OK | MB_ICONERROR);
								rc  = LS_BACK_BACKGROUND_1; 
								break;
							case 3:
								sprintf(sMsg, "REAR background row = %d NOT adequate !\n\nMore then 80 row over the level of 32 !!!", row);
								rc  = LS_BACK_BACKGROUND_2 ; 
								//MessageBox(hDlg, sMsg, TITLE_POPUP, MB_OK | MB_ICONERROR);
								break;
							}
						}
						else
						{
							//MessageBox(hDlg, "Check background PASSED !", TITLE_POPUP, MB_OK | MB_ICONINFORMATION);
							rc = 0 ; 
						}
					}
					else
					{
						switch( rc )
						{
						case 1:
							sprintf(sMsg, "FRONT background row = %d NOT adequate !\n\nOne or more row up 64 level !!!", row);
							rc  = LS_FRONT_BACKGROUND; 
							//MessageBox(hDlg, sMsg, TITLE_POPUP, MB_OK | MB_ICONERROR);
							break;
						case 2:
							sprintf(sMsg, "FRONT background row = %d NOT adequate !\n\nMore then 8 row contiguous over the level of 32 !!!", row);
							rc  = LS_FRONT_BACKGROUND_1; 
							//MessageBox(hDlg, sMsg, TITLE_POPUP, MB_OK | MB_ICONERROR);
							break;
						case 3:
							sprintf(sMsg, "FRONT background row = %d NOT adequate !\n\nMore then 80 row over the level of 32 !!!", row);
							rc = LS_FRONT_BACKGROUND_2; 
							//MessageBox(hDlg, sMsg, TITLE_POPUP, MB_OK | MB_ICONERROR);
							break;
						}
					}
				}

				LSFreeImage((HWND)hDlg, &pFront);
				LSFreeImage((HWND)hDlg, &pRear);
			}
			else
			{
				//CheckReply(hDlg, Reply, "LS150_ReadImage");
				ReturnC->FunzioneChiamante = "LSReadImage";
			}
		}
		else
		{
			//CheckReply(hDlg, Reply, "LS150_DocHandle");
			ReturnC->FunzioneChiamante = "LSDocHandle";
		}

		LSDisconnect(Connessione,(HWND)hDlg);
	}
	else
	{
		//CheckReply(hDlg, Reply, "LS150_Open");
		ReturnC->FunzioneChiamante = "LSOpen";
	}

	return rc;
} // TestBackground


int Ls150Class::Periferica::SetTrimmer(short TrimmerValue , char TypeSpeed)
{
	short	hLS;
	short	Reply;

	//ReturnC->FunzioneChiamante = "SetTrimmer";

	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&hLS);
	if( Reply == LS_OKAY )
	{
		Reply = LSSetTrimmerMICR(hLS, (HWND)hDlg, TypeSpeed, TrimmerValue);

		LSDisconnect(hLS, (HWND)hDlg);
	}

	return Reply;
}


//
// Esecuzione lettura documento per calcolo media valori testina MICR
//
int Ls150Class::Periferica::DoMICRCalibration3(int MICR_Value, long NrReadGood, char TypeSpeed, ListBox^ list)
{
	short	hLS;
	long	ii;
	short	Reply;
	short	ScanMode, Stamp;
	short	Retry;
	float	TotValRif, MinValue;
	short	fRetryLetture, fRetryCalibration;
	short	NrPicchi, NrPicchiMin, TotPicchi;
	float	RangeMore, RangeLess;
	float	Percento;
	char	BufCodelineHW[CODE_LINE_LENGTH];
	short	len_codeline;
	short	TrimmerValue, NewTrimmerValue, r33k_TrimmerValue;
	unsigned char buffDati[NR_BYTE_BUFFER_E13B];		// Dati da chiedere
	long    llDati; 					// Dati da chiedere
	long	ValMin;
	float	SommaMin, TotSommeMin, TotaleMin;
	BOOL	fSetDiagnostic;
	short	nrLettura ;
	char	IdentStr[64],Version[64],LsName[64];
	float 	Valore_100_in_Volt;
	double  Unita_Volt;



	
	// Forzo i valori di test
	MICR_TolleranceLess = 4;
	MICR_TolleranceMore = 4;
	MICR_DocValue = MICR_Value;
	NrReadGood = 3;

	// Calcolo tensione di riferimento ed unita' di Volt !!!
	Valore_100_in_Volt = (float)(TENSIONE_VCC * MOLTIPL_RIFERIMENTO_VOLT);
	Unita_Volt = TENSIONE_VCC / NR_VALORI_CONVERTITORE;

	// Setto i valori iniziali
	fRetryLetture = fRetryCalibration = 0;
	MinValue = TotValRif = 0;
	r33k_TrimmerValue = TRIMMER_NO_VALUE;
	Retry = 1;
	nrLettura = 0;
	//Percentile = 0;
	TotPicchi = 0;
	TotSommeMin = 0;
	TrimmerValue = 0;
	Stamp = NO_STAMP;

	if( TypeSpeed == TEST_SPEED_200_DPI )
		ScanMode = SCAN_MODE_256GR200;
	else if( TypeSpeed == TEST_SPEED_300_DPI )
		ScanMode = SCAN_MODE_256GR300;
	else if( TypeSpeed == TEST_SPEED_75_DPM )
		ScanMode = SCAN_MODE_256GR200;
	else if( TypeSpeed == TEST_SPEED_300_DPI_UV )
		ScanMode = SCAN_MODE_256GR300_AND_UV;
	else if( TypeSpeed == TEST_SPEED_300_COLOR )
		ScanMode = SCAN_MODE_COLOR_300;


	// -------Open ----------
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&hLS);

	//Reply = LS150_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);

	if( Reply == LS_OKAY )
	{
		//----------- Disabilito la doppia sfogliatura -----------
		//Reply = LSConfigDoubleLeafingAndDocLength(hLS, (HWND)hDlg, DOUBLE_LEAFING_DISABLE, 50, 150, 215);
		Reply = LSConfigDoubleLeafingAndDocLength(hLS, (HWND)hDlg, DOUBLE_LEAFING_DISABLE, 50, 80, 225);

		//----------- Set attesa introduzione documento -----------
		Reply = LSDisableWaitDocument(hLS,(HWND)hDlg, FALSE);

		//----------- Set speed document -----------
		Reply = LSSetUnitSpeed(hLS, (HWND)hDlg, SPEED_DEFAULT);

		// Setto il valore del trimmer a meta' (16)
//		TrimmerValue = 16;
//		Reply = LSSetTrimmerMICR(hLS, hDlg, TypeSpeed, TrimmerValue);
		// Lettura segnale
		Reply = LSReadTrimmerMICR(hLS, (HWND)hDlg, TypeSpeed, buffDati, NR_BYTES_TRIMMER);

		if( Reply == LS_OKAY)
		{
			// Salvo il valore del trimmer
			TrimmerValue = buffDati[0];

			// Eseguo il primo loop di misurazione di 3 letture
			for( ii = 0; ii < (TRIMMER_PRE_READ + NrReadGood); ii++ )
			{
retry_verifica:
				// Controllo la configurazione della periferica
				fSetDiagnostic = FALSE;
				//-----------Identify--------------------------------
				Reply = LSUnitIdentify(hLS,(HWND) hDlg, IdentStr, LsName, Version, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
				if( Reply == LS_OKAY )
				{
					TypeLS = GetTypeLS(LsName, Version, IdentStr);

					if( TypeSpeed == TEST_SPEED_75_DPM )
					{
#if TimbroAlMomentoSoppresso
						// Se c'� lo stamp lo abilito
						if( IdentStr[0] & MASK_STAMP )
						{
							Stamp = FRONT_STAMP;
						}
						else
#endif
						{
							LSSetUnitSpeed(hLS, (HWND)hDlg, SPEED_STAMP);
						}
					}
					else //if( TypeSpeed == TEST_SPEED_300_DPI )

					{
						// Set Diagnostic
						fSetDiagnostic = TRUE;

						// Metto la periferica in diagnostica
						LSSetDiagnosticMode(hLS, (HWND)hDlg, TRUE);
					}
				}


				nrLettura ++;

				// Chiamo la funzione per via documento
				Reply = LSDocHandle(hLS, (HWND)hDlg,
									Stamp,
									NO_PRINT_VALIDATE,
									READ_CODELINE_MICR,
									SIDE_NONE_IMAGE,
									ScanMode,
									AUTOFEED,
									SORTER_BAY1,
									WAIT_YES,
									NO_BEEP,
									NULL,
									0,
									0);

				if( Reply == LS_OKAY )
				{
					// Leggo la codeline Per liberare i buffer periferica
					len_codeline = CODE_LINE_LENGTH;
					LSReadCodeline(hLS, (HWND)hDlg,
								   BufCodelineHW,
								   &len_codeline,
								   NULL, NULL,
								   NULL, NULL);
				}


				// Risetto la velocit� impostata
				if( fSetDiagnostic )
					// Tolgo il diagnostic mode ...
					LSSetDiagnosticMode(hLS, (HWND)hDlg, FALSE);
				else
					LSSetUnitSpeed(hLS, (HWND)hDlg, SPEED_DEFAULT);



				// Testo il reply della DocHandle()
				if( Reply == LS_OKAY )
				{
					// Leggo i dati di diagnostica
					memset(buffDati, 0, sizeof(buffDati));
					llDati = NR_BYTE_BUFFER_E13B;		// 16 Kb.
					Reply = LSReadE13BSignal(hLS, (HWND)hDlg, buffDati, &llDati);
					if( Reply == LS_OKAY )
					{
						/*sprintf(FileOut, "dati\\SgnE13B_LS150_%04d.cts", stParDocHandle.NrNomeFile++);
						if( (fh = fopen(FileOut, "wb")) != NULL )
						{
							fwrite(buffDati, sizeof(unsigned char), llDati, fh);
							fclose( fh );
						}*/


						// Controllo se hanno inserito il documento giusto !
						//if( DistanzaPicco(buffDati, llDati) < PICCO_GAIN_DISTANZA_MINIMA )
						//{
						//	MessageBox(hDlg, "Wrong document or device assambled improperly !", TITLE_ERROR, MB_OK | MB_ICONERROR);
						//	Reply = LS_CALIBRATION_FAILED;
						//	break;
						//}


						// Calcolo la somma dei valori minimi ...
						EstraiDatiSpeedE13B(buffDati, llDati, &NrPicchi, &SommaMin, &ValMin, &NrPicchiMin);

						// Controllo se ho trovato tutti i picchi
						if( NrPicchi == NR_PICCHI )
						{
							TotSommeMin += SommaMin;
							TotPicchi += NrPicchi;

							// Calcolo il valore da visualizzare in bitmap
							// SOLO se NrPicchi diverso da zero
							if( NrPicchi )
							{
								SommaMin /= NrPicchi;
								SommaMin = ZERO_TEORICO - SommaMin;
								SommaMin *= (float)Unita_Volt;			// Converto in Volt
								Percento = SommaMin * 100 / Valore_100_in_Volt;

								// Disegna segnale e visualizza la bitmap
								//ShowMICRSignal(hDlg, buffDati, llDati, Percento, 0, 0, 0, TrimmerValue);
								ShowMICRSignal(Percento,TrimmerValue);

								// Controllo se il valore e' fuori dal +- 4%
								if( (Percento < ((float)MICR_DocValue * 0.96f)) || (Percento > ((float)MICR_DocValue * 1.04f)) )
								{
									// ed ho un valore valido per il 33K ... lo sostituisco e lo setto
									if( r33k_TrimmerValue > TRIMMER_NO_VALUE )
									{
										TrimmerValue = r33k_TrimmerValue;
										Reply = LSSetTrimmerMICR(hLS, (HWND)hDlg, TypeSpeed, TrimmerValue);

										if( Reply != LS_OKAY )
										{
											CheckReply((HWND)hDlg, Reply, "LS150_SetTrimmerMICR");
											break;
										}
										r33k_TrimmerValue = TRIMMER_NO_VALUE;

										// Scalo un giro
//										ii --;
										goto retry_verifica;
									}
								}
							}

							// Se il valore minimo � zero ci sono picchi in saturazione
							// dimezzo il valore del trimmer e ... ricomincio da capo !!!
							if( ValMin )
							{
								if( ii == (TRIMMER_PRE_READ - 1) )
								{
ricalcolo_trimmer:
									// Calcolo il valore per regolare il trimmer
									// SOLO se TotPicchi diverso da zero ...
									if( TotPicchi )
									{
										TotaleMin = TotSommeMin / TotPicchi;
										TotaleMin = ZERO_TEORICO - TotaleMin;
										TotaleMin *= (float)Unita_Volt;			// Converto in Volt
										Percento = TotaleMin * 100 / Valore_100_in_Volt;

										// Calcolo il nuovo valore del trimmer ...
										if( TypeLS == TYPE_LS150_VE )
											NewTrimmerValue = Calc_Trimmer_Position_ev(Percento, (unsigned char)TrimmerValue);
										else
										{
											NewTrimmerValue = Calc_Trimmer_Position(Percento, (unsigned char)TrimmerValue, FALSE);
											r33k_TrimmerValue = Calc_Trimmer_Position(Percento, (unsigned char)TrimmerValue, TRUE);
										}
									}
									else
									{
										// Il segnale � in SATURAZIONE !!!
										if( r33k_TrimmerValue > TRIMMER_NO_VALUE )
										{
											// Ho il valore del calcolo con la resistenza da 33K
											// setto questo !
											NewTrimmerValue = r33k_TrimmerValue;

											ii --;		// Scalo solo UNA lettura
										}
										else
										{
											// Decremento il valore del trimmer di 11
											NewTrimmerValue = TrimmerValue - 11;

											ii = -1;			// Cos� ricomincia da inizio pre letture
										}

										TotSommeMin = 0;	// Riazzero il totale delle somme
										TotPicchi = 0;		// Riazzero il totale picchi
									}


									// Se il valore ritornato � uguale zero 
									// Fallisco la calibrazione !!!!!
									if( NewTrimmerValue <= TRIMMER_NO_VALUE )
									{
										// Salvo il valore da ritornare
										//*Percentile = NewTrimmerValue;
										Reply = LS_MICR_TRIMMER_VALUE_NEGATIVE;
										break;
									}

									// Testo se sono nel range accettato dal trimmer
									else if( NewTrimmerValue < TRIMMER_MIN_VALUE )
									{
										// Controllo se ho gi� fatto 3 tentativi ... esco !
										if( fRetryCalibration < 3 )
										{
											NewTrimmerValue = TRIMMER_MIN_VALUE;
											ii = -1;			// Cos� ricomincia da inizio pre letture
											fRetryCalibration ++;
										}
										else
										{
											// Salvo il valore da ritornare
											//*Percentile = NewTrimmerValue;
											Reply = LS_CALIBRATION_FAILED;
											break;
										}
									}

									else if( NewTrimmerValue > TRIMMER_MAX_VALUE )
									{
										// Controllo se ho gi� fatto 3 tentativi ... esco !
										if( fRetryCalibration < 3 )
										{
											NewTrimmerValue = TRIMMER_MAX_VALUE;
											ii = -1;			// Cos� ricomincia da inizio pre letture
											fRetryCalibration ++;
										}
										else
										{
											// Salvo il valore da ritornare
											Percentile = NewTrimmerValue;
											Reply = LS_CALIBRATION_FAILED;
											break;
										}
									}

	//								if( (NewTrimmerValue <= TRIMMER_MIN_VALUE) ||
	//									(NewTrimmerValue >= TRIMMER_MAX_VALUE) )
	//								{
	//									Reply = LS800_CALIBRATION_FAILED;
	//									*Percentile = NewTrimmerValue;
	//									break;
	//								}


									// ... e setto il nuovo valore del trimmer
									TrimmerValue = NewTrimmerValue;
									Reply = LSSetTrimmerMICR(hLS, (HWND)hDlg, TypeSpeed, TrimmerValue);

									if( Reply != LS_OKAY )
									{
										CheckReply((HWND)hDlg, Reply, "LS150_SetTrimmerMICR");
										break;
									}

									// NON Riazzero il totale delle somme e dei picchi
									// Le utilizzo per un'eventuale ricalcolo trimmer
									TotSommeMin = 0;
									TotPicchi = 0;
								}
								else if( ii == ((TRIMMER_PRE_READ + NrReadGood) - 1) )
								{
									// Calcolo il valore delle POST LETTURE
									TotaleMin = TotSommeMin / TotPicchi;
									TotaleMin = ZERO_TEORICO - TotaleMin;
									TotaleMin *= (float)Unita_Volt;			// Converto in Volt
									Percento = TotaleMin * 100 / Valore_100_in_Volt;

									// Controllo se la lettura sta nel + o - 7% della tolleranza
									RangeMore = (float)MICR_DocValue * ((float)MICR_TolleranceMore / 100);
									RangeLess = (float)MICR_DocValue * ((float)MICR_TolleranceLess / 100);
									if( (Percento >= (MICR_DocValue - RangeLess)) &&
										(Percento <= (MICR_DocValue + RangeMore)) )
									{
										// Salvo il valore da ritornare, se ho letto tutti i documenti
										Percentile = Percento;
									}
									else
									{
										// se no controllo se ho gi� fatto 3 tentativi ... o esco !
										if( fRetryCalibration < 3 )
										{
											ii = 2;				// Cosi' ricalocolo il trimmer
											goto ricalcolo_trimmer;
//											ii = -1;			// Cosi' ricomincia da inizio pre letture
//											TotSommeMin = 0;	// Riazzero il totale delle somme
//											TotPicchi = 0;		// Riazzero il totale picchi
											fRetryCalibration ++;
										}
										else
										{
											Reply = LS_CALIBRATION_FAILED;
											break;
										}
									}
								}
							}
							else // if( ValMin )
							{
								// ... e setto il nuovo valore del trimmer
								TrimmerValue /= 2;
								if( TrimmerValue )
								{
									Reply = LSSetTrimmerMICR(hLS, (HWND)hDlg, TypeSpeed, TrimmerValue);

									if( Reply != LS_OKAY )
									{
										CheckReply((HWND)hDlg, Reply, "LSSetTrimmerMICR");
										break;
									}

									ii = -1;			// Cos� ricomincia da inizio pre letture
									TotSommeMin = 0;	// Riazzero il totale delle somme
									TotPicchi = 0;		// Riazzero il totale picchi
								}
								else
								{
									Reply = LS_CALIBRATION_FAILED;
									break;
								}
							} // if( ValMin )
						}
						else // if( NrPicchi == NR_PICCHI )
						{
							// Disegna segnale e visualizza la bitmap
							ShowMICRSignal(Percento,TrimmerValue);
							//ShowMICRSignal(hInst, hDlg, buffDati, llDati, Percento, 0, 0, 0, TrimmerValue);
							
							// Decremento ii per ripetere l'acquisizione !!!
							ii --;

							fRetryLetture ++;
							if( fRetryLetture >= MAX_READ_RETRY )
							{
								//MessageBox(hDlg, "Document loose !!!\n\nChange Document !", TITLE_ERROR, MB_OK | MB_ICONERROR);
								Reply = LS_CALIBRATION_FAILED;
								break;
							}

							// Se NON ho segnale provo a mettere il default !
							TrimmerValue = 50;
							Reply = LSSetTrimmerMICR(hLS, (HWND)hDlg, TypeSpeed, TrimmerValue);
							// E non faccio altri retry !
							fRetryLetture = 3;

						} // if( NrPicchi == NR_PICCHI )
					}
					else
					{
						CheckReply(0, Reply, "LSReadE13BSignal");
						LSDisconnect(hLS,(HWND)hDlg);		// Faccio la close
						return LS_CALIBRATION_FAILED;
					}
				}
				else
				{
					CheckReply(0, Reply, "LSDocHandle");
					break;
				}
			} // fine for
		}
		else
		{
			CheckReply(0, Reply, "LSReadTrimmerMICR");
		}

		// Tolgo il diagnostic mode ...
		LSSetDiagnosticMode(hLS, (HWND)hDlg, FALSE);
		// ... Faccio un reset per le versioni che NON lo supportano (cos� le lascio ok)
		LSReset(hLS, (HWND)hDlg, RESET_ERROR);

		LSDisconnect(hLS, (HWND)hDlg);
	}
	else
	{
		CheckReply(0, Reply, "LSOpen");
	}
	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Calibrazione MICR";

	//prova
	ValoreCalibrazione = TrimmerValue ;
	
	if (TypeSpeed == TEST_SPEED_300_COLOR)
	{
		TrimmerValue300C = (unsigned char)TrimmerValue;
	}
	if (TypeSpeed == TEST_SPEED_200_DPI)
	{
		TrimmerValue200 = (unsigned char)TrimmerValue;
	}
	if (TypeSpeed == TEST_SPEED_300_DPI)
	{
		TrimmerValue300 = (unsigned char)TrimmerValue;
	}
	if (TypeSpeed == TEST_SPEED_300_DPI_UV)
	{
		TrimmerValue300UV = (unsigned char)TrimmerValue;
	}
	if (TypeSpeed == TEST_SPEED_75_DPM)
	{
		TrimmerValue75 = (unsigned char)TrimmerValue;
	}

	return Reply;

} // DoMICRCalibration3



unsigned char Ls150Class::Periferica::Calc_Trimmer_Position(float ValoreLetto, unsigned char PosTrimmer, BOOL r33k_TrimmerValue)
{
	#define R_SERIE_TRIMMER_33K			33000	// Valore di restistenza presente
	#define R_SERIE_TRIMMER				8200	// Valore di restistenza presente
												// in serie al trimmer digitale
	#define R_X_STEP_TRIMMER			1000	// Valore resistivo corrispondente a ogni
												// step del trimmer digitale per LS150
	#define R_SERIE_TRIMMER_COLOR		3300	// Valore di restistenza presente
												// in serie al trimmer digitale
	#define R_X_STEP_TRIMMER_COLOR		39		// Valore resistivo corrispondente a ogni
												// step del trimmer digitale per LS150
	#define R_SERIE_TRIMMER_LEVEL_12	1200	// Valore di restistenza presente
												// in serie al trimmer digitale
	#define R_X_STEP_TRIMMER_LEVEL_12	39		// Valore resistivo corrispondente a ogni
												// step del trimmer digitale per LS150
	float	Var_Per_Sign;
	float	tmp;
	long	R_trimmer;
	short	R_x_step_trimmer;
	long	R_serie_trimmer;


	// Le piastre da livello 12 sono tutte uguali !!!
	if( System::Convert::ToInt16(SBoardNr) >= 0x12 )
	{
		// Di default setto il valore per LS150 colore
		R_serie_trimmer = R_SERIE_TRIMMER_LEVEL_12;
		R_x_step_trimmer = R_X_STEP_TRIMMER_LEVEL_12;
	}
	else if( TypeLS == TYPE_LS150_COLOR )
	{
		// Di default setto il valore per LS150 colore
		R_serie_trimmer = R_SERIE_TRIMMER_COLOR;
		R_x_step_trimmer = R_X_STEP_TRIMMER_COLOR;
	}
	else
	{
		// Di default setto il valore per LS150
		if( r33k_TrimmerValue )
			R_serie_trimmer = R_SERIE_TRIMMER_33K;
		else
			R_serie_trimmer = R_SERIE_TRIMMER;
		R_x_step_trimmer = R_X_STEP_TRIMMER;
	}

	//Calcolo la variazione percentuale del segnale della media rispetto
	//al valore del segnale magnetico indicato sul documento di test in
	//valore assoluto
#if _MSC_VER < 1500
	// Nel VC6 abs() tratta solo int !!!
	tmp = (float)abs((int)(ValoreLetto - (float)stParDocHandle.MICR_DocValue));
#else
	tmp = (float)abs((int)(ValoreLetto - (float)MICR_DocValue));
#endif
	Var_Per_Sign = tmp * 100.0f / ValoreLetto;

	//Calcolo il valore resistivo fornito dal trimmer nella sua posizione
	//attulale e gli sommo la resistenza presente in serie
	R_trimmer = (PosTrimmer * R_x_step_trimmer);
//	R_trimmer &= 0xFFFF;
	R_trimmer += R_serie_trimmer;

	//Determino se bisogna incrementare o decrementare l'ampiezza del segnale
	if( ValoreLetto > (float)MICR_DocValue )
		tmp = (100.0f - Var_Per_Sign);
	else
		tmp = (Var_Per_Sign + 100.0f);


	R_trimmer = (unsigned long)(((float)R_trimmer * tmp) / 100.0f );
	// Se la resistenza totale vale di pi� della resistenza fissa
	// dovrei settare un valore di trimmer negativo (non posso)
	// e quindi fisso il valore di trimmer a 0 !!!
	// Cos� la calibrazione fallir� !!!
	if( R_trimmer > R_serie_trimmer )
	{
		//Calcolo la variazione in percentuale da effettuare alla resistenza
		//complessiva presente sulla reazione negativa e di conseguenza
		//la nuova posizione del trimmer
		PosTrimmer = (unsigned char)((R_trimmer - R_serie_trimmer) / R_x_step_trimmer);
	}
	else
		PosTrimmer = TRIMMER_NO_VALUE;	// Errore !!!

	return PosTrimmer;
}



// ******************************************************************* //
// Calcolo della posizione del trimmer in base alla variazione         //
// percentuale del segnale della media rispetto al valore del segnale  //
// magnetico indicato sul documento di test                            //
// ******************************************************************* //
unsigned char Ls150Class::Periferica::Calc_Trimmer_Position_ev(float ValoreLetto, unsigned char PosTrimmer)
{
    #define LS150_EV_R_SERIE_TRIMMER			1800	// Valore di restistenza presente
											// in serie al trimmer digitale
    #define LS150_EV_R_X_STEP_TRIMMER		39		// Valore resistivo corrispondente a ogni
											// step del trimmer digitale per LS40
    float Var_Per_Sign;
    float tmp;
    unsigned long  R_trimmer;
	short R_x_step_trimmer;
	long  R_serie_trimmer;


	// Di default setto il valore per LS40
	R_serie_trimmer = LS150_EV_R_SERIE_TRIMMER;
	R_x_step_trimmer = LS150_EV_R_X_STEP_TRIMMER;

	//Calcolo la variazione percentuale del segnale della media rispetto
	//al valore del segnale magnetico indicato sul documento di test in
	//valore assoluto
	tmp = (float)abs((int)ValoreLetto - MICR_DocValue);
	Var_Per_Sign = tmp * 100 / ValoreLetto;

	//Calcolo il valore resistivo fornito dal trimmer nella sua posizione
	//attulale e gli sommo la resistenza presente in serie
	R_trimmer = (PosTrimmer * R_x_step_trimmer);
//	R_trimmer &= 0xFFFF;
	R_trimmer += R_serie_trimmer;

	//Determino se bisogna incrementare o decrementare l'ampiezza del segnale
	if( ValoreLetto > MICR_DocValue )
		tmp = (100 - Var_Per_Sign);
	else
		tmp = (Var_Per_Sign + 100);

	R_trimmer = (R_trimmer / 100) * (unsigned long)tmp;


	if( R_trimmer > (unsigned long)R_serie_trimmer )
	{
		//Calcolo la variazione in percentuale da effettuare alla resistenza
		//complessiva presente sulla reazione negativa e di conseguenza
		//la nuova posizione del trimmer
	
		PosTrimmer = (unsigned char)((R_trimmer - R_serie_trimmer) / R_x_step_trimmer);
	}
	else
		PosTrimmer = 0;	// Errore !!!

	return PosTrimmer;
}//Calc_Trimmer_Position_ev


int Ls150Class::Periferica::CalibraScanner()
{
	short Connessione;
	ReturnC->FunzioneChiamante = "Calibrazione Scanner";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	
	if( Reply == LS_OKAY )
	{
		Reply = LSScannerCalibration(Connessione,(HWND)hDlg, 0);
		LSDisconnect(Connessione,(HWND)hDlg);
	}

	ReturnC->ReturnCode = Reply;

	return Reply;
}

int Ls150Class::Periferica::CalibraScannerOld()
{
	short Connessione;
	ReturnC->FunzioneChiamante = "Calibrazione Scanner";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	
	if( Reply == LS_OKAY )
	{
		Reply = LSScannerCalibrationOld(Connessione,(HWND)hDlg, 0);
		LSDisconnect(Connessione,(HWND)hDlg);
	}

	ReturnC->ReturnCode = Reply;

	return Reply;
}

int Ls150Class::Periferica::CalibraScanner2()
{
	short Connessione;
	ReturnC->FunzioneChiamante = "Calibrazione Scanner";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	
	if( Reply == LS_OKAY )
	{
		Reply = LSScannerCalibration2(Connessione,(HWND)hDlg, 1,10);
		LSDisconnect(Connessione,(HWND)hDlg);
	}

	ReturnC->ReturnCode = Reply;

	return Reply;
}

int Ls150Class::Periferica::LeggiImmagini(char Side , short ScanMode,short Clear)
{

	HANDLE HBackGray=0;
	HANDLE HFrontGray=0;
	HANDLE HFrontUV=0;
	char *StrNome=NULL;
	short Connessione;
	

	short floop = false;
	ReturnC->FunzioneChiamante = "LeggiImmagini";
	
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	if( Reply == LS_OKAY )
	{
		
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);
		
		do
		{
		

			Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								SIDE_ALL_IMAGE , //Side,
								ScanMode,
								AUTOFEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);
			if (Reply == LS_FEEDER_EMPTY)
			{
				floop = TRUE;
			}
			else
				floop = FALSE;
		}while(floop);

		if( Reply == LS_OKAY )
		{

			Reply = LSReadImage(Connessione, (HWND)hDlg,
										Clear , // NO_CLEAR_BLACK,
										SIDE_ALL_IMAGE , //Side,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										(LPHANDLE)&HFrontUV,
										NULL);
				
			if(Reply == LS_OKAY)
			{
				// salvo le immagini
				if (Side == SIDE_FRONT_IMAGE)
				{
					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
					Reply = LSSaveDIB((HWND)hDlg,  HFrontGray,StrNome);

					if (ScanMode == SCAN_MODE_256GR200_AND_UV)
					{
						StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineUV ).ToPointer();
						Reply = LSSaveDIB((HWND)hDlg,  HFrontUV,StrNome);
					}
					// Always free trhe unmanaged string.
					Marshal::FreeHGlobal(IntPtr(StrNome));
				}
				else if (Side == SIDE_BACK_IMAGE)
				{
					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();
					Reply = LSSaveDIB((HWND)hDlg,HBackGray,StrNome);

					 // Always free the unmanaged string.
					 Marshal::FreeHGlobal(IntPtr(StrNome));
				}	
			}
			else
			{
				ReturnC->FunzioneChiamante = "LSReadImage";
			}
			LSDisconnect(Connessione,(HWND)hDlg);
		}
		else
		{
			ReturnC->FunzioneChiamante = "LSDocHandle";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}
	// libero le immagini
	if (HFrontUV)
		LSFreeImage((HWND)hDlg, &HFrontUV);
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	ReturnC->ReturnCode = Reply;
	

	return Reply;
}



int Ls150Class::Periferica::LeggiImmCard2(char Side,short ScanMode,short fPdf)
{

	HANDLE HBackGray=0;
	HANDLE HFrontGray=0;
	//char StrNome[256];
	char *StrNome= NULL ; 
	char Matri[32];
	short floop = false;
	short Connessione;
	char conf[5];
	char BarcodeRead[2700];
	char BarcodeRead2[256];
	int lenBarcodeRead;
	int lenBarcodeRead2;
	char BarcodeReadR[2700];
	char BarcodeRead2R[256];
	int lenBarcodeReadR;
	int lenBarcodeRead2R;
	short ErrorRate;



	conf[0] = 0x40;
	conf[1] = 0x40;	
	conf[2] = 0x40;
	conf[3] = 0x43;
	conf[4] = 0x0;
	strcpy_s(Matri, sizeof(Matri), "Ls150.000000");
	

	ReturnC->FunzioneChiamante = "LeggiImmCard2";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	//Reply = LS150_Open((HWND)hDlg,(HANDLE)hInst,SUSPENSIVE_MODE);

	if( Reply == LS_OKAY )
	{
		Reply = CheckLicense(conf,Matri);
		Sleep(200);

		/*if (HFrontGray)
			LSFreeImage((HWND)hDlg, &HFrontGray);
		if (HBackGray)
			LSFreeImage((HWND)hDlg, &HBackGray);*/

		//Reply = LS150_DocHandle((HWND)hDlg,
		//						SUSPENSIVE_MODE,
		//						NO_FRONT_STAMP,
		//						NO_PRINT_VALIDATE,
		//						NO_READ_CODELINE,
		//						Side,
		//						SCAN_MODE_256GR300,
		//						PATHFEED,

		//						SORTER_BAY1,
		//						WAIT_YES,
		//						NO_BEEP,
		//						//&NrDoc,
		//						0,
		//						0);
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								Side,
								ScanMode,//SCAN_MODE_256GR300,
								PATH_FEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_CARD,
								0);
	

		if( Reply == LS_OKAY )
		{
			// Leggo l'immagine filmata
		//	Reply = LS150_ReadImage((HWND)hDlg,
		//							SUSPENSIVE_MODE,
		//							CLEAR_ALL_BLACK,
		////							(char)(TypeLS == TYPE_LS150_5 ? SIDE_FRONT_RED_IMAGE : SIDE_FRONT_IMAGE),
		//							Side,
		//							(LPHANDLE)&HFrontGray,
		//							(LPHANDLE)&HBackGray,
		//							NULL,
		//							NULL);
			Reply = LSReadImage(Connessione, (HWND)hDlg,
										CLEAR_ALL_BLACK,
										Side,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);

			if(Reply == LS_OKAY)
			{
				if(Reply == LS_OKAY)
					{
						Marshal::FreeHGlobal(IntPtr(StrNome));
						StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
						
						LSSaveDIB((HWND)hDlg,HFrontGray,StrNome);

						Marshal::FreeHGlobal(IntPtr(StrNome));
						StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();
					

						LSSaveDIB((HWND)hDlg,HBackGray,StrNome);

					}
					else
					{
						ReturnC->FunzioneChiamante = "LSReadImage";
					}
			

				if (fPdf)
				{
					memset(BarcodeRead,0,sizeof(BarcodeRead));
					memset(BarcodeRead2,0,sizeof(BarcodeRead2));
					memset(BarcodeReadR,0,sizeof(BarcodeReadR));
					memset(BarcodeRead2R,0,sizeof(BarcodeRead2R));

					lenBarcodeRead = sizeof(BarcodeRead);
					lenBarcodeRead2 = sizeof(BarcodeRead2);
					ErrorRate = 0;

					/*Reply = LSReadPdf417FromBitmap((HWND)hDlg,
											   HFrontGray,
											   LPSTR	Codeline,
											   UINT		*Length,
											   char		*ErrorRate,
											   int		Reserved1,
											   int		Reserved2,
											   int		Reserved3,
											   int		Reserved4);*/



					Reply = LSReadBarcodesDriverLicense((HWND)hDlg,
													HFrontGray,
													ENCODE_NO,
													BarcodeRead,
													&lenBarcodeRead,
													&ErrorRate,
													READ_BARCODE_CODE128,
													BarcodeRead2,
													&lenBarcodeRead2,
													0,
													0,
													0,
													0);

					if(Reply == LS_OKAY)
					{
						
					

						lenBarcodeReadR = sizeof(BarcodeReadR);
						lenBarcodeRead2R = sizeof(BarcodeRead2R);
						ErrorRate = 0;
						Reply = LSReadBarcodesDriverLicense((HWND)hDlg,
													HBackGray,
													ENCODE_NO,
													BarcodeReadR,
													&lenBarcodeReadR,
													&ErrorRate,
													READ_BARCODE_CODE128,
													BarcodeRead2R,
													&lenBarcodeRead2R,
													0,
													0,
													0,
													0);
						

						if(Reply == LS_OKAY)
						{
							// controllo che le 2 linghezze dei buffer siano uguali
							PDFLetto= Marshal::PtrToStringAnsi((IntPtr) (char *)BarcodeRead);
							BarcodeLetto= Marshal::PtrToStringAnsi((IntPtr) (char *)BarcodeRead2);

							if (lenBarcodeReadR > 0 && lenBarcodeRead > 0 && (lenBarcodeRead == lenBarcodeReadR) && (PDFLetto->Length > 0))
							{
								// controllo che i buffer siano uguali
								if (memcmp(BarcodeReadR,BarcodeRead,sizeof(lenBarcodeReadR)) == 0) 
								{
									if (lenBarcodeRead2R > 0 && lenBarcodeRead2 > 0 && (lenBarcodeRead2 == lenBarcodeRead2R))
									{
										// controllo che i buffer siano uguali
										if (memcmp(BarcodeRead2R,BarcodeRead2,sizeof(lenBarcodeRead2)) == 0) 
										{
											// OK 
										}
										else
										{
											// secondo bardoce errore PDF
											Reply = -6001;
										}
									}
									else
									{
										// secondo lunghezza bardoce errore LUN PDF
										Reply = -6002;
									}

								}
								else
								{
									// buffer differenti !!!! 128
									Reply = -6003;
								}
							}
							else
							{
								// errore lunghezze differenti lun 128
								Reply = -6004;
							}


						}
						else
						{
							// errore lettura retro
						}
					}
					else
					{
						// fronte non letto
					}
				}
				
			}
			
			LSDisconnect(Connessione,(HWND)hDlg);
		}
		else
		{
			ReturnC->FunzioneChiamante = "LSDocHandle";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}
	// libero le immagini
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	ReturnC->ReturnCode = Reply;
	
	return Reply;
}




int Ls150Class::Periferica::LeggiImmCard(char Side, short ScanMode, short ^%NrDistanze)
{
	HANDLE HBackGray=0;
	HANDLE HFrontGray=0;
	char *StrNome= NULL;
	short floop = false;
	short Connessione;

	ReturnC->FunzioneChiamante = "LeggiImmCard";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	*NrDistanze = 0;

	//Reply = LS150_Open((HWND)hDlg,(HANDLE)hInst,SUSPENSIVE_MODE);
	
	if( Reply == LS_OKAY )
	{
		/*if (HFrontGray)
			LSFreeImage((HWND)hDlg, &HFrontGray);
		if (HBackGray)
			LSFreeImage((HWND)hDlg, &HBackGray);*/

		//Reply = LS150_DocHandle((HWND)hDlg,
		//						SUSPENSIVE_MODE,
		//						NO_FRONT_STAMP,
		//						NO_PRINT_VALIDATE,
		//						NO_READ_CODELINE,
		//						Side,
		//						SCAN_MODE_256GR300,
		//						PATHFEED,

		//						SORTER_BAY1,
		//						WAIT_YES,
		//						NO_BEEP,
		//						//&NrDoc,
		//						0,
		//						0);
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								Side,
								ScanMode,//SCAN_MODE_256GR300,
								PATH_FEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_CARD,
								0);
	

		if( Reply == LS_OKAY )
		{
			// Leggo l'immagine filmata
		//	Reply = LS150_ReadImage((HWND)hDlg,
		//							SUSPENSIVE_MODE,
		//							CLEAR_ALL_BLACK,
		////							(char)(TypeLS == TYPE_LS150_5 ? SIDE_FRONT_RED_IMAGE : SIDE_FRONT_IMAGE),
		//							Side,
		//							(LPHANDLE)&HFrontGray,
		//							(LPHANDLE)&HBackGray,
		//							NULL,
		//							NULL);
			Reply = LSReadImage(Connessione, (HWND)hDlg,
										CLEAR_ALL_BLACK,
										Side,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);

			
			if(Reply == LS_OKAY)
			{
				Marshal::FreeHGlobal(IntPtr(StrNome));
				StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
				
				LSSaveDIB((HWND)hDlg,HFrontGray,StrNome);

				Marshal::FreeHGlobal(IntPtr(StrNome));
				StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();


				LSSaveDIB((HWND)hDlg,HBackGray,StrNome);


				// COSTRUISCO IL GRAFICO DELLE BARRETTE

				*NrDistanze = CalcolaSalvaDistanze((HWND)hDlg,(BITMAPINFOHEADER*)HFrontGray,24);

			}
			else
			{
				ReturnC->FunzioneChiamante = "LSReadImage";
			}
			LSDisconnect(Connessione,(HWND)hDlg);
		}
		else
		{
			ReturnC->FunzioneChiamante = "LSDocHandle";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}
	// libero le immagini
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	ReturnC->ReturnCode = Reply;
	
	return Reply;
}




int Ls150Class::Periferica::DecodificaBarcode2D(char Side,short ScanMode,short fPdf)
{

	HANDLE HBackGray=0;
	HANDLE HFrontGray=0;
	char *StrNome= NULL ; 
	short floop = false;
	short Connessione;
	short indiceBarcode = 0 ;
	short indiceDataMatrix = 0 ; 

	char BarcodeRead[2700];
	int len_barcode;

	BarcodeLetto0 = "NO_PRESENT" ;
	BarcodeLetto1 = "NO_PRESENT";
	BarcodeLetto2 = "NO_PRESENT" ;
	DataMatrixLetto0 = "NO_PRESENT" ;
	DataMatrixLetto1 = "NO_PRESENT" ;
	DataMatrixLetto2 = "NO_PRESENT" ;

	ReturnC->FunzioneChiamante = "DecodificaBarcode2D";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	if( Reply == LS_OKAY )
	{
		
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);
		
	
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								Side,
								ScanMode,
								AUTOFEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);
	

		if( Reply == LS_OKAY )
		{
	
		 Reply = LSReadImage(Connessione, (HWND)hDlg,
										CLEAR_ALL_BLACK,
										Side,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);

			if(Reply == LS_OKAY)
			{
				Marshal::FreeHGlobal(IntPtr(StrNome));
				StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
						
				LSSaveDIB((HWND)hDlg,HFrontGray,StrNome);

				Marshal::FreeHGlobal(IntPtr(StrNome));
				StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();
					
				LSSaveDIB((HWND)hDlg,HBackGray,StrNome);

				memset(BarcodeRead,0,sizeof(BarcodeRead));
				//cerco il primo Barcode  con localizzazione automatica
				len_barcode = sizeof(BarcodeRead);
				Reply = LSReadBarcodeFromBitmap((HWND)hDlg,
												HFrontGray,
												READ_2D_BARCODE_DATAMATRIX,
												0 , //(int)stParDocHandle.Pdf417_Sw_x,
												0 , //(int)stParDocHandle.Pdf417_Sw_y,
												0 , //(int)stParDocHandle.Pdf417_Sw_w,
												0 , //(int)stParDocHandle.Pdf417_Sw_h,
												BarcodeRead,
												(UINT *)&len_barcode);
				if(Reply == LS_OKAY)
				{
							
					BarcodeLetto0= Marshal::PtrToStringAnsi((IntPtr) (char *)BarcodeRead);
					do
					{
						memset(BarcodeRead,0,sizeof(BarcodeRead));
						Reply = LSGetNextBarcode((HWND)hDlg,BarcodeRead,(long *)&len_barcode ); 
						if (Reply == LS_OKAY )  
						{
							indiceBarcode ++ ; 
							if (indiceBarcode == 1 ) 
								BarcodeLetto1 = Marshal::PtrToStringAnsi((IntPtr) (char *)BarcodeRead); 
							else if (indiceBarcode == 2 ) 
								BarcodeLetto2 = Marshal::PtrToStringAnsi((IntPtr) (char *)BarcodeRead); 
						}
					}while(Reply == LS_OKAY) ; 
						
				}
				else
					ReturnC->FunzioneChiamante = "LSReadBarcodeFromBitmap";
					
							
							
				if (Reply == LS_OKAY || Reply == LS_NO_MORE_DATA)
				{
					////cerco il datamatrix centrale , localizzazione automatica
				   	len_barcode = sizeof(BarcodeRead);
					Reply = LSReadBarcodeFromBitmap((HWND)hDlg,
												HFrontGray,
												READ_2D_BARCODE_QRCODE ,
												0 , //81 , //(int)stParDocHandle.Pdf417_Sw_x,
												0 , //17 , //(int)stParDocHandle.Pdf417_Sw_y,
												0, //35 , //(int)stParDocHandle.Pdf417_Sw_w,
												0, //35 , //(int)stParDocHandle.Pdf417_Sw_h,
												BarcodeRead,
												(UINT *)&len_barcode);
						
							
					DataMatrixLetto0= Marshal::PtrToStringAnsi((IntPtr) (char *)BarcodeRead);
					if ( Reply == LS_OKAY )
					{
						do
						{
							memset(BarcodeRead,0,sizeof(BarcodeRead));
							Reply = LSGetNextBarcode((HWND)hDlg,BarcodeRead,(long *)&len_barcode ); 
							if (Reply == LS_OKAY )  
							{
								indiceDataMatrix ++ ; 
								if (indiceDataMatrix == 1 ) 
									DataMatrixLetto1 = Marshal::PtrToStringAnsi((IntPtr) (char *)BarcodeRead); 
								else if (indiceDataMatrix == 2 ) 
									DataMatrixLetto2 = Marshal::PtrToStringAnsi((IntPtr) (char *)BarcodeRead); 
							}
						}while(Reply == LS_OKAY) ;
					}
					else
					ReturnC->FunzioneChiamante = "LSReadBarcodeFromBitmap(READ_2D_BARCODE_QRCODE)";
				}
			}
			else
			{
				ReturnC->FunzioneChiamante = "LSReadImage";
			}
			
			
			LSDisconnect(Connessione,(HWND)hDlg);
		}
		else
		{
			ReturnC->FunzioneChiamante = "LSDocHandle";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}
	// libero le immagini
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	ReturnC->ReturnCode = Reply;

	if (Reply == LS_NO_MORE_DATA )
		Reply = LS_OKAY ; 
	
	return Reply;
}


int Ls150Class::Periferica::CalcolaSalvaDistanze(HWND hDlg, BITMAPINFOHEADER *pImage, short LineToCheck)
{
	BOOL fNeroFound;
	unsigned char *pDati;
	long nColorData;
	long Width, Diff;
	short NrBarreTrovate, OffsetBarra;
	short currPixel, PixelIni;
	short ii;


	ii = 0;
	/*for (ii=0;ii<1024;ii++)
		Distanze[ii]= 0;*/

	// Vado ad inizio dati
	if( pImage->biBitCount <= 8)
		nColorData = 1 << pImage->biBitCount;
	else
		nColorData = 0;
	pDati = (unsigned char *)pImage + sizeof (BITMAPINFOHEADER) + (nColorData * sizeof (RGBQUAD));

	// Mi sposto a meta' documento
	Width = pImage->biWidth;
	if( Diff = Width % 4 )
		Width += (4 - Diff);

	pDati += (Width * (pImage->biHeight / 2));

	// Cerco le barre nere
	NrBarreTrovate = 0;
	OffsetBarra = MM_OFFSET_BORDO;		// Parto da 1 mm dal bordo
	currPixel = OffsetBarra;

	// Se sono arrivato in fondo all'immagine finisco il loop
	while( currPixel < (pImage->biWidth - MM_OFFSET_BORDO) )
	{
		fNeroFound = FALSE;
		currPixel = OffsetBarra;
		while( currPixel < (pImage->biWidth - MM_OFFSET_BORDO) )
		{
			if( (*(pDati + currPixel) < SOGLIA_NERO_BIANCO) && !fNeroFound)
			{
				fNeroFound = TRUE;

				// Salvo il pixel di inizio barra nera
				if( NrBarreTrovate == 0 )
					PixelIni = currPixel;

				NrBarreTrovate ++;
				if( NrBarreTrovate == LineToCheck )
				{
					Distanze[ii] = currPixel - PixelIni;
					ii ++;
					// Reinizzializzo per il prossimo conteggio
					NrBarreTrovate = 0;
					break;
				}
			}
			else
			{
				// Ho ritrovato il bianco
				if( (*(pDati + currPixel) > SOGLIA_NERO_BIANCO) && fNeroFound )
				{
					fNeroFound = FALSE;

					// Salvo l'inizio del prossimo check
					if( NrBarreTrovate == 1 )
					{
						OffsetBarra = currPixel;
					}
				}
			}

			currPixel ++;
		}
	}

	return ii-1;
} // CalcolaSalvaDistanze














int Ls150Class::Periferica::LeggiCodeline(short DocHandling)
{

	char	BufCodeLine[CODE_LINE_LENGTH];
	short	llBufCodeLine = 256;
	short ret;
	short Connessione;

	ReturnC->FunzioneChiamante = "LeggiCodeline";
	
	//-----------Open--------------------------------------------

	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	//Reply = LS150_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( Reply == LS_OKAY )
	{
		//Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_WARNING,50,150,215);
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_WARNING,50,80,225);
		//Reply  = LS150_ConfigDoubleLeafing((HWND)hDlg,DOUBLE_LEAFING_WARNING,DOUBLE_LEAFING_DEFAULT,0);

		/*Reply = LS150_DocHandle((HWND)hDlg,
							SUSPENSIVE_MODE,
							NO_FRONT_STAMP,
							NO_PRINT_VALIDATE,
							READ_CODELINE_MICR,
							SIDE_NONE_IMAGE,
							SCAN_MODE_256GR200,
							AUTOFEED,
							SORTER_BAY1,
							WAIT_YES,
							NO_BEEP,
							0,
							0);*/
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								READ_CODELINE_MICR,
								SIDE_NONE_IMAGE,
								SCAN_MODE_256GR200,
								AUTOFEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);
		if (Reply == LS_OKAY)
		{
			////-----------ReadCodeLine--------------------------------------------
			///*Reply = LS150_ReadCodeline((HWND)hDlg,
			//							'S',
			//							BufCodeLine,
			//							&llBufCodeLine,
			//							NULL,
			//							&ret);*/
			Reply = LSReadCodeline(Connessione, (HWND)hDlg,
										   BufCodeLine,
										   &llBufCodeLine,
										   NULL, 0,
										   NULL, 0);

			
			if (Reply == LS_OKAY)
			{
				CodelineLetta = Marshal::PtrToStringAnsi((IntPtr) (char *)BufCodeLine);
			}
			else
			{
				ReturnC->FunzioneChiamante =  "LSReadCodeline";
			}
		}
		else
		{
			ReturnC->FunzioneChiamante =  "LSDocHandle";

		}			
	}
	else
	{
		ReturnC->FunzioneChiamante =  "LSOpen";
	}
	//ret  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,150,215);
	ret  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);
	//LS150_ConfigDoubleLeafing((HWND)hDlg,DOUBLE_LEAFING_ERROR,DOUBLE_LEAFING_DISABLE,0);
	//-----------Close-------------------------------------------
	LSDisconnect(Connessione,(HWND)hDlg);	
	
	ReturnC->ReturnCode = Reply;
	
	return Reply;
}


int Ls150Class::Periferica::LeggiCodelineImmagini(short DocHandling)
{

	char	BufCodeLine[CODE_LINE_LENGTH];
	short	llBufCodeLine = 256;
	HANDLE HBackGray = NULL ;
	HANDLE HFrontGray = NULL ; 
	short Connessione;

	char *StrNome= NULL ; 
	short ret;

	ReturnC->FunzioneChiamante = "LeggiCodelineImmagini";
	
	//-----------Open--------------------------------------------

	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	//Reply = LS150_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( Reply == LS_OKAY )
	{
		//Reply = LS150_DoubleLeafingSensibility((HWND)hDlg,DOUBLE_LEAFING_DEFAULT );
		//Reply  = LS150_ConfigDoubleLeafing((HWND)hDlg,DOUBLE_LEAFING_WARNING,DOUBLE_LEAFING_DEFAULT,0);
		//Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_WARNING,50,150,215);
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_WARNING,50,80,225);
		// do via documento
		///*Reply = LS150_DocHandle((HWND)hDlg,
		//					SUSPENSIVE_MODE,
		//					NO_FRONT_STAMP,
		//					NO_PRINT_VALIDATE,
		//					READ_CODELINE_MICR,
		//					SIDE_ALL_IMAGE,
		//					SCAN_MODE_256GR200,
		//					AUTOFEED,
		//					SORTER_BAY1,
		//					WAIT_YES,
		//					NO_BEEP,
		//					0,
		//					0);*/
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								READ_CODELINE_MICR,
								SIDE_ALL_IMAGE,
								SCAN_MODE_256GR200,
								AUTOFEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);
		if (Reply == LS_OKAY)
		{
			//-----------ReadCodeLine--------------------------------------------
			///*Reply = LS150_ReadCodeline((HWND)hDlg,
			//							'S',
			//							BufCodeLine,
			//							&llBufCodeLine,
			//							NULL,
			//							&ret);*/
			Reply = LSReadCodeline(Connessione,(HWND)hDlg,
										   BufCodeLine,
										   &llBufCodeLine,
										   NULL, 0,
										   NULL, 0);
			
			if (Reply == LS_OKAY)
			{
				// setto la codeline di ritorno
				CodelineLetta = Marshal::PtrToStringAnsi((IntPtr) (char *)BufCodeLine);
				// Leggo l'immagine filmata
		//		Reply = LS150_ReadImage((HWND)hDlg,
		//							SUSPENSIVE_MODE,
		//							CLEAR_ALL_BLACK,
		////							(char)(TypeLS == TYPE_LS150_5 ? SIDE_FRONT_RED_IMAGE : SIDE_FRONT_IMAGE),
		//							SIDE_ALL_IMAGE,
		//							(LPHANDLE)&HFrontGray,
		//							(LPHANDLE)&HBackGray,
		//							NULL,
		//							NULL);
				Reply = LSReadImage(Connessione, (HWND)hDlg,
										CLEAR_ALL_BLACK,
										SIDE_ALL_IMAGE,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);
				
				if (Reply == LS_OKAY)
				{
					//for (int ii=0;ii<= NomeFileImmagine->Length-1;ii++)
					//{
//						StrNome[ii] = NomeFileImmagine->get_Chars(ii);
					//}
					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();

					

					Reply = LSSaveDIB((HWND)hDlg, HFrontGray,StrNome);
					 // Always free the unmanaged string.

					if (Reply == LS_OKAY)
					{
						Marshal::FreeHGlobal(IntPtr(StrNome));
						StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();


						Reply = LSSaveDIB((HWND)hDlg,HBackGray,StrNome);									
					}	
					else
					{
						ReturnC->FunzioneChiamante =  "LSSaveimageFronte";
					}
				}
				else
				{
					ReturnC->FunzioneChiamante =  "LSReadImage";
				}

			}
			else
			{
				ReturnC->FunzioneChiamante =  "LSReadCodeline";
	//			Reply = LS5xx_Reset((HWND)hDlg, SUSPENSIVE_MODE, RESET_PERIPHERAL_FREE_PATH);
			}
			//BufCodeLine[llBufCodeLine] = '\0';
		}
		else
		{

			ReturnC->FunzioneChiamante =  "LSDocHandle";
	//			Reply = LS5xx_Reset((HWND)hDlg, SUSPENSIVE_MODE, RESET_PERIPHERAL_FREE_PATH);
		}			
	}
	else
	{
		ReturnC->FunzioneChiamante =  "LSOpen";
	}
	// libero le immagini
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	//ret = LS150_DoubleLeafingSensibility((HWND)hDlg,DOUBLE_LEAFING_DISABLE);
	//ret  = LS150_ConfigDoubleLeafing((HWND)hDlg,DOUBLE_LEAFING_ERROR,DOUBLE_LEAFING_DISABLE,0);
	//ret  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,150,215);
	ret  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);
	//-----------Close-------------------------------------------
	LSDisconnect(Connessione,(HWND)hDlg);	
	ReturnC->ReturnCode = Reply;
	
	return Reply;
}


int Ls150Class::Periferica::LeggiCodelineImmaginiAuto(short DocHandling)
{

	char	BufCodeLine[CODE_LINE_LENGTH];
	short	llBufCodeLine = 256;
	HANDLE HBackGray = NULL ;
	HANDLE HFrontGray = NULL ;


	
	char *StrNome ; 
	unsigned long NrDoc;
	int ii=0;



	short ret=0;

	ReturnC->FunzioneChiamante = "LeggiCodelineImmaginiAuto";
	
	//-----------Open--------------------------------------------
//	Reply = LS150_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if ((Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) || (Reply == LS_SORTER1_FULL) || (Reply == LS_SORTER2_FULL) ||(Reply == LS_DOUBLE_LEAFING_WARNING))
	{
		//Reply = LS150_GetDocData((HWND)hDlg,
		//							 NULL,
		//							 NULL,
		//							 NULL,	//BufFrontNettoFile,
		//							 NULL,	//BufBackNettoFile,
		//							(LPLONG*)&HFrontGray,
		//							(LPLONG *)&HBackGray,
		//							 NULL,	//BufFrontNettoImage,
		//							 NULL,	//BufbackNettoImage,
		//							 NULL,
		//							 BufCodeLine,
		//							 NULL,
		//							 &NrDoc,
		//							 NULL,
		//							 NULL);
		Reply  = LSGetDocData(Connessione,
					((HWND)hDlg),
					&NrDoc,
		 			NULL,//FilenameFront,
		 			NULL,//FilenameBack,
		 			NULL,//Reserved1,		// not used must be NULL
		 			NULL,//Reserved2,		// not used must be NULL
		 			(LPHANDLE *)&HFrontGray,
		 			(LPHANDLE *)&HBackGray,
		 			NULL,//*Reserved3,		// not used must be NULL
		 			NULL,//*Reserved4,		// not used must be NULL
		 			NULL,//CodelineSW,
		 			BufCodeLine,
		 			NULL,//BarCode,
		 			NULL,//CodelinesOptical,
		 			NULL,//*DocToRead,
		 			NULL,//*NrPrinted,
		 			NULL,//Reserved5,		// not used must be NULL
			 		NULL);//Reserved6);		// not used must be NULL
	
		if ((Reply == LS_OKAY) || (Reply == LS_SORTER1_FULL) || (Reply == LS_SORTER2_FULL) ||(Reply == LS_DOUBLE_LEAFING_WARNING))
		{
			// setto la codeline di ritorno
			CodelineLetta = Marshal::PtrToStringAnsi((IntPtr) (char *)BufCodeLine);

			if (HFrontGray)
			{
				StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
				
				Reply = LSSaveDIB((HWND)hDlg,HFrontGray,StrNome);

				 // Always free the unmanaged string.
				 Marshal::FreeHGlobal(IntPtr(StrNome));
			}

			if(HBackGray)
			{
				//NomeFileImmagine = "..\\Dati\\BackImage.bmp";	
			//	for (int ii=0;ii<256;ii++)
			//		StrNome[ii] = 0;
				//for (int ii=0;ii<= NomeFileImmagineRetro->Length-1;ii++)
				//{
//					StrNome[ii] = NomeFileImmagineRetro->get_Chars(ii);
				//}
				StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();

				
				 LSSaveDIB((HWND)hDlg,HBackGray,StrNome);

				 	 // Always free the unmanaged string.
					  Marshal::FreeHGlobal(IntPtr(StrNome));
				
			}	
			/*else
			{
				ReturnC->FunzioneChiamante =  "LS150_SaveimageFronte";
			}
			*/
			
			//BufCodeLine[llBufCodeLine] = '\0';
		}
		else
		{
	
			ReturnC->FunzioneChiamante =  "LSGetdocData";
	//			Reply = LS5xx_Reset((HWND)hDlg, SUSPENSIVE_MODE, RESET_PERIPHERAL_FREE_PATH);
		}			
	}
	// libero le immagini
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	//ret = LS150_DoubleLeafingSensibility((HWND)hDlg,DOUBLE_LEAFING_DISABLE);
	//ret  = LS150_ConfigDoubleLeafing((HWND)hDlg,DOUBLE_LEAFING_ERROR,DOUBLE_LEAFING_DISABLE,0);

	ReturnC->ReturnCode = Reply;
	
	return Reply;
}




int Ls150Class::Periferica::Stopaaaaa()
{
	//int ret;

	char	BufCodeLine[CODE_LINE_LENGTH];
	short	llBufCodeLine = 256;
	HANDLE HBackGray = NULL ;
	HANDLE HFrontGray = NULL ;

	ReturnC->FunzioneChiamante = "LSStopAutoDocHandle";

	//char *StrNome ; 
	unsigned long NrDoc;
	int ii=0;
	bool fcicla = true;

	LSStopAutoDocHandle(Connessione,(HWND)hDlg);

	while (fcicla)
	{
		Reply  = LSGetDocData(Connessione,
						((HWND)hDlg),
						&NrDoc,
		 				NULL,//FilenameFront,
		 				NULL,//FilenameBack,
		 				NULL,//Reserved1,		// not used must be NULL
		 				NULL,//Reserved2,		// not used must be NULL
		 				(LPHANDLE *)&HFrontGray,
		 				(LPHANDLE *)&HBackGray,
		 				NULL,//*Reserved3,		// not used must be NULL
		 				NULL,//*Reserved4,		// not used must be NULL
		 				NULL,//CodelineSW,
		 				BufCodeLine,
		 				NULL,//BarCode,
		 				NULL,//CodelinesOptical,
		 				NULL,//*DocToRead,
		 				NULL,//*NrPrinted,
		 				NULL,//Reserved5,		// not used must be NULL
			 			NULL);//Reserved6);		// not used must be NULL


		// libero le immagini
		if (HFrontGray)
			LSFreeImage((HWND)hDlg, &HFrontGray);
		if (HBackGray)
			LSFreeImage((HWND)hDlg, &HBackGray);


		if((Reply  == LS_OKAY) ||(Reply  == LS_DOUBLE_LEAFING_WARNING)||(Reply  == LS_SORTER1_FULL))
			fcicla = true;
		else
			fcicla = false;
	}


    
	
	//ret  = LS150_ConfigDoubleLeafing((HWND)hDlg,DOUBLE_LEAFING_ERROR,DOUBLE_LEAFING_DISABLE,0);
	//Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,150,215);
	Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);
	//LS150_Wait((HWND)hDlg, &ret, NULL);
	

	LSDisconnect(Connessione,(HWND)hDlg);	
	return Reply;
}


int Ls150Class::Periferica::ViaDocumentiAuto( short scanMode)
{

//	char	BufCodeLine[CODE_LINE_LENGTH];
	short	llBufCodeLine = 256;
	//HANDLE HBackGray=0;
	//HANDLE HFrontGray=0;
	short miaconn;
	short nrDoc = 0;

//	char StrNome[256];

	short ret=0;


	
	//-----------Open--------------------------------------------
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&miaconn);

	//Reply = LS150_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);

	if( Reply == LS_OKAY )
	{
		//Reply  = LSConfigDoubleLeafingAndDocLength(miaconn,(HWND)hDlg,DOUBLE_LEAFING_ERROR,50,150,225);
		Reply  = LSConfigDoubleLeafingAndDocLength(miaconn,(HWND)hDlg,DOUBLE_LEAFING_ERROR,50,80,225);

		


		Reply = LSAutoDocHandle(	miaconn,
						((HWND)(hDlg)),
							NO_STAMP,
							NO_PRINT_VALIDATE,
							READ_CODELINE_MICR,
							scanMode,
							AUTOFEED,
							SORTER_BAY1,
							nrDoc,
							CLEAR_ALL_BLACK,
							SIDE_ALL_IMAGE,
							READMODE_BRUTTO,
							IMAGE_SAVE_HANDLE,
							NULL,
							NULL,
							0,
							0,
							0,
							0,
							BOTTOM_RIGHT_MM,
							0,
							SAVE_JPEG,
								100,
								SAVE_OVERWRITE,
								1,
							WAIT_YES,
							BEEP,
							NULL,
							NULL,
							NULL);

		if (Reply == LS_OKAY)
		{
			
			
			
		}
		else
		{

			ReturnC->FunzioneChiamante =  "LSautodocHandle";
	//			Reply = LS5xx_Reset((HWND)hDlg, SUSPENSIVE_MODE, RESET_PERIPHERAL_FREE_PATH);
		}			
	}
	else
	{
		ReturnC->FunzioneChiamante =  "LSOpen";
	}
	// libero le immagini
	
	//ret = LS150_DoubleLeafingSensibility((HWND)hDlg,DOUBLE_LEAFING_DISABLE);
	//ret  = LS150_ConfigDoubleLeafing((HWND)hDlg,DOUBLE_LEAFING_ERROR,DOUBLE_LEAFING_DISABLE,0);
	//ret  = LSConfigDoubleLeafingAndDocLength(miaconn,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,150,215);
	ret  = LSConfigDoubleLeafingAndDocLength(miaconn,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);
	//-----------Close-------------------------------------------
//	LSDisconnect(Connessione,(HWND)hDlg);
	
	//// libero le immagini
	//if (HFrontGray)
	//	LSFreeImage((HWND)hDlg, &HFrontGray);
	//if (HBackGray)
	//	LSFreeImage((HWND)hDlg, &HBackGray);

	ReturnC->ReturnCode = Reply;
	Connessione = miaconn ;
	return Reply;
}






int Ls150Class::Periferica::Timbra(short DocHandling)
{

	DateTime iniTime,currTime;
	TimeSpan t;
	HANDLE HBackGray = NULL ;
	HANDLE HFrontGray = NULL ;
	char *StrNome = NULL; 
	short Connessione;

	ReturnC->FunzioneChiamante = "Timbra";
	//-----------Open--------------------------------------------Diagnostica
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	//Reply = LS150_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( Reply == LS_OKAY )
	{
		// Do il via al documento
		///*Reply = LS150_DocHandle((HWND)hDlg,
		//					SUSPENSIVE_MODE,
		//					FRONT_STAMP,
		//					NO_PRINT_VALIDATE,
		//					NO_READ_CODELINE,
		//					SIDE_ALL_IMAGE,
		//					SCAN_MODE_256GR200,
		//					AUTOFEED,
		//					SORTER_BAY1,
		//					WAIT_YES,
		//					BEEP,
		//					0,
		//					0);*/
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
								FRONT_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								SIDE_ALL_IMAGE,
								SCAN_MODE_256GR200,
								AUTOFEED,
								SORTER_BAY1,
								WAIT_YES,
								BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);
			
		if (Reply == LS_OKAY)
		{
	//		Reply = LS150_ReadImage((HWND)hDlg,
	//							SUSPENSIVE_MODE,
	//							CLEAR_ALL_BLACK,
	////							(char)(TypeLS == TYPE_LS150_5 ? SIDE_FRONT_RED_IMAGE : SIDE_FRONT_IMAGE),
	//							SIDE_ALL_IMAGE,
	//							(LPHANDLE)&HFrontGray,
	//							(LPHANDLE)&HBackGray,
	//							NULL,
	//							NULL);
			Reply = LSReadImage(Connessione, (HWND)hDlg,
										CLEAR_ALL_BLACK,
										SIDE_ALL_IMAGE,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);
				
			if (Reply == LS_OKAY)
			{
				
				//Marshal::FreeHGlobal(IntPtr(StrNome));
				StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();				
				
				Reply = LSSaveDIB((HWND)hDlg,HFrontGray,StrNome);
				if (Reply == LS_OKAY)
				{
					Marshal::FreeHGlobal(IntPtr(StrNome));
					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();
				
					Reply = LSSaveDIB((HWND)hDlg,HBackGray,StrNome);
					
				}	
				else
				{
					ReturnC->FunzioneChiamante =  "LSSaveimageFronte";
				}
			}
			else
			{
				ReturnC->FunzioneChiamante = "LSReadImage";
			}
		
		}
		else
		{
			ReturnC->FunzioneChiamante = "LSDocHandle";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}

	ReturnC->ReturnCode = Reply;

	// libero le immagini
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	
	LSDisconnect(Connessione,(HWND)hDlg);
	
	return Reply;
}


int Ls150Class::Periferica::TestStampa(int StampaStd)
{

	String ^strstp;
	char *str = NULL; 
	DateTime iniTime,currTime;
	TimeSpan t;
	HANDLE HBackGray = NULL ;
	HANDLE HFrontGray = NULL ;
	char *StrNome = NULL; 
	short Connessione;

	ReturnC->FunzioneChiamante =  "TestStampa";

	strstp = "TEST INKJET 888888888888 HHHHHHHHHHHHHHHH 0000000000000000";
	
	if (StampaStd)
	{
		strstp = Stampafinale;
	}
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	//Reply = LS150_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( Reply == LS_OKAY )
	{
		Marshal::FreeHGlobal(IntPtr(str));
		str = (char*) Marshal::StringToHGlobalAnsi(strstp ).ToPointer();		
				
		Reply = LSLoadString(Connessione,(HWND)hDlg,
								 FORMAT_BOLD ,//FORMAT_NORMAL,//: ) FORMAT_BOLD 
								 strstp->Length,
								 str);	
		if (Reply == LS_OKAY)
		{
			// via doc
			///*Reply = LS150_DocHandle((HWND)hDlg,
			//			SUSPENSIVE_MODE,
			//			NO_FRONT_STAMP,
			//			PRINT_VALIDATE,
			//			NO_READ_CODELINE,
			//			SIDE_ALL_IMAGE,
			//			SCAN_MODE_256GR200,
			//			AUTOFEED,
			//			SORTER_BAY1,
			//			WAIT_YES,
			//			NO_BEEP,
			//			0,
			//			0);*/
			Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								PRINT_VALIDATE,
								NO_READ_CODELINE,
								SIDE_ALL_IMAGE,
								SCAN_MODE_256GR200,
								AUTOFEED,
								SORTER_BAY1,
								WAIT_YES,
								BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);
			if (Reply == LS_OKAY)
			{
			//	Reply = LS150_ReadImage((HWND)hDlg,
			//							SUSPENSIVE_MODE,
			//							CLEAR_ALL_BLACK,
			////							(char)(TypeLS == TYPE_LS150_5 ? SIDE_FRONT_RED_IMAGE : SIDE_FRONT_IMAGE),
			//							SIDE_ALL_IMAGE,
			//							(LPHANDLE)&HFrontGray,
			//							(LPHANDLE)&HBackGray,
			//							NULL,
			//							NULL);
				Reply = LSReadImage(Connessione, (HWND)hDlg,
										CLEAR_ALL_BLACK,
										SIDE_ALL_IMAGE,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);
				
				if (Reply == LS_OKAY)
				{
					Marshal::FreeHGlobal(IntPtr(StrNome));
					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();			
					
					Reply = LSSaveDIB((HWND)hDlg,HFrontGray,StrNome);
					if (Reply == LS_OKAY)
					{
						Marshal::FreeHGlobal(IntPtr(StrNome));
						StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();			
						
						Reply = LSSaveDIB((HWND)hDlg,HBackGray,StrNome);
						
					}	
					else
					{
						ReturnC->FunzioneChiamante =  "LSSaveimageFronte";
					}
				}
				else
				{
					ReturnC->FunzioneChiamante = "LSReadImage";
				}

			}
			else
			{
				ReturnC->FunzioneChiamante =  "LSDocHandle";
			}
		}
		else
		{
			ReturnC->FunzioneChiamante =  "LSLoadString";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante =  "LSOpen";
	}

	ReturnC->ReturnCode = Reply;

	// libero le immagini
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);


	LSDisconnect(Connessione,(HWND)hDlg);

	return Reply;
}


int Ls150Class::Periferica::TestStampaAlto(int StampaStd)
{

	String  ^strstp;
	char *str = NULL; 
	DateTime iniTime,currTime;
	TimeSpan t;
	HANDLE HBackGray = NULL ;
	HANDLE HFrontGray = NULL ;
	char *StrNome = NULL; 
	short Connessione;

	ReturnC->FunzioneChiamante = "TestStampaAlto";
	

	strstp = "TEST INKJET 888888888888 HHHHHHHHHHHHHHHH 0000000000000000";
	
	if (StampaStd)
	{
		strstp = Stampafinale;
	}
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	//Reply = LS150_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( Reply == LS_OKAY )
	{
			Marshal::FreeHGlobal(IntPtr(str));
			str = (char*) Marshal::StringToHGlobalAnsi(strstp ).ToPointer();			
		
			Reply = LSLoadString(Connessione,(HWND)hDlg,
									 PRINT_UP_FORMAT_BOLD,//PRINT_UP_FORMAT_NORMAL,//: ) FORMAT_BOLD 
									 strstp->Length,
									 str);
			
			if (Reply == LS_OKAY)
			{
				// via doc
				///*Reply = LS150_DocHandle((HWND)hDlg,
				//			SUSPENSIVE_MODE,
				//			NO_FRONT_STAMP,
				//			PRINT_VALIDATE,
				//			NO_READ_CODELINE,
				//			SIDE_ALL_IMAGE,
				//			SCAN_MODE_256GR200,
				//			AUTOFEED,
				//			SORTER_BAY1,
				//			WAIT_YES,
				//			NO_BEEP,
				//			0,
				//			0);*/
				Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								PRINT_VALIDATE,
								NO_READ_CODELINE,
								SIDE_ALL_IMAGE,
								SCAN_MODE_256GR200,
								AUTOFEED,
								SORTER_BAY1,
								WAIT_YES,
								BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);
				if (Reply == LS_OKAY)
				{
			//			Reply = LS150_ReadImage((HWND)hDlg,
			//							SUSPENSIVE_MODE,
			//							CLEAR_ALL_BLACK,
			////							(char)(TypeLS == TYPE_LS150_5 ? SIDE_FRONT_RED_IMAGE : SIDE_FRONT_IMAGE),
			//							SIDE_ALL_IMAGE,
			//							(LPHANDLE)&HFrontGray,
			//							(LPHANDLE)&HBackGray,
			//							NULL,
			//							NULL);
						Reply = LSReadImage(Connessione, (HWND)hDlg,
										CLEAR_ALL_BLACK,
										SIDE_ALL_IMAGE,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);
				
				if (Reply == LS_OKAY)
				{
					Marshal::FreeHGlobal(IntPtr(StrNome));
					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();			
					
					Reply = LSSaveDIB((HWND)hDlg,HFrontGray,StrNome);
					if (Reply == LS_OKAY)
					{
						Marshal::FreeHGlobal(IntPtr(StrNome));
						StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();			

						Reply = LSSaveDIB((HWND)hDlg,HBackGray,StrNome);
						
					}	
					else
					{
						ReturnC->FunzioneChiamante =  "LSSaveimageFronte";
					}
				}
				else
				{
					ReturnC->FunzioneChiamante = "LSReadImage";
				}
			}
			else
			{
				ReturnC->FunzioneChiamante = "LSDocHandle";
			}
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}

	ReturnC->ReturnCode = Reply;

	// libero le immagini
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	
	LSDisconnect(Connessione,(HWND)hDlg);
	return Reply;

}


int Ls150Class::Periferica::LeggiBadge(short traccia)
{

	short Length = 256;
	char strBadge[256];
	short Connessione;

	ReturnC->FunzioneChiamante = "LeggiBadge";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	//Reply = LS150_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( Reply == LS_OKAY )
	{
		if (traccia == 1)
			Reply = LSReadBadgeWithTimeout(Connessione,(HWND)hDlg,FORMAT_IATA_ABA ,256,
				strBadge,&Length,15000);
		if (traccia == 2)
			Reply = LSReadBadgeWithTimeout(Connessione,(HWND)hDlg,FORMAT_ABA_MINTS ,256,
				strBadge,&Length,15000);
		if ( traccia == 3)
			Reply = LSReadBadgeWithTimeout(Connessione,(HWND)hDlg,FORMAT_IATA_ABA_MINTS ,256,
				strBadge,&Length,4000);

		
		BadgeLetto = Marshal::PtrToStringAnsi((IntPtr) (char *)strBadge);

	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}

	ReturnC->ReturnCode = Reply;
	
	LSDisconnect(Connessione,(HWND)hDlg);
	return Reply;
}



int Ls150Class::Periferica::SerialNumber()
{

	short Length = 256;
	char *str;
	short ii=0;
	short Connessione;	

	ReturnC->FunzioneChiamante = "SerialNumber";

	str = (char*) Marshal::StringToHGlobalAnsi(SSerialNumber ).ToPointer();
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	//Reply = LS150_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( Reply == LS_OKAY )
	{
		Reply = LSSetSerialNumber(Connessione,(HWND)hDlg, (unsigned char *)str,1962);
	}
	ReturnC->ReturnCode = Reply;
	LSDisconnect(Connessione,(HWND)hDlg);

	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Serial Number";

	return Reply;
}

//int Ls150Class::Periferica::TestPalettaMotor()
//{
//
//	short Length = 256;
//	char *str;
//	short ii=0;
//	short Connessione;	
//
//	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
//
//	//Reply = LS150_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
//	if( Reply == LS_OKAY )
//	{
//		Reply = LSSetSerialNumber(Connessione,(HWND)hDlg, (unsigned char *)str,1962);
//	}
//	ReturnC->ReturnCode = Reply;
//	LSDisconnect(Connessione,(HWND)hDlg);
//
//	ReturnC->ReturnCode = Reply;
//	ReturnC->FunzioneChiamante = "Serial Number";
//
//	return Reply;
//}
//
int Ls150Class::Periferica::Visual(char * str)
{
	//Ls150Class::FDownload *f;
	
	//f = new Ls150Class::FDownload;
	//f = 
	//fShow();
	//f->PDwl->Value ++;
	return 0;
}

int Ls150Class::Periferica::SetConfigurazione(int E13B,int CMC7,int timbro,int badge12,int badge23,int badge123,int scannerfronte, int scannerretro,int inkjet,int Velocita,int Sflogliatore50,int SwAbilitato,int fCardCol)
{

	char BytesCfg[4];
	char IdentStr[64],Version[64],Date_Fw[64],LsName[64],SerialNumber[64],FeederVersion[64];
	char BoardNr[8];
	char CpldNr[8];
	short Connessione;

	CpldNr[1]= 0;
	CpldNr[2]= 0;
	//HWND 
	HINSTANCE hInst = 0;

	ReturnC->FunzioneChiamante = "SetConfigurazione";

	// setto configurazione periferica da file
	// Azzero la variabile
	//memset(BytesCfg, 0x40, sizeof(BytesCfg));
	BytesCfg[0] = 0x40;
	BytesCfg[1] = 0x40;
	BytesCfg[2] = 0x40;
	BytesCfg[3] = 0x40;

	if( E13B  || CMC7  )
	{
		BytesCfg[0] |= 0x01;
	}
	if( timbro)
	{
		BytesCfg[0] |= 0x20;
	}
	
	
	if( badge12)
	{
		BytesCfg[1] |= 0x10;
	}
	if( badge23)
	{
		BytesCfg[1] |= 0x08;
	}
	if( badge123)
	{
		BytesCfg[1] |= 0x18;
	}

	
	if( scannerfronte && scannerretro )
	{
		BytesCfg[1] |= 0x03;		
	}
	else if( scannerretro )
	{
		BytesCfg[1] |= 0x02;
	}
	else if(scannerfronte )
	{
		BytesCfg[1] |= 0x01;	
	}
	if( inkjet)
	{
		BytesCfg[0] |= 0x08;
	}

	if (Velocita)
	{
		BytesCfg[0] |= 0x02;
	}
	if (Sflogliatore50)
	{
		BytesCfg[0] |= 0x10;
	}

 	if(SwAbilitato >0)
	{
		BytesCfg[3] |= SwAbilitato;
	}

	//11/09/2017 Abilito Sempre Top Image /ClearPix
	BytesCfg[3] |= 0x01;

	//06/09/2018 Abilito Sempre SCAN ID CARD TO COLOR NFR 580
	if (fCardCol)
	{
		BytesCfg[2] |= 0x02;
	}

	if (ConfScannerGrigio==1)
		BytesCfg[2] |= 0x02;

	// open
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	//Reply = LS150_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( Reply == LS_OKAY )
	{
		
		Reply = LS150_WriteConfiguration((HWND)hDlg,BytesCfg );
		if (Reply == LS_OKAY )
		{
			// Setto la configurazione corrente
			Sleep(500);
			//Reply = LSSetConfiguration(Connessione,(HWND)hDlg,BytesCfg );   //NON SCRIVE LA CONFIGURAZIONE....
		}

		if (Reply == LS_OKAY)
		{//
			//Reply =	LS150_Identify((HWND)0, SUSPENSIVE_MODE,IdentStr,LsName,Version,Date_Fw,BoardNr,FeederVersion,SerialNumber,NULL);
			Reply =	LSUnitIdentify(Connessione,(HWND)0, IdentStr,LsName,Version,Date_Fw,SerialNumber,BoardNr, NULL, NULL, FeederVersion, NULL, NULL, NULL, NULL);
			
			if (Reply == LS_OKAY)
			{
					SIdentStr = Marshal::PtrToStringAnsi((IntPtr) (char *) IdentStr);
						
					SVersion = Marshal::PtrToStringAnsi((IntPtr) (char *)Version);
					SDateFW = Marshal::PtrToStringAnsi((IntPtr) (char *)Date_Fw);
					SLsName = Marshal::PtrToStringAnsi((IntPtr) (char *)LsName);
					SSerialNumber = Marshal::PtrToStringAnsi((IntPtr) (char *)SerialNumber);
					SFeederVersion = Marshal::PtrToStringAnsi((IntPtr) (char *)FeederVersion);
					SBoardNr = BoardNr[0].ToString();
					CpldNr[0] =  BoardNr[1];
					SCpldNr = Marshal::PtrToStringAnsi(static_cast<IntPtr>(CpldNr));
				
					TypeLS = GetTypeLS(LsName, Version, IdentStr);
					//ConfMicr = true;
					
					if((( IdentStr[0] & 0x02 ) == 0x02) == (Velocita))						// 00000010
					{								
						//if((( IdentStr[0] & 0x04 )== 0x04						// 00000100
						//	ConfFeederMotorized = true;

						if((( IdentStr[0] & 0x08 )== 0x08) == (inkjet))
						{							// 00001000
							if((( IdentStr[0] & 0x20 )== 0x20) == (timbro))
							{
								if((( IdentStr[1] & 0x01 )== 0x01) == (scannerfronte))
								{
									if((( IdentStr[1] & 0x02 )== 0x02) == (scannerretro))
									{
										if((( IdentStr[1] & 0x08 )== 0x08) )
										{
											if((( IdentStr[1] & 0x10 )== 0x10) == (badge123))
											{
												if((( IdentStr[0] & 0x10 )== 0x10) == (Sflogliatore50))
												{
													
												}
												else
												{
													// Sfogliatore 50 doc
													Reply = -7006;
												}
											}
											else
											{
												// bdg123
												Reply = -7000;
											}
										}
										
										
									}
									else
									{
										// scanner retro
										Reply = -7001;
									}
								}
								else
								{
									// scanner fronte
									Reply = -7002;
								}
							}
							else
							{
								// timbro
								Reply = -7003;
							}
						}
						else
						{
							// ink
							Reply = -7004;
						}
					}
					else
					{
						// velocita
						Reply = -7005;
					}
				}
				else
				{
					ReturnC->FunzioneChiamante = "LSIdentify";
				}
			}
			else
			{
				ReturnC->FunzioneChiamante = "LSSet_cfg";
			}

	}	
	// close
	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Set Configurazione";

	LSDisconnect(Connessione,(HWND)hDlg);

	return Reply;
}


int Ls150Class::Periferica::SetValoreDefImmagine()
{
	short NewValue;
	short Connessione;
	
	NewValue = 1427;

	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	//Reply = LS150_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( Reply == LS_OKAY )
	{
		Reply = LSImageCalibration(Connessione,(HWND)hDlg, TRUE, &NewValue);
	}
	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Calibra immagine";

	LSDisconnect(Connessione,(HWND)hDlg);

	return Reply;
}



int Ls150Class::Periferica::CalibraImmagine()
{

	int ii=0;
	short ret=0;
	short Connessione;

		// open
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	//Reply = LS150_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( Reply == LS_OKAY )
	{
		Reply = DoImageCalibration(Connessione,(HWND)hDlg, 1654);
	}	
	// close

	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Calibra immagine";

	
	LSDisconnect(Connessione,(HWND)hDlg);

	return Reply;

}

int Ls150Class::Periferica::CalibraImmagineNew()
{

	int ii=0;
	short ret=0;
	short Connessione;

		// open
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	
	if( Reply == LS_OKAY )
	{
		//161*25.4 * 200 = 1268 pixel , teorical value
		//300 dpi 161 mm = 1902 mm
		Reply =  DoImageCalibrationNew(Connessione,(HWND)hDlg, 1902);
	}	


	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "CalibraImmagineNew";

	
	LSDisconnect(Connessione,(HWND)hDlg);

	return Reply;

}









int Ls150Class::Periferica::StatoPeriferica()
{
	unsigned char Sense;
	unsigned char Status[16];
	short Connessione;

	ReturnC->FunzioneChiamante = "Stato Periferica";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	//Reply = LS150_Open((HWND)hDlg,(HANDLE)hInst,SUSPENSIVE_MODE);
	if( Reply == LS_OKAY )
	{
		Reply = LSPeripheralStatus(Connessione,(HWND)hDlg, &Sense, Status);
		
		StatusByte[0] = Status[0];
		StatusByte[1] = Status[1];
		StatusByte[2] = Status[2];
		StatusByte[3] = Status[3];
		StatusByte[4] = Status[4];
		StatusByte[5] = Status[5];
		StatusByte[6] = Status[6];
		StatusByte[7] = Status[7];
		StatusByte[8] = Status[8];
		StatusByte[9] = Status[9];
		StatusByte[10] = Status[10];
		StatusByte[11] = Status[11];
		StatusByte[12] = Status[12];
		StatusByte[13] = Status[13];
		StatusByte[14] = Status[14];
		StatusByte[15] = Status[15];
		LSDisconnect(Connessione,(HWND)hDlg);
	}
	ReturnC->ReturnCode = Reply;

	return Reply;
}


int Ls150Class::Periferica::DoImageCalibration(short Connessione,HWND hDlg, float TeoricValue)
{
	long	*BufFrontImage;
	int	nRet;
	int fretry;
	float	RealValue, DiffPercentuale;
	short	OldValue, NewValue;
	

	nRet = TRUE;
	fretry = FALSE;
	
	
	
	{
		//Reply = LSUnitIdentify(hLS, hDlg, IdentStr, LsName, Version, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

		//----------- Set Block Document if double leafing -----------
		//Reply = LS150_DoubleLeafingSensibility(hDlg, DOUBLE_LEAFING_DISABLE);
		//Reply  = LS150_ConfigDoubleLeafing((HWND)hDlg,DOUBLE_LEAFING_ERROR,DOUBLE_LEAFING_DISABLE,0);
		//Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,150,215);
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);
		//----------- Set attesa introduzione documento -----------
		Reply = LSDisableWaitDocument(Connessione,hDlg, FALSE);


	// Chiamo la funzione passando l'ultima configurazione settata
		///*Reply = LS150_DocHandle(hDlg,
		//						SUSPENSIVE_MODE,
		//						NO_FRONT_STAMP,
		//						NO_PRINT_VALIDATE,
		//						NO_READ_CODELINE,
		//						SIDE_FRONT_IMAGE,
		//						SCAN_MODE_256GR200,
		//						AUTOFEED,
		//						SORTER_BAY1,
		//						WAIT_YES,
		//						NO_BEEP,
		//						0,
		//						0);*/
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
									NO_STAMP,
									NO_PRINT_VALIDATE,
									NO_READ_CODELINE,
									SIDE_FRONT_IMAGE,
									SCAN_MODE_256GR200,
									AUTOFEED,
									SORTER_BAY1,
									WAIT_YES,
									NO_BEEP,
									NULL,
									SCAN_PAPER_DOCUMENT,
									0);

		if( Reply != LS_OKAY )
		{
			fretry = TRUE;
			return Reply;	
		}
		else
		{
		//-----------ReadImage-------------------------------------
		///*	Reply = LS150_ReadImage(hDlg,
		//							SUSPENSIVE_MODE,
		//							CLEAR_ALL_BLACK,
		//							SIDE_FRONT_IMAGE,
		//							(LPHANDLE)&BufFrontImage,
		//							NULL,
		//							NULL,
		//							NULL);*/
			Reply = LSReadImage(Connessione, (HWND)hDlg,
											CLEAR_ALL_BLACK,
											SIDE_FRONT_IMAGE,
											0, 0,
											(LPHANDLE)&BufFrontImage,
											NULL,
											NULL,
											NULL);
					

			if (Reply != LS_OKAY)
			{
				fretry = TRUE;
				return Reply;

			}

			// Tolgo il nero davanti e dietro e NON sopra per avere 864 righe
			ClearFrontAndRearBlack( (BITMAPINFOHEADER *)BufFrontImage );

			
			// Leggo lunghezza image
			RealValue = (float)((BITMAPINFOHEADER *)BufFrontImage)->biWidth;

			// Leggo il valore settato nella periferica
			Reply = LSImageCalibration(Connessione,hDlg, FALSE, &OldValue);
			if( Reply != LS_OKAY )
			{
				CheckReply(hDlg, Reply, "LSImageCalibration");
				fretry = TRUE;
				return Reply;
			}


			// Controllo lunghezza image
			DiffPercentuale = TeoricValue * (float)0.004;		// 0.4%

			if(	RealValue < (TeoricValue - DiffPercentuale) )
			{
				// Calcolo il valore in percentuale da sommare a quello presente nella perifeica
				DiffPercentuale = TeoricValue - RealValue;
				DiffPercentuale = DiffPercentuale * (float)100 / TeoricValue;

				NewValue = (unsigned short)((float)OldValue * DiffPercentuale) / 100;
				NewValue = OldValue - NewValue;

				

				nRet = FALSE;	// Devo riprovare la calibrazione
				fretry = TRUE;
				Reply = LS_RETRY;
				
			}


			if( RealValue > (TeoricValue + DiffPercentuale) )
			{
				// Calcolo il valore in percentuale da sommare a quello presente nella perifeica
				DiffPercentuale = RealValue - TeoricValue;
				DiffPercentuale = DiffPercentuale * (float)100 / TeoricValue;

				NewValue = (unsigned short)((float)OldValue * DiffPercentuale) / 100;
				NewValue += OldValue;

			

				nRet = FALSE;	// Devo riprovare la calibrazione
				fretry = TRUE;
				Reply = LS_RETRY;
			}


			// Se nRet = FALSE ho variato i valori, perci� vado a scrivere quelli nuovi
			if( fretry == TRUE)
			{
				if( TypeLS == TYPE_LS150_VE )
				{
					// Controllo che non sia inferiore al 10% del default
					if( NewValue < (SCANNER_TIME_DEFAULT_VE * 0.90) )
						NewValue = (short)(SCANNER_TIME_DEFAULT_VE * 0.90);		// forzo il default ?

					// Controllo che non sia superiore al 10% del default
					if( NewValue > (SCANNER_TIME_DEFAULT_VE * 1.10) )
						NewValue = (short)(SCANNER_TIME_DEFAULT_VE * 1.10);		// forzo il default ?
				}
				else
				{
					// Controllo che non sia inferiore al 1% del default
					if( NewValue < (SCANNER_TIME_DEFAULT * 0.99) )
						NewValue = SCANNER_TIME_DEFAULT;	// forzo il default

					// Controllo che non sia superiore al 3% del default
					if( NewValue > (SCANNER_TIME_DEFAULT * 1.03) )
						NewValue = SCANNER_TIME_DEFAULT;		// forzo il default
				}

				Reply = LSImageCalibration(Connessione,hDlg, TRUE, &NewValue);
				if( Reply != LS_OKAY )
				{
					CheckReply(hDlg, Reply, "LSImageCalibration");
					nRet = TRUE;
					fretry = TRUE;
					return Reply;
					
				}
				else
				{
					Reply = LS_RETRY;
				}
			}

			// Libero le aree bitmap ritornate
			if( BufFrontImage )
				GlobalFree( BufFrontImage );
		}
	}

	return Reply;
} // DoImageCalibration




void Ls150Class::Periferica::ClearFrontAndRearBlack(BITMAPINFOHEADER *Bitmap)
{
	unsigned long ii, jj;
	long	CondDib;
	short	ValoriOk, NrColonneNere;
	long	NrByte;
	BOOL	ContinueClear;
	BOOL	HalfByteHigh;
	unsigned char *pbm, *pbmt, *pbmtm;
	unsigned char HighValue;
	long	RealWidth, DiffWidth;
	long	nr_col_bmp, nr_row_bmp;


	//Test se � una bitmap a 16 o 256 livelli di grigio
	if( Bitmap->biBitCount == 4 )
	{
		// Bitmap a 16 grigi

//	SalvaDIBBitmap(Bitmap, "\\Progetti\\bmp\\front2.bmp");

		// Inizio dati bitmap
		pbm = (unsigned char *)Bitmap + sizeof(BITMAPINFOHEADER) + (16 * sizeof(RGBQUAD));
		NrByte = Bitmap->biWidth / 2;
		if(CondDib = (Bitmap->biWidth % 8) )
			nr_col_bmp = (Bitmap->biWidth + 8 - CondDib) / 2;
		else
			nr_col_bmp = NrByte;
		nr_row_bmp = Bitmap->biHeight;
		pbmt = pbm;
		ContinueClear = TRUE;
		HalfByteHigh = TRUE;
		ValoriOk = 0;
		NrColonneNere = 0;

		// Tolgo le colonne nere davanti
		do
		{
			for( ii = 0; ii < (unsigned long)nr_row_bmp; ii ++, pbmt += nr_col_bmp)
			{
				// Prendo il valore di grigio parte alta se colonna pari, altrimenti parte bassa
				if( HalfByteHigh )
					HighValue = *pbmt >> 4;
				else
					HighValue = *pbmt & 0x0f;

				if( HighValue > LIMITE_16_GRIGI )
					ValoriOk ++;
			}

			//Incremento il counter nr. colonne nere
			if( ValoriOk <= NR_VALORI_OK )
			{
				NrColonneNere ++;
				ValoriOk = 0;
			}
			else
				ContinueClear = FALSE;

			// Reinizializzo il pointer ad inizio bitmap
			pbmt = pbm + (NrColonneNere / 2);

			HalfByteHigh = !HalfByteHigh;

		} while( ContinueClear && (NrColonneNere < (nr_col_bmp * 2)) );


		//Controllo se shiftare la bitmap
		if( NrColonneNere && (NrColonneNere < (nr_col_bmp * 2)) )
		{
			// Inizializzo i pointer
			pbmt = pbmtm = pbm;
			NrByte = nr_col_bmp;

			// Calcolo il NUOVO numero di colonne e lo salvo
			nr_col_bmp *= 2;
			nr_col_bmp -= NrColonneNere;
			Bitmap->biWidth = nr_col_bmp;

			if( CondDib = (nr_col_bmp % 8) )
				nr_col_bmp = (nr_col_bmp + 8 - CondDib);

			// Trasformo in nr. di byte
			NrColonneNere /= 2;

			// Sposto la bitmap
			for( ii = 0; ii < (unsigned long)Bitmap->biHeight; ii ++)
			{
				pbmt += NrColonneNere;
				for( jj = NrColonneNere; jj < (unsigned long)NrByte; jj ++)
					*(pbmtm++) = *(pbmt++);

				// Aggiungo il nero
				for( jj = 0; jj < (unsigned long)((nr_col_bmp - Bitmap->biWidth) / 2); jj ++)
					*(pbmtm++) = 0x00;	//NERO;
			}

			// Aggiorno il nr. di bytes della bitmap
			Bitmap->biSizeImage = Bitmap->biHeight * nr_col_bmp / 2;
		}

//	SalvaDIBBitmap(Bitmap, "\\Progetti\\bmp\\front3.bmp");


		// Inizio dati bitmap
		pbm = (unsigned char *)Bitmap + sizeof(BITMAPINFOHEADER) + (16 * sizeof(RGBQUAD));
		NrByte = Bitmap->biWidth / 2;
		if(CondDib = (Bitmap->biWidth % 8) )
			nr_col_bmp = (Bitmap->biWidth + 8 - CondDib) / 2;
		else
			nr_col_bmp = NrByte;
		nr_row_bmp = Bitmap->biHeight;
		pbmt = pbm + (nr_col_bmp - 1);
		ContinueClear = TRUE;
		HalfByteHigh = FALSE;
		ValoriOk = 0;
		NrColonneNere = 0;

		// Tolgo le colonne nere dietro
		do
		{
			for( ii = 0; ii < (unsigned long)nr_row_bmp; ii ++, pbmt += nr_col_bmp)
			{
				// Prendo il valore di grigio parte alta se colonna pari, altrimenti parte bassa
				if( HalfByteHigh )
					HighValue = *pbmt >> 4;
				else
					HighValue = *pbmt & 0x0f;

				if( HighValue > LIMITE_16_GRIGI )
					ValoriOk ++;
			}

			//Incremento il counter nr. colonne nere
			if( ValoriOk <= NR_VALORI_OK )
			{
				NrColonneNere ++;
				ValoriOk = 0;
			}
			else
				ContinueClear = FALSE;

			// Reinizializzo il pointer ad inizio bitmap
			pbmt = pbm + (nr_col_bmp - 1) - (NrColonneNere / 2);

			HalfByteHigh = !HalfByteHigh;

		} while( ContinueClear && (NrColonneNere < (nr_col_bmp * 2)) );

		if( NrColonneNere && (NrColonneNere < (nr_col_bmp * 2)) )
		{
			DiffWidth = Bitmap->biWidth;

			// Arrotondo il numero di colonne nere ad un nr. pari
			if( NrColonneNere % 2 )
				NrColonneNere --;

			// Aggiorno l'header della bitmap
			Bitmap->biWidth -= NrColonneNere;

			RealWidth = Bitmap->biWidth;

			// Calcolo la larghezza reale della bitmap
			if( CondDib = DiffWidth % 8 )
				DiffWidth += (8 - CondDib);

			if( CondDib = RealWidth % 8 )
				RealWidth += (8 - CondDib);

			DiffWidth -= RealWidth;

			Bitmap->biSizeImage = (Bitmap->biHeight * RealWidth) / 2;

			// Arrotondo al byte
			RealWidth /= 2;
			DiffWidth /= 2;

			//Controllo se shiftare la bitmap
			if( NrColonneNere )
			{
				pbm += RealWidth;
				pbmt = pbm;

				// Sposto la bitmap
				for( ii = 1; ii < (unsigned long)Bitmap->biHeight; ii ++)
				{
					pbmt += DiffWidth;
					for( jj = 0; jj < (unsigned long)RealWidth; jj ++)
						*(pbm++) = *(pbmt++);
				}
			}
		}

//	SalvaDIBBitmap(Bitmap, "\\Progetti\\bmp\\front_16.bmp");

	}
	else
	{
		// Bitmap a 256 grigi

//	SalvaDIBBitmap(Bitmap, "\\Progetti\\bmp\\front2.bmp");

		// Inizio dati bitmap
		pbm = (unsigned char *)Bitmap + sizeof(BITMAPINFOHEADER) + (256 * sizeof(RGBQUAD));
		if(CondDib = (Bitmap->biWidth % 4) )
			nr_col_bmp = (Bitmap->biWidth + 4 - CondDib);
		else
			nr_col_bmp = Bitmap->biWidth;
		nr_row_bmp = Bitmap->biHeight;
		ContinueClear = TRUE;
		ValoriOk = 0;
		NrColonneNere = 0;

		// Tolgo le colonne nere davanti
		pbmt = pbm;
		do
		{
			for( ii = 0; ii < (unsigned long)nr_row_bmp; ii ++, pbmt += nr_col_bmp)
				if( *pbmt > LIMITE_256_GRIGI )
					ValoriOk ++;

			//Controllo se eliminare la colonna
			if( ValoriOk <= NR_VALORI_OK )
			{
				NrColonneNere ++;
				ValoriOk = 0;
			}
			else
				ContinueClear = FALSE;

			// Reinizializzo il pointer ad inizio bitmap
			pbmt = pbm + NrColonneNere;

		} while( ContinueClear && (NrColonneNere < nr_col_bmp) );


		//Controllo se shiftare la bitmap
		if( NrColonneNere && (NrColonneNere < nr_col_bmp) )
		{
			// Inizializzo i pointer
			pbmt = pbmtm = pbm;
			NrByte = nr_col_bmp;

			// Calcolo il NUOVO numero di colonne e lo salvo
			nr_col_bmp -= NrColonneNere;
			Bitmap->biWidth = nr_col_bmp;

			if( CondDib = (nr_col_bmp % 4) )
				nr_col_bmp = (nr_col_bmp + 4 - CondDib);

			// Sposto la bitmap
			for( ii = 0; ii < (unsigned long)Bitmap->biHeight; ii ++)
			{
				pbmt += NrColonneNere;
				for( jj = NrColonneNere; jj < (unsigned long)NrByte; jj ++)
					*(pbmtm++) = *(pbmt++);

				// Aggiungo il nero
				for( jj = 0; jj < (unsigned long)(nr_col_bmp - Bitmap->biWidth); jj ++)
					*(pbmtm++) = 0x00;	//NERO;
			}

			// Aggiorno il nr. di bytes della bitmap
			Bitmap->biSizeImage = Bitmap->biHeight * nr_col_bmp;
		}

//	SalvaDIBBitmap(Bitmap, "\\Progetti\\bmp\\front3.bmp");


		// Inizio dati bitmap
		pbm = (unsigned char *)Bitmap + sizeof(BITMAPINFOHEADER) + (256 * sizeof(RGBQUAD));
		if(CondDib = (Bitmap->biWidth % 4) )
			nr_col_bmp = (Bitmap->biWidth + 4 - CondDib);
		else
			nr_col_bmp = Bitmap->biWidth;
		nr_row_bmp = Bitmap->biHeight;
		pbmt = pbm + nr_col_bmp - 1;
		ContinueClear = TRUE;
		ValoriOk = 0;
		NrColonneNere = 0;

		// Tolgo le colonne nere dietro
		do
		{
			for( ii = 0; ii < (unsigned long)nr_row_bmp; ii ++, pbmt += nr_col_bmp)
				if( *pbmt > LIMITE_256_GRIGI )
					ValoriOk ++;

			//Incremento il counter nr. colonne nere
			if( ValoriOk <= NR_VALORI_OK )
			{
				NrColonneNere ++;
				ValoriOk = 0;
			}
			else
				ContinueClear = FALSE;

			// Reinizializzo il pointer ad inizio bitmap
			pbmt = pbm + (nr_col_bmp - 1) - NrColonneNere;

		} while( ContinueClear && (NrColonneNere < nr_col_bmp) );

		if( NrColonneNere && (NrColonneNere < nr_col_bmp) )
		{
			DiffWidth = Bitmap->biWidth;

			// Aggiorno l'header della bitmap
			Bitmap->biWidth -= NrColonneNere;

			RealWidth = Bitmap->biWidth;

			// Calcolo la larghezza reale della bitmap
			if( CondDib = DiffWidth % 4 )
				DiffWidth += (4 - CondDib);

			if( CondDib = RealWidth % 4 )
				RealWidth += (4 - CondDib);

			DiffWidth -= RealWidth;

			Bitmap->biSizeImage = Bitmap->biHeight * RealWidth;


			//Controllo se shiftare la bitmap
			if( NrColonneNere )
			{
				pbm += RealWidth;
				pbmt = pbm;

				// Sposto la bitmap
				for( ii = 1; ii < (unsigned long)Bitmap->biHeight; ii ++)
				{
					pbmt += DiffWidth;
					for( jj = 0; jj < (unsigned long)RealWidth; jj ++)
						*(pbm++) = *(pbmt++);
				}
			}
		}

//	SalvaDIBBitmap(Bitmap, "\\Progetti\\bmp\\front_256.bmp");

	} // Fine else

} // ClearFrontAndRearBlack



int Ls150Class::Periferica::ViewE13BSignal(int fView)
{

	char BufCodelineHW[256];
	unsigned long len_codeline;
	long llDati;
	unsigned char pDati[32*1024];
	//int NrDoc;
	short NrChar;
	short NrCampioni;
	short index;
	short Connessione;

	 llDati = 32*1024;
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	//Reply = LS150_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);

	if( Reply == LS_OKAY )
	{
		// Setto il diagnostic mode
	//	Reply = LS150_SetDiagnosticMode((HWND)hDlg, TRUE);
		if (Reply == LS_OKAY) 
		{
			// Chiamo la funzione passando l'ultima configurazione settata
			///*Reply = LS150_DocHandle((HWND)hDlg,
			//				SUSPENSIVE_MODE,
			//				NO_FRONT_STAMP,
			//				NO_PRINT_VALIDATE,
			//				READ_CODELINE_MICR,
			//				SIDE_NONE_IMAGE,
			//				SCAN_MODE_256GR200,
			//				AUTOFEED,
			//				SORTER_BAY1,
			//				WAIT_YES,
			//				NO_BEEP,
			//				0,
			//				0);*/
			Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								READ_CODELINE_MICR,
								SIDE_NONE_IMAGE,
								SCAN_MODE_256GR200,
								AUTOFEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);
			if (Reply == LS_OKAY) 
			{
				// Leggo la codeline
				len_codeline = 256;
				Reply = LSReadCodeline(Connessione,(HWND)hDlg,
										BufCodelineHW,
										(short *)&len_codeline,NULL,
										NULL,NULL,
										0);
				
				Reply = LSReadE13BSignal(Connessione,(HWND)hDlg, pDati, (long *)&llDati);
				if (Reply == LS_OKAY)
				{
					// salvo nella variabile pubblica i dati 


					if (fView)
					{
						NrChar = 0;
						while( (pDati[NrChar]) != E13B_FINE_LLC )
							NrChar ++;
						NrCampioni =(((pDati[NrChar + 2])<<8) + pDati[NrChar + 1]);
						index = NrChar + 1 + 2;		// Vado ad inizio dati segnale
					}
					else
					{
						NrCampioni	= (short)llDati;
						index = 0;
					}
					//this->E13BSignal   = new Byte[NrCampioni];
					for ( long ii = 0 ; ii<NrCampioni;ii++)
					{
						//this->E13BSignal[ii] = pDati[ii+index];
						E13BSignal[ii] = pDati[ii+index];
					}
				}
				// Tolgo il diagnostic mode
			//	LS150_SetDiagnosticMode((HWND)hDlg, FALSE);

				LSDisconnect(Connessione,(HWND)hDlg);
			}


		}
	}

	return Reply ;
}


int Ls150Class::Periferica::DisegnaEstrectLenBar(short NrChar)
{
	register int  xx;
	short ii, Save_ii;
	BOOL fFine;
	short NrLenghtBar = 7;	// Nr. raggruppamento barre
	short NrCampioni;
	short  NrValori;
	short NrBarre;

	short Save_pdct;	// Ptr. temp a caratteri




//	FILE *fh;
//	char *ptmp;
//	char FileOut[_MAX_FNAME];
	short index,index2;
//	this->ViewSignal = new Byte[10000];

	if( TypeLS == TYPE_LS150_VE )
	{
		NrCampioni = NrChar;
		//pdct = pd;
		index = 1;
	}
	else
	{
		//NrCampioni = *(short *)(pd + NrChar + 1);
		//pdct = (unsigned char *)(pd + NrChar + 1 + 2);		// Vado ad inizio dati segnale
		//pdct = (unsigned char *)(((unsigned char *)pdct) + (DIM_DESCR_CHAR * NrChar));	// Vado ad inizio dati segnale

		NrCampioni =(((E13BSignal[NrChar + 2])<<8) + E13BSignal[NrChar + 1]);
		index = NrChar + 1 + 2;		// Vado ad inizio dati segnale
		index = index + (DIM_DESCR_CHAR * NrChar);	// Vado ad inizio dati segnale
		NrCampioni -= (DIM_DESCR_CHAR * NrChar);
	}

	

	// Da lasciare o da togliere ? Bah !!!   Solo il destino lo dir� !
	//NrCampioni -= (DIM_DESCR_CHAR * NrChar);

	// Cerco l'ultimo valore valido
	index2 = index + NrCampioni;
	while( E13BSignal[index2-1] < SOGLIA_INIZIO_NERO )
	{
		index2 --;
		NrCampioni --;
	}
	// Ne tengo uno dei non validi come tappo
	NrCampioni ++;

	// Elimino tre barre finali
	for( ii = 0; ii < 3; ii++)
	{
		// Elimino la prima mezza barra, un'eventuale parte nera ...
		while( E13BSignal[index2-1] < SOGLIA_INIZIO_NERO )
		{
			index2 --;
			NrCampioni --;
		}

		// ... e la parte bianca, cos� parto sempre da un nero
		while( E13BSignal[index2-1] >= SOGLIA_INIZIO_NERO )
		{
			index2 --;
			NrCampioni --;
		}
	}

	// Elimino tre barre iniziali
	for( ii = 0; ii < 3; ii++)
	{
		// Elimino la prima mezza barra, un'eventuale parte nera ...
		while( E13BSignal[index-1] < SOGLIA_INIZIO_NERO )
		{
			index ++;
			NrCampioni --;
		}

		// ... e la parte bianca, cos� parto sempre da un nero
		while( E13BSignal[index-1] >= SOGLIA_INIZIO_NERO )
		{
			index ++;
			NrCampioni --;
		}
	}

	// Conto i valori di una oscillazione

	fFine = FALSE;
	NrValori = 0;
	NrBarre = 1;
	llMax = 0;
	llMin = NrLenghtBar * 0xff;	// setto il massimo
	llMedia = 0;
	xx = 0;
	index2 = index;
	for( ii = 1; ii <= NrCampioni; ii ++)
	{
		//Sono arrivato alla fine di una barra ?
		if( (fFine == TRUE ) && (E13BSignal[index-1] < SOGLIA_INIZIO_NERO) && (E13BSignal[index] < SOGLIA_INIZIO_NERO))
		{
			if( NrBarre == 1 )
			{
				Save_pdct = index;
				Save_ii = ii;
			}

			if( NrBarre < NrLenghtBar )
				NrBarre ++;
			else
			{
				ViewSignal[xx] = (unsigned char)NrValori;
				xx ++;

				// Se il valore esce dal grafico ... lo forzo al valore massimo
				if( NrValori > (HEIGHT_BAR * NrLenghtBar) )
					NrValori = (HEIGHT_BAR * NrLenghtBar) - 1;
				
				// Salvo il minimo ed il massimo
				if( NrValori < llMin )
					llMin = NrValori;
				if( NrValori > llMax )
					llMax = NrValori;

				// Aggiungo il valore alla media
				llMedia += NrValori;

				// Riazzero le variabili
				NrValori = 0;
				NrBarre = 1;
				index = Save_pdct;
				ii = Save_ii;
			}

			fFine = FALSE;
		}

		// Il nero lo somma per FALSE, il bianco per TRUE
		if( (fFine == FALSE) && (E13BSignal[index-1] >= SOGLIA_INIZIO_NERO) && (E13BSignal[index] >= SOGLIA_INIZIO_NERO) )
			fFine = TRUE;

		// Sposto il puntatore
		index ++;
		NrValori ++;
	}


	// Calcolo media
	llMedia = llMedia / xx;

	// Calcolo deviazione standard
	index = index2;	// Vado ad inizio dati
	DevStd = 0;
	for( ii = 1; ii <= NrCampioni; ii ++)
	{
		//Sono arrivato alla fine di una barra ?
		if( (fFine == TRUE ) && (E13BSignal[index-1] < SOGLIA_INIZIO_NERO) )
		{
			if( NrBarre == 1 )
			{
				Save_pdct = index;
				Save_ii = ii;
			}

			if( NrBarre < NrLenghtBar )
				NrBarre ++;
			else
			{
				DevStd += (float)(Math::Pow((double)(NrValori - llMedia), 2));

				// Riazzero le variabili
				NrValori = 0;
				NrBarre = 1;
				index = Save_pdct;
				ii = Save_ii;
			}

			fFine = FALSE;
		}

		// Il nero lo somma per FALSE, il bianco per TRUE
		if( (fFine == FALSE) && (index >= SOGLIA_INIZIO_NERO) )
			fFine = TRUE;

		// Sposto il puntatore
		index ++;
		NrValori ++;
	}

	DevStd /= xx;
	DevStd = (float)Math::Sqrt(DevStd);

return 1;
}




int Ls150Class::Periferica::Docutest04()
{
	ReturnC->FunzioneChiamante = "Test Doppia sfogliatura";
	short Connessione;

	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	//Reply = LS150_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( Reply == LS_OKAY )
	{
		//Reply = LS150_DoubleLeafingSensibility((HWND)hDlg,DOUBLE_LEAFING_DEFAULT);
		//Reply  = LS150_ConfigDoubleLeafing((HWND)hDlg,DOUBLE_LEAFING_ERROR,DOUBLE_LEAFING_DEFAULT,0);
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_ERROR,50,100,225);
		///*Reply = LS150_DocHandle((HWND)hDlg,
		//						SUSPENSIVE_MODE,
		//						NO_FRONT_STAMP,
		//						NO_PRINT_VALIDATE,
		//						NO_READ_CODELINE,
		//						SIDE_NONE_IMAGE,
		//						SCAN_MODE_256GR200,
		//						AUTOFEED,
		//						SORTER_BAY1,
		//						WAIT_YES,
		//						NO_BEEP,0,0);*/
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								SIDE_NONE_IMAGE,
								SCAN_MODE_256GR200,
								AUTOFEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);
		
		//Reply = LS150_DoubleLeafingSensibility((HWND)hDlg,DOUBLE_LEAFING_DISABLE);
		//Reply  = LS150_ConfigDoubleLeafing((HWND)hDlg,DOUBLE_LEAFING_ERROR,DOUBLE_LEAFING_DISABLE,0);
		//Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,150,215);
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);
		LSDisconnect(Connessione,(HWND)hDlg);
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}

	ReturnC->ReturnCode = Reply;
	

	return Reply;	//ret
}

int Ls150Class::Periferica::Reset(short TypeReet)
{
	short Connessione;
	
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	if( Reply == LS_OKAY )
	{
		Reply = LSReset(Connessione,(HWND)0,(char)TypeReet);

		LSDisconnect(Connessione,(HWND)hDlg);
	}

	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Reset";

	return Reply;	
}

#if 1 
//*******************************************************************
// FUNCTION  : DisegnaEstractLenBar
//
// PARAMETER :	pd		(I) Ptr buffer dati.
//				pb		(O) Ptr a bitmap segnale.
//
// PURPOSE   : Disegna il grafico delle lunghezze barre sulla bitmap.
//*******************************************************************
int Ls150Class::Periferica::DisegnaEstractLenBar(short NrChar, unsigned char *pd, char *pb, BOOL fSaveData, char *strDocSpeed , char *FileNameReport)
{
	int		xx, yy;
	short	ii, Save_ii;
	BOOL	fFine;
	short	NrLenghtBar = BARRE_GROUP;	// Nr. raggruppamento barre
	short	NrCampioni;
	short	NrValori; //, NrChar;
	short	NrBarre;
	BITMAPINFOHEADER *pbi;
	unsigned char *pbt, *pbt2;			// Ptr. temp a bitmap
	unsigned char *pdct, *Save_pdct;	// Ptr. temp a caratteri
	unsigned char *pdct2;				// Ptr. temp a caratteri
	unsigned char *pOutBitmap;			// Ptr. a fine area bitmap
	short	Width, Height;
	long	nColorData;
	unsigned char Colore;
	short	OffChar;
	char	ValStr[16];

	FILE *fh = NULL;
	char FileOut[_MAX_FNAME];
	//char *StrNome ;


	// Inizializzo variabili locali
	pbi = (BITMAPINFOHEADER *)pb;
	Width = (short)pbi->biWidth;
	Height = (short)pbi->biHeight;
	nColorData = 1 << pbi->biBitCount;
	pbt = (unsigned char *)(pb + sizeof(BITMAPINFOHEADER) + (nColorData * sizeof(RGBQUAD)));
	pOutBitmap = (pbt + (Width * Height));

	// Disegno le righe orizzontali
	for( yy = (Width * OFFSET_SCALA_X); yy < (Width * (OFFSET_SCALA_X + Height)); yy += (Width * 10))
		for( xx = 0; xx < Width; xx ++)
			*(pbt + xx + yy) = GRIGIO;

	// Disegno assi scale x e y
	yy = (Width * OFFSET_SCALA_X);
	for( xx = 0; xx < Width; xx ++)
		*(pbt + xx + yy) = NERO;

	for( yy = 0; yy < Height; yy ++)
		*(pbt + (yy * Width) + OFFSET_SCALE_Y) = NERO;

	// Disegno le frecce
	*(pbt + ((Width * OFFSET_SCALA_X) + (Width * 3) - 3)) = NERO;
	*(pbt + ((Width * OFFSET_SCALA_X) + (Width * 2) - 2)) = NERO;
	*(pbt + ((Width * OFFSET_SCALA_X) - 2)) = NERO;
	*(pbt + ((Width * OFFSET_SCALA_X) - (Width + 3))) = NERO;

	*(pbt + ((Width * Height) - ((Width * 2) - 15))) = NERO;
	*(pbt + ((Width * Height) - ((Width * 2) - 17))) = NERO;
	*(pbt + ((Width * Height) - ((Width * 3) - 14))) = NERO;
	*(pbt + ((Width * Height) - ((Width * 3) - 18))) = NERO;

	// Apro il file di save caratteri
	if( fSaveData )
	{
		
		//StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileReport ).ToPointer();
		//if( strlen(stParDocHandle.fileReport) )
		{
			sprintf(FileOut, "%s_%s_E13B_%d_bar.txt", FileNameReport, strDocSpeed, NrLenghtBar);
			fh = fopen(FileOut, "at");
		}
	}

	//if( TypeLS == TYPE_LS150_VE )
	//{
	//	NrCampioni = NrChar;
	//	pdct = pd;
	//}
	//else
	{
		NrCampioni = *(short *)(pd + NrChar + 1);
		pdct = (unsigned char *)(pd + NrChar + 1 + 2);		// Vado ad inizio dati segnale
		pdct = (unsigned char *)(((unsigned char *)pdct) + (DIM_DESCR_CHAR * NrChar));	// Vado ad inizio dati segnale

		NrCampioni -= (DIM_DESCR_CHAR * NrChar);
	}

	// Disegno grafico
	// Cerco l'ultimo valore valido
	pdct2 = pdct + NrCampioni;
	while( *pdct2 < SOGLIA_INIZIO_NERO )
	{
		pdct2 --;
		NrCampioni --;
	}
	// Ne tengo uno dei non validi come tappo
	NrCampioni ++;

	// Elimino tre barre finali
	for( ii = 0; ii < 3; ii++)
	{
		// Elimino la prima mezza barra, un'eventuale parte nera ...
		while( *pdct2 < SOGLIA_INIZIO_NERO )
		{
			pdct2 --;
			NrCampioni --;
		}

		// ... e la parte bianca, cos� parto sempre da un nero
		while( *pdct2 >= SOGLIA_INIZIO_NERO )
		{
			pdct2 --;
			NrCampioni --;
		}
	}

	// Elimino tre barre iniziali
	for( ii = 0; ii < 3; ii++)
	{
		// Elimino la prima mezza barra, un'eventuale parte nera ...
		while( *pdct < SOGLIA_INIZIO_NERO )
		{
			pdct ++;
			NrCampioni --;
		}

		// ... e la parte bianca, cos� parto sempre da un nero
		while( *pdct >= SOGLIA_INIZIO_NERO )
		{
			pdct ++;
			NrCampioni --;
		}
	}

	// Conto i valori di una oscillazione
	Colore = GRANATA;
	fFine = FALSE;
	NrValori = 0;
	NrBarre = 1;
	llMax = 0;
	llMin = NrLenghtBar * 0xff;	// setto il massimo
	llMedia = 0;
	xx = 0;
	pdct2 = pdct;
	for( ii = 1; ii <= NrCampioni; ii ++)
	{
		//Sono arrivato alla fine di una barra ?
		if( (fFine == TRUE ) && ((*pdct < SOGLIA_INIZIO_NERO) && (*(pdct + 1) < SOGLIA_INIZIO_NERO)) )
		{
			if( NrBarre == 1 )
			{
				Save_pdct = pdct;
				Save_ii = ii;
			}

			if( NrBarre < NrLenghtBar )
				NrBarre ++;
			else
			{
				xx ++;

				// Se il valore esce dal grafico ... lo forzo al valore massimo
				if( NrValori > (HEIGHT_BAR * NrLenghtBar) )
					NrValori = (HEIGHT_BAR * NrLenghtBar) - 1;

				// Setto il pixel
//				*(pbt + xx + ((NrValori + OFFSET_SCALA_X) * Width) + OFFSET_SCALE_Y) = Colore;

				// Prima di settare il pixel controllo se sono fuori dalla memoria bitmap
				// Se sono dentro l'area scrivo il valore, altrimenti no !
				pbt2 = (pbt + xx + ((NrValori + OFFSET_SCALA_X) * Width) + OFFSET_SCALE_Y);
				if( pbt2 <= pOutBitmap )
					// *(pbt + xx + ((NrValori + OFFSET_SCALA_X) * Width) + OFFSET_SCALE_Y) = Colore;
					*pbt2 = Colore;


				// Salvo il valore su file
				if( fSaveData && fh )
					fprintf(fh, "%d ", NrValori);


				// Salvo il minimo ed il massimo
				if( NrValori < llMin )
					llMin = NrValori;
				if( NrValori > llMax )
					llMax = NrValori;

				// Aggiungo il valore alla media
				llMedia += NrValori;

				// Riazzero le variabili
				NrValori = 0;
				NrBarre = 1;
				pdct = Save_pdct;
				ii = Save_ii;
			}

			fFine = FALSE;
		}

		// Il nero lo somma per FALSE, il bianco per TRUE
		if( (fFine == FALSE) && ((*pdct >= SOGLIA_INIZIO_NERO) && (*(pdct + 1) >= SOGLIA_INIZIO_NERO)) )
			fFine = TRUE;

		// Sposto il puntatore
		pdct ++;
		NrValori ++;
	}


	// Calcolo media
	llMedia = llMedia / xx;

	// Calcolo deviazione standard
	xx = 0;
	DevStd = 0;
	fFine = FALSE;
	NrValori = 0;
	NrBarre = 1;
	pdct = pdct2;	// Vado ad inizio dati
	for( ii = 1; ii <= NrCampioni; ii ++)
	{
		//Sono arrivato alla fine di una barra ?
		if( (fFine == TRUE ) && ((*pdct < SOGLIA_INIZIO_NERO) && (*(pdct + 1) < SOGLIA_INIZIO_NERO)) )
		{
			if( NrBarre == 1 )
			{
				Save_pdct = pdct;
				Save_ii = ii;
			}

			if( NrBarre < NrLenghtBar )
				NrBarre ++;
			else
			{
				DevStd += (float)(pow((NrValori - llMedia), 2));
				xx ++;

				// Riazzero le variabili
				NrValori = 0;
				NrBarre = 1;
				pdct = Save_pdct;
				ii = Save_ii;
			}

			fFine = FALSE;
		}

		// Il nero lo somma per FALSE, il bianco per TRUE
		if( (fFine == FALSE) && ((*pdct >= SOGLIA_INIZIO_NERO) && (*(pdct + 1) >= SOGLIA_INIZIO_NERO)) )
			fFine = TRUE;

		// Sposto il puntatore
		pdct ++;
		NrValori ++;
	}

	DevStd /= xx;
	DevStd = (float)sqrt(DevStd);

	// Scrivo i valori di Min. Max. e Media sulla bitmap
	Height -= 20;
	OffChar = 30;
	//DisegnaChar(pbt, Width, OffChar, Height, 'M');
	//DisegnaChar(pbt, Width, (short)(OffChar + 10), Height, 'i');
	//DisegnaChar(pbt, Width, (short)(OffChar + 14), Height, 'n');
	//DisegnaChar(pbt, Width, (short)(OffChar + 25), Height, '=');
	itoa(llMin, ValStr, 10);
	for( ii = 0, xx = (OffChar + 33); ValStr[ii] != '\0'; ii++, xx += 7)
	{
		//DisegnaChar(pbt, Width, (short)xx, Height,	ValStr[ii]);
		if( ValStr[ii] == '1' )
			xx -= 2;
	}

	// Disegno un Quadrattino Rosso o Verde
	if( llMin < SAMPLE_VALUE_MIN )
	{
		for( xx = (OffChar - 12); xx < (OffChar - 4); xx ++)
			for( yy = Height; yy < (Height + 8); yy ++)
				*(pbt + xx + (yy * Width)) = GRANATA;
	}
	else
	{
		for( xx = (OffChar - 12); xx < (OffChar - 4); xx ++)
			for( yy = Height; yy < (Height + 8); yy ++)
				*(pbt + xx + (yy * Width)) = VERDINO;
	}

	Height -= 20;
	//DisegnaChar(pbt, Width, OffChar, Height, 'M');
	//DisegnaChar(pbt, Width, (short)(OffChar + 10), Height, 'a');
	//DisegnaChar(pbt, Width, (short)(OffChar + 18), Height, 'x');
	//DisegnaChar(pbt, Width, (short)(OffChar + 28), Height, '=');
	itoa(llMax, ValStr, 10);
	for( ii = 0, xx = (OffChar + 36); ValStr[ii] != '\0'; ii++, xx += 7)
	{
		//DisegnaChar(pbt, Width, (short)xx, Height, ValStr[ii]);
		if( ValStr[ii] == '1' )
			xx -= 2;
	}

	// Disegno un Quadrattino Rosso o Verde
	if( llMax > SAMPLE_VALUE_MAX )
	{
		for( xx = (OffChar - 12); xx < (OffChar - 4); xx ++)
			for( yy = Height; yy < (Height + 8); yy ++)
				*(pbt + xx + (yy * Width)) = ROSSO;
	}
	else
	{
		for( xx = (OffChar - 12); xx < (OffChar - 4); xx ++)
			for( yy = Height; yy < (Height + 8); yy ++)
				*(pbt + xx + (yy * Width)) = VERDINO;
	}

	Height -= 20;
	/*DisegnaChar(pbt, Width, OffChar, Height, 'M');
	DisegnaChar(pbt, Width, (short)(OffChar + 10), Height, 'e');
	DisegnaChar(pbt, Width, (short)(OffChar + 18), Height, 'd');
	DisegnaChar(pbt, Width, (short)(OffChar + 26), Height, 'i');
	DisegnaChar(pbt, Width, (short)(OffChar + 30), Height, 'a');
	DisegnaChar(pbt, Width, (short)(OffChar + 40), Height, '=');*/
	sprintf(ValStr,"%.2f", llMedia);
	for( ii = 0, xx = (OffChar + 48); ValStr[ii] != '\0'; ii++, xx += 7)
	{
		//DisegnaChar(pbt, Width, (short)xx, Height, ValStr[ii]);
		if( ValStr[ii] == '1' )
			xx -= 2;
		if( ValStr[ii] == '.' )
			xx -= 4;
	}

	Height -= 20;
	/*DisegnaChar(pbt, Width, OffChar, Height, 'D');
	DisegnaChar(pbt, Width, (short)(OffChar + 9), Height, 'e');
	DisegnaChar(pbt, Width, (short)(OffChar + 17), Height, 'v');
	DisegnaChar(pbt, Width, (short)(OffChar + 24), Height, '.');
	DisegnaChar(pbt, Width, (short)(OffChar + 29), Height, 'S');
	DisegnaChar(pbt, Width, (short)(OffChar + 37), Height, 't');
	DisegnaChar(pbt, Width, (short)(OffChar + 42), Height, 'a');
	DisegnaChar(pbt, Width, (short)(OffChar + 50), Height, 'n');
	DisegnaChar(pbt, Width, (short)(OffChar + 57), Height, 'd');
	DisegnaChar(pbt, Width, (short)(OffChar + 65), Height, 'a');
	DisegnaChar(pbt, Width, (short)(OffChar + 73), Height, 'r');
	DisegnaChar(pbt, Width, (short)(OffChar + 78), Height, 'd');
	DisegnaChar(pbt, Width, (short)(OffChar + 88), Height, '=');*/
	sprintf(ValStr,"%.3f", DevStd);
	for( ii = 0, xx = (OffChar + 96); ValStr[ii] != '\0'; ii++, xx += 7)
	{
		//DisegnaChar(pbt, Width, (short)xx, Height, ValStr[ii]);
		if( ValStr[ii] == '1' )
			xx -= 2;
		if( ValStr[ii] == '.' )
			xx -= 4;
	}


	if( fSaveData && fh )
	{
		fprintf(fh, "\n");
		fclose( fh );
	}

	return 0;

}  // DisegnaEstractLenBar

#endif

//
// Esecuzione lettura documento per calcolo media valori testina MICR
//

int Ls150Class::Periferica::ExecReadDocSpeed(char TypeSpeed, short StartFrom, short Sorter)
{
	short	Reply;
	short	ScanMode;
	short	Stamp;
	unsigned char *pDati;
	long	llDati = 0x10000;		// 64 Kb. Dati da chiedere
	FILE	*fh = NULL;
	char	FileOut[_MAX_FNAME];
	short Connessione;	
	short	nrC, nrS;
	short	Width, Diff;
	unsigned char *pb;
	//BITMAPHANDLE pbh;
	long	nColorData = 256;
	//int		SavefOptionFitImage;
	//char	Titolo[200];
	char   *StrNome ; 
	short	lenCodeline;
	char	BufCodelineHW[CODE_LINE_LENGTH];
	char szTextMsg[1024];
	char strDocSpeed[1024];

	Stamp = NO_STAMP;

	
	pDati = (unsigned char *)GlobalAlloc(GPTR, llDati);

	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	if (Reply == LS_OKAY )
	{

		// Aggiungo la valocita al nome file
		if( TypeSpeed == TEST_SPEED_200_DPI )
		{
			ScanMode = SCAN_MODE_256GR200;
			strcpy(strDocSpeed, "_256gray_200dpi");
		}
		else if( TypeSpeed == TEST_SPEED_300_DPI )
		{
			ScanMode = SCAN_MODE_256GR300;
			strcpy(strDocSpeed, "_256gray_300dpi");
		}
		else if( TypeSpeed == TEST_SPEED_75_DPM )
		{
		ScanMode = SCAN_MODE_256GR200;
		//Stamp = FRONT_STAMP;
		LSSetUnitSpeed(Connessione, (HWND)hDlg, SPEED_STAMP);

		strcpy(strDocSpeed, "_256gray_200dpi_Stamp");
		}
		else if( TypeSpeed == TEST_SPEED_300_DPI_UV )
		{
			ScanMode = SCAN_MODE_256GR300_AND_UV;
			strcpy(strDocSpeed, "_256grayUV_300dpi");
		}
		else if( TypeSpeed == TEST_SPEED_300_COLOR )
		{
			ScanMode = SCAN_MODE_COLOR_300;
			strcpy(strDocSpeed, "_Color_300dpi");
		}
		else
		{
			ScanMode = SCAN_MODE_256GR200;
			strcpy(strDocSpeed, "_256gray_200dpi");
		}

		if( pDati )
		{
			// Chiamo la funzione passando l'ultima configurazione settata
			Reply = LSDocHandle(Connessione, (HWND)hDlg,
								Stamp,
								NO_PRINT_VALIDATE,
								READ_CODELINE_MICR,
								SIDE_NONE_IMAGE,
								ScanMode,
								StartFrom,
								Sorter,
								WAIT_YES,
								NO_BEEP,
								NULL,
								0,
								0);

			if( Reply == LS_OKAY )
			{
				// Lettura segnale CMC7
				Reply = LSReadCMC7Signal(Connessione, (HWND)hDlg, pDati, 2048);
				if( Reply != LS_OKAY)
				{
					//CheckReply(hDlg, Reply, "LSReadCMC7Signal");
					GlobalFree( pDati );
					return Reply;
				}

				// Salvo il segnale CMC7 per Flecchia
				StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileReport ).ToPointer();
				sprintf(FileOut, "%s_%s_CMC7_value.txt", StrNome, strDocSpeed);
				if( fh = fopen(FileOut, "at") )
				{
					nrC = 0;
					// Il tappo e' fatto da 20 byte a 00 (o 10 short, o 5 long )
					while( (((long)pDati[nrC]) != 0) || (((long)pDati[nrC + 1]) != 0) || (((long)pDati[nrC + 2]) != 0) || (((long)pDati[nrC + 3]) != 0) || (((long)pDati[nrC + 4]) != 0))
					{
						fprintf(fh, "%d ", pDati[nrC]);
						nrC ++;
					}
					fprintf(fh, "\n");
					fclose( fh );
				}
				
				// Lettura segnale E13B
				Reply = LSReadE13BSignal(Connessione, (HWND)hDlg, pDati, &llDati);
				if( Reply != LS_OKAY)
				{
					//CheckReply(hDlg, Reply, "LS150_ReadE13BSignal");
					GlobalFree( pDati );
					return Reply;
				}

				// Verifica presenza della directory di salvataggio file dati segnale E13B
				//if( GetFileAttributes("Dati") != 0xFFFFFFFF)
				//{
					// Se esiste la directory creo i files
					//sprintf(FileOut, "%s\\dati\\SgnE13B_LS150_%04d.cts", PathAppl, stParDocHandle.NrNomeFile++);
					//if( (fh = fopen(FileOut, "wb")) != NULL )
					//{
					//	fwrite(pDati, sizeof(unsigned char), llDati, fh);
					//	fclose( fh );
					//}
				//}

				// Se c'� una sola lunghezza il documento � passato al contrario
				if( pDati[0] == 0x0d || pDati[1] == 0x0d || pDati[2] == 0x0d || pDati[3] == 0x0d )
				{
					//MessageBox(hDlg, "Document Wrong !", TITLE_ERROR, MB_OK | MB_ICONSTOP);
					GlobalFree( pDati );
					return LS_CALIBRATION_FAILED;
				}

				
				//{
					// Conto i bytes larghezza char
					nrC = 0;
					while( *(pDati + nrC) != E13B_FINE_LLC )
						nrC ++;

					// Estraggo il numero di segnali
					nrS = *(short *)(pDati + nrC + 1);		// 1 per il CR
					nrS -= 2;	// Il valore comprende anche i 2 bytes di nr. campioni segnale
				}

				if( nrS )	// Costrisco bitmap
				{
					Width = ((nrS / BARRE_GROUP_GRAF) + SIZE_SCALE + OFFSET_SCALE_Y);
					// Allineo la bitmap a multiplo di 4
					if( (Diff = Width % 4) )
						Width += (4 - Diff);

					pb = (unsigned char *)GlobalAlloc(GPTR, (sizeof(BITMAPINFOHEADER) + (nColorData * sizeof(RGBQUAD)) + (Width * HEIGHT_SIGNAL)));

				//	CreaWhiteBitmap((char *)pb, (short)HEIGHT_SIGNAL, Width);

				//	// Costruisco bitmap velocit� e visualizzo
					DisegnaEstractLenBar(nrC, pDati, (char *)pb, TRUE, strDocSpeed,StrNome);

				//	// Setto FitImage = FALSE per avere la window normal
				//	//ConverteDIBtoLEAD((BITMAPINFO *)pb, &pbh);

					GlobalFree( pb );

				//	// Setto FitImage = FALSE per avere la window normal
				//	SavefOptionFitImage = fOptionFitImage;	// Salvo il valore
				//	fOptionFitImage = FALSE;

				//	//Setto il flag di image not present
				//	//pbh.Flags.Allocated = TRUE;

				//	//sprintf(Titolo, "Bitmap Width char E13B -*- Min.=%d, Max=%d", llMin, llMax);
				//	//CreateChildWindow("", Titolo, &pbh);
				//	//fOptionFitImage = SavefOptionFitImage;	// Ripristino il vecchio valore
				//}

				// Leggo la codeline per cancellare il buffer
				lenCodeline = CODE_LINE_LENGTH;
				Reply = LSReadCodeline(Connessione, (HWND)hDlg,
										BufCodelineHW,
										&lenCodeline,
										NULL, NULL,
										NULL, NULL);

				sprintf(szTextMsg, " %ld,           %ld,                %.2f,                  %.3f", llMin, llMax, llMedia, DevStd);
				// ... il file se impostato ...
				StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileReport ).ToPointer();
				sprintf(FileOut, "%s_%s_Report.txt", StrNome, strDocSpeed);
				if ((fh = fopen(FileOut, "at"))) 
				{
					fprintf(fh, "%s\n", szTextMsg);
					fclose( fh );
				}


			}
			else
				CheckReply((HWND)hDlg, Reply, "LS150_DocHandle");

			GlobalFree( pDati );
		} // if pDati
	}
	else
	{
		//connessione KO
	}


	if( TypeSpeed == TEST_SPEED_75_DPM )
		// Ripristino la normal speed
		LSSetUnitSpeed(Connessione, (HWND)hDlg, SPEED_DEFAULT);

	return Reply;
	//return 0 ;
} // ExecReadDocSpeed


//
// Esecuzione lettura documento per calcolo media valori testina MICR
//
//'La calibrazione della velocit� di campionamento permette alla periferica di eseguire in modo corretto la decodifica dei caratteri micr  
int Ls150Class::Periferica::DoTimeSampleMICRCalibration(char TypeSpeed,float RangeTollerance)
{
	BOOL	nRet;
	BOOL	fSetDiagnostic ;
	short Connessione;	
	float	DiffPercentuale;
	unsigned short    CurrSampleTime, NewSampleTime , TolleranceSampleTime ;
	float floatNewSampleTime;
	char  Stamp;
	short ScanMode;
	short	nrC, nrS;
	char	BufCodelineHW[CODE_LINE_LENGTH];
	short	len_codeline;
	unsigned char buffDati[NR_BYTE_BUFFER_E13B];		// Dati da chiedere
	short   llDati; 					// Dati da chiedere
	unsigned char *pDati;
	float	SampleValueMin, SampleValueMax;
	char IdentStr[256],LsName[256],Version[256];
	
	// Setto i valori iniziali
	nRet = TRUE;
	Percentile = 0;
	
	Stamp = NO_STAMP;
	if( TypeSpeed == TEST_SPEED_200_DPI )
		ScanMode = SCAN_MODE_256GR200;
	else if( TypeSpeed == TEST_SPEED_300_DPI )
		ScanMode = SCAN_MODE_256GR300;
	else if( TypeSpeed == TEST_SPEED_75_DPM )
		ScanMode = SCAN_MODE_256GR200;
	else if( TypeSpeed == TEST_SPEED_300_DPI_UV )
		ScanMode = SCAN_MODE_256GR300_AND_UV;
	else if( TypeSpeed == TEST_SPEED_300_COLOR )
		ScanMode = SCAN_MODE_COLOR_300;

	

	//-----------Open--------------------------------------------
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	if( Reply == LS_OKAY )
	{
		fSetDiagnostic = FALSE ;

		Reply = LSUnitIdentify(Connessione, (HWND)0, IdentStr, LsName, Version, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

		//----------- Set Block Document if double leafing -----------
	
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,100,215);
		if (Reply == LS_OKAY)
		{
			//----------- Set speed document -----------
			
			TypeLS = GetTypeLS(LsName,Version,IdentStr);
			
			if( TypeSpeed == TEST_SPEED_75_DPM )
			{
						#if TimbroAlMomentoSoppresso
						// Se c'� lo stamp lo abilito
						if( IdentStr[0] & MASK_STAMP )
						{
							Stamp = FRONT_STAMP;
						}
						else
						#endif
						{
							LSSetUnitSpeed(Connessione, (HWND)hDlg, SPEED_STAMP);
						}
			}
			else //if( TypeSpeed == TEST_SPEED_300_DPI )
			{
				// Set Diagnostic
				fSetDiagnostic = TRUE;

				// Metto la periferica in diagnostica
				LSSetDiagnosticMode(Connessione, (HWND)hDlg, TRUE);
			}
			
			
			//----------- Set attesa introduzione documento -----------
			Reply = LSDisableWaitDocument(Connessione,(HWND)hDlg,FALSE);
			if (Reply == LS_OKAY)
			{
				// Chiamo la funzione passando l'ultima configurazione settata
			
				Reply = LSDocHandle(Connessione,(HWND)hDlg,
								Stamp,
								NO_PRINT_VALIDATE,
								READ_CODELINE_MICR,
								SIDE_NONE_IMAGE,
								ScanMode,
								AUTOFEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);

				if( Reply == LS_OKAY )
				{

					// Leggo la codeline Per liberare i buffer periferica
					len_codeline = CODE_LINE_LENGTH;
					Reply = LSReadCodeline(Connessione,(HWND)hDlg,
												   BufCodelineHW,
												   &len_codeline,
												   NULL,NULL,
												   NULL,
												   0);
					// Risetto la velocit� impostata
					if( fSetDiagnostic )
						// Tolgo il diagnostic mode ...
						LSSetDiagnosticMode(Connessione, (HWND)hDlg, FALSE);
					else
						LSSetUnitSpeed(Connessione, (HWND)hDlg, SPEED_DEFAULT);
					
					llDati = NR_BYTE_BUFFER_E13B;
					Reply = LSReadE13BSignal(Connessione,(HWND)hDlg, buffDati, (long *)&llDati);
					if( Reply == LS_OKAY )
					{
						CurrSampleTime = 0;
						// Leggo il valore settato nella periferica
						Reply = LSReadTimeSampleMICR(Connessione,(HWND)hDlg, TypeSpeed, &CurrSampleTime, 2);
						if( Reply == LS_OKAY )
						{
							// Conto i bytes larghezza char
							//this->E13BSignal = new Byte[llDati];
							for ( long ii = 0 ; ii<llDati;ii++)
							{
								this->E13BSignal[ii] = buffDati[ii];
							}
							pDati = buffDati;
							// tolgo i primi dati che non servono
							if( TypeLS == TYPE_LS150_VE )
							{
								// Conto i bytes larghezza char
								nrC = 0;
								while( *(pDati + nrC) ) //!= E13B_FINE_LLC )
									nrC ++;

								// Forzo llDati diverso da nrC per eseguire l'if !!!
								llDati = nrC + 1000;
							}
							else
							{
								// Conto i bytes larghezza char
								nrC = 0;
								while( *(pDati + nrC) != E13B_FINE_LLC )
									nrC ++;
							}

							// Controllo se ho trovato il fine lunghezza caratteri
							if( nrC != llDati )
							{
								if( TypeLS == TYPE_LS150_VE )
								{
									// Estraggo il numero di segnali
									nrS = nrC;
								}
								else
								{
									// Estraggo il numero di segnali
									nrS = *(short *)(pDati + nrC + 1);		// 1 per il CR
									nrS -= 2;	// Il valore comprende anche i 2 bytes di nr. campioni segnale
								}

								DisegnaEstrectLenBar(nrC);
								// Setto il valore min e max
								if( TypeLS == TYPE_LS150_VE )
								{
									SampleValueMin = SAMPLE_VALUE_MIN_LS150_VE;
									SampleValueMax = SAMPLE_VALUE_MAX_LS150_VE;
								}
								else
								{
									SampleValueMin = SAMPLE_VALUE_MIN;
									SampleValueMax = SAMPLE_VALUE_MAX;
								}			
							} // end if( nrC == llDati )

							if( (llMax - llMin) > 12 )
							{
								//MessageBox(hDlg, "Bad assembly !", TITLE_ERROR, MB_ICONERROR | MB_OK);
								LSDisconnect(Connessione,(HWND)hDlg);
								return LS_CALIBRATION_FAILED;
							}
							
							// Controllo differenza minimo massimo ....inzio.....
							//if( (llMax - llMin) <= 12 )   
							//{
#if 1  
		float valMedio = (float)(llMax + llMin) / 2;
		floatNewSampleTime = (valMedio * (float)CurrSampleTime) / 111.5f;
		if( (floatNewSampleTime - (long)floatNewSampleTime) >= 0.5 )
		{
			floatNewSampleTime += 1;
		}
		// Lo converto in unsigned short
		NewSampleTime = (unsigned short)floatNewSampleTime;


		// Assicuro che la larghezza misurata sia entro i limiti massimo e minimo
		if ( ( llMin >= SampleValueMin ) && ( llMax <= SampleValueMax ) )
		{
			// Assicuro che il tempo di campionamento trovato rimanga stabile nelle 3 letture consecutive 

			// Per essere tale il Tempo di campionamento che si vuole impostare o
			// � identico a quello impostato o si discosta di un solo campione

			// La soglia 330 indica se e' un FW vechio o nuovo con migliore risoluzione
			//if( CurrSampleTime > 330 )
			//	TolleranceSampleTime = 1;
			//else
			//	TolleranceSampleTime = 1;

			// Cambiato controllo in percentuale (0.5%) al posto del +- 1
			//DiffPercentuale = ((float)NewSampleTime * 0.005F);
			DiffPercentuale = ((float)NewSampleTime * RangeTollerance);
			if( (DiffPercentuale - (long)DiffPercentuale) >= 0.5 )
				DiffPercentuale += 1;

			TolleranceSampleTime = (unsigned short)DiffPercentuale;

			if( NewSampleTime == CurrSampleTime || ((NewSampleTime >= (CurrSampleTime - TolleranceSampleTime)) && (NewSampleTime <= (CurrSampleTime + TolleranceSampleTime))) )
			{
				// Aggiorno il valore di letture da fare
				NrLettureOk ++;
				nRet = FALSE;	// Devo continuare la calibrazione
			}
			else
			{
				// Aggiorno il valore di letture da fare
				NrLettureOk = 0;
				nRet = FALSE;	// Devo continuare la calibrazione
			}
		}
		else
		{
			// Aggiorno il valore di letture da fare
			NrLettureOk = 0;
			nRet = FALSE;	// Devo continuare la calibrazione
		}
#else

								float valMedio = (float)(llMax + llMin) / 2;
								DiffPercentuale = (valMedio * (float)CurrSampleTime) / 111.5f;
								if( (DiffPercentuale - (long)DiffPercentuale) >= 0.5 )
								{
									NewSampleTime = (unsigned short)(DiffPercentuale + 1);
								}
								else
								{
									NewSampleTime = (unsigned short)DiffPercentuale;
								}
								 
								 
								// Assicuro che la larghezza misurata sia entro i limiti massimo e minimo
								if ( ( llMin >= SampleValueMin ) && ( llMax <= SampleValueMax ) )
								{
									// Assicuro che il tempo di campionamento trovato rimanga stabile nelle 3 letture consecutive 
								 
									// Per essere tale il Tempo di campionamento che si vuole impostare o
									// � identico a quello impostato o si discosta di un solo campione
									if( NewSampleTime == CurrSampleTime || NewSampleTime == LastBeforeTimeSample1 || NewSampleTime == LastBeforeTimeSample2 || NewSampleTime == LastBeforeTimeSample3 )
									{
										// Aggiorno il valore di letture da fare
										NrLettureOk ++;
										nRet = FALSE;    // Devo riprovare la calibrazione
									}
									else
									{
										// Aggiorno il valore di letture da fare
										NrLettureOk = 0;
										nRet = FALSE;    // Devo riprovare la calibrazione
									 
										if( LastBeforeTimeSample1 == 0 && LastBeforeTimeSample1 != NewSampleTime && (abs(NewSampleTime - CurrSampleTime) < 2) )
											LastBeforeTimeSample1 = NewSampleTime;
										else if( LastBeforeTimeSample2 == 0 && LastBeforeTimeSample2 != NewSampleTime && (abs(NewSampleTime - CurrSampleTime) < 2) )
											LastBeforeTimeSample2 = NewSampleTime;
										else if( LastBeforeTimeSample3 == 0 && LastBeforeTimeSample3 != NewSampleTime && (abs(NewSampleTime - CurrSampleTime) < 2) )
											LastBeforeTimeSample3 = NewSampleTime;
									}
								}
								else
								{
									// Aggiorno il valore di letture da fare
									NrLettureOk = 0;
									LastBeforeTimeSample1 = 0;
									LastBeforeTimeSample2 = 0;
									LastBeforeTimeSample3 = 0;
									nRet = FALSE;    // Devo riprovare la calibrazione
								}
#endif
#if 0
								// Se nRet = FALSE ho variato i valori, perci� vado a scrivere quelli nuovi
								//if( nRet == FALSE )
#endif
								// Se NrLettureOk = 0 vado a scrivere o nuovo valore
								if( NrLettureOk == 0 )
								{
									// 26/1/2015
									// Tolto tutti i controlli di MIN e MAX perch� li fa il FW !!!
									// infatti erano sbagliati !

						//			if( TypeLS == TYPE_LS150_VE )
						//			{
						//			}
						//			else
						//			{
						//				// Controllo il che valore da impostrare stia nel range
						//				if( TypeSpeed == TEST_SPEED_200_DPI )
						//				{
						//					// Controllo che non sia inferiore a default - 10%
						//					if( NewValue < (99 * 0.90) )
						//						NewValue = (unsigned short)(99 * 0.90);	// forzo il valore minimo

						//					// Controllo che non sia superiore a default + 10%
						//					if( NewValue > (99 * 1.10) )
						//						NewValue = (unsigned short)(99 * 1.10);	// forzo il valore massimo
						//				}
						//				else if( TypeSpeed == TEST_SPEED_75_DPM )
						//				{
						//					
						//					if( ! (IdentStr[1] & MASK_SCANNER_UV)  )
						//					{
						//						// Controllo che non sia inferiore a default - 10%
						//						if( NewValue < (201 * 0.90) )
						//							NewValue = (unsigned short)(201 * 0.90);	// forzo il valore minimo

						//						// Controllo che non sia superiore a default + 10%
						//						if( NewValue > (201 * 1.10) )
						//							NewValue = (unsigned short)(201 * 1.10);	// forzo il valore massimo
						//					}
						//					else
						//					{
						//						// Controllo che non sia inferiore a default - 10%
						//						if( NewValue < (189 * 0.90) )
						//							NewValue = (unsigned short)(189 * 0.90);	// forzo il valore minimo

						//						// Controllo che non sia superiore a default + 10%
						//						if( NewValue > (189 * 1.10) )
						//							NewValue = (unsigned short)(189 * 1.10);	// forzo il valore massimo
						//					}
						//				}
						//				else if( TypeSpeed == TEST_SPEED_300_DPI_UV )
						//				{
						//					// Controllo che non sia inferiore a default - 10%
						////					if( NewValue < (242 * 0.90) )
						////						NewValue = (unsigned short)(242 * 0.90);	// forzo il valore minimo

						//					// Controllo che non sia superiore a default + 10%
						////					if( NewValue > (242 * 1.10) )
						////						NewValue = (unsigned short)(242 * 1.10);	// forzo il valore massimo
						//				}
						//				else if( TypeSpeed == TEST_SPEED_300_COLOR )
						//				{
						//					// Controllo che non sia inferiore a default - 10%
						//					if( NewValue < (242 * 0.90) )
						//						NewValue = (unsigned short)(242 * 0.90);	// forzo il valore minimo

						//					// Controllo che non sia superiore a default + 10%
						//					if( NewValue > (242 * 1.10) )
						//						NewValue = (unsigned short)(242 * 1.10);	// forzo il valore massimo
						//				}
						//				else // if( TypeSpeed == TEST_SPEED_300_DPI )
						//				{
						//					// Controllo che non sia inferiore a default - 10%
						//					if( NewValue < (149 * 0.90) )
						//						NewValue = (unsigned short)(149 * 0.90);	// forzo il valore minimo

						//					// Controllo che non sia superiore a default + 10%
						//					if( NewValue > (149 * 1.10) )
						//						NewValue = (unsigned short)(149 * 1.10);	// forzo il valore massimo
						//				}
						//			}

									Reply = LSSetTimeSampleMICR(Connessione,(HWND)hDlg, TypeSpeed, NewSampleTime);
									if( Reply != LS_OKAY )
									{
										TimeSampleVal = -1 ; 
										// ho cambiato il valore del trimmer , devo riprovare , quindi
										// metto reply a RETRY
										//Reply = LS_RETRY;
										nRet = TRUE;
									}		
									else
										TimeSampleVal = NewSampleTime ; 
									//{
										//CheckReply((HWND)hDlg, Reply, "LSSetTimeSampleMICR");
										//Reply = LS_OKAY;								 
										//NrLettureOk = 0;
									//}
								}
								//else
								//{
								//	Reply = LS_RETRY;
								//	NrLettureOk ++;
								//}
							//}
							//else
							//{
								//MessageBox(hDlg, "Bad assembly !", TITLE_ERROR, MB_ICONERROR | MB_OK);						
								//Reply = LS_CALIBRATION_FAILED;
								//NrLettureOk = 0;
							//}
						}
						else
						{
							CheckReply((HWND)hDlg, Reply, "SCSILS_ReadTimeSampleMICR");
							NrLettureOk = 0;
						}
					}
					else
					{
						CheckReply((HWND)hDlg, Reply, "LSReadE13BSignal");
						NrLettureOk = 0;
					}
				}
				else
				{
					CheckReply((HWND)hDlg, Reply, "LSDocHandle");
					NrLettureOk = 0;
				}
			}
			else
			{
				// errore disable wait document
				NrLettureOk = 0;
			}
		}
		else
		{
			// errore set double leafing
			NrLettureOk = 0;
		}
	}
	else
	{
		if( CheckReply((HWND)hDlg, Reply, "LSOpen"))
		{		    
		}
		NrLettureOk = 0;
	}
    LSDisconnect(Connessione,(HWND)hDlg);

	TimeSampleVal = NewSampleTime ; 
	if (TypeSpeed == TEST_SPEED_300_COLOR)
	{
		TempoCamp300C = NewSampleTime;
	}
	if (TypeSpeed == TEST_SPEED_200_DPI)
	{
		TempoCamp200 = NewSampleTime;
	}
	if (TypeSpeed == TEST_SPEED_300_DPI)
	{
		TempoCamp300 = NewSampleTime;
	}
	if (TypeSpeed == TEST_SPEED_300_DPI_UV)
	{
		TempoCamp300UV = NewSampleTime;
	}
	if (TypeSpeed == TEST_SPEED_75_DPM)
	{
		TempoCamp75 = NewSampleTime;
	}

	//return Reply;
	return nRet;
} // DoTimeSampleMICRCalibration




 



int Ls150Class::Periferica::EraseHistory()
{
	//S_HISTORY_LS150 History;
	short Connessione;
	



	ReturnC->FunzioneChiamante = "Cancellazione Storico";

	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	//Reply = LS150_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( Reply == LS_OKAY )
	{
		//History.Size = sizeof  (History);
		//Reply = LSHistoryCommand(Connessione,(HWND)hDlg, CMD_READ_HISTORY, &History);

		//S_Storico->Documenti_Trattati = History.doc_sorted;
		//S_Storico->Documenti_Trattenuti = History.doc_retain;
		//S_Storico->Errori_CMC7 = History.doc_cmc7_err;
		//S_Storico->Errori_E13B = History.doc_e13b_err;
		//S_Storico->Jam_Card = History.jams_card;
		//S_Storico->Jam_Micr = History.jams_micr;
		//S_Storico->Jam_Scanner = History.jams_scanner;
		//S_Storico->Num_Accensioni = History.num_turn_on;
		//S_Storico->Doc_Timbrati = History.doc_stamp;
		//S_Storico->TempoAccensione = History.time_peripheral_on;
		//S_Storico->Documenti_Trattati = History.doc_sorted;
		//
		//LSDisconnect(Connessione,(HWND)hDlg);

		//Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
		
		
		Reply = LSHistoryCommand(Connessione,(HWND)hDlg, CMD_ERASE_HISTORY, NULL);
		if( Reply != LS_OKAY )
		{
			ReturnC->FunzioneChiamante = "LSHistoryCommand";
		}
		
		
		LSDisconnect(Connessione,(HWND)hDlg);
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}


	ReturnC->ReturnCode = Reply;
	return Reply;
}




int Ls150Class::Periferica::AccendiLuce(short Luce)
{
	short ret=0;
	short Connessione;

	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	//Reply = LS150_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( Reply == LS_OKAY )
	{
		Reply = LSSetScannerLight(Connessione,(HWND)hDlg, Luce);	
		LSDisconnect(Connessione,(HWND)hDlg);
	}
	else
	{
		CheckReply(0, Reply, "LSOpen");
	}
	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Test Luci";

	return ret;
}


int Ls150Class::Periferica::MotoreFeeder(short Acceso)
{
	short ret=0;
	short Connessione;
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	//Reply = LS150_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( Reply == LS_OKAY )
	{
		Reply = LSMoveMotorFeeder(Connessione,(HWND)hDlg, Acceso);	
		LSDisconnect(Connessione,(HWND)hDlg);
	}
	else
	{
		CheckReply(0, Reply, "LS150_Open");
	}
	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Test Motore Feeder";

	return ret;
}
//
int Ls150Class::Periferica::sensorFeeder()
{
	short ret=0;
	unsigned short  aa;
	short Connessione;
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	//Reply = LS150_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( Reply == LS_OKAY )
	{
		Reply = LSReadFeederMotorAD(Connessione,(HWND)hDlg,&aa);	
		Sensore = aa;
		LSDisconnect(Connessione,(HWND)hDlg);
	}
	else
	{
		CheckReply(0, Reply, "LSOpen");
	}
	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Test Motore Feeder";

	return ret;

}

int Ls150Class::Periferica::LeggiValoriPeriferica()
{
	int LenghtBuffer = LENGH_DUMP_150;
	char ch = 0x1;
	char TypeSpeed;
	short VelVal = 0;
	unsigned short CurrValue = 0;
	//unsigned char BufferDump1[LENGH_DUMP_150];
	unsigned char buffDati[1024];
	short Connessione;
	unsigned char buffscanner[3000];
	double aaa;

	int nrBytes;

	ReturnC->FunzioneChiamante = "LeggiValoriPeriferica";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	if( Reply == LS_OKAY )
	{
		// leggo il dump della memoria

	/*	Reply  = LSDumpPeripheralMemory(Connessione, 0, (unsigned char*)BufferDump1, LenghtBuffer, 1, ch);
		for (int ii = 0;ii<LenghtBuffer-1;ii++)
			BufferDump[ii] = BufferDump1[ii];*/

		// leggo i valori del trimmer MICR
		
		
		TypeSpeed = TEST_SPEED_200_DPI; 
		
		Reply = LSReadTimeSampleMICR(Connessione,(HWND)hDlg, TypeSpeed, &CurrValue, 2);
		if( Reply  == LS_OKAY)
		{
			TempoCamp200 = CurrValue;
		}
		Reply = LSReadTrimmerMICR(Connessione,(HWND)hDlg, TypeSpeed, buffDati, NR_BYTES_TRIMMER);
		if( Reply  == LS_OKAY)
		{
		TrimmerValue200 = buffDati[0];
		}	
		
		TypeSpeed = TEST_SPEED_300_DPI; 
		Reply = LSReadTimeSampleMICR(Connessione,(HWND)hDlg, TypeSpeed, &CurrValue, 2);
		if( Reply  == LS_OKAY)
		{
			TempoCamp300 = CurrValue;
		}

		Reply = LSReadTrimmerMICR(Connessione,(HWND)hDlg, TypeSpeed, buffDati, NR_BYTES_TRIMMER);
		if( Reply  == LS_OKAY)
		{
			TrimmerValue300 = buffDati[0];
		}

		
		if( TypeLS == TYPE_LS150)
		{
			
			// 75 doc per minuto
			TypeSpeed = TEST_SPEED_75_DPM; 
			Reply = LSReadTimeSampleMICR(Connessione,(HWND)hDlg, TypeSpeed, &CurrValue, 2);
			if( Reply  == LS_OKAY)
			{
				TempoCamp75 = CurrValue;
			}
			Reply = LSReadTrimmerMICR(Connessione,(HWND)hDlg, TypeSpeed, buffDati, NR_BYTES_TRIMMER);
			if( Reply  == LS_OKAY)
			{
				TrimmerValue75 = buffDati[0];
			}
		}
		if( TypeLS == TYPE_LS150_COLOR)
		{

			TypeSpeed = TEST_SPEED_300_COLOR; 
			Reply = LSReadTimeSampleMICR(Connessione,(HWND)hDlg, TypeSpeed, &CurrValue, 2);
			if( Reply  == LS_OKAY)
			{
				TempoCamp300C = CurrValue;
			}
			Reply = LSReadTrimmerMICR(Connessione,(HWND)hDlg, TypeSpeed, buffDati, NR_BYTES_TRIMMER);
			if( Reply  == LS_OKAY)
			{
				TrimmerValue300C = buffDati[0];
			}
		}
		else
		{

			//controllare perche' si blocca

			//TypeSpeed = TEST_SPEED_300_DPI_UV; 
			//Reply = LSReadTimeSampleMICR(Connessione,(HWND)hDlg, TypeSpeed, &CurrValue, 2);
			//
			//if (CurrValue != 0 ) //se non e' UV risponde 0...
			//{
			//	if( Reply  == LS_OKAY)
			//		TempoCamp300UV = CurrValue;

			//	Reply = LSReadTrimmerMICR(Connessione,(HWND)hDlg, TypeSpeed, buffDati, NR_BYTES_TRIMMER);
			//	if( Reply  == LS_OKAY)
			//		TrimmerValue300UV = buffDati[0];
			//}
		
		}

		
		// trimmer	

		Reply = LSImageCalibration(Connessione,(HWND)hDlg, FALSE, &VelVal);
		TempoImmagine = VelVal;


		if (TypeLS == TYPE_LS150_COLOR)
		{
			 aaa = System::Convert::ToDouble(SVersion);
			if( (aaa >= 213) )
			{
				fDatiScannerColore = true;
				Reply = LSReadScannerData(Connessione,SIDE_FRONT_IMAGE,DATA_SCANNER_GRAY,0,4,(unsigned char *)(&nrBytes));
				if ( Reply == LS_OKAY)
				{
					Reply = LSReadScannerData(Connessione,SIDE_FRONT_IMAGE,DATA_SCANNER_GRAY,1,nrBytes,(unsigned char *)(buffscanner));
					if ( Reply == LS_OKAY)
					{
						for(int iii = 0;iii <nrBytes;iii++)
						{
							BufferDumpGray[iii] = buffscanner[iii];
						}
					}
				}

				Reply = LSReadScannerData(Connessione,SIDE_FRONT_IMAGE,DATA_SCANNER_RED,0,4,(unsigned char *)(&nrBytes));
				if ( Reply == LS_OKAY)
				{
					Reply = LSReadScannerData(Connessione,SIDE_FRONT_IMAGE,DATA_SCANNER_RED,1,nrBytes,(unsigned char *)(buffscanner));
					if ( Reply == LS_OKAY)
					{
						for(int iii = 0;iii <nrBytes;iii++)
						{
							BufferDumpRed[iii] = buffscanner[iii];
						}
					}
				}

				Reply = LSReadScannerData(Connessione,SIDE_FRONT_IMAGE,DATA_SCANNER_GREEN,0,4,(unsigned char *)(&nrBytes));
				if ( Reply == LS_OKAY)
				{
					Reply = LSReadScannerData(Connessione,SIDE_FRONT_IMAGE,DATA_SCANNER_GREEN,1,nrBytes,(unsigned char *)(buffscanner));
					if ( Reply == LS_OKAY)
					{
						for(int iii = 0;iii <nrBytes;iii++)
						{
							BufferDumpGreen[iii] = buffscanner[iii];
						}
					}
				}

				Reply = LSReadScannerData(Connessione,SIDE_FRONT_IMAGE,DATA_SCANNER_BLUE,0,4,(unsigned char *)(&nrBytes));
				if ( Reply == LS_OKAY)
				{
					Reply = LSReadScannerData(Connessione,SIDE_FRONT_IMAGE,DATA_SCANNER_BLUE,1,nrBytes,(unsigned char *)(buffscanner));
					if ( Reply == LS_OKAY)
					{
						for(int iii = 0;iii <nrBytes;iii++)
						{
							BufferDumpBlue[iii] = buffscanner[iii];
						}
					}
				}

				/* SCANNER RETRO */
				Reply = LSReadScannerData(Connessione,SIDE_BACK_IMAGE,DATA_SCANNER_GRAY,0,4,(unsigned char *)(&nrBytes));
				if ( Reply == LS_OKAY)
				{
					Reply = LSReadScannerData(Connessione,SIDE_BACK_IMAGE,DATA_SCANNER_GRAY,1,nrBytes,(unsigned char *)(buffscanner));
					if ( Reply == LS_OKAY)
					{
						for(int iii = 0;iii <nrBytes;iii++)
						{
							BufferDumpGrayR[iii] = buffscanner[iii];
						}
					}
				}

				Reply = LSReadScannerData(Connessione,SIDE_BACK_IMAGE,DATA_SCANNER_RED,0,4,(unsigned char *)(&nrBytes));
				if ( Reply == LS_OKAY)
				{
					Reply = LSReadScannerData(Connessione,SIDE_BACK_IMAGE,DATA_SCANNER_RED,1,nrBytes,(unsigned char *)(buffscanner));
					if ( Reply == LS_OKAY)
					{
						for(int iii = 0;iii <nrBytes;iii++)
						{
							BufferDumpRedR[iii] = buffscanner[iii];
						}
					}
				}

				Reply = LSReadScannerData(Connessione,SIDE_BACK_IMAGE,DATA_SCANNER_GREEN,0,4,(unsigned char *)(&nrBytes));
				if ( Reply == LS_OKAY)
				{
					Reply = LSReadScannerData(Connessione,SIDE_BACK_IMAGE,DATA_SCANNER_GREEN,1,nrBytes,(unsigned char *)(buffscanner));
					if ( Reply == LS_OKAY)
					{
						for(int iii = 0;iii <nrBytes;iii++)
						{
							BufferDumpGreenR[iii] = buffscanner[iii];
						}
					}
				}

				Reply = LSReadScannerData(Connessione,SIDE_BACK_IMAGE,DATA_SCANNER_BLUE,0,4,(unsigned char *)(&nrBytes));
				if ( Reply == LS_OKAY)
				{
					Reply = LSReadScannerData(Connessione,SIDE_BACK_IMAGE,DATA_SCANNER_BLUE,1,nrBytes,(unsigned char *)(buffscanner));
					if ( Reply == LS_OKAY)
					{
						for(int iii = 0;iii <nrBytes;iii++)
						{
							BufferDumpBlueR[iii] = buffscanner[iii];
						}
					}
				}

			}// se la versione � supportata chiedo i dati

		}// fine scanner a colori


		if ((TypeLS == TYPE_LS150_COLOR) &&( aaa  > 213))
		{
			// salvo i valori degli AD
			Reply = LSGetPWMValues(Connessione,SIDE_FRONT_IMAGE,0,4,(unsigned char *)(&nrBytes));
			if( Reply == LS_OKAY)
			{
				Reply = LSGetPWMValues(Connessione,SIDE_FRONT_IMAGE,1,nrBytes,(unsigned char *)(buffscanner));
				if( Reply == LS_OKAY)
				{
					for(int iii = 0;iii <nrBytes;iii++)
					{
						BufferADValues[iii] = buffscanner[iii];
					}
				}
			}



			Reply = LSGetPWMValues(Connessione,SIDE_BACK_IMAGE,0,4,(unsigned char *)(&nrBytes));
			if( Reply == LS_OKAY)
			{
				Reply = LSGetPWMValues(Connessione,SIDE_BACK_IMAGE,1,nrBytes,(unsigned char *)(buffscanner));
				if( Reply == LS_OKAY)
				{
					for(int iii = 0;iii <nrBytes;iii++)
					{
						BufferADValuesR[iii] = buffscanner[iii];
					}
				}
			}
		}

	}
	LSDisconnect(Connessione,(HWND)hDlg);
	return Reply;
}

int Ls150Class::Periferica::PuliziaCinghia()
{
	//unsigned char pDati[50*1024];
	long lunDati= 50*1024;
	//int ii, max, min;
	//float media;
	//int somma;
	int MinScarto,MaxScarto;
	short Connessione;
	
	MinScarto = 110;
	MaxScarto = 145;
	ReturnC->FunzioneChiamante = " Pulizia Cinghia";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	//Reply = LS150_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( Reply == LS_OKAY )
	{
		//----------- Faccio partire la cinghia -----------
		LSReset(Connessione,(HWND)0, 0x32);	
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}
	ReturnC->ReturnCode = Reply;
	
	LSDisconnect(Connessione,(HWND)hDlg);
	return Reply;
}



int Ls150Class::Periferica::TestCinghia()
{
	unsigned char pDati[50*1024];
	long lunDati= 50*1024;
	int ii, max, min;
	float media;
	int somma;
	int MinScarto,MaxScarto;
	short Connessione;
	
	MinScarto = 110;
	MaxScarto = 145;
	ReturnC->FunzioneChiamante = " Test cinghia";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	//Reply = LS150_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( Reply == LS_OKAY )
	{
		
		//Chiediamo a D. Loggia di implementare nel software di collaudo (asap) una funzione che prima del test belt forzi a 128 il valore del trimmer sulla piastra a velocit� di 300dpi (velocit� del test)
		Reply = LSSetTrimmerMICR(Connessione, (HWND)hDlg,  TEST_SPEED_300_DPI, 128);
		if(Reply == LS_OKAY)
		{
			//----------- Disabilito la doppia sfogliatura -----------
			Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,150,215);	
			Reply = LSTestCinghia(Connessione,(HWND)hDlg,pDati,&lunDati);
			if(Reply == LS_OKAY)
			{
				max = 0;
				min = 255;
				media = 0;
				somma = 0;
				if (lunDati <= 400)
				{
					ReturnC->FunzioneChiamante = "LSTestCinghia";
					Reply = -7000;
				}
				else
				{
					//  ii = 10;    // Salto i primi 10
					 // Non controllo gli ultimi 400
					for(ii = 10; ii < lunDati-400;ii++)
					{	
						
						if (pDati[ii] < min )
							min = pDati[ii]; 
						if (pDati[ii] > max )
							max = pDati[ii];
						somma += pDati[ii];
					}
					media  = (float)(somma / (lunDati-410));	
					if((min < MinScarto) || (max > MaxScarto))
					{
						ReturnC->FunzioneChiamante = "LSTestCinghia";
						Reply = -6000;
						
					}
					else
					{

					}
					ValMinCinghia = min;
					ValMaxCinghia = max;
				}
			}
			else
			{
				ReturnC->FunzioneChiamante = "LSTestCinghia";
			}
		}
		else
		{
			ReturnC->FunzioneChiamante = "LSSetTrimmerMICR";
		}

	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}
	ReturnC->ReturnCode = Reply;
	
	LSDisconnect(Connessione,(HWND)hDlg);
	return Reply;
}






int Ls150Class::Periferica::TestCinghiaEx(int Thn , int Thp ,int DocSignalValue)
{
	short Connessione;
	struct _TESTBELTPARAM TestBeltParam;

	ReturnC->FunzioneChiamante = "TestCinghiaEx";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	if( Reply == LS_OKAY )
	{
		
		//Imposto il Trimmer a 40
		Reply = LSSetTrimmerMICR(Connessione, (HWND)hDlg,  TEST_SPEED_300_DPI, 40);
		Reply = LSSetTrimmerMICR(Connessione, (HWND)hDlg,  TEST_SPEED_200_DPI, 40);
		
		if(Reply == LS_OKAY)
		{
			//----------- Disabilito la doppia sfogliatura -----------
			Reply = LSConfigDoubleLeafingEx(Connessione ,(HWND)hDlg ,  (short)DOUBLE_LEAFING_WARNING , DOUBLE_LEAFING_DISABLE, 0 ) ; 

			TestBeltParam.NegativeThreshold = Thn ; 
			TestBeltParam.PositiveThreshold = Thp ;
			TestBeltParam.DocSignalValue = DocSignalValue ;
			

			Reply = LSTestCinghiaEx(Connessione,(HWND)hDlg, &TestBeltParam);
			if(Reply == LS_OKAY)
			{
				if (TestBeltParam.TestResult == 1 )
				{
					Reply = LS_OKAY ; 
								//lo risetto...
			//Chiediamo a D. Loggia di implementare nel software di collaudo (asap) una funzione che prima del test belt forzi a 128 il valore del trimmer sulla piastra a velocit� di 300dpi (velocit� del test)
			LSSetTrimmerMICR(Connessione, (HWND)hDlg,  TEST_SPEED_300_DPI, 128);
			Reply = LSSetTrimmerMICR(Connessione, (HWND)hDlg,  TEST_SPEED_200_DPI, 128);
				}
				else
				{
					Reply = -6000; 
					TestBeltParam.SignalValueRead = 0 ;
				}
			}
			else
			{
				LSReset(Connessione,(HWND)0,RESET_ERROR);
				ReturnC->FunzioneChiamante = "LSTestCinghiaEx";
			}

			ValMinCinghiaNew = TestBeltParam.MinNegativeValue ;
		    ValMaxCinghiaNew = TestBeltParam.MaxPositiveValue;
			SignalValueRead = TestBeltParam.SignalValueRead ;
			NrPicchi = 	TestBeltParam.NrPicchi ; 


		}
		else
		{
			ReturnC->FunzioneChiamante = "LSSetTrimmerMICR";
		}

	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}
	ReturnC->ReturnCode = Reply;
	
	LSDisconnect(Connessione,(HWND)hDlg);
	return Reply;
}




int Ls150Class::Periferica::LeggiValoriCIS()
{
	//char pColonne[5000];
	//short Connessione;
	//AD_LM98714 sScanner ;//= new AD_LM98714;

	//Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	//
	////Reply = LS150_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	//if( Reply == LS_OKAY )
	//{
	//	Reply = LS150_ScannerReadValue((HWND)hDlg, pColonne, &sScanner, 0);	
	//	strCis->Add_PWM_Blue_F = sScanner.Add_PWM_Blue_F;
	//	strCis->Add_PWM_Blue_R = sScanner.Add_PWM_Blue_R;
	//	strCis->Add_PWM_Green_F = sScanner.Add_PWM_Green_F;
	//	strCis->Add_PWM_Green_R = sScanner.Add_PWM_Green_R;
	//	strCis->Add_PWM_Red_F = sScanner.Add_PWM_Red_F;
	//	strCis->Add_PWM_Red_R = sScanner.Add_PWM_Red_R;
	//	
	//	
	//	strCis->Dato_Gain_F = sScanner.Dato_Gain_F;
	//	strCis->Dato_Gain_R = sScanner.Dato_Gain_R;
	//	strCis->Dato_Offset_F = sScanner.Dato_Offset_F;
	//	strCis->Dato_Offset_R = sScanner.Dato_Offset_R;
	//	strCis->Dato_PWM_Blue_F = sScanner.Dato_PWM_Blue_F;
	//	strCis->Dato_PWM_Blue_R = sScanner.Dato_PWM_Blue_R;
	//	strCis->Dato_PWM_Green_F = sScanner.Dato_PWM_Green_F;
	//	strCis->Dato_PWM_Green_R = sScanner.Dato_PWM_Green_R;
	//	strCis->Dato_PWM_Red_F = sScanner.Dato_PWM_Red_F;
	//	strCis->Dato_PWM_Red_R = sScanner.Dato_PWM_Red_R;
	//	


	//}
	//ReturnC->ReturnCode = Reply;
	//
	//LSDisconnect(Connessione,(HWND)hDlg);
	return 0;
}





int Ls150Class::Periferica::SetDiagnostic(int fbool)
{
	short Connessione;
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	//Reply = LS150_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( Reply == LS_OKAY )
	{
		//
		
		Reply = LSSetDiagnosticMode(Connessione,(HWND)hDlg,fbool);
	}
	LSDisconnect(Connessione,(HWND)hDlg);
	return Reply;
}


int Ls150Class::Periferica::SetElettromagnete(int Elettromagnete)
{
		short Connessione;

	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	//Reply = LS150_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( Reply == LS_OKAY )
	{
		/*
		#define LS150_ELECTROMAGNET_POCKET		1
		#define LS150_ELECTROMAGNET_LEAFER		2
		#define LS150_ELECTROMAGNET_INKJET		3
		#define LS150_ELECTROMAGNET_STAMP		4
		*/

		Reply = LSTestElectromagnet(Connessione,0, Elettromagnete);
	}
	LSDisconnect(Connessione,(HWND)hDlg);
	return Reply;
}



int Ls150Class::Periferica::SetVelocita(int fSpeed)
{

	short Connessione;
	
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	//Reply = LS150_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( Reply == LS_OKAY )
	{
		//
		Sleep(500);
		Reply = LSSetUnitSpeed(Connessione,(HWND)hDlg,fSpeed);
	}
	LSDisconnect(Connessione,(HWND)hDlg);
	return Reply;
}


int Ls150Class::Periferica::TestDownload()
{
	//short ret;
	short Connessione=0;
	char *StrNome = NULL ; 

	ReturnC->FunzioneChiamante = "TestDownload";
	
	//-----------Open--------------------------------------------
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione);
	
	if (Reply == LS_OKAY )
	{
		StrNome = (char*) Marshal::StringToHGlobalAnsi(FileNameDownload ).ToPointer();
		Reply =  LSDownloadFirmware(Connessione , 
									   (HWND)hDlg,
									   StrNome , //char		*FileFw,
									   NULL ) ; //int		(__stdcall *userfunc1)(char *Item));
		
		ReturnC->FunzioneChiamante =  "LSDownloadFirmware";
	}
	else
	{
		ReturnC->FunzioneChiamante =  "TestDownload";
	}
	

	//-----------Close-------------------------------------------
	LSDisconnect(Connessione,(HWND)hDlg);
	ReturnC->ReturnCode = Reply;
	
	return Reply;


}

int Ls150Class::Periferica::TestControlloFascioniParamter(int ^%RNrDocForCalib,int ^%RNrDocVerify )
{
	char DocuTest[35];
	
	int NrDocForCalib ; 
	int NrDocVerify ; 
	Reply = CHKGetDocToProcess(DocuTest,sizeof(DocuTest),&NrDocForCalib,&NrDocVerify) ; 
	
	*RNrDocForCalib = NrDocForCalib ;
	*RNrDocVerify = NrDocVerify ;
	
	DocutestFascioni= Marshal::PtrToStringAnsi((IntPtr) (char *)DocuTest);

	return 0 ;
	
				
}


int Ls150Class::Periferica::TestControlloFascioni(int fControlloBackground , int fZebrato , int fTestFascioni)
{

	HANDLE HBackGray=0;
	HANDLE HFrontGray=0;
	short Connessione;
	char *StrNome= NULL ; 
	FASCIONI_RES pRes;
	int 	rc;
	short	row;
	char	sMsg[128];
	/*FILE *fh ;*/

	rc = 0 ;

	
							/*if( fh = fopen("Trace_Fascioni.txt", "at") )
							{
								fprintf(fh, "fControlloBackground = %d fZebrato = %d fTestFascioni = %d ", fControlloBackground,fZebrato,fTestFascioni);
								fclose( fh );
							}*/

	

	ReturnC->FunzioneChiamante = "TestControlloFascioni";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	if( Reply == LS_OKAY )
	{
		
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);
		ReturnC->FunzioneChiamante = "TestControlloFascioni1";
	
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								SIDE_ALL_IMAGE,
								SCAN_MODE_256GR300 , //obbligatorio
								AUTOFEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);
		ReturnC->FunzioneChiamante = "TestControlloFascioni2";

		if( Reply == LS_OKAY )
		{
	
		 Reply = LSReadImage(Connessione, (HWND)hDlg,
										NO_CLEAR_BLACK,
										SIDE_ALL_IMAGE,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);

		ReturnC->FunzioneChiamante = "TestControlloFascioni3";
				if(Reply == LS_OKAY)
				{
					
					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro).ToPointer();
					Reply = LSSaveDIB((HWND)hDlg,HBackGray,StrNome);
					ReturnC->FunzioneChiamante = "TestControlloFascioni5";
						/*if( fh = fopen("Trace_Fascioni.txt", "at") )
							{
								fprintf(fh, "Reply3 = %d ", Reply);
								fclose( fh );
							}*/

				
					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine).ToPointer();
					Reply = LSSaveDIB((HWND)hDlg,  HFrontGray,StrNome);
						/*if( fh = fopen("Trace_Fascioni.txt", "at") )
							{
								fprintf(fh, "Reply4= %d ", Reply);
								fclose( fh );
							}*/

					ReturnC->FunzioneChiamante = "TestControlloFascioni4";
					
					if (fControlloBackground == 1 ) 
					{
						if ( fZebrato == 1  )
						{
							rc = CheckBackground((HWND)hDlg, HBackGray, &row);
							switch( rc )
							{
								case 1:
									sprintf(sMsg, "TestBackground KO -  REAR background row = %d NOT adequate !\n\nOne or more row up 64 level !!!", row);
									rc  = LS_BACK_BACKGROUND; 
									break;
								case 2:
									sprintf(sMsg, "TestBackground KO -  REAR background row = %d NOT adequate !\n\nMore then 10 row contiguous over the level of 32 !!!", row);
									rc  = LS_BACK_BACKGROUND_1; 
									break;
								case 3:
									sprintf(sMsg, "TestBackground KO -  REAR background row = %d NOT adequate !\n\nMore then 80 row over the level of 32 !!!", row);
									rc  = LS_BACK_BACKGROUND_2 ; 
									break;
								}
							}
						else
						{
							/*if( fh = fopen("Trace_Fascioni.txt", "at") )
							{
								fprintf(fh, "CheckBackground1= %d ", rc);
								fclose( fh );
							}*/
							rc = CheckBackground((HWND)hDlg, HFrontGray, &row);

							/*if( fh = fopen("Trace_Fascioni.txt", "at") )
							{
								fprintf(fh, "CheckBackground2= %d ", rc);
								fclose( fh );
							}*/
							if( rc == 0  )
							{
								rc = CheckBackground((HWND)hDlg, HBackGray, &row);
								if( rc )
								{
									switch( rc )
									{
									case 1:
										sprintf(sMsg, "TestBackground KO - REAR background row = %d NOT adequate !\n\nOne or more row up 64 level !!!", row);
										rc  = LS_BACK_BACKGROUND; 
										break;
									case 2:
										sprintf(sMsg, "TestBackground KO -  REAR background row = %d NOT adequate !\n\nMore then 10 row contiguous over the level of 32 !!!", row);
										rc  = LS_BACK_BACKGROUND_1; 
										break;
									case 3:
										sprintf(sMsg, "TestBackground KO -  REAR background row = %d NOT adequate !\n\nMore then 80 row over the level of 32 !!!", row);
										rc  = LS_BACK_BACKGROUND_2 ; 
										break;
									}
								}
								else
									rc = 0 ; 
							}
							else
							{
								switch( rc )
								{
								case 1:
									sprintf(sMsg, "TestBackground KO -  FRONT background row = %d NOT adequate !\n\nOne or more row up 64 level !!!", row);
									rc  = LS_FRONT_BACKGROUND; 
									break;
								case 2:
									sprintf(sMsg, "TestBackground KO -  FRONT background row = %d NOT adequate !\n\nMore then 8 row contiguous over the level of 32 !!!", row);
									rc  = LS_FRONT_BACKGROUND_1; 
									break;
								case 3:
									sprintf(sMsg, "TestBackground KO -  FRONT background row = %d NOT adequate !\n\nMore then 80 row over the level of 32 !!!", row);
									rc = LS_FRONT_BACKGROUND_2; 
									break;
								}
							}
						}

					}

					if (rc != 0 )
						Reply = rc ; 

					if(Reply == LS_OKAY)
					{
						if (fTestFascioni == 1 )
						{
								//StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileFascioni).ToPointer();
								//Reply = LSSaveDIB((HWND)hDlg,HFrontGray,StrNome);
								//if( fh = fopen("Trace_Fascioni.txt", "at") )
							//{
								//fprintf(fh, "Reply1 = %d ", Reply);
								//fclose( fh );
							//}

						/*	if( fh = fopen("Trace_Fascioni.txt", "at") )
							{
								fprintf(fh, "PrimaFascioni", Reply);
								fclose( fh );
							}	*/

							Reply = CHKControlloFascioni(StrNome,&pRes) ; 

							/*if( fh = fopen("Trace_Fascioni.txt", "at") )
							{
								fprintf(fh, "DopoFascioni = %d ", Reply);
								fclose( fh );
							}*/

							// Always free trhe unmanaged string.
							//	Marshal::FreeHGlobal(IntPtr(StrNome));
							

							if(Reply == LS_OKAY)
								Reply = LS_OKAY ;
							else
								Reply = LS_CCI_TEST_FAILED; 
						}
						else
							Reply = LS_OKAY ;

					}
					
						
				}
				

			
			LSDisconnect(Connessione,(HWND)hDlg);
		}
		else
		{
			ReturnC->FunzioneChiamante = "TestControlloFascioni-LSDocHandle";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "TestControlloFascioni-LSOpen";
	}
	
	// libero le immagini
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	ReturnC->ReturnCode = Reply;
	
	return Reply;
}


int Ls150Class::Periferica::TestNormalization(short ScanMode, BOOL fCertificazione, unsigned short levelGray, unsigned short levelRD, unsigned short levelGR, unsigned short levelBL,int lotto,int FaiOpen,int FaiClose)
{
	HANDLE HBackGray=0;
	HANDLE HFrontGray=0;
	//HANDLE HFrontUV=0;
	
	char *StrNome=NULL;
	short floop = false;
	//char BarcodeRead[100];
	//int len_barcode;

	ReturnC->FunzioneChiamante = "TestNormalization";
	
	if (FaiOpen == 1 ) 
		Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,&ConnessioneGlobal);
	else
		Reply = LS_OKAY ; 

	if (Reply == LS_OKAY )
	{
	
		Reply  = LSConfigDoubleLeafingAndDocLength(ConnessioneGlobal,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);

		//Reply = LSModifyPWMUltraViolet(Connessione , (HWND)hDlg  , 100 , TRUE , 0 ) ; 

		do
		{
		
			Reply = LSDocHandle(ConnessioneGlobal,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								SIDE_ALL_IMAGE,
								ScanMode , //SCAN_MODE_256GR300/SCAN_MODE_COLOR_300 , //obbligatorio
								AUTOFEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);

			if (Reply == LS_FEEDER_EMPTY)
			{
				floop = TRUE;
			}
			else
				floop = FALSE;
		}while(floop);

		if( Reply == LS_OKAY )
		{
			if (fCertificazione == TRUE ) 
			{

			// Leggo l'immagine filmata
				 Reply = LSReadImage(ConnessioneGlobal, (HWND)hDlg,
										NO_CLEAR_BLACK,
										SIDE_ALL_IMAGE,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);
			}
			else
			{
				 Reply = LSReadImage(ConnessioneGlobal, (HWND)hDlg,
										NO_CLEAR_BLACK, //CLEAR_AND_ALIGN_IMAGE,
										SIDE_ALL_IMAGE,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);
			}

			if(Reply != LS_OKAY ) 
				ReturnC->FunzioneChiamante = "LSReadImage";


#if 0 
			if(Reply == LS_OKAY && ScanMode == SCAN_MODE_256GR300)
			{

				HANDLE	ConvertImage;
				Reply = LSConvertImageResolution((HWND)hDlg,HFrontGray,200,&ConvertImage);


				memset(BarcodeRead,0,sizeof(BarcodeRead));
				len_barcode = sizeof(BarcodeRead);	
				//cerco il Barcode con la scritta NOME_DOCUTEST...
				len_barcode = sizeof(BarcodeRead);
		
					//LSSaveDIB((HWND)hDlg,HFrontGray,"C:\\Arca\\PrimaRuotata.bmp");

					HANDLE	phImage;
					// Giro l'immagine di 90 gradi e riprovo prima di dare errore
					Reply  = LSRotateImageEx((HWND)hDlg, ConvertImage, -9000, &phImage);
						
					if (Reply == LS_OKAY)
					{
						HANDLE	rhImage; 

						LSSaveDIB((HWND)hDlg,phImage,"C:\\Arca\\DopRuotata.bmp");

						
						//Reply = LSCutImage((HWND)hDlg,phImage,UNIT_MM,30,265,150,50,&rhImage);
						//Reply = LSCutImage((HWND)hDlg,phImage,UNIT_MM,2,260,75,50,&rhImage);
						Reply = LSCutImage((HWND)hDlg,phImage,UNIT_MM,2,190,75,18,&rhImage);
						////LSCutImage(HWND		hWnd HANDLE	hImage,
						//	   short	Unit,
						//	   float	pos_x,
						//	   float	pos_y,
						//	   float	sizeW,
						//	   float	sizeH,
						//	   HANDLE	*pImage);
							   
						
						LSSaveDIB((HWND)hDlg,rhImage,"C:\\Arca\\Ritagliata.bmp");
	
						Reply = LSReadBarcodeFromBitmap((HWND)hDlg,
												rhImage , //phImage,
												READ_BARCODE_CODE128,
												//READ_BARCODE_CODE39,
												0 , 
												0 , 
												0 , 
												0 , 
												BarcodeRead,
												(UINT *)&len_barcode);

						// Rilascio l'immagine rotata
						LSFreeImage((HWND)hDlg, &phImage);

						// Rilascio l'immagine ritagliata
						LSFreeImage((HWND)hDlg, &rhImage);

						// Rilascio l'immagine ritagliata
						LSFreeImage((HWND)hDlg, &ConvertImage);
					}
			}
			else
			{
				if (ScanMode == SCAN_MODE_COLOR_300  ) //ho gia' decodificato il barcode nel giro a grigio...
					strcpy(BarcodeRead,"309") ; 
			}

#endif
				if (Reply == LS_OKAY )
				{
					

					//if (strncmp(BarcodeRead,"309",3) == 0) 
					//{
						if (fCertificazione == TRUE ) 
							// Setto i valori del documento campione
							LSSetDocutest309Values(levelGray, levelRD, levelGR, levelBL);

						Reply = LSCISCompensationEx(ConnessioneGlobal, (HWND)hDlg, LS150_RECOUP_FRONT, HFrontGray, NULL, lotto);
						if( Reply == LS_OKAY )
							Reply = LSCISCompensationEx(ConnessioneGlobal, (HWND)hDlg, LS150_RECOUP_REAR , HBackGray, NULL, lotto);
					//}
					//else
					//	 Reply = LS_DOCUTEST_NOT_CORRECT ; 
				}
				
			
			if (FaiClose == 1 ) 
				LSDisconnect(ConnessioneGlobal,(HWND)hDlg);
		}
		else
		{
			ReturnC->FunzioneChiamante = "LSDocHandle";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSConnect";
	}

	// libero le immagini
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	//if (HFrontUV)
	//	LSFreeImage((HWND)hDlg, &HFrontUV);

	
	ReturnC->ReturnCode = Reply;
	
	return Reply;
}






int Ls150Class::Periferica::CheckReply(HWND hDlg,int Rep,char *s)
{
	return TRUE;
}



int Ls150Class::Periferica::EstraiDatiSpeedE13B(unsigned char *pd, long llDati,
						 short *NrPicchi, float *SommaValori, long *ValMin, short *NrPicchiMin)
{
	BOOL ret;
	short ii, Save_ii;
	short NrCampioni;
	short NrChar;
	unsigned char *pdct;

//	FILE *fh;


	// Conto i bytes larghezza char
		// Conto i bytes larghezza char
	if( TypeLS == TYPE_LS150_VE )
	{
		NrCampioni = 0;
		while( *(pd + NrCampioni) ) //!= E13B_FINE_LLC )
			NrCampioni ++;

		pdct = pd;
		NrChar = (short)(llDati - 1000);		// Setto NrChar diverso da llDati per fare l'if
	}
	else
	{
		NrChar = 0;
		while( (*(pd + NrChar) != E13B_FINE_LLC) && (NrChar < llDati) )
			NrChar ++;
	
		NrCampioni = *(short *)(pd + NrChar + 1);

		pdct = (unsigned char *)(pd + NrChar + 1 + 2);		// Vado ad inizio dati segnale
		pdct = (unsigned char *)(((unsigned char *)pdct) + (DIM_DESCR_CHAR * NrChar));	// Vado ad inizio dati segnale

		// Tolgo i descrittori dal numero bytes
		NrCampioni -= (DIM_DESCR_CHAR * NrChar);
	}

	if( NrChar != llDati )
	{
		// Cerco i picchi e calcolo il valore medio
		*NrPicchi = *NrPicchiMin = 0;
		*ValMin = 0xff;		// setto il massimo
		*SommaValori = 0;
		Save_ii = 0;
		// Elimino una parte iniziale e finale
		for( ii = 20; ii <= (NrCampioni - 20); ii ++)
		{
			// Controllo se � un picco,
			// per essere un picco deve avere 7 valori prima e dopo di valore inferiore
			if( (pdct[ii] <= VALORE_MAX_PICCO_MIN) &&
//			if( (pdct[ii]) && (pdct[ii] <= VALORE_MAX_PICCO_MIN) &&
				((pdct[ii] <= pdct[ii-1]) && (pdct[ii] < pdct[ii+1])) &&
				((pdct[ii] <= pdct[ii-2]) && (pdct[ii] < pdct[ii+2])) &&
				((pdct[ii] <= pdct[ii-3]) && (pdct[ii] < pdct[ii+3])) &&
				((pdct[ii] <= pdct[ii-4]) && (pdct[ii] < pdct[ii+4])) ) //&&
				/*((pdct[ii] <= pdct[ii-5]) && (pdct[ii] < pdct[ii+5])) &&
				((pdct[ii] <= pdct[ii-6]) && (pdct[ii] < pdct[ii+6])) &&
				((pdct[ii] <= pdct[ii-7]) && (pdct[ii] < pdct[ii+7])) )*/
			{
				// Controllo se il picco � ad una distanza minima
				if( (ii - Save_ii) >= PICCO_DISTANZA_MINIMA )
				{
					// Media del picco
					*SommaValori += pdct[ii];

//					if( fh = fopen("Trace_Calibrazione_MICR.txt", "at") )
//					{
//						fprintf(fh, "%d ", pdct[ii]);
//						fclose( fh );
//					}

					// Salvo il valore minimo
					if( *ValMin == pdct[ii] )
						*NrPicchiMin = *NrPicchiMin + 1;

					if( *ValMin > pdct[ii] )
					{
						*ValMin = pdct[ii];
						*NrPicchiMin = 1;
					}

					*NrPicchi = *NrPicchi + 1;
					Save_ii = ii;
				}
			}
		}

		// Calcolo media
//		*llMedia = *llMedia / *NrPicchi;

		ret = TRUE;
	}
	else
	{
		// C'� un baco che qualche volta non ritorna i dati corretti,
		// setto tutto a zero e butto l'acquisizione !!!
		*NrPicchi = *NrPicchiMin = 0;
		*ValMin = 0;
		*SommaValori = 0;
		ret = FALSE;
	}


//	if( fh = fopen("Trace_Calibrazione_MICR.txt", "at") )
//	{
//		fprintf(fh, "\n ");
//		fclose( fh );
//	}

	return ret;
}  // EstraiDatiSpeedE13B



short Ls150Class::Periferica::GetTypeLS(char *LsName,char *Version, char *Ls150_Config)
{

		int TypeLS;

	TypeLS = TYPE_LS150;
	//Identifico la periferica LS150 USB
	if( strncmp(LsName, MODEL_LS150_VE, strlen(MODEL_LS150_VE)) == 0 )
	{
		TypeLS = TYPE_LS150_VE;
	}

	else if( strncmp(LsName, MODEL_LS150, strlen(MODEL_LS150)) == 0 )
	{
		if( Ls150_Config[1] & MASK_SCANNER_UV )
		{
			if( Version[0] == '1' )
				TypeLS = TYPE_LS150_UV_OLD;
			else
				TypeLS = TYPE_LS150;
		}

		else if( Ls150_Config[1] & MASK_SCANNER_COLOR )
			TypeLS = TYPE_LS150_COLOR;

		else if( Version[0] == '1' )
		{
			if( atof(Version) < 1.25 )
				TypeLS = TYPE_LS150_DL_OLD;
		}
	}
	return TypeLS;


	
} // GetTypeLS






int Ls150Class::Periferica::CollaudoLSConnect(HWND hDlg,HANDLE hInst,short TipoPeriferica,short *Connessione)
{
	int Reply;



	do
	{
		Reply = LSConnectDiagnostica((HWND)0,(HANDLE)hInst,TipoPeriferica,Connessione);

		// Siccome MANCANO un sacco di connect, in caso di LS_ALREADY_OPEN
		// Mi disconnetto e mi riconnetto fino a reply == LS_OKAY
		// per risolvere problemi di applicativo collaudo !!!
		if( Reply == LS_ALREADY_OPEN )
		{
			int rc;
			rc = LSDisconnect(*Connessione, (HWND)0);	// Per la connect di adesso
			rc = LSDisconnect(*Connessione, (HWND)0);	// Per la connect che c'era !!
		}

	} while( Reply == LS_ALREADY_OPEN );


	// Connect alla lsapi
//	Reply = LSConnectDiagnostica((HWND)0,(HANDLE)hInst,TipoPeriferica,Connessione);
	if(( Reply == LS_OKAY )||( Reply == LS_TRY_TO_RESET )||( Reply == LS_ALREADY_OPEN ))
	//if( Reply == LS_TRY_TO_RESET)
	{
		Reply = LSReset(*Connessione,(HWND)0,RESET_ERROR);

		if( Reply != LS_OKAY )
			Reply = LSReset(*Connessione,(HWND)0,RESET_FREE_PATH);
	}
	//else if (Reply == LS_ALREADY_OPEN ) 
	//	Reply = LS_OKAY ;


	return Reply;
}



void Ls150Class::Periferica::ShowMICRSignal(float Percentile, short Trimmer)
{

	int conta ;
	list->Items->Add("Valore Letto: " + Percentile + "  :" + "Trimmer Value: "+ Trimmer );
	
	conta = list->Items->Count;
	list->SelectedIndex = conta-1;
	list->Refresh();
}



double Ls150Class::Periferica::CalcolaIstogramma(HANDLE pBitmapGr, array<float>^vValue)
{
	long xx, yy;
	long ii, row, col;
	long SizeBitmap, diff;
	unsigned char *pData;
	double SommaX2;
	double N = 256;
	double Sigma = 0;
	double somma = 0;


	row = ((BITMAPINFO *)pBitmapGr)->bmiHeader.biHeight;
	col = ((BITMAPINFO *)pBitmapGr)->bmiHeader.biWidth;

	// Arrotondo alla legge del multiplo di 4 bytes
	if( diff = (col % 4) )
		col += 4 - diff;

	SizeBitmap = col * row;

	// Compilo istogramma del valore dei pixel
	for( ii = 0; ii < 256; ii ++)
		vValue[ii] = 0;
	pData = (unsigned char *)pBitmapGr + 1064;
	for( yy = 0; yy < row; yy ++ )
	{
		for( xx = 0; xx < ((BITMAPINFO *)pBitmapGr)->bmiHeader.biWidth; xx ++ )
		{
			vValue[*(pData + xx)] ++;
		}
		pData += col;
	}
	SommaX2 = 0;
	for( yy = 0; yy < N; yy ++ )
	{
		
		vValue[yy]= (vValue[yy] / (row*((BITMAPINFO *)pBitmapGr)->bmiHeader.biWidth)) ;
		somma += vValue[yy];
		SommaX2+= vValue[yy]*vValue[yy];
		
	}
	somma = somma / N;
	SommaX2 = SommaX2 / N;
	Sigma = (Math::Sqrt(SommaX2- somma*somma));
	return Sigma;
} // CalcolaIstogramma



int Ls150Class::Periferica::LeggiImmCardQual(char Side,short ScanMode)
{
	// qualita carta

	HANDLE HBackGray=0;
	HANDLE HFrontGray=0;
	//char StrNome[256];
	char *StrNome= NULL ; 
	//char Matri[32];
	short floop = false;
	short Connessione;
	//char conf[5];
	//char BarcodeRead[256];
	//short lenBarcodeRead;
	//short ErrorRate;



	ReturnC->FunzioneChiamante = "LeggiImmCardQual";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	//Reply = LS150_Open((HWND)hDlg,(HANDLE)hInst,SUSPENSIVE_MODE);
	
	if( Reply == LS_OKAY )
	{
		//if (HFrontGray)
		//	LSFreeImage((HWND)hDlg, &HFrontGray);
		//if (HBackGray)
		//	LSFreeImage((HWND)hDlg, &HBackGray);

		
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								Side,
								ScanMode,//SCAN_MODE_256GR300,
								PATH_FEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_CARD,
								0);
	

		if( Reply == LS_OKAY )
		{
			// Leggo l'immagine filmata
	
			Reply = LSReadImage(Connessione, (HWND)hDlg,
										CLEAR_ALL_BLACK,
										Side,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);

			
			if(Reply == LS_OKAY)
			{
				if ((Side == SIDE_FRONT_IMAGE) || (Side == SIDE_ALL_IMAGE))
				{
					Marshal::FreeHGlobal(IntPtr(StrNome));
					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
					
					LSSaveDIB((HWND)hDlg,HFrontGray,StrNome);
				}
				
				if ((Side == SIDE_BACK_IMAGE) || (Side == SIDE_ALL_IMAGE))
				{
					Marshal::FreeHGlobal(IntPtr(StrNome));
					if (Side == SIDE_ALL_IMAGE)
					{					
						StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();
					}
					else
					{
						StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
					}
					
					LSSaveDIB((HWND)hDlg,HBackGray,StrNome);
				}
			

				
			
			}
			else
			{
				ReturnC->FunzioneChiamante = "LSReadImage";
			}
			LSDisconnect(Connessione,(HWND)hDlg);
		}
		else
		{
			ReturnC->FunzioneChiamante = "LSDocHandle";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}
	// libero le immagini
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	ReturnC->ReturnCode = Reply;
	
	return Reply;
}

int Ls150Class::Periferica::Docutest309(short ScanMode)
{

	DateTime iniTime,currTime;
	TimeSpan t;
	HANDLE HBackGray = NULL ;
	HANDLE HFrontGray = NULL ;
	char *StrNome = NULL; 
	short Connessione;

	//SCAN_MODE_256GR200 
	//SCAN_MODE_COLOR_300

	ReturnC->FunzioneChiamante = "Docutest309";
	//-----------Open--------------------------------------------Diagnostica
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	
	if( Reply == LS_OKAY )
	{

		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,100,215);
		if( Reply == LS_OKAY )
		{
	
			Reply = LSDocHandle(Connessione,(HWND)hDlg,
									NO_STAMP,
									NO_PRINT_VALIDATE,
									NO_READ_CODELINE,
									SIDE_ALL_IMAGE,
									ScanMode , //SCAN_MODE_256GR200,
									AUTOFEED,
									SORTER_BAY1,
									WAIT_YES,
									BEEP,
									NULL,
									SCAN_PAPER_DOCUMENT,
									0);
			
			if (Reply == LS_OKAY)
			{
		
				Reply = LSReadImage(Connessione, (HWND)hDlg,
											NO_CLEAR_BLACK,
											SIDE_ALL_IMAGE,
											0, 0,
											(LPHANDLE)&HFrontGray,
											(LPHANDLE)&HBackGray,
											NULL,
											NULL);
					
				if (Reply == LS_OKAY)
				{
					
					
					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();				
					
					Reply = LSSaveDIB((HWND)hDlg,HFrontGray,StrNome);
					if (Reply == LS_OKAY)
					{
						Marshal::FreeHGlobal(IntPtr(StrNome));
						StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();
						Reply = LSSaveDIB((HWND)hDlg,HBackGray,StrNome);
						
					}	
					else
					{
						ReturnC->FunzioneChiamante =  "LSSaveDIB";
					}
				}
				else
				{
					ReturnC->FunzioneChiamante = "LSReadImage";
				}
			
			}
			else
			{
				ReturnC->FunzioneChiamante = "LSDocHandle";
			}
		}
		else
		{
			ReturnC->FunzioneChiamante = "LSConfigDoubleLeafingAndDocLength";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}

	ReturnC->ReturnCode = Reply;

	// libero le immagini
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	
	LSDisconnect(Connessione,(HWND)hDlg);
	
	return Reply;
}


int Ls150Class::Periferica::TestFotoPresenza(int ^%PresenzaAssegno)
{
	short Connessione;

	//SCAN_MODE_256GR200 
	//SCAN_MODE_COLOR_300

	ReturnC->FunzioneChiamante = "TestFotoPresenza";
	//-----------Open--------------------------------------------Diagnostica
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	
	if( Reply == LS_OKAY )
	{

		UNITSTATUS Stato;
		Stato.Size  = sizeof  (Stato);
		Stato.UnitStatus = LS_150_USB ;
		Reply = LSUnitStatus(Connessione,(HWND)0,&Stato ) ; 
			
		if (Reply == LS_OKAY)
		{
			if (Stato.Photo_Feeder == TRUE ) 
				*PresenzaAssegno = 1 ;
			else
				*PresenzaAssegno = 0 ; 

		}
	}
					
	ReturnC->ReturnCode = Reply;

	LSDisconnect(Connessione,(HWND)hDlg);
	
	return Reply;
}


int Ls150Class::Periferica::TestScannerUvCalibrationReExecute()
{

	HANDLE HBackGray=0;
	HANDLE HFrontGray=0;
	HANDLE HFrontUV=0;
	short Connessione=0;
	char *StrNome=NULL;
	short floop = false;
	//FILE	*fh;
	//char	IdentStr[8];
	//char	LsName[20];
	//char	Version[20];
	//char BytesCfg[8];
	//float	ver;
	//int rc_temp ; 
	

	ReturnC->FunzioneChiamante = "TestScannerUvCalibrationReExecute";
	
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	if (Reply == LS_OKAY )
	{
	
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,150,230);

		Reply = LSModifyPWMUltraViolet(Connessione , (HWND)hDlg  , 100 , TRUE , 0 ) ; 

		//Reply =	LSUnitIdentify(Connessione,(HWND)0, IdentStr,LsName,Version,NULL,NULL,NULL, NULL,NULL, NULL, NULL, NULL, NULL, NULL);

		do
		{
		
			Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								SIDE_ALL_IMAGE , //SIDE 
								SCAN_MODE_256GR300_AND_UV , 
								AUTOFEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);


			if (Reply == LS_FEEDER_EMPTY)
			{
				floop = TRUE;
			}
			else
				floop = FALSE;
		}while(floop);

		if( Reply == LS_OKAY )
		{
			// Leggo l'immagine filmata
			Reply = LSReadImage(Connessione,(HWND)hDlg,
									NO_CLEAR_BLACK , //CLEAR_AND_ALIGN_IMAGE , 
									SIDE_ALL_IMAGE , //Side,
									0,NULL,
									(LPHANDLE)&HFrontGray,
									(LPHANDLE)&HBackGray,
									(LPHANDLE)&HFrontUV,//NULL,
									NULL);

			LSDisconnect(Connessione,(HWND)hDlg);

		}
		else
		{	
			ReturnC->FunzioneChiamante = "LSDocHandle";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSConnect";
	}

		Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione);
		if(Reply == LS_OKAY)
		{
			
			//Davide TRACCIAMENTO
			//time_t ora;
			//ora = time(NULL);
			//rc_temp = LSSaveDIB((HWND)hDlg,HFrontUV,"C:\\ARCA\\SCIS.bmp");

			//printf("%s\n", asctime(localtime(&ora)));
			Reply = LSCISCompensation(Connessione, (HWND)hDlg, LS150_IMAGE_UV_STEP_2, HFrontUV, NULL);//pImageUVL);
			/*if( fh = fopen("Trace_Calibrazione_Periferica.txt", "at") )
			{
				fprintf(fh, "LSCISCompensation Reply =%d Data=%s",Reply,asctime(localtime(&ora)));
				fprintf(fh, "\n");
				fclose( fh );
			}*/
		}
		else
		{
			ReturnC->FunzioneChiamante = "LSCISCompensation";
		}

		LSDisconnect(Connessione,(HWND)hDlg);

		
	

	// libero le immagini
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	if (HFrontUV)
	{
		//rc = LSFreeImage((HWND)hDlg, &HFrontUV);
		LSFreeImage((HWND)hDlg, &HFrontUV);

		/*if( rc != LS_OKAY || HFrontUV != 0 )
		{
		}*/
	}

	
	ReturnC->ReturnCode = Reply;
	

	return Reply;
}


int Ls150Class::Periferica::DoImageCalibrationNew(short Connessione,HWND hDlg, float TeoricValue)
{
	HANDLE	BufFrontImage = 0 ; 
	int	nRet;
	int fretry;
	float	RealValue, DiffPercentuale;
	short	OldValue, NewValue;
	struct GRIDPRO_GeometryCriteria pGeoCriteria ; 
	struct GRIDPRO_PointCriteria pPntCriteria ;
	struct GRIDPRO_Point p1 ; 
	struct GRIDPRO_Point p2 ; 
	struct GRIDPRO_Point p3 ; 
	struct GRIDPRO_Point p4 ; 
	int pError ;
	GRIDHANDLE  handle ;
	int Rret = 0 ;
    double dx = 0 ;
	double dy = 0 ;
	double dx1 = 0 ;
	double dy1 = 0 ;
	int d = 0 ;
	int d1 = 0 ;
	double ax = 0 ;
	double ax1 = 0 ;
	int ad = 0 ;
	int ad1 = 0 ;

	
	nRet = TRUE;
	fretry = FALSE;
	
	{
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);
		//----------- Set attesa introduzione documento -----------
		Reply = LSDisableWaitDocument(Connessione,hDlg, FALSE);

		Reply = LSDocHandle(Connessione,(HWND)hDlg,
									NO_STAMP,
									NO_PRINT_VALIDATE,
									NO_READ_CODELINE,
									SIDE_FRONT_IMAGE,
									//SCAN_MODE_256GR200,
									SCAN_MODE_256GR300 ,
									AUTOFEED,
									SORTER_BAY1,
									WAIT_YES,
									NO_BEEP,
									NULL,
									SCAN_PAPER_DOCUMENT,
									0);

		if( Reply != LS_OKAY )
		{
			fretry = TRUE;
			return Reply;	
		}
		else
		{
		
			Reply = LSReadImage(Connessione, (HWND)hDlg,
											CLEAR_ALL_BLACK,
											SIDE_FRONT_IMAGE,
											0, 0,
											(LPHANDLE)&BufFrontImage,
											NULL,
											NULL,
											NULL);
					

			if (Reply != LS_OKAY)
			{
				fretry = TRUE;
				return Reply;

			}

			//LSSaveDIB((HWND)hDlg,BufFrontImage,"C:\\Arca\\PrimaClear.bmp");

			// Tolgo il nero davanti e dietro e NON sopra per avere 864 righe
			ClearFrontAndRearBlack( (BITMAPINFOHEADER *)BufFrontImage );

			//LSSaveDIB((HWND)hDlg,BufFrontImage,"C:\\Arca\\DopoClear.bmp");
		
			
            pGeoCriteria.columns =2 ;
			pGeoCriteria.rows = 2;
			pGeoCriteria.dispX = (double)161 ; 
			pGeoCriteria.dispY = (double)78 ;
			pGeoCriteria.deformPercX = (double)0.24 ; 
			pGeoCriteria.deformPercY = (double)0.24 ; 


			pPntCriteria.th = 90 ; 
			pPntCriteria.filledDensity.min = (double)0.5 ; 
			pPntCriteria.filledDensity.max  = (double)1.0 ; 
			pPntCriteria.emptyDensity.min = (double)0.5; 
			pPntCriteria.emptyDensity.max =  (double)1.0 ; 
			pPntCriteria.diameter.min = (double)1.5 ;
			pPntCriteria.diameter.max = (double)2.5 ;
		
			handle = GRIDPRO_NewGrid(GRIDPRO_SINGLE_MODE,( const unsigned char * )BufFrontImage,&pGeoCriteria,&pPntCriteria,&pError);

			if (pError == GRIDPRO_OK )
			{
				Rret  =GRIDPRO_GetGridPointAt(handle,0, 0,&p1 ) ; 
				
				Rret  =GRIDPRO_GetGridPointAt(handle,1, 0,&p2 ) ;

				Rret  =GRIDPRO_GetGridPointAt(handle,0, 1,&p3 ) ;

				Rret  =GRIDPRO_GetGridPointAt(handle,1, 1,&p4 ) ;

				GRIDPRO_DeleteGrid(handle);

				dx1 = (p4.x - p3.x );
				dy1 = (p4.y - p3.y );

				dx = (p2.x - p1.x );
				dy = (p2.y - p1.y );

				ax = (p3.y - p1.y );
				ax1 = (p4.y - p2.y ) ; 


				d = (int)sqrtf((float)(dx*dx+dy*dy));
				d1 = (int)sqrtf((float)(dx1*dx1+dy1*dy1));

				if((d > (d1 - 10)) || (d < (d1 + 10)))
				{
					Reply = LS_OKAY;
				}
				else
				{
					fretry = TRUE;
					Reply = LS_RETRY;
					return Reply;
				}

				if((ax > (ax1 - 10)) || (ax < (ax1 + 10)))
				{
					Reply = LS_OKAY;
				}
				else
				{
					fretry = TRUE;
					Reply = LS_RETRY;
					return Reply;
				}

			}
			else
			{
				fretry = TRUE;
				Reply = LS_RETRY;
				return Reply;
			}

			// Leggo lunghezza image
			RealValue = (float)d ; 

			// Leggo il valore settato nella periferica
			Reply = LSImageCalibration(Connessione,hDlg, FALSE, &OldValue);
			if( Reply != LS_OKAY )
			{
				CheckReply(hDlg, Reply, "LSImageCalibration");
				fretry = TRUE;
				return Reply;
			}

			// Controllo lunghezza image
			DiffPercentuale = TeoricValue * (float)0.004;		// 0.4%

			if(	RealValue < (TeoricValue - DiffPercentuale) )
			{
				// Calcolo il valore in percentuale da sommare a quello presente nella perifeica
				DiffPercentuale = TeoricValue - RealValue;
				DiffPercentuale = DiffPercentuale * (float)100 / TeoricValue;

				NewValue = (unsigned short)((float)OldValue * DiffPercentuale) / 100;
				NewValue = OldValue - NewValue;

				

				nRet = FALSE;	// Devo riprovare la calibrazione
				fretry = TRUE;
				Reply = LS_RETRY;
				
			}


			if( RealValue > (TeoricValue + DiffPercentuale) )
			{
				// Calcolo il valore in percentuale da sommare a quello presente nella perifeica
				DiffPercentuale = RealValue - TeoricValue;
				DiffPercentuale = DiffPercentuale * (float)100 / TeoricValue;

				NewValue = (unsigned short)((float)OldValue * DiffPercentuale) / 100;
				NewValue += OldValue;

			

				nRet = FALSE;	// Devo riprovare la calibrazione
				fretry = TRUE;
				Reply = LS_RETRY;
			}


			// Se nRet = FALSE ho variato i valori, perci� vado a scrivere quelli nuovi
			if( fretry == TRUE)
			{
				
				// Controllo che non sia inferiore al 1% del default
				if( NewValue < (SCANNER_TIME_DEFAULT * 0.99) )
					NewValue = SCANNER_TIME_DEFAULT;	// forzo il default

				// Controllo che non sia superiore al 3% del default
				if( NewValue > (SCANNER_TIME_DEFAULT * 1.03) )
					NewValue = SCANNER_TIME_DEFAULT;		// forzo il default
		

				Reply = LSImageCalibration(Connessione,hDlg, TRUE, &NewValue);
				if( Reply != LS_OKAY )
				{
					CheckReply(hDlg, Reply, "LSImageCalibration");
					nRet = TRUE;
					fretry = TRUE;
					return Reply;
					
				}
				else
				{
					Reply = LS_RETRY;
				}
			}

			// Libero le aree bitmap ritornate
			if( BufFrontImage )
				GlobalFree( BufFrontImage );
		}
	}

	return Reply;
} // DoImageCalibrationNew


///********************************************************************
//// FUNCTION  : ControlloUniformitaImmagine
////
//typedef int (__stdcall CHKGETDOCFORIMGQUALITY)(CHK_DEVICES deviceName, char *DocuTest, int sizeDocutest, /*int *NrDocForCalib,*/ int *NrDocVerify);
//typedef int (__stdcall CHKCHECKQUALITY)(char *fullFileName, IMG_QUALITY_RES *pRes);
//typedef int (__stdcall CHKCHECKQUALITYIMAGEUV)(char *fullFileName, IMG_QUALITY_RES *pRes);
//typedef int (__stdcall CHKRELEASESTR)(IMG_QUALITY_RES *pRes);
//extern GETCODEDESCRIPTION	*pGetCodeDescription;	// puntatore alla funzione
//
////int	CINrDocForCalib;
//char	CIDocuTest[64];
//char	FileOutQIF[_MAX_PATH + _MAX_FNAME], FileOutQIR[_MAX_PATH + _MAX_FNAME], FileOutQIFUV[_MAX_PATH + _MAX_FNAME];
////********************************************************************
//int ControlloUniformitaImmagine(HWND hDlg, short ScanMode)
//{
//int	Reply, rc, ii;
//HINSTANCE h_Library	= NULL;	// handle della dll caricata
//CHKGETDOCFORIMGQUALITY	*pGetDocToProcess = NULL;	// puntatore alla funzione
//CHKCHECKQUALITY	*pCheckQuality = NULL;	// puntatore alla funzione
//CHKCHECKQUALITYIMAGEUV	*pCheckQualityImageUV = NULL;	// puntatore alla funzione
//CHKRELEASESTR	*pReleaseStruct = NULL;	// puntatore alla funzione
//char	PathLibrary[_MAX_PATH + _MAX_FNAME];
//HANDLE	BufFrontImage, BufRearImage, BufFrontImageUV;
//int	NrDocVerify;
//IMG_QUALITY_RES pResF, pResR, pResFUV;
//char	msgMinMax[1024];
//
//
//// Inizializzo variabili
//Reply = LS_OKAY;
//
//// Carico la libreria per verifica fascioni
//sprintf(PathLibrary, "%s%s", PathAppl, "libimgdiag.dll");
//if( (h_Library = LoadLibrary(PathLibrary)) != NULL )
//{
//pGetDocToProcess = (CHKGETDOCFORIMGQUALITY *)GetProcAddress(h_Library, "CHKGetDocForImgQuality");
//pCheckQuality = (CHKCHECKQUALITY *)GetProcAddress(h_Library, "CHKCheckQuality");
//pCheckQualityImageUV = (CHKCHECKQUALITY *)GetProcAddress(h_Library, "CHKCheckQualityImageUV");
//pReleaseStruct = (CHKRELEASESTR *)GetProcAddress(h_Library, "CHKCheckQuality_release");
//pGetCodeDescription = (GETCODEDESCRIPTION *)GetProcAddress(h_Library, "CCIGetCodeDescription");
//
//if( (pGetDocToProcess == NULL) || (pCheckQuality == NULL) )
//{
//sprintf(PathLibrary, "2 - GetLastError( %d )", GetLastError());
//MessageBox(hDlg, PathLibrary, TITLE_ERROR, MB_OK | MB_ICONERROR);
//Reply = LS_NO_LIBRARY_LOAD;
//}
//
//if( Reply == LS_OKAY )
//{
//// Carico i parametri
//if(ScanMode == SCAN_MODE_256GR300_AND_UV)
//Reply = pGetDocToProcess(CHECKREADER_LS_150UV, CIDocuTest, sizeof(CIDocuTest), /*&CINrDocForCalib,*/ &NrDocVerify);
//else
//Reply = pGetDocToProcess(CHECKREADER_LS_150, CIDocuTest, sizeof(CIDocuTest), /*&CINrDocForCalib,*/ &NrDocVerify);
//}
//
//if( Reply == LS_OKAY )
//{
//// Visualizzo la dialog introduzione documenti
////	if( DialogBox(hInst, MAKEINTRESOURCE(IDD_CHECK_FASCIONI), hDlg, (DLGPROC)DlgProcCheckFascioni) )
//{
////-----------Open--------------------------------------------
//// Da non mettere LSConnectDiagnostica() !!!   altrimenti NON calibra !
//Reply = ConnectLSUnit(hDlg, CONNECT_NORMAL, FALSE, &hLS);
//if( Reply == LS_OKAY )
//{
//Reply = LSUnitConfiguration(hLS, hDlg, NULL, NULL, NULL, NULL, NULL, SerialNumber, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
//
////----------- Set Block Document if double leafing -----------
//Reply = LSConfigDoubleLeafingEx(hLS, hDlg, (short)DOUBLE_LEAFING_WARNING, DOUBLE_LEAFING_DISABLE, 0);
//
////----------- Set attesa introduzione documento -----------
//Reply = LSDisableWaitDocument(hLS, hDlg, FALSE);
//
////----------- Only for Ultra Violet type -----------
//if(ScanMode == SCAN_MODE_256GR300_AND_UV)
//Reply = LSModifyPWMUltraViolet(hLS, hDlg, 100, TRUE, 0);
//
//// Chiamo la funzione passando l'ultima configurazione settata
//Reply = LSDocHandle(hLS, hDlg,
//NO_STAMP,
//NO_PRINT_VALIDATE,
//NO_READ_CODELINE,
//SIDE_ALL_IMAGE,
//ScanMode,
//AUTO_FEED,
//SORTER_BAY1,
//WAIT_YES,
//NO_BEEP,
//NULL,
//0,
//0);
//
//if( Reply != LS_OKAY )
//{
//CheckReply(hDlg, Reply, "LS150_DocHandle");
//}
//
////-----------ReadImage-------------------------------------
//Reply = LSReadImage(hLS, hDlg,
//NO_CLEAR_BLACK,
//SIDE_ALL_IMAGE,
//0, 0,
//(LPHANDLE)&BufFrontImage,
//(LPHANDLE)&BufRearImage,
//(LPHANDLE)&BufFrontImageUV,
//NULL); 
//
//if( Reply == LS_OKAY )
//{
//sprintf(FileOutQIF, "%s%s\\ImageQuality_%s_%04dFF.bmp", PathAppl, SAVE_DIRECTORY_IMAGE, SerialNumber, stParDocHandle.IndexQualityImage);
//Reply = LSSaveDIB(hDlg, BufFrontImage, FileOutQIF);
//sprintf(FileOutQIR, "%s%s\\ImageQuality_%s_%04dBB.bmp", PathAppl, SAVE_DIRECTORY_IMAGE, SerialNumber, stParDocHandle.IndexQualityImage ++);
//Reply = LSSaveDIB(hDlg, BufRearImage, FileOutQIR);
//// Ho chiesto di controllare UV ?
//if(ScanMode == SCAN_MODE_256GR300_AND_UV)
//{
//sprintf(FileOutQIFUV, "%s%s\\ImageQuality_%s_%04dFFUV.bmp", PathAppl, SAVE_DIRECTORY_IMAGE, SerialNumber, stParDocHandle.IndexQualityImage);
//Reply = LSSaveDIB(hDlg, BufFrontImageUV, FileOutQIFUV);
//}
//
//if( BufFrontImage )
//LSFreeImage(hDlg, &BufFrontImage);
//if( BufRearImage )
//LSFreeImage(hDlg, &BufRearImage);
//if(BufFrontImageUV)
//LSFreeImage(hDlg, &BufFrontImageUV);
//}
//else
//CheckReply(hDlg, Reply, "LS150_ReadImage");
//
//LSDisconnect(hLS, hDlg);	// Faccio la close
//
//
//if( Reply == LS_OKAY )
//{
//memset(&pResF, 0, sizeof(pResF));
//memset(&pResR, 0, sizeof(pResR));
//memset(&pResFUV, 0, sizeof(pResFUV));
//
//// Ho chiesto di controllare UV ?
//if(ScanMode == SCAN_MODE_256GR300_AND_UV)
//{
//// controllo Reply
//Reply = pCheckQualityImageUV(FileOutQIFUV, &pResFUV);
//
//// Visualizzo il minimo ed il massimo
//if(Reply == CCI_OKAY || Reply == CCI_TEST_FAILED)
//{
//char	sEsito[32];
//
//sprintf(msgMinMax, "Side  Esito               Min       Max         Avg         devStd");
//SendMessage(GetDlgItem(hDlg, IDC_LIST_ESITO), LB_ADDSTRING, 0L, (LPARAM)msgMinMax);
//for(ii = 0; ii < pResFUV.numOfData; ii ++)
//{
//ConvertEsito((int)pResFUV.data[ii].esito, sEsito);
//sprintf(msgMinMax, " F%d    %c                  %3d       %3d        %3.2f        %3.2f", (ii + 1), sEsito[0], /*pResFUV.data[ii].delta, */pResFUV.data[ii].minColor, pResFUV.data[ii].maxColor, pResFUV.data[ii].avg, pResFUV.data[ii].devStd);
//SendMessage(GetDlgItem(hDlg, IDC_LIST_ESITO), LB_ADDSTRING, 0L, (LPARAM)msgMinMax);
//}
//ConvertEsito((int)pResFUV.esito, sEsito);
//sprintf(msgMinMax, "Front Image - esito = %s", sEsito);
//SendMessage(GetDlgItem(hDlg, IDC_LIST_ESITO), LB_ADDSTRING, 0L, (LPARAM)msgMinMax);
//}
//}
//else
//{
//// controllo Reply
//Reply = pCheckQuality(FileOutQIF, &pResF);
//
//// Visualizzo il minimo ed il massimo
//if( Reply == CCI_OKAY || Reply == CCI_TEST_FAILED )
//{
//char	sEsito[32];
//
//sprintf(msgMinMax, "Side  Esito  Delta        Min       Max         Avg         devStd");
//SendMessage(GetDlgItem(hDlg, IDC_LIST_ESITO), LB_ADDSTRING, 0L, (LPARAM)msgMinMax);
//for( ii = 0; ii < pResF.numOfData; ii ++)
//{
//ConvertEsito((int)pResF.data[ii].esito, sEsito);
//sprintf(msgMinMax, " F%d    %c      %3d         %3d       %3d        %3.2f        %3.2f", (ii + 1), sEsito[0], pResF.data[ii].delta, pResF.data[ii].minColor, pResF.data[ii].maxColor, pResF.data[ii].avg, pResF.data[ii].devStd);
//SendMessage(GetDlgItem(hDlg, IDC_LIST_ESITO), LB_ADDSTRING, 0L, (LPARAM)msgMinMax);
//}
//ConvertEsito((int)pResF.esito, sEsito);
//sprintf(msgMinMax, "Front Image - esito = %s", sEsito);
//SendMessage(GetDlgItem(hDlg, IDC_LIST_ESITO), LB_ADDSTRING, 0L, (LPARAM)msgMinMax);
//
//// controllo Reply
//rc = pCheckQuality(FileOutQIR, &pResR);
//
//// Visualizzo il minimo ed il massimo
//if( rc == CCI_OKAY || rc == CCI_TEST_FAILED )
//{
//SendMessage(GetDlgItem(hDlg, IDC_LIST_ESITO), LB_ADDSTRING, 0L, (LPARAM)" ");
//sprintf(msgMinMax, "Side  Esito  Delta        Min       Max         Avg         devStd");
//SendMessage(GetDlgItem(hDlg, IDC_LIST_ESITO), LB_ADDSTRING, 0L, (LPARAM)msgMinMax);
//for( ii = 0; ii < pResR.numOfData; ii ++)
//{
//ConvertEsito((int)pResR.data[ii].esito, sEsito);
//sprintf(msgMinMax, " R%d    %c      %3d         %3d       %3d        %3.2f        %3.2f", (ii + 1), sEsito[0], pResR.data[ii].delta, pResR.data[ii].minColor, pResR.data[ii].maxColor, pResR.data[ii].avg, pResR.data[ii].devStd);
//SendMessage(GetDlgItem(hDlg, IDC_LIST_ESITO), LB_ADDSTRING, 0L, (LPARAM)msgMinMax);
//}
//ConvertEsito((int)pResR.esito, sEsito);
//sprintf(msgMinMax, "Rear Image - esito = %s", sEsito);
//SendMessage(GetDlgItem(hDlg, IDC_LIST_ESITO), LB_ADDSTRING, 0L, (LPARAM)msgMinMax);
//
//// Se aveva fallito gi� il fronte non lo cambio
//if( rc != CCI_OKAY )
//Reply = rc;
//}
//}
//}
//
//EnableWindow(GetDlgItem(hDlg, ID_SHOW_IMAGES), TRUE);
//
//CheckReply_libimgdiag(hDlg, Reply, "Test Image Quality", FILE_QUALITY_IMAGE_INI);
//
//// Libero le aree dati
//pReleaseStruct(&pResF);
//pReleaseStruct(&pResR);
//pReleaseStruct(&pResFUV);
//}
//}
//else
//CheckReply(hDlg, Reply, "LS150_Open");
//}
//}
//
//FreeLibrary(h_Library);
//}
//else
//{
//sprintf(PathLibrary, "1 - GetLastError( %d )", GetLastError());
//MessageBox(hDlg, PathLibrary, TITLE_ERROR, MB_OK | MB_ICONERROR);
//Reply = LS_NO_LIBRARY_LOAD;
//}
//
//return Reply;
//} // ControlloUniformitaImmagine


