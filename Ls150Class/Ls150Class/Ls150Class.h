// Ls515Class.h

#include <windows.h>
#include "ErrWar.h"
#include "is.h"
#include "Constanti.h"


#pragma once

using namespace System;
using namespace System::IO;
using namespace System::Runtime::InteropServices;
using namespace System::Runtime::Serialization;
using namespace System::Reflection;
using namespace System::Windows::Forms;



namespace Ls150Class {

	public ref class Storico
	{
	public:
		// TODO: aggiungere qui i metodi per la classe.
		int Documenti_Catturati;
		int Documenti_Ingresso;
		int Documenti_Trattati;
		int Documenti_Trattenuti;
		int Doppia_Sfogliatura;
		int Errori_Barcode;
		int Errori_CMC7;
		int Errori_E13B;
		int Errori_Ottici;
		int Jam_Allinea;
		int Jam_Card;
		int Jam_Cassetto1;
		int Jam_Cassetto2;
		int Jam_Cassetto3;
		int Jam_Feeder;
		int Jam_Feeder_Ret;
		int Jam_Micr;
		int Jam_Micr_Ret;
		int Jam_Percorso_DX;
		int Jam_Percorso_SX;
		int Jam_Print;
		int Jam_Print_Ret;
		int Jam_Scanner;
		int Jam_Scanner_Ret;
		int Jam_Stamp;
		int Jam_Sorter;
		int Num_Accensioni;
		int Doc_Timbrati;
		int Doc_Timbrati_retro;
		int Trattenuti_Micr;
		int Trattenuti_Scan;
		int TempoAccensione;
	};

	
	
	public ref class Periferica
	{
			
	private : 
	 
	
			//int __stdcall Visual(char * str);
			int __clrcall Visual(char * str);
			//short GetTypeLS(String ^Version);
			short GetTypeLS(char *LsName,char *Version, char *Ls515_Config);
			int CheckReply(HWND hDlg,int Rep,char *s);

			short MICR_TolleranceMore;
			short MICR_TolleranceLess;
			float MICR_Valore100 ;
			short	nrLettura;
			//int MICR_SignalValue;
			int MICR_DocValue;

			

			// legge le codeline OCR
			//int LeggiCodelineOCR(short Type);

	public : 
		
		//	static const String^ VERSIONEDLL_1 = "Ls515Class Ver 1.00 del 10-06-09";
			  ErrWar ^ReturnC ;
			  Cis ^strCis;
			Storico ^S_Storico;

			String ^  VersioneDll_0;
			String ^ VersioneDll_1;



			short TipoPeriferica;
			short Connessione;
		

			unsigned short    LastBeforeTimeSample1, LastBeforeTimeSample2, LastBeforeTimeSample3;
			short NrLettureOk;
			long  llMax, llMin;
			long  TimeSampleVal ; 
			float llMedia;
			double DevStd;
			short ConfVelocitBassa;
			//short ConfMicr;
			short ConfFeederMotorized;
			short ConfEndorserPrinter;
			short ConfVoidingStamp;
			short ConfScannerFront;
			short ConfScannerBack;
			short ConfBadgeReader;
			short ConfScannerGrigio;
			

			// stringa che contiene l'ultima codeline letta
			String ^  CodelineLetta;
			String ^  BadgeLetto;
			String ^  PDFLetto;
			String ^  BarcodeLetto;
			String ^  DocutestFascioni ; 
			
			//variabili usate da DOCUTEST 089
			String ^  BarcodeLetto0;
			String ^  BarcodeLetto1;
			String ^  BarcodeLetto2;
			String ^  DataMatrixLetto0;
			String ^  DataMatrixLetto1;
			String ^  DataMatrixLetto2;

			// stringhe ritornate dall identificativo
			String   ^ SIdentStr;
			String ^  SLsName;
			String ^  SDateFW;
			String ^  SVersion;
			String ^  SSerialNumber;
			String ^  SFeederVersion;
			String ^  SBoardNr;
			String ^  SCpldNr;
			String ^  Stampafinale;
			String ^  FileNameDownload;
			String ^  NomeFileImmagine;
			String ^  NomeFileImmagineRetro;
			String ^  NomeFileImmagineUV;
			String ^  NomeFileFascioni ; 
			String ^  NomeFileReport ; 
			
			// valori photosensori
			static cli::array<int> ^ PhotoValue = gcnew cli::array<int>(8);
			// valori fotosensori doppia SF
			static cli::array<Byte> ^ PhotoValueDP = gcnew cli::array<Byte>(16);
			// Byte di Stato periferica 
			static cli::array<Byte> ^ StatusByte = gcnew cli::array<Byte>(16);
			static cli::array<Int16> ^ Distanze = gcnew cli::array<Int16>(1024);
			static cli::array<float> ^ HistogrammaFronte = gcnew cli::array<float>(256);
			static cli::array<float> ^ HistogrammaRetro = gcnew cli::array<float>(256);
			
			double SigmaFronte,SigmaRetro;

			// dump memoria
			static cli::array<Byte> ^ BufferDump = gcnew cli::array<Byte>(0x9000);

			static cli::array<Byte> ^ BufferDumpGray = gcnew cli::array<Byte>(3000);
			static cli::array<Byte> ^ BufferDumpBlue = gcnew cli::array<Byte>(3000);
			static cli::array<Byte> ^ BufferDumpGreen = gcnew cli::array<Byte>(3000);
			static cli::array<Byte> ^ BufferDumpRed = gcnew cli::array<Byte>(3000);
			static cli::array<Byte> ^ BufferDumpUVLow = gcnew cli::array<Byte>(3000);
			static cli::array<Byte> ^ BufferDumpUVHight = gcnew cli::array<Byte>(3000);

			
			static cli::array<Byte> ^ BufferDumpGrayR = gcnew cli::array<Byte>(3000);
			static cli::array<Byte> ^ BufferDumpBlueR = gcnew cli::array<Byte>(3000);
			static cli::array<Byte> ^ BufferDumpGreenR = gcnew cli::array<Byte>(3000);
			static cli::array<Byte> ^ BufferDumpRedR = gcnew cli::array<Byte>(3000);
			static cli::array<Byte> ^ BufferDumpUVLowR = gcnew cli::array<Byte>(3000);
			static cli::array<Byte> ^ BufferDumpUVHightR = gcnew cli::array<Byte>(3000);

			static cli::array<Byte> ^ BufferADValues = gcnew cli::array<Byte>(256);
			static cli::array<Byte> ^ BufferADValuesR = gcnew cli::array<Byte>(256);


			//int stile;
			//int prova ;


			int ValMinCinghia;
			int ValMaxCinghia;
			float ValMinCinghiaNew;
			float ValMaxCinghiaNew;
			float SignalValueRead ;
			int NrPicchi ; 

			ListBox^ list;

			static cli::array<Byte> ^ E13BSignal  = gcnew cli::array<Byte>(32*1024);
			//Byte *E13BSignal;
			//Byte *ViewSignal;
			static cli::array<Byte> ^ ViewSignal  = gcnew cli::array<Byte>(32*1024);
			
			int fDatiScannerColore;
			int fDatiScannerUV;
			int fDatiScanner;
		

			
		// Reply delle funzioni
		int Reply;

		static int hDlg;
		static int hInst;
		static HANDLE IDPort;
		// tipo di periferica
		short TypeLS;
		// MICR lettura
		float Percentile;
		short ValMax,ValMin;
		short Sensore;


		
		unsigned short TempoCamp200;
		unsigned short TempoCamp300;
		unsigned short TempoCamp75;
		unsigned short TempoCamp300UV;
		unsigned short TempoCamp300C;

		short TempoImmagine;

		unsigned char TrimmerValue200;
		unsigned char TrimmerValue300;
		unsigned char TrimmerValue75;
		unsigned char TrimmerValue300UV;
		unsigned char TrimmerValue300C;

		short ValoreCalibrazione ;

		//Micro-Hole
		unsigned short errorFlags;       ///< Pattern failure information
		double innerDimValidPerc;        ///< Percentage of valid bars relative to inner dimension criteria
		double outerDimValidPerc;        ///< Percentage of valid bars relative to outer dimension criteria
		double innerLevValidPerc;        ///< Percentage of valid bars relative to inner grey level criteria
		double outerLevValidPerc;        ///< Percentage of valid bars relative to outer grey level criteria

		double wDimStatmean ;
		double wDimStatStdDev ;
		double bDimStatmean ;
		double bDimStatStdDev ;
		double gDimStatmean ;
		double gDimStatDev ;
		double wLevStatmean ;
		double wLevStatStdDev ;
		double bLevStatmean ;
		double bLevStatStdDev ;



		
		// identificativo
		int Identificativo();
		// Calibra i fotosensori e ritorna i valori
		int SetConfigurazione(int E13B,int CMC7,int timbro,int badge12,int badge23,int badge123,int scannerfronte, int scannerretro,int inkjet, int velocita,int Sflogliatore50,int SwAbilitato,int cardCol);
		int CalibraImmagine();
		int CalibraImmagineNew();
		// legge le codeline OCR
		int LeggiCodelineOCR(short Type);

		int CalibrazioneFoto();
		// imposta la configurazione , calibra il foto DP e reimposta la configurazione
		int CalSfogliatura();

		// Timbra fronte e retro
		int Timbra(short DocHandling);
		//testa la stampante
		int TestStampa(int StampaStd);
		int TestStampaAlto(int StampaStd);
		int LeggiBadge(short Traccia);

		int SerialNumber();
		//int DoMICRCalibration3_rg(int MICR_Value,long NrReadGood,	char	TypeSpeed, ListBox^ list);
		int DoMICRCalibration3(int MICR_Value,long NrReadGood,	char	TypeSpeed, ListBox^ list);
		

	//	unsigned char Calc_Trimmer_Position_old(float ValoreLetto, unsigned char PosTrimmer);
		unsigned char Calc_Trimmer_Position(float ValoreLetto, unsigned char PosTrimmer, BOOL r33k_TrimmerValue);
		unsigned char Calc_Trimmer_Position_ev(float ValoreLetto, unsigned char PosTrimmer);

		int EstraiDatiSpeedE13B(unsigned char *pd, long llDati,short *NrPicchi, float *SommaValori, long *ValMin, short *NrPicchiMin);
		
		

		//int LeggiImmaginiAllinea(char Side, short ScanMode);

		int LeggiImmCard(char Side, short ScanMode, short ^%NrDistanze);
		int LeggiImmCard2(char Side, short ScanMode, short fPdf);
		
		// calibra gli scanner, viene passato il tipo di scanner fronte o retro
		int CalibraScannerOld();
		int CalibraScanner();
		int CalibraScanner2();
		
		// fa il reset della periferica
		int Reset(short TypeReset);

		int StatoPeriferica();

		int ViewE13BSignal(int fView);

		// legge la codeline e permette di trattenere catturare un documento
		int LeggiCodeline(short DocHandling);
		int LeggiCodelineImmagini(short DocHandling);
		int LeggiCodelineImmaginiAuto(short DocHandling);

		// esegue l'autodoc handle
		int ViaDocumentiAuto(short scanMode);
		int Stopaaaaa();
		//int DisegnaEstrectLenBar();
		int DisegnaEstrectLenBar(short NrChar);
		int Docutest04();

		int DoImageCalibration(short Connessione,HWND hDlg, float TeoricValue);
		int DoImageCalibrationNew(short Connessione,HWND hDlg, float TeoricValue);
		int CalcolaSalvaDistanze(HWND hDlg, BITMAPINFOHEADER *pImage, short LineToCheck);
		void ClearFrontAndRearBlack(BITMAPINFOHEADER *Bitmap);
		
		int DoTimeSampleMICRCalibration(char TypeSpeed,float);
		int EraseHistory();
		int AccendiLuce(short Luce);
		int MotoreFeeder(short Acceso);
		int sensorFeeder();
		int LeggiValoriCIS();

		int LeggiValoriPeriferica();

	//	void ShowMICRSignal(ListBox ^lista,float p,unsigned char trim);

		int SetValoreDefImmagine();

		// costruttore della classe
		Periferica();

		int SetVelocita(int fSpeed);
		int SetDiagnostic(int fbool);

		int GetVersione();
		int CollaudoLSConnect(HWND hDlg,HANDLE hInst,short TipoPeriferica,short *Connessione);
		 
		void ShowMICRSignal( float Percentile, short Trimmer);
		int SetElettromagnete(int elletro);
		
		double CalcolaIstogramma(HANDLE pBitmapGr, array<float>^vValue);
	/*	int TestPalettaMotor();*/

		int LeggiImmCardQual(char Side,short ScanMode);
		int PuliziaCinghia();

		int Ls150Class::Periferica::DecodificaBarcode2D(char Side,short ScanMode,short fPdf);
		int TestMicroForatura();
		int TestDownload();
		int TestCinghia();
		int Ls150Class::Periferica::TestCinghiaEx(int  THN , int  THP , int DocSignalValue);
		int Ls150Class::Periferica::TestBackground(int  fZebrato);
		int Ls150Class::Periferica::TestControlloFascioni(int  fControlloBackground , int fZebrato , int fTestFascioni); 
		int Ls150Class::Periferica::TestControlloFascioniParamter(int ^%RNrDocForCalib,int ^%RNrDocVerify );
		//int SetConfigurazioneProvvisoria(int E13B,int CMC7,int timbro,int badge12,int badge23,int badge123,int scannerfronte, int scannerretro,int inkjet,int Velocita,int Sflogliatore50,int SwAbilitato,int fCardCol);
		int Ls150Class::Periferica::ExecReadDocSpeed(char TypeSpeed, short StartFrom, short Sorter);
		int Ls150Class::Periferica::DisegnaEstractLenBar(short NrChar, unsigned char *pd, char *pb, BOOL fSaveData, char *strDocSpeed , char *FileNameReport);		
		int Ls150Class::Periferica::Docutest309(short ScanMode);
		int Ls150Class::Periferica::TestFotoPresenza(int ^%PresenzaAssegno);
		int Ls150Class::Periferica::TestScannerUvCalibrationReExecute();
		int Ls150Class::Periferica::SetTrimmer(short TrimmerValue , char TypeSpeed);
		int Ls150Class::Periferica::TestNormalization(short ScanMode, BOOL fCertificazione, unsigned short levelGray, unsigned short levelRD, unsigned short levelGR, unsigned short levelBL,int lotto,int , int );
		int LeggiImmagini(char Side , short ScanMode,short Clear);
		
	};
}
