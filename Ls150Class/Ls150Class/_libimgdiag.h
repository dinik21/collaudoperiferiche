//
// ARCA Technologies s.r.l.
// All rights reserved
//
// We reserve the right to vary technical specification
//
//  MODULE:   libimgdiag.h
//

#ifndef CCI_DEPOSIT_H
#define CCI_DEPOSIT_H		1

// ------------------------------------------------------------------------
//             DEFINES
// ------------------------------------------------------------------------

// Folder trace libreria corrente
#define CCI_DEBUG_DIRECTORY         "trace_libimgdiag"

// Parametro Operazione
#define OPERATION_CALCULATE		   'C'
#define OPERATION_CHECK					'V'

#define NR_K_COEFFICIENTS				4

enum
{
	K_POS_FV,				// Posizione del valore K Fronte Visibile
	K_POS_RV,				// Posizione del valore K retro Visibile
	K_POS_FI,				// Posizione del valore K Fronte Infrarosso
	K_POS_RI,				// Posizione del valore K retro Infrarosso
};

// **

typedef enum
{
   FASCIONI_PASS = 0,
   FASCIONI_WARNING,
   FASCIONI_FAIL,
   FASCIONI_NOT_EVALUATE
}
   ESITO_FASCIONI;

typedef struct
{
   ESITO_FASCIONI esito;
   float          data;
}
   FASCIONI_DATA, *PFASCIONI_DATA;

typedef struct
{
   FASCIONI_DATA bigArea;
   FASCIONI_DATA smallArea;
   FASCIONI_DATA auxArea;
   FASCIONI_DATA noiseArea;
}
   FASCIONI_RES, *PFASCIONI_RES;

// ------------------------------------------------------------------------
//             REPLY-CODE
// ------------------------------------------------------------------------

#define		CCI_OKAY								      0

// ------------------------------------------------------------------------
//             ERRORS
// ------------------------------------------------------------------------

#define		CCI_SYSTEM_ERROR						   -1
#define		CCI_FILE_INI_MISSING					   -2
#define		CCI_PARAMETER_IN_FILE_INI_MISSING	-3
#define		CCI_FILE_IMAGE_NOT_FOUND				-4
#define		CCI_DOCT_FILE_INI_MISSING	         -5
#define		CCI_PARAMETER_IN_DOCT_INI_MISSING	-6
#define		CCI_DOCT_INI_NOT_VALID              -7

// calcolo/verifica coefficienti K

#define		CCI_FV_OUT_MAX_DISTANCE					-10
#define		CCI_RV_OUT_MAX_DISTANCE					-11
#define		CCI_FI_OUT_MAX_DISTANCE					-12
#define		CCI_RI_OUT_MAX_DISTANCE					-13
#define		CCI_TEST_FAILED							-14
#define		CCI_MAX_SIGMA_VISIBLE_MISSING			-15
#define		CCI_MAX_SIGMA_INFRARED_MISSING		-16
#define		CCI_FV_SIGMA_OUT						   -17
#define		CCI_RV_SIGMA_OUT						   -18
#define		CCI_FI_SIGMA_OUT						   -19
#define		CCI_RI_SIGMA_OUT						   -20
#define		CCI_RECT1_VISIBLE_MISSING				-21
#define		CCI_RECT1_INFRARED_MISSING				-22
#define		CCI_DOC3_FACT_FV_MISSING				-23
#define		CCI_DOC3_FACT_RV_MISSING				-24
#define		CCI_DOC3_FACT_FI_MISSING				-25
#define		CCI_DOC3_FACT_RI_MISSING				-26
#define		CCI_MEAN_POS_VISIBLE_MISSING			-27
#define		CCI_MEAN_NEG_VISIBLE_MISSING			-28
#define		CCI_MEAN_POS_INFRARED_MISSING			-29
#define		CCI_MEAN_NEG_INFRARED_MISSING			-30
#define		CCI_DOC3_Ax_MEAN_POS_FV_MISSING		-31
#define		CCI_DOC3_Ax_MEAN_NEG_FV_MISSING		-32
#define		CCI_DOC3_Ax_MEAN_POS_IR_MISSING		-33
#define		CCI_DOC3_Ax_MEAN_NEG_IR_MISSING		-34
#define		CCI_MEAN_OF_MEAN_FV_OUT_RANGE			-35
#define		CCI_MEAN_OF_MEAN_RV_OUT_RANGE			-36
#define		CCI_MEAN_OF_MEAN_FI_OUT_RANGE			-37
#define		CCI_MEAN_OF_MEAN_RI_OUT_RANGE			-38

// setting immagini

#define     CCI_DOC3_ROT_MIN_FRONT_MISSING		-50
#define     CCI_DOC3_ROT_MIN_REAR_MISSING		   -51
#define     CCI_DOC3_LOCATE_MISSING		         -52
#define     CCI_DOC3_RES_MISSING		            -53
#define     CCI_RULE_UNIT_AREE_MISSING          -54
#define     CCI_RULES_AREE_MISSING              -55
#define     CCI_DOC3_TOO_INCLINED               -56

// ereditati dalla libdocana.dll

#define		CCI_DOCANA_SYSTEM_ERROR					-301	   // error
#define		CCI_DOCANA_RULES_NOT_FOUND				-302	   // rules file not found
#define		CCI_DOCANA_RULES_SYNTAX_ERROR			-303	   // syntax error on rules file
#define		CCI_DOCANA_FILES_BMP_NOT_FOUND		-304	   // files bitmap not found
#define		CCI_DOCANA_BITMAP_FAILED_ERROR		-305	   // bitmap failed to load
#define		CCI_DOCANA_BITMAP_LOCATE_ERROR		-306	   // bitmap failed to locate
#define		CCI_DOCANA_RESULT_OBJECT_EMPTY		-307	   // output variable is null
#define		CCI_DOCANA_INVALID_RESULT_OBJECT		-308	   // some output variable not properly format

// ------------------------------------------------------------------------
//             WARNINGS
// ------------------------------------------------------------------------
#define		CCI_DOCANA_STRING_TRUNCATED	       299
#define     CCI_MISSING_CALL_INIT_PARAMETERS     300

// ------------------------------------------------------------------------
//             EXPORT FUNCTIONS
// ------------------------------------------------------------------------
#ifdef __cplusplus
extern "C" {
#endif

// ** Deposit

extern int APIENTRY CCIGetDocToProcess(
   char *DocuTest, 
   int sizeDocutest, 
   int *NrDocForK, 
   int *NrDocVerify );

extern int APIENTRY CCICalcoloCoefficientiK(
   char *SerialID, 
   int StartNrImage, 
   char *RevDocuTest, 
   char *Lotto, 
   char Operazione, 
   float *CoefficentiK );

// ** Check readers

extern int APIENTRY CHKGetDocToProcess(
   char *DocuTest,
   int sizeDocutest,
   int *NrDocForCalib,
   int *NrDocVerify);

extern int APIENTRY CHKControlloFascioni(
   char *fullFileName,
   PFASCIONI_RES pRes );

// ** utils

extern int APIENTRY CCIGetCodeDescription(
   int errorCode,
   char szBuffErr[],
   int buffLength);

#ifdef __cplusplus
}
#endif

#endif