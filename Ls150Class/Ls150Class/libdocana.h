#ifndef _LIB_DOC_ANA
#define _LIB_DOC_ANA

#include <windows.h>

#ifdef LIBDOCANA_EXPORTS
#define LIBDOCANA_API __declspec(dllexport)
#else
#define LIBDOCANA_API __declspec(dllimport)
#endif

// ****************************************************************************************

#define DOCANA_DEBUG_DIRECTORY        "trace_libdocana"

#define DOCANA_OK                      0
#define DOCANA_STRING_TRUNCATED        1     // warning
#define DOCANA_SYSTEM_ERROR            -1    // error
#define DOCANA_RULES_NOT_FOUND         -2    // rules file not found
#define DOCANA_RULES_SYNTAX_ERROR      -3    // syntax error on rules file
#define DOCANA_FILES_BMP_NOT_FOUND     -4    // files bitmap not found
#define DOCANA_BITMAP_FAILED_ERROR     -5    // bitmap failed to load
#define DOCANA_BITMAP_LOCATE_ERROR     -6    // bitmap failed to locate
#define DOCANA_RESULT_OBJECT_EMPTY     -7    // output variable is null
#define DOCANA_INVALID_RESULT_OBJECT   -8    // some output variable not properly format
#define DOCANA_INVALID_FILTER_PARAMS   -9    // invalid or wrong filter parameters number
#define DOCANA_ERROR_ON_IMAGE_FILTER  -10    // error on libdenoising_fft or others

#define DOCANA_MAX_STRING_LENGTH       100

// **

#define IMG_MAX_GRAY_LEVELS            256
#define IMG_BLACK                      0

// ****************************************************************************************

typedef enum
{
   DOCANA_MIRROR_NONE = 0,
   DOCANA_MIRROR_V,
   DOCANA_MIRROR_H,
   DOCANA_MIRROR_ALL
}
   DOCANA_MIRROR_TYPE;

typedef enum
{
   DOCANA_RES_200x120_DPI = 0,               // Deposit system

   DOCANA_RES_200x200_DPI,                   // Check reader
   DOCANA_RES_300x300_DPI,
}
   DOCANA_RESOLUTION;

typedef enum
{
   DOCANA_ROTATE_ON = 0,
   DOCANA_ROTATE_OFF
}
   DOCANA_ROTATE;

typedef enum
{
   DOCANA_UNIT_AREA_MM = 0,
   DOCANA_UNIT_AREA_PIXEL
}
   DOCANA_UNIT_AREA;

// ** input struct

typedef struct
{
   char name[ 15 ];
   double xo;
   double yo;
   double width;
   double height;
}
   DOCANA_AREA, *PDOCANA_AREA;

typedef struct
{
   int               areaNum;
   DOCANA_UNIT_AREA  areaUnit;
   DOCANA_ROTATE     areaRot;
   DOCANA_AREA       area[ 10 ];
}
   DOCANA_AREA_2_EVAL, *PDOCANA_AREA_2_EVAL, **PPDOCANA_AREA_2_EVAL;

// ** filters parameters

typedef struct
{
   char szFilterMethodName[DOCANA_MAX_STRING_LENGTH];
}
DOCANA_FILTER_METHOD, *PDOCANA_FILTER_METHOD;

// ** output struct

typedef struct
{
   int elementNum;
   float *element;
}
   DOCANA_DERIVATA_2, *PDOCANA_DERIVATA_2;

typedef struct
{
   int colorFrequency[ IMG_MAX_GRAY_LEVELS] ;
   int minColor;
   int maxColor;
   int maxGrayLevels;
   int totalPixels;
}
   DOCANA_IMGHISTOGRAM, *PDOCANA_IMGHISTOGRAM;

typedef struct
{
   float E;                                  // Expected value or mean value
   float SD;                                 // Standard deviation
}
   DOCANA_IMGSTATINFO, *PDOCANA_IMGSTATINFO;

typedef struct
{
   char name[ 15 ];
   int xi;
   int xf;
   int yi;
   int yf;
}
   DOCANA_IMGREGION, *PDOCANA_IMGREGION;

typedef struct
{
   int ox;
   int oy;
   float angle;
   bool bRightOrigin;
}
   DOCANA_COORDS, *PDOCANA_COORDS;

typedef struct
{
   DOCANA_COORDS        coords;
   DOCANA_IMGREGION     imgRgn[10];
   DOCANA_IMGSTATINFO   statInfo[10];
   DOCANA_IMGHISTOGRAM  histogramInfo[10];
}
   DOCANA_STATINFO_RESULT, *PDOCANA_STATINFO_RESULT, **PPDOCANA_STATINFO_RESULT;

typedef struct
{
   DOCANA_COORDS        coords;
   DOCANA_IMGREGION     imgRgn[10];
   DOCANA_DERIVATA_2    der2[10];
}
   DOCANA_DERIVATA2_RESULT, *PDOCANA_DERIVATA2_RESULT, **PPDOCANA_DERIVATA2_RESULT;
  
// ** analyzer routine

LIBDOCANA_API int __stdcall docana_getStatInfo( 
   const PDOCANA_AREA_2_EVAL pArea,          // [ IN ] - rule areas to evaluate
   const PBITMAPINFO pBmp,                   // [ IN ] - bitmap pointer
   const DOCANA_MIRROR_TYPE mt,              // [ IN ] - set mirror on bitmap
   const DOCANA_RESOLUTION res,              // [ IN ] - set image resolution
   PDOCANA_STATINFO_RESULT pRes );           // [ IN/OUT ] - analysis results

LIBDOCANA_API int __stdcall docana_getDerivata2( 
   const PDOCANA_AREA_2_EVAL pArea,          // [ IN ] - rule areas to evaluate
   const PBITMAPINFO pBmp,                   // [ IN ] - bitmap pointer
   const DOCANA_MIRROR_TYPE mt,              // [ IN ] - set mirror on bitmap
   const DOCANA_RESOLUTION res,              // [ IN ] - set image resolution
   PDOCANA_DERIVATA2_RESULT pRes );          // [ IN/OUT ] - analysis results pointer

LIBDOCANA_API void __stdcall docana_releaseDerivata2(
   PDOCANA_DERIVATA2_RESULT pRes);           // [ IN/OUT ] - analysis results pointer to free

// ** filters parameters

LIBDOCANA_API int __stdcall docana_addFilters(
   const DOCANA_FILTER_METHOD methodName);   // [ IN ]

LIBDOCANA_API int __stdcall docana_clearFilters(void);

// **

LIBDOCANA_API int __stdcall docana_Version( 
   char szVersion[],                         // [ IN/OUT ]
   int buffLength );                         // [ IN ]

LIBDOCANA_API int __stdcall docana_GetLastError( 
   char szBuffErr[],                         // [ IN/OUT ]
   int buffLength );                         // [ IN ]

LIBDOCANA_API int __stdcall docana_GetLastErrorLength( 
   void );

#endif