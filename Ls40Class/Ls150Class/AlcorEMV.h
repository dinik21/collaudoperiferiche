

#include <windows.h>
#include <winscard.h>

#ifdef __cplusplus
//#define EXPORT	extern "C" __declspec (dllexport)
#else
#define EXPORT __declspec (dllexport)
#endif

//Code for switch card mode
#define ASYNCHRONOUS_CARD_MODE					0x01
#define	I2C_CARD_MODE							0x02
#define	SYNCHRONOUS_CARD_SLE4428_MODE			0x03
#define	SYNCHRONOUS_CARD_SLE4442_MODE			0x04
#define	AT88SC_CARD_MODE						0x05
#define	INPHONE_CARD_MODE						0x06


//Codes for SLE4418/28
#define	CODE_WRITE_ERASE_WITH_PB				0x31
#define	CODE_WRITE_ERASE_WITHOUT_PB				0x33
#define	CODE_WRITE_PB_WITH_DATA_COMPARISON		0x30
#define	CODE_READ_9BITS							0x0C
#define	CODE_READ_8BITS							0x0E
// the following 2 codes are only for SLE4428
#define	CODE_WRITE_ERROR_COUNTER				0x32
#define	CODE_VERIFY_1ST_PSC_BYTE				0x0D
#define	CODE_VERIFY_2ND_PSC_BYTE				0x0D

//Code for SLE4432/42
#define CODE_READ_MAIN_MEMORY					0x30
#define CODE_UPDATE_MAIN_MEMORY					0x38
#define CODE_READ_PROTECTION_MEMORY				0x34
#define CODE_WRITE_PROTECTION_MEMORY			0x3C
#define CODE_READ_SECURITY_MEMORY				0x31
#define CODE_UPDATE_SECURITY_MEMORY				0x39
#define CODE_COMPARE_VERIFICATION_DATA			0x33

#define	SLE4428_CMD_SIZE						0x06
#define	SLE4442_CMD_SIZE						0x05

//Code for AT88SC1608
#define	CODE_WRITE_USER_ZONE					0xB0
#define	CODE_READ_USER_ZONE						0xB1
#define	CODE_WRITE_CONFIGURATION_ZONE			0xB4
#define	CODE_READ_CONFIGURATION_ZONE			0xB5
#define	CODE_SET_USER_ZONE_ADDRESS				0xB2
#define	CODE_VERIFY_PASSWORD					0xB3
#define	CODE_INITIALIZE_AUTHENTICATION			0xB6
#define	CODE_VERIFY_AUTHENTICATION				0xB7


/* Alcor vendor command */
#define CMD_ALCOR_HEADER_LENGTH					0x08
#define CMD_ALCOR_OP_CODE						0x40
// SWITCH CARD MODE
#define CMD_SWITCH_CARD_MODE					0x50
// POWER ON
#define CMD_POWER_ON							0x51
#define CMD_POWER_OFF							0x52
// I2C
#define	CMD_SET_I2C_ADD							0x60
#define CMD_WRITE_I2C							0x61
#define CMD_READ_I2C							0x62
// AT45D041
#define CMD_AT45D01_HEADER_LENGTH				0x04
#define CMD_AT45D01_COMMAND						0x64
// SMC AT88xxx
#define CMD_SMC_HEADER_LENGTH					0x02
#define CMD_SMC_COMMAND							0x70
// SLE4442
#define CMD_SLE4442_CARD_HEADER_LENGTH			0x03
#define	CMD_SLE4442_CARD_COMMAND				0x80
#define	CMD_SLE4442_CARD_BREAK					0x81
// SLE4428
#define CMD_SLE4428_CARD_HEADER_LENGTH			0x03
#define	CMD_SLE4428_CARD_COMMAND				0x82
// INPHONE CARD
#define CMD_INPHONE_CARD_RESET					0x90
#define	CMD_INPHONE_CARD_READ					0x91
#define	CMD_INPHONE_CARD_PROG					0x92
#define CMD_SWITCH_CERTIFICATION_MODE			0xA1
#define AlcorSCD_IOCTL_INDEX  0x0800

#define IOCTL_SWITCH_CARD_MODE	   		   CTL_CODE(FILE_DEVICE_SMARTCARD,  \
                                                   AlcorSCD_IOCTL_INDEX+9,\
                                                   METHOD_BUFFERED,  \
                                                   FILE_ANY_ACCESS)


#define IOCTL_I2C_WRITE						   CTL_CODE(FILE_DEVICE_SMARTCARD,  \
                                                   AlcorSCD_IOCTL_INDEX+10,\
                                                   METHOD_BUFFERED,  \
                                                   FILE_ANY_ACCESS)
#define IOCTL_I2C_READ						   CTL_CODE(FILE_DEVICE_SMARTCARD,  \
                                                   AlcorSCD_IOCTL_INDEX+11,\
                                                   METHOD_BUFFERED,  \
                                                   FILE_ANY_ACCESS)

#define IOCTL_SMC_WRITEREAD					   CTL_CODE(FILE_DEVICE_SMARTCARD,  \
                                                   AlcorSCD_IOCTL_INDEX+12,\
                                                   METHOD_BUFFERED,  \
                                                   FILE_ANY_ACCESS)


#define IOCTL_SEND_SLE4442_CARD_COMMAND	   	   CTL_CODE(FILE_DEVICE_SMARTCARD,  \
                                                   AlcorSCD_IOCTL_INDEX+13,\
                                                   METHOD_BUFFERED,  \
                                                   FILE_ANY_ACCESS)
#define IOCTL_GET_SLE4442_CARD_ATR 	   	   	CTL_CODE(FILE_DEVICE_SMARTCARD,  \
                                                   AlcorSCD_IOCTL_INDEX+14,\
                                                   METHOD_BUFFERED,  \
                                                   FILE_ANY_ACCESS)
#define IOCTL_SLE4442_CARD_BREAK	   	   CTL_CODE(FILE_DEVICE_SMARTCARD,  \
                                                   AlcorSCD_IOCTL_INDEX+15,\
                                                   METHOD_BUFFERED,  \
                                                   FILE_ANY_ACCESS)

//#define IOCTL_SWITCH_TO_SLE4428_CARD	   	   CTL_CODE(FILE_DEVICE_SMARTCARD,  \
//                                                  AlcorSCD_IOCTL_INDEX+30,\
//                                                   METHOD_BUFFERED,  \
//                                                   FILE_ANY_ACCESS)

#define IOCTL_SEND_SLE4428_CARD_COMMAND	   	   CTL_CODE(FILE_DEVICE_SMARTCARD,  \
                                                   AlcorSCD_IOCTL_INDEX+16,\
                                                   METHOD_BUFFERED,  \
                                                   FILE_ANY_ACCESS)

#define IOCTL_SMARTCARD_CCID_REQUEST		   CTL_CODE(FILE_DEVICE_SMARTCARD,  \
                                                   AlcorSCD_IOCTL_INDEX+17,\
                                                   METHOD_BUFFERED,  \
                                                   FILE_ANY_ACCESS)

#define	IOCTL_SET_CARD_MODE			  		   CTL_CODE(FILE_DEVICE_SMARTCARD,  \
                                                   AlcorSCD_IOCTL_INDEX+18,\
                                                   METHOD_BUFFERED,  \
                                                   FILE_ANY_ACCESS)

#define	IOCTL_AT45D01_COMMAND		  		   CTL_CODE(FILE_DEVICE_SMARTCARD,  \
                                                   AlcorSCD_IOCTL_INDEX+19,\
                                                   METHOD_BUFFERED,  \
                                                   FILE_ANY_ACCESS)

#define	IOCTL_CCID_ESCAPE		  		   CTL_CODE(FILE_DEVICE_SMARTCARD,  \
                                                   3500,\
                                                   METHOD_BUFFERED,  \
                                                   FILE_ANY_ACCESS)




//#define	IOCTL_SET_PCSC_MODE			   CTL_CODE(FILE_DEVICE_SMARTCARD,  \
//                                                   AlcorSCD_IOCTL_INDEX+16,\
//                                                   METHOD_BUFFERED,  \
//                                                   FILE_ANY_ACCESS)


//EXPORT	LONG   AlcorMCardRead(	IN	LONG			lngCard//,
//								IN	LONG			ucDeviceAddr,
//								IN	LONG		usStartAddr,
//								IN	LONG			usReadLen,
//								OUT	LPVOID			pReadData
//								);


LONG APIENTRY Alcor_SwitchCardMode(
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	UCHAR	bCardMode
		);
		
LONG APIENTRY Alcor_PowerOn(
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	ULONG	lngReadLen,
		OUT	LPVOID	pReadData,
		OUT	ULONG	*plngReturnLen
		);

LONG APIENTRY Alcor_PowerOff(
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum
		);

LONG APIENTRY AT24CxxCmd_Write(
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	ULONG	lngDeviceAddr,
		IN	ULONG	lngStartAddr,
		IN	ULONG	lngWordPageSize,
		IN	ULONG	lngWriteLen,
		IN	LPVOID	pWriteData
		);

LONG APIENTRY AT24CxxCmd_Read(
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	ULONG	lngDeviceAddr,
		IN	ULONG	lngStartAddr,
		IN	ULONG	lngReadLen,
		OUT	LPVOID	pReadData,
		OUT	ULONG	*plngReturnLen
		);

LONG APIENTRY SLE4428Cmd_WriteEraseWithPB(
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	ULONG	lngAddress,
		IN	UCHAR	bData
		);

LONG APIENTRY SLE4428Cmd_WriteEraseWithoutPB(
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	ULONG	lngAddress,
		IN	UCHAR	bData
		);

LONG APIENTRY SLE4428Cmd_WritePBWithDataComparison(
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	ULONG	lngAddress,
		IN	UCHAR	bData
		);
		
LONG APIENTRY SLE4428Cmd_Read9Bits(
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	ULONG	lngAddress,
		IN	ULONG	lngReadLen,
		OUT	LPVOID	pReadData,
		OUT LPVOID	pReadPB,
		OUT	ULONG	*plngReturnLen
		);

LONG APIENTRY SLE4428Cmd_Read8Bits(
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	ULONG	lngAddress,
		IN	ULONG	lngReadLen,
		OUT	LPVOID	pReadData,
		OUT	ULONG	*plngReturnLen
		);

LONG APIENTRY SLE4428Cmd_WriteErrorCounter(
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	UCHAR	bData
		);

LONG APIENTRY SLE4428Cmd_Verify1stPSC(
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	UCHAR	bData
		);

LONG APIENTRY SLE4428Cmd_Verify2ndPSC(
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	UCHAR	bData
		);

LONG APIENTRY SLE4442Cmd_ReadMainMemory(
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	UCHAR	bAddress,
		IN	UCHAR	bReadLen,
		OUT	LPVOID	pReadData,
		OUT	ULONG	*pbReturnLen
		);
		
LONG APIENTRY SLE4442Cmd_UpdateMainMemory(
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	UCHAR	bAddress,
		IN	UCHAR	bData
		);

LONG APIENTRY SLE4442Cmd_ReadProtectionMemory(
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	UCHAR	bReadLen,
		OUT	LPVOID	pReadData,
		OUT	ULONG	*pbReturnLen
		);

LONG APIENTRY SLE4442Cmd_WriteProtectionMemory(
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	UCHAR	bAddress,
		IN	UCHAR	bData
		);

LONG APIENTRY SLE4442Cmd_ReadSecurityMemory(
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	UCHAR	bReadLen,
		OUT	LPVOID	pReadData,
		OUT	ULONG	*pbReturnLen
		);

LONG APIENTRY SLE4442Cmd_UpdateSecurityMemory(
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	UCHAR	bAddress,
		IN	UCHAR	bData
		);

LONG APIENTRY SLE4442Cmd_CompareVerificationData(
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	UCHAR	bAddress,
		IN	UCHAR	bData
		);

LONG APIENTRY AT88SC1608Cmd_WriteUserZone(
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	UCHAR	bAddress,
		IN	UCHAR	bWriteLen,
		IN	LPVOID	pWriteBuffer
		);

LONG APIENTRY AT88SC1608Cmd_ReadUserZone(
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	UCHAR	bAddress,
		IN	UCHAR	bReadLen,
		OUT	LPVOID	pReadBuffer,
		OUT	UCHAR	*pReturnLen
		);

LONG APIENTRY AT88SC1608Cmd_WriteConfigurationZone(
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	UCHAR	bAddress,
		IN	UCHAR	bWriteLen,
		IN	LPVOID	pWriteBuffer
		);

LONG APIENTRY AT88SC1608Cmd_ReadConfigurationZone(
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	UCHAR	bAddress,
		IN	UCHAR	bReadLen,
		OUT	LPVOID	pReadBuffer,
		OUT	UCHAR	*pReturnLen
		);

LONG APIENTRY AT88SC1608Cmd_SetUserZoneAddress(
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	UCHAR	bAddress
		);

LONG APIENTRY AT88SC1608Cmd_VerifyPassword(
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	UCHAR	bZoneNo,
		IN	BOOL	bIsReadAccess,
		IN	UCHAR	bPW1,
		IN	UCHAR	bPW2,
		IN	UCHAR	bPW3
		);
		
LONG APIENTRY AT88SC1608Cmd_InitializeAuthentication(
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	UCHAR	*pbHostRand
		);
		
LONG APIENTRY AT88SC1608Cmd_VerifyAuthentication(
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	UCHAR	*pbHostChallenge
		);
				
LONG APIENTRY AT45D041Cmd(
		IN  UCHAR	OPcode,
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	int 	PageNo,
		IN	int 	lngStartAddr,
		IN	ULONG	lngWriteLen,
		IN	LPVOID	pWriteData,
		IN	ULONG	lngReadLen,
		OUT	LPVOID	pReadData,
		OUT	ULONG	*plngReturnLen
		);

LONG APIENTRY AT88SC1608Cmd(
		IN  UCHAR	OPcode,
		IN	LONG	lngCard,
		IN	UCHAR	bSlotNum,
		IN	UCHAR	bAddress,
		IN	UCHAR	bWriteLen,
		IN	LPVOID	pWriteData,
		IN	UCHAR	bReadLen,
		OUT	LPVOID	pReadData,
		OUT	ULONG	*plngReturnLen
		);




		