#include "StdAfx.h"
#include "Constanti.h"

Ls40Class::Costanti::Costanti()
{
	
}



String ^Ls40Class::Costanti::GetDesc(int ReturnCode)
{
			String ^Description;
			switch( ReturnCode)
			{
			case S_LS_SERVERSYSERR:
			//case S_LS_SYSTEM_ERROR:	
				Description = "LS_SERVERSYSERR";
					break;
			case S_LS_USBERR:
					Description = "LS_USBERR";
					break;
			case S_LS_PERIFNOTFOUND:
					Description = "LS_PERIFNOTFOUND";
					break;
			case S_LS_HARDERR:
					Description = "LS_HARDERR";
					break;	
			
			case S_LS_CODELINE_ERROR:
					Description = "LS_CODELINE_ERROR";
					break;
				
				case S_LS_PAPER_JAM:
					Description = "LS_PAPER_JAM";
					break;
				case S_LS_TARGET_BUSY:
					Description = "LS_TARGET_BUSY";
					break;
				case S_LS_INVALIDCMD:
					Description = "LS_INVALIDCMD";
					break;
				case S_LS_DATALOST:
					Description = "LS_DATALOST";
					break;
				case S_LS_EXECCMD:
					Description = "LS_EXECCMD";
					break;
				case S_LS_JPEGERROR:
					Description = "LS_JPEGERROR";
					break;
				case S_LS_CMDSEQUENCEERROR:
					Description = "LS_CMDSEQUENCEERROR";
					break;
				case S_LS_NOTUSED:
					Description = "LS_NOTUSED";
					break;
				case S_LS_IMAGEOVERWRITE:
					Description = "LS_IMAGEOVERWRITE,Spegnere e riaccendere il computer....";
					break;
				case S_LS_INVALID_HANDLE:
					Description = "LS_INVALID_HANDLE";
					break;
				case S_LS_NO_LIBRARY_LOAD:
					Description = "LS_NO_LIBRARY_LOAD";
					break;
				case S_LS_BMP_ERROR:
					Description = "LS_BMP_ERROR";
					break;
				case S_LS_TIFFERROR:
					Description = "LS_TIFFERROR";
					break;
				case S_LS_IMAGE_NO_MORE_AVAILABLE:
					Description = "LS_IMAGE_NO_MORE_AVAILABLE";
					break;
				case S_LS_IMAGE_NO_FILMED:
					Description = "LS_IMAGE_NO_FILMED";
					break;
				case S_LS_IMAGE_NOT_PRESENT:
					Description = "LS_IMAGE_NOT_PRESENT";
					break;
				case S_LS_FUNCTION_NOT_AVAILABLE:
					Description = "LS_FUNCTION_NOT_AVAILABLE";
					break;
				case S_LS_DOCUMENT_NOT_SUPPORTED:
					Description = "LS_DOCUMENT_NOT_SUPPORTED";
					break;
				case S_LS_BARCODE_ERROR:
					Description = "LS_BARCODE_ERROR";
					break;
				case S_LS_INVALID_LIBRARY:
					Description = "LS_INVALID_LIBRARY";
					break;
				case S_LS_INVALID_IMAGE:
					Description = "LS_INVALID_IMAGE";
					break;
				case S_LS_INVALID_IMAGE_FORMAT:
					Description = "LS_INVALID_IMAGE";
					break;
				case S_LS_INVALID_BARCODE_TYPE:
					Description = "LS_INVALID_BARCODE_TYPE";
					break;
				case S_LS_OPEN_NOT_DONE:
					Description = "LS_OPEN_NOT_DONE";
					break;
				case S_LS_INVALID_TYPE_CMD:
					Description = "LS_INVALID_TYPE_CMD";
					break;
				case S_LS_INVALID_CLEARBLACK:
					Description = "LS_INVALID_CLEARBLACK";
					break;
				case S_LS_INVALID_SIDE:
					Description = "LS_INVALID_SIDE";
					break;
				case S_LS_MISSING_IMAGE:
					Description = "LS_MISSING_IMAGE";
					break;
				case S_LS_INVALID_TYPE:
					Description = "LS_INVALID_TYPE";
					break;
				case S_LS_INVALID_SAVEMODE:
					Description = "LS_INVALID_SAVEMODE";
					break;
				case S_LS_INVALID_PAGE_NUMBER:
					Description = "LS_INVALID_PAGE_NUMBER";
					break;
				case S_LS_INVALID_NRIMAGE:
					Description = "LS_INVALID_NRIMAGE";
					break;
				case S_LS_INVALID_FRONTSTAMP:
					Description = "LS_INVALID_FRONTSTAMP";
					break;
				case S_LS_INVALID_WAITTIMEOUT:
					Description = "LS_INVALID_WAITTIMEOUT";
					break;
				case S_LS_INVALID_VALIDATE:
					Description = "LS_INVALID_VALIDATE";
					break;
				case S_LS_INVALID_CODELINE_TYPE:
					Description = "LS_INVALID_CODELINE_TYPE";
					break;
				case S_LS_MISSING_NRIMAGE:
					Description = "LS_MISSING_NRIMAGE";
					break;
				case S_LS_INVALID_SCANMODE:
					Description = "LS_INVALID_SCANMODE";
					break;
				case S_LS_INVALID_BEEP_PARAM:
					Description = "LS_INVALID_BEEP_PARAM";
					break;
				case S_LS_INVALID_FEEDER_PARAM:
					Description = "LS_INVALID_FEEDER_PARAM";
					break;
				case S_LS_INVALID_SORTER_PARAM:
					Description = "LS_INVALID_SORTER_PARAM";
					break;
				case S_LS_INVALID_BADGE_TRACK:
					Description = "LS_INVALID_BADGE_TRACK";
					break;
				case S_LS_MISSING_FILENAME:
					Description = "LS_MISSING_FILENAME";
					break;
				case S_LS_INVALID_QUALITY:
					Description = "LS_INVALID_QUALITY";
					break;
				case S_LS_INVALID_FILEFORMAT:
					Description = "LS_INVALID_FILEFORMAT";
					break;
				case S_LS_INVALID_COORDINATE:
					Description = "LS_INVALID_COORDINATE";
					break;
				case S_LS_MISSING_HANDLE_VARIABLE:
					Description = "LS_MISSING_HANDLE_VARIABLE";
					break;
				case S_LS_INVALID_POLO_FILTER:
					Description = "LS_INVALID_POLO_FILTER";
					break;
				case S_LS_INVALID_ORIGIN_MEASURES:
					Description = "LS_INVALID_ORIGIN_MEASURES";
					break;
				case S_LS_INVALID_SIZEH_VALUE:
					Description = "LS_INVALID_SIZEH_VALUE";
					break;
				case S_LS_INVALID_FORMAT:
					Description = "LS_INVALID_FORMAT";
					break;
				case S_LS_STRINGS_TOO_LONGS:
					Description = "LS_STRINGS_TOO_LONGS";
					break;
				case S_LS_READ_IMAGE_FAILED:
					Description = "LS_READ_IMAGE_FAILED";
					break;
				
				case S_LS_MISSING_BUFFER_HISTORY:
					Description = "LS_MISSING_BUFFER_HISTORY";
					break;
				case S_LS_INVALID_ANSWER:
					Description = "LS_INVALID_ANSWER";
					break;
				case S_LS_OPEN_FILE_ERROR_OR_NOT_FOUND:
					Description = "LS_OPEN_FILE_ERROR_OR_NOT_FOUND";
					break;
				case S_LS_READ_TIMEOUT_EXPIRED:
					Description = "LS_READ_TIMEOUT_EXPIRED";
					break;
				case S_LS_INVALID_METHOD:
					Description = "LS_INVALID_METHOD";
					break;
				case S_LS_CALIBRATION_FAILED:
					Description = "LS_CALIBRATION_FAILED";
					break;
				case S_LS_INVALID_SAVEIMAGE:
					Description = "LS_INVALID_SAVEIMAGE";
					break;
				case S_LS_UNIT_PARAM:
					Description = "LS_UNIT_PARAM";
					break;
				case S_LS_INVALID_NRWINDOWS:
					Description = "LS_INVALID_NRWINDOWS";
					break;
				case S_LS_INVALID_VALUE:
					Description = "LS_INVALID_VALUE";
					break;
				case S_LS_INVALID_DEGREE:
					Description = "LS_INVALID_DEGREE";
					break;
				case S_LS_R0TATE_ERROR:
					Description = "LS_R0TATE_ERROR";
					break;
				case S_LS_PERIPHERAL_RESERVED:
					Description = "LS_PERIPHERAL_RESERVED";
					break;
				case S_LS_INVALID_NCHANGE:
					Description = "LS_INVALID_NCHANGE";
					break;
				case S_LS_BRIGHTNESS_ERROR:
					Description = "LS_BRIGHTNESS_ERROR";
					break;
				case S_LS_CONTRAST_ERROR:
					Description = "LS_CONTRAST_ERROR";
					break;
				case S_LS_DOUBLE_LEAFING_ERROR:
					Description = "LS_DOUBLE_LEAFING_ERROR";
					break;
				case S_LS_INVALID_BADGE_TIMEOUT:
					Description = "LS_INVALID_BADGE_TIMEOUT";
					break;
				case S_LS_INVALID_RESET_TYPE:
					Description = "LS_INVALID_RESET_TYPE";
					break;
				case S_LS_IMAGE_NOT_200_DPI:
					Description = "LS_IMAGE_NOT_200_DPI";
					break;

				case S_LS_DECODE_FONT_NOT_PRESENT:
					Description = "LS_DECODE_FONT_NOT_PRESENT";
					break;
				case S_LS_DECODE_INVALID_COORDINATE:
					Description = "LS_DECODE_INVALID_COORDINATE";
					break;
				case S_LS_DECODE_INVALID_OPTION:
					Description = "LS_DECODE_INVALID_OPTION";
					break;
				case S_LS_DECODE_INVALID_CODELINE_TYPE:
					Description = "LS_DECODE_INVALID_CODELINE_TYPE";
					break;
				case S_LS_DECODE_SYSTEM_ERROR:
					Description = "LS_DECODE_SYSTEM_ERROR";
					break;
				case S_LS_DECODE_DATA_TRUNC:
					Description = "LS_DECODE_DATA_TRUNC";
					break;
				case S_LS_DECODE_INVALID_BITMAP:
					Description = "LS_DECODE_INVALID_BITMAP";
					break;
				case S_LS_DECODE_ILLEGAL_USE:
					Description = "LS_DECODE_ILLEGAL_USE";
					break;
				case S_LS_BARCODE_GENERIC_ERROR:
					Description = "LS_BARCODE_GENERIC_ERROR";
					break;
				case S_LS_BARCODE_NOT_DECODABLE:
					Description = "LS_BARCODE_NOT_DECODABLE";
					break;
				case S_LS_BARCODE_OPENFILE_ERROR:
					Description = "LS_BARCODE_OPENFILE_ERROR";
					break;
				case S_LS_BARCODE_READBMP_ERROR:
					Description = "LS_BARCODE_READBMP_ERROR";
					break;
				case S_LS_BARCODE_MEMORY_ERROR:
					Description = "LS_BARCODE_MEMORY_ERROR";
					break;
				case S_LS_BARCODE_START_NOTFOUND:
					Description = "LS_BARCODE_START_NOTFOUND";
					break;
				case S_LS_BARCODE_STOP_NOTFOUND:
					Description = "LS_BARCODE_STOP_NOTFOUND";
					break;
				case S_LS_PDF_NOT_DECODABLE:
					Description = "LS_PDF_NOT_DECODABLE";
					break;
				case S_LS_PDF_READBMP_ERROR:
					Description = "LS_PDF_READBMP_ERROR";
					break;
				case S_LS_PDF_BITMAP_FORMAT_ERROR:
					Description = "LS_PDF_BITMAP_FORMAT_ERROR";
					break;
				case S_LS_PDF_MEMORY_ERROR:
					Description = "LS_PDF_MEMORY_ERROR";
					break;
				case S_LS_PDF_START_NOTFOUND:
					Description = "LS_PDF_START_NOTFOUND";
					break;
				case S_LS_PDF_STOP_NOTFOUND:
					Description = "LS_PDF_STOP_NOTFOUND";
					break;
				case S_LS_PDF_LEFTIND_ERROR:
					Description = "LS_PDF_LEFTIND_ERROR";
					break;
				case S_LS_PDF_RIGHTIND_ERROR:
					Description = "LS_PDF_RIGHTIND_ERROR";
					break;
				case S_LS_PDF_OPENFILE_ERROR:
					Description = "LS_PDF_OPENFILE_ERROR";
					break;
				case S_LS_USER_ABORT:
					Description = "S_LS_USER_ABORT";
					break;
				case S_LS_JAM_AT_MICR_PHOTO:
					Description = "S_LS_JAM_AT_MICR_PHOTO";
					break;
				case S_LS_JAM_DOC_TOOLONG:
					Description = "S_LS_JAM_DOC_TOO_LONG";
					break;
				case S_LS_JAM_AT_SCANNERPHOTO:
					Description = "S_LS_JAM_AT_SCANNER_PHOTO";
					break;
				//12/10/2015
				case S_LS_MICR_TRIMMER_VALUE_NEGATIVE:
					Description = "Micr Trimmer Value Negative";
					break;
				case S_LS_ERROR_OFFSET_FRONT:
					Description = "Error Offset front";
					break;
				case S_LS_ERROR_OFFSET_REAR:
					Description = "Error Offset rear";
					break;
				case S_LS_ERROR_SCANNER_PWM:
					Description = "Error Scanne PWM";
					break;
				case S_LS_ERROR_GAIN_FRONT:
					Description = "Error Gain Front";
					break;
				case S_LS_ERROR_GAIN_REAR:
					Description = "Error Gain Rear";
					break;
				case S_LS_ERROR_COEFF_FRONT:
					Description = "Error Coeff Front";
					break;
				case S_LS_ERROR_COEFF_REAR:
					Description = "Error Coeff Rear";
					break;
				case S_LS_ERROR_SCANNER_GENERICO:
					Description = "Error Scanner Generico";
					break;




				// ------------------------------------------------------------------------
				//                  WARNINGS
				// ------------------------------------------------------------------------
				case S_LS_FEEDEREMPTY:
					Description = "LS_FEEDEREMPTY";
					break;
				case S_LS_DATATRUNC:
					Description = "LS_DATATRUNC";
					break;
				case S_LS_DOCPRESENT:
					Description = "LS_DOCPRESENT";
					break;
				case S_LS_BADGETIMEOUT:
					Description = "LS_BADGETIMEOUT";
					break;
				case S_LS_PERIF_BUSY:
					Description = "LS_PERIF_BUSY";
					break;
				case S_LS_NO_ENDCMD:
					Description = "LS_NO_ENDCMD";
					break;
				case S_LS_RETRY:
					Description = "LS_RETRY";
					break;
				case S_LS_NO_OTHER_DOCUMENT:
					Description = "LS_NO_OTHER_DOCUMENT";
					break;
				case S_LS_QUEUEFULL:
					Description = "LS_QUEUEFULL";
					break;
				case S_LS_NOSENSE:
					Description = "LS_NOSENSE";
					break;
				case S_LS_TRY_TO_RESET:
					Description = "LS_TRY_TO_RESET";
					break;
				case S_LS_STRINGTRUNCATED:
					Description = "LS_STRINGTRUNCATED";
					break;
				case S_LS_COMMAND_NOT_SUPPORTED:
					Description = "LS_COMMAND_NOT_SUPPORTED";
					break;
				case S_LS_LOOP_INTERRUPTED:
					Description = "LS_LOOP_INTERRUPTED";
					break;
				
				case	S_LS40_FPGA_NOT_OK :
					Description = "S_LS40_FPGA_NOT_OK";
					break;
				case	S_LS40_BOARD_NOT_OK :
					Description = "S_LS40_BOARD_NOT_OK";
					break;
				case	S_LS40_DATA_NOT_OK :
					Description = "S_LS40_DATA_NOT_OK";
					break;
				case	S_LS40_FIRMWARE_NOT_OK :
					Description = "S_LS40_FIRMWARE_NOT_OK";
					break;
				case	S_LS40_FEEDER_NOT_OK :
					Description = "S_LS40_FEEDER_NOT_OK";
					break;
				case	S_LS40_BADGE_NOT_CONFIGURED	:
					Description = "S_LS40_BADGE_NOT_CONFIGURED";
					break;
				case	S_LS40_TIMBRO_NOT_CONFIGURED :
					Description = "S_LS40_TIMBRO_NOT_CONFIGURED";
					break;
				case	S_LS40_CONDENS_NOT_CONFIGURED :
					Description = "S_LS40_CONDENS_NOT_CONFIGURED";
					break;
				case	S_LS40_INK_HW_ERROR	:
					Description = "S_LS40_INK_HW_ERROR";
					break;
				case	S_LS40_INK__NOT_CONFIGURED	:
					Description = "S_LS40_INK__NOT_CONFIGURED";
					break;
				case	S_LS40_CARD__NOT_CONFIGURED	:
					Description = "S_LS40_CARD__NOT_CONFIGURED";
					break;
				case	S_LS40_MICR__NOT_CONFIGURED	:
					Description = "S_LS40_MICR__NOT_CONFIGURED";
					break;
				case  S_LS_ERROR_US_CURRENT_0	: 
					Description = "Corrente Foto 0 in Errore";
					break ; 
				case  S_LS_ERROR_US_CURRENT_1	: 
					Description = "Corrente Foto 1 in Errore";
					break ; 
				case  S_LS_ERROR_US_CURRENT_2	: 
					Description = "Corrente Foto 2 in Errore";
					break ; 
				case  S_LS_ERROR_US_FLY_TIME: 
					Description = "Fly Time Error" ; 
					break ; 
				case  S_LS_ERROR_US_PWM	: 
					Description = "Pwm Error";
					break ; 
				case  S_LS_ERROR_US_SENSOR_AVG	: 
					Description = "Error Sensor AVG";
					break ; 
				case  S_LS_ERROR_US_EMPTY_PEAK_MIN	: 
					Description = "S_LS_ERROR_US_EMPTY_PEAK_MIN";
					break ; 
				case  S_LS_ERROR_US_STATIC_DOC_AVG	: 
					Description = "S_LS_ERROR_US_STATIC_DOC_AVG";
					break ; 
				case  S_LS_ERROR_US_STATIC_DOC_ST_DEV	: 
					Description = "S_LS_ERROR_US_STATIC_DOC_ST_DEV";
					break ;
				case  S_LS_ERROR_US_DYN_DOC_MIN	: 
					Description = "S_LS_ERROR_US_DYN_DOC_MIN";
					break ;
				case  S_LS_ERROR_US_DYN_DOC_AVG	: 
					Description = "S_LS_ERROR_US_DYN_DOC_AVG";
					break ;
				case  S_LS_ERROR_US_DYN_DOC_ST_DEV	: 
					Description = "S_LS_ERROR_US_DYN_DOC_ST_DEV"; 
					break ;
				case  S_LS_ERROR_US_NOISE_MAX	: 
					Description = "S_LS_ERROR_US_NOISE_MAX"; 
					break ;
				case  S_LS_ERROR_US_NOISE_AVG	: 
					Description = "S_LS_ERROR_US_NOISE_AVG" ; 
					break ;
				case  S_LS_ERROR_US_NOISE_ST_DEV	: 
					Description = "S_LS_ERROR_US_NOISE_ST_DEV" ; 
					break ;
				case  S_LS_ERROR_US_CALIBRATE_0x10	: 
					Description = "S_LS_ERROR_US_CALIBRATE_0x10"; 
					break ;
				case  S_LS_ERROR_US_CALIBRATE_0x01	: 
					Description = "S_LS_ERROR_US_CALIBRATE_0x01"; 
					break ;
				case  S_LS_ERROR_US_CALIBRATE_0x02	: 
					Description = "S_LS_ERROR_US_CALIBRATE_0x02"; 
					break ;
				case  S_LS_ERROR_US_CALIBRATE_0x03	: 
					Description = "S_LS_ERROR_US_CALIBRATE_0x03" ; 
					break ;
				case  S_LS_ERROR_US_CHECKLIST	: 
					Description = "S_LS_ERROR_US_CHECKLIST"; 
					break ;

			}
			return Description;
		}