#pragma once

using namespace System;

 
namespace Ls40Class
{
	/// <summary> 
	/// Riepilogo per Constanti
	/// </summary>
	//__gc public class Costanti 
	public ref class Costanti
	{

	public:
		Costanti();
		String ^GetDesc(int ReturnCode);


// ------------------------------------------------------------------------
//                     DEFINES
// ------------------------------------------------------------------------

// Parameter : Type_com
//static const int char  S_SUSPENSIVE_MODE			=	'S';
//static const int char S_NOT_SUSPENSIVE_MODE		=	'T';


// Parameter : FrontStamp
static const  int  S_NO_FRONT_STAMP				=0;
static const  int S_FRONT_STAMP			=		1;


// Parameter : BackStamp
static const  int S_NO_BACK_STAMP				=0;
static const int S_BACK_STAMP				=	1;


// Parameter : Validate
static const int S_NO_PRINT_VALIDATE		=	0;
static const int S_PRINT_VALIDATE			=	1;


// Parameter : Feed
static const int S_AUTOFEED					=0;
static const int S_SCANFEED				=	1;


// Scanner choise
static const int SCANNER_FRONT			=	1;
static const int SCANNER_BACK			=	2;

static const int SCANNER_GRAY			=   0;
static const int SCANNER_UV				=	2;
static const int SCANNER_UVGR			=	3;
static const int SCANNER_COLORE			=	4;




// Parameter : CODELINE
static const int S_NO_READ_CODELINE	=		0;
static const int S_READ_CODELINE_MICR	=		1;
static const int S_READ_CODELINE_CMC7		=	1;
static const int S_READ_BARCODE_PDF417		=	2;
static const int S_READ_CODELINE_E13B		=	4;
static const int S_READ_BARCODE			=	6;
static const int S_READ_CMC7_AND_BARCODE	=	7;
static const int S_READ_E13B_AND_BARCODE	=	14;

static const int S_READ_BARCODE_2_OF_5		=	50;
static const int S_READ_BARCODE_CODE39	=		51;
static const int S_READ_BARCODE_CODE128	=	52;
static const int S_READ_BARCODE_EAN13		=		53;

static const int S_READ_OPTIC				=		21;
static const int S_READ_OPTIC_AND_CMC7	=			22;
static const int S_READ_OPTIC_AND_E13B	=			23;
static const int S_READ_OPTIC_AND_BARCODE	=		24;
static const int S_READ_OPTIC_CMC7_AND_BARCODE	=	25;
static const int S_READ_OPTIC_E13B_AND_BARCODE	=	26;

static const int S_READ_CODELINE_SW_OCRA		=	0x41;	//'A'
static const int S_READ_CODELINE_SW_OCRB_NUM	=	0x42;	//'B'
static const int S_READ_CODELINE_SW_OCRB_ALFANUM	=0x43;	//'C'
static const int S_READ_CODELINE_SW_OCRB_ITALY	=	0x46;	//'F'
static const int S_READ_CODELINE_SW_E13B	=		0x45;	//'E'
static const int S_READ_CODELINE_SW_E13B_X_OCRB	= 0x58;	//'X'
static const int S_READ_CODELINE_SW_MULTI_READ	=	0x4d;	//'M'

static const int S_READ_ONE_CODELINE_TYPE		=	0x4e;	//'N'

static const int S_READ_OPTIC_OCRA			=		0x41;	//'A'
static const int S_READ_OPTIC_OCRB			=		0x42;	//'B'
static const int S_READ_OPTIC_OCRB_FULL		=	0x43;	//'C'
static const int S_READ_OPTIC_E13B			=		0x45;	//'E'
static const int S_READ_OPTIC_MULTIFONTS		=	0x4d;	//'M'
static const int S_READ_OPTIC_E13B_X_SWITCH	=	0x58;	//'X'

// old define leave for compatibility DO NOT USE
static const int S_READ_CODELINE_SOFTWARE		=	5;

static const int S_READ_BAR_CODE				=	6;
static const int S_READ_CMC7_AND_BAR_CODE		=	7;
static const int S_READ_E13B_AND_BAR_CODE		=	14;

static const int S_READ_CODELINE_SW_LEFT		=	8;
static const int S_READ_CODELINE_SW_RIGHT		=	9;

static const int S_READ_CODELINE_SW_KZ21_L	=		10;
static const int S_READ_CODELINE_SW_KZ23_L	=		11;

static const int S_READ_CODELINE_SW_KZ21_R	=		12;
static const int S_READ_CODELINE_SW_KZ23_R	=		13;


// Parameter OriginMeasureDoc
static const int S_BOTTOM_LEFT_PIXEL			=	0;
static const int S_BOTTOM_RIGHT_MM			=		10;
static const int S_BOTTOM_RIGHT_INCH			=	20;


// Parameter Unit
static const int S_UNIT_MM					=		0;
static const int S_UNIT_INCH					=	1;


// Value of height to decode a software Codeline
static const int S_MAX_PIXEL_HEIGHT			=	42;
static const float S_OCR_VALUE_IN_MM			=		10.5f;
static const float S_OCR_VALUE_IN_INCH			=	0.41f;


// Parameter OCR_Image_Side
static const int S_OCR_FRONT_IMAGE			=		0;
static const int S_OCR_BACK_IMAGE				=	1;


// Parameter Sorter
static const int S_HOLD_DOCUMENT				=	0;
static const int S_SORTER_BAY1				=		1;
static const int S_SORTER_BAY2				=		2;
static const int S_SORTER_AUTOMATIC			=	3;
static const int S_SORTER_SWITCH_1_TO_2		=	4;
static const int S_SORTER_ON_CODELINE_CALLBACK =		6;


// Parameter ResetType
static const int S_RESET_ERROR				=		0x30;
static const int S_RESET_FREE_PATH			=		0x31;




// Parametro ScanMode


// Parameter ScanMode
static const int S_SCAN_MODE_BW				=	1;
static const int S_SCAN_MODE_16GR100			=	2;
static const int S_SCAN_MODE_16GR200			=	3;
static const int S_SCAN_MODE_256GR100			=	4;
static const int S_SCAN_MODE_256GR200			=	5;
static const int S_SCAN_MODE_COLOR_100			=	10;
static const int S_SCAN_MODE_COLOR_200			=	11;
static const int S_SCAN_MODE_COLOR_AND_RED_100	=	12;
static const int S_SCAN_MODE_COLOR_AND_RED_200	=	13;
static const int S_SCAN_MODE_16GR300			=	20;
static const int S_SCAN_MODE_256GR300			=	21;
static const int S_SCAN_MODE_16GR240			=	30;
static const int S_SCAN_MODE_256GR240			=	31;

static const int S_SCAN_MODE_256GR100_AND_UV		=	40;
static const int S_SCAN_MODE_256GR200_AND_UV		=	41;
static const int S_SCAN_MODE_256GR300_AND_UV		=	42;


// Parameter : ClearBlack
static const int S_NO_CLEAR_BLACK			=		10;
static const int S_CLEAR_UP_BLACK			=		11;
static const int S_CLEAR_ALL_BLACK		=			12;

// Old value leave for compatibility
static const int S_CLEARBLACK	 			=		0;
static const int S_NO_CLEARBLACK			=		1;

// Parameter Threshold
static const int S_DEFAULT_BLACK_THRESHOLD	=		0x44;


// Parameter : Side
static const char  S_SIDE_NONE_IMAGE			=		'N';
static const char S_SIDE_FRONT_IMAGE			=	'F';
static const char S_SIDE_BACK_IMAGE			=		'B';
static const char S_SIDE_ALL_IMAGE				=	'X';
//static const char S_SIDE_FRONT_BLUE_IMAGE		=	'G';
//static const char S_SIDE_BACK_BLUE_IMAGE		=	'C';
//static const char S_SIDE_ALL_BLUE_IMAGE		=		'Y';
//static const char S_SIDE_FRONT_GREEN_IMAGE		=	'H';
//static const char S_SIDE_BACK_GREEN_IMAGE		=	'D';
//static const char S_SIDE_ALL_GREEN_IMAGE		=	'W';
//static const char S_SIDE_FRONT_RED_IMAGE		=	'I';
//static const char S_SIDE_BACK_RED_IMAGE		=		'E';
//static const char  S_SIDE_ALL_RED_IMAGE			=	'Z';


// Parameter : ReadMode
static const int S_READMODE_BRUTTO			=		0;
static const int S_READMODE_NETTO				=	1;
static const int S_READMODE_ALL				=	2;


// Parameter Image Coordinate
static const int S_IMAGE_MAX_WIDTH			=		1720;
static const int S_IMAGE_MAX_HEIGHT			=	848;


// Parameter : METHOD
static const int S_WINDOW_ALGORITHM			=		1;
static const int S_PERCENT_ALGORITHM			=		2;
static const int S_DYNAMIC_THRESHOLD_ALGORITHM   =	3;
static const int S_ALGORITHM_CTS					=	4;
static const int S_ALGORITHM_NODITHERING			=	0x10;
static const int S_ALGORITHM_FLOYDSTEINDITHERING	=	0x11;
static const int S_ALGORITHM_STUCKIDITHERING		=	0x12;
static const int S_ALGORITHM_BURKESDITHERING		=	0x13;
static const int S_ALGORITHM_SIERRADITHERING		=	0x14;
static const int S_ALGORITHM_STEVENSONARCEDITHERING =	0x15;
static const int S_ALGORITHM_JARVISDITHERING		=	0x16;

static const int S_DEFAULT_POLO_FILTER			=		450;


// Parameter : Format
static const char S_FORMAT_NORMAL			=		'N';
static const char S_FORMAT_BOLD			=			'B';
static const char S_FORMAT_NORMAL_15_CHAR	=		'A';


// Parameter : Badge
static const char S_FORMAT_ABA				=		'A';
static const char S_FORMAT_MINTS			=		'M';
static const char S_FORMAT_ABA_MINTS		=		'B'	;	// Boths


// Parameter : Wait_com
static const char S_WAIT_YES				=		'W';
static const char S_WAIT_NO				=			'G';


//Parameter : Beep
static const int S_NO_BEEP				=			0;
static const int S_BEEP				=			1;


//Parameter : SaveOnFile
static const int S_IMAGE_SAVE_ON_FILE	=			4;
static const int S_IMAGE_SAVE_HANDLE	=			5;
static const int S_IMAGE_SAVE_BOTH		=			6;
static const int S_IMAGE_SAVE_NONE		=			7;


//Parameter : FileFormat
static const int S_SAVE_JPEG			=			10;
static const int S_SAVE_BMP			=			11;

// Parameter: Tiff type
static const int S_FILE_TIF				=		3;		// Tagged Image File Format
static const int S_FILE_CCITT				=		25;		// TIFF  CCITT
static const int S_FILE_CCITT_GROUP3_1DIM	=		27;		// CCITT Group3 one dimension
static const int S_FILE_CCITT_GROUP3_2DIM	=		28;		// CCITT Group3 two dimensions
static const int S_FILE_CCITT_GROUP4		=		29;		// CCITT Group4 two dimensions


// Parameter: uSaveMulti
static const int S_SAVE_OVERWRITE			=		0;
static const int S_SAVE_APPEND			=		1;
static const int S_SAVE_REPLACE			=		2;
static const int S_SAVE_INSERT			=		3;


// Parameter Sorter Criteria only for LS510
static const int S_CRITERIA_NO					=		0x00;
static const int S_CRITERIA_ERROR_IN_CODELINE		=		0x01;
static const int S_CRITERIA_CODELINE_EQUAL_STR1	=		0x02;
static const int S_CRITERIA_CODELINE_DIFF_STR1	=		0x03;
static const int S_CRITERIA_CODELINE_GREAT_STR1	=		0x04;
static const int S_CRITERIA_CODELINE_MIN_STR1		=		0x05;
static const int S_CRITERIA_CODELINE_INTO_STR1_STR2	=	0x06;
static const int S_CRITERIA_CODELINE_OUT_STR1_STR2	=	0x07;
static const int S_CRITERIA_CODELINE_EQUAL_STR1_OR_STR2 =	0x08;
static const int S_CRITERIA_CODELINE_DIFF_STR1_AND_STR2 =	0x09;
static const int S_CRITERIA_CODELINE_NOT_PRESENT		=	0x0a;


// Parameter Double Leafing only for LS510
static const int S_DOUBLE_LEAFING_WARNING		=	0;
static const int S_DOUBLE_LEAFING_ERROR		=	1;

static const int S_DOUBLE_LEAFING_STORAGE		=	10;
static const int S_DOUBLE_LEAFING_VOLATILE	=	11;

static const int S_DOUBLE_LEAFING_LEVEL1		=	0x01;
static const int S_DOUBLE_LEAFING_LEVEL2		=	0x02;
static const int S_DOUBLE_LEAFING_LEVEL3		=	0x03;
static const int S_DOUBLE_LEAFING_DEFAULT		=	0x04;
static const int S_DOUBLE_LEAFING_LEVEL4		=	0x05;
static const int S_DOUBLE_LEAFING_LEVEL5		=	0x06;


// Parameter History
static const int S_CMD_READ_HISTORY			=	1;
static const int S_CMD_ERASE_HISTORY			=	2;


static const int S_CODE_LINE_LENGTH		=	256;

static const int S_MAX_OPTICAL_WINDOWS		=		5;		// Nr. window * 5 bytes per window
static const int S_MAX_CRITERIA				=	5;		// Nr. max of selection criteria
static const int S_MAX_CHAR_CHECK				=	10;		// Nr. max of check char

static const int S_PERIPHERAL_SIZE_MEMORY		=	64 * 1024;	//Total memory of the peripheral





#if 0 
// String for identify the periferal connected
static const char String S_MODEL_LS500				=		"C.T.S.  LS500";
static const char S_MODEL_LS505				=		"C.T.S.  LS505";
static const char S_MODEL_LS510S				=	"C.T.S.  LS510S";
static const char S_MODEL_LS510D				=	"C.T.S.  LS510D";
static const char S_MODEL_LS515				=		"C.T.S.  LS515";
static const char S_MODEL_LS515S				=	"C.T.S.  LS515S";
static const char S_MODEL_LS515_1				=	"C.T.S.  LS515/1";
static const char S_MODEL_LS515_2				=	"C.T.S.  LS515/2";
static const char S_MODEL_LS515_3				=	"C.T.S.  LS515/3";
static const char S_MODEL_LS515_5				=	"C.T.S.  LS515/5";
static const char S_MODEL_LS515_6				=	"C.T.S.  LS515/6";
static const char S_MODEL_LS520				=		"C.T.S.  LS520";

#endif 



// ------------------------------------------------------------------------
//                          REPLY-CODE
// ------------------------------------------------------------------------

static const int 		S_LS_OKAY				=			0;

// ------------------------------------------------------------------------
//                  ERRORS
// ------------------------------------------------------------------------
static const int 		S_LS_SERVERSYSERR		=			-1;
static const int 		S_LS_USBERR			=			-2;
static const int 		S_LS_PERIFNOTFOUND		=			-3;
static const int 		S_LS_HARDERR			=			-4;
static const int 		S_LS_PERIFOFFON		=			-5;
static const int 		S_LS_CODELINE_ERROR	=			-6;
static const int 		S_LS_BOURRAGE			=			-7;
static const int 		S_LS_PAPER_JAM			=			-7;
static const int 		S_LS_TARGET_BUSY		=			-8;
static const int 		S_LS_INVALIDCMD		=			-9;
static const int 		S_LS_DATALOST			=			-10;
static const int 		S_LS_EXECCMD			=			-11;
static const int 		S_LS_JPEGERROR			=			-12;
static const int 		S_LS_CMDSEQUENCEERROR	=			-13;
static const int 		S_LS_NOTUSED			=			-14;
static const int 		S_LS_IMAGEOVERWRITE	=			-15;
static const int 		S_LS_INVALID_HANDLE	=			-16;
static const int 		S_LS_NO_LIBRARY_LOAD	=			-17;
static const int 		S_LS_BMP_ERROR			=			-18;
static const int 		S_LS_TIFFERROR			=			-19;
static const int 		S_LS_IMAGE_NO_MORE_AVAILABLE	=	-20;
static const int 		S_LS_IMAGE_NO_FILMED	=			-21;
static const int 		S_LS_IMAGE_NOT_PRESENT	=			-22;
static const int 		S_LS_FUNCTION_NOT_AVAILABLE	=	-23;
static const int 		S_LS_DOCUMENT_NOT_SUPPORTED	=	-24;
static const int 		S_LS_BARCODE_ERROR				=	-25;
static const int 		S_LS_INVALID_LIBRARY			=	-26;
static const int 		S_LS_INVALID_IMAGE				=	-27;
static const int 		S_LS_INVALID_IMAGE_FORMAT		=	-28;
static const int 		S_LS_INVALID_BARCODE_TYPE		=	-29;
static const int 		S_LS_OPEN_NOT_DONE				=	-30;
static const int 		S_LS_INVALID_TYPE_CMD			=	-31;
static const int 		S_LS_INVALID_CLEARBLACK		=	-32;
static const int 		S_LS_INVALID_SIDE				=	-33;
static const int 		S_LS_MISSING_IMAGE				=	-34;
static const int 		S_LS_INVALID_TYPE				=	-35;
static const int 		S_LS_INVALID_SAVEMODE			=	-36;
static const int 		S_LS_INVALID_PAGE_NUMBER		=	-37;
static const int 		S_LS_INVALID_NRIMAGE			=	-38;
static const int 		S_LS_INVALID_FRONTSTAMP		=	-39;
static const int 		S_LS_INVALID_WAITTIMEOUT		=	-40;
static const int 		S_LS_INVALID_VALIDATE			=	-41;
static const int 		S_LS_INVALID_CODELINE_TYPE		=	-42;
static const int 		S_LS_MISSING_NRIMAGE			=	-43;
static const int 		S_LS_INVALID_SCANMODE			=	-44;
static const int 		S_LS_INVALID_BEEP_PARAM		=	-45;
static const int 		S_LS_INVALID_FEEDER_PARAM		=	-46;
static const int 		S_LS_INVALID_SORTER_PARAM		=	-47;
static const int		S_LS_INVALID_BADGE_TRACK		=	-48;
static const int		S_LS_MISSING_FILENAME			=	-49;
static const int		S_LS_INVALID_QUALITY			=	-50;
static const int		S_LS_INVALID_FILEFORMAT		=	-51;
static const int		S_LS_INVALID_COORDINATE		=	-52;
static const int		S_LS_MISSING_HANDLE_VARIABLE	=	-53;
static const int		S_LS_INVALID_POLO_FILTER		=	-54;
static const int		S_LS_INVALID_ORIGIN_MEASURES	=	-55;
static const int		S_LS_INVALID_SIZEH_VALUE		=	-56;
static const int		S_LS_INVALID_FORMAT			=	-57;
static const int		S_LS_STRINGS_TOO_LONGS			=	-58;
static const int 		S_LS_READ_IMAGE_FAILED			=	-59;
static const int 		S_LS_INVALID_CMD_HISTORY		=	-60;
static const int		S_LS_MISSING_BUFFER_HISTORY	=	-61;
static const int		S_LS_INVALID_ANSWER			=	-62;
static const int		S_LS_OPEN_FILE_ERROR_OR_NOT_FOUND	=-63;
static const int	    S_LS_READ_TIMEOUT_EXPIRED		=	-64;
static const int	    S_LS_INVALID_METHOD			=	-65;
static const int 		S_LS_CALIBRATION_FAILED		=	-66;
static const int 		S_LS_INVALID_SAVEIMAGE			=	-67;
static const int 		S_LS_UNIT_PARAM				=	-68;
static const int 		S_LS_INVALID_NRWINDOWS			=	-71;
static const int 		S_LS_INVALID_VALUE				=	-72;
static const int 		S_LS_INVALID_DEGREE			=	-77;
static const int 		S_LS_R0TATE_ERROR				=	-78;
static const int 		S_LS_PERIPHERAL_RESERVED		=	-80;
static const int 		S_LS_INVALID_NCHANGE			=	-81;
static const int 		S_LS_BRIGHTNESS_ERROR			=	-82;
static const int 		S_LS_CONTRAST_ERROR			=	-83;
static const int 		S_LS_DOUBLE_LEAFING_ERROR		=	-85;
static const int 		S_LS_INVALID_BADGE_TIMEOUT		=	-86;
static const int 		S_LS_INVALID_RESET_TYPE		=	-87;
static const int 		S_LS_IMAGE_NOT_200_DPI			=	-89;

static const int		S_LS_JAM_AT_MICR_PHOTO = -201;
static const int		S_LS_JAM_DOC_TOOLONG = -202;
static const int		S_LS_JAM_AT_SCANNERPHOTO = -203;

static const int S_LS_ERROR_US_CURRENT_0		=	-310;
static const int S_LS_ERROR_US_CURRENT_1		=	-311;
static const int S_LS_ERROR_US_CURRENT_2		=	-312;
static const int S_LS_ERROR_US_FLY_TIME		=	-313;
static const int S_LS_ERROR_US_PWM			=		-314;
static const int S_LS_ERROR_US_SENSOR_AVG		=	-315;
static const int S_LS_ERROR_US_EMPTY_PEAK_MIN	=	-316;
static const int S_LS_ERROR_US_STATIC_DOC_AVG	=	-317;
static const int S_LS_ERROR_US_STATIC_DOC_ST_DEV=	-318;
static const int S_LS_ERROR_US_DYN_DOC_MIN	=		-319;
static const int S_LS_ERROR_US_DYN_DOC_AVG	=		-320;
static const int S_LS_ERROR_US_DYN_DOC_ST_DEV	=	-321;
static const int S_LS_ERROR_US_NOISE_MAX		=	-322;
static const int S_LS_ERROR_US_NOISE_AVG		=	-323;
static const int S_LS_ERROR_US_NOISE_ST_DEV	=	-324;
static const int S_LS_ERROR_US_CALIBRATE_0x10	=	-325;
static const int S_LS_ERROR_US_CALIBRATE_0x01	=	-326;
static const int S_LS_ERROR_US_CALIBRATE_0x02	=	-327;
static const int S_LS_ERROR_US_CALIBRATE_0x03	=	-328;
static const int S_LS_ERROR_US_CHECKLIST		=	-329;

static const int		S_LS_MICR_TRIMMER_VALUE_NEGATIVE	=	-300;
static const int		S_LS_ERROR_OFFSET_FRONT		=		-348;
static const int		S_LS_ERROR_OFFSET_REAR		=		-349;
static const int		S_LS_ERROR_SCANNER_PWM		=		-350;
static const int		S_LS_ERROR_GAIN_FRONT		=			-351;
static const int		S_LS_ERROR_GAIN_REAR		=			-352;
static const int		S_LS_ERROR_COEFF_FRONT		=		-353;
static const int		S_LS_ERROR_COEFF_REAR		=			-354;
static const int		S_LS_ERROR_SCANNER_GENERICO		=	-363;

static const int 		S_LS_DECODE_FONT_NOT_PRESENT	=	-1101;
static const int 		S_LS_DECODE_INVALID_COORDINATE	=	-1102;
static const int 		S_LS_DECODE_INVALID_OPTION		=	-1103;
static const int 		S_LS_DECODE_INVALID_CODELINE_TYPE=	-1104;
static const int 		S_LS_DECODE_SYSTEM_ERROR		=	-1105;
static const int 		S_LS_DECODE_DATA_TRUNC			=	-1106;
static const int 		S_LS_DECODE_INVALID_BITMAP		=	-1107;
static const int 		S_LS_DECODE_ILLEGAL_USE		=	-1108;

static const int 		S_LS_BARCODE_GENERIC_ERROR		=	-1201;
static const int 		S_LS_BARCODE_NOT_DECODABLE		=	-1202;
static const int 		S_LS_BARCODE_OPENFILE_ERROR	=	-1203;
static const int 		S_LS_BARCODE_READBMP_ERROR		=	-1204;
static const int 		S_LS_BARCODE_MEMORY_ERROR		=	-1205;
static const int 		S_LS_BARCODE_START_NOTFOUND	=	-1206;
static const int 		S_LS_BARCODE_STOP_NOTFOUND		=	-1207;

static const int 		S_LS_PDF_NOT_DECODABLE			=	-1301;
static const int 		S_LS_PDF_READBMP_ERROR			=	-1302;
static const int 		S_LS_PDF_BITMAP_FORMAT_ERROR	=	-1303;
static const int 		S_LS_PDF_MEMORY_ERROR			=	-1304;
static const int 		S_LS_PDF_START_NOTFOUND		=	-1305;
static const int 		S_LS_PDF_STOP_NOTFOUND			=	-1306;
static const int 		S_LS_PDF_LEFTIND_ERROR			=	-1307;
static const int 		S_LS_PDF_RIGHTIND_ERROR		=	-1308;
static const int 		S_LS_PDF_OPENFILE_ERROR		=	-1309;

static const int		S_LS_USER_ABORT				=	-9999;


static const int		S_LS40_FPGA_NOT_OK				= -5000;
static const int		S_LS40_BOARD_NOT_OK				= -5001;
static const int		S_LS40_DATA_NOT_OK				= -5002;
static const int		S_LS40_FIRMWARE_NOT_OK			= -5003;
static const int		S_LS40_FEEDER_NOT_OK			= -5004;



static const int		S_LS40_BADGE_NOT_CONFIGURED			= -5005;
static const int		S_LS40_TIMBRO_NOT_CONFIGURED		= -5006;
static const int		S_LS40_CONDENS_NOT_CONFIGURED		= -5007;
static const int		S_LS40_INK_HW_ERROR					= -5008;
static const int		S_LS40_INK__NOT_CONFIGURED			= -5009;
static const int		S_LS40_CARD__NOT_CONFIGURED			= -5010;
static const int		S_LS40_MICR__NOT_CONFIGURED			= -5011;

static const int		S_L40_FRONT_BACKGROUND				= -5012;
static const int		S_L40_FRONT_BACKGROUND_1			= -5013;
static const int		S_L40_FRONT_BACKGROUND_2			= -5014;

static const int		S_L40_BACK_BACKGROUND				= -5015;
static const int		S_L40_BACK_BACKGROUND_1				= -5016;
static const int		S_L40_BACK_BACKGROUND_2				= -5017;











// ------------------------------------------------------------------------
//                  WARNINGS
// ------------------------------------------------------------------------
static const int 		S_LS_FEEDEREMPTY				=	1;
static const int 		S_LS_DATATRUNC					=	2;
static const int 		S_LS_DOCPRESENT				=	3;
static const int 		S_LS_BADGETIMEOUT				=	4;
static const int 		S_LS_ALREADY_OPEN				=	5;
static const int 		S_LS_PERIF_BUSY				=	6;
static const int 		S_LS_NO_ENDCMD					=	8;
static const int 		S_LS_RETRY						=	9;
static const int 		S_LS_NO_OTHER_DOCUMENT			=	10;
static const int 		S_LS_QUEUEFULL					=	11;
static const int 		S_LS_NOSENSE					=	12;
static const int 		S_LS_TRY_TO_RESET				=	14;
static const int 		S_LS_STRINGTRUNCATED			=	15;
static const int 		S_LS_COMMAND_NOT_SUPPORTED		=	19;
static const int 		S_LS_LOOP_INTERRUPTED			=	40;



		
	protected: 
		
				
	private:
		
	};
}
