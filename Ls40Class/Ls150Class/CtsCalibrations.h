///************************************************************************
//  MODULE:   CTSCALIBRATIONS.H
//
//  PURPOSE:  Defines 
//            Structures and defines used in the modules.
//            This file must be included in all source files
//            need for the calibrations.
//
//
//	  Ultima modifica: 03 Aprile 2007
//
///************************************************************************

#ifndef CTSCALIBRATIONS_H
	#define CTSCALIBRATIONS_H     1



// ------------------------------------------------------------------------
// Definizione di ERROR CODES di periferica convertiti
// ------------------------------------------------------------------------
#define		CTS_ERROR_OFFSET_FRONT		-3000
#define		CTS_ERROR_OFFSET_REAR		-3001
#define		CTS_ERROR_PWM				-3002
#define		CTS_ERROR_GAIN_FRONT		-3003
#define		CTS_ERROR_GAIN_REAR			-3004
#define		CTS_ERROR_COEFF_FRONT		-3005
#define		CTS_ERROR_COEFF_REAR		-3006



// ------------------------------------------------------------------------
// Definizione di ERROR CODES ritornati da periferica
// ------------------------------------------------------------------------
#define ERROR_OFFSET_FRONT				0x30
#define ERROR_OFFSET_REAR				0x31
#define ERROR_PWM						0x32
#define ERROR_GAIN_FRONT				0x33
#define ERROR_GAIN_REAR					0x34
#define ERROR_COEFF_FRONT				0x35
#define ERROR_COEFF_REAR				0x36



// ------------------------------------------------------------------------
//                  STRUCTURES
// ------------------------------------------------------------------------

#pragma pack(1)

typedef struct                             // Dati PWM
{
    unsigned char Add_PWM_Blue_F;
    unsigned char Dato_PWM_Blue_F;
    unsigned char Add_PWM_Green_F;
    unsigned char Dato_PWM_Green_F;
    unsigned char Add_PWM_Red_F;
    unsigned char Dato_PWM_Red_F;
    unsigned char Dato_Offset_F;
    unsigned char Dato_Gain_F;
 
    unsigned char Add_PWM_Blue_R;
    unsigned char Dato_PWM_Blue_R;
    unsigned char Add_PWM_Green_R;
    unsigned char Dato_PWM_Green_R;
    unsigned char Add_PWM_Red_R;
    unsigned char Dato_PWM_Red_R;
    unsigned char Dato_Offset_R;
    unsigned char Dato_Gain_R;
 
    unsigned char OutMap_0;
    unsigned char OutMap_1;
    unsigned char OutMap_2;
    unsigned char OutMap_3;
 
} AD_LM98714;

#pragma pack(4)



// -------------------------------------------------------------
// CTS Reserved function
// -------------------------------------------------------------
#ifdef __cplusplus
extern "C" {
#endif


// -------------------------------------------------------------
// CTS LS150 calibration function
// -------------------------------------------------------------
extern int APIENTRY LS150_OpenDiagnostica(HWND, HANDLE, CHAR);

extern int APIENTRY LS150_ReadE13BSignal(HWND, unsigned char *, long *);

extern int APIENTRY LS150_PhotosCalibration(HWND, short);

extern int APIENTRY LS150_ReadPhotosValue(HWND hWnd, unsigned short *Value, unsigned short *Threshold);

extern int APIENTRY LS150_DumpPeripheralMemory(HWND, unsigned char *, long, long, char);

extern int APIENTRY LS150_ReadTrimmerMICR(HWND, char, unsigned char *, short);

extern int APIENTRY LS150_BuilderSetting(HWND, void *);

extern int APIENTRY LS150_SetConfiguration(HWND, char *);

extern int APIENTRY LS150_SetTrimmerMICR(HWND, char, short);

extern int APIENTRY LS150_ImageCalibration(HWND, BOOL, short *);

extern int APIENTRY LS150_SetSerialNumber(HWND, unsigned char *, short);

extern int APIENTRY LS150_SetScannerLight(HWND, short);

extern int APIENTRY LS150_ScannerCalibration(HWND, short);

extern int APIENTRY LS150_ScannerReadValue(HWND hWnd, char *pColonne, AD_LM98714 *sScanner, char fluci);

extern int APIENTRY LS150_ScannerWriteValue(HWND hWnd, AD_LM98714 *sScanner);

extern int APIENTRY LS150_ReadTimeSampleMICR(HWND, char, unsigned short *, short);

extern int APIENTRY LS150_SetTimeSampleMICR(HWND, char, unsigned short);

extern int APIENTRY LS150_MoveMotorFeeder(HWND, short);

extern int APIENTRY LS150_ReadFeederMotorAD(HWND, unsigned short *);

extern int APIENTRY LS150_TestCinghia(HWND, unsigned char *, long *);

#ifdef __cplusplus
}
#endif



#endif