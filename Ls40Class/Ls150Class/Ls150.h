#include<windows.h>

//
// CTS Electronics
// Corso Vercelli, 332 - 10015 Ivrea (TORINO) Italy
// January 2001
//
// Phone ++39 125 235611
// Telefax ++39 125 235623
//
// www.ctsgroup.it		techsupp@ctsgroup.it
//
//
// All rights reserved
//
// We reserve the right to vary technical specification
//
//  MODULE:   LS150.H
//
//  PURPOSE:  LS150 Include Interface


#ifndef LS150_H
#define LS150_H     1



// ------------------------------------------------------------------------
//                     DEFINES
// ------------------------------------------------------------------------

// Parameter Type_com
#define SUSPENSIVE_MODE					'S'
#define NOT_SUSPENSIVE_MODE				'T'


// Parameter FrontStamp
#define NO_FRONT_STAMP					0
#define FRONT_STAMP						1


// Parameter Validate
#define NO_PRINT_VALIDATE				0
#define PRINT_VALIDATE					1
#define PRINT_DIGITAL_VALIDATE			2


// Parameter Feed
#define AUTOFEED						0
#define PATHFEED						1


// Parameter Sorter
#define HOLD_DOCUMENT					0
#define SORTER_BAY1						1


// Parameter Codeline
#define NO_READ_CODELINE				0
#define READ_CODELINE_MICR				1
#define READ_BARCODE_PDF417				2
#define READ_CODELINE_OPTIC				21
#define READ_MICR_AND_OPTIC				22

#define READ_CODELINE_HW_OCRA			0x41	//'A'
#define READ_CODELINE_HW_OCRB_NUM		0x42	//'B'
#define READ_CODELINE_HW_OCRB_ALFANUM	0x43	//'C'
#define READ_CODELINE_HW_E13B			0x45	//'E'
#define READ_CODELINE_HW_OCRB_ITALY		0x46	//'F'
#define READ_CODELINE_HW_E13B_X_OCRB	0x58	//'X'
#define READ_CODELINE_HW_MULTI_READ		0x4d	//'M'

#define READ_BARCODE_2_OF_5				50
#define READ_BARCODE_CODE39				51
#define READ_BARCODE_CODE128			52
#define READ_BARCODE_EAN13				53

#define READ_CODELINE_SW_OCRA			0x41	//'A'
#define READ_CODELINE_SW_OCRB_NUM		0x42	//'B'
#define READ_CODELINE_SW_OCRB_ALFANUM	0x43	//'C'
#define READ_CODELINE_SW_OCRB_ITALY		0x46	//'F'
#define READ_CODELINE_SW_E13B			0x45	//'E'

#define READ_ONE_CODELINE_TYPE			0x4e	//'N'
#define READ_CODELINE_SW_MULTI_READ		0x4d	//'M'
#define READ_CODELINE_SW_E13B_X_OCRB	0x58	//'X'


// Parameter OriginMeasureDoc
#define BOTTOM_RIGHT_MM					10
#define BOTTOM_RIGHT_INCH				20


// Parameter Unit
#define UNIT_MM							0
#define UNIT_INCH						1


// OCR Hardware configuration
#define OCRH_BALNKS_NO					0x00
#define OCRH_BLANKS_YES					0x01


// Value of height to decode a software Codeline
#define MAX_PIXEL_HEIGHT				42
#define OCR_VALUE_IN_MM					10.5
#define OCR_VALUE_IN_INCH				0.41


// Parameter ResetType
#define RESET_ERROR						0x30
#define RESET_FREE_PATH					0x31
#define RESET_BELT_CLEANING				0x32


// Parameter ScanMode
#define SCAN_MODE_BW					1
#define SCAN_MODE_16GR100				2
#define SCAN_MODE_16GR200				3
#define SCAN_MODE_256GR100				4
#define SCAN_MODE_256GR200				5
#define SCAN_MODE_COLOR_100				10
#define SCAN_MODE_COLOR_200				11
#define SCAN_MODE_COLOR_AND_RED_100		12
#define SCAN_MODE_COLOR_AND_RED_200		13
#define SCAN_MODE_16GR300				20
#define SCAN_MODE_256GR300				21
#define SCAN_MODE_16GR240				30
#define SCAN_MODE_256GR240				31
#define SCAN_MODE_16GR120				34
#define SCAN_MODE_256GR120				35


// Parameter ClearBlack
#define NO_CLEAR_BLACK					0
#define CLEAR_ALL_BLACK 				1
#define CLEAR_AND_ALIGN_IMAGE			2

// Parameter Threshold
#define DEFAULT_BLACK_THRESHOLD			0x44


// Parameter Side
#define SIDE_NONE_IMAGE					'N'
#define SIDE_FRONT_IMAGE				'F'
#define SIDE_BACK_IMAGE					'B'
#define SIDE_ALL_IMAGE					'X'
#define SIDE_FRONT_BLUE_IMAGE			'G'
#define SIDE_BACK_BLUE_IMAGE			'C'
#define SIDE_ALL_BLUE_IMAGE				'Y'
#define SIDE_FRONT_GREEN_IMAGE			'H'
#define SIDE_BACK_GREEN_IMAGE			'D'
#define SIDE_ALL_GREEN_IMAGE			'W'
#define SIDE_FRONT_RED_IMAGE			'I'
#define SIDE_BACK_RED_IMAGE				'E'
#define SIDE_ALL_RED_IMAGE				'Z'


// Parameter Image Coordinate
#define IMAGE_MAX_WIDTH					1720
#define IMAGE_MAX_HEIGHT				848


// Parameter Method
#define ALGORITHM_CTS						4
#define ALGORITHM_CTS_2						5
#define ALGORITHM_NODITHERING				0x10
#define ALGORITHM_FLOYDSTEINDITHERING		0x11
#define ALGORITHM_STUCKIDITHERING			0x12
#define ALGORITHM_BURKESDITHERING			0x13
#define ALGORITHM_SIERRADITHERING			0x14
#define ALGORITHM_STEVENSONARCEDITHERING	0x15
#define ALGORITHM_JARVISDITHERING			0x16

#define DEFAULT_POLO_FILTER					450


// Parameter Format
#define FORMAT_NORMAL						'N'
#define FORMAT_BOLD							'B'
#define FORMAT_NORMAL_15_CHAR				'A'

#define PRINT_UP_FORMAT_NORMAL				'n'
#define PRINT_UP_FORMAT_BOLD				'b'
#define PRINT_UP_FORMAT_NORMAL_15_CHAR		'a'


// Parameter Badge
#define FORMAT_IATA						0x20	// Badge Track 1
#define FORMAT_ABA						0x40	// Badge Track 2
#define FORMAT_MINTS					0x80	// Badge Track 3
#define FORMAT_IATA_ABA					0x60	// Badge Track 1 and 2
#define FORMAT_ABA_MINTS				0xc0	// Badge Track 2 and 3
#define FORMAT_IATA_ABA_MINTS			0xe0	// Badge Track 1, 2 and 3


// Parameter Timeout
#define MIN_TIMEOUT						500


// Parameter Wait_com
#define WAIT_NO							'G'
#define WAIT_YES						'W'


//Parameter Beep
#define	NO_BEEP							0
#define	BEEP							1


//Parameter SaveMode
#define	IMAGE_SAVE_ON_FILE				4
#define	IMAGE_SAVE_HANDLE				5
#define	IMAGE_SAVE_BOTH					6
#define	IMAGE_SAVE_NONE					7


//Parameter FileFormat
#define	SAVE_JPEG						10
#define	SAVE_BMP						11


// Parameter Tiff type
#define FILE_TIF						3		// Tagged Image File Format
#define FILE_CCITT						25		// TIFF  CCITT
#define FILE_CCITT_GROUP3_1DIM			27		// CCITT Group3 one dimension
#define FILE_CCITT_GROUP3_2DIM			28		// CCITT Group3 two dimensions
#define FILE_CCITT_GROUP4				29		// CCITT Group4 two dimensions

// Parameter uSaveMulti
#define SAVE_OVERWRITE					0
#define SAVE_APPEND						1
#define SAVE_REPLACE					2
#define SAVE_INSERT						3

// Parameter Double Leafing
#define DOUBLE_LEAFING_WARNING			0
#define DOUBLE_LEAFING_ERROR			1

#define DOUBLE_LEAFING_LEVEL2			2
#define DOUBLE_LEAFING_LEVEL3			3
#define DOUBLE_LEAFING_DEFAULT			4
#define DOUBLE_LEAFING_LEVEL4			5
#define DOUBLE_LEAFING_LEVEL5			6
#define DOUBLE_LEAFING_DISABLE			7


// Parameter UnitSpeed
#define SPEED_DEFAULT					0
#define SPEED_STAMP						1


// History choise
#define CMD_READ_HISTORY				1
#define CMD_ERASE_HISTORY				2


#define	CODE_LINE_LENGTH				256		// Max length of returned codeline

#define MAX_OPTICAL_WINDOWS				1		// Nr. window * 5 bytes per window

#define PERIPHERAL_SIZE_MEMORY			36 * 1024	//Total memory of the peripheral


// String for identify the periferal connected
#define MODEL_LS150					    "LS150"



// ------------------------------------------------------------------------
//                          REPLY-CODE
// ------------------------------------------------------------------------

#define		LS150_OKAY							0

// ------------------------------------------------------------------------
//                  ERRORS
// ------------------------------------------------------------------------
#define		LS150_SYSTEM_ERROR					-1
#define		LS150_USB_ERROR						-2
#define		LS150_PERIPHERAL_NOT_FOUND			-3
#define		LS150_HARDWARE_ERROR				-4
#define		LS150_PERIPHERAL_OFF_ON				-5
#define		LS150_CODELINE_ERROR				-6
#define		LS150_PAPER_JAM						-7
#define		LS150_TARGET_BUSY					-8
#define		LS150_INVALID_COMMAND				-9
#define		LS150_COMMAND_IN_EXECUTION_YET		-11
#define		LS150_JPEG_ERROR					-12
#define		LS150_COMMAND_SEQUENCE_ERROR		-13
#define		LS150_INVALID_HANDLE				-16
#define		LS150_NO_LIBRARY_LOAD				-17
#define		LS150_BMP_ERROR						-18
#define		LS150_TIFF_ERROR					-19
#define		LS150_IMAGE_NO_FILMED				-21
#define		LS150_IMAGE_NOT_PRESENT				-22
#define		LS150_DOCUMENT_NOT_SUPPORTED		-24
#define		LS150_BARCODE_ERROR					-25
#define		LS150_INVALID_LIBRARY				-26
#define		LS150_INVALID_IMAGE					-27
#define		LS150_INVALID_IMAGE_FORMAT			-28
#define		LS150_INVALID_BARCODE_TYPE			-29
#define		LS150_OPEN_NOT_DONE					-30
#define		LS150_INVALID_TYPE_COMMAND			-31
#define		LS150_INVALID_CLEARBLACK			-32
#define		LS150_INVALID_SIDE					-33
#define		LS150_MISSING_IMAGE					-34
#define		LS150_INVALID_TYPE					-35
#define		LS150_INVALID_SAVEMODE				-36
#define		LS150_INVALID_PAGE_NUMBER			-37
#define		LS150_INVALID_NRIMAGE				-38
#define		LS150_INVALID_FRONTSTAMP			-39
#define		LS150_INVALID_WAITTIMEOUT			-40
#define		LS150_INVALID_VALIDATE				-41
#define		LS150_INVALID_CODELINE_TYPE			-42
#define		LS150_INVALID_SCANMODE				-44
#define		LS150_INVALID_BEEP					-45
#define		LS150_INVALID_FEEDER				-46
#define		LS150_INVALID_SORTER				-47
#define		LS150_INVALID_BADGE_TRACK			-48
#define		LS150_MISSING_FILENAME				-49
#define		LS150_INVALID_QUALITY				-50
#define		LS150_INVALID_FILEFORMAT			-51
#define		LS150_INVALID_COORDINATE			-52
#define		LS150_MISSING_HANDLE_VARIABLE		-53
#define		LS150_INVALID_POLO_FILTER			-54
#define		LS150_INVALID_ORIGIN_MEASURES		-55
#define		LS150_INVALID_SIZEH_VALUE			-56
#define		LS150_INVALID_FORMAT				-57
#define		LS150_READ_IMAGE_FAILED				-59
#define		LS150_INVALID_CMD_HISTORY			-60
#define		LS150_MISSING_BUFFER_HISTORY		-61
#define		LS150_INVALID_ANSWER				-62
#define		LS150_OPEN_FILE_ERROR_OR_NOT_FOUND	-63
#define		LS150_READ_TIMEOUT_EXPIRED			-64
#define		LS150_INVALID_METHOD				-65
#define		LS150_CALIBRATION_FAILED			-66
#define		LS150_INVALID_SAVEIMAGE				-67
#define		LS150_INVALID_UNIT					-68
#define		LS150_INVALID_NRWINDOWS				-71
#define		LS150_INVALID_VALUE					-72
#define		LS150_INVALID_DEGREE				-77
#define		LS150_R0TATE_ERROR					-78
#define		LS150_PERIPHERAL_RESERVED			-80
#define		LS150_INVALID_NCHANGE				-81
#define		LS150_BRIGHTNESS_ERROR				-82
#define		LS150_CONTRAST_ERROR				-83
#define		LS150_DOUBLE_LEAFING_ERROR			-85
#define		LS150_INVALID_BADGE_TIMEOUT			-86
#define		LS150_INVALID_RESET_TYPE			-87
#define		LS150_IMAGE_NOT_200_DPI				-89
#define		LS150_INVALID_FONT					-92
#define		LS150_INVALID_UNIT_SPEED			-93

#define     LS150_JAM_AT_MICR_PHOTO             -201		
#define S_LS150_JAM_DOC_TOO_LONG				-202
#define S_LS150_JAM_AT_SCANNER_PHOTO			-203

#define		LS_ERROR_OFFSET_FRONT				-348
#define		LS_ERROR_OFFSET_REAR				-349
#define		LS_ERROR_SCANNER_PWM				-350
#define		LS_ERROR_GAIN_FRONT					-351
#define		LS_ERROR_GAIN_REAR					-352
#define		LS_ERROR_COEFF_FRONT				-353
#define		LS_ERROR_COEFF_REAR					-354
#define		LS_ERROR_SCANNER_GENERICO			-363

#define		LS150_DECODE_FONT_NOT_PRESENT		-1101
#define		LS150_DECODE_INVALID_COORDINATE		-1102
#define		LS150_DECODE_INVALID_OPTION			-1103
#define		LS150_DECODE_INVALID_CODELINE_TYPE	-1104
#define		LS150_DECODE_SYSTEM_ERROR			-1105
#define		LS150_DECODE_DATA_TRUNC				-1106
#define		LS150_DECODE_INVALID_BITMAP			-1107
#define		LS150_DECODE_ILLEGAL_USE			-1108

#define		LS150_BARCODE_GENERIC_ERROR			-1201
#define		LS150_BARCODE_NOT_DECODABLE			-1202
#define		LS150_BARCODE_OPENFILE_ERROR		-1203
#define		LS150_BARCODE_READBMP_ERROR			-1204
#define		LS150_BARCODE_MEMORY_ERROR			-1205
#define		LS150_BARCODE_START_NOTFOUND		-1206
#define		LS150_BARCODE_STOP_NOTFOUND			-1207

#define		LS150_PDF_NOT_DECODABLE				-1301
#define		LS150_PDF_READBMP_ERROR				-1302
#define		LS150_PDF_BITMAP_FORMAT_ERROR		-1303
#define		LS150_PDF_MEMORY_ERROR				-1304
#define		LS150_PDF_START_NOTFOUND			-1305
#define		LS150_PDF_STOP_NOTFOUND				-1306
#define		LS150_PDF_LEFTIND_ERROR				-1307
#define		LS150_PDF_RIGHTIND_ERROR			-1308
#define		LS150_PDF_OPENFILE_ERROR			-1309


// ------------------------------------------------------------------------
//                  WARNINGS
// ------------------------------------------------------------------------
#define		LS150_FEEDER_EMPTY					1
#define		LS150_DATA_TRUNCATED				2
#define		LS150_BADGE_TIMEOUT					4
#define		LS150_ALREADY_OPEN					5
#define		LS150_PERIF_BUSY					6
#define		LS150_DOUBLE_LEAFING_WARNING		7
#define		LS150_NO_ENDCMD						8
#define		LS150_RETRY							9
#define		LS150_NO_OTHER_DOCUMENT				10
#define		LS150_QUEUE_FULL					11
#define		LS150_TRY_TO_RESET					14
#define		LS150_STRING_TRUNCATED				15
#define		LS150_COMMAND_NOT_SUPPORTED			19
#define		LS150_SORTER1_FULL					35
#define		LS150_LOOP_INTERRUPTED				40


// ------------------------------------------------------------------------
//					DEFINES STRUTTURES
// ------------------------------------------------------------------------
// structure for configure the read codeline from bitmap

typedef struct _ReadOption
{
	BOOL	PutBlanks;
	char	TypeRead;
}READOPTIONS, *LPREADOPTIONS;


// Structure with information about the just stored codeline
typedef struct _S_CODELINE_INFO
{
	short			Size;							// Size of the struct
	unsigned long	NrDoc;							// Progessive document number
	char			CodelineRead[CODE_LINE_LENGTH];	// Codeline returned
	short			NrBytes;						// Length of the codeline
} S_CODELINE_INFO, *LPS_CODELINE_INFO;


// Structure with information about the just stored image
typedef struct _S_IMAGE_INFO
{
	short			Size;				// Size of the struct
	unsigned long	NrDoc;				// Progressive document number
	HANDLE			hImage;				// Image handle
	int				ImageSize;			// Image size bytes
	int				Width;				// Image width
	int				Height;				// Image height
	int				Resolution;			// Image resolution
	int				BitCount;			// Image bit count (level of grey)
} S_IMAGE_INFO, *LPS_IMAGE_INFO;


// Structure with information about the just stored codeline
typedef struct _S_CODELINE_INFO_VB
{
	short			Size;							// Size of the struct
	unsigned long	NrDoc;							// Progessive document number
	WCHAR			CodelineRead[CODE_LINE_LENGTH];	// Codeline returned
	short			NrBytes;						// Length of the codeline
} S_CODELINE_INFO_VB, *LPS_CODELINE_INFO_VB;


// Structure with information about the just stored image
typedef struct _S_IMAGE_INFO_VB
{
	short			Size;				// Size of the struct
	unsigned long	NrDoc;				// Progressive document number
	HANDLE			hImage;				// Image handle
	int				ImageSize;			// Image size bytes
	int				Width;				// Image width
	int				Height;				// Image height
	int				Resolution;			// Image resolution
	int				BitCount;			// Image bit count (level of grey)
} S_IMAGE_INFO_VB, *LPS_IMAGE_INFO_VB;


// structure for read usefull information about the peripheral life
typedef struct _History
{
	short			Size;					// Size of the struct
	unsigned long	doc_sorted;				// Nr. of document sortered
	unsigned long	doc_retain;				// Nr. of document retained
	unsigned long	jams_card;				// Jam in the card entry
	unsigned long	jams_micr;				// Jam during the MICR reading
	unsigned long	jams_scanner;			// Jam in the feeder
	unsigned long	doc_cmc7_err;			// Nr. of document CMC7, read with error
	unsigned long	doc_e13b_err;			// Nr. of document E13B, read with error
	unsigned long	time_peripheral_on;		// Minutes peripheral time life
	unsigned long	num_turn_on;			// Nr. of power on
	unsigned long	doc_ink_jet;			// Nr. of document printed
	unsigned long	doc_stamp;				// Nr. of document stamped

} S_HISTORY, *LPS_HISTORY;



// ------------------------------------------------------------------------
//                  EXPORT FUNCTIONS
// ------------------------------------------------------------------------
#ifdef __cplusplus
extern "C" {
#endif

extern int APIENTRY LS150_Open(HWND hWnd,
							   HANDLE Hinst,
							   char Type_com);

extern int APIENTRY LS150_Close(HWND hWnd,
								char Type_com);

extern int APIENTRY LS150_Identify(HWND hWnd,
								  char  Type_com,
								  char  *lpCfg,
								  char  *NameProduct,
								  char  *ProductVersion,
								  char  *Date_Fw,
								  char	*BoardAndFPGANr,
								  char	*FeederVersion,
								  char  *SerialNumber,
								  char  *Reserved);

extern int APIENTRY LS150_DocHandle(HWND			hWnd,
									char			Type_com,
									short			FrontStamp,
									short			Validate,
									short			CodeLine,
									char			Side,
									short			ScanMode,
									short			Feeder,
									short			Sorter,
									short			WaitTimeout,
									short			Beep,
									short			Reserved1,
									long			Reserved2);

extern int APIENTRY LS150_ReadImage(HWND			hWnd,
									char			Type_com,
									short			ClearBlack,
									char			Side,
									LPHANDLE		FrontImage,
									LPHANDLE		BackImage,
									LPHANDLE		Reserved1,
									LPHANDLE		Reserved2);

extern int APIENTRY LS150_ConfigDoubleLeafing(HWND	hWnd,
											  long	Type,
											  short Value,
											  long	Reserved);

extern int APIENTRY LS150_ChangeStampPosition(HWND	hWnd,
											  char	Type_com,
											  short Step,
											  char	Reserved);

extern int APIENTRY LS150_SetThresholdClearBlack(HWND			hWnd,
												 char			Type_com,
												 unsigned char	Threshold);

extern int APIENTRY LS150_SaveJPEG(	HWND	hWnd, 
									char	Type_com, 
									HANDLE	Face, 
									int		quality, 
									LPSTR	filename);

extern int APIENTRY LS150_SaveDIB(HWND		hWnd,
								  char		Type_com,
								  HANDLE	hImage,
								  LPSTR		filename);

extern int APIENTRY LS150_SaveTIFF(HWND		hWnd,
								   char		Type_com,
								   HANDLE	hImage,
								   LPSTR	filename,
								   int		Type,
								   int		SaveMode,
								   int		PageNumber);

extern int APIENTRY LS150_RotateImage(HWND		hWnd,
									  char		Type_com,
									  HANDLE	hImage,
									  int		degree,
									  HANDLE	*pImage);

extern int APIENTRY LS150_ImageBrightness(HWND		hWnd,
										  char		Type_com,
										  HANDLE	hImage,
										  int		nChange,
										  HANDLE	*pImage);

extern int APIENTRY LS150_ImageContrast(HWND	hWnd,
										char	Type_com,
										HANDLE	hImage,
										int		nChange,
										HANDLE *pImage);

extern int APIENTRY LS150_CutImage(HWND		hWnd,
								   char		Type_com,
								   HANDLE	hImage,
								   short	Unit,
								   float	pos_x,
								   float	pos_y,
								   float	sizeW,
								   float	sizeH,
								   HANDLE  *pImage);

extern int APIENTRY LS150_ConvertImage200To100dpi(HWND		hWnd,
												  char		Type_com,
												  HANDLE	hImage,
												  HANDLE	*pImage);

extern int APIENTRY LS150_ReadCodeline(HWND	 hWnd,
									   char  Type_com,
									   LPSTR Codeline,
									   short *Length_Codeline,
									   LPSTR Barcode,
									   short *Length_Barcode);

extern int APIENTRY LS150_CodelineReadFromBitmap(HWND hWnd,
												 char Type_com,
												 HANDLE hImage,
												 char *codelineType,
												 short UnitMeasure,
												 float x,
												 float y,
												 float sizeW,
												 float sizeH,
												 READOPTIONS *Option,
												 LPSTR Codeline,
												 UINT *Length);

extern int APIENTRY LS150_ReadPdf417FromBitmap(HWND		hWnd,
											   char		Type_com,
											   HANDLE	hImage,
											   LPSTR	Codeline,
											   UINT		*Length,
											   char		*ErrorRate,
											   int		Reserved1,
											   int		Reserved2,
											   int		Reserved3,
											   int		Reserved4);

extern int APIENTRY LS150_ReadBarcodeFromBitmap(HWND	hWnd,
												char	Type_com,
												HANDLE	hImage,
												char	TypeBarcode,
												int		pos_x,
												int		pos_y,
												int		sizeW,
												int		sizeH,
												LPSTR	Codeline,
												UINT	*Length);

extern int APIENTRY LS150_ConvertImageToBW(HWND		hWnd,
											char	Type_com,
											short	Method, 
											HANDLE	GrayImage,
											LPHANDLE BWImage,
											short	PoloFilter,
											float	Reserved);

extern int APIENTRY LS150_FreeImage(HWND	hWnd,
									LPHANDLE hImage);

extern int APIENTRY LS150_LoadString(HWND hWnd,
									 char Type_com,
									 char Format, 
									 short Length,
									 LPSTR String);

extern int APIENTRY LS150_LoadStringWithCounter(HWND hWnd,
												char Type_com,
												char Format,
												LPSTR String,
												short Length,
												unsigned long StartNumber,
												short Step);

extern int APIENTRY LS150_LoadDigitalStringWithCounter(HWND			 hWnd,
													   char			 Side,
													   char			 *String,
													   short		 llString,
													   unsigned long StartNumber,
													   short		 Step,
													   char			 *font,
													   int			 dim,
													   BOOL			 bold,
													   BOOL			 italic,
													   BOOL			 underline,
													   long			 TextColor,
													   short		 Unit,
													   float		 pos_x,
													   float		 pos_y);

extern int APIENTRY LS150_ReadBadge(HWND hWnd,
									char Type_com,
									unsigned char Format,
									short MaxLength, 
									char *String,
									short *length);

extern int APIENTRY LS150_ReadBadgeWithTimeout(HWND hWnd, 
												char Type_com, 
												unsigned char Format,
												short MaxLength, 
												char *String, 
												short *length, 
												long timeout);

extern int APIENTRY LS150_Reset(HWND	hWnd,
								char	Type_com,
								char	ResetType);

extern int APIENTRY LS150_Wait(HWND hWnd,
							   int  *ReplyCmd,
							   void *Reserved);

extern int APIENTRY LS150_Test(HWND hWnd,
							   int  *ReplyCmd,
							   void *Reserved);

extern int APIENTRY LS150_DisplayImage(HWND hWnd,
									   HANDLE hInstance,
									   char TypeCom,
									   char *FilenameFront,
									   char *FilenameBack,
									   int XFront,
									   int YFront,
									   int XBack,
									   int YBack,
									   int FrontWidth,
									   int FrontHeight,
									   int BackWidth,
									   int BackHeight,
									   HWND *RetHwndFront,
									   HWND *RetHwndBack);

extern int APIENTRY LS150_UpdateImage(HWND hWnd,
									  char TypeCom,
									  char *FilenameFront,
									  char *FilenameBack,
									  HWND hWndFront,
									  HWND hWndBack);

extern int APIENTRY LS150_AutoDocHandle(HWND	hWnd,
										char	Type_com,
										short	FrontStamp,
										short	Validate,
										short	CodeLine,
										short	ScanMode,
										short	Sorter,
										short	NumDocument,
										short	ClearBlack,
										char	Side,
										short	SaveImage,
										char	*DirectoryFile,
										char	*BaseFilename,
										float	pos_x,
										float	pos_y,
										float	sizeW,
										float	sizeH,
										short	OriginMeasureDoc,
										int		Quality,
										short	FileFormat,
										short	WaitTimeout,
										short	Beep,
										int		SaveMode,
										int		PageNumber,
										LPVOID	Reserved1,
										LPVOID	Reserved2,
										LPVOID	Reserved3);

extern int APIENTRY LS150_GetDocData(HWND			hWnd,
									 LPSTR			FilenameFront,
									 LPSTR			FilenameBack,
									 LPSTR			Reserved1,		// not used must be NULL
									 LPSTR			Reserved2,		// not used must be NULL
									 LPLONG			*FrontImage,
									 LPLONG			*BackImage,
									 LPLONG			*Reserved3,		// not used must be NULL
									 LPLONG			*Reserved4,		// not used must be NULL
									 LPSTR			CodelineSW,
									 LPSTR			CodelineHW,
									 LPSTR			Barcode,
									 unsigned long	*NrDoc,			// set to 0
									 LPVOID			Reserved5,		// not used must be NULL
									 LPVOID			Reserved6);		// not used must be NULL

extern int APIENTRY LS150_StopAutoDocHandle(HWND hWnd,
											char Type_com);

extern int APIENTRY LS150_DisableWaitDocument(HWND hWnd,
											  char Type_com,
											  BOOL fDo);

extern int APIENTRY LS150_PeripheralStatus(HWND			 hWnd,
										   unsigned char *SenseKey,
										   unsigned char *StatusByte);

extern int APIENTRY LS150_GetVersion(char *, short);

extern int APIENTRY LS150_ViewOCRRectangle(HWND hWnd,
										   BOOL fView);

extern int APIENTRY LS150_HistoryCommand(HWND		hWnd,
										 short		Cmd,
										 S_HISTORY	*sHistory);

extern int APIENTRY LS150_DownloadFirmware(HWND		hWnd,
										   char		*FileFw,
										   int		(__stdcall *userfunc1)(char *Item));

extern int APIENTRY LS150_DoubleLeafingSensibility(HWND			hWnd,
												   unsigned char Value);

extern int APIENTRY LS150_SetUnitSpeed(HWND		hWnd,
									   short	UnitSpeed);

extern int APIENTRY LS150_SetDiagnosticMode(HWND		hWnd,
									   short	UnitSpeed);

#ifdef __cplusplus
}
#endif

/////////////////////////////////////////////////////////////////////////

#endif