// File DLL principale.
#include "LsCFG150.h"
#include "CtsCalibrations.h"
#include "CtsPdfDriverLicense.h"


#include <stdafx.h>

//#include <windows.h>		// required for all Windows applications
//#include <windowsx.h>
//#include <stdio.h>
#include <windows.h>
#include <time.h>
#include <ctime>

#include "lsapi.h"
#include "LsApi_Calibr.h"
#include "LS_SetConfig.h"

#include "Ls40Class.h"
#include "ErrWar.h"
#include "is.h"
#include "AlcorEMV.h"
#include "MCardAPI.h"
#include "CtsMicroHoleTester.h" 
#include "math.h"
#include <stdio.h>
#include "libimgdiag.h"
#pragma pack(8)

extern "C" 
{
#include "gridpro.h"
}
#pragma pack(4)
#include "Constanti.h" 

#define TRIMMER_PRE_READ				3
#define TRIMMER_MIN_VALUE				0x00
//#define TRIMMER_MAX_VALUE				0x63
#define TRIMMER_MAX_VALUE				0xff	//0x1f   //cambiato 27/05/2016

#define NR_VALORI_LETTI					2
//#define OFFSET_INZIO_CALCOLO			250
#define NR_VALORI_PER_MEDIA				200
#define OFFSET_PRIMO_PICCO				72

#define SCANNER_BACK					0
#define SCANNER_FRONT					1
#define SCANNER_TIME_DEFAULT			1427
#define SCANNER_TIME_DEFAULT_300		1902
#define SCANNER_TIME_DEFAULT_VE			0x768

#define LIMITE_16_GRIGI				0x06
#define LIMITE_256_GRIGI			0x66
#define NR_VALORI_OK				228
#define E13B_FINE_LLC				0x0d		// Terminatore dati Larghezza char
#define DIM_DESCR_CHAR				5
#define HEIGHT_BAR					50


#define TENSIONE_VCC					3.3		// Tensione di alimentazione del circuito MICR
#define MOLTIPL_RIFERIMENTO_VOLT		0.4		// Moltiplicatore per riferimento in Volt
#define NR_VALORI_CONVERTITORE			256
#define NR_BYTES_TRIMMER				2

#define OFFSET_INZIO_CALCOLO			300
#define NR_VALORI_PER_MEDIA				200
#define ZERO_TEORICO					0x80
// Defines taratura MICR
#define TENSIONE_VCC_LS150ETH				3.3		// Tensione di alimentazione del circuito MICR
#define MOLTIPL_RIFERIMENTO_VOLT_LS150ETH	0.4		// Moltiplicatore per riferimento in Volt
#define NR_VALORI_CONVERTITORE_LS150ETH		256

#define TEST_SPEED_300_DPI				0x00
#define TEST_SPEED_300_DPI_UV			0x01
#define TEST_SPEED_300_DPI_COLOR		0x02


#define VALORE_MAX_PICCO_MIN			0x60
#define NR_PICCHI						50
#define MAX_READ_RETRY					3
#define PICCO_GAIN_DISTANZA_MINIMA		100
#define PICCO_DISTANZA_MINIMA			100
#define NR_BYTE_BUFFER_E13B				0x4000

#define SOGLIA_INIZIO_NERO				0x80 - 0x10	// Valore per inizio barretta nera
#define SPEED_GRAY						0x0001
#define SPEED_COLOR						0x0101

#define SPEED_200_DPI					0x00
#define SPEED_300_DPI					0x01


// Defines for dump memory
#define MEMORY_EEPROM					0
#define MEMORY_FLASH					1
#define MEMORY_RAM						2
#define MEMORY_EXTERN_RAM				3

#define OFF_PHOTO_1						4
#define OFF_PHOTO_2						5
#define OFF_PHOTO_3						6
#define OFF_PHOTO_4						7
#define OFF_PHOTO_5					16 //8
#define OFF_PHOTO_6					17 //9
#define OFF_PHOTO_7					18 //10


#define TYPE_LS150						150

#define RETRY_IMAGE_CALIBRATION			1000
#define GOOD_TIME_SAMPLE_VALUE			112.0f
#define MM_OFFSET_BORDO					12
#define SOGLIA_NERO_BIANCO				128


#define SAMPLE_VALUE_MIN				107.0f
#define SAMPLE_VALUE_MAX				116.0f
#define SAMPLE_VALUE_MEDIO				112.0f

bool fCLOUD2700Connected;

#define MASK_SCANNER_UV					0x01
#define MASK_SCANNER_COLOR				0x02
#define MASK_ATM_CONFIGURATION			0x20

#define LS40_VERSION_MYTELLER_2			3.04f

// Parametri per calibrazione foto
#define CALIBRATION_PHOTO_PATH				0
#define CALIBRATION_PHOTO_DOUBLE_LEAFING	1

char PathAppl[_MAX_PATH];


Ls40Class::Periferica::Periferica(void)
{
	/*PhotoValue[0]= 0;
	PhotoValue[1]= 0;
	PhotoValue[2]= 0;
	PhotoValue[3]= 0;
	PhotoValue[4]= 0;
	PhotoValue[5]= 0;
	PhotoValue[6]= 0;
	PhotoValue[7]= 0;*/
//	PhotoValue  = gcnew  array<System::Int16>{0,0,0,0,0,0,0,0};

	ReturnC = gcnew ErrWar();
	//strCis = gcnew Cis();
	S_Storico = gcnew Storico();
	TipoPeriferica = LS_40_USB;
	GetVersione();


	Limite16Grigi  = LIMITE_16_GRIGI;			// Soglia nera per Clear Black
	Limite256Grigi = LIMITE_256_GRIGI;
	
	
}

int Ls40Class::Periferica::GetVersione()
{
	char Versione[80];

	VersioneDll_1 = "C.T.S. Ls40Class.dll -- Ver. 1.00, Rev 002,  date 24/10/2013";

	Reply = LSGetVersion(Versione,80);
	if(Reply == LS_OKAY)
	{
		VersioneDll_0 = Marshal::PtrToStringAnsi((IntPtr) (char *)Versione);
	}

	return 0;
}


HMODULE mhDLL = NULL;

// DLL function Pointers
pfnMCardInitialize					MCardInitialize;
pfnMCardShutdown					MCardShutdown;
pfnMCardConnect						MCardConnect;
pfnMCardDisconnect					MCardDisconnect;
pfnMCardGetAttrib					MCardGetAttrib;
pfnMCardSetAttrib					MCardSetAttrib;
pfnMCardReadMemory					MCardReadMemory;
pfnMCardWriteMemory					MCardWriteMemory;
pfnMCardSetMemoryWriteProtection	MCardSetMemoryWriteProtection;
pfnMCardSetMemoryReadProtection		MCardSetMemoryReadProtection;
pfnMCardReadMemoryStatus			MCardReadMemoryStatus;
pfnMCardVerifyPIN					MCardVerifyPIN;
pfnMCardChangePIN					MCardChangePIN;
pfnMCardChallengeResponse			MCardChallengeResponse;
pfnMCardDeductCounter				MCardDeductCounter;
pfnMCardWaitForCardState			MCardWaitForCardState;



void InitializeProcAddresses ()
{
	LPCSTR lpcszProcName;

	if( mhDLL == NULL )
	{
		mhDLL = LoadLibraryA("MCSCM.dll");
		
		lpcszProcName = "MCardInitialize";
		MCardInitialize = (pfnMCardInitialize)GetProcAddress (mhDLL, lpcszProcName);

		
		lpcszProcName = "MCardShutdown";
		MCardShutdown = (pfnMCardShutdown)GetProcAddress (mhDLL, lpcszProcName);


		lpcszProcName = "MCardConnect";
		MCardConnect = (pfnMCardConnect)GetProcAddress (mhDLL, lpcszProcName);


		lpcszProcName = "MCardDisconnect";
		MCardDisconnect = (pfnMCardDisconnect)GetProcAddress (mhDLL, lpcszProcName);


		lpcszProcName = "MCardGetAttrib";
		MCardGetAttrib = (pfnMCardGetAttrib)GetProcAddress (mhDLL, lpcszProcName);


		lpcszProcName = "MCardSetAttrib";
		MCardSetAttrib = (pfnMCardSetAttrib)GetProcAddress (mhDLL, lpcszProcName);


		lpcszProcName = "MCardReadMemory";
		MCardReadMemory = (pfnMCardReadMemory)GetProcAddress (mhDLL, lpcszProcName);


		lpcszProcName = "MCardWriteMemory";
		MCardWriteMemory = (pfnMCardWriteMemory)GetProcAddress (mhDLL, lpcszProcName);


		lpcszProcName = "MCardSetMemoryWriteProtection";
		MCardSetMemoryWriteProtection = (pfnMCardSetMemoryWriteProtection)GetProcAddress (mhDLL, lpcszProcName);


		lpcszProcName = "MCardSetMemoryReadProtection";
		MCardSetMemoryReadProtection = (pfnMCardSetMemoryReadProtection)GetProcAddress (mhDLL, lpcszProcName);


		lpcszProcName = "MCardReadMemoryStatus";
		MCardReadMemoryStatus = (pfnMCardReadMemoryStatus)GetProcAddress (mhDLL, lpcszProcName);


		lpcszProcName = "MCardVerifyPIN";
		MCardVerifyPIN = (pfnMCardVerifyPIN)GetProcAddress (mhDLL, lpcszProcName);


		lpcszProcName = "MCardChangePIN";
		MCardChangePIN = (pfnMCardChangePIN)GetProcAddress (mhDLL, lpcszProcName);


		lpcszProcName = "MCardChallengeResponse";
		MCardChallengeResponse = (pfnMCardChallengeResponse)GetProcAddress (mhDLL, lpcszProcName);


		lpcszProcName = "MCardDeductCounter";
		MCardDeductCounter = (pfnMCardDeductCounter)GetProcAddress (mhDLL, lpcszProcName);


		lpcszProcName = "MCardWaitForCardState";
		MCardWaitForCardState = (pfnMCardWaitForCardState)GetProcAddress (mhDLL, lpcszProcName);

	}
}

SCARDHANDLE	CardHandle;
MCARDCONTEXT	hMCardContext = 0;
LONG			lStatus = MCARD_S_SUCCESS;

int Ls40Class::Periferica::ConnettiCard( SCARDCONTEXT  *hSC, SCARDHANDLE *hCard, char * ReturnString)
{
	long		lReturn;
	DWORD		dwAP = 0;
	long		lStatus;	
	//	DWORD		dwLen;
	//SCARDCONTEXT	ContextHandle;
	DWORD			ActiveProtocol = 0 ;
	BYTE			byCardType = 0;
	DWORD dwDllVersion = 0;
	FILE *fh ; 
	
	char ReaderInFocus[100];

	//Caricamento librerie...
	InitializeProcAddresses();
	fCLOUD2700Connected = FALSE ;

	lReturn = SCARD_S_SUCCESS;
	lStatus = MCARD_S_SUCCESS;
	lReturn = SCardEstablishContext(SCARD_SCOPE_SYSTEM,NULL,NULL,hSC);
	if (lReturn == SCARD_S_SUCCESS)
	{
		//lReturn = SCardConnect(*hSC, L"Identive CLOUD 2700 R Smart Card Reader 0", SCARD_SHARE_SHARED, SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1, &CardHandle, &ActiveProtocol);
		//lReturn = SCardConnect(ContextHandle, L"Identive CLOUD 2700 R Smart Card Reader 0", SCARD_SHARE_DIRECT, SCARD_PROTOCOL_UNDEFINED, hCard, &dwAP);
		//if (lReturn == SCARD_S_SUCCESS)
		//{
			//lReturn = SCardDisconnect(CardHandle, SCARD_UNPOWER_CARD );
		lReturn = MCardShutdown (hMCardContext);

		//strcpy(ReaderInFocus,"Identive CLOUD 2700 R Smart Card Reader 0");
		strcpy(ReaderInFocus,"CLOUD 2700 R Smart Card Reader");
		lReturn = MCardInitialize(*hSC,(LPCTSTR)ReaderInFocus , &hMCardContext, &dwDllVersion);
		//Davide
		if( fh = fopen("Trace_Calibrazione_SMART.txt", "at") )
		{
			fprintf(fh, "lReturnold =%d",lReturn);
			fprintf(fh, "\n");
			fclose( fh );
		}
		if ( lReturn == MCARD_S_SUCCESS ) 
		{
			//lReturn = MCardShutdown (hMCardContext);
			//lReturn = MCardInitialize(*hSC,(LPCTSTR)ReaderInFocus , &hMCardContext, &dwDllVersion);
			

			//if (lReturn == MCARD_S_SUCCESS)
			{
				lStatus = MCardConnect(hMCardContext, TRUE, byCardType, &CardHandle);
				if (lStatus == MCARD_S_SUCCESS )
					fCLOUD2700Connected = TRUE ;
			}
				
		}
		//}
		else
		{
			//strcpy(ReaderInFocus,"Identive CLOUD 2700 R Smart Card Reader 0");
			strcpy(ReaderInFocus,"Identive uTrust 2700 R Smart Card Reader 0");
			lReturn = MCardInitialize(*hSC,(LPCTSTR)ReaderInFocus , &hMCardContext, &dwDllVersion);
			//Davide
			if( fh = fopen("Trace_Calibrazione_SMART.txt", "at") )
			{
				fprintf(fh, "lReturnNew =%d",lReturn);
				fprintf(fh, "\n");
				fclose( fh );
			}
			if ( lReturn == MCARD_S_SUCCESS ) 
			{
				lStatus = MCardConnect(hMCardContext, TRUE, byCardType, &CardHandle);
				if (lStatus == MCARD_S_SUCCESS )
				{
					lStatus = MCardConnect(hMCardContext, TRUE, byCardType, &CardHandle);
					if (lStatus == MCARD_S_SUCCESS )
						fCLOUD2700Connected = TRUE ;
				}
				
			}
			else
			{

				// Provo il lettore vecchio
				lReturn = SCardConnect(*hSC, (LPCSTR)L"Generic Usb SmartCard Reader 0", SCARD_SHARE_DIRECT, SCARD_PROTOCOL_UNDEFINED, hCard, &dwAP);
				if( lReturn != SCARD_S_SUCCESS )
				{
					lReturn = SCardConnect(*hSC, (LPCSTR)L"Generic Usb Smart Card Reader 0", SCARD_SHARE_DIRECT, SCARD_PROTOCOL_UNDEFINED, hCard, &dwAP);
				}

				DWORD eee = GetLastError();

				if (lReturn == SCARD_S_SUCCESS)
				{
					lReturn = Alcor_SwitchCardMode(*hCard, 0, SYNCHRONOUS_CARD_SLE4442_MODE);
					if (lReturn == SCARD_S_SUCCESS)
					{
						lReturn = SCardDisconnect(*hCard, SCARD_LEAVE_CARD);
						if (lReturn == SCARD_S_SUCCESS)
						{
							//lReturn = SCardConnect(*hSC, L"Generic Usb Smart Card Reader 0", SCARD_SHARE_SHARED, SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1, hCard, &dwAP);
							lReturn = SCardConnect(*hSC, (LPCSTR)L"Generic Usb SmartCard Reader 0", SCARD_SHARE_DIRECT, SCARD_PROTOCOL_UNDEFINED, hCard, &dwAP);
							if ( SCARD_S_SUCCESS != lReturn )
							{
								strcpy(ReturnString,"Errore in SCardConnect - SCARD_SHARE_SHARED");
							}
						}
						else
						{
							strcpy(ReturnString,"Errore in SCardDisconnect");
						}
					}
					else
					{
						strcpy(ReturnString,"Errore in Alcor_SwitchCardMode");				
					}           
				}
				else
				{
					strcpy(ReturnString,"Errore in SCardConnect - SCARD_SHARE_DIRECT");
				}
			}
		}
	}
	else
	{
	  strcpy(ReturnString,"Errore in SCardEstablishContext");
	}

	return lReturn;
}


int Ls40Class::Periferica::ScriviCard(String ^strWrite)
{
	LONG lReturn;
	SCARDCONTEXT  hSC;
	SCARDHANDLE hCard;
	UCHAR sm[4];
	ULONG smLen = 0;
	BYTE bData = 0;
	char Errordesc[1024];
	char *BufferData;
	char DataToWrite ;
	DWORD	dwOffset;
	DWORD	dwLen;
	


	BufferData = (char*) Marshal::StringToHGlobalAnsi(strWrite ).ToPointer();
	lReturn = ConnettiCard( &hSC, &hCard, Errordesc);
	if (lReturn  == SCARD_S_SUCCESS)
	{
		if ( fCLOUD2700Connected == TRUE )
		{
			BYTE PINbuffer [3] = {0xFF, 0xFF, 0xFF};
			lReturn=MCardVerifyPIN (  CardHandle,
                                      0,
                                      PINbuffer,
                                      3);
			if (lReturn  == SCARD_S_SUCCESS)
			{
				//BufferData
				dwOffset = 0x28 ; //in queste carte posso scrivere dalla posizione 40 in poi
				dwLen = 215 ;
		
				if (MCARD_S_SUCCESS != (lStatus = MCardWriteMemory (
										CardHandle,
										0 , //byMemZone,
										dwOffset,
										(LPBYTE)BufferData , 
										&dwLen
										)))
					{
						lReturn = MCardShutdown (hMCardContext);
						return -1 ;
					}
					else
						lReturn = MCardShutdown (hMCardContext);
			}
		}
		else
		{
			lReturn = SLE4442Cmd_ReadSecurityMemory(hCard, 0, 4, sm, &smLen);
			if (lReturn  == SCARD_S_SUCCESS)
			{
				if((sm[0] & 0x01) == 0x01) 
					bData = sm[0] & 0xfe;
				else if((sm[0] & 0x02) == 0x02) 
						bData = sm[0] & 0xfd;
					else if((sm[0] & 0x04) == 0x04) 
						bData = sm[0] & 0xfb;

				lReturn = SLE4442Cmd_UpdateSecurityMemory(hCard, 0, 0, bData);
				if (lReturn  == SCARD_S_SUCCESS)
				{
					for(int i = 1; i <= 3; i++)
					{
						lReturn = SLE4442Cmd_CompareVerificationData(hCard, 0, i, 0xFF);					
					}
					if (lReturn  == SCARD_S_SUCCESS)
					{
						lReturn = SLE4442Cmd_UpdateSecurityMemory(hCard, 0, 0, 0xff);
						if (lReturn  == SCARD_S_SUCCESS)
						{
							 //WRITE MEMORY
							
							
							for(int i = 0; i < 215; i++)
							{
								UCHAR add = 0x28 + i;
								UCHAR slot = 0;
								if ( i > (int)strlen(BufferData))
									DataToWrite = 0;
								else
									DataToWrite = BufferData[i];

								lReturn = SLE4442Cmd_UpdateMainMemory(hCard, slot, add, (UCHAR)DataToWrite);
							}
				 
							lReturn = SCardDisconnect(hCard, SCARD_LEAVE_CARD);
							if ( SCARD_S_SUCCESS != lReturn )
							{
									 //CString err;
									 //err.Format(L"UNABLE TO DISCONNECT CARD: 0x%.2X", lReturn);
									 //MessageBox(err,L"ERROR - SCardDisconnect", MB_OK);
									 //return;
							}
				 
							SCardReleaseContext(hSC);
							if ( SCARD_S_SUCCESS == lReturn ) 
							{

								  //MessageBox(L"CARD ENCODED SUCCESSFULLY",L"ENCODING COMPLETE", MB_OK);
							}
						}
						else
						{
							//CString err;
							//err.Format(L"UNABLE TO UPDATE SECURITY MEMORY [clear-err]: 0x%.2X", lReturn);
							//MessageBox(err,L"ERROR - SLE4442Cmd_UpdateSecurityMemory", MB_OK);
							return -1;
						}

					
					}
					else
					{
						// ERRORE SLE4442Cmd_CompareVerificationData
					}
				}
				else
				{
					//CString err;
					//err.Format(L"UNABLE TO UPDATE SECURITY MEMORY: 0x%.2X", lReturn);
					//MessageBox(err,L"ERROR - SLE4442Cmd_UpdateSecurityMemory", MB_OK);
					return -1;
				}
			}
			else
			{
				//CString err;
				//err.Format(L"UNABLE TO READ SECURITY CODE: 0x%.2X", lReturn);
				//MessageBox(err,L"ERROR - SLE4442Cmd_ReadSecurityMemory", MB_OK);
				return -1;
			}
		}
		//else
		//{

		//}	
	}
	return lReturn;
}




int Ls40Class::Periferica::LeggiCard()
{
	int lReturn;
	//connetticard
	SCARDCONTEXT  hSC;
	SCARDHANDLE hCard;
	//UCHAR sm[4];
	ULONG smLen = 0;
	BYTE bData = 0;
	char Errordesc[1024];

	

	//char BufferData[256];
	CHAR BufferData[256] = {0};
	
	if( strRead != "" )
		strRead = "";

	lReturn = ConnettiCard( &hSC, &hCard, Errordesc);
	if (lReturn  == SCARD_S_SUCCESS)
	{

		if ( fCLOUD2700Connected == TRUE )
		{
			LONG			lStatus = MCARD_S_SUCCESS;
			BYTE	byMemZone;
			DWORD	dwOffset;
			LPBYTE	pbyData;
			DWORD	dwLen;

			dwOffset = 40 ;
			dwLen = 80 ;
			byMemZone = 0 ;

			pbyData = (LPBYTE) malloc ((sizeof (BYTE) * dwLen) + 10);
			
			if (MCARD_S_SUCCESS != (lStatus = MCardReadMemory (
				CardHandle,
				byMemZone,
				dwOffset,
				pbyData,
			    &dwLen
				)))
			{	
				lReturn = MCardShutdown (hMCardContext);
				//ERRORE
				return  -1 ;
			}
			else
			{
				
				strRead =  Marshal::PtrToStringAnsi((IntPtr) pbyData,dwLen);
				LONG lReturn = MCardShutdown (hMCardContext);
				lReturn = MCardShutdown (hMCardContext);
			}
		}
		else
		{
			ULONG dataLen = 0;
			lReturn = SLE4442Cmd_ReadMainMemory(hCard, 0, 0x28, 255-0x28, BufferData, &dataLen);

			DWORD eee = GetLastError();

			if ( SCARD_S_SUCCESS != lReturn )
			{
				return -1;
			}

			strRead =  Marshal::PtrToStringAnsi((IntPtr) (char *) BufferData);
			lReturn = SCardDisconnect(hCard, SCARD_LEAVE_CARD);
			if ( SCARD_S_SUCCESS != lReturn )
			{
				return -1;
			}

			SCardReleaseContext(hSC);
		}
	}
	else
	{
		return lReturn ;
	}
	return lReturn;
}




int Ls40Class::Periferica::Identificativo()
{
	
	char IdentStr[64],Date_Fw[64],LsName[64],SerialNumber[64];
	char StrStampa[64];
	char BoardNr[8];
	char CpldNr[8];
	char Version[64];
	short Connessione=0;
	
	CpldNr[1]= 0;
	CpldNr[2]= 0;


	memset(LsName,0x00,sizeof(LsName));
	memset(Version,0x00,sizeof(Version));
	memset(IdentStr,0x00,sizeof(IdentStr));
	memset(Date_Fw,0x00,sizeof(Date_Fw));
	memset(SerialNumber,0x00,sizeof(SerialNumber));
	

	//HWND 
	HINSTANCE hInst = 0;
	ReturnC->FunzioneChiamante = "Test Identificativo";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,  &Connessione);
	/*if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	// Chiamo la funzione per via documento
	if ((Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN))*/
	if (Reply == LS_OKAY )
	{
		Reply =	LSUnitIdentify(Connessione,(HWND)0, IdentStr,LsName,Version,Date_Fw,SerialNumber,BoardNr, NULL,StrStampa, NULL, NULL, NULL, NULL, NULL);
		if (Reply == LS_OKAY)
		{
			SIdentStr = Marshal::PtrToStringAnsi((IntPtr) (char *) IdentStr);
			
			SStrStampa = Marshal::PtrToStringAnsi((IntPtr) (char *) StrStampa);
			SVersion = Marshal::PtrToStringAnsi((IntPtr) (char *)Version);
			SDateFW = Marshal::PtrToStringAnsi((IntPtr) (char *)Date_Fw);
			SLsName = Marshal::PtrToStringAnsi((IntPtr) (char *)LsName);
			SSerialNumber = Marshal::PtrToStringAnsi((IntPtr) (char *)SerialNumber);
			SBoardNr = BoardNr[0].ToString();
			CpldNr[0] =  BoardNr[1];
			SFpga = Marshal::PtrToStringAnsi(static_cast<IntPtr>(CpldNr));
			GetTypeLS(SLsName);

			

			if( IdentStr[0] & 0x01 )						// 00000001
				ConfMicr = true;
			
			//if( IdentStr[0] & 0x04 )						
			if( IdentStr[0] & 0x02 )						
				ConfCard = true;

			if( IdentStr[0] & 0x08 )						// 00001000
				ConfEndorserPrinter = true;
			
			if( IdentStr[0] & 0x10 )						// 00100000
				ConfCapacitor = true;
			
			if( IdentStr[0] & 0x20 )						// 00100000
				ConfVoidingStamp = true;

			if( IdentStr[1] & 0x01 )						// 00000001
				ConfScannerFront = true;

			if( IdentStr[1] & 0x02 )						// 00000010
				ConfScannerBack = true;

			if( IdentStr[1] & 0x04 )
				ConfBadgeReader = 1;

			if( IdentStr[1] & 0x08 )
				ConfBadgeReader = 2;

			if( IdentStr[1] & 0x10 )
				ConfBadgeReader = 3;
		}
		else
		{
			ReturnC->FunzioneChiamante = "LSIdentify";
		}
		LSDisconnect(Connessione,(HWND)hDlg);

	}
	else
	{
		ReturnC->FunzioneChiamante = "LS_Open";
	}
	ReturnC->ReturnCode = Reply;
	
	return Reply;
}






int Ls40Class::Periferica::IdentificativoMyT()
{
	
	
	short Connessione=0;
	char	IdentStr[8];
	char	LsName[20];
	
	

	//HWND 
	HINSTANCE hInst = 0;
	ReturnC->FunzioneChiamante = "Test Identificativo";
	
	Reply = LSUnitIdentify(Connessione, (HWND)hDlg, IdentStr, LsName, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
	LSDisconnect(Connessione,(HWND)hDlg);

	
	ReturnC->ReturnCode = Reply;
	
	return Reply;
}






int Ls40Class::Periferica::CalibrazioneFoto()
{
	//HINSTANCE hInst = 0;
	unsigned short pDump[24],Threshold[24];
	int llBuffer=0;
	short Connessione=0;
	ReturnC->FunzioneChiamante= "CalibrazioneFoto";
	
	Reply = CollaudoLSConnectDiagnostica((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione, true);
	//Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,  &Connessione);
	
	if( Reply == LS_OKAY || Reply == LS_ALREADY_OPEN || Reply == LS_TRY_TO_RESET )
	{
		Reply = LSPhotosCalibration(Connessione,(HWND)hDlg,0);
		if (Reply == LS_OKAY)
		{
			LSReadPhotosValue(Connessione,(HWND)0,pDump,Threshold);
			if (Reply == LS_OKAY)
			{
				// Leggo il valore dei photo
				PhotoValue[0] = pDump[0];
				PhotoValue[1] = pDump[1];
				PhotoValue[2] = pDump[2];
				// Doppia sfogliatura..... 
				PhotoValueDP[0] = (unsigned char)pDump[3];
				PhotoValueDP[1] = (unsigned char)pDump[4];
				PhotoValueDP[2] = (unsigned char)pDump[5];

				PhotoValueDP[3] = (unsigned char)Threshold[0];
				PhotoValueDP[4] = (unsigned char)Threshold[1];
				PhotoValueDP[5] = (unsigned char)Threshold[2];
				PhotoValueDP[6] = (unsigned char)Threshold[3];
				PhotoValueDP[7] = (unsigned char)Threshold[4];
				PhotoValueDP[8] = (unsigned char)Threshold[5];

			}
			else
			{
				ReturnC->FunzioneChiamante= "LSReadPhotosValue";
			}
		}
		else
		{
			ReturnC->FunzioneChiamante= "LSPhotosCalibration";
		}
		LSDisconnect(Connessione,(HWND)hDlg);

	}
	else
	{
		ReturnC->FunzioneChiamante= "LSConnect";
	}
	ReturnC->ReturnCode = Reply;
	
	return Reply;
}



int Ls40Class::Periferica::DoCalibrationUltrasonic(String ^strWrite , int NrLoop )
{
	int		Reply = LS_CALIBRATION_FAILED;
	char	sTmp[512];
	char	sRead[512];
	char	ParIni[256];
	char	*fileIni;
	short   Connessione=0;
	//char	Ultrasonic_LottoDoc[16];
	char    *BufferData ; 

	//memset(Ultrasonic_LottoDoc, 0x00 , sizeof(Ultrasonic_LottoDoc)) ; 

	BufferData = (char*) Marshal::StringToHGlobalAnsi(strWrite ).ToPointer();

	
	fileIni = (char*) Marshal::StringToHGlobalAnsi(NomeFileUltrasonic ).ToPointer();
	//-----------Open--------------------------------------------
    
	Reply = CollaudoLSConnectDiagnostica((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione, true);
	//Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,  &Connessione);

	if( Reply == LS_OKAY || Reply == LS_ALREADY_OPEN || Reply == LS_TRY_TO_RESET )
	{
		// Read ini parameter
		//sprintf(fileIni, "%s%s", PathAppl, ULTRASONIC_FILE_INI);
		
		//if( GetPrivateProfileString("Files", "IniID", NULL, sRead, sizeof(sRead), fileIni) )
		{
			if (NrLoop == 1 ) 
				Reply = LSUltrasonicCalibration(Connessione, ULTRASONIC_CALIBRATION_STEP_1, NULL, ParIni);
			else
				Reply = LS_OKAY ; 

			if( Reply == LS_OKAY && NrLoop != 1)
			{
				// Leggo il documento da inserire
				//if( GetPrivateProfileString("DocStats", "Doc1", NULL, sRead, sizeof(sRead), fileIni) )
				{
					// Dialog box di setup periferica
			//		if( DialogBox(hInst, MAKEINTRESOURCE (IDD_ULTRASONIC_SENSOR), hDlg, (DLGPROC)DlgProcUltrasonic) )
					{
						
						
						//sprintf(sTmp, "Insert properly the document %s in the feeder", sRead);
						//MessageBox((HWND)0, sTmp, "Collaudi", MB_OK | MB_ICONINFORMATION);

						//Reply = LSUltrasonicCalibration(Connessione, ULTRASONIC_CALIBRATION_STEP_2,Ultrasonic_LottoDoc, ParIni);
						//Ultrasonic_LottoDoc =  (char)LottoDoc ; 
						Reply = LSUltrasonicCalibration(Connessione, ULTRASONIC_CALIBRATION_STEP_2,BufferData, ParIni);
						if( Reply == LS_OKAY )
						{
							// Sposto il documento di fronte al sensore
							Reply = LSPhotosCalibration(Connessione, (HWND)0, CALIBRATION_PHOTO_DOUBLE_LEAFING);
							if( Reply == LS_OKAY )
							{
								Reply = LSUltrasonicCalibration(Connessione, ULTRASONIC_CALIBRATION_STEP_3, NULL, ParIni);
								if( Reply == LS_OKAY )
								{
									sprintf(sTmp, "Calibration OK !", sRead);
									//MessageBox(hDlg, sTmp, TITLE_POPUP, MB_OK | MB_ICONINFORMATION);
								}
								else
								{
									// Errore di calibrazione
									//CheckReplyInkDetector(hDlg, Reply);
									if( Reply == LS_INVALID_PARAMETER )
									{
										sprintf(sTmp, "Missing parameter \"%s\" in file ini", ParIni);
										//MessageBox(hDlg, sTmp, TITLE_ERROR, MB_ICONERROR | MB_OK);
									}
									else
										//CheckReply(hDlg, Reply, "LSUltrasonicCalibration 3");
										ReturnC->FunzioneChiamante= "LSUltrasonicCalibration 3";
								}
							}
							else
								ReturnC->FunzioneChiamante= "LSPhotosCalibration";
								//CheckReply(hDlg, Reply, "LSPhotosCalibration");
						}
						else
						{
							// Errore di calibrazione
							//CheckReplyInkDetector(hDlg, Reply);
							if( Reply == LS_INVALID_PARAMETER )
							{
								sprintf(sTmp, "Missing parameter \"%s\" in file ini", ParIni);
								ReturnC->FunzioneChiamante= "LSUltrasonicCalibration 2" ; 
								//MessageBox(hDlg, sTmp, TITLE_ERROR, MB_ICONERROR | MB_OK);
							}
							//else
								//CheckReply(hDlg, Reply, "LSUltrasonicCalibration 2");
								//ReturnC->FunzioneChiamante= "LSUltrasonicCalibration 2";
						}
					}
					//else
					//	MessageBox(hDlg, "Calibration Aborted !", TITLE_ERROR, MB_ICONERROR | MB_OK);
				}
				//else
					//MessageBox(hDlg, "Missing Parameter 'Doc1' in Ultrasonic.ini !", TITLE_ERROR, MB_ICONERROR | MB_OK);
					//Reply = LS_INVALID_PARAMETER;

			}
			else
			{
				// Errore di calibrazione
				//CheckReplyInkDetector(hDlg, Reply);
				if( Reply == LS_INVALID_PARAMETER )
				{
					sprintf(sTmp, "Missing parameter \"%s\" in file ini", ParIni);
					ReturnC->FunzioneChiamante= "LSUltrasonicCalibration 1";
					//MessageBox(hDlg, sTmp, TITLE_ERROR, MB_ICONERROR | MB_OK);
				}
				//else
					//CheckReply(hDlg, Reply, "LSUltrasonicCalibration 1");
					//ReturnC->FunzioneChiamante= "LSUltrasonicCalibration 1";
			}
		}
		//else
			//MessageBox(hDlg, "Missing file Ultrasonic.ini !", TITLE_ERROR, MB_ICONERROR | MB_OK);
			//ReturnC->FunzioneChiamante= "LSUltrasonicCalibration";

			LSDisconnect(Connessione, (HWND)0);
	}
	
	return Reply;
} // DoCalibrationUltrasonic

int Ls40Class::Periferica::InkDetectorLoadCmds()
{
	short Connessione;
	char *StrNome=NULL;


	ReturnC->FunzioneChiamante = "LSInkDetectorLoadCmds";

	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	if( Reply == LS_OKAY )
	{
		StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileSPT ).ToPointer();
		Reply = LSUltrasonicLoadCmds(Connessione,StrNome );
	}
	else
	{
		CheckReply(0, Reply, "LSInkDetectorLoadCmds");
		
	}
	
	ReturnC->ReturnCode = Reply ; 
	return Reply;

	
}


//
// Ritorna la distanza in pixel tra 2 picchi negativi a meta' segnale
//
int DistanzaPicco(unsigned char *pdct, long NrCampioni)
{
	int ii;
	int picco1, picco2, Distanza;


	// Inizilizzo le variabili
	Distanza = picco1 = picco2 = 0;
	for( ii = (NrCampioni / 2); ii < NrCampioni; ii ++)
	{
		// Controllo se è un picco,
		// per essere un picco deve avere 7 valori prima e dopo di valore inferiore
		if( (pdct[ii] <= VALORE_MAX_PICCO_MIN) &&
			((pdct[ii] <= pdct[ii-1]) && (pdct[ii] < pdct[ii+1])) &&
			((pdct[ii] <= pdct[ii-2]) && (pdct[ii] < pdct[ii+2])) &&
			((pdct[ii] <= pdct[ii-3]) && (pdct[ii] < pdct[ii+3])) &&
			((pdct[ii] <= pdct[ii-4]) && (pdct[ii] < pdct[ii+4])) &&
			((pdct[ii] <= pdct[ii-5]) && (pdct[ii] < pdct[ii+5])) &&
			((pdct[ii] <= pdct[ii-6]) && (pdct[ii] < pdct[ii+6])) &&
			((pdct[ii] <= pdct[ii-7]) && (pdct[ii] < pdct[ii+7])) )
		{
			// Se Distanza e' a zero, salvo il primo picco
			if( picco1 == 0 )
			{
				picco1 = ii;
			}
			else if( picco2 == 0 )
			{
				picco2 = ii;
			}
			else
			{
				if( (picco2 - picco1) > (ii - picco2) )
					Distanza = picco2 - picco1;
				else
					Distanza = ii - picco2;
				break;
			}
		}
	}

	return Distanza;
} // DistanzaPicco

int Ls40Class::Periferica::DoMICRCalibration3(int MICR_Value,long NrReadGood,	char	TypeSpeed, ListBox^ list)
{
	int		ii;
	short	Reply;
	short	fRetryLetture, fRetryCalibration;
	short	NrPicchi, NrPicchiMin, TotPicchi;
	float	RangeMore, RangeLess;
	float	Percento;
	unsigned char	TrimmerValue, NewTrimmerValue;
	unsigned char	buffDati[NR_BYTE_BUFFER_E13B];
	long	llDati;									// Dati da chiedere
	long	ValMin;
	float	SommaMin, TotSommeMin;
	char	BufCodelineHW[CODE_LINE_LENGTH];
	short	len_codeline;
	float   Valore_100_in_Volt;
	double  Unita_Volt;
	short	Connessione=0 , ScanMode ; 
	/*char	IdentStr[8];
	char	LsName[20];
	char	Version[20];*/
	//float	ver;	
	//FILE	*fh;
	// Setto i valori iniziali
	//fMyTeller2 = FALSE;
	fRetryLetture = fRetryCalibration = 0;
	nrLettura = 0;
	Percentile = Percento = 0;
	TotPicchi = 0;
	TotSommeMin = 0;
	TrimmerValue = 0;
	MICR_SignalValue = MICR_Value;
	NrReadGood = 3;
	MICR_TolleranceLess = 4;
	MICR_TolleranceMore = 4;

	if( TypeSpeed == TEST_SPEED_300_DPI )
		ScanMode = SCAN_MODE_256GR300;
	else if( TypeSpeed == TEST_SPEED_300_DPI_UV )
		ScanMode = SCAN_MODE_256GR300_AND_UV;
	else if( TypeSpeed == TEST_SPEED_300_DPI_COLOR )
		ScanMode = SCAN_MODE_COLOR_300;	


	Valore_100_in_Volt = (float)(TENSIONE_VCC * MOLTIPL_RIFERIMENTO_VOLT);
	Unita_Volt = TENSIONE_VCC / NR_VALORI_CONVERTITORE;

	/*Reply = LSConnectDiagnostica((HWND)hDlg, (HANDLE)hInst, TipoPeriferica, &hLS);
	if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(hLS,(HWND)0,RESET_FREE_PATH);
	}*/

	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,  &Connessione);
	if( Reply == LS_OKAY )
	{
		// Setto il valore del trimmer a meta' (16)
//			TrimmerValue = 16;
//			Reply = LSSetTrimmerMICR(hLS, hDlg, 0, TrimmerValue);
		//writeDebug("DoMICRCalibration3( Connessione = " + Connessione.ToString() + ")", "DoMICRCalibration3Ls40");
		
		//sporcava l'handle ?
		/*if( fh = fopen("Trace_Calibrazione_MICR.txt", "at") )
		{
			fprintf(fh, "Connessione =%d",Connessione);
			fprintf(fh, "\n");
			fclose( fh );
		}*/
		
		

		//TypeLS = TYPE_LS40;
		//-----------Identify--------------------------------
		//Reply = LSUnitIdentify(Connessione, (HWND)hDlg, IdentStr, LsName, Version, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
		//if( Reply == LS_OKAY )
		//	TypeLS = GetTypeLS(LsName, Version);

		//// Se MyTeller NON faccio trattenuto
		//ver = (float)atof(Version);
		//if( IdentStr[1] & MASK_ATM_CONFIGURATION && (ver >= LS40_VERSION_MYTELLER_2) )
		//	fMyTeller2 = TRUE;


		if( Reply == LS_OKAY)
		{
			// Eseguo il primo loop di misurazione di 3 letture
			for( ii = 0; ii < (TRIMMER_PRE_READ + NrReadGood); ii++ )
			{
				nrLettura ++;

				// Chiamo la funzione per via documento
				Reply = LSDocHandle(Connessione, (HWND)hDlg,
									NO_STAMP,
									NO_PRINT_VALIDATE,
									READ_CODELINE_MICR,
									SIDE_NONE_IMAGE,
									ScanMode , //SCAN_MODE_256GR200,
									AUTO_FEED,
									HOLD_DOCUMENT , //(short)(fMyTeller2 ? SORTER_BAY1 : HOLD_DOCUMENT) ,//HOLD_DOCUMENT,
									WAIT_YES,
									NO_BEEP,
									NULL,
									0, 0);

				
				if( Reply == LS_OKAY )
				{
					// Dopo il primo via leggo il valore del trimmer
					if( !TrimmerValue )
					{
						//Reply = LSReadTrimmerMICR(Connessione, (HWND)hDlg, 0, buffDati, NR_VALORI_LETTI);
						// Leggo il valore settato nella periferica
						Reply = LSReadTrimmerMICR(Connessione, (HWND)hDlg, TypeSpeed, buffDati, NR_VALORI_LETTI);
			

						if( Reply == LS_OKAY)
						{
							// Salvo il valore del trimmer
							TrimmerValue = buffDati[0];
						}
					}

					// Lettura segnale
					llDati = NR_BYTE_BUFFER_E13B;		// 16 Kb.
					Reply = LSReadE13BSignal(Connessione, (HWND)hDlg, buffDati, &llDati);
					if (Reply != LS_OKAY)
					{
						//CheckReply((HWND)hDlg, Reply, "LS40_ReadE13BSignal");
						//Reply = LS_CALIBRATION_FAILED;
						//Lascio a Video il Reply Originale
						break;
					}

					// Controllo se hanno inserito il documento giusto !
					if( DistanzaPicco(buffDati, llDati) < PICCO_GAIN_DISTANZA_MINIMA )
					{
						//MessageBox(hDlg, "Wrong document or device assambled improperly !", TITLE_ERROR, MB_OK | MB_ICONERROR);
						//Reply = LS_CALIBRATION_FAILED;
						Reply = LS_CALIBRATION_FAILED_1  ;
						break;
					}

					//sprintf(FileOut, "dati\\SgnE13B_LS40_%04d.cts", stParDocHandle.NrNomeFile++);
					//if( (fh = fopen(FileOut, "wb")) != NULL )
					//{
					//	fwrite(buffDati, sizeof(unsigned char), llDati, fh);
					//	fclose( fh );
					//}

					// Calcolo la somma dei valori minimi ...
					Media50Barrette(buffDati, llDati, &NrPicchi, &SommaMin, &ValMin, &NrPicchiMin);

					// Controllo se ho trovato tutti i picchi
					if( NrPicchi == NR_PICCHI )
					{
						TotSommeMin += SommaMin;

						// elimino i picchi a zero !!!
						if( ValMin == 0 )
							NrPicchi -= NrPicchiMin;
						TotPicchi += NrPicchi;

						// Calcolo il valore da visualizzare in bitmap
						// SOLO se NrPicchi diverso da zero
						if( NrPicchi )
						{
							SommaMin /= NrPicchi;
							SommaMin = ZERO_TEORICO - SommaMin;
							SommaMin *= (float)Unita_Volt;			// Converto in Volt
							Percento = (SommaMin * 100) / Valore_100_in_Volt;
						}

						// Disegna segnale e visualizza la bitmap
						ShowMICRSignal((HINSTANCE)hInst, (HWND)hDlg, buffDati, (short)llDati, Percento, TrimmerValue);

						if( ii == (TRIMMER_PRE_READ - 1) )
						{
							// Calcolo il valore per regolare il trimmer
							// SOLO se TotPicchi diverso da zero ...
							if( TotPicchi )
							{
								TotSommeMin /= TotPicchi;
								TotSommeMin = ZERO_TEORICO - TotSommeMin;
								TotSommeMin *= (float)Unita_Volt;	// Converto in Volt
								Percento = TotSommeMin * 100 / Valore_100_in_Volt;

								// Calcolo il nuovo valore del trimmer ...
								NewTrimmerValue = Calc_Trimmer_Position(Percento, TrimmerValue);
							}
							else
							{
								// Il segnale è in SATURAZIONE !!!
								// Decremento il valore del trimmer di 11
								NewTrimmerValue = TrimmerValue - 11;

								ii = -1;			// Così ricomincia da inizio pre letture
								TotSommeMin = 0;	// Riazzero il totale delle somme
								TotPicchi = 0;		// Riazzero il totale picchi
							}


							// Se il valore ritornato é uguale zero 
							// Fallisco la calibrazione !!!!!
							if( NewTrimmerValue == 0 )
							{
								// Salvo il valore da ritornare
								Percentile = NewTrimmerValue;
								Reply = LS_MICR_TRIMMER_VALUE_NEGATIVE;
								break;
							}

							// Testo se sono nel range accettato dal trimmer
//							else if( NewTrimmerValue <= TRIMMER_MIN_VALUE )
//							{
//								NewTrimmerValue = TRIMMER_MIN_VALUE;
//								ii = -1;			// Così ricomincia da inizio pre letture
//							}

							else if( (NewTrimmerValue <= TRIMMER_MIN_VALUE) ||
									(NewTrimmerValue >= TRIMMER_MAX_VALUE) )
							{
								//Reply = LS_CALIBRATION_FAILED;
								Reply = LS_CALIBRATION_FAILED_2 ;
								Percentile = NewTrimmerValue;
								break;
							}


							// ... e setto il nuovo valore del trimmer
							TrimmerValue = NewTrimmerValue;
							Reply = LSSetTrimmerMICR(Connessione, (HWND)hDlg, TypeSpeed, NewTrimmerValue);

							if( Reply != LS_OKAY )
							{
								CheckReply((HWND)hDlg, Reply, "LS40_SetTrimmerMICR");
								break;
							}

							// Riazzero il totale delle somme e dei picchi
							TotSommeMin = 0;
							TotPicchi = 0;
						}
						else if( ii == ((TRIMMER_PRE_READ + NrReadGood) - 1) )
						{
							// Calcolo il valore delle POST LETTURE
							TotSommeMin /= TotPicchi;
							TotSommeMin = ZERO_TEORICO - TotSommeMin;
							TotSommeMin *= (float)Unita_Volt;	// Converto in Volt
							Percento = TotSommeMin * 100 / Valore_100_in_Volt;

							// Controllo se la lettura sta nel + o - 4% della tolleranza
							RangeMore = (float)MICR_SignalValue * ((float)MICR_TolleranceMore / 100);
							RangeLess = (float)MICR_SignalValue * ((float)MICR_TolleranceLess / 100);
							if( (Percento >= (MICR_SignalValue - RangeLess)) &&
								(Percento <= (MICR_SignalValue + RangeMore)) )
							{
								// Salvo il valore da ritornare, se ho letto tutti i documenti
								Percentile = Percento;
							}
							else
							{
								// se no controllo se ho già fatto 3 tentativi ... o esco !
								if( fRetryCalibration < 3 )
								{
									ii = -1;			// Così ricomincia da inizio pre letture
									TotSommeMin = 0;	// Riazzero il totale delle somme
									TotPicchi = 0;		// Riazzero il totale picchi
									fRetryCalibration ++;
								}
								else
								{
									//Reply = LS_CALIBRATION_FAILED;
									Reply = LS_CALIBRATION_FAILED_2 ;
									break;
								}
							}
						}
					} //if( NrPicchi == NR_PICCHI )
					else
					{
						// Disegna segnale e visualizza la bitmap
						ShowMICRSignal((HINSTANCE)hInst, (HWND)hDlg, buffDati, (short)llDati, 0, TrimmerValue);

						// Decremento ii per ripetere l'acquisizione !!!
						ii --;

						fRetryLetture ++;
						if( fRetryLetture == MAX_READ_RETRY )
						{
						    //MessageBox(0, "Document loose !!!\n\nChange Document !", "Collaudo", MB_OK);
							//Reply = LS_CALIBRATION_FAILED;
							Reply = LS_CALIBRATION_FAILED_3 ;
							break;
						}
					}

					// Leggo la codeline per cancellare il doppio buffer !
					len_codeline = CODE_LINE_LENGTH;
					Reply = LSReadCodeline(Connessione, (HWND)hDlg,
										   BufCodelineHW,
										   &len_codeline,
										   NULL, 0,
										   NULL, 0);

					//if( ! fMyTeller2 )
					{
						// Riporto il documento in bocchetta
						Reply = LSDocHandle(Connessione, (HWND)hDlg,
										NO_STAMP,
										NO_PRINT_VALIDATE,
										NO_READ_CODELINE,
										SIDE_NONE_IMAGE,
										SCAN_MODE_256GR200,
										PATH_FEED,
										EJECT_DOCUMENT,
										WAIT_YES,
										NO_BEEP,
										NULL,
										0, 0);

						if( Reply != LS_OKAY )
						{
							CheckReply((HWND)hDlg, Reply, "LS40_DocHandle");
							break;
						}
					}
				}
				else
				{
					CheckReply((HWND)hDlg, Reply, "LS40_DocHandle");
					break;
				}
			} // fine for
		}
		else
		{
			CheckReply((HWND)hDlg, Reply, "LS40_SetTrimmerMICR");
		}

		//if( ! fMyTeller2 )
			// Sorto il documento
			LSDocHandle(Connessione, (HWND)hDlg,
					NO_STAMP,
					NO_PRINT_VALIDATE,
					READ_CODELINE_MICR,
					SIDE_NONE_IMAGE,
					SCAN_MODE_256GR200,
					AUTO_FEED,
					SORTER_BAY1,
					WAIT_YES,
					NO_BEEP,
					NULL,
					0, 0);
		LSDisconnect(Connessione, (HWND)hDlg);
	}
	else
	{
		CheckReply((HWND)hDlg, Reply, "LS40_Open");
	}
	
	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Calibrazione MICR";

	return Reply;
} // DoMICRCalibration3


// ******************************************************************* //
// Calcolo della posizione del trimmer in base alla variazione         //
// percentuale del segnale della media rispetto al valore del segnale  //
// magnetico indicato sul documento di test                            //
// ******************************************************************* //
unsigned char Ls40Class::Periferica::Calc_Trimmer_Position(float ValoreLetto, unsigned char PosTrimmer)
{
    #define R_SERIE_TRIMMER            1800    // Valore di restistenza presente
                                            // in serie al trimmer digitale
    #define R_X_STEP_TRIMMER        39        // Valore resistivo corrispondente a ogni
                                            // step del trimmer digitale per LS40
    float    Var_Per_Sign;
    float    tmp;
    long    R_trimmer;
    short    R_x_step_trimmer;
    long    R_serie_trimmer;
 
 
    // Di default setto il valore per LS40
    R_serie_trimmer = R_SERIE_TRIMMER;
    R_x_step_trimmer = R_X_STEP_TRIMMER;
 
    //Calcolo la variazione percentuale del segnale della media rispetto
    //al valore del segnale magnetico indicato sul documento di test in
    //valore assoluto
    tmp = (float)abs((int)ValoreLetto - MICR_SignalValue);
    Var_Per_Sign = tmp * 100 / ValoreLetto;
 
    //Calcolo il valore resistivo fornito dal trimmer nella sua posizione
    //attulale e gli sommo la resistenza presente in serie
    R_trimmer = (PosTrimmer * R_x_step_trimmer);
//    R_trimmer &= 0xFFFF;
    R_trimmer += R_serie_trimmer;
 
    //Determino se bisogna incrementare o decrementare l'ampiezza del segnale
    if( ValoreLetto > MICR_SignalValue )
        tmp = (100 - Var_Per_Sign);
    else
        tmp = (Var_Per_Sign + 100);
 
 
    R_trimmer = (R_trimmer / 100) * (unsigned long)tmp;
    // Se la resistenza totale vale di più della resistenza fissa
    // dovrei settare un valore di trimmer negativo (non posso)
    // e quindi fisso il valore di trimmer a 0 !!!
    // Così la calibrazione fallirà !!!
    if( R_trimmer > R_serie_trimmer )
    {
        //Calcolo la variazione in percentuale da effettuare alla resistenza
        //complessiva presente sulla reazione negativa e di conseguenza
        //la nuova posizione del trimmer
        PosTrimmer = (unsigned char)((R_trimmer - R_serie_trimmer) / R_x_step_trimmer);
    }
    else
        PosTrimmer = 0;    // Errore !!!
 
    return PosTrimmer;
} // Calc_Trimmer_Position

//// ******************************************************************* //
//// Calcolo della posizione del trimmer in base alla variazione         //
//// percentuale del segnale della media rispetto al valore del segnale  //
//// magnetico indicato sul documento di test                            //
//// ******************************************************************* //
//unsigned char Ls40Class::Periferica::Calc_Trimmer_Position(float ValoreLetto, unsigned char PosTrimmer)
//{
//    #define R_SERIE_TRIMMER			1800	// Valore di restistenza presente
//											// in serie al trimmer digitale
//    //#define R_DENOMINATORE_TRIMMER	3300	// Valore di restistenza presente
//											// al denominatore della formula del guadagno
//    #define R_X_STEP_TRIMMER		39		// Valore resistivo corrispondente a ogni
//											// step del trimmer digitale
//    float	Var_Per_Sign;
//    float	tmp;
//    float	R_trimmer;
//	short	R_x_step_trimmer;
//	long	R_serie_trimmer;
//
//
//	// Di default setto il valore per LS40
//	R_serie_trimmer = R_SERIE_TRIMMER;
//	R_x_step_trimmer = R_X_STEP_TRIMMER;
//
//	//Calcolo la variazione percentuale del segnale della media rispetto
//	//al valore del segnale magnetico indicato sul documento di test in
//	//valore assoluto
//	tmp = (float)abs((int)ValoreLetto - MICR_SignalValue);
//	Var_Per_Sign = tmp * 100 / ValoreLetto;
//
//	//Calcolo il valore resistivo fornito dal trimmer nella sua posizione
//	//attulale e gli sommo la resistenza presente in serie
//	R_trimmer = (float)(PosTrimmer * R_x_step_trimmer);
////	R_trimmer &= 0xFFFF;
//	R_trimmer += R_serie_trimmer;
//
//	//Determino se bisogna incrementare o decrementare l'ampiezza del segnale
//	if( ValoreLetto > MICR_SignalValue )
//		tmp = (100 - Var_Per_Sign);
//	else
//		tmp = (Var_Per_Sign + 100);
//
//
//	//Calcolo la variazione in percentuale da effettuare alla resistenza
//	//complessiva presente sulla reazione negativa e di conseguenza
//	//la nuova posizione del trimmer
//	R_trimmer = (R_trimmer / 100) * (unsigned long)tmp;
//	PosTrimmer = (unsigned char)((R_trimmer - R_serie_trimmer) / R_x_step_trimmer);
//
//	return PosTrimmer;
//} // Calc_Trimmer_Position


void Ls40Class::Periferica::ShowMICRSignal(HINSTANCE hInst, HWND hwnd, unsigned char *pd, short llDati, float Percentile, short Trimmer)
{

	int conta ;
	list->Items->Add("Valore Letto: " + Percentile + " " + "Trimmer Value: "+ Trimmer );
	
	conta = list->Items->Count;
	list->SelectedIndex = conta-1;
	list->Refresh();
}





//int Ls40Class::Periferica::CalibraScanner(int Side)
int Ls40Class::Periferica::CalibraScanner(short ScannerType) //da versione Ls40UV+COLORE
{

	short Connessione=0;
	ReturnC->FunzioneChiamante = "Calibrazione Scanner";
	Reply = CollaudoLSConnect((HWND)hDlg,(HANDLE)hInst,TipoPeriferica,&Connessione);//Diagnostica
	if (Reply == LS_OKAY )
	{
		//Reply = LSScannerCalibration(Connessione,(HWND)hDlg, Side);
		Reply = LSScannerCalibration(Connessione,(HWND)hDlg, ScannerType);
		LSDisconnect(Connessione,(HWND)hDlg);

	}
	else
	{
		ReturnC->FunzioneChiamante = "LSConnect";
	}
	ReturnC->ReturnCode = Reply;

	return Reply;
}




int Ls40Class::Periferica::LeggiImmagini(char Side,short ScanMode)
{

	HANDLE HBackGray=0;
	HANDLE HFrontGray=0;
	HANDLE HFrontUV=0;
	short Connessione=0;
	char *StrNome=NULL;
	
	

	short floop = false;
	ReturnC->FunzioneChiamante = "Lettura Immagini";
	
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione);
	/*if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	if ((Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN))*/
	if (Reply == LS_OKAY )
	{
		/*if (HFrontGray)
			LSFreeImage((HWND)hDlg, &HFrontGray);
		if (HBackGray)
			LSFreeImage((HWND)hDlg, &HBackGray);*/
		if (ScanMode == SCAN_MODE_256GR200_AND_UV || ScanMode == SCAN_MODE_256GR100_AND_UV || ScanMode == SCAN_MODE_256GR300_AND_UV)
			Reply = LSModifyPWMUltraViolet(Connessione , (HWND)hDlg  , 100 , TRUE , 0 ) ; 


		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,150,230);

		do
		{
		

			Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								SIDE_ALL_IMAGE , //SIDE 
								ScanMode , //SCAN_MODE_256GR300,
								AUTO_FEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_SHORT_DOCUMENT,
								//SCAN_PAPER_DOCUMENT,
								0);
			if (Reply == LS_FEEDER_EMPTY)
			{
				floop = TRUE;
			}
			else
				floop = FALSE;
		}while(floop);

		if( Reply == LS_OKAY )
		{
			// Leggo l'immagine filmata
			Reply = LSReadImage(Connessione,(HWND)hDlg,
									NO_CLEAR_BLACK,//CLEAR_ALL_BLACK,// ,NO_CLEAR_BLACK
		//							(char)(TypeLS == TYPE_LS5 ? SIDE_FRONT_RED_IMAGE : SIDE_FRONT_IMAGE),
									SIDE_ALL_IMAGE , //Side,
									0,NULL,
									(LPHANDLE)&HFrontGray,
									(LPHANDLE)&HBackGray,
									(LPHANDLE)&HFrontUV,//NULL,
									NULL);
			if(Reply == LS_OKAY)
			{
				// salvo le immagini
				if (Side == SIDE_FRONT_IMAGE)
				{
					//NomeFileImmagine = "..\\Dati\\FrontImage.bmp";
	
					//for (int ii=0;ii<= NomeFileImmagine->Length-1;ii++)
					//{
					//StrNome =Marshal::PtrToStringAnsi((IntPtr) (char *)NomeFileImmagine);
						//StrNome[ii] = NomeFileImmagine->
						
					//}
					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
					Reply = LSSaveDIB((HWND)hDlg, HFrontGray,StrNome);

					if (ScanMode == SCAN_MODE_256GR200_AND_UV || ScanMode == SCAN_MODE_256GR100_AND_UV || ScanMode == SCAN_MODE_256GR300_AND_UV)
					{
						StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineUV ).ToPointer();
						Reply = LSSaveDIB((HWND)hDlg,  HFrontUV,StrNome);
					}

					

						 // Always free the unmanaged string.
					  Marshal::FreeHGlobal(IntPtr(StrNome));
				}
				else if (Side == SIDE_BACK_IMAGE)
				{
					//NomeFileImmagine = "..\\Dati\\BackImage.bmp";	
					//for (int ii=0;ii<= NomeFileImmagine->Length-1;ii++)
					//{
//						StrNome[ii] = NomeFileImmagine->get_Chars(ii);
					//}
					

					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();
					Reply = LSSaveDIB((HWND)hDlg,HBackGray,StrNome);

					 // Always free the unmanaged string.
					  Marshal::FreeHGlobal(IntPtr(StrNome));
				}	
			}
			else
			{
				ReturnC->FunzioneChiamante = "LSReadImage";
			}
			LSDisconnect(Connessione,(HWND)hDlg);

		}
		else
		{

			ReturnC->FunzioneChiamante = "LSDocHandle";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSConnect";
	}
	// libero le immagini
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	if (HFrontUV)
		LSFreeImage((HWND)hDlg, &HFrontUV);

	
	ReturnC->ReturnCode = Reply;
	

	return Reply;
}

int Ls40Class::Periferica::ChipCardLS40()
{
	ReturnC->FunzioneChiamante = "Test ChipCard";
	
	ReturnC->ReturnCode=LS_OKAY;
	return LS_OKAY;
}

int Ls40Class::Periferica::LeggiImmCard(char Side, short ^%NrDistanze,short ScanMode)
{
	HANDLE HBackGray=0;
	HANDLE HFrontGray=0;
	short Connessione=0;
	char *StrNome= NULL ; 
	short floop = false;

	ReturnC->FunzioneChiamante = "Lettura Immagini Carta";

	*NrDistanze = 0;

	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione);
	/*if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	if ((Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN))*/
	if (Reply == LS_OKAY )
	{
		/*if (HFrontGray)
			LSFreeImage((HWND)hDlg, &HFrontGray);
		if (HBackGray)
			LSFreeImage((HWND)hDlg, &HBackGray);*/


		Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								Side,
								ScanMode , //SCAN_MODE_256GR300,
								AUTO_FEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_CARD,
								0);

		if( Reply == LS_OKAY )
		{
			// Leggo l'immagine filmata
			Reply = LSReadImage(Connessione,(HWND)hDlg,									
									CLEAR_ALL_BLACK,
									Side,
									0,NULL,
									(LPHANDLE)&HFrontGray,
									(LPHANDLE)&HBackGray,
									NULL,
									NULL);
			if(Reply == LS_OKAY)
			{
				Marshal::FreeHGlobal(IntPtr(StrNome));
				StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
				
				LSSaveDIB((HWND)hDlg,HFrontGray,StrNome);

				Marshal::FreeHGlobal(IntPtr(StrNome));
				StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();
			

				LSSaveDIB((HWND)hDlg,  HBackGray,StrNome);
			


				// COSTRUISCO IL GRAFICO DELLE BARRETTE

				*NrDistanze = CalcolaSalvaDistanze((HWND)hDlg,(BITMAPINFOHEADER*)HFrontGray,24);

			}
			else
			{
				ReturnC->FunzioneChiamante = "LSReadImage";
			}
			LSDisconnect(Connessione,(HWND)hDlg);

		}
		else
		{
			ReturnC->FunzioneChiamante = "LSDocHandle";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSConnect";
	}
	// libero le immagini
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	ReturnC->ReturnCode = Reply;
	
	return Reply;
}


int Ls40Class::Periferica::CalcolaSalvaDistanze(HWND hDlg, BITMAPINFOHEADER *pImage, short LineToCheck)
{
	BOOL fNeroFound;
	unsigned char *pDati;
	long nColorData;
	long Width, Diff;
	short NrBarreTrovate, OffsetBarra;
	short currPixel, PixelIni;
	int ii;


	ii = 0;
	///*for (ii=0;ii<1024;ii++)
	//	Distanze[ii]= 0;

	// Vado ad inizio dati
	if( pImage->biBitCount <= 8)
		nColorData = 1 << pImage->biBitCount;
	else
		nColorData = 0;
	pDati = (unsigned char *)pImage + sizeof (BITMAPINFOHEADER) + (nColorData * sizeof (RGBQUAD));

	// Mi sposto a meta' documento
	Width = pImage->biWidth;
	if( Diff = Width % 4 )
		Width += (4 - Diff);

	pDati += (Width * (pImage->biHeight / 2));

	// Cerco le barre nere
	NrBarreTrovate = 0;
	OffsetBarra = MM_OFFSET_BORDO;		// Parto da 1 mm dal bordo
	currPixel = OffsetBarra;

	// Se sono arrivato in fondo all'immagine finisco il loop
	while( currPixel < (pImage->biWidth - MM_OFFSET_BORDO) )
	{
		fNeroFound = FALSE;
		currPixel = OffsetBarra;
		while( currPixel < (pImage->biWidth - MM_OFFSET_BORDO) )
		{
			if( (*(pDati + currPixel) < SOGLIA_NERO_BIANCO) && !fNeroFound)
			{
				fNeroFound = TRUE;

				// Salvo il pixel di inizio barra nera
				if( NrBarreTrovate == 0 )
					PixelIni = currPixel;

				NrBarreTrovate ++;
				if( NrBarreTrovate == LineToCheck )
				{
					Distanze[ii] = currPixel - PixelIni;
					ii ++;
					// Reinizzializzo per il prossimo conteggio
					NrBarreTrovate = 0;
					break;
				}
			}
			else
			{
				// Ho ritrovato il bianco
				if( (*(pDati + currPixel) > SOGLIA_NERO_BIANCO) && fNeroFound )
				{
					fNeroFound = FALSE;

					// Salvo l'inizio del prossimo check
					if( NrBarreTrovate == 1 )
					{
						OffsetBarra = currPixel;
					}
				}
			}

			currPixel ++;
		}
	}

	return ii-1;
} // CalcolaSalvaDistanze














int Ls40Class::Periferica::LeggiCodeline(short DocHandling)
{

	char	BufCodeLine[CODE_LINE_LENGTH];
	short	llBufCodeLine = 256;
	//short ret;
	short Connessione=0;

	ReturnC->FunzioneChiamante = "LeggiCodeline";
	
	//-----------Open--------------------------------------------
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione);
	/*if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )*/

	if (Reply == LS_OKAY )
	{
		

		Reply = LSDocHandle(Connessione,(HWND)hDlg,
							NO_STAMP,
							NO_PRINT_VALIDATE,
							READ_CODELINE_MICR,
							SIDE_NONE_IMAGE,
							SCAN_MODE_256GR200,
							AUTO_FEED,
							SORTER_BAY1,
							WAIT_YES,
							NO_BEEP,
							0,
							SCAN_SHORT_DOCUMENT,
							0);

		if (Reply == LS_OKAY)
		{
			//-----------ReadCodeLine--------------------------------------------
			Reply = LSReadCodeline(Connessione, (HWND)hDlg,
										  BufCodeLine,
										  &llBufCodeLine,
										  NULL, 0,
										  NULL, 0);
			
			if (Reply == LS_OKAY)
			{
				CodelineLetta = Marshal::PtrToStringAnsi((IntPtr) (char *)BufCodeLine);
			}
			else
			{
				ReturnC->FunzioneChiamante =  "LSReadCodeline";
			}
		}
		else
		{
			ReturnC->FunzioneChiamante =  "LSDocHandle";

		}			
	}
	else
	{
		ReturnC->FunzioneChiamante =  "LSConnect";
	}

	
	//-----------Close-------------------------------------------
	LSDisconnect(Connessione,(HWND)hDlg);

	
	ReturnC->ReturnCode = Reply;
	
	return Reply;
}


int Ls40Class::Periferica::DisableATMConfiguration(short Enable)
{
	short Connessione=0;
	char	IdentStr[8];
	char	LsName[20];
	char	Version[20];
	char BytesCfg[8];
	float	ver;
	BOOL fMyTeller2 = FALSE ; 

	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione);
	
	if (Reply == LS_OKAY )
	{
		//TypeLS = TYPE_LS40;
		//-----------Identify--------------------------------
		Reply = LSUnitIdentify(Connessione, (HWND)hDlg, IdentStr, LsName, Version, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
		ver = (float)atof(Version);
		if( IdentStr[1] & MASK_ATM_CONFIGURATION && (ver >= LS40_VERSION_MYTELLER_2) )
			fMyTeller2 = TRUE;
		
		if (fMyTeller2 == TRUE )
		{
			
			//Disabilito/Abilito la Modalita' ATM per effettuare questo TEST
			memset(BytesCfg, 0x00, 8) ; 
			memcpy(BytesCfg,IdentStr,8) ; 
			
			if (Enable == 1 )
				BytesCfg[1] |= 0x20 ; 
			else
				BytesCfg[1] &= 0xdf; 
			
			Reply = LS40_WriteConfiguration((HWND)hDlg,BytesCfg );
			
		}
	}

	//-----------Close-------------------------------------------
	LSDisconnect(Connessione,(HWND)hDlg);

	ReturnC->ReturnCode = Reply;
	
	return Reply;
}

int Ls40Class::Periferica::LeggiCodelineImmagini(short DocHandling)
{

	char	BufCodeLine[CODE_LINE_LENGTH];
	short	llBufCodeLine = 256;
	HANDLE HBackGray = 0;
	HANDLE HFrontGray = 0;
	short Connessione=0;
	char *StrNome= NULL ; 
	BOOL fMyTeller2; 
	char	IdentStr[8];
	char	LsName[20];
	char	Version[20];
	//char BytesCfg[4];
	//short ret;
	float	ver;

	fMyTeller2 = FALSE ;

	ReturnC->FunzioneChiamante = "LeggiCodelineImmagini";
	
	//-----------Open--------------------------------------------
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione);
	/*if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )*/
	if (Reply == LS_OKAY )
	{
		//TypeLS = TYPE_LS40;
		//-----------Identify--------------------------------
		Reply = LSUnitIdentify(Connessione, (HWND)hDlg, IdentStr, LsName, Version, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
		ver = (float)atof(Version);
		if( IdentStr[1] & MASK_ATM_CONFIGURATION && (ver >= LS40_VERSION_MYTELLER_2) )
			fMyTeller2 = TRUE;
		
		if (fMyTeller2 == TRUE )
			Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,130,230);
		else
			Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,150,230);
		
		// do via documento
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
							NO_STAMP,
							NO_PRINT_VALIDATE,
							READ_CODELINE_MICR,
							SIDE_ALL_IMAGE,
							SCAN_MODE_256GR300,
							AUTO_FEED,
							SORTER_BAY1,
							WAIT_YES,
							NO_BEEP,
							0,
							SCAN_SHORT_DOCUMENT,
							0);
		if (Reply == LS_OKAY)
		{
			//-----------ReadCodeLine--------------------------------------------
			llBufCodeLine = CODE_LINE_LENGTH;
			Reply = LSReadCodeline(Connessione, (HWND)hDlg,
									   BufCodeLine,
									   &llBufCodeLine,
									   NULL, 0,
									   NULL, 0);
			
			if (Reply == LS_OKAY)
			{
				// setto la codeline di ritorno
				CodelineLetta = Marshal::PtrToStringAnsi((IntPtr) (char *)BufCodeLine);
				// Leggo l'immagine filmata
				Reply = LSReadImage(Connessione, (HWND)hDlg,
										CLEAR_ALL_BLACK,
										SIDE_ALL_IMAGE,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);
				
				if (Reply == LS_OKAY)
				{
				
					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();

					

					Reply = LSSaveDIB((HWND)hDlg,  HFrontGray,StrNome);
					 // Always free the unmanaged string.

					if (Reply == LS_OKAY)
					{
						Marshal::FreeHGlobal(IntPtr(StrNome));
						StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();


						Reply = LSSaveDIB((HWND)hDlg,HBackGray,StrNome);									
					}	
					else
					{
						ReturnC->FunzioneChiamante =  "LSSaveimageFronte";
					}
					// libero le immagini
					if (HFrontGray)
						LSFreeImage((HWND)hDlg, &HFrontGray);
					if (HBackGray)
						LSFreeImage((HWND)hDlg, &HBackGray);
				}
				else
				{
					ReturnC->FunzioneChiamante =  "LSReadImage";
				}

			}
			else
			{
				ReturnC->FunzioneChiamante =  "LSReadCodeline";
	//			Reply = LS5xx_Reset((HWND)hDlg, SUSPENSIVE_MODE, RESET_PERIPHERAL_FREE_PATH);
			}
			//BufCodeLine[llBufCodeLine] = '\0';
		}
		else
		{

			ReturnC->FunzioneChiamante =  "LSDocHandle";
	//			Reply = LS5xx_Reset((HWND)hDlg, SUSPENSIVE_MODE, RESET_PERIPHERAL_FREE_PATH);
		}			
	}
	else
	{
		ReturnC->FunzioneChiamante =  "LSConnect";
	}
	
	//-----------Close-------------------------------------------
	LSDisconnect(Connessione,(HWND)hDlg);

	ReturnC->ReturnCode = Reply;
	
	return Reply;
}



int Ls40Class::Periferica::Timbra()
{

	DateTime iniTime,currTime;
	TimeSpan t;
	HANDLE HBackGray;
	HANDLE HFrontGray;
	char *StrNome = NULL; 
	short Connessione=0;
	
	ReturnC->FunzioneChiamante = "Timbra";
	//-----------Open--------------------------------------------Diagnostica
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione);
	/*if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )*/
	if (Reply == LS_OKAY )
	{
		// Do il via al documento
		Reply = LSDocHandle(Connessione, (HWND)hDlg,
								FRONT_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								SIDE_ALL_IMAGE,
								SCAN_MODE_256GR300,
								AUTO_FEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								0,
								0);
			
		if (Reply == LS_OKAY)
		{
			Reply = LSReadImage(Connessione, (HWND)hDlg,
										CLEAR_ALL_BLACK,
										SIDE_ALL_IMAGE,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);
			if (Reply == LS_OKAY)
			{
				
				Marshal::FreeHGlobal(IntPtr(StrNome));
				StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();				
				
				Reply = LSSaveDIB((HWND)hDlg,  HFrontGray,StrNome);
				if (Reply == LS_OKAY)
				{
					Marshal::FreeHGlobal(IntPtr(StrNome));
					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();
				
					Reply = LSSaveDIB((HWND)hDlg,  HBackGray,StrNome);
					
				}	
				else
				{
					ReturnC->FunzioneChiamante =  "LSSaveimageFronte";
				}
			}
			else
			{
				ReturnC->FunzioneChiamante = "LSReadImage";
			}
		
		}
		else
		{
			ReturnC->FunzioneChiamante = "LSDocHandle";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSConnect";
	}

	ReturnC->ReturnCode = Reply;

	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	
	LSDisconnect(Connessione,(HWND)hDlg);

	
	return Reply;
}


int Ls40Class::Periferica::TestStampa(int StampaStd)
{

	String ^strstp;
	char *str = NULL; 
	DateTime iniTime,currTime;
	TimeSpan t;
	HANDLE HBackGray;
	HANDLE HFrontGray;
	char *StrNome = NULL; 
	short Connessione=0;

	ReturnC->FunzioneChiamante =  "TestStampa";

	strstp = "TEST INKJET 888888888888 HHHHHHHHHHHHHHHH 0000000000000000";
	if (StampaStd)
	{
		strstp = Stampafinale;
	}
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione);
	/*if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )*/
	if (Reply == LS_OKAY )
	{
		Marshal::FreeHGlobal(IntPtr(str));
		str = (char*) Marshal::StringToHGlobalAnsi(strstp ).ToPointer();	

		if (StampaStd)
		{
			Reply = LSLoadString(Connessione,(HWND)hDlg,
								 FORMAT_BOLD ,//FORMAT_NORMAL,//: ) FORMAT_BOLD 
								 strstp->Length,
								 str);	
		}
		else
		{
			Reply = LSLoadString(Connessione,(HWND)hDlg,
								 FORMAT_NORMAL ,//FORMAT_NORMAL,//: ) FORMAT_BOLD 
								 strstp->Length,
								 str);	
		}		
		
		if (Reply == LS_OKAY)
		{
			// via doc
			Reply = LSDocHandle(Connessione, (HWND)hDlg,
								NO_STAMP,
								PRINT_VALIDATE,
								NO_READ_CODELINE,
								SIDE_ALL_IMAGE,
								SCAN_MODE_256GR300,
								AUTO_FEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								0,
								0);
			
			if (Reply == LS_OKAY)
			{
				Reply = LSReadImage(Connessione, (HWND)hDlg,
										CLEAR_ALL_BLACK,
										SIDE_ALL_IMAGE,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);
				if (Reply == LS_OKAY)
				{
					Marshal::FreeHGlobal(IntPtr(StrNome));
					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();			
					
					Reply = LSSaveDIB((HWND)hDlg,  HFrontGray,StrNome);
					if (Reply == LS_OKAY)
					{
						Marshal::FreeHGlobal(IntPtr(StrNome));
						StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();			
						
						Reply = LSSaveDIB((HWND)hDlg,  HBackGray,StrNome);
						
					}	
					else
					{
						ReturnC->FunzioneChiamante =  "LSSaveimageFronte";
					}
				}
				else
				{
					ReturnC->FunzioneChiamante = "LSReadImage";
				}

			}
			else
			{
				ReturnC->FunzioneChiamante =  "LSDocHandle";
			}
		}
		else
		{
			ReturnC->FunzioneChiamante =  "LSLoadString";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante =  "LSConnect";
	}

	ReturnC->ReturnCode = Reply;

	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);


	LSDisconnect(Connessione,(HWND)hDlg);


	return Reply;
}


int Ls40Class::Periferica::LeggiBadge(short traccia)
{

	short Length = 256;
	char strBadge[256];
	short Connessione=0;
	ReturnC->FunzioneChiamante = "LeggiBadge";

	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione);
	/*if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )*/
	if (Reply == LS_OKAY )
	{
		if (traccia == 1)
			Reply = LSReadBadgeWithTimeout(Connessione,(HWND)hDlg,FORMAT_IATA_ABA ,256,
				strBadge,&Length,15000);
		if (traccia == 2)
			Reply = LSReadBadgeWithTimeout(Connessione,(HWND)hDlg,FORMAT_ABA_MINTS ,256,
				strBadge,&Length,15000);
		if ( traccia == 3)
			Reply = LSReadBadgeWithTimeout(Connessione,(HWND)hDlg,FORMAT_IATA_ABA_MINTS ,256,
				strBadge,&Length,15000);

		
		BadgeLetto = Marshal::PtrToStringAnsi((IntPtr) (char *)strBadge);

	}
	else
	{
		ReturnC->FunzioneChiamante = "LSConnect";
	}

	ReturnC->ReturnCode = Reply;
	
	LSDisconnect(Connessione,(HWND)hDlg);

	return Reply;
}



int Ls40Class::Periferica::SerialNumber()
{

	short Length = 256;
	char *str;
	short ii=0;
	short Connessione=0;

	str = (char*) Marshal::StringToHGlobalAnsi(SSerialNumber ).ToPointer();

	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione);
	/*if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )*/
	if (Reply == LS_OKAY )
	{
		Reply = LSSetSerialNumber(Connessione,(HWND)hDlg, (unsigned char *)str,1962);
	}
	ReturnC->ReturnCode = Reply;
	LSDisconnect(Connessione,(HWND)hDlg);


	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Serial Number";

	return Reply;
}
/*
int Ls40Class::Periferica::Visual(char * str)
{
	//Ls40Class::FDownload *f;
	
	//f = new Ls40Class::FDownload;
	//f = 
	//fShow();
	//f->PDwl->Value ++;
	return 0;
}

*/


int Ls40Class::Periferica::SetConfigurazione(int E13B,int CMC7,int timbro,int badge12,int badge23,int badge123,int scannerfronte, int scannerretro,int inkjet, int card,int condensat,int SwAbilitato,int ScannerType,int SensoreDoubleLeafing,int ATMMode)
{

	char BytesCfg[4];
	unsigned char Sense[4];
	unsigned char Status[4];
	char IdentStr[64],Version[64],Date_Fw[64],LsName[64],SerialNumber[64],StampaVer[64];;
	char BoardNr[8];
	//char CpldNr[8];
	short Connessione=0;
	// setto configurazione periferica da file
	// Azzero la variabile
	//memset(BytesCfg, 0x40, sizeof(BytesCfg));
	BytesCfg[0] = 0x40;
	BytesCfg[1] = 0x40;
	BytesCfg[2] = 0x40;
	BytesCfg[3] = 0x40;

	if( E13B  || CMC7  )
	{
		BytesCfg[0] |= 0x01;
	}
	if( card )
	{
		BytesCfg[0] |= 0x02;
	}
	
	if (SensoreDoubleLeafing )
	{
		BytesCfg[0] |= 0x04;
	}

	if( inkjet)
	{
		BytesCfg[0] |= 0x08;
	}

	if( condensat )
	{
		BytesCfg[0] |= 0x10;
	}
	if( timbro)
	{
		BytesCfg[0] |= 0x20;
	}

	//if( IdentStr[1] & 0x01 )						// 00000001
	//	PeriphFeatures.ScannerFront = TRUE;

	//if( IdentStr[1] & 0x02 )						// 00000010
	//	PeriphFeatures.ScannerBack = TRUE;
// per il momento gli scanner ci sono SEMPRE 
//	if( scannerfronte && scannerretro )
	{
		BytesCfg[1] |= 0x03;		
	}

	if( badge12)
	{
		BytesCfg[1] |= 0x04;
	}
	if( badge23)
	{
		BytesCfg[1] |= 0x08;
	}
	if( badge123)
	{
		BytesCfg[1] |= 0x10;
	}

	if (ATMMode )
		BytesCfg[1] |= 0x20 ; 

	if(SwAbilitato >0)
	{
		BytesCfg[3] |= SwAbilitato;
	}

	//11/09/2017 Abilito Sempre Top Image /ClearPix
	BytesCfg[3] |= 0x01;


	//if( IdentStr[3] & 0x01 )						// 00000001
	//	PeriphFeatures.LicenseTopImage = TRUE;

	//if( IdentStr[3] & 0x02 )						// 00000010
	//	PeriphFeatures.LicensePDF417 = TRUE;

	//if( IdentStr[3] & 0x04 )						// 00000100
	//	PeriphFeatures.LicenseIQA = TRUE;

	//if( IdentStr[3] & 0x08 )						// 00001000
	//	PeriphFeatures.LicenseMicroHole = TRUE;


	//Setto le Configurazioni Scanner (UV o COLOR )
	switch( ScannerType )
	{
	case 62:	// Scanner a grigio
		break;
	case 63:	// Scanner a colori
		BytesCfg[2] |= MASK_SCANNER_COLOR;
		break;
	case 64:	// Scanner a grigio e UV
		BytesCfg[2] |= MASK_SCANNER_UV;
		break;
	case 75:	// Scanner a colori e UV ...per ora non esiste ancora...
		BytesCfg[2] |= MASK_SCANNER_UV;
		BytesCfg[2] |= MASK_SCANNER_COLOR;
		break;
	}
	
	
	// open
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione);
	/*if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )*/
	if (Reply == LS_OKAY )
	{

		// Setto la configurazione corrente

			
		Reply = LS40_WriteConfiguration((HWND)hDlg,BytesCfg );
		
		//Reply = LSSetConfiguration(Connessione,(HWND)hDlg,BytesCfg );
		if(Reply == LS_OKAY)
		{

			Reply =	LSUnitIdentify(Connessione,(HWND)0, IdentStr,LsName,Version,Date_Fw,SerialNumber,BoardNr, NULL,StampaVer, NULL, NULL, NULL, NULL, NULL);
			if (Reply == LS_OKAY)
			{
				// verifico la configurazione scritta 
				// testina micr
				if((IdentStr[0] & 0x01 ) == (E13B || CMC7))		
				{
					// card
					if(((IdentStr[0] & 0x02 )== 0x02) == (card))
					{	// stampante
						if((( IdentStr[0] & 0x08) == 0x08 )== (inkjet)) 
						{
							// devo ancora verificare che il piastrino funzioni...
							Reply = LSPeripheralStatus(Connessione,(HWND)hDlg,Sense,Status);
							if (Reply == LS_OKAY)
							{
								// verifica bit
								if(!((Status[0] & 0x20)  == 0x20 ))
								{
									//
									if(((IdentStr[0] & 0x10 )==0x10) == (condensat))
									{
										if(((IdentStr[0] & 0x20 )== 0x20 )==(timbro))
										{
											//if( IdentStr[1] & 0x01 )						// 00000001
											//ConfScannerFront = true;

											//if( IdentStr[1] & 0x02 )						// 00000010
											//ConfScannerBack = true;

											if(((IdentStr[1] & 0x04 )== 0x04) == (badge12))
											{
												if(((IdentStr[1] & 0x08 )== 0x08) == (badge23))
												{
													if(((IdentStr[1] & 0x10 )== 0x10) == (badge123))
													{
													}
													else
													{
														// badge 3

														Reply =S_LS40_BADGE_NOT_CONFIGURED;
														ReturnC->FunzioneChiamante = "LSIdentify";
													}
												}	
												else
												{
													// badge 2
													Reply = S_LS40_BADGE_NOT_CONFIGURED;
													ReturnC->FunzioneChiamante = "LSIdentify";
												}
											}
											else
											{
												// badge 1
												Reply = S_LS40_BADGE_NOT_CONFIGURED;
												ReturnC->FunzioneChiamante = "LSIdentify";
											}
										}
										else
										{
											// tmbro
											Reply = S_LS40_TIMBRO_NOT_CONFIGURED;
											ReturnC->FunzioneChiamante = "LSIdentify";
										}
									}
									else
									{
										// condensatori
										Reply = S_LS40_CONDENS_NOT_CONFIGURED;
										ReturnC->FunzioneChiamante = "LSIdentify";
									}
								}
								else
								{
									// hardware error la tentina non funziona
									Reply = S_LS40_INK_HW_ERROR;
									ReturnC->FunzioneChiamante = "LSIdentify";
								}
							}
							else
							{
								// stato non abuon fine
								ReturnC->FunzioneChiamante = "LSIdentify";
							}
						}
						else
						{
							//  INK non configurata
							Reply = S_LS40_INK__NOT_CONFIGURED;
							ReturnC->FunzioneChiamante = "LSIdentify";
						}
					}
					else
					{
						// errore Card
						Reply = S_LS40_CARD__NOT_CONFIGURED;
						ReturnC->FunzioneChiamante = "LSIdentify";
					}
				}
				else
				{
					// errore micr
					Reply = S_LS40_MICR__NOT_CONFIGURED;
					ReturnC->FunzioneChiamante = "LSIdentify";
				}
			}
			else
			{
				ReturnC->FunzioneChiamante = "LSIdentify";
			}
		}
	}	
	// close
	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Set Configurazione";

	LSDisconnect(Connessione,(HWND)hDlg);


	return Reply;
}



int Ls40Class::Periferica::SetConfigurazioneMyTeller(int E13B,int CMC7,int timbro,int badge12,int badge23,int badge123,int scannerfronte, int scannerretro,int inkjet, int card,int condensat,int SwAbilitato,int ScannerType,int SensoreDoubleLeafing,int ATMMode)
{

	char BytesCfg[4];
	//unsigned char Sense[4];
	//unsigned char Status[4];
	//char IdentStr[64],Version[64],Date_Fw[64],LsName[64],SerialNumber[64],StampaVer[64];;
	//char BoardNr[8];
	//char CpldNr[8];
	short Connessione=0;
	// setto configurazione periferica da file
	// Azzero la variabile
	//memset(BytesCfg, 0x40, sizeof(BytesCfg));
	BytesCfg[0] = 0x40;
	BytesCfg[1] = 0x40;
	BytesCfg[2] = 0x40;
	BytesCfg[3] = 0x40;

	if( E13B  || CMC7  )
	{
		BytesCfg[0] |= 0x01;
	}
	if( card )
	{
		BytesCfg[0] |= 0x02;
	}
	
	if (SensoreDoubleLeafing )
	{
		BytesCfg[0] |= 0x04;
	}

	if( inkjet)
	{
		BytesCfg[0] |= 0x08;
	}

	if( condensat )
	{
		BytesCfg[0] |= 0x10;
	}
	if( timbro)
	{
		BytesCfg[0] |= 0x20;
	}

	//if( IdentStr[1] & 0x01 )						// 00000001
	//	PeriphFeatures.ScannerFront = TRUE;

	//if( IdentStr[1] & 0x02 )						// 00000010
	//	PeriphFeatures.ScannerBack = TRUE;
// per il momento gli scanner ci sono SEMPRE 
//	if( scannerfronte && scannerretro )
	{
		BytesCfg[1] |= 0x03;		
	}

	if( badge12)
	{
		BytesCfg[1] |= 0x04;
	}
	if( badge23)
	{
		BytesCfg[1] |= 0x08;
	}
	if( badge123)
	{
		BytesCfg[1] |= 0x10;
	}

	if (ATMMode )
		BytesCfg[1] |= 0x20 ; 

	if(SwAbilitato >0)
	{
		BytesCfg[3] |= SwAbilitato;
	}

	//if( IdentStr[3] & 0x01 )						// 00000001
	//	PeriphFeatures.LicenseTopImage = TRUE;

	//if( IdentStr[3] & 0x02 )						// 00000010
	//	PeriphFeatures.LicensePDF417 = TRUE;

	//if( IdentStr[3] & 0x04 )						// 00000100
	//	PeriphFeatures.LicenseIQA = TRUE;

	//if( IdentStr[3] & 0x08 )						// 00001000
	//	PeriphFeatures.LicenseMicroHole = TRUE;


	//Setto le Configurazioni Scanner (UV o COLOR )
	switch( ScannerType )
	{
	case 62:	// Scanner a grigio
		break;
	case 63:	// Scanner a colori
		BytesCfg[2] |= MASK_SCANNER_COLOR;
		break;
	case 64:	// Scanner a grigio e UV
		BytesCfg[2] |= MASK_SCANNER_UV;
		break;
	case 75:	// Scanner a colori e UV ...per ora non esiste ancora...
		BytesCfg[2] |= MASK_SCANNER_UV;
		BytesCfg[2] |= MASK_SCANNER_COLOR;
		break;
	}
	
	
	// open
	Reply = CollaudoLSConnectMyTeller((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione);
	/*if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )*/
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{

		// Setto la configurazione corrente

			
		Reply = LS40_WriteConfiguration((HWND)hDlg,BytesCfg );
		
		Reply = LS_OKAY ; 
		//Reply = LSSetConfiguration(Connessione,(HWND)hDlg,BytesCfg );
		//if(Reply == LS_OKAY)
		//{

		//	Reply =	LSUnitIdentify(Connessione,(HWND)0, IdentStr,LsName,Version,Date_Fw,SerialNumber,BoardNr, NULL,StampaVer, NULL, NULL, NULL, NULL, NULL);
		//	if (Reply == LS_OKAY)
		//	{
		//		// verifico la configurazione scritta 
		//		// testina micr
		//		if((IdentStr[0] & 0x01 ) == (E13B || CMC7))		
		//		{
		//			// card
		//			if(((IdentStr[0] & 0x02 )== 0x02) == (card))
		//			{	// stampante
		//				if((( IdentStr[0] & 0x08) == 0x08 )== (inkjet)) 
		//				{
		//					// devo ancora verificare che il piastrino funzioni...
		//					Reply = LSPeripheralStatus(Connessione,(HWND)hDlg,Sense,Status);
		//					if (Reply == LS_OKAY)
		//					{
		//						// verifica bit
		//						if(!((Status[0] & 0x20)  == 0x20 ))
		//						{
		//							//
		//							if(((IdentStr[0] & 0x10 )==0x10) == (condensat))
		//							{
		//								if(((IdentStr[0] & 0x20 )== 0x20 )==(timbro))
		//								{
		//									//if( IdentStr[1] & 0x01 )						// 00000001
		//									//ConfScannerFront = true;

		//									//if( IdentStr[1] & 0x02 )						// 00000010
		//									//ConfScannerBack = true;

		//									if(((IdentStr[1] & 0x04 )== 0x04) == (badge12))
		//									{
		//										if(((IdentStr[1] & 0x08 )== 0x08) == (badge23))
		//										{
		//											if(((IdentStr[1] & 0x10 )== 0x10) == (badge123))
		//											{
		//											}
		//											else
		//											{
		//												// badge 3

		//												Reply =S_LS40_BADGE_NOT_CONFIGURED;
		//												ReturnC->FunzioneChiamante = "LSIdentify";
		//											}
		//										}	
		//										else
		//										{
		//											// badge 2
		//											Reply = S_LS40_BADGE_NOT_CONFIGURED;
		//											ReturnC->FunzioneChiamante = "LSIdentify";
		//										}
		//									}
		//									else
		//									{
		//										// badge 1
		//										Reply = S_LS40_BADGE_NOT_CONFIGURED;
		//										ReturnC->FunzioneChiamante = "LSIdentify";
		//									}
		//								}
		//								else
		//								{
		//									// tmbro
		//									Reply = S_LS40_TIMBRO_NOT_CONFIGURED;
		//									ReturnC->FunzioneChiamante = "LSIdentify";
		//								}
		//							}
		//							else
		//							{
		//								// condensatori
		//								Reply = S_LS40_CONDENS_NOT_CONFIGURED;
		//								ReturnC->FunzioneChiamante = "LSIdentify";
		//							}
		//						}
		//						else
		//						{
		//							// hardware error la tentina non funziona
		//							Reply = S_LS40_INK_HW_ERROR;
		//							ReturnC->FunzioneChiamante = "LSIdentify";
		//						}
		//					}
		//					else
		//					{
		//						// stato non abuon fine
		//						ReturnC->FunzioneChiamante = "LSIdentify";
		//					}
		//				}
		//				else
		//				{
		//					//  INK non configurata
		//					Reply = S_LS40_INK__NOT_CONFIGURED;
		//					ReturnC->FunzioneChiamante = "LSIdentify";
		//				}
		//			}
		//			else
		//			{
		//				// errore Card
		//				Reply = S_LS40_CARD__NOT_CONFIGURED;
		//				ReturnC->FunzioneChiamante = "LSIdentify";
		//			}
		//		}
		//		else
		//		{
		//			// errore micr
		//			Reply = S_LS40_MICR__NOT_CONFIGURED;
		//			ReturnC->FunzioneChiamante = "LSIdentify";
		//		}
		//	}
		//	else
		//	{
		//		ReturnC->FunzioneChiamante = "LSIdentify";
		//	}
		//}
	}	
	// close
	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Set Configurazione";

	//LSDisconnect(Connessione,(HWND)hDlg);


	return Reply;
}


/*
int Ls40Class::Periferica::SetValoreDefImmagine()
{
	short NewValue;
	short Connessione=0;
	
	NewValue = 1427;

	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,Connessione);
	if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
		Reply = LSImageCalibration((HWND)hDlg, TRUE, &NewValue);
	}
	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Calibra immagine";

	LSDisconnect(Connessione,(HWND)hDlg);


	return Reply;
}

*/

int Ls40Class::Periferica::CalibraImmagine()
{

	int ii=0;
	short ret=0;
	short Connessione=0;

		// open
	//Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione);
	/*if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )*/
	//if (Reply == LS_OKAY )
	//{
		Reply = DoImageCalibration((HWND)hDlg, 1654);
	//}	
	// close

	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Calibra immagine";

	
	//LSDisconnect(Connessione,(HWND)hDlg);


	return Reply;

}


int Ls40Class::Periferica::CalibraImmagineNew()
{

	int ii=0;
	short ret=0;
	short Connessione=0;

		// open
	//Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione);
	/*if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )*/
	//if (Reply == LS_OKAY )
	//{
		Reply = DoImageCalibrationNew((HWND)hDlg, 1902);
	//}	
	// close

	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Calibra immagine";

	
	//LSDisconnect(Connessione,(HWND)hDlg);


	return Reply;

}



/*

int Ls40Class::Periferica::StatoPeriferica()
{
	unsigned char Sense;
	unsigned char Status[16];
	short Connessione=0;
	ReturnC->FunzioneChiamante = "Stato Periferica";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,Connessione);
	if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	if ((Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN))
	{
		Reply = LSPeripheralStatus((HWND)hDlg, &Sense, Status);
		
		StatusByte[0] = Status[0];
		StatusByte[1] = Status[1];
		StatusByte[2] = Status[2];
		StatusByte[3] = Status[3];
		StatusByte[4] = Status[4];
		StatusByte[5] = Status[5];
		StatusByte[6] = Status[6];
		StatusByte[7] = Status[7];
		StatusByte[8] = Status[8];
		StatusByte[9] = Status[9];
		StatusByte[10] = Status[10];
		StatusByte[11] = Status[11];
		StatusByte[12] = Status[12];
		StatusByte[13] = Status[13];
		StatusByte[14] = Status[14];
		StatusByte[15] = Status[15];
		LSDisconnect(Connessione,(HWND)hDlg);

	}
	ReturnC->ReturnCode = Reply;

	return Reply;
}

*/
int Ls40Class::Periferica::DoImageCalibration(HWND hDlg, float TeoricValue)
{
	long	*BufFrontImage;
	int	nRet;
	int fretry;
	float	RealValue, DiffPercentuale;
	short	OldValue, NewValue;
	short Connessione=0;
	short Tentativi;
	short ScanMode;
	short ColNereAnt;
	int Reply2   = LS_OKAY ;
	char IdentStr[64],Date_Fw[64],LsName[64],SerialNumber[64];
	char StrStampa[64];
	char BoardNr[8];
	char CpldNr[8];
	char Version[64];
	
	
	CpldNr[1]= 0;
	CpldNr[2]= 0;


	

	nRet = TRUE;
	fretry = FALSE;
	
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica, &Connessione);
	/*if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )*/
	Reply =	LSUnitIdentify(Connessione,(HWND)0, IdentStr,LsName,Version,Date_Fw,SerialNumber,BoardNr, NULL,StrStampa, NULL, NULL, NULL, NULL, NULL);

	if (Reply == LS_OKAY )
	{
		Tentativi = 0;
		do
		{
			
			ScanMode = SCAN_MODE_256GR200 ;
	
			// Chiamo la funzione di via documento
			Reply = LSDocHandle(Connessione, (HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								SIDE_FRONT_IMAGE,
								ScanMode,
								AUTO_FEED,
								HOLD_DOCUMENT,
								WAIT_YES,
								NO_BEEP,
								NULL,
								0,
								0);
			if(Reply == LS_OKAY)
			{		
				//-----------ReadImage-------------------------------------
					Reply = LSReadImage(Connessione, hDlg,
										CLEAR_ALL_BLACK , //NO_CLEAR_BLACK,
										SIDE_FRONT_IMAGE,
										0, 0,
										(LPHANDLE)&BufFrontImage,
										NULL,
										NULL,
										NULL);
				if(Reply == LS_OKAY)
				{
						ColNereAnt = 0 ;
						// Tolgo il nero davanti e dietro e NON sopra per avere 864 righe
						//ClearFrontAndRearBlack( (BITMAPINFOHEADER *)BufFrontImage,&ColNereAnt );
						
						
						// Leggo lunghezza image
						RealValue = (float)((BITMAPINFOHEADER *)BufFrontImage)->biWidth;
						RealValue -= ColNereAnt;
						ValoreLunghezzaImmagine= RealValue;
						// Leggo il valore settato nella periferica
						Reply = LSImageCalibration(Connessione,hDlg, FALSE, &OldValue);
						if( Reply == LS_OKAY )
						{
							Reply = LSDocHandle(Connessione, hDlg,
										NO_STAMP,
										NO_PRINT_VALIDATE,
										NO_READ_CODELINE,
										SIDE_NONE_IMAGE,
										SCAN_MODE_256GR200,
										PATH_FEED,
										EJECT_DOCUMENT,
										WAIT_YES,
										NO_BEEP,
										NULL,
										0, 0);

							DiffPercentuale = TeoricValue * (float)0.005;		// 0.5%

							if(	RealValue > (TeoricValue + DiffPercentuale) )
							{
								// Calcolo il valore in percentuale da sommare a quello presente nella perifeica
								DiffPercentuale = RealValue - TeoricValue;
								DiffPercentuale = DiffPercentuale * (float)100 / TeoricValue;
								NewValue = (unsigned short)((float)OldValue * DiffPercentuale) / 100;

								// Cambia il tempo di colonna come gli altri LS !!!
								NewValue += OldValue;

								Reply = RETRY_IMAGE_CALIBRATION;	// Devo riprovare la taratura
							}


							// Controllo lunghezza image
							DiffPercentuale = TeoricValue * (float)0.005;		// 0.5%

							if( RealValue < (TeoricValue - DiffPercentuale) )
							{
								// Calcolo il valore in percentuale da sommare a quello presente nella perifeica
								DiffPercentuale = TeoricValue - RealValue;
								DiffPercentuale = DiffPercentuale * (float)100 / TeoricValue;
								NewValue = (unsigned short)((float)OldValue * DiffPercentuale) / 100;


								// Cambia il tempo di colonna come gli altri LS !!!
								NewValue = OldValue - NewValue;

								Reply = RETRY_IMAGE_CALIBRATION;	// Devo riprovare la taratura
							}


							// Se Reply = RETRY_IMAGE_CALIBRATION ho variato i valori,
							// perciò vado a scrivere quelli nuovi
							if( Reply == RETRY_IMAGE_CALIBRATION )
							{
								// Con le nuove versioni NON faccio piu' il controllo !
								if( Version[0] <= '2' )
								{
									// Controllo che non sia inferiore al 10% del default 0x69d
									//if( NewValue < (0x69d * 0.90) )
									//	NewValue = (short)(0x69d * 0.90);		// forzo il default ?

									// Controllo che non sia superiore al 10% del default 1100
									//if( NewValue > (0x69d * 1.10) )
									//	NewValue = (short)(0x69d * 1.10);		// forzo il default ?
									
									//modifiche Ferragatta 18-06-2015
									// Controllo che non sia inferiore al 10% del default 0x615
									if( NewValue < (0x615 * 0.90) )
										NewValue = (short)(0x615 * 0.90);		// forzo il default ?

									// Controllo che non sia superiore al 10% del default 1100
									if( NewValue > (0x615 * 1.10) )
										NewValue = (short)(0x0615 * 1.10);		// forzo il default ?
								}


								Reply2 = LSImageCalibration(Connessione, hDlg, TRUE, &NewValue);
								if( Reply2 != LS_OKAY )
								{
									CheckReply(hDlg, Reply2, "LS40_ImageCalibration");
					//				LSDisconnect(hLS, hDlg);
									return Reply2;
								}
								//else
								//{
								//	Reply = RETRY_IMAGE_CALIBRATION;
								//}
							}

							// Libero le aree bitmap ritornate
							if( BufFrontImage )
								GlobalFree( BufFrontImage );
						}
						else
						{
							CheckReply(hDlg, Reply, "LSImageCalibration");					
						}

				}
				else
				{
					// read image errore
				}

			}
			else
			{
				// via documento errore
			}

			
				

			Tentativi ++;
		} while( (Reply == RETRY_IMAGE_CALIBRATION) && (Tentativi <= 8));
	}
	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Calibra immagine";
	
	LSDocHandle(Connessione, hDlg,
				NO_STAMP,
				NO_PRINT_VALIDATE,
				READ_CODELINE_MICR,
				SIDE_NONE_IMAGE,
				SCAN_MODE_256GR200,
				AUTO_FEED,
				SORTER_BAY1,
				WAIT_YES,
				NO_BEEP,
				NULL,
				0, 0);
	
	LSDisconnect(Connessione,(HWND)hDlg);

	return Reply;
} // DoImageCalibration



int Ls40Class::Periferica::DoImageCalibrationNew(HWND hDlg, float TeoricValue)
{
	struct GRIDPRO_GeometryCriteria pGeoCriteria;
	struct GRIDPRO_PointCriteria pPntCriteria ;
	struct GRIDPRO_Point p1 ; 
	struct GRIDPRO_Point p2 ; 
	struct GRIDPRO_Point p3 ; 
	struct GRIDPRO_Point p4 ; 
	int pError ;
	GRIDHANDLE  handle ;
	int Rret = 0 ;
    double dx = 0 ;
	double dy = 0 ;
	double dx1 = 0 ;
	double dy1 = 0 ;
	int d = 0 ;
	int d1 = 0 ;
	double ax = 0 ;
	double ax1 = 0 ;
	int ad = 0 ;
	int ad1 = 0 ;
	long	*BufFrontImage;
	int	nRet;
	int fretry;
	float	RealValue, DiffPercentuale;
	short	OldValue, NewValue;
	short Connessione=0;
	short Tentativi;
	short ScanMode;
	short ColNereAnt;
	int Reply2   = LS_OKAY ;
	char IdentStr[64],Date_Fw[64],LsName[64],SerialNumber[64];
	char StrStampa[64];
	char BoardNr[8];
	char CpldNr[8];
	char Version[64];
	
	
	CpldNr[1]= 0;
	CpldNr[2]= 0;


	

	nRet = TRUE;
	fretry = FALSE;
	
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica, &Connessione);
	/*if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )*/
	Reply =	LSUnitIdentify(Connessione,(HWND)0, IdentStr,LsName,Version,Date_Fw,SerialNumber,BoardNr, NULL,StrStampa, NULL, NULL, NULL, NULL, NULL);

	if (Reply == LS_OKAY )
	{
		Tentativi = 0;
		do
		{
			
			ScanMode = SCAN_MODE_256GR300;
	
			// Chiamo la funzione di via documento
			Reply = LSDocHandle(Connessione, (HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								SIDE_FRONT_IMAGE,
								ScanMode,
								AUTO_FEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								0,
								0);
			if(Reply == LS_OKAY)
			{		
				//-----------ReadImage-------------------------------------
					Reply = LSReadImage(Connessione, hDlg,
										CLEAR_ALL_BLACK , //NO_CLEAR_BLACK,
										SIDE_FRONT_IMAGE,
										0, 0,
										(LPHANDLE)&BufFrontImage,
										NULL,
										NULL,
										NULL);
				if(Reply == LS_OKAY)
				{

						// Leggo il valore settato nella periferica
						Reply = LSImageCalibration(Connessione,hDlg, FALSE, &OldValue);
						if( Reply == LS_OKAY )
						{
							ColNereAnt = 0 ;
					pGeoCriteria.columns =2 ;
					pGeoCriteria.rows = 2;
					pGeoCriteria.dispX = (double)161 ; 
					pGeoCriteria.dispY = (double)78 ;
					pGeoCriteria.deformPercX = (double)0.24 ; 
					pGeoCriteria.deformPercY = (double)0.24 ; 
					pPntCriteria.th = 90 ; 
					pPntCriteria.filledDensity.min = (double)0.5 ; 
					pPntCriteria.filledDensity.max  = (double)1.0 ; 
					pPntCriteria.emptyDensity.min = (double)0.5; 
					pPntCriteria.emptyDensity.max =  (double)1.0 ; 
					pPntCriteria.diameter.min = (double)1.5 ;
					pPntCriteria.diameter.max = (double)2.5 ;
		
					handle = GRIDPRO_NewGrid(GRIDPRO_SINGLE_MODE,( const unsigned char * )BufFrontImage,&pGeoCriteria,&pPntCriteria,&pError);

					if (pError == GRIDPRO_OK )
					{
						Rret  =GRIDPRO_GetGridPointAt(handle,0, 0,&p1 ) ; 
						
						Rret  =GRIDPRO_GetGridPointAt(handle,1, 0,&p2 ) ;

						Rret  =GRIDPRO_GetGridPointAt(handle,0, 1,&p3 ) ;

						Rret  =GRIDPRO_GetGridPointAt(handle,1, 1,&p4 ) ;

						GRIDPRO_DeleteGrid(handle);

						dx1 = (p4.x - p3.x );
						dy1 = (p4.y - p3.y );

						dx = (p2.x - p1.x );
						dy = (p2.y - p1.y );

						ax = (p3.y - p1.y );
						ax1 = (p4.y - p2.y ) ; 


						d = (int)sqrtf((float)(dx*dx+dy*dy));
						d1 = (int)sqrtf((float)(dx1*dx1+dy1*dy1));

						if((d > (d1 - 10)) || (d < (d1 + 10)))
						{
							Reply = LS_OKAY;
						}
						else
						{
							fretry = TRUE;
							Reply = LS_RETRY;
							return Reply;
						}

						if((ax > (ax1 - 10)) || (ax < (ax1 + 10)))
						{
							Reply = LS_OKAY;
						}
						else
						{
							fretry = TRUE;
							Reply = LS_RETRY;
							return Reply;
						}

					}
					else
					{
						fretry = TRUE;
						Reply = LS_RETRY;
						return Reply;
					}

					// Leggo lunghezza image
					RealValue = (float)d ; 

					ValoreLunghezzaImmagine= RealValue;

							DiffPercentuale = TeoricValue * (float)0.005;		// 0.5%

							if(	RealValue > (TeoricValue + DiffPercentuale) )
							{
								// Calcolo il valore in percentuale da sommare a quello presente nella perifeica
								DiffPercentuale = RealValue - TeoricValue;
								DiffPercentuale = DiffPercentuale * (float)100 / TeoricValue;
								NewValue = (unsigned short)((float)OldValue * DiffPercentuale) / 100;

								// Cambia il tempo di colonna come gli altri LS !!!
								NewValue += OldValue;

								Reply = RETRY_IMAGE_CALIBRATION;	// Devo riprovare la taratura
							}


							// Controllo lunghezza image
							DiffPercentuale = TeoricValue * (float)0.005;		// 0.5%

							if( RealValue < (TeoricValue - DiffPercentuale) )
							{
								// Calcolo il valore in percentuale da sommare a quello presente nella perifeica
								DiffPercentuale = TeoricValue - RealValue;
								DiffPercentuale = DiffPercentuale * (float)100 / TeoricValue;
								NewValue = (unsigned short)((float)OldValue * DiffPercentuale) / 100;


								// Cambia il tempo di colonna come gli altri LS !!!
								NewValue = OldValue - NewValue;

								Reply = RETRY_IMAGE_CALIBRATION;	// Devo riprovare la taratura
							}


							// Se Reply = RETRY_IMAGE_CALIBRATION ho variato i valori,
							// perciò vado a scrivere quelli nuovi
							if( Reply == RETRY_IMAGE_CALIBRATION )
							{
							


								Reply2 = LSImageCalibration(Connessione, hDlg, TRUE, &NewValue);
								if( Reply2 != LS_OKAY )
								{
									CheckReply(hDlg, Reply2, "LS40_ImageCalibration");
					//				LSDisconnect(hLS, hDlg);
									return Reply2;
								}
								//else
								//{
								//	Reply = RETRY_IMAGE_CALIBRATION;
								//}
							}

							// Libero le aree bitmap ritornate
							if( BufFrontImage )
								GlobalFree( BufFrontImage );
						}
						else
						{
							CheckReply(hDlg, Reply, "LSImageCalibration");					
						}

				}
				else
				{
					// read image errore
				}

			}
			else
			{
				// via documento errore
			}

			
				

			Tentativi ++;
		} while( (Reply == RETRY_IMAGE_CALIBRATION) && (Tentativi <= 8));
	}
	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Calibra immagine";
	
	LSDisconnect(Connessione,(HWND)hDlg);

	return Reply;
} // DoImageCalibration



//int Ls40Class::Periferica::DoImageCalibrationNewCancellare(HWND hDlg, float TeoricValue)
//{
//	struct GRIDPRO_GeometryCriteria pGeoCriteria;
//	struct GRIDPRO_PointCriteria pPntCriteria ;
//	struct GRIDPRO_Point p1 ; 
//	struct GRIDPRO_Point p2 ; 
//	struct GRIDPRO_Point p3 ; 
//	struct GRIDPRO_Point p4 ; 
//	int pError ;
//	GRIDHANDLE  handle ;
//	int Rret = 0 ;
//    double dx = 0 ;
//	double dy = 0 ;
//	double dx1 = 0 ;
//	double dy1 = 0 ;
//	int d = 0 ;
//	int d1 = 0 ;
//	double ax = 0 ;
//	double ax1 = 0 ;
//	int ad = 0 ;
//	int ad1 = 0 ;
//	long *BufFrontImage;
//	int	nRet;
//	int fretry;
//	float RealValue, DiffPercentuale;
//	short OldValue, NewValue;
//	short Connessione=0;
//	short Tentativi;
//	short ScanMode;
//	short ColNereAnt;
//	int Reply2   = LS_OKAY ;
//	char IdentStr[64],Date_Fw[64],LsName[64],SerialNumber[64];
//	char StrStampa[64];
//	char BoardNr[8];
//	char CpldNr[8];
//	char Version[64];
//
//	CpldNr[1]= 0;
//	CpldNr[2]= 0;
//
//	nRet = TRUE;
//	fretry = FALSE;
//	
//	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica, &Connessione);
//	Reply =	LSUnitIdentify(Connessione,(HWND)0, IdentStr,LsName,Version,Date_Fw,SerialNumber,BoardNr, NULL,StrStampa, NULL, NULL, NULL, NULL, NULL);
//
//	if (Reply == LS_OKAY )
//	{
//		Tentativi = 0;
//		//do
//		{
//			ScanMode = SCAN_MODE_256GR300 ;
//			// Chiamo la funzione di via documento
//			Reply = LSDocHandle(Connessione, (HWND)hDlg,
//								NO_STAMP,
//								NO_PRINT_VALIDATE,
//								NO_READ_CODELINE,
//								SIDE_FRONT_IMAGE,
//								ScanMode,
//								AUTO_FEED,
//								SORTER_BAY1,
//								WAIT_YES,
//								NO_BEEP,
//								NULL,
//								0,
//								0);
//			if(Reply == LS_OKAY)
//			{		
//				//-----------ReadImage-------------------------------------
//				Reply = LSReadImage(Connessione, hDlg,
//									CLEAR_ALL_BLACK , //NO_CLEAR_BLACK,
//									SIDE_FRONT_IMAGE,
//									0, 0,
//									(LPHANDLE)&BufFrontImage,
//									NULL,
//									NULL,
//									NULL);
//				if(Reply == LS_OKAY)
//				{
//					ColNereAnt = 0 ;
//					pGeoCriteria.columns =2 ;
//					pGeoCriteria.rows = 2;
//					pGeoCriteria.dispX = (double)161 ; 
//					pGeoCriteria.dispY = (double)78 ;
//					pGeoCriteria.deformPercX = (double)0.24 ; 
//					pGeoCriteria.deformPercY = (double)0.24 ; 
//					pPntCriteria.th = 90 ; 
//					pPntCriteria.filledDensity.min = (double)0.5 ; 
//					pPntCriteria.filledDensity.max  = (double)1.0 ; 
//					pPntCriteria.emptyDensity.min = (double)0.5; 
//					pPntCriteria.emptyDensity.max =  (double)1.0 ; 
//					pPntCriteria.diameter.min = (double)1.5 ;
//					pPntCriteria.diameter.max = (double)2.5 ;
//		
//					handle = GRIDPRO_NewGrid(GRIDPRO_SINGLE_MODE,( const unsigned char * )BufFrontImage,&pGeoCriteria,&pPntCriteria,&pError);
//
//					if (pError == GRIDPRO_OK )
//					{
//						Rret  =GRIDPRO_GetGridPointAt(handle,0, 0,&p1 ) ; 
//						
//						Rret  =GRIDPRO_GetGridPointAt(handle,1, 0,&p2 ) ;
//
//						Rret  =GRIDPRO_GetGridPointAt(handle,0, 1,&p3 ) ;
//
//						Rret  =GRIDPRO_GetGridPointAt(handle,1, 1,&p4 ) ;
//
//						GRIDPRO_DeleteGrid(handle);
//
//						dx1 = (p4.x - p3.x );
//						dy1 = (p4.y - p3.y );
//
//						dx = (p2.x - p1.x );
//						dy = (p2.y - p1.y );
//
//						ax = (p3.y - p1.y );
//						ax1 = (p4.y - p2.y ) ; 
//
//
//						d = (int)sqrtf((float)(dx*dx+dy*dy));
//						d1 = (int)sqrtf((float)(dx1*dx1+dy1*dy1));
//
//						if((d > (d1 - 10)) || (d < (d1 + 10)))
//						{
//							Reply = LS_OKAY;
//						}
//						else
//						{
//							fretry = TRUE;
//							Reply = LS_RETRY;
//							return Reply;
//						}
//
//						if((ax > (ax1 - 10)) || (ax < (ax1 + 10)))
//						{
//							Reply = LS_OKAY;
//						}
//						else
//						{
//							fretry = TRUE;
//							Reply = LS_RETRY;
//							return Reply;
//						}
//
//					}
//					else
//					{
//						fretry = TRUE;
//						Reply = LS_RETRY;
//						return Reply;
//					}
//
//					// Leggo lunghezza image
//					RealValue = (float)d ; 
//
//					ValoreLunghezzaImmagine= RealValue;
//					// Leggo il valore settato nella periferica
//					Reply = LSImageCalibration(Connessione,hDlg, FALSE, &OldValue);
//
//					if( Reply != LS_OKAY )
//					{
//						CheckReply(hDlg, Reply, "LSImageCalibration");
//						fretry = TRUE;
//						return Reply;
//					}
//
//						
//
//					// Controllo lunghezza image
//					DiffPercentuale = TeoricValue * (float)0.004;		// 0.4%
//
//					if( RealValue < (TeoricValue - DiffPercentuale) )
//					{
//						// Calcolo il valore in percentuale da sommare a quello presente nella perifeica
//						DiffPercentuale = TeoricValue - RealValue;
//						DiffPercentuale = DiffPercentuale * (float)100 / TeoricValue;
//						NewValue = (unsigned short)((float)OldValue * DiffPercentuale) / 100;
//
//
//						// Cambia il tempo di colonna come gli altri LS !!!
//						NewValue = OldValue - NewValue;
//						
//						nRet = FALSE;	// Devo riprovare la calibrazione
//						fretry = TRUE;
//						Reply = LS_RETRY;	// Devo riprovare la taratura
//
//					}
//
//
//
//					if( RealValue > (TeoricValue + DiffPercentuale) )
//					{
//						// Calcolo il valore in percentuale da sommare a quello presente nella perifeica
//						DiffPercentuale = RealValue - TeoricValue;
//						DiffPercentuale = DiffPercentuale * (float)100 / TeoricValue;
//
//						NewValue = (unsigned short)((float)OldValue * DiffPercentuale) / 100;
//						NewValue += OldValue;
//
//					
//
//						nRet = FALSE;	// Devo riprovare la calibrazione
//						fretry = TRUE;
//						Reply = LS_RETRY;
//					}
//
//
//					// Se nRet = FALSE ho variato i valori, perciò vado a scrivere quelli nuovi
//					if( fretry == TRUE)
//					{
//						
//						// Controllo che non sia inferiore al 1% del default
//						if( NewValue < (SCANNER_TIME_DEFAULT_300 * 0.99) )
//							NewValue = SCANNER_TIME_DEFAULT_300;	// forzo il default
//
//						// Controllo che non sia superiore al 3% del default
//						if( NewValue > (SCANNER_TIME_DEFAULT_300 * 1.03) )
//							NewValue = SCANNER_TIME_DEFAULT_300;		// forzo il default
//				
//
//						Reply = LSImageCalibration(Connessione,hDlg, TRUE, &NewValue);
//						if( Reply != LS_OKAY )
//						{
//							CheckReply(hDlg, Reply, "LSImageCalibration");
//							nRet = TRUE;
//							fretry = TRUE;
//							return Reply;
//							
//						}
//						else
//						{
//							Reply = LS_RETRY;
//						}
//					}
//
//					// Libero le aree bitmap ritornate
//					if( BufFrontImage )
//						GlobalFree( BufFrontImage );
//				}
//			}
//		}//while( (Reply == RETRY_IMAGE_CALIBRATION) && (Tentativi <= 8));
//	}
//	//LSDisconnect(Connessione,(HWND)hDlg);
//	return Reply;
//}// DoImageCalibration
//
//
//



//int Ls40Class::Periferica::DoImageCalibrationNew(HWND hDlg, float TeoricValue)
//{
//	HANDLE	BufFrontImage = 0 ; 
//	int	nRet;
//	int fretry;
//	float RealValue, DiffPercentuale;
//	short OldValue, NewValue;
//	struct GRIDPRO_GeometryCriteria pGeoCriteria ; 
//	struct GRIDPRO_PointCriteria pPntCriteria ;
//	struct GRIDPRO_Point p1 ; 
//	struct GRIDPRO_Point p2 ; 
//	struct GRIDPRO_Point p3 ; 
//	struct GRIDPRO_Point p4 ; 
//	int pError ;
//	GRIDHANDLE  handle ;
//	int Rret = 0 ;
//    double dx = 0 ;
//	double dy = 0 ;
//	double dx1 = 0 ;
//	double dy1 = 0 ;
//	int d = 0 ;
//	int d1 = 0 ;
//	double ax = 0 ;
//	double ax1 = 0 ;
//	int ad = 0 ;
//	int ad1 = 0 ;
//
//	
//	nRet = TRUE;
//	fretry = FALSE;
//	
//	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica, &Connessione);
//	Reply =	LSUnitIdentify(Connessione,(HWND)0, IdentStr,LsName,Version,Date_Fw,SerialNumber,BoardNr, NULL,StrStampa, NULL, NULL, NULL, NULL, NULL);
//
//	if( Reply != LS_OKAY )
//	{
//		fretry = TRUE;
//		return Reply;	
//	}
//	else
//	{
//		ScanMode = SCAN_MODE_256GR200 ;
//		// Chiamo la funzione di via documento
//		Reply = LSDocHandle(Connessione, (HWND)hDlg,
//							NO_STAMP,
//							NO_PRINT_VALIDATE,
//							NO_READ_CODELINE,
//							SIDE_FRONT_IMAGE,
//							ScanMode,
//							AUTO_FEED,
//							HOLD_DOCUMENT,
//							WAIT_YES,
//							NO_BEEP,
//							NULL,
//							0,
//							0);
//		if (Reply != LS_OKAY)
//		{
//			fretry = TRUE;
//			return Reply;
//		}
//		Reply = LSReadImage(Connessione, hDlg,
//										CLEAR_ALL_BLACK , //NO_CLEAR_BLACK,
//										SIDE_FRONT_IMAGE,
//										0, 0,
//										(LPHANDLE)&BufFrontImage,
//										NULL,
//										NULL,
//										NULL);
//		if (Reply != LS_OKAY)
//		{
//			fretry = TRUE;
//			return Reply;
//		}
//	
//		//Clear front and rear
//
//		pGeoCriteria.columns =2 ;
//		pGeoCriteria.rows = 2;
//		pGeoCriteria.dispX = (double)161 ; 
//		pGeoCriteria.dispY = (double)78 ;
//		pGeoCriteria.deformPercX = (double)0.24 ; 
//		pGeoCriteria.deformPercY = (double)0.24 ; 
//
//		pPntCriteria.th = 90 ; 
//		pPntCriteria.filledDensity.min = (double)0.5 ; 
//		pPntCriteria.filledDensity.max  = (double)1.0 ; 
//		pPntCriteria.emptyDensity.min = (double)0.5; 
//		pPntCriteria.emptyDensity.max =  (double)1.0 ; 
//		pPntCriteria.diameter.min = (double)1.5 ;
//		pPntCriteria.diameter.max = (double)2.5 ;
//
//		handle = GRIDPRO_NewGrid(GRIDPRO_SINGLE_MODE,( const unsigned char * )BufFrontImage,&pGeoCriteria,&pPntCriteria,&pError);
//
//		if (pError == GRIDPRO_OK )
//		{
//			Rret  =GRIDPRO_GetGridPointAt(handle,0, 0,&p1 ) ; 
//			
//			Rret  =GRIDPRO_GetGridPointAt(handle,1, 0,&p2 ) ;
//
//			Rret  =GRIDPRO_GetGridPointAt(handle,0, 1,&p3 ) ;
//
//			Rret  =GRIDPRO_GetGridPointAt(handle,1, 1,&p4 ) ;
//
//			GRIDPRO_DeleteGrid(handle);
//
//			dx1 = (p4.x - p3.x );
//			dy1 = (p4.y - p3.y );
//
//			dx = (p2.x - p1.x );
//			dy = (p2.y - p1.y );
//
//			ax = (p3.y - p1.y );
//			ax1 = (p4.y - p2.y ) ; 
//
//
//			d = (int)sqrtf((float)(dx*dx+dy*dy));
//			d1 = (int)sqrtf((float)(dx1*dx1+dy1*dy1));
//
//			if((d > (d1 - 10)) || (d < (d1 + 10)))
//			{
//				Reply = LS_OKAY;
//			}
//			else
//			{
//				fretry = TRUE;
//				Reply = LS_RETRY;
//				return Reply;
//			}
//
//			if((ax > (ax1 - 10)) || (ax < (ax1 + 10)))
//			{
//				Reply = LS_OKAY;
//			}
//			else
//			{
//				fretry = TRUE;
//				Reply = LS_RETRY;
//				return Reply;
//			}
//
//		}
//		else
//		{
//			fretry = TRUE;
//			Reply = LS_RETRY;
//			return Reply;
//		}
//
//		// Leggo lunghezza image
//		RealValue = (float)d ; 
//		Reply = LSImageCalibration(Connessione, hDlg, FALSE, &OldValue);
//		if( Reply != LS_OKAY )
//		{
//			CheckReply(hDlg, Reply, "LSImageCalibration");
//			fretry = TRUE;
//			return Reply;
//		}
//		// Controllo lunghezza image
//		DiffPercentuale = TeoricValue * (float)0.004;		// 0.4%
//
//		if(	RealValue < (TeoricValue - DiffPercentuale) )
//		{
//			// Calcolo il valore in percentuale da sommare a quello presente nella perifeica
//			DiffPercentuale = TeoricValue - RealValue;
//			DiffPercentuale = DiffPercentuale * (float)100 / TeoricValue;
//
//			NewValue = (unsigned short)((float)OldValue * DiffPercentuale) / 100;
//			NewValue = OldValue - NewValue;
//
//			
//
//			nRet = FALSE;	// Devo riprovare la calibrazione
//			fretry = TRUE;
//			Reply = LS_RETRY;
//			
//		}
//
//
//		if( RealValue > (TeoricValue + DiffPercentuale) )
//		{
//			// Calcolo il valore in percentuale da sommare a quello presente nella perifeica
//			DiffPercentuale = RealValue - TeoricValue;
//			DiffPercentuale = DiffPercentuale * (float)100 / TeoricValue;
//
//			NewValue = (unsigned short)((float)OldValue * DiffPercentuale) / 100;
//			NewValue += OldValue;
//
//		
//
//			nRet = FALSE;	// Devo riprovare la calibrazione
//			fretry = TRUE;
//			Reply = LS_RETRY;
//		}
//
//
//		// Se nRet = FALSE ho variato i valori, perciò vado a scrivere quelli nuovi
//		if( fretry == TRUE)
//		{
//			
//			// Controllo che non sia inferiore al 1% del default
//			if( NewValue < (SCANNER_TIME_DEFAULT * 0.99) )
//				NewValue = SCANNER_TIME_DEFAULT;	// forzo il default
//
//			// Controllo che non sia superiore al 3% del default
//			if( NewValue > (SCANNER_TIME_DEFAULT * 1.03) )
//				NewValue = SCANNER_TIME_DEFAULT;		// forzo il default
//	
//
//			Reply = LSImageCalibration(Connessione,hDlg, TRUE, &NewValue);
//			if( Reply != LS_OKAY )
//			{
//				CheckReply(hDlg, Reply, "LSImageCalibration");
//				nRet = TRUE;
//				fretry = TRUE;
//				return Reply;
//				
//			}
//			else
//			{
//				Reply = LS_RETRY;
//			}
//		}
//
//		// Libero le aree bitmap ritornate
//		if( BufFrontImage )
//			GlobalFree( BufFrontImage );
//	}
//	LSDocHandle(Connessione, hDlg,
//				NO_STAMP,
//				NO_PRINT_VALIDATE,
//				READ_CODELINE_MICR,
//				SIDE_NONE_IMAGE,
//				SCAN_MODE_256GR200,
//				AUTO_FEED,
//				SORTER_BAY1,
//				WAIT_YES,
//				NO_BEEP,
//				NULL,
//				0, 0);
//	
//	LSDisconnect(Connessione,(HWND)hDlg);
//
//	return Reply;
//}





void Ls40Class::Periferica::ClearFrontAndRearBlack(BITMAPINFOHEADER *Bitmap, short *ColNereAnt)
{
	unsigned long ii, jj;
	long	CondDib;
	short	ValoriOk, NrColonneNere;
	long	NrByte;
	BOOL	ContinueClear;
	BOOL	HalfByteHigh;
	unsigned char *pbm, *pbmt, *pbmtm;
	unsigned char HighValue,MaskBit;
	long	RealWidth, OldWidth, DiffWidth;
	long	nr_col_bmp, nr_row_bmp;
	

	// test se la bitmap è in bianco nero o a livelli di grigio
	if( Bitmap->biBitCount == 1 )
	{
		// Bitmap a B/N
//	SalvaDIBBitmap(Bitmap, "g:\\Progetti\\bmp\\front2.bmp");

		// Inizio dati bitmap
		pbm = (unsigned char *)Bitmap + sizeof(BITMAPINFOHEADER) + (2 * sizeof(RGBQUAD));
		NrByte = Bitmap->biWidth / 8;
		if(CondDib = (Bitmap->biWidth % 32) )
			nr_col_bmp = (Bitmap->biWidth + 32 - CondDib) / 8;
		else
			nr_col_bmp = NrByte;
		nr_row_bmp = Bitmap->biHeight;
		pbmt = pbm;
		ContinueClear = TRUE;
		HalfByteHigh = TRUE;
		ValoriOk = 0;
		NrColonneNere = 0;
		MaskBit = 0x80;

		// Tolgo le colonne nere davanti
		do
		{
			for( ii = 0; ii < (unsigned long)nr_row_bmp; ii ++, pbmt += nr_col_bmp)
			{
				// Controllo tutti bit
				if( *pbmt & MaskBit )
					ValoriOk ++;
			}

			//Incremento il counter nr. colonne nere
			if( ValoriOk <= NR_VALORI_OK )
			{
				NrColonneNere ++;
				ValoriOk = 0;
				if( MaskBit & 0x01 )
					MaskBit = 0x80;
				else
					MaskBit >>= 1;
			}
			else
				ContinueClear = FALSE;

			pbmt = pbm;
			pbmt += (NrColonneNere / 8);

		} while( ContinueClear && (NrColonneNere < (nr_col_bmp * 8)) );

		// SALVO il nr. di colonne nere anteriori
		*ColNereAnt = (NrColonneNere % 8);

		//Controllo se shiftare la bitmap
		if( NrColonneNere && (NrColonneNere < (nr_col_bmp * 8)) )
		{
			// Inizializzo i pointer
			pbmt = pbmtm = pbm;
			NrByte = nr_col_bmp;

			// Calcolo il NUOVO numero di colonne e lo salvo
			nr_col_bmp *= 8;
			nr_col_bmp -= NrColonneNere;
			Bitmap->biWidth = nr_col_bmp;

			if( CondDib = (nr_col_bmp % 32) )
				nr_col_bmp = (nr_col_bmp + 32 - CondDib);

			// Trasformo in nr. di byte
			NrColonneNere /= 8;

			// Sposto la bitmap
			for( ii = 0; ii < (unsigned long)Bitmap->biHeight; ii ++)
			{
				pbmt = pbm + (NrByte * ii) + NrColonneNere;
//				for( jj = NrColonneNere; jj < (unsigned long)NrByte; jj ++)
				for( jj = 0; jj < (unsigned long)(Bitmap->biWidth / 8); jj ++)
					*(pbmtm++) = *(pbmt++);

				// Aggiungo il nero
//				for( jj = 0; jj < (unsigned long)((nr_col_bmp - Bitmap->biWidth) / 8); jj ++)
				for( ; jj < (unsigned long)(nr_col_bmp / 8); jj ++)
					*(pbmtm++) = 0x00;	//NERO;
			}

			// Aggiorno il nr. di bytes della bitmap
			Bitmap->biSizeImage = Bitmap->biHeight * nr_col_bmp / 8;
		}



//	SalvaDIBBitmap(Bitmap, "\\Progetti\\bmp\\front3.bmp");

		// Inizio dati bitmap
		pbm = (unsigned char *)Bitmap + sizeof(BITMAPINFOHEADER) + (2 * sizeof(RGBQUAD));
		NrByte = Bitmap->biWidth / 8;
		if(CondDib = (Bitmap->biWidth % 32) )
			nr_col_bmp = (Bitmap->biWidth + 32 - CondDib) / 8;
		else
			nr_col_bmp = NrByte;
		nr_row_bmp = Bitmap->biHeight;
		pbmt = pbm + (nr_col_bmp - 1);
		ContinueClear = TRUE;
		ValoriOk = 0;
		NrColonneNere = 0;
		MaskBit = 0x01;

		// Tolgo le colonne nere dietro
		do
		{
			for( ii = 0; ii < (unsigned long)nr_row_bmp; ii ++, pbmt += nr_col_bmp)
			{
				// Controllo tutti bit
				if( *pbmt & MaskBit )
					ValoriOk ++;
			}

			//Incremento il counter nr. colonne nere
			if( ValoriOk <= NR_VALORI_OK )
			{
				NrColonneNere ++;
				ValoriOk = 0;
				if( MaskBit & 0x80 )
					MaskBit = 0x01;
				else
					MaskBit <<= 1;
			}
			else
				ContinueClear = FALSE;

			// Reinizializzo il pointer ad inizio bitmap
			pbmt = pbm + (nr_col_bmp - 1) - (NrColonneNere / 8);

		} while( ContinueClear && (NrColonneNere < (nr_col_bmp * 8)) );

		if( NrColonneNere && (NrColonneNere < (nr_col_bmp * 8)) )
		{
			// Sottraggo dal nr. di colonne nere trovate il nr di colonne di arrotondamento
			if( CondDib )
				NrColonneNere -= (short)(32 - CondDib);

			OldWidth = Bitmap->biWidth;

			// Aggiorno l'header della bitmap
			Bitmap->biWidth -= NrColonneNere;

			RealWidth = Bitmap->biWidth;

			// Calcolo la larghezza reale della bitmap
			if( CondDib = OldWidth % 32 )
				OldWidth += (32 - CondDib);

			if( CondDib = RealWidth % 32 )
				RealWidth += (32 - CondDib);

			// Aggiorno la size image
			Bitmap->biSizeImage = (Bitmap->biHeight * RealWidth) / 8;

			DiffWidth = OldWidth - RealWidth;

			// Arrotondo al byte
			DiffWidth /= 8;

			//Controllo se shiftare la bitmap
			if( DiffWidth >= 4 )
			{
				// Porto le misure in bytes
				OldWidth /= 8;
				RealWidth /= 8;

				// Sposto la bitmap
				for( ii = 1; ii < (unsigned long)Bitmap->biHeight; ii ++)
				{
					pbmtm = pbm + (RealWidth * ii);
					pbmt = pbmtm + (DiffWidth * ii);
					for( jj = 0; jj < (unsigned long)RealWidth; jj ++)
						*(pbmtm++) = *(pbmt++);
				}
			}
		}

//	SalvaDIBBitmap(Bitmap, "\\Progetti\\bmp\\front_BW.bmp");

	}
	//Test se è una bitmap a 16 o 256 livelli di grigio
	else if( Bitmap->biBitCount == 4 )
	{
		// Bitmap a 16 grigi

//	SalvaDIBBitmap(Bitmap, "e:\\bmp\\front.bmp");

		// Inizio dati bitmap
		pbm = (unsigned char *)Bitmap + sizeof(BITMAPINFOHEADER) + (16 * sizeof(RGBQUAD));
		NrByte = Bitmap->biWidth / 2;
		if(CondDib = (Bitmap->biWidth % 8) )
			nr_col_bmp = (Bitmap->biWidth + 8 - CondDib) / 2;
		else
			nr_col_bmp = NrByte;
		nr_row_bmp = Bitmap->biHeight;
		pbmt = pbm;
		ContinueClear = TRUE;
		HalfByteHigh = TRUE;
		ValoriOk = 0;
		NrColonneNere = 0;

		// Tolgo le colonne nere davanti
		do
		{
			for( ii = 0; ii < (unsigned long)nr_row_bmp; ii ++, pbmt += nr_col_bmp)
			{
				// Prendo il valore di grigio parte alta se colonna pari, altrimenti parte bassa
				if( HalfByteHigh )
					HighValue = *pbmt >> 4;
				else
					HighValue = *pbmt & 0x0f;

				if( HighValue > Limite16Grigi )
					ValoriOk ++;
			}

			//Incremento il counter nr. colonne nere
			if( ValoriOk <= NR_VALORI_OK )
			{
				NrColonneNere ++;
				ValoriOk = 0;
			}
			else
				ContinueClear = FALSE;

			// Reinizializzo il pointer ad inizio bitmap
			pbmt = pbm + (NrColonneNere / 2);

			HalfByteHigh = !HalfByteHigh;

		} while( ContinueClear && (NrColonneNere < (nr_col_bmp * 2)) );


		//Controllo se shiftare la bitmap
		if( NrColonneNere && (NrColonneNere < (nr_col_bmp * 2)) )
		{
			// Inizializzo i pointer
			pbmt = pbmtm = pbm;
			NrByte = nr_col_bmp;

			// Calcolo il NUOVO numero di colonne e lo salvo
			nr_col_bmp *= 2;
			nr_col_bmp -= NrColonneNere;
			Bitmap->biWidth = nr_col_bmp;

			if( CondDib = (nr_col_bmp % 8) )
				nr_col_bmp = (nr_col_bmp + 8 - CondDib);

			// Trasformo in nr. di byte
			NrColonneNere /= 2;

			// Sposto la bitmap
			for( ii = 0; ii < (unsigned long)Bitmap->biHeight; ii ++)
			{
				pbmt += NrColonneNere;
				for( jj = NrColonneNere; jj < (unsigned long)NrByte; jj ++)
					*(pbmtm++) = *(pbmt++);

				// Aggiungo il nero
				for( jj = 0; jj < (unsigned long)((nr_col_bmp - Bitmap->biWidth) / 2); jj ++)
					*(pbmtm++) = 0x00;	//NERO;
			}

			// Aggiorno il nr. di bytes della bitmap
			Bitmap->biSizeImage = Bitmap->biHeight * nr_col_bmp / 2;
		}

//	SalvaDIBBitmap(Bitmap, "g:\\Progetti\\bmp\\front3.bmp");


		// Inizio dati bitmap
		pbm = (unsigned char *)Bitmap + sizeof(BITMAPINFOHEADER) + (16 * sizeof(RGBQUAD));
		NrByte = Bitmap->biWidth / 2;
		if(CondDib = (Bitmap->biWidth % 8) )
			nr_col_bmp = (Bitmap->biWidth + 8 - CondDib) / 2;
		else
			nr_col_bmp = NrByte;
		nr_row_bmp = Bitmap->biHeight;
		pbmt = pbm + (nr_col_bmp - 1);
		ContinueClear = TRUE;
		HalfByteHigh = FALSE;
		ValoriOk = 0;
		NrColonneNere = 0;

// --- Secondo me sono inutili
//		NrByte /= ALLIGN_COLONNE;
//		NrByte *= ALLIGN_COLONNE;

		// Tolgo le colonne nere dietro
		do
		{
			for( ii = 0; ii < (unsigned long)nr_row_bmp; ii ++, pbmt += nr_col_bmp)
			{
				// Prendo il valore di grigio parte alta se colonna pari, altrimenti parte bassa
				if( HalfByteHigh )
					HighValue = *pbmt >> 4;
				else
					HighValue = *pbmt & 0x0f;

				if( HighValue > Limite16Grigi )
					ValoriOk ++;
			}

			//Incremento il counter nr. colonne nere
			if( ValoriOk <= NR_VALORI_OK )
			{
				NrColonneNere ++;
				ValoriOk = 0;
			}
			else
				ContinueClear = FALSE;

			// Reinizializzo il pointer ad inizio bitmap
			pbmt = pbm + (nr_col_bmp - 1) - (NrColonneNere / 2);

			HalfByteHigh = !HalfByteHigh;

		} while( ContinueClear && (NrColonneNere < (nr_col_bmp * 2)) );

		if( NrColonneNere && (NrColonneNere < (nr_col_bmp * 2)) )
		{
			DiffWidth = Bitmap->biWidth;

			// Arrotondo il numero di colonne nere ad un nr. pari
			if( NrColonneNere % 2 )
				NrColonneNere --;

			// Aggiorno l'header della bitmap
			Bitmap->biWidth -= NrColonneNere;

			RealWidth = Bitmap->biWidth;

			// Calcolo la larghezza reale della bitmap
			if( CondDib = DiffWidth % 8 )
				DiffWidth += (8 - CondDib);

			if( CondDib = RealWidth % 8 )
				RealWidth += (8 - CondDib);

			DiffWidth -= RealWidth;

			Bitmap->biSizeImage = (Bitmap->biHeight * RealWidth) / 2;

			// Arrotondo al byte
			RealWidth /= 2;
			DiffWidth /= 2;

			//Controllo se shiftare la bitmap
			if( NrColonneNere )
			{
				pbm += RealWidth;
				pbmt = pbm;

				// Sposto la bitmap
				for( ii = 1; ii < (unsigned long)Bitmap->biHeight; ii ++)
				{
					pbmt += DiffWidth;
					for( jj = 0; jj < (unsigned long)RealWidth; jj ++)
						*(pbm++) = *(pbmt++);
				}
			}
		}

//	SalvaDIBBitmap(Bitmap, "g:\\Progetti\\bmp\\front4.bmp");

	}
	else
	{
		// Bitmap a 256 grigi

//	SalvaDIBBitmap(Bitmap, "e:\\bmp\\front.bmp");

		// Inizio dati bitmap
		pbm = (unsigned char *)Bitmap + sizeof(BITMAPINFOHEADER) + (256 * sizeof(RGBQUAD));
		if(CondDib = (Bitmap->biWidth % 4) )
			nr_col_bmp = (Bitmap->biWidth + 4 - CondDib);
		else
			nr_col_bmp = Bitmap->biWidth;
		nr_row_bmp = Bitmap->biHeight;
		ContinueClear = TRUE;
		ValoriOk = 0;
		NrColonneNere = 0;

		// Tolgo le colonne nere davanti
		pbmt = pbm;
		do
		{
			for( ii = 0; ii < (unsigned long)nr_row_bmp; ii ++, pbmt += nr_col_bmp)
				if( *pbmt > Limite256Grigi )
					ValoriOk ++;

			//Controllo se eliminare la colonna
			if( ValoriOk <= NR_VALORI_OK )
			{
				NrColonneNere ++;
				ValoriOk = 0;
			}
			else
				ContinueClear = FALSE;

			// Reinizializzo il pointer ad inizio bitmap
			pbmt = pbm + NrColonneNere;

		} while( ContinueClear && (NrColonneNere < nr_col_bmp) );


		//Controllo se shiftare la bitmap
		if( NrColonneNere && (NrColonneNere < nr_col_bmp) )
		{
			// Inizializzo i pointer
			pbmt = pbmtm = pbm;
			NrByte = nr_col_bmp;

			// Calcolo il NUOVO numero di colonne e lo salvo
			nr_col_bmp -= NrColonneNere;
			Bitmap->biWidth = nr_col_bmp;

			if( CondDib = (nr_col_bmp % 4) )
				nr_col_bmp = (nr_col_bmp + 4 - CondDib);

			// Sposto la bitmap
			for( ii = 0; ii < (unsigned long)Bitmap->biHeight; ii ++)
			{
				pbmt += NrColonneNere;
				for( jj = NrColonneNere; jj < (unsigned long)NrByte; jj ++)
					*(pbmtm++) = *(pbmt++);

				// Aggiungo il nero
				for( jj = 0; jj < (unsigned long)(nr_col_bmp - Bitmap->biWidth); jj ++)
					*(pbmtm++) = 0x00;	//NERO;
			}

			// Aggiorno il nr. di bytes della bitmap
			Bitmap->biSizeImage = Bitmap->biHeight * nr_col_bmp;
		}

//	SalvaDIBBitmap(Bitmap, "g:\\Progetti\\bmp\\front3.bmp");


		// Inizio dati bitmap
		pbm = (unsigned char *)Bitmap + sizeof(BITMAPINFOHEADER) + (256 * sizeof(RGBQUAD));
		if(CondDib = (Bitmap->biWidth % 4) )
			nr_col_bmp = (Bitmap->biWidth + 4 - CondDib);
		else
			nr_col_bmp = Bitmap->biWidth;
		nr_row_bmp = Bitmap->biHeight;
		pbmt = pbm + nr_col_bmp - 1;
		ContinueClear = TRUE;
		ValoriOk = 0;
		NrColonneNere = 0;

		// Tolgo le colonne nere dietro
		do
		{
			for( ii = 0; ii < (unsigned long)nr_row_bmp; ii ++, pbmt += nr_col_bmp)
				if( *pbmt > Limite256Grigi )
					ValoriOk ++;

			//Incremento il counter nr. colonne nere
			if( ValoriOk <= NR_VALORI_OK )
			{
				NrColonneNere ++;
				ValoriOk = 0;
			}
			else
				ContinueClear = FALSE;

			// Reinizializzo il pointer ad inizio bitmap
			pbmt = pbm + (nr_col_bmp - 1) - NrColonneNere;

		} while( ContinueClear && (NrColonneNere < nr_col_bmp) );

		if( NrColonneNere && (NrColonneNere < nr_col_bmp) )
		{
			DiffWidth = Bitmap->biWidth;

			// Aggiorno l'header della bitmap
			Bitmap->biWidth -= NrColonneNere;

			RealWidth = Bitmap->biWidth;

			// Calcolo la larghezza reale della bitmap
			if( CondDib = DiffWidth % 4 )
				DiffWidth += (4 - CondDib);

			if( CondDib = RealWidth % 4 )
				RealWidth += (4 - CondDib);

			DiffWidth -= RealWidth;

			Bitmap->biSizeImage = Bitmap->biHeight * RealWidth;


			//Controllo se shiftare la bitmap
			if( NrColonneNere )
			{
				pbm += RealWidth;
				pbmt = pbm;

				// Sposto la bitmap
				for( ii = 1; ii < (unsigned long)Bitmap->biHeight; ii ++)
				{
					pbmt += DiffWidth;
					for( jj = 0; jj < (unsigned long)RealWidth; jj ++)
						*(pbm++) = *(pbmt++);
				}
			}
		}

//	SalvaDIBBitmap(Bitmap, "g:\\Progetti\\bmp\\front4.bmp");

	} // Fine else

} // ClearFrontAndRearBlack
/*
int Ls40Class::Periferica::ViewE13BSignal(int fView)
{

	char BufCodelineHW[256];
	unsigned long len_codeline;
	long llDati;
	unsigned char pDati[32*1024];
	//int NrDoc;
	short NrChar;
	short NrCampioni;
	short index;
	short Connessione=0;

	 llDati = 32*1024;
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,Connessione);
	if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
		// Setto il diagnostic mode
	//	Reply = LSSetDiagnosticMode((HWND)hDlg, TRUE);
		if (Reply == LS_OKAY) 
		{
			// Chiamo la funzione passando l'ultima configurazione settata
			Reply = LSDocHandle((HWND)hDlg,
							SUSPENSIVE_MODE,
							NO_FRONT_STAMP,
							NO_PRINT_VALIDATE,
							READ_CODELINE_MICR,
							SIDE_NONE_IMAGE,
							SCAN_MODE_256GR200,
							AUTOFEED,
							SORTER_BAY1,
							WAIT_YES,
							NO_BEEP,
							0,
							0);
			if (Reply == LS_OKAY) 
			{
				// Leggo la codeline
				len_codeline = 256;
				Reply = LSReadCodeline((HWND)hDlg,
										SUSPENSIVE_MODE, 
										BufCodelineHW,
										(short *)&len_codeline,
										NULL,
										0);
				
				Reply = LSReadE13BSignal((HWND)hDlg, pDati, (long *)&llDati);
				if (Reply == LS_OKAY)
				{
					// salvo nella variabile pubblica i dati 


					if (fView)
					{
						NrChar = 0;
						while( (pDati[NrChar]) != E13B_FINE_LLC )
							NrChar ++;
						NrCampioni =(((pDati[NrChar + 2])<<8) + pDati[NrChar + 1]);
						index = NrChar + 1 + 2;		// Vado ad inizio dati segnale
					}
					else
					{
						NrCampioni	= (short)llDati;
						index = 0;
					}
					//this->E13BSignal   = new Byte[NrCampioni];
					for ( long ii = 0 ; ii<NrCampioni;ii++)
					{
						//this->E13BSignal[ii] = pDati[ii+index];
						E13BSignal[ii] = pDati[ii+index];
					}
				}
				// Tolgo il diagnostic mode
			//	LSSetDiagnosticMode((HWND)hDlg, FALSE);

				LSDisconnect(Connessione,(HWND)hDlg);

			}


		}
	}

	return Reply ;
}

*/
int Ls40Class::Periferica::DisegnaEstrectLenBar()
{
	register int  xx;
	short ii, Save_ii;
	BOOL fFine;
	short NrLenghtBar = 7;	// Nr. raggruppamento barre
	short NrCampioni;
	short NrChar, NrValori;
	short NrBarre;

	short Save_pdct;	// Ptr. temp a caratteri




//	FILE *fh;
//	char *ptmp;
//	char FileOut[_MAX_FNAME];
	short index,index2;
//	this->ViewSignal = new Byte[10000];


	NrChar = 0;
	while( (E13BSignal[NrChar]) )
		NrChar ++;

	NrCampioni = NrChar;

	index = 0;		// Vado ad inizio dati segnale



	// Cerco l'ultimo valore valido
	index2 = index + NrCampioni;

	while( E13BSignal[index2-1] < SOGLIA_INIZIO_NERO )
	{
		index2 --;
		NrCampioni --;
	}
	// Ne tengo uno dei non validi come tappo
	NrCampioni ++;

	// Elimino tre barre finali
	for( ii = 0; ii < 3; ii++)
	{
		// Elimino la prima mezza barra, un'eventuale parte nera ...
		while( E13BSignal[index2-1] < SOGLIA_INIZIO_NERO )
		{
			index2 --;
			NrCampioni --;
		}

		// ... e la parte bianca, così parto sempre da un nero
		while( E13BSignal[index2-1] >= SOGLIA_INIZIO_NERO )
		{
			index2 --;
			NrCampioni --;
		}
	}

	// Elimino tre barre iniziali
	for( ii = 0; ii < 3; ii++)
	{
		// Elimino la prima mezza barra, un'eventuale parte nera ...
		while( E13BSignal[index] < SOGLIA_INIZIO_NERO )
		{
			index ++;
			NrCampioni --;
		}

		// ... e la parte bianca, così parto sempre da un nero
		while( E13BSignal[index] >= SOGLIA_INIZIO_NERO )
		{
			index ++;
			NrCampioni --;
		}
	}

	// Conto i valori di una oscillazione

	fFine = FALSE;
	NrValori = 0;
	NrBarre = 1;
	llMax = 0;
	llMin = NrLenghtBar * 0xff;	// setto il massimo
	llMedia = 0;
	xx = 0;
	index2 = index;
	for( ii = 1; ii <= NrCampioni; ii ++)
	{
		//Sono arrivato alla fine di una barra ?
		if( (fFine == TRUE ) && (E13BSignal[index] < SOGLIA_INIZIO_NERO))
		{
			if( NrBarre == 1 )
			{
				Save_pdct = index;
				Save_ii = ii;
			}

			if( NrBarre < NrLenghtBar )
				NrBarre ++;
			else
			{
				ViewSignal[xx] = (unsigned char)NrValori;
				xx ++;

				// Se il valore esce dal grafico ... lo forzo al valore massimo
				if( NrValori > (HEIGHT_BAR * NrLenghtBar) )
					NrValori = (HEIGHT_BAR * NrLenghtBar) - 1;
				
				// Salvo il minimo ed il massimo
				if( NrValori < llMin )
					llMin = NrValori;
				if( NrValori > llMax )
					llMax = NrValori;

				// Aggiungo il valore alla media
				llMedia += NrValori;

				// Riazzero le variabili
				NrValori = 0;
				NrBarre = 1;
				index = Save_pdct;
				ii = Save_ii;
			}

			fFine = FALSE;
		}

		// Il nero lo somma per FALSE, il bianco per TRUE
		if( (fFine == FALSE) && (E13BSignal[index] >= SOGLIA_INIZIO_NERO) )
			fFine = TRUE;

		// Sposto il puntatore
		index ++;
		NrValori ++;
	}


	// Calcolo media
	llMedia = llMedia / xx;

	// Calcolo deviazione standard
	index = index2;	// Vado ad inizio dati
	DevStd = 0;
	for( ii = 1; ii <= NrCampioni; ii ++)
	{
		//Sono arrivato alla fine di una barra ?
		if( (fFine == TRUE ) && (E13BSignal[index] < SOGLIA_INIZIO_NERO) )
		{
			if( NrBarre == 1 )
			{
				Save_pdct = index;
				Save_ii = ii;
			}

			if( NrBarre < NrLenghtBar )
				NrBarre ++;
			else
			{
				DevStd += (float)(Math::Pow((double)(NrValori - llMedia), 2));

				// Riazzero le variabili
				NrValori = 0;
				NrBarre = 1;
				index = Save_pdct;
				ii = Save_ii;
			}

			fFine = FALSE;
		}

		// Il nero lo somma per FALSE, il bianco per TRUE
		if( (fFine == FALSE) && (index >= SOGLIA_INIZIO_NERO) )
			fFine = TRUE;

		// Sposto il puntatore
		index ++;
		NrValori ++;
	}

	DevStd /= xx;
	DevStd = (float)Math::Sqrt(DevStd);
return 1;
}


int Ls40Class::Periferica::Reset(short TypeReet)
{


	short Connessione=0;
	
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	if( Reply == LS_OKAY )
	{
		Reply = LSReset(Connessione,(HWND)0,(char)TypeReet);

		LSDisconnect(Connessione,(HWND)hDlg);
	}

	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Reset";

	return Reply;	
}

int Ls40Class::Periferica::DoTimeSampleMICRCalibration(char TypeSpeed,ListBox^ list)
{
	short	ii;
	//short	hLS;
	BOOL	nRet;
	float	DiffPercentuale;
	unsigned short	CurrValue, NewValue;
	float	SampleValueMin, SampleValueMax;

	//MSG		msg;

	short	nrC;
	//short	Width, Diff;
	//int		SavefOptionFitImage;
	
	//unsigned char *pb;
	
	long	nColorData = 256;
	unsigned char	*pDati;
	unsigned char buffDati[NR_BYTE_BUFFER_E13B];		// Dati da chiedere
	long	llDati; 					// Dati da chiedere
	//char	FileOut[256];
	short Connessione=0;

	//char	Version[16];
	long valMedio;
	short	Feeder , ScanMode ;
	//BOOL	fMyTeller2;
	char	IdentStr[8];
	char	LsName[20];
	char	Version[20];
	//float	ver;


	// Setto i valori iniziali
	//fMyTeller2 = FALSE;
	Feeder = PATH_FEED;
	nRet = TRUE;
	Percentile = 0;


	//-----------Open--------------------------------------------
	/*Reply = LSConnectDiagnostica((HWND)hDlg, (HANDLE)hInst, TipoPeriferica, &hLS);
	if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(hLS,(HWND)0,RESET_FREE_PATH);
	}*/
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,  &Connessione);
	if (Reply != LS_OKAY)
	{
		if( CheckReply((HWND)hDlg, Reply, "LS40_Open"))
		    return nRet;
	}

	
	if( TypeSpeed == TEST_SPEED_300_DPI )
		ScanMode = SCAN_MODE_256GR300;
	else if( TypeSpeed == TEST_SPEED_300_DPI_UV )
		ScanMode = SCAN_MODE_256GR300_AND_UV;
	else if( TypeSpeed == TEST_SPEED_300_DPI_COLOR )
		ScanMode = SCAN_MODE_COLOR_300;

	Reply = LSUnitIdentify(Connessione, (HWND)hDlg, IdentStr, LsName, Version, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
	//// Se MyTeller NON faccio trattenuto
	//ver = (float)atof(Version);
	//if( IdentStr[1] & MASK_ATM_CONFIGURATION && (ver >= LS40_VERSION_MYTELLER_2) )
	//	fMyTeller2 = TRUE;

	NrLettureOk = 0;
	// Devo fare almeno 3 letture ok
	for( ii = 0; ((ii < 13) && (NrLettureOk < 3)); ii ++)
	{
		// Chiamo la funzione passando l'ultima configurazione settata
		Reply = LSDocHandle(Connessione, (HWND)hDlg,
							NO_STAMP,
							NO_PRINT_VALIDATE,
							READ_CODELINE_MICR,
							SIDE_NONE_IMAGE,
							ScanMode ,
							AUTO_FEED,
							HOLD_DOCUMENT , //(short)(fMyTeller2 ? SORTER_BAY1 : HOLD_DOCUMENT), //HOLD_DOCUMENT,
							WAIT_YES,
							NO_BEEP,
							NULL,
							0,
							0);

		if( Reply == LS_OKAY )
		{
			llDati = NR_BYTE_BUFFER_E13B;
			Reply = LSReadE13BSignal(Connessione, (HWND)hDlg, buffDati, &llDati);
			if( Reply != LS_OKAY )
			{
				CheckReply((HWND)hDlg, Reply, "LS40_ReadE13BSignal");
				Feeder = PATH_FEED;	// Faccio uscire il documento
				break;
			}
			for( long iii = 0; iii<llDati; iii++)
			{
				this->E13BSignal[iii] = buffDati[iii];
			}
			//// Salvo su file i dati ricevuti
			//sprintf(FileOut, "dati\\SgnE13B_LS40_%04d.cts", stParDocHandle.NrNomeFile++);
			//if( (fh = fopen(FileOut, "wb")) != NULL )
			//{
			//	fwrite(buffDati, sizeof(unsigned char), llDati, fh);
			//	fclose( fh );
			//}

			CurrValue = 0 ;
			
			// Leggo il valore settato nella periferica
			Reply = LSReadTimeSampleMICR(Connessione, (HWND)hDlg, TypeSpeed, &CurrValue, 2);
			if( Reply != LS_OKAY )
			{
				CheckReply((HWND)hDlg, Reply, "LSReadTimeSampleMICR");
				Feeder = PATH_FEED;	// Faccio uscire il documento
				break;
			}


			// Visualizzo la bitmap del segnale letto
			// Conto i bytes larghezza char
			pDati = buffDati;

			// Controllo se ho trovato il fine lunghezza caratteri
			if( llDati )
			{
				// Estraggo il numero di segnali
				nrC = (short)llDati;		// Nr. campioni segnale

				if( nrC )	// Costrisco bitmap
				{
				//	Width = ((nrC / BARRE_GROUP_GRAF) + SIZE_SCALE + OFFSET_SCALE_Y);
				//	// Allineo la bitmap a multiplo di 4
				//	if( (Diff = Width % 4) )
				//		Width += (4 - Diff);

				//	pb = GlobalAlloc(GPTR, (sizeof(BITMAPINFOHEADER) + (nColorData * sizeof(RGBQUAD)) + (Width * HEIGHT_SIGNAL)));

				//	CreaWhiteBitmap(pb, (short)HEIGHT_SIGNAL, Width);

				//	// Costruisco bitmap velocità e visualizzo
					DisegnaEstrectLenBar();
					

					//// Setto FitImage = FALSE per avere la window normal
					//ConverteDIBtoLEAD((BITMAPINFO *)pb, &pbh);

					//GlobalFree( pb );

					//// Setto FitImage = FALSE per avere la window normal
					//SavefOptionFitImage = fOptionFitImage;	// Salvo il valore
					//fOptionFitImage = FALSE;

					////Setto il flag di image present
					//pbh.Flags.Allocated = TRUE;

					//sprintf(Titolo, "Bitmap Width char E13B -*- PWM = %d -*-Min = %d, Max = %d", CurrValue, llMin, llMax);
					//CreateChildWindow("", Titolo, &pbh);
					//fOptionFitImage = SavefOptionFitImage;	// Ripristino il vecchio valore
				}
			} // end if( llDati )


			// Do un colpo di messaggi per visualizzare l'immagine
			/*while( PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) )
			{
				TranslateMessage( &msg );
				DispatchMessage( &msg );
			}*/


			int conta ;
			list->Items->Add("Ciclo : " + ii.ToString() + " Valore Massimo : " + llMax.ToString() + " Valore minimo : " + llMin.ToString());
			
			conta = list->Items->Count;
			list->SelectedIndex = conta-1;
			list->Refresh();

			// Setto il valore min e max
			SampleValueMin = SAMPLE_VALUE_MIN;
			SampleValueMax = SAMPLE_VALUE_MAX;

			// Controllo differenza minimo massimo
			if( (llMax - llMin) > 11 )
			{
			//	MessageBox((HWND)hDlg, "Bad assembly !", TITLE_ERROR, MB_ICONERROR | MB_OK);
				Reply = LS_CALIBRATION_FAILED;
				Feeder = PATH_FEED;	// Faccio uscire il documento
				break;
			}


			// Controllo il valore massimo
			if( llMax >= SampleValueMax )
			{
				// Calcolo il valore in percentuale da sommare a quello presente nella perifeica
				DiffPercentuale = (llMax * CurrValue) / SampleValueMax;
				if( (DiffPercentuale - (long)DiffPercentuale) >= 0.5 )
					NewValue = (unsigned short)(DiffPercentuale + 1);
				else
					NewValue = (unsigned short)DiffPercentuale;

				if( NewValue == CurrValue )
					NewValue ++;

				nRet = FALSE;	// Devo riprovare la calibrazione
				NrLettureOk = 0;
			}

			// Controllo il valore minimo
			else if( llMin < SampleValueMin )
			{
				// Calcolo il valore in percentuale da sommare a quello presente nella perifeica
				DiffPercentuale = (llMin * CurrValue) / SampleValueMin;
				if( (DiffPercentuale - (long)DiffPercentuale) >= 0.5 )
					NewValue = (unsigned short)(DiffPercentuale + 1);
				else
					NewValue = (unsigned short)DiffPercentuale;

				if( NewValue == CurrValue )
					NewValue --;

				nRet = FALSE;	// Devo riprovare la calibrazione
				NrLettureOk = 0;
			}


			// Se i valori stanno nei Min e Max controllo se la media è vicino a 112
			else if( (llMin >= SampleValueMin) && (llMax <= SampleValueMax) )
			{
				valMedio = (llMax + llMin) / 2;

				// Sommo dei valori a seconda della distanza
				if( (valMedio - SAMPLE_VALUE_MEDIO) >= 1 )
					NewValue = CurrValue + 1;
				else if( (valMedio - SAMPLE_VALUE_MEDIO) <= -1 )
					NewValue = CurrValue - 1;
				else
					NewValue = CurrValue;


				NrLettureOk ++;
				nRet = FALSE;	// Devo riprovare la calibrazione
			}


			// Se nRet = FALSE ho variato i valori, perciò vado a scrivere quelli nuovi
			if( nRet == FALSE )
			{
				// Controllo che non sia superiore a default + 10%
	//			if( NewValue > (TIME_SAMPLE_DEFAULT * 1.10) )
	////				NewValue = TIME_SAMPLE_DEFAULT;	// forzo il default
	//				nRet = TRUE;	// Lascio il valore invariato ?

				// Controllo che non sia inferiore a default - 10%
	//			if( NewValue < (TIME_SAMPLE_DEFAULT * 0.90) )
	////				NewValue = TIME_SAMPLE_DEFAULT;	// forzo il default
	//				nRet = TRUE;	// Lascio il valore invariato ?


				Reply = LSSetTimeSampleMICR(Connessione, (HWND)hDlg, TypeSpeed, NewValue);
				if( Reply != LS_OKAY )
				{
					CheckReply((HWND)hDlg, Reply, "LSSetTimeSampleMICR");
					Reply = LS_CALIBRATION_FAILED;
					Feeder = PATH_FEED;	// Faccio uscire il documento
				}
			}

			//if( ! fMyTeller2 )
			{
				// Riporto il documento in bocchetta
				Reply = LSDocHandle(Connessione, (HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								SIDE_NONE_IMAGE,
								SCAN_MODE_256GR200,
								Feeder,
								EJECT_DOCUMENT,
								WAIT_YES,
								NO_BEEP,
								NULL,
								0, 0);

				if( Reply != LS_OKAY )
				{
					CheckReply((HWND)hDlg, Reply, "LS40_DocHandle");
					break;
				}
			}
		}
		else
		{
			CheckReply((HWND)hDlg, Reply, "LS40_DocHandle");
		}
	} // for( ii = 0; ((ii < 13) && (NrLettureOk < 3)); ii ++)
	if( (ii >= 13) && (NrLettureOk < 3) )
		Reply = LS_CALIBRATION_FAILED;


	//if( ! fMyTeller2 )
		// Sorto il documento
		LSDocHandle(Connessione, (HWND)hDlg,
				NO_STAMP,
				NO_PRINT_VALIDATE,
				READ_CODELINE_MICR,
				SIDE_NONE_IMAGE,
				SCAN_MODE_256GR200,
				AUTO_FEED,
				SORTER_BAY1,
				WAIT_YES,
				NO_BEEP,
				NULL,
				0, 0);
    LSDisconnect(Connessione, (HWND)hDlg);

	return Reply;
} // DoTimeSampleMICRCalibration




 int Ls40Class::Periferica::EraseHistory()
{
	S_HISTORY_LS40 History;
	short Connessione=0;


	ReturnC->FunzioneChiamante = "Cancellazione Storico";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione);
	/*if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}*/
	if( Reply == LS_OKAY )
	{
		Reply = LSHistoryCommand(Connessione,(HWND)hDlg, CMD_READ_HISTORY, &History);

		S_Storico->Documenti_Trattati = History.doc_sorted;
		S_Storico->Documenti_Trattenuti = History.doc_retain;
		S_Storico->Errori_CMC7 = History.doc_cmc7_err;
		S_Storico->Errori_E13B = History.doc_e13b_err;
		S_Storico->Jam_Card = History.paper_jam;
		
		S_Storico->Num_Accensioni = History.num_turn_on;
		S_Storico->Doc_Timbrati = History.doc_stamp;
		S_Storico->TempoAccensione = History.time_peripheral_on;
		S_Storico->Documenti_Trattati = History.doc_sorted;

		Reply = LSHistoryCommand(Connessione,(HWND)hDlg, CMD_ERASE_HISTORY, NULL);
		if( Reply != LS_OKAY )
		{
			ReturnC->FunzioneChiamante = "LSHistoryCommand";
		}

		LSDisconnect(Connessione,(HWND)hDlg);
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSConnect";
	}


	ReturnC->ReturnCode = Reply;
	return Reply;
}

/*


int Ls40Class::Periferica::AccendiLuce(short Luce)
{
	short ret=0;
	short Connessione=0;
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,Connessione);
	if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	if( (Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN) )
	{
		Reply = LSSetScannerLight((HWND)hDlg, Luce);	
		LSDisconnect((HWND)hDlg, SUSPENSIVE_MODE);
	}
	else
	{
		CheckReply(0, Reply, "LSConnect");
	}
	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Test Luci";

	return ret;
}


int Ls40Class::Periferica::MotoreFeeder(short Acceso)
{
	short ret=0;
	short Connessione=0;
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,Connessione);
	if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	if( (Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN) )
	{
		Reply = LSMoveMotorFeeder((HWND)hDlg, Acceso);	
		LSDisconnect((HWND)hDlg, SUSPENSIVE_MODE);
	}
	else
	{
		CheckReply(0, Reply, "LSConnect");
	}
	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Test Motore Feeder";

	return ret;
}
//
int Ls40Class::Periferica::sensorFeeder()
{
	short ret=0;
	unsigned short  aa;
	short Connessione=0;
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,Connessione);
	if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	if( (Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN) )
	{
		Reply = LSReadFeederMotorAD((HWND)hDlg,&aa);	
		Sensore = aa;
		LSDisconnect((HWND)hDlg, SUSPENSIVE_MODE);
	}
	else
	{
		CheckReply(0, Reply, "LSConnect");
	}
	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Test Motore Feeder";

	return ret;

}
*/
int Ls40Class::Periferica::TestCinghia()
{
	unsigned char pDati[50*1024];
	long lunDati= 50*1024;
	int ii, max, min;
	float media;
	int somma;
	int MinScarto,MaxScarto;
	short Connessione=0;	

	MinScarto = 110;
	MaxScarto = 145;
	ReturnC->FunzioneChiamante = " Test cinghia";
	
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione);
	/*if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )*/
	if (Reply == LS_OKAY )
		{//----------- Disabilito la doppia sfogliatura -----------
		//Reply = LSDoubleLeafingSensibility((HWND)hDlg, DOUBLE_LEAFING_DISABLE);
	//	Reply  = LSConfigDoubleLeafing((HWND)hDlg,DOUBLE_LEAFING_ERROR,DOUBLE_LEAFING_DISABLE,0);
		
		Reply = LSTestCinghia(Connessione,(HWND)hDlg,pDati,&lunDati);
		if(Reply == LS_OKAY)
		{
			max = 0;
			min = 255;
			media = 0;
			somma = 0;
			if (lunDati <= 400)
			{
				ReturnC->FunzioneChiamante = "LSTestCinghia";
				Reply = -7000;
			}
			else
			{
				for(ii = 10; ii < lunDati-400;ii++)
				{	
					
					if (pDati[ii] < min )
						min = pDati[ii]; 
					if (pDati[ii] > max )
						max = pDati[ii];
					somma += pDati[ii];
				}
				media  = (float)(somma / (lunDati-410));	
				if((min < MinScarto) || (max > MaxScarto))
				{
					ReturnC->FunzioneChiamante = "LSTestCinghia";
					Reply = -6000;
					
				}
				else
				{

				}
				ValMinCinghia = min;
				ValMaxCinghia = max;
			}
		}
		else
		{
			ReturnC->FunzioneChiamante = "LSTestCinghia";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSConnect";
	}
	ReturnC->ReturnCode = Reply;
	
	LSDisconnect(Connessione,(HWND)hDlg);
	return Reply;
}





int Ls40Class::Periferica::TestCinghiaEx(int Thn, int Thp, int DocSignalValue)
{
	short Connessione;
	struct _TESTBELTPARAM TestBeltParam;

	ReturnC->FunzioneChiamante = "TestCinghiaEx";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	if( Reply == LS_OKAY )
	{
		
		//Imposto il Trimmer a 40
		Reply = LSSetTrimmerMICR(Connessione, (HWND)hDlg,  TEST_SPEED_300_DPI, 40);
		//Reply = LSSetTrimmerMICR(Connessione, (HWND)hDlg,  TEST_SPEED_200_DPI, 40);
		
		if(Reply == LS_OKAY)
		{
			//----------- Disabilito la doppia sfogliatura -----------
			//Reply = LSConfigDoubleLeafingEx(Connessione ,(HWND)hDlg ,  (short)DOUBLE_LEAFING_WARNING , DOUBLE_LEAFING_DISABLE, 0 ) ; 

			TestBeltParam.NegativeThreshold = Thn ; 
			TestBeltParam.PositiveThreshold = Thp ;
			TestBeltParam.DocSignalValue = DocSignalValue ;
			

			Reply = LSTestCinghiaEx(Connessione,(HWND)hDlg,&TestBeltParam);
			if(Reply == LS_OKAY)
			{
				if (TestBeltParam.TestResult == 1 )
				{
					Reply = LS_OKAY ; 
								//lo risetto...
			//Chiediamo a D. Loggia di implementare nel software di collaudo (asap) una funzione che prima del test belt forzi a 128 il valore del trimmer sulla piastra a velocità di 300dpi (velocità del test)
			LSSetTrimmerMICR(Connessione, (HWND)hDlg,  TEST_SPEED_300_DPI, 128);
			//Reply = LSSetTrimmerMICR(Connessione, (HWND)hDlg,  TEST_SPEED_200_DPI, 128);
				}
				else
				{
					Reply = -6000; 
					TestBeltParam.SignalValueRead = 0 ;
				}
			}
			else
			{
				LSReset(Connessione,(HWND)0,RESET_ERROR);
				ReturnC->FunzioneChiamante = "LSTestCinghiaEx";
			}

			ValMinCinghiaNew = TestBeltParam.MinNegativeValue ;
		    ValMaxCinghiaNew = TestBeltParam.MaxPositiveValue;
			SignalValueRead = TestBeltParam.SignalValueRead ;
			NrPicchi = 	TestBeltParam.NrPicchi ; 


		}
		else
		{
			ReturnC->FunzioneChiamante = "LSSetTrimmerMICR";
		}

	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}
	ReturnC->ReturnCode = Reply;
	
	LSDisconnect(Connessione,(HWND)hDlg);
	return Reply;
}

/*

int Ls40Class::Periferica::LeggiValoriCIS()
{
	//
	//short Connessione=0;
	//char pColonne[5000];
	//AD_LM98714 sScanner ;//= new AD_LM98714;

	//
	//Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,Connessione);
	if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	//if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	//{
	//	Reply = LSScannerReadValue((HWND)hDlg, pColonne, &sScanner, 0);	
	//	strCis->Add_PWM_Blue_F = sScanner.Add_PWM_Blue_F;
	//	strCis->Add_PWM_Blue_R = sScanner.Add_PWM_Blue_R;
	//	strCis->Add_PWM_Green_F = sScanner.Add_PWM_Green_F;
	//	strCis->Add_PWM_Green_R = sScanner.Add_PWM_Green_R;
	//	strCis->Add_PWM_Red_F = sScanner.Add_PWM_Red_F;
	//	strCis->Add_PWM_Red_R = sScanner.Add_PWM_Red_R;
	//	
	//	
	//	strCis->Dato_Gain_F = sScanner.Dato_Gain_F;
	//	strCis->Dato_Gain_R = sScanner.Dato_Gain_R;
	//	strCis->Dato_Offset_F = sScanner.Dato_Offset_F;
	//	strCis->Dato_Offset_R = sScanner.Dato_Offset_R;
	//	strCis->Dato_PWM_Blue_F = sScanner.Dato_PWM_Blue_F;
	//	strCis->Dato_PWM_Blue_R = sScanner.Dato_PWM_Blue_R;
	//	strCis->Dato_PWM_Green_F = sScanner.Dato_PWM_Green_F;
	//	strCis->Dato_PWM_Green_R = sScanner.Dato_PWM_Green_R;
	//	strCis->Dato_PWM_Red_F = sScanner.Dato_PWM_Red_F;
	//	strCis->Dato_PWM_Red_R = sScanner.Dato_PWM_Red_R;
	//	


	//}
	//ReturnC->ReturnCode = Reply;
	//
	//LSDisconnect((HWND)hDlg, SUSPENSIVE_MODE);
	//return Reply;
	//
	return 0;
}




//extern int APIENTRY LSMoveMotorFeeder(HWND, short);

//extern int APIENTRY LSReadFeederMotorAD(HWND, unsigned short *);

int Ls40Class::Periferica::SetDiagnostic(int fbool)
{
	short Connessione=0;
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,Connessione);
	if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
		//
		
		Reply = LSSetDiagnosticMode((HWND)hDlg,fbool);
	}
	LSDisconnect((HWND)hDlg, SUSPENSIVE_MODE);
	return Reply;
}


int Ls40Class::Periferica::SetVelocita(int fSpeed)
{
	short Connessione=0;
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,Connessione);
	if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
		//
		Reply = LSSetUnitSpeed((HWND)hDlg,fSpeed);
	}
	LSDisconnect((HWND)hDlg, SUSPENSIVE_MODE);
	return Reply;
}
*/





int Ls40Class::Periferica::CheckReply(HWND hDlg,int Rep,char *s)
{
	return TRUE;
}


//*******************************************************************
// FUNCTION  : Media50Barrette(unsigned char *pd, char *pb, short NrChar)
//
// PARAMETER :	pd		(I) Ptr buffer dati.
//
// PURPOSE   : Calcola la somma dei valori minimi.
//*******************************************************************
BOOL Ls40Class::Periferica::Media50Barrette(unsigned char *pd, long llDati,
						 short *NrPicchi, float *SommaValori, long *ValMin, short *NrPicchiMin)
{
	BOOL ret;
	short ii, Save_ii;
	long NrCampioni;
	unsigned char *pdct;

//	FILE *fh;


	// Conto i bytes larghezza char
	if( llDati )
	{
		pdct = pd;		// Vado ad inizio dati segnale

		// Tolgo i descrittori dal numero bytes
		NrCampioni = llDati;


		// Cerco i picchi e calcolo il valore medio
		*NrPicchi = *NrPicchiMin = 0;
		*ValMin = 0xff;		// setto il massimo
		*SommaValori = 0;
		Save_ii = 0;
		// Elimino una parte iniziale e finale
		for( ii = 20; ii <= (NrCampioni - 20); ii ++)
		{
			// Controllo se è un picco,
			// per essere un picco deve avere 7 valori prima e dopo di valore inferiore
			if( (pdct[ii] <= VALORE_MAX_PICCO_MIN) &&
//			if( (pdct[ii]) && (pdct[ii] <= VALORE_MAX_PICCO_MIN) &&
				((pdct[ii] <= pdct[ii-1]) && (pdct[ii] < pdct[ii+1])) &&
				((pdct[ii] <= pdct[ii-2]) && (pdct[ii] < pdct[ii+2])) &&
				((pdct[ii] <= pdct[ii-3]) && (pdct[ii] < pdct[ii+3])) &&
				((pdct[ii] <= pdct[ii-4]) && (pdct[ii] < pdct[ii+4])) &&
				((pdct[ii] <= pdct[ii-5]) && (pdct[ii] < pdct[ii+5])) &&
				((pdct[ii] <= pdct[ii-6]) && (pdct[ii] < pdct[ii+6])) &&
				((pdct[ii] <= pdct[ii-7]) && (pdct[ii] < pdct[ii+7])) )
			{
				// Controllo se il picco è ad una distanza minima
				if( (ii - Save_ii) >= PICCO_DISTANZA_MINIMA )
				{
					// Media del picco
					*SommaValori += pdct[ii];

//					if( fh = fopen("Trace_Calibrazione_MICR.txt", "at") )
//					{
//						fprintf(fh, "%d ", pdct[ii]);
//						fclose( fh );
//					}

					// Salvo il valore minimo
					if( *ValMin == pdct[ii] )
						*NrPicchiMin = *NrPicchiMin + 1;

					if( *ValMin > pdct[ii] )
					{
						*ValMin = pdct[ii];
						*NrPicchiMin = 1;
					}

					*NrPicchi = *NrPicchi + 1;
					Save_ii = ii;
				}
			}
		}

		// Calcolo media
//		*llMedia = *llMedia / *NrPicchi;

		ret = TRUE;
	}
	else
	{
		// C'è un baco che qualche volta non ritorna i dati corretti,
		// setto tutto a zero e butto l'acquisizione !!!
		*NrPicchi = *NrPicchiMin = 0;
		*ValMin = 0;
		*SommaValori = 0;
		ret = FALSE;
	}


//	if( fh = fopen("Trace_Calibrazione_MICR.txt", "at") )
//	{
//		fprintf(fh, "\n ");
//		fclose( fh );
//	}

	return ret;
}  // Media50Barrette





short Ls40Class::Periferica::GetTypeLS(String ^Version)
{

	String ^mm;
	mm =MODEL_LS40;

	//TypeLS = TYPE_LS150;
	TypeLS = 150 ; 

	//Identifico la periferica se LS150 USB only or USB/RS232
	if ( String::Compare(Version,0,MODEL_LS40,0,mm->Length) == 0)
		//TypeLS = TYPE_LS150;
		TypeLS = 40 ; 
	
	return TypeLS;
} // GetTypeLS

int Ls40Class::Periferica::TestBeep()
{
	//short ret;
	short Connessione=0;

	ReturnC->FunzioneChiamante = "TestBeep";
	
	//-----------Open--------------------------------------------
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione);
	/*if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )*/
	if (Reply == LS_OKAY )
	{
		//Reply = LSBeep(Connessione, (HWND)hDlg,5,500,200);
		Reply = LSBeep(Connessione, (HWND)hDlg,5,500,200);
		ReturnC->FunzioneChiamante =  "LSBeep";
	}
	else
	{
		ReturnC->FunzioneChiamante =  "LSConnect";
	}
	
	//-----------Close-------------------------------------------
	LSDisconnect(Connessione,(HWND)hDlg);
	ReturnC->ReturnCode = Reply;
	
	return Reply;


}



int Ls40Class::Periferica::TestLed(short Color)
{
	//short ret;
	short Connessione=0;

	ReturnC->FunzioneChiamante = "TestLed";
	
	//-----------Open--------------------------------------------
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione);
	/*if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )*/
	if (Reply == LS_OKAY )
	{
		Reply = LSLED(Connessione, (HWND)hDlg,Color,500,500);
		ReturnC->FunzioneChiamante =  "LSLed";
	}
	else
	{
		ReturnC->FunzioneChiamante =  "LSConnect";
	}
	
	//-----------Close-------------------------------------------
	LSDisconnect(Connessione,(HWND)hDlg);
	ReturnC->ReturnCode = Reply;
	
	return Reply;


}


int Ls40Class::Periferica::LeggiImmaginiAllinea(char Side,short ScanMode)
{

	HANDLE HBackGray=0;
	HANDLE HFrontGray=0;
	HANDLE HFrontUV=0;
	char *StrNome=NULL;
	short Connessione=0;
	

	short floop = false;
	ReturnC->FunzioneChiamante = "Lettura Immagini";
	
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	/*if ( Reply  == LS_TRY_TO_RESET )
	{
		Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	}
	
	if ((Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN))*/
	if (Reply == LS_OKAY )
	{
	/*	if (HFrontGray)
			LSFreeImage((HWND)hDlg, &HFrontGray);

		if (HFrontUV)
			LSFreeImage((HWND)hDlg, &HFrontUV);

		if (HBackGray)
			LSFreeImage((HWND)hDlg, &HBackGray);*/

		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,150,230);


		//Abbassa la soglia di Nero....default 0x44
		Reply = LSSetThresholdClearBlack(Connessione,(HWND)hDlg,0x33);
	
		do
		{
		

			Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								Side,
								ScanMode,
								AUTO_FEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_SHORT_DOCUMENT ,
								//SCAN_PAPER_DOCUMENT,
								0);
			if (Reply == LS_FEEDER_EMPTY)
			{
				floop = TRUE;
			}
			else
				floop = FALSE;
		}while(floop);

		if( Reply == LS_OKAY )
		{

			Reply = LSReadImage(Connessione, (HWND)hDlg,
										CLEAR_AND_ALIGN_IMAGE,
										Side,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										(LPHANDLE)&HFrontUV,
										NULL);

			//Ripristina il Default....
			LSSetThresholdClearBlack(Connessione,(HWND)hDlg,0x44);
				
			if(Reply == LS_OKAY)
			{
				// salvo le immagini
				if ((Side == SIDE_ALL_IMAGE) ||(Side == SIDE_FRONT_IMAGE))
				{
				//	CTSLoadDIBimage("Teorico.bmp",&HBackGray);
				//	SigmaRetro = CalcolaIstogramma(HBackGray,HistogrammaRetro);

//
				//	SigmaFronte = CalcolaIstogramma(HFrontGray,HistogrammaFronte);

			/*		double Pearson = 0;
					double PearsonX = 0;
					double PearsonY = 0;
					
					for (int iii = 0;iii < 256; iii++)
					{
						Pearson += HistogrammaRetro[iii] * HistogrammaFronte[iii];
						PearsonX += HistogrammaRetro[iii] ;
						PearsonY += HistogrammaFronte[iii] ;

					}
					Pearson = Pearson/256;
					PearsonX = PearsonX/256;
					PearsonY = PearsonY/256;
					Pearson = (Pearson - PearsonX * PearsonY) /(SigmaRetro * SigmaFronte);*/
				
					


					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
					Reply = LSSaveDIB((HWND)hDlg,  HFrontGray,StrNome);

					if (ScanMode == SCAN_MODE_256GR200_AND_UV)
					{
						StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
						Reply = LSSaveDIB((HWND)hDlg,  HFrontUV,StrNome);
					}
						 // Always free trhe unmanaged string.
					  Marshal::FreeHGlobal(IntPtr(StrNome));
				}
				if ((Side == SIDE_ALL_IMAGE) || (Side == SIDE_BACK_IMAGE))
				{
					
					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();
					Reply = LSSaveDIB((HWND)hDlg,HBackGray,StrNome);

					 // Always free the unmanaged string.
					  Marshal::FreeHGlobal(IntPtr(StrNome));
				}	
			}
			else
			{
				ReturnC->FunzioneChiamante = "LSReadImage";
			}
			
			LSDisconnect(Connessione,(HWND)hDlg);
		}
		else
		{
			ReturnC->FunzioneChiamante = "LSDocHandle";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}
	// libero le immagini
	if (HFrontUV)
		LSFreeImage((HWND)hDlg, &HFrontUV);
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	ReturnC->ReturnCode = Reply;
	

	return Reply;
}
int Ls40Class::Periferica::Docutest04()
{
	ReturnC->FunzioneChiamante = "Test Doppia sfogliatura";
	short Connessione;

	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	
	if( Reply == LS_OKAY )
	{
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_ERROR,50,100,180);
	
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								SIDE_NONE_IMAGE,
								SCAN_MODE_256GR300,
								AUTO_FEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);
		
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,150,180);
		
		LSDisconnect(Connessione,(HWND)hDlg);
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}

	ReturnC->ReturnCode = Reply;
	

	return Reply;	//ret
}

int Ls40Class::Periferica::CalSfogliatura()
{
	unsigned short pDump[24],Threshold[24];
	int llBuffer;
	HINSTANCE hInst = 0;
	short Connessione;
	
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	if( Reply == LS_OKAY )
	{
		// double leafing 
		Reply = LSPhotosCalibration(Connessione,(HWND)hDlg,1);
		if (Reply == LS_OKAY)
		{
			llBuffer = 32;
			LSReadPhotosValue(Connessione,(HWND)0,pDump,Threshold);
			
			if( Reply == LS_OKAY )
			{
				// Leggo il valore dei photo
				PhotoValue[0] = pDump[0];
				PhotoValue[1] = pDump[1];
				PhotoValue[2] = pDump[2];
				// Doppia sfogliatura..... 
				PhotoValueDP[0] = (unsigned char)pDump[3];
				PhotoValueDP[1] = (unsigned char)pDump[4];
				PhotoValueDP[2] = (unsigned char)pDump[5];

				PhotoValueDP[3] = (unsigned char)Threshold[0];
				PhotoValueDP[4] = (unsigned char)Threshold[1];
				PhotoValueDP[5] = (unsigned char)Threshold[2];
				PhotoValueDP[6] = (unsigned char)Threshold[3];
				PhotoValueDP[7] = (unsigned char)Threshold[4];
				PhotoValueDP[8] = (unsigned char)Threshold[5];

			}
			else
			{
				ReturnC->FunzioneChiamante= "LSReadPhotosValue";
			}
		}
		else
		{
			ReturnC->FunzioneChiamante= "LSPhotosCalibration";
		}
		LSDisconnect(Connessione,(HWND)hDlg);	
	}
	else
	{
		ReturnC->FunzioneChiamante= "LSOpen";
	}
	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Calibrazione Doppia Sfogl.";
	return Reply;
}


int Ls40Class::Periferica::TestMicroForatura()
{

	HANDLE HBackGray=0;
	HANDLE HFrontGray=0;
	short Connessione;
	struct MHTESTER_Criteria criteria;
	struct MHTESTER_PatternInfo patternInfo;
    char bPassed;
	MHTESTER_HANDLE handle;
	int error;
	//char *StrNome=NULL;

 
	ReturnC->FunzioneChiamante = "TestMicroForatura";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	if( Reply == LS_OKAY )
	{
		
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);
		
	
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								SIDE_ALL_IMAGE,
								SCAN_MODE_256GR300 ,
								AUTO_FEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);
	

		if( Reply == LS_OKAY )
		{
	
		 Reply = LSReadImage(Connessione, (HWND)hDlg,
										NO_CLEAR_BLACK,
										SIDE_ALL_IMAGE,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);

			//StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
			//Reply = LSSaveDIB((HWND)hDlg,  HFrontGray,StrNome);


			if(Reply == LS_OKAY)
			{
				
				Reply = MHTESTER_getCriteria( "LS40", &criteria );
				if ( Reply == MHTESTER_OK )
				{
					

					handle = MHTESTER_new( ( const char * ) HFrontGray, &criteria, &error );
					if ( handle != NULL )
					{
						
						MHTESTER_getPatternInfo( handle, &patternInfo );
						MHTESTER_passed( handle, &bPassed );
						MHTESTER_free( handle );
						
						if (bPassed == 0x00 )
							Reply = patternInfo.errorFlags ;
						else
							Reply = LS_OKAY ;

						errorFlags = patternInfo.errorFlags  ;
						innerDimValidPerc = patternInfo.innerDimValidPerc ; 

						outerDimValidPerc = patternInfo.outerDimValidPerc ; 
						innerLevValidPerc = patternInfo.innerLevValidPerc ; 
						outerLevValidPerc = patternInfo.outerLevValidPerc ; 
						wDimStatmean = patternInfo.wDimStat.mean;
					    wDimStatStdDev = patternInfo.wDimStat.stdDev;
						bDimStatmean = patternInfo.bDimStat.mean ;
						bDimStatStdDev = patternInfo.bDimStat.stdDev; 
						gDimStatmean = patternInfo.gDimStat.mean ;
						gDimStatDev =patternInfo.gDimStat.stdDev ;
						wLevStatmean =patternInfo.wLevStat.mean;
						wLevStatStdDev =patternInfo.wLevStat.stdDev; 
						bLevStatmean =patternInfo.bLevStat.mean ;
						bLevStatStdDev =patternInfo.bLevStat.stdDev ;

					}
				}
             }
			
			LSDisconnect(Connessione,(HWND)hDlg);
		}
		else
		{
			ReturnC->FunzioneChiamante = "LSDocHandle";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}
	
	// libero le immagini
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	ReturnC->ReturnCode = Reply;
	
	return Reply;
}

//********************************************************************
// Controlla l'uniformita' dello sfondo
//
// Il test fallisce se trovo una riga che supera il valore di 0x40
// oppure se trovo 10 righe contigue che stanno tra 0x20 e 0x40
// oppure se trovo 80 righe in totale che stanno tra 0x20 e 0x40
//
#define NR_PIXEL_BACKGROUND				7
#define GR256_BMPHEADER					1064
//********************************************************************
int CheckBackground(HWND hDlg, HANDLE hImage, short *Row)
{
	int 	rc;
	long	height, width, diff;
	long	width_bytes;
	short	cc, rr;
	unsigned char 	*pData;
	short	Pixel;
	short	CounterContigue, CounterTotale;

	rc = 0;
	*Row = -1;
	CounterContigue = CounterTotale = 0;

	height = ((BITMAPINFOHEADER *)hImage)->biHeight;
	width = ((BITMAPINFOHEADER *)hImage)->biWidth;

	// Inizializzo i parametri per copiare i dati
	pData = (unsigned char *)hImage + GR256_BMPHEADER;
	width_bytes = width;
	if( (diff = width_bytes % 4) )
		width_bytes += (4 - diff);

	for( rr = 0; rr < height; rr ++)
	{
		Pixel = 0;
		for( cc = 0; cc < NR_PIXEL_BACKGROUND; cc ++)
		{
			Pixel += *(pData + cc);
		}

		Pixel /= NR_PIXEL_BACKGROUND;

		if( rc == 0 )
		{
			// Controllo se la media sta sopra il valore 0x40
			if( Pixel >= 0x40 )
			{
				if( *Row == -1 )
					*Row = rr;
				rc = 1;		// Ho trovato una riga fuori valore !!!
			}

			// Controllo se la media sta sopra il valore 0x30
			if( Pixel > 0x20 )
			{
				CounterContigue ++;
				CounterTotale ++;

				if( CounterContigue >= 10 )
				{
					if( *Row == -1 )
						*Row = rr;
					rc = 2;		// Ho trovato una riga fuori valore !!!
				}

				if( CounterTotale > 80 )
				{
					if( *Row == -1 )
						*Row = rr;
					rc = 3;		// Ho trovato una riga fuori valore !!!
				}
			}
			else
				CounterContigue = 0;	// Riazzero il counter delle righe contigue
		}

		// Passo alla riga successiva
		pData += width_bytes;
	}

	return rc;
} // CheckBackground


//********************************************************************
// Controlla l'uniformita' dello sfondo
//unsigned long	NrDocTest;
//********************************************************************
int Ls40Class::Periferica::TestBackground(int  fZebrato)
{
	int		Reply, rc;
	HANDLE	pFront, pRear;
	short	row;
	char	sMsg[128];
//	unsigned char	SenseKey, StatusByte[8];
	int		SavefOptionFitImage;
	short Connessione;
	BOOL fOptionFitImage = FALSE ;
	char *StrNome = NULL; 
	//FILE *fh ; 


	//-----------Open--------------------------------------------
	//SetLSConfiguration(hDlg);
	//Reply = LSConnect(hDlg, hInst, LS_Model, &hLS);
	ReturnC->FunzioneChiamante = "TestBackground";
	
	//effettuo la Connect Diagnostica per ricaricare i dati di compensazione su Ls40 Colore...
    Reply = CollaudoLSConnectMyTeller((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione);
	//Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	if( Reply == LS_OKAY || Reply == LS_ALREADY_OPEN )
	{
		/*Reply = LSConfigDoubleLeafingAndDocLength(hLS, hDlg,
												  DOUBLE_LEAFING_DISABLE,
												  stParDocHandle.DL_Value,
												  stParDocHandle.DL_MinDoc, stParDocHandle.DL_MaxDoc);*/
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);

		//LSPeripheralStatus(hLS, hDlg, &SenseKey, StatusByte);
		//if( ! (StatusByte[0] & MASK_FEEDER_EMPTY) )
		//{
			//Beep(700, 700);
			//MessageBox(hDlg, "Insert a document in the feeder", TITLE_POPUP, MB_OK | MB_ICONINFORMATION);
		//}

		// Leggo il documento
		Reply = LSDocHandle(Connessione, (HWND)hDlg,
							NO_STAMP,
							NO_PRINT_VALIDATE,
							NO_READ_CODELINE,
							(char)SIDE_ALL_IMAGE,
							SCAN_MODE_256GR300,
							AUTO_FEED,
							SORTER_BAY1,
							WAIT_YES,
							NO_BEEP,
							NULL,
							0, 0);
		/*if( fh = fopen("Trace_BackGround.txt", "at") )
					{
						fprintf(fh, "Reply DOC HANDLE= %d ", Reply);
						fclose( fh );
					}*/

		if( Reply == LS_OKAY)
		{
			// Leggo l'immagine senza clear
			Reply = LSReadImage(Connessione, (HWND)hDlg,
								NO_CLEAR_BLACK,
								SIDE_ALL_IMAGE,
								0, 0,
								&pFront,
								&pRear,
								NULL,
								NULL);
			/*if( fh = fopen("Trace_BackGround.txt", "at") )
					{
						fprintf(fh, "Reply READ IMAGE= %d ", Reply);
						fclose( fh );
					}*/

			if( Reply == LS_OKAY )
			{
				// Setto FitImage = FALSE per avere la window normal
				SavefOptionFitImage = fOptionFitImage;	// Salvo il valore
				fOptionFitImage = FALSE;

				//// Visualizzo i dati ritornati
				//ShowCodelineAndImage(LS_OKAY,	//Reply,
				//					 0,
				//					 NrDocTest ++,
				//					 (unsigned char *)pFront,
				//					 (unsigned char *)pRear,
				//					 NULL, NULL, NULL,
				//					 NULL, NULL, NULL, NULL);

				fOptionFitImage = SavefOptionFitImage;	// Ripristino il vecchio valore

				// salvo le immagini
				//if ((Side == SIDE_ALL_IMAGE) ||(Side == SIDE_FRONT_IMAGE))
				{

					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
					Reply = LSSaveDIB((HWND)hDlg,  pFront,StrNome);

					 // Always free trhe unmanaged string.
					 Marshal::FreeHGlobal(IntPtr(StrNome));

				  	StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();
					Reply = LSSaveDIB((HWND)hDlg,pRear,StrNome);

					 // Always free the unmanaged string.
					 Marshal::FreeHGlobal(IntPtr(StrNome));
				}
				
				if ( fZebrato == 1 ) 
				{
					//rc  = 0 ;
					rc = CheckBackground((HWND)hDlg, pRear, &row);
					/*if( fh = fopen("Trace_BackGround.txt", "at") )
					{
						fprintf(fh, "Reply REAR= %d ", rc);
						fclose( fh );
					}*/

					switch( rc )
					{
					case 1:
						sprintf(sMsg, "REAR background row = %d NOT adequate !\n\nOne or more row up 64 level !!!", row);
						rc  = S_L40_BACK_BACKGROUND; 
						//MessageBox(hDlg, sMsg, TITLE_POPUP, MB_OK | MB_ICONERROR);
						break;
					case 2:
						sprintf(sMsg, "REAR background row = %d NOT adequate !\n\nMore then 10 row contiguous over the level of 32 !!!", row);
						//MessageBox(hDlg, sMsg, TITLE_POPUP, MB_OK | MB_ICONERROR);
						rc  = S_L40_BACK_BACKGROUND_1; 
						break;
					case 3:
						sprintf(sMsg, "REAR background row = %d NOT adequate !\n\nMore then 80 row over the level of 32 !!!", row);
						rc  = S_L40_BACK_BACKGROUND_2 ; 
						//MessageBox(hDlg, sMsg, TITLE_POPUP, MB_OK | MB_ICONERROR);
						break;
					}
				}
				else
				{
					
					// Controllo lo sfondo delle 2 immagini
					rc = CheckBackground((HWND)hDlg, pFront, &row);
					/*if( fh = fopen("Trace_BackGround.txt", "at") )
					{
						fprintf(fh, "Reply FRONT= %d ", rc);
						fclose( fh );
					}*/
					if( rc == 0  )
					{
						rc = CheckBackground((HWND)hDlg, pRear, &row);
						/*if( fh = fopen("Trace_BackGround.txt", "at") )
						{
							fprintf(fh, "Reply REAR= %d ", rc);
							fclose( fh );
						}*/
						if( rc )
						{
							switch( rc )
							{
							case 1:
								sprintf(sMsg, "REAR background row = %d NOT adequate !\n\nOne or more row up 64 level !!!", row);
								rc  = S_L40_BACK_BACKGROUND; 
								//MessageBox(hDlg, sMsg, TITLE_POPUP, MB_OK | MB_ICONERROR);
								break;
							case 2:
								sprintf(sMsg, "REAR background row = %d NOT adequate !\n\nMore then 10 row contiguous over the level of 32 !!!", row);
								//MessageBox(hDlg, sMsg, TITLE_POPUP, MB_OK | MB_ICONERROR);
								rc  = S_L40_BACK_BACKGROUND_1; 
								break;
							case 3:
								sprintf(sMsg, "REAR background row = %d NOT adequate !\n\nMore then 80 row over the level of 32 !!!", row);
								rc  = S_L40_BACK_BACKGROUND_2 ; 
								//MessageBox(hDlg, sMsg, TITLE_POPUP, MB_OK | MB_ICONERROR);
								break;
							}
						}
						else
						{
							//MessageBox(hDlg, "Check background PASSED !", TITLE_POPUP, MB_OK | MB_ICONINFORMATION);
							rc = 0 ; 
						}
					}
					else
					{
						switch( rc )
						{
						case 1:
							sprintf(sMsg, "FRONT background row = %d NOT adequate !\n\nOne or more row up 64 level !!!", row);
							rc  = S_L40_FRONT_BACKGROUND; 
							//MessageBox(hDlg, sMsg, TITLE_POPUP, MB_OK | MB_ICONERROR);
							break;
						case 2:
							sprintf(sMsg, "FRONT background row = %d NOT adequate !\n\nMore then 8 row contiguous over the level of 32 !!!", row);
							rc  = S_L40_FRONT_BACKGROUND_1; 
							//MessageBox(hDlg, sMsg, TITLE_POPUP, MB_OK | MB_ICONERROR);
							break;
						case 3:
							sprintf(sMsg, "FRONT background row = %d NOT adequate !\n\nMore then 80 row over the level of 32 !!!", row);
							rc = S_L40_FRONT_BACKGROUND_2; 
							//MessageBox(hDlg, sMsg, TITLE_POPUP, MB_OK | MB_ICONERROR);
							break;
						}
					}
				}

				LSFreeImage((HWND)hDlg, &pFront);
				LSFreeImage((HWND)hDlg, &pRear);
			}
			else
			{
				//CheckReply(hDlg, Reply, "LS150_ReadImage");
				ReturnC->FunzioneChiamante = "LSReadImage";
			}
		}
		else
		{
			//CheckReply(hDlg, Reply, "LS150_DocHandle");
			ReturnC->FunzioneChiamante = "LSDocHandle";
		}

		LSDisconnect(Connessione,(HWND)hDlg);
	}
	else
	{
		//CheckReply(hDlg, Reply, "LS150_Open");
		ReturnC->FunzioneChiamante = "LSOpen";
	}

	return rc;
} // TestBackground

int Ls40Class::Periferica::TestDownload()
{
	//short ret;
	short Connessione=0;
	char *StrNome = NULL ; 

	ReturnC->FunzioneChiamante = "TestDownload";
	//-----------Open--------------------------------------------
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione);
	
	if (Reply == LS_OKAY )
	{
		StrNome = (char*) Marshal::StringToHGlobalAnsi(FileNameDownload ).ToPointer();
		Reply =  LSDownloadFirmware(Connessione , 
									   (HWND)hDlg,
									   StrNome , //char		*FileFw,
									   NULL ) ; //int		(__stdcall *userfunc1)(char *Item));
		
		ReturnC->FunzioneChiamante =  "LSDownloadFirmware";
	}
	else
	{
		ReturnC->FunzioneChiamante =  "TestDownload";
	}
	

	//-----------Close-------------------------------------------
	LSDisconnect(Connessione,(HWND)hDlg);
	ReturnC->ReturnCode = Reply;
	
	return Reply;


}




int Ls40Class::Periferica::DecodificaDataMatrix(char Side,short ScanMode)
{

	HANDLE HBackGray=0;
	HANDLE HFrontGray=0;
	char *StrNome= NULL ; 
	short floop = false;
	short Connessione;
	short indiceBarcode = 0 ;
	short indiceDataMatrix = 0 ; 

	char BarcodeRead[2700];
	int len_barcode;

	DataMatrixLetto = "NO_PRESENT" ;

	ReturnC->FunzioneChiamante = "DecodificaBarcode2D";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	if( Reply == LS_OKAY )
	{
		
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);
		
	
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								Side,
								ScanMode,
								AUTO_FEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);
	

		if( Reply == LS_OKAY )
		{
	
		 Reply = LSReadImage(Connessione, (HWND)hDlg,
										CLEAR_ALL_BLACK,
										Side,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);

			if(Reply == LS_OKAY)
			{
				Marshal::FreeHGlobal(IntPtr(StrNome));
				StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
						
				LSSaveDIB((HWND)hDlg,HFrontGray,StrNome);

				Marshal::FreeHGlobal(IntPtr(StrNome));
				StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();
					
				LSSaveDIB((HWND)hDlg,HBackGray,StrNome);

				memset(BarcodeRead,0,sizeof(BarcodeRead));
				//cerco il primo Barcode  con localizzazione automatica
				len_barcode = sizeof(BarcodeRead);
				Reply = LSReadBarcodeFromBitmap((HWND)hDlg,
												HFrontGray,
												READ_2D_BARCODE_DATAMATRIX,
												0 , //(int)stParDocHandle.Pdf417_Sw_x,
												0 , //(int)stParDocHandle.Pdf417_Sw_y,
												0 , //(int)stParDocHandle.Pdf417_Sw_w,
												0 , //(int)stParDocHandle.Pdf417_Sw_h,
												BarcodeRead,
												(UINT *)&len_barcode);
				if(Reply == LS_OKAY)
				{
							
					DataMatrixLetto= Marshal::PtrToStringAnsi((IntPtr) (char *)BarcodeRead);
					/*do
					{
						memset(BarcodeRead,0,sizeof(BarcodeRead));
						Reply = LSGetNextBarcode((HWND)hDlg,BarcodeRead,(long *)&len_barcode ); 
						if (Reply == LS_OKAY )  
						{
							indiceBarcode ++ ; 
							if (indiceBarcode == 1 ) 
								BarcodeLetto1 = Marshal::PtrToStringAnsi((IntPtr) (char *)BarcodeRead); 
							else if (indiceBarcode == 2 ) 
								BarcodeLetto2 = Marshal::PtrToStringAnsi((IntPtr) (char *)BarcodeRead); 
						}
					}while(Reply == LS_OKAY) ; */
						
				}
				else
					ReturnC->FunzioneChiamante = "LSReadBarcodeFromBitmap";
					
							
							
				//if (Reply == LS_OKAY || Reply == LS_NO_MORE_DATA)
				//{
				//	////cerco il datamatrix centrale , localizzazione automatica
				//   	len_barcode = sizeof(BarcodeRead);
				//	Reply = LSReadBarcodeFromBitmap((HWND)hDlg,
				//								HFrontGray,
				//								READ_2D_BARCODE_QRCODE ,
				//								0 , //81 , //(int)stParDocHandle.Pdf417_Sw_x,
				//								0 , //17 , //(int)stParDocHandle.Pdf417_Sw_y,
				//								0, //35 , //(int)stParDocHandle.Pdf417_Sw_w,
				//								0, //35 , //(int)stParDocHandle.Pdf417_Sw_h,
				//								BarcodeRead,
				//								(UINT *)&len_barcode);
				//		
				//			
				//	DataMatrixLetto0= Marshal::PtrToStringAnsi((IntPtr) (char *)BarcodeRead);
				//	if ( Reply == LS_OKAY )
				//	{
				//		do
				//		{
				//			memset(BarcodeRead,0,sizeof(BarcodeRead));
				//			Reply = LSGetNextBarcode((HWND)hDlg,BarcodeRead,(long *)&len_barcode ); 
				//			if (Reply == LS_OKAY )  
				//			{
				//				indiceDataMatrix ++ ; 
				//				if (indiceDataMatrix == 1 ) 
				//					DataMatrixLetto1 = Marshal::PtrToStringAnsi((IntPtr) (char *)BarcodeRead); 
				//				else if (indiceDataMatrix == 2 ) 
				//					DataMatrixLetto2 = Marshal::PtrToStringAnsi((IntPtr) (char *)BarcodeRead); 
				//			}
				//		}while(Reply == LS_OKAY) ;
				//	}
				//	else
				//	ReturnC->FunzioneChiamante = "LSReadBarcodeFromBitmap(READ_2D_BARCODE_QRCODE)";
				//}
			}
			else
			{
				ReturnC->FunzioneChiamante = "LSReadImage";
			}
			
			
			LSDisconnect(Connessione,(HWND)hDlg);
		}
		else
		{
			ReturnC->FunzioneChiamante = "LSDocHandle";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}
	// libero le immagini
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	ReturnC->ReturnCode = Reply;

	if (Reply == LS_NO_MORE_DATA )
		Reply = LS_OKAY ; 
	
	return Reply;
}


int Ls40Class::Periferica::CollaudoLSConnectMyTeller(HWND hDlg,HANDLE hInst,short TipoPeriferica,short *Connessione)
{
	int Reply;
	/*char	IdentStr[8];
	char	LsName[20];
	char	Version[20];
	float	ver;*/
	//BOOL    fMyTeller2 ; 

	//fMyTeller2 = FALSE ;



	do
	{
		Reply = LSConnectDiagnostica((HWND)0,(HANDLE)hInst,TipoPeriferica,Connessione);

		// Siccome MANCANO un sacco di connect, in caso di LS_ALREADY_OPEN
		// Mi disconnetto e mi riconnetto fino a reply == LS_OKAY
		// per risolvere problemi di applicativo collaudo !!!
		if( Reply == LS_ALREADY_OPEN )
		{
			int rc;
			rc = LSDisconnect(*Connessione, (HWND)0);	// Per la connect di adesso
			rc = LSDisconnect(*Connessione, (HWND)0);	// Per la connect che c'era !!
		}

	} while( Reply == LS_ALREADY_OPEN );


	// Connect alla lsapi
	//Reply = LSConnectDiagnostica((HWND)0,(HANDLE)hInst,TipoPeriferica,Connessione);
	if(( Reply == LS_OKAY )||( Reply == LS_TRY_TO_RESET )||( Reply == LS_ALREADY_OPEN ))
	{
		//TypeLS = TYPE_LS40;
		//-----------Identify--------------------------------
		//Reply = LSUnitIdentify(*Connessione, (HWND)hDlg, IdentStr, LsName, Version, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
		//if( Reply == LS_OKAY )
		//	TypeLS = GetTypeLS(LsName, Version);

		//// Se MyTeller NON faccio trattenuto
		//ver = (float)atof(Version);
		/*if( IdentStr[1] & MASK_ATM_CONFIGURATION && (ver >= LS40_VERSION_MYTELLER_2) )
			fMyTeller2 = TRUE;

		if (fMyTeller2 == TRUE )*/
		//	Reply = LS_OKAY ; 
		//else
		//	Reply = LSReset(*Connessione,(HWND)0,RESET_ERROR);
	

		//if( Reply != LS_OKAY )
		//	Reply = LSReset(*Connessione,(HWND)0,RESET_FREE_PATH);
	}

	return Reply;
}



int Ls40Class::Periferica::CollaudoLSConnect(HWND hDlg,HANDLE hInst,short TipoPeriferica,short *Connessione)
{
	int Reply;
	
	do
	{
		//Reply = LSConnectDiagnostica((HWND)0,(HANDLE)hInst,TipoPeriferica,Connessione);
		Reply = LSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,Connessione);

		// Siccome MANCANO un sacco di connect, in caso di LS_ALREADY_OPEN
		// Mi disconnetto e mi riconnetto fino a reply == LS_OKAY
		// per risolvere problemi di applicativo collaudo !!!
		if( Reply == LS_ALREADY_OPEN )
		{
			int rc;
			rc = LSDisconnect(*Connessione, (HWND)0);	// Per la connect di adesso
			rc = LSDisconnect(*Connessione, (HWND)0);	// Per la connect che c'era !!
		}

	} while( Reply == LS_ALREADY_OPEN );


	// Connect alla lsapi
	//Reply = LSConnectDiagnostica((HWND)0,(HANDLE)hInst,TipoPeriferica,Connessione);
	//Reply = LSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,Connessione);
	if(( Reply == LS_OKAY )||( Reply == LS_TRY_TO_RESET )||( Reply == LS_ALREADY_OPEN ))
	{
		Reply = LSReset(*Connessione,(HWND)0,RESET_ERROR);
	
		if( Reply != LS_OKAY )
			Reply = LSReset(*Connessione,(HWND)0,RESET_FREE_PATH);
	}

	return Reply;
}




int Ls40Class::Periferica::CollaudoLSConnectDiagnostica(HWND hDlg, HANDLE hInst, short TipoPeriferica, short *Connessione, bool fPhotoCalibration)
{
	int Reply;


	do
	{
		Reply = LSConnectDiagnostica((HWND)0,(HANDLE)hInst,TipoPeriferica,Connessione);

		// Siccome MANCANO un sacco di connect, in caso di LS_ALREADY_OPEN
		// Mi disconnetto e mi riconnetto fino a reply == LS_OKAY
		// per risolvere problemi di applicativo collaudo !!!
		if( Reply == LS_ALREADY_OPEN )
		{
			int rc;
			rc = LSDisconnect(*Connessione, (HWND)0);	// Per la connect di adesso
			rc = LSDisconnect(*Connessione, (HWND)0);	// Per la connect che c'era !!
		}

	} while( Reply == LS_ALREADY_OPEN );

	if( ((Reply == LS_OKAY) || (Reply == LS_TRY_TO_RESET) ) && (fPhotoCalibration == false))
	{
		Reply = LSReset(*Connessione,(HWND)0,RESET_ERROR);

		if( Reply != LS_OKAY )
			Reply = LSReset(*Connessione,(HWND)0,RESET_FREE_PATH);
	}

	return Reply;
}


int Ls40Class::Periferica::TestScannerUvCalibrationReExecute()
{

	HANDLE HBackGray=0;
	HANDLE HFrontGray=0;
	HANDLE HFrontUV=0;
	short Connessione=0;
	char *StrNome=NULL;
	short floop = false;
	//FILE	*fh;
	//char	IdentStr[8];
	//char	LsName[20];
	//char	Version[20];
	//char BytesCfg[8];
	//float	ver;
	//int rc_temp ; 
	

	ReturnC->FunzioneChiamante = "TestScannerUvCalibrationReExecute";
	
	//Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione);
	Reply = CollaudoLSConnectDiagnostica((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione,false);
	
	if (Reply == LS_OKAY )
	{
	
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,150,230);

		Reply = LSModifyPWMUltraViolet(Connessione , (HWND)hDlg  , 100 , TRUE , 0 ) ; 

		//Reply =	LSUnitIdentify(Connessione,(HWND)0, IdentStr,LsName,Version,NULL,NULL,NULL, NULL,NULL, NULL, NULL, NULL, NULL, NULL);

		do
		{
		
			Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								SIDE_ALL_IMAGE , //SIDE 
								SCAN_MODE_256GR300_AND_UV , 
								AUTO_FEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);
			if (Reply == LS_FEEDER_EMPTY)
			{
				floop = TRUE;
			}
			else
				floop = FALSE;
		}while(floop);

		if( Reply == LS_OKAY )
		{
			// Leggo l'immagine filmata
			Reply = LSReadImage(Connessione,(HWND)hDlg,
									NO_CLEAR_BLACK , //CLEAR_AND_ALIGN_IMAGE , 
									SIDE_ALL_IMAGE , //Side,
									0,NULL,
									(LPHANDLE)&HFrontGray,
									(LPHANDLE)&HBackGray,
									(LPHANDLE)&HFrontUV,//NULL,
									NULL);

			LSDisconnect(Connessione,(HWND)hDlg);

		}
		else
		{	
			ReturnC->FunzioneChiamante = "LSDocHandle";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSConnect";
	}

		Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione);
		if(Reply == LS_OKAY)
		{
			
			//Davide TRACCIAMENTO
			time_t ora;
			ora = time(NULL);
			//rc_temp = LSSaveDIB((HWND)hDlg,HFrontUV,"C:\\ARCA\\SCIS.bmp");

			//printf("%s\n", asctime(localtime(&ora)));
			Reply = LSCISCompensation(Connessione, (HWND)hDlg, LS40_SCANNER_UV, HFrontUV, NULL);//pImageUVL);
			/*if( fh = fopen("Trace_Calibrazione_Periferica.txt", "at") )
			{
				fprintf(fh, "LSCISCompensation Reply =%d Data=%s",Reply,asctime(localtime(&ora)));
				fprintf(fh, "\n");
				fclose( fh );
			}*/
		}
		else
		{
			ReturnC->FunzioneChiamante = "LSCISCompensation";
		}

		LSDisconnect(Connessione,(HWND)hDlg);

		
	

	// libero le immagini
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	if (HFrontUV)
	{
		//rc = LSFreeImage((HWND)hDlg, &HFrontUV);
		LSFreeImage((HWND)hDlg, &HFrontUV);

		/*if( rc != LS_OKAY || HFrontUV != 0 )
		{
		}*/
	}

	
	ReturnC->ReturnCode = Reply;
	

	return Reply;
}

int Ls40Class::Periferica::TestRetained()
{
	short Connessione=0;
	//BOOL fMyTeller2; 
	//char	IdentStr[8];
	//char	LsName[20];
	//char	Version[20];
	//float	ver;
	
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	if( Reply == LS_OKAY )
	{
			//TypeLS = TYPE_LS40;
		//-----------Identify--------------------------------
		/*Reply = LSUnitIdentify(Connessione, (HWND)hDlg, IdentStr, LsName, Version, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
		ver = (float)atof(Version);
		if( IdentStr[1] & MASK_ATM_CONFIGURATION && (ver >= LS40_VERSION_MYTELLER_2) )
			fMyTeller2 = TRUE;
		
		if (fMyTeller2 == TRUE )
			Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,130,230);
		else
			Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,150,230);*/

		Reply = LSDocHandle(Connessione, (HWND)hDlg,
										NO_STAMP,
										NO_PRINT_VALIDATE,
										NO_READ_CODELINE,
										SIDE_NONE_IMAGE,
										SCAN_MODE_256GR200,
										PATH_FEED,
										EJECT_DOCUMENT, //HOLD_DOCUMENT,
										WAIT_YES,
										NO_BEEP,
										NULL,
										0, 0);

		/*if( Reply == LS_OKAY )
		{
			Reply = LSDocHandle(Connessione, (HWND)hDlg,
										NO_STAMP,
										NO_PRINT_VALIDATE,
										NO_READ_CODELINE,
										SIDE_NONE_IMAGE,
										SCAN_MODE_256GR200,
										PATH_FEED,
										EJECT_DOCUMENT,
										WAIT_YES,
										NO_BEEP,
										NULL,
										0, 0);
		}*/
	}

	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "TestEject";

	LSDisconnect(Connessione, (HWND)hDlg);

	return Reply;	
}


int Ls40Class::Periferica::LeggiImmCard2(char Side,short ScanMode)
{
	HANDLE HBackGray=0;
	HANDLE HFrontGray=0;
	short Connessione=0;
	char *StrNome= NULL ; 
	short floop = false;

	ReturnC->FunzioneChiamante = "LeggiImmCard2";

	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione);
	
	if (Reply == LS_OKAY )
	{
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								Side,
								ScanMode , //SCAN_MODE_256GR300,
								AUTO_FEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_CARD,
								0);

		if( Reply == LS_OKAY )
		{
			// Leggo l'immagine filmata
			Reply = LSReadImage(Connessione,(HWND)hDlg,									
									CLEAR_ALL_BLACK,
									Side,
									0,NULL,
									(LPHANDLE)&HFrontGray,
									(LPHANDLE)&HBackGray,
									NULL,
									NULL);
			if(Reply == LS_OKAY)
			{
				Marshal::FreeHGlobal(IntPtr(StrNome));
				StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
				
				LSSaveDIB((HWND)hDlg,HFrontGray,StrNome);

				Marshal::FreeHGlobal(IntPtr(StrNome));
				StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();
			

				LSSaveDIB((HWND)hDlg,  HBackGray,StrNome);

				// libero le immagini
				if (HFrontGray)
					LSFreeImage((HWND)hDlg, &HFrontGray);
				if (HBackGray)
					LSFreeImage((HWND)hDlg, &HBackGray);
			}
			else
			{
				ReturnC->FunzioneChiamante = "LSReadImage";
			}
			LSDisconnect(Connessione,(HWND)hDlg);

		}
		else
		{
			ReturnC->FunzioneChiamante = "LSDocHandle";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSConnect";
	}
	
	ReturnC->ReturnCode = Reply;
	
	return Reply;
}

