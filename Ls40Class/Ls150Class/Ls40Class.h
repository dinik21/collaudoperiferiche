// Ls40Class.h

#include <windows.h>
#include "ErrWar.h"
#include "is.h"
#include "constanti.h"


#pragma once

using namespace System;
using namespace System::IO;
using namespace System::Runtime::InteropServices;
using namespace System::Runtime::Serialization;
using namespace System::Reflection;
using namespace System::Windows::Forms;



namespace Ls40Class {

	public ref class Storico
	{
	public:
		// TODO: aggiungere qui i metodi per la classe.
		int Documenti_Catturati;
		int Documenti_Ingresso;
		int Documenti_Trattati;
		int Documenti_Trattenuti;
		int Doppia_Sfogliatura;
		int Errori_Barcode;
		int Errori_CMC7;
		int Errori_E13B;
		int Errori_Ottici;
		int Jam_Allinea;
		int Jam_Card;
		int Jam_Cassetto1;
		int Jam_Cassetto2;
		int Jam_Cassetto3;
		int Jam_Feeder;
		int Jam_Feeder_Ret;
		int Jam_Micr;
		int Jam_Micr_Ret;
		int Jam_Percorso_DX;
		int Jam_Percorso_SX;
		int Jam_Print;
		int Jam_Print_Ret;
		int Jam_Scanner;
		int Jam_Scanner_Ret;
		int Jam_Stamp;
		int Jam_Sorter;
		int Num_Accensioni;
		int Doc_Timbrati;
		int Doc_Timbrati_retro;
		int Trattenuti_Micr;
		int Trattenuti_Scan;
		int TempoAccensione;
	};

	
	
	public ref class Periferica
	{
			
	private : 
	 
	
			//int __stdcall Visual(char * str);
//			int __clrcall Visual(char * str);
			short GetTypeLS(String ^Version);
			int CheckReply(HWND hDlg,int Rep,char *s);

			short MICR_TolleranceMore;
			short MICR_TolleranceLess;
			float MICR_Valore100 ;
			short	nrLettura;
			int MICR_SignalValue;
			unsigned char Limite16Grigi ;			// Soglia nera per Clear Black
			unsigned char Limite256Grigi ;		// Soglia nera per Clear Black
			



			

			// legge le codeline OCR
			//int LeggiCodelineOCR(short Type);

	public : 
		
		
		static const int		S_LS40_FPGA_NOT_OK				= -5000;
		static const int		S_LS40_BOARD_NOT_OK				= -5001;
		static const int		S_LS40_DATA_NOT_OK				= -5002;
		static const int		S_LS40_FIRMWARE_NOT_OK			= -5003;
		static const int		S_LS40_FEEDER_NOT_OK			= -5004;



		static const int		S_LS40_BADGE_NOT_CONFIGURED			= -5005;
		static const int		S_LS40_TIMBRO_NOT_CONFIGURED		= -5006;
		static const int		S_LS40_CONDENS_NOT_CONFIGURED		= -5007;
		static const int		S_LS40_INK_HW_ERROR					= -5008;
		static const int		S_LS40_INK__NOT_CONFIGURED			= -5009;
		static const int		S_LS40_CARD__NOT_CONFIGURED			= -5010;
		static const int		S_LS40_MICR__NOT_CONFIGURED			= -5011;
		
		static const int		S_L40_FRONT_BACKGROUND				= -5012;
		static const int		S_L40_FRONT_BACKGROUND_1			= -5013;
		static const int		S_L40_FRONT_BACKGROUND_2			= -5014;
		static const int		S_L40_BACK_BACKGROUND				= -5015;
		static const int		S_L40_BACK_BACKGROUND_1				= -5016;
		static const int		S_L40_BACK_BACKGROUND_2				= -5017;

	//	static const String^ VERSIONEDLL_1 = "Ls40Class Ver 1.00 del 10-06-09";
		short TipoPeriferica;
	
		ErrWar ^ReturnC ;
//			  Cis ^strCis;
		Storico ^S_Storico;

		String ^  VersioneDll_0;
		String ^ VersioneDll_1;

		short NrLettureOk;
		short ConfVelocitBassa;
		short ConfMicr;
		short ConfCard;
		short ConfEndorserPrinter;
		short ConfVoidingStamp;
		short ConfScannerFront;
		short ConfScannerBack;
		short ConfBadgeReader;
		short ConfCapacitor;

		// stringa che contiene l'ultima codeline letta
		String ^  CodelineLetta;
		String ^  BadgeLetto;
		String ^  DataMatrixLetto;
		

		// stringhe ritornate dall identificativo
		String ^ SIdentStr;
		String ^  SLsName;
		String ^  SDateFW;
		String ^  SVersion;
		String ^  SStrStampa;
		String ^  SSerialNumber;
		String ^  SBoardNr;
		String ^  SFpga;

		String  ^ FileNameDownload;
		String  ^ NomeFileImmagine;
		String  ^ NomeFileImmagineRetro;
		String  ^ NomeFileImmagineUV;
		String  ^ NomeFileUltrasonic;
		String  ^ NomeFileSPT;
		String  ^ strRead;
		String  ^ strDocSpeed;

		/*
		Questa variabile la uso SOLAMENTE nella leggiCodeline e immagini,
		dalle altre parte uso solo NomeFileImmagine
		*/
		

		ListBox^ list;
		
		// valori photosensori
		static cli::array<int> ^ PhotoValue = gcnew cli::array<int>(8);
		// valori fotosensori doppia SF
		static cli::array<Byte> ^ PhotoValueDP = gcnew cli::array<Byte>(16);
		// Byte di Stato periferica 
		static cli::array<Byte> ^ StatusByte = gcnew cli::array<Byte>(16);
		static cli::array<Int16> ^ Distanze = gcnew cli::array<Int16>(1024);
		
		long llMax, llMin;
		float llMedia;
		double DevStd;
		int stile;
		int prova ;


		int ValMinCinghia;
		int ValMaxCinghia;
		float ValMinCinghiaNew;
		float ValMaxCinghiaNew;
		float SignalValueRead ;
		int NrPicchi ; 

		static cli::array<Byte> ^ E13BSignal  = gcnew cli::array<Byte>(16000);
		//Byte *E13BSignal;
		//Byte *ViewSignal;
		static cli::array<Byte> ^ ViewSignal  = gcnew cli::array<Byte>(10000);

		//Micro-Hole
		unsigned short errorFlags;       ///< Pattern failure information
		double innerDimValidPerc;        ///< Percentage of valid bars relative to inner dimension criteria
		double outerDimValidPerc;        ///< Percentage of valid bars relative to outer dimension criteria
		double innerLevValidPerc;        ///< Percentage of valid bars relative to inner grey level criteria
		double outerLevValidPerc;        ///< Percentage of valid bars relative to outer grey level criteria

		double wDimStatmean ;
		double wDimStatStdDev ;
		double bDimStatmean ;
		double bDimStatStdDev ;
		double gDimStatmean ;
		double gDimStatDev ;
		double wLevStatmean ;
		double wLevStatStdDev ;
		double bLevStatmean ;
		double bLevStatStdDev ;

		// Reply delle funzioni
		int Reply;
		String ^ sMessage;

		static int hDlg;
		static int hInst;
		static HANDLE IDPort;
		// tipo di periferica
		short TypeLS;
		// MICR lettura
		float Percentile;
		short ValMax,ValMin;
		short Sensore;
		float ValoreLunghezzaImmagine;

		String ^Stampafinale;

		// costruttore della classe
		Periferica();
		

		// FUNZIONI
		unsigned char Calc_Trimmer_Position(float ValoreLetto, unsigned char PosTrimmer);
		BOOL Media50Barrette(unsigned char *pd, long llDati,short *NrPicchi, float *SommaValori, long *ValMin, short *NrPicchiMin);
		void ClearFrontAndRearBlack(BITMAPINFOHEADER *Bitmap, short *ColNereAnt);
		void ShowMICRSignal(HINSTANCE hInst, HWND hwnd, unsigned char *pd, short llDati, float Percentile, short Trimmer);

		int Identificativo();
		int SetConfigurazione(int E13B,int CMC7,int timbro,int badge12,int badge23,int badge123,int scannerfronte, int scannerretro,int inkjet, int card,int condensat,int SwAbilitato,int ScannerType,int SensoreDoubleLeafing,int ATMMode);
		int CalibraImmagine();
		int CalibraImmagineNew();
		//int LeggiCodelineOCR(short Type);
		int TestBeep();
		int TestLed(short Color);
		int CalibrazioneFoto();
		int CalSfogliatura();
		int Timbra();
		int TestStampa(int StampaStd);
		int LeggiBadge(short Traccia);
		int SerialNumber();
		int DoMICRCalibration3(int MICR_Value,long NrReadGood,	char	TypeSpeed, ListBox^ list);
		int LeggiImmCard(char Side, short ^%NrDistanze,short ScanMode);
		int CalibraScanner(short Scanner);
		int Reset(short TypeReset);
		int LeggiCodeline(short DocHandling);
		int LeggiCodelineImmagini(short DocHandling);
		int DisegnaEstrectLenBar();
		int Docutest04();
		int TestBackground(int  fZebrato);
		int DoImageCalibration(HWND hDlg, float TeoricValue);
		int DoImageCalibrationNew(HWND hDlg, float TeoricValue);
		int CalcolaSalvaDistanze(HWND hDlg, BITMAPINFOHEADER *pImage, short LineToCheck);
		int DoTimeSampleMICRCalibration(char TypeSpeed,ListBox^ list);
		int EraseHistory();
		int TestMicroForatura();
		int DoCalibrationUltrasonic(String ^ LottoDoc , int NrLoop ); 
		int GetVersione();
		int CollaudoLSConnect(HWND hDlg,HANDLE hInst,short TipoPeriferica,short *Connessione);
		int CollaudoLSConnectMyTeller(HWND hDlg,HANDLE hInst,short TipoPeriferica,short *Connessione);
		int CollaudoLSConnectDiagnostica(HWND hDlg, HANDLE hInst, short TipoPeriferica, short *Connessione, bool fPhotoCalibration);
		int ScriviCard(String ^ strWrite);
		int LeggiCard();
		int ConnettiCard( SCARDCONTEXT  *hSC, SCARDHANDLE *hCard, char * ReturnString);
		int TestCinghia();
		//int TestCinghiaEx(int Thn , int Thp ,int DocSignalValue);
		int Ls40Class::Periferica::TestCinghiaEx(int Thn , int Thp ,int DocSignalValue);
		int Ls40Class::Periferica::TestDownload();
		int Ls40Class::Periferica::SetConfigurazioneMyTeller(int E13B,int CMC7,int timbro,int badge12,int badge23,int badge123,int scannerfronte, int scannerretro,int inkjet, int card,int condensat,int SwAbilitato,int ScannerType,int SensoreDoubleLeafing,int ATMMode) ;
		int Ls40Class::Periferica::InkDetectorLoadCmds();
		int Ls40Class::Periferica::DisableATMConfiguration(short Enable);
		int Ls40Class::Periferica::IdentificativoMyT();
		int Ls40Class::Periferica::DecodificaDataMatrix(char Side,short ScanMode);
		int Ls40Class::Periferica::TestScannerUvCalibrationReExecute();
		int Ls40Class::Periferica::LeggiImmagini(char Side,short ScanMode);
		int Ls40Class::Periferica::LeggiImmaginiAllinea(char Side,short ScanMode);
		int Ls40Class::Periferica::TestRetained();
		int Ls40Class::Periferica::LeggiImmCard2(char Side,short ScanMode);
		int ChipCardLS40();


	};
}
