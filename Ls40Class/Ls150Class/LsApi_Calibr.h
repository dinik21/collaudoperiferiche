//
// ARCA Technologies s.r.l.
// Corso Vercelli, 332 - 10015 Ivrea (TORINO) Italy
//
// Phone ++39 125 235611
// Telefax ++39 125 235623
//
// www.arca.com		techsupp.it@arca.com
//
//
// All rights reserved
//
// We reserve the right to vary technical specification
//
//  MODULE:   LSAPI_CALIBR.H
//
//  PURPOSE:  LS Include Interface


#ifndef LSAPI_CALIBR_H
#define LSAPI_CALIBR_H     1


// Defines maschere Software
#define MASK_TOP_IMAGE					0x01
	

#define PRINT_FORMAT_HEAD_TEST			0x20	// Stampa una predefinita riga di test


// Parameter PinchRoller
#define	PINCHROLLER_OPEN				0
#define	PINCHROLLER_CLOSE				1


// Parameter Direction
#define LS40_MOVE_DOC_FORWARD			0
#define LS40_MOVE_DOC_BACKWARD			1


// Parameter Direction
#define LS150_MOTOR_LEAFER				0
#define LS150_MOTOR_FEEDER				1
#define LS150_MOTOR_PATH				2


// Parameter Action
#define LS150_MOTOR_STOP				0
#define LS150_MOTOR_START				1


// Scanner / Param1 CIS Calibration / Reward
#define LS_SCANNER_DOC_055				0
#define LS_SCANNER_DOC_069				1

#define LS40_SCANNER_GRAY				0
#define LS40_SCANNER_UV					2
#define LS40_SCANNER_UVGR				3
#define LS40_SCANNER_COLOR				4

#define LS40_CALIBR_INSERT_DOC_069		0
#define LS40_CALIBR_INSERT_DOC_055		1

#define LS150_RECOUP_FRONT				0x46	//'F'
#define LS150_RECOUP_REAR				0x42	//'B'


#define SIDE_FRONT_RGB_IMAGE			'f'
#define SIDE_REAR_RGB_IMAGE				'r'


#define LS515_LED_ON					0x02
#define LS515_LED_BADGE					0x01
#define LS515_LED_PROCESS				0x04


#define LS150_ELECTROMAGNET_POCKET		1
#define LS150_ELECTROMAGNET_LEAFER		2
#define LS150_ELECTROMAGNET_INKJET		3
#define LS150_ELECTROMAGNET_STAMP		4


#define LS515_ELECTROMAGNET_RELEASE		0
#define LS515_ELECTROMAGNET_FEEDER		1
#define LS515_ELECTROMAGNET_LINEAR		2
#define LS515_ELECTROMAGNET_STAMP		3
#define LS515_ELECTROMAGNET_POCKET		4


// Parameter Sensor Calibration
#define LS40_CALIBRATION_PHOTO_PATH				0
#define LS40_CALIBRATION_PHOTO_DOUBLE_LEAFING	1
#define LS40_CALIBRATION_INK_DETECTOR_SENSOR	3

#define LS100_CALIBRATION_PHOTO_PATH			0
#define LS100_CALIBRATION_PHOTO_DOUBLE_LEAFING	1

#define LS515_CALIBRATION_PHOTO_DOUBLE_LEAFING	0
#define LS515_CALIBRATION_PHOTO_PATH			1


// Parameter Sensor Calibration
#define LS150_CALIBRATION_PHOTO_PATH				0
#define LS150_CALIBRATION_PHOTO_DOUBLE_LEAFING		1
#define LS150_CALIBRATION_VERIFY_DOUBLE_LEAFING		2
#define LS100_CALIBRATION_INK_DETECTOR_SENSOR		3

#define LS150_READ_DATA_DOUBLE_LEAFING		0x01	// Lettura dati sensori Doppia Sfogliatura
#define LS150_READ_DATA_VERIFICATION_DL		0x02	// Lettura dati di verifica sensori Doppia Sfogliatura

// Nr. PWM Sensor Double Leafing
#define LS150_NR_PWM_DOUBLE_LEAFING			3


// Defines for Ink Detector sensor
#define INKD_CALIBRATION_STEP_1				1	// Preparazione alla calibrazione
#define INKD_CALIBRATION_STEP_2				2	// Verifica valori di riferimento
#define INKD_CALIBRATION_STEP_3				3	// Calibrazione sensore
#define INKD_CALIBRATION_STEP_4				4	// Verifica finale e Salvataggio dati

#define INKD_FILE_INI						"InkDetector.ini"
#define INKD_LS40_FILE_INI					"Ls40_InkDetector.ini"
#define INKD_FILES_SECTION					"Files"
#define INKD_CALIBRATION_SECTION			"Calibration"
#define INKD_DOC_SECTION					"DocStats"

// Ink Detector Status
#define INKD_REPLY_PowerFault				0x14
#define INKD_REPLY_SptWriteFault			0x15
#define INKD_REPLY_SptChecksumError			0x16
#define INKD_REPLY_DownloadError			0x19
#define INKD_REPLY_FirmwareCrcError			0x1a
#define INKD_REPLY_DarkRefError				0x20
#define INKD_REPLY_DarkFluError				0x21
#define INKD_REPLY_NoiseError				0x22
#define INKD_REPLY_CalibrationError			0x23
#define INKD_REPLY_SpiTimeout				0x30
#define INKD_REPLY_SyntaxError				0x31
#define INKD_REPLY_Ready					0x40
#define INKD_REPLY_Busy						0x41


// Defines for Ultrasonic sensor
#define ULTRASONIC_CALIBRATION_STEP_1		1	// Calibrazione dei tre foto passaggio carta
#define ULTRASONIC_CALIBRATION_STEP_2		2	// Set valore di riferimento del Doc1
#define ULTRASONIC_CALIBRATION_STEP_3		3	// Calibrazione fot0 Ultrasuoni

#define ULTRASONIC_FILE_INI					"Ultrasonic.ini"
#define ULTRASONIC_FILES_SECTION			"Files"
#define ULTRASONIC_FOTO_CALIBR_SECTION		"FotoCalibration"
#define ULTRASONIC_US_CALIBR_SECTION		"UsCalibration"
#define ULTRASONIC_DOC_SECTION				"DocStats"
#define ULTRASONIC_US_NOISE_SECTION			"UsNoise"
#define ULTRASONIC_MOTOR_SECTION			"Motor"

// Ultrasonic Status
#define ULTRASONIC_REPLY_SptWriteFault		0x15
#define ULTRASONIC_REPLY_SptChecksumError	0x16
#define ULTRASONIC_REPLY_DownloadError		0x19
#define ULTRASONIC_REPLY_FirmwareCrcError	0x1A
#define ULTRASONIC_REPLY_PwmError			0x21
#define ULTRASONIC_REPLY_FlytimeError		0x22
#define ULTRASONIC_REPLY_CalibrationError	0x23
#define ULTRASONIC_REPLY_VdrvError			0x25
#define ULTRASONIC_REPLY_MaxSensorError		0x26
#define ULTRASONIC_REPLY_SerialTimeout		0x30
#define ULTRASONIC_REPLY_SyntaxError		0x31
#define ULTRASONIC_REPLY_Ready				0x40
#define ULTRASONIC_REPLY_Busy				0x41
#define ULTRASONIC_REPLY_FotoDarkError		0x50
#define ULTRASONIC_REPLY_FotoCurrentError	0x51
#define ULTRASONIC_REPLY_FotoToleranceError	0x52
#define ULTRASONIC_REPLY_FotoNotFreeError	0x53


// Enum for single step Ultrasonic sensor
enum
{
	LS40_US_CALIBRATION_PASSAGE_1,
	LS40_US_CALIBRATION_PASSAGE_2,
	LS40_US_CALIBRATION_PASSAGE_3,
	LS40_US_CALIBRATION_PASSAGE_4,
	LS40_US_CALIBRATION_PASSAGE_5,
	LS40_US_CALIBRATION_PASSAGE_6,
	LS40_US_CALIBRATION_PASSAGE_7,
	LS40_US_CALIBRATION_PASSAGE_8,
	LS40_US_CALIBRATION_PASSAGE_9,
	LS40_US_CALIBRATION_PASSAGE_10,
	LS40_US_CALIBRATION_PASSAGE_11,
	LS40_US_CALIBRATION_PASSAGE_ALL,
};


// Tabella per lettura dati PWM e AD della periferica
enum
{
	DATA_SCANNER_GRAY,
	DATA_SCANNER_BLUE,
	DATA_SCANNER_GREEN,
	DATA_SCANNER_RED,
	DATA_SCANNER_UV_LOW_CONTRAST,
	DATA_SCANNER_UV_HIGH_CONTRAST,
	DATA_SCANNER_GRAY_AND_UV,
	DATA_SCANNER_RED_AND_UV,
	DATA_SCANNER_GREEN_AND_UV,
	DATA_SCANNER_BLUE_AND_UV,
	DATA_SCANNER_ALL_FLASH_MEMORY = 64,
	DATA_SCANNER_CALIBRATION_SCANLINE = 128,
};


#define SCANNER_TABLE_FRONT_GRAY				"SCANNER_TABLE_FRONT_GRAY"
#define SCANNER_TABLE_FRONT_GRAY_AND_UV			"SCANNER_TABLE_FRONT_GRAY_AND_UV"
#define SCANNER_TABLE_FRONT_UV_LOW_CONTRAST		"SCANNER_TABLE_FRONT_UV_LOW_CONTRAST"
#define SCANNER_TABLE_FRONT_UV_HIGH_CONTRAST	"SCANNER_TABLE_FRONT_UV_HIGH_CONTRAST"
#define SCANNER_TABLE_FRONT_RED					"SCANNER_TABLE_FRONT_RED"
#define SCANNER_TABLE_FRONT_GREEN				"SCANNER_TABLE_FRONT_GREEN"
#define SCANNER_TABLE_FRONT_BLUE				"SCANNER_TABLE_FRONT_BLUE"
#define SCANNER_TABLE_FRONT_RED_AND_UV			"SCANNER_TABLE_FRONT_RED_AND_UV"
#define SCANNER_TABLE_FRONT_GREEN_AND_UV		"SCANNER_TABLE_FRONT_GREEN_AND_UV"
#define SCANNER_TABLE_FRONT_BLUE_AND_UV			"SCANNER_TABLE_FRONT_BLUE_AND_UV"

#define SCANNER_TABLE_REAR_GRAY					"SCANNER_TABLE_REAR_GRAY"
#define SCANNER_TABLE_REAR_RED					"SCANNER_TABLE_REAR_RED"
#define SCANNER_TABLE_REAR_GREEN				"SCANNER_TABLE_REAR_GREEN"
#define SCANNER_TABLE_REAR_BLUE					"SCANNER_TABLE_REAR_BLUE"


typedef struct _BUILDERPARAM
{
	BOOL	Beep;						// Passo il Beep per LS100, LS150
	BOOL	StopLoopOnErrorInCodeline;	// Stop loop di DocHandle su erroe in codeline
	BOOL	SaveE13BSignal;				// Save E13B Signal
	BOOL	fApplDoCheckCodeline;		// L'applicativo segnala errore in codeline
	char	CheckCodeline[CODE_LINE_LENGTH];
	BOOL	fViewUVbackground;			// Non cancella il background dell'immagine UV
	BOOL	f100dpiFromUnit;			// Filma con la risoluzione a 100 dpi, esclude la conversione da 200
	BOOL	fDebugImageColorUV;			// Se 1 Ls150 UV filma l'immagine a colori completa anziche' il modalita' Bayer
	short	Ls40_TimeoutWaitDoc;		// Timeout attesa documento per LS40
} BUILDERPARAM, *LPBUILDERPARAM;

typedef struct _TESTBELTPARAM
{
	int		TestResult;					// Risultato test cinghia
	int		NrPicchi;					// Numero di picchi trovati
	int		DocSignalValue;				// Valore del Doc inserito
	float	SignalValueRead;			// Valore medio rilevato
	int		PositiveThreshold;			// Soglia picco positivo
	float	MaxPositiveValue;			// Valore massimo picco positivo
	int		NegativeThreshold;			// Soglia picco negativo
	float	MinNegativeValue;			// Valore minimo picco negativo
} TESTBELTPARAM, *LPTESTBELTPARAM;



#pragma pack(1)

typedef struct _LS150_AD_DATA  // Dati AD
{
	//Dati AD per la modalita' GRAY
	unsigned short ucPWM_Red;				//PWM RED
	unsigned short ucPWM_Green;				//PWM GREEN
	unsigned short ucPWM_Blue;				//PWM BLUE
	unsigned short ucOffset[3];				//Registro offset
	unsigned short gain[3];					//Registro guadagno AD

	//Dati AD per la modalita' GRAY + UV
	unsigned short ucPWM_Red_gu;			//PWM RED
	unsigned short ucPWM_Green_gu;			//PWM GREEN
	unsigned short ucPWM_Blue_gu;			//PWM BLUE
	unsigned short ucPWM_UV;				//PWM UV
	unsigned short ucFPGA_sott_UV;			//sotrattore digitale FPGA per dati UV

	//Dati AD per la modalita' COLOR
	unsigned short ucPWM_Red_c;				//PWM RED
	unsigned short ucPWM_Green_c;			//PWM GREEN
	unsigned short ucPWM_Blue_c;			//PWM BLUE
	unsigned short ucOffset_c[3];			//Registro offset
	unsigned short gain_c[3];				//Registro guadagno AD

	//Dati AD per la modalita' COLOR + UV
	unsigned short ucPWM_Red_cu;			//PWM RED
	unsigned short ucPWM_Green_cu;			//PWM GREEN
	unsigned short ucPWM_Blue_cu;			//PWM BLUE
	unsigned short ucPWM_UV_cu;				//PWM UV
	unsigned short gain_cu[3];				//Registro guadagno AD
	unsigned short ucFPGA_sott_UV_cu;		//sotrattore digitale FPGA per dati UV

	unsigned bAB2XUV_Disabled 	: 1;		//Bit presenza moltiplicatore x2 digitale 	//dati UV( 1=assente, 0=presente )
	unsigned not_used 			: 15;

} LS150_AD_DATA, *LPLS150_AD_DATA;

#pragma pack(8)


// ------------------------------------------------------------------------
//                 REPLY-CODE ERROR FOR CALIBRATION FUNCTION
// ------------------------------------------------------------------------
#define LS_MICR_TRIMMER_VALUE_NEGATIVE	-300
#define LS_ERROR_OFFSET_FRONT			-348
#define LS_ERROR_OFFSET_REAR			-349
#define LS_ERROR_SCANNER_PWM			-350
#define LS_ERROR_GAIN_FRONT				-351
#define LS_ERROR_GAIN_REAR				-352
#define LS_ERROR_COEFF_FRONT			-353
#define LS_ERROR_COEFF_REAR				-354
#define LS_ERROR_SCANNER_GENERICO		-363

#define LS_ERROR_SCANNER_PWM_FRONT		-368
#define LS_ERROR_SCANNER_PWM_REAR		-369
#define LS_CALIBR_ERR_PHOTO_SCANNER		-370

#define LS_ERROR_ID_OFFSET_FLU_2		-380
#define LS_ERROR_ID_OFFSET_FLU_3		-381
#define LS_ERROR_ID_LED_CURRENT_2		-382
#define LS_ERROR_ID_LED_CURRENT_3		-383
#define LS_ERROR_ID_OFFSET_REF			-384
#define LS_ERROR_ID_TARGET_REF_2		-385
#define LS_ERROR_ID_TARGET_REF_3		-386
#define LS_ERROR_ID_OUT_TOLERANCE		-387
#define LS_ERROR_ID_INK_DETECTED		-388
#define LS_ERROR_ID_VERIFY_CALIBRATION	-389
#define LS_ID_INDK_DETECTED				300

#define LS_ERROR_US_CURRENT_0			-310
#define LS_ERROR_US_CURRENT_1			-311
#define LS_ERROR_US_CURRENT_2			-312
#define LS_ERROR_US_FLY_TIME			-313
#define LS_ERROR_US_PWM					-314
#define LS_ERROR_US_SENSOR_AVG			-315
#define LS_ERROR_US_EMPTY_PEAK_MIN		-316
#define LS_ERROR_US_STATIC_DOC_AVG		-317
#define LS_ERROR_US_STATIC_DOC_ST_DEV	-318
#define LS_ERROR_US_DYN_DOC_MIN			-319
#define LS_ERROR_US_DYN_DOC_AVG			-320
#define LS_ERROR_US_DYN_DOC_ST_DEV		-321
#define LS_ERROR_US_NOISE_MAX			-322
#define LS_ERROR_US_NOISE_AVG			-323
#define LS_ERROR_US_NOISE_ST_DEV		-324
#define LS_ERROR_US_CALIBRATE_0x10		-325
#define LS_ERROR_US_CALIBRATE_0x01		-326
#define LS_ERROR_US_CALIBRATE_0x02		-327
#define LS_ERROR_US_CALIBRATE_0x03		-328
#define LS_ERROR_US_CHECKLIST			-329

#define LS_CALIBRATION_FAILED_1			-666
#define LS_CALIBRATION_FAILED_2			-667
#define LS_CALIBRATION_FAILED_3			-668
#define LS_CALIBRATION_FAILED_4			-669
#define LS_CALIBRATION_FAILED_5			-670
#define LS_CALIBRATION_FAILED_6			-671
#define LS_CALIBRATION_FAILED_7			-672


// --------------------------------------------------------------
// CTS Reserved function
// --------------------------------------------------------------
#ifdef __cplusplus
extern "C" {
#endif

extern int APIENTRY LSConnectDiagnostica(HWND, HANDLE, short, short *);

extern int APIENTRY LSConnectDownload(HWND hWnd, HANDLE hInstAppl, short Peripheral, short *hConnect);

extern int APIENTRY LSUSBConnect(short LsUnit, short *hConnect);

extern int APIENTRY LSReadCodelineWhitOCR(short hConnect, HWND hWnd, LPSTR Codeline, short *Length_Codeline, LPSTR Optic, short *Length_Optic);

extern int APIENTRY CTS_F1(short hConnect, HWND hWnd, char *BytesCfg);

extern int APIENTRY LSConvertImageTo256Gray(HWND hWnd, HANDLE hImage, LPHANDLE pImage);

extern int APIENTRY LSBuilderSetting(short, HWND, void *);

extern int APIENTRY LSSetSerialNumber(short, HWND, unsigned char *, short);

extern int APIENTRY LSReadE13BSignal(short, HWND, unsigned char *, long *);

extern int APIENTRY LSReadCMC7Signal(short hConnect, HWND hWnd, unsigned char *pBuff, long llBuff);

extern int APIENTRY LSReadImageOCR(short hConnect, HWND hWnd, void **pBuff);

extern int APIENTRY LSCISCompensation(short hConnect, HWND hWnd, short Param1, HANDLE pImageUVH, HANDLE pImageUVL);

extern int APIENTRY LSScannerCalibration2(short, HWND, short, short);

extern int APIENTRY LSScannerCalibration(short, HWND, short);

extern int APIENTRY LSScannerCalibrationOld(short, HWND, short);

extern int APIENTRY LSPhotosCalibrationEx(short, HWND, short, char);

extern int APIENTRY LSPhotosCalibration(short, HWND, short);

extern int APIENTRY LSReadBytesDoubleleafing(short, HWND, unsigned char, unsigned short, unsigned char *);

extern int APIENTRY LSSetLeaferDeltaThrust(short hConnect, HWND hWnd, short Value);

extern int APIENTRY LSReadPhotosValue(short, HWND, unsigned short *, unsigned short *);

extern int APIENTRY LSDumpPeripheralMemory(short, HWND, unsigned char *, long, long, char);

extern int APIENTRY LSCalibrationMICR(short, HWND, unsigned char *, short);

extern int APIENTRY LSSetDiagnosticMode(short, HWND, BOOL);

extern int APIENTRY LSSetTrimmerMICR(short, HWND, short, short);

extern int APIENTRY LSReadTrimmerMICR(short, HWND, char, unsigned char *, short);

extern int APIENTRY LSReadTimeSampleMICR(short, HWND, char, unsigned short *, short);

extern int APIENTRY LSSetTimeSampleMICR(short, HWND, char, short);

extern int APIENTRY LSSetInImageCalibration(short, HWND);

extern int APIENTRY LSImageCalibration(short, HWND, BOOL, short *);

extern int APIENTRY LSMoveMotor(short hConnect, char Direction, short NrStep, BOOL fWait);

extern int APIENTRY LSTestMotor(short hConnect, short MotorType, short Action, short Speed);

extern int APIENTRY LSMoveMotorFeeder(short, HWND, short);

extern int APIENTRY LSTestElectromagnet(short hConnect, HWND hWnd, short nrElectromagnet);

extern int APIENTRY LSTestElectromagnet2(short hConnect, HWND hWnd, short nrElectromagnet, short PWMvalue);

extern int APIENTRY LSReadFeederMotorAD(short hConnect, HWND hWnd, unsigned short *pDati);

extern int APIENTRY LSSetScannerLight(short hConnect, HWND hWnd, short Light);

extern int APIENTRY LSReloadImage(short	hConnect, HWND hWnd, short ClearBlack, char Side, short nrBank, long ImageLen, unsigned long NrDoc, LPHANDLE FrontImage, LPHANDLE BackImage, LPHANDLE FrontImageNetto, LPHANDLE BackImageNetto);

extern int APIENTRY LSTestCinghia(short hConnect, HWND hWnd, unsigned char *pBuff, long *llBuff);

extern int APIENTRY LSTestCinghiaEx(short hConnect, HWND hWnd, LPTESTBELTPARAM TestBeltParam);

extern int APIENTRY LSGetPWMValues(short hConnect, char Side, long fReadData, long nrBytes, unsigned char *pData);

extern int APIENTRY LSSetPWMValues(short hConnect, char Side, unsigned char *pData, long nrBytes);

extern int APIENTRY LSReadScannerData(short hConnect, char Side, short Color, long fReadData, long nrBytes, unsigned char *pData);

extern int APIENTRY LSReadFlashScannerData(short hConnect, char Side, short Color, unsigned short *pSubtractor, unsigned short *pMultiplier);

extern int APIENTRY LSWriteFlashScannerData(short hConnect, char Side, short Color, unsigned short *pSubtractor, unsigned short *pMultiplier, char *pFileTables);

extern int APIENTRY LS800_PhotosCalibration(HWND, char, unsigned char *, short);

extern int APIENTRY LS800_ReadPhotosValue(HWND, char, unsigned char *, short);

extern int APIENTRY LS515usbSetdefaulADvalue(short Reserved);

extern int APIENTRY LS150usbSetPWMDoubleLeafing(unsigned char *PWMvalues, short nrPWM);

extern int APIENTRY LSSendBytes(short, unsigned char *, short);

extern int APIENTRY LSReceiveBytes(short, unsigned char *, short, long);

extern int APIENTRY ARCATestPWMScanner(short hConnect, HWND hWnd, short Value);

extern int APIENTRY LSInkDetectionSendCmd(short, unsigned char *, short);

extern int APIENTRY LSInkDetectionReadBytes(short, unsigned char *, short, long);

extern int APIENTRY LSInkDetectorVersion(short hConnect, char *pFWrelease, char *pHWlevel, char *pFWrevision, char *pFWdate, char *pSensorID, char *pReserved);

extern int APIENTRY LSInkDetectorStatus(short hConnect, char *pCode, char *pDescription);

extern int APIENTRY LSInkDetectorCalibration(short hLS, short Step, short DocValue, char *ParIni);

extern int APIENTRY LSUltrasonicSensorVersion(short hConnect, char *pFWrelease, char *pHWlevel, char *pFWrevision, char *pFWdate, char *pSensorID, char *pReserved);

extern int APIENTRY LSUltrasonicDLStatus(short hConnect, char *pCode, char *pDescription);

extern int APIENTRY LSInkDetectorLoadCmds(short hConnect, char *pFname);

extern int APIENTRY ARCAPinchRoller(short hConnect, BOOL PinchRoller);

extern int APIENTRY LSUltrasonicCalibration(short hConnect, short Step, char *DocLotto, char *ErrorParIni);

extern int APIENTRY LS40usbUltrasonicCalibrationOnePassage(short Passage, char *DocLotto, int *StatusUltrasonic, char *ErrorParIni);

extern int APIENTRY LSUltrasonicLoadCmds(short hConnect, char *pFname);

extern int APIENTRY ARCA_LS40_ALT_F12();

extern int APIENTRY MT_DrawerStatus(int *Status);

extern int APIENTRY MT_DrawerErrorDescription(int Error, char *strError);

extern int APIENTRY LSReadAD(short hConnect, char *pData);

extern int APIENTRY LSWriteAD(short hConnect, char Mode, char *pPwmValue);

#ifdef __cplusplus
}
#endif

////////////////////////////////////////////////////////////////////

#endif
