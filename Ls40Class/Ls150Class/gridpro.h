/// \file
#ifndef __GRIDPRO_H
#define __GRIDPRO_H

// ****************************************************************************

#ifdef _WINDOWS
#define __GRIDPRO_API   __stdcall
#else
#define __GRIDPRO_API
#endif

// ****************************************************************************

// Error messages
#define GRIDPRO_NOT_FOUND         1 ///< Consistent grid order not found
#define GRIDPRO_OK                0 ///< No error
#define GRIDPRO_SYSTEM_ERROR     -1 ///< System error (should be reported)
#define GRIDPRO_INVALID_INPUT    -2 ///< Generic invalid input
#define GRIDPRO_INVALID_HANDLE   -3 ///< Invalid handle
#define GRIDPRO_INVALID_MODE     -4 ///< Invalid mode
#define GRIDPRO_INVALID_BITMAP   -5 ///< Invalid bitmap
#define GRIDPRO_INVALID_CRITERIA -6 ///< Invalid criteria

// Working mode
#define GRIDPRO_SINGLE_MODE      0  ///< Single working mode
#define GRIDPRO_COMPOSITE_MODE   1  ///< Composite working mode

#define GRIDPRO_MIN_SIDE         2  ///< Minimum length of a grid side
#define GRIDPRO_MIN_DISPLACEMENT 1  ///< Minimum point displacement (mm)

// ****************************************************************************

/// \brief     Defines the pointer to the grid instance
typedef void * GRIDHANDLE;

//#pragma pack(8)
/// \brief     Structure that defines a real interval
struct GRIDPRO_Interval
{
   double min;
   double max;
};

//#pragma pack(4)


/// \brief     Defines a data type for structure GRIDPRO_Interval
typedef struct GRIDPRO_Interval GRIDPRO_INTERVAL;

/// \brief     Defines a pointer data type for structure GRIDPRO_Interval
typedef struct GRIDPRO_Interval * PGRIDPRO_INTERVAL;

/// \brief     Structure that defines the grid geometry searching criteria
/// \details   The values of <b>deformPercX</b> and <b>deformPercY</b> must belong to interval [0,0.5)
///            and their sum must be less than 0.5
struct GRIDPRO_GeometryCriteria
{
   int columns;         ///< Grid columns (range 2..)
   int rows;            ///< Grid rows (range 2..)
   double dispX;        ///< Horizontal point displacement (mm)
   double dispY;        ///< Vertical point displacement (mm)
   double deformPercX;  ///< Maximum acceptable percentage of point displacement for deformed document along x-axis [0,0.5)
   double deformPercY;  ///< Maximum acceptable percentage of point displacement for deformed document along y-axis [0,0.5)
};

/// \brief     Defines a data type for structure GRIDPRO_GeometryCriteria
typedef struct GRIDPRO_GeometryCriteria GRIDPRO_GEOMETRYCRITERIA;

/// \brief     Defines a pointer data type for structure GRIDPRO_GeometryCriteria
typedef struct GRIDPRO_GeometryCriteria * PGRIDPRO_GEOMETRYCRITERIA;

//#pragma pack(8)

/// \brief     Structure that defines the point searching criteria
struct GRIDPRO_PointCriteria
{
   int th;                                ///< Threshold for points segmentation [0,255]
   struct GRIDPRO_Interval diameter;      ///< Point diameter interval (mm)
   struct GRIDPRO_Interval filledDensity; ///< Filled point density interval (0,1]
   struct GRIDPRO_Interval emptyDensity;  ///< Empty point density interval (0,1)
};

//#pragma pack(4)

/// \brief     Defines a data type for structure GRIDPRO_PointCriteria
typedef struct GRIDPRO_PointCriteria GRIDPRO_POINTCRITERIA;

/// \brief     Defines a pointer data type for structure GRIDPRO_PointCriteria
typedef struct GRIDPRO_PointCriteria * PGRIDPRO_POINTCRITERIA;

/// \brief     Structure that defines the point in the grid
struct GRIDPRO_Point
{
   double x;   ///< x-axis coordinate (pixels)
   double y;   ///< y-axis coordinate (pixels)
};

/// \brief     Defines a data type for structure GRIDPRO_Point
typedef struct GRIDPRO_Point GRIDPRO_POINT;

/// \brief     Defines a pointer data type for structure GRIDPRO_Point
typedef struct GRIDPRO_Point * PGRIDPRO_POINT;

// ****************************************************************************

/// \brief     Returns the version of the library in the format <b>X.Y.Z.W</b>
/// \details   <ul>
///               <li><b>X</b>,<b>Y</b>,<b>Z</b>,<b>W</b> version numbers</li>
///            </ul>
/// \return    Pointer to the null terminated string of characters containing
///            the version of the library.
const char * __GRIDPRO_API GRIDPRO_GetLibraryVersion( void );

// ****************************************************************************

/// \brief     Constructor
/// \details   Creates a grid by extracting the points from the adquired bitmap
///            of the testing document.
///            Elaborates the grid as follows:<br>
///            <ul>
///               <li>Recognizes the document points within the bitmap</li>
///               <li>
///                   Searches the first consistent alignment of the points
///                   within the grid by considering their planar coordinates
///               </li>
///               <li>
///                   Rotates the grid in order to have at position (0,0) the
///                   point representing the bottom left origin coordinates
///               </li>
/// \param     [in]  mode           the working mode:
///                                 <ul>
///                                    <li>GRIDPRO_SINGLE_MODE</li>
///                                    <li>GRIDPRO_COMPOSITE_MODE</li>
///                                 </ul>
/// \param     [in]  bmp            the bitmap of the testing document
/// \param     [in]  pGeoCriteria   the grid geometry searching criteria
/// \param     [in]  pPntCriteria   the point searching criteria
/// \param     [out] pError         the reference to memory where the result of the operation will be stored.<br>
///                                 Possible values are:
///                                 <ul>
///                                    <li>GRIDPRO_OK</li>
///                                    <li>GRIDPRO_INVALID_MODE</li>
///                                    <li>GRIDPRO_INVALID_BITMAP</li>
///                                    <li>GRIDPRO_INVALID_CRITERIA</li>
///                                    <li>GRIDPRO_NOT_FOUND</li>
///                                    <li>GRIDPRO_SYSTEM_ERROR</li>
///                                 </ul>
/// \return    Handle to the new grid instance, <b>NULL</b> if error.


GRIDHANDLE __GRIDPRO_API GRIDPRO_NewGrid(
   int mode,
   const unsigned char bmp[],
   const struct GRIDPRO_GeometryCriteria *pGeoCriteria,
   const struct GRIDPRO_PointCriteria *pPntCriteria,
   int *pError );




/// \brief     Constructor
/// \details   Simulates the acquisition of a grid. Generates points with
///            equally spaced planar coordinates using horizontal and vertical
///            displacements; then applies to them random coordinates
///            deformation. See method GRIDPRO_NewGrid next processing information.
/// \param     [in]  mode              the working mode:
///                                    <ul>
///                                       <li>GRIDPRO_SINGLE_MODE</li>
///                                       <li>GRIDPRO_COMPOSITE_MODE</li>
///                                    </ul>
/// \param     [in]  resolution     the resolution of the simulated bitmap (pixel/meter)
/// \param     [in]  pGeoCriteria   the grid geometry searching criteria
/// \param     [out] pError         the reference to memory where the result of the operation will be stored.<br>
///                                 Possible values are:
///                                 <ul>
///                                    <li>GRIDPRO_OK</li>
///                                    <li>GRIDPRO_INVALID_MODE</li>
///                                    <li>GRIDPRO_INVALID_INPUT (invalid resolution)</li>
///                                    <li>GRIDPRO_INVALID_CRITERIA</li>
///                                    <li>GRIDPRO_NOT_FOUND</li>
///                                    <li>GRIDPRO_SYSTEM_ERROR</li>
///                                 </ul>
/// \return    Handle to the new grid instance, <b>NULL</b> if error.
GRIDHANDLE __GRIDPRO_API GRIDPRO_NewGridSim(
   int mode,
   int resolution,
   const struct GRIDPRO_GeometryCriteria *pGeoCriteria,
   int *pError );

/// \brief     Destructor
/// \details   Destroyes the grid instance. NULL value is checked before object removal.
/// \param     [in]  handle   the reference to the grid instance
void __GRIDPRO_API GRIDPRO_DeleteGrid( GRIDHANDLE handle );

// ****************************************************************************

/// \brief     Returns the number of columns in the specified grid
/// \param     [in]  handle   the reference to the grid instance
/// \param     [out] pColumns the reference to memory where to store the number of columns
/// \return    <ul>
///               <li>GRIDPRO_OK</li>
///               <li>GRIDPRO_INVALID_HANDLE</li>
///            </ul>
int __GRIDPRO_API GRIDPRO_GetGridColumns( GRIDHANDLE handle, int *pColumns );

/// \brief     Returns the number of rows in the specified grid
/// \param     [in]  handle   the reference to the grid instance
/// \param     [out] pRows    the reference to memory where to store the number of rows
/// \return    <ul>
///               <li>GRIDPRO_OK</li>
///               <li>GRIDPRO_INVALID_HANDLE</li>
///            </ul>
int __GRIDPRO_API GRIDPRO_GetGridRows( GRIDHANDLE handle, int *pRows );

/// \brief     Returns the total number of points in the specified grid
/// \param     [in]  handle         the reference to the grid instance
/// \param     [out] pCardinality   the reference to memory where to store total number of points
/// \return    <ul>
///               <li>GRIDPRO_OK</li>
///               <li>GRIDPRO_INVALID_HANDLE</li>
///            </ul>
int __GRIDPRO_API GRIDPRO_GetGridCardinality( GRIDHANDLE handle, int *pCardinality );

/// \brief     Returns the value of the horizontal point displacement in the
///            specified grid
/// \param     [in]  handle   the reference to the grid instance
/// \param     [out] pDX      the reference to memory where to store the dispacement value
/// \return    <ul>
///               <li>GRIDPRO_OK</li>
///               <li>GRIDPRO_INVALID_HANDLE</li>
///            </ul>
int __GRIDPRO_API GRIDPRO_GetGridDispX( GRIDHANDLE handle, double *pDX );

/// \brief     Returns the value of the vertical point displacement in the
///            specified grid
/// \param     [in]  handle   the reference to the grid instance
/// \param     [out] pDY      the reference to memory where to store the dispacement value
/// \return    <ul>
///               <li>GRIDPRO_OK</li>
///               <li>GRIDPRO_INVALID_HANDLE</li>
///            </ul>
int __GRIDPRO_API GRIDPRO_GetGridDispY( GRIDHANDLE handle, double *pDY );

/// \brief     Returns the point at the specified position in the grid
/// \param     [in]  handle   the reference to the grid instance
/// \param     [in]  column   the point column
/// \param     [in]  row      the point row
/// \param     [out] pPoint   the reference to memory where to store the point
/// \return    <ul>
///               <li>GRIDPRO_OK</li>
///               <li>GRIDPRO_INVALID_HANDLE</li>
///               <li>GRIDPRO_INVALID_INPUT</li>
///            </ul>
int __GRIDPRO_API GRIDPRO_GetGridPointAt(
   GRIDHANDLE handle,
   int column, int row,
   struct GRIDPRO_Point *pPoint );

/// \brief     Returns the point at the specified offset in the grid
/// \param     [in]  handle   the reference to the grid instance
/// \param     [in]  offset   the point offset
/// \param     [out] pPoint   the reference to memory where to store the point
/// \return    <ul>
///               <li>GRIDPRO_OK</li>
///               <li>GRIDPRO_INVALID_HANDLE</li>
///               <li>GRIDPRO_INVALID_INPUT</li>
///            </ul>
int __GRIDPRO_API GRIDPRO_GetGridPointAtOffset(
   GRIDHANDLE handle,
   int offset,
   struct GRIDPRO_Point *pPoint );

// ****************************************************************************

#endif
