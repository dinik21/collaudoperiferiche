//
// ARCA Technologies s.r.l.
// All rights reserved
//
// We reserve the right to vary technical specification
//
//  MODULE:   libimgdiag.h
//

#ifndef CCI_DEPOSIT_H
#define CCI_DEPOSIT_H		1

// ------------------------------------------------------------------------
//             DEFINES
// ------------------------------------------------------------------------

// Folder trace libreria corrente
#define DIAG_DEBUG_DIRECTORY         "trace_libimgdiag"

typedef enum
{
   ESITO_PASS = 0,
   ESITO_WARNING,
   ESITO_FAIL,
   ESITO_NOT_EVALUATE
}
   ESITO_DIAG;

typedef enum
{
   CHECKREADER_LS_150
}
   CHK_DEVICES;

// ** Calc Deposti K

// Parametro Operazione
#define OPERATION_CALCULATE		   'C'
#define OPERATION_CHECK					'V'

#define NR_K_COEFFICIENTS				4

enum
{
	K_POS_FV,				// Posizione del valore K Fronte Visibile
	K_POS_RV,				// Posizione del valore K retro Visibile
	K_POS_FI,				// Posizione del valore K Fronte Infrarosso
	K_POS_RI,				// Posizione del valore K retro Infrarosso
};

// ** Fascioni

typedef ESITO_DIAG ESITO_FASCIONI;

typedef struct
{
   ESITO_FASCIONI esito;
   float          data;
}
   FASCIONI_DATA, *PFASCIONI_DATA;

typedef struct
{
   FASCIONI_DATA bigArea;
   FASCIONI_DATA smallArea;
   FASCIONI_DATA auxArea;
   FASCIONI_DATA noiseArea;
}
   FASCIONI_RES, *PFASCIONI_RES;

// ** Check image quality

typedef ESITO_DIAG ESITO_IMG_QUALITY;

typedef struct
{
   ESITO_IMG_QUALITY esito;
   int delta;

   int minColor;
   int maxColor;
   float avg;
   float devStd;
}
IMG_QUALITY_DATA, *PIMG_QUALITY_DATA;

typedef struct
{
   ESITO_IMG_QUALITY esito;
   
   int numOfData;
   PIMG_QUALITY_DATA data;
}
   IMG_QUALITY_RES, *PIMG_QUALITY_RES;

// ------------------------------------------------------------------------
//             REPLY-CODE
// ------------------------------------------------------------------------

#define		CCI_OKAY								            0

// ------------------------------------------------------------------------
//             ERRORS
// ------------------------------------------------------------------------

#define		CCI_SYSTEM_ERROR						         -1
#define		CCI_FILE_INI_MISSING					         -2
#define		CCI_PARAMETER_IN_FILE_INI_MISSING	      -3
#define		CCI_FILE_IMAGE_NOT_FOUND				      -4
#define		CCI_DOCT_FILE_INI_MISSING	               -5
#define		CCI_PARAMETER_IN_DOCT_INI_MISSING	      -6
#define		CCI_DOCT_INI_NOT_VALID                    -7
#define		CCI_FILE_IMAGE_NOT_COMPLIANCE			      -8
#define     CCI_DEVICE_UNKNOWN                        -9

// setting immagini

#define     CCI_DOC3_ROT_MIN_FRONT_MISSING		      -50
#define     CCI_DOC3_ROT_MIN_REAR_MISSING		         -51
#define     CCI_DOC3_LOCATE_MISSING		               -52
#define     CCI_DOC3_RES_MISSING		                  -53
#define     CCI_RULE_UNIT_AREE_MISSING                -54
#define     CCI_RULES_AREE_MISSING                    -55
#define     CCI_DOC3_TOO_INCLINED                     -56

// calcolo/verifica coefficienti K

#define		CCI_FV_OUT_MAX_DISTANCE					      -100
#define		CCI_RV_OUT_MAX_DISTANCE					      -101
#define		CCI_FI_OUT_MAX_DISTANCE					      -102
#define		CCI_RI_OUT_MAX_DISTANCE					      -103
#define		CCI_TEST_FAILED							      -104
#define		CCI_FV_SIGMA_OUT						         -105
#define		CCI_RV_SIGMA_OUT						         -106
#define		CCI_FI_SIGMA_OUT						         -107
#define		CCI_RI_SIGMA_OUT						         -108
#define		CCI_RECT1_VISIBLE_MISSING				      -109
#define		CCI_RECT1_INFRARED_MISSING				      -110
#define		CCI_DOC3_FACT_FV_MISSING				      -111
#define		CCI_DOC3_FACT_RV_MISSING				      -112
#define		CCI_DOC3_FACT_FI_MISSING				      -113
#define		CCI_DOC3_FACT_RI_MISSING				      -114
#define		CCI_DOC3_2n_MEAN_POS_FV_MISSING		      -115
#define		CCI_DOC3_2n_MEAN_NEG_FV_MISSING		      -116
#define		CCI_DOC3_2n_MEAN_POS_RV_MISSING		      -117
#define		CCI_DOC3_2n_MEAN_NEG_RV_MISSING		      -118
#define		CCI_DOC3_2n_MEAN_POS_FI_MISSING		      -119
#define		CCI_DOC3_2n_MEAN_NEG_FI_MISSING		      -120
#define		CCI_DOC3_2n_MEAN_POS_RI_MISSING		      -121
#define		CCI_DOC3_2n_MEAN_NEG_RI_MISSING		      -122
#define		CCI_DOC3_2n_SIGMA_MAX_FV_MISSING		      -123
#define		CCI_DOC3_2n_SIGMA_MAX_RV_MISSING		      -124
#define		CCI_DOC3_2n_SIGMA_MAX_FI_MISSING		      -125
#define		CCI_DOC3_2n_SIGMA_MAX_RI_MISSING		      -126
#define		CCI_DOC3_2n_MEAN_DIFF_FV_MISSING			   -127
#define		CCI_DOC3_2n_MEAN_DIFF_RV_MISSING			   -128
#define		CCI_DOC3_2n_MEAN_DIFF_FI_MISSING			   -129
#define		CCI_DOC3_2n_MEAN_DIFF_RI_MISSING			   -130
#define		CCI_DOC3_COMP_MEAN_POS_FV_MISSING		   -131
#define		CCI_DOC3_COMP_MEAN_NEG_FV_MISSING		   -132
#define		CCI_DOC3_COMP_MEAN_POS_RV_MISSING		   -133
#define		CCI_DOC3_COMP_MEAN_NEG_RV_MISSING		   -134
#define		CCI_DOC3_COMP_MEAN_POS_FI_MISSING		   -135
#define		CCI_DOC3_COMP_MEAN_NEG_FI_MISSING		   -136
#define		CCI_DOC3_COMP_MEAN_POS_RI_MISSING		   -137
#define		CCI_DOC3_COMP_MEAN_NEG_RI_MISSING		   -138
#define		CCI_DOC3_COMP_SIGMA_MAX_FV_MISSING		   -139
#define		CCI_DOC3_COMP_SIGMA_MAX_RV_MISSING		   -140
#define		CCI_DOC3_COMP_SIGMA_MAX_FI_MISSING		   -141
#define		CCI_DOC3_COMP_SIGMA_MAX_RI_MISSING		   -142
#define		CCI_DOC3_1_MEAN_POS_FV_MISSING		      -143
#define		CCI_DOC3_1_MEAN_NEG_FV_MISSING		      -144
#define		CCI_DOC3_1_MEAN_POS_RV_MISSING		      -145
#define		CCI_DOC3_1_MEAN_NEG_RV_MISSING		      -146
#define		CCI_DOC3_1_MEAN_POS_FI_MISSING		      -147
#define		CCI_DOC3_1_MEAN_NEG_FI_MISSING		      -148
#define		CCI_DOC3_1_MEAN_POS_RI_MISSING		      -149
#define		CCI_DOC3_1_MEAN_NEG_RI_MISSING		      -150
#define		CCI_DOC3_1_SIGMA_MAX_FV_MISSING		      -151
#define		CCI_DOC3_1_SIGMA_MAX_RV_MISSING		      -152
#define		CCI_DOC3_1_SIGMA_MAX_FI_MISSING		      -153
#define		CCI_DOC3_1_SIGMA_MAX_RI_MISSING		      -154
#define		CCI_MISSING_RGN_MEAN_OF_FLATNESS          -155
#define		CCI_MEAN_OF_FLATNESS_FV_MISSING           -156
#define		CCI_MEAN_OF_FLATNESS_FI_MISSING           -157
#define		CCI_MEAN_OF_FLATNESS_RV_MISSING           -158
#define		CCI_MEAN_OF_FLATNESS_RI_MISSING           -159
#define		CCI_DOC3_1_2n_Ax_MEAN_DIFF_FV_MISSING     -160
#define		CCI_DOC3_1_2n_Ax_MEAN_DIFF_RV_MISSING     -161
#define		CCI_DOC3_1_2n_Ax_MEAN_DIFF_FI_MISSING     -162
#define		CCI_DOC3_1_2n_Ax_MEAN_DIFF_RI_MISSING     -163
#define		CCI_MEAN_OF_MEAN_FV_OUT_RANGE			      -164
#define		CCI_MEAN_OF_MEAN_RV_OUT_RANGE			      -165
#define		CCI_MEAN_OF_MEAN_FI_OUT_RANGE			      -166
#define		CCI_MEAN_OF_MEAN_RI_OUT_RANGE			      -167
#define		CCI_MEAN_FLATNESS_FV_OUT_RANGE            -168
#define		CCI_MEAN_FLATNESS_RV_OUT_RANGE            -169
#define		CCI_MEAN_FLATNESS_FI_OUT_RANGE            -170
#define		CCI_MEAN_FLATNESS_RI_OUT_RANGE            -171
#define		CCI_MEAN_DIFF_1_2n_FV_OUT_RANGE           -172
#define		CCI_MEAN_DIFF_1_2n_RV_OUT_RANGE           -173
#define		CCI_MEAN_DIFF_1_2n_FI_OUT_RANGE           -174
#define		CCI_MEAN_DIFF_1_2n_RI_OUT_RANGE           -175

// ereditati dalla libdocana.dll

#define		CCI_DOCANA_SYSTEM_ERROR					      -301     // error
#define		CCI_DOCANA_RULES_NOT_FOUND				      -302	   // rules file not found
#define		CCI_DOCANA_RULES_SYNTAX_ERROR			      -303	   // syntax error on rules file
#define		CCI_DOCANA_FILES_BMP_NOT_FOUND		      -304	   // files bitmap not found
#define		CCI_DOCANA_BITMAP_FAILED_ERROR		      -305	   // bitmap failed to load
#define		CCI_DOCANA_BITMAP_LOCATE_ERROR		      -306	   // bitmap failed to locate
#define		CCI_DOCANA_RESULT_OBJECT_EMPTY		      -307	   // output variable is null
#define		CCI_DOCANA_INVALID_RESULT_OBJECT		      -308	   // some output variable not properly format
#define     CCI_DOCANA_INVALID_FILTER_PARAMS          -309     // invalid or wrong filter parameters number
#define     CCI_DOCANA_ERROR_ON_IMAGE_FILTER          -310     // error on libdenoising_fft or others

// ------------------------------------------------------------------------
//             WARNINGS
// ------------------------------------------------------------------------

#define		CCI_DOCANA_STRING_TRUNCATED	            299
#define     CCI_MISSING_CALL_INIT_PARAMETERS          300

// ------------------------------------------------------------------------
//             EXPORT FUNCTIONS
// ------------------------------------------------------------------------

#ifdef __cplusplus
extern "C" {
#endif

// ** Deposit

extern int APIENTRY SetOperator(
	char *Operator,
	char *ComputerName);

extern int APIENTRY CCIGetDocToProcess(
   char *DocuTest,
   int sizeDocutest,
   int *NrDocForK,
   int *NrDocVerify);

extern int APIENTRY CCIGetTimeout(
   int *TimeoutMeasure,
   int *TimeoutVerify);

extern int APIENTRY CCICalcoloCoefficientiK(
   char *SerialID, 
   int StartNrImage, 
   char *RevDocuTest, 
   char *Lotto, 
   char Operazione, 
   float *CoefficentiK );

// ** Check readers

extern int APIENTRY CHKGetDocToProcess(
   char *DocuTest,
   int sizeDocutest,
   int *NrDocForCalib,
   int *NrDocVerify);

extern int APIENTRY CHKControlloFascioni(
   char *fullFileName,
   PFASCIONI_RES pRes );

// **

extern int APIENTRY CHKGetDocForImgQuality(
   CHK_DEVICES deviceName,
   char *szDocuTest,
   int sizeDocuTest,
   //int *NrDocForCalib,
   int *pNrDocVerify );

extern int APIENTRY CHKCheckQuality(
   char *fullFileName,
   PIMG_QUALITY_RES pRes);

extern void APIENTRY CHKCheckQuality_release(
   PIMG_QUALITY_RES pRes);

// ** utils

extern int APIENTRY CCIGetCodeDescription(
   int errorCode,
   char szBuffErr[],
   int buffLength);

#ifdef __cplusplus
}
#endif

#endif