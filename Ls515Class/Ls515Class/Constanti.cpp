#include "StdAfx.h"
#include "Constanti.h"

Ls515Class::Costanti::Costanti()
{
	
}



String ^Ls515Class::Costanti::GetDesc(int ReturnCode)
{
			String ^Description;
			switch( ReturnCode)
			{
			case S_LS515_SERVERSYSERR:
			//case S_LS515_SYSTEM_ERROR:	
				Description = "LS515_SERVERSYSERR";
					break;
			case S_LS515_USBERR:
					Description = "LS515_USBERR";
					break;
			case S_LS515_PERIFNOTFOUND:
					Description = "LS515_PERIFNOTFOUND";
					break;
			case S_LS515_HARDERR:
					Description = "LS515_HARDERR";
					break;	
			
			case S_LS515_CODELINE_ERROR:
					Description = "LS515_CODELINE_ERROR";
					break;
				
				case S_LS515_PAPER_JAM:
					Description = "LS515_PAPER_JAM";
					break;
				case S_LS515_TARGET_BUSY:
					Description = "LS515_TARGET_BUSY";
					break;
				case S_LS515_INVALIDCMD:
					Description = "LS515_INVALIDCMD";
					break;
				case S_LS515_DATALOST:
					Description = "LS515_DATALOST";
					break;
				case S_LS515_EXECCMD:
					Description = "LS515_EXECCMD";
					break;
				case S_LS515_JPEGERROR:
					Description = "LS515_JPEGERROR";
					break;
				case S_LS515_CMDSEQUENCEERROR:
					Description = "LS515_CMDSEQUENCEERROR";
					break;
				case S_LS515_NOTUSED:
					Description = "LS515_NOTUSED";
					break;
				case S_LS515_IMAGEOVERWRITE:
					Description = "LS515_IMAGEOVERWRITE";
					break;
				case S_LS515_INVALID_HANDLE:
					Description = "LS515_INVALID_HANDLE";
					break;
				case S_LS515_NO_LIBRARY_LOAD:
					Description = "LS515_NO_LIBRARY_LOAD";
					break;
				case S_LS515_BMP_ERROR:
					Description = "LS515_BMP_ERROR";
					break;
				case S_LS515_TIFFERROR:
					Description = "LS515_TIFFERROR";
					break;
				case S_LS515_IMAGE_NO_MORE_AVAILABLE:
					Description = "LS515_IMAGE_NO_MORE_AVAILABLE";
					break;
				case S_LS515_IMAGE_NO_FILMED:
					Description = "LS515_IMAGE_NO_FILMED";
					break;
				case S_LS515_IMAGE_NOT_PRESENT:
					Description = "LS515_IMAGE_NOT_PRESENT";
					break;
				case S_LS515_FUNCTION_NOT_AVAILABLE:
					Description = "LS515_FUNCTION_NOT_AVAILABLE";
					break;
				case S_LS515_DOCUMENT_NOT_SUPPORTED:
					Description = "LS515_DOCUMENT_NOT_SUPPORTED";
					break;
				case S_LS515_BARCODE_ERROR:
					Description = "LS515_BARCODE_ERROR";
					break;
				case S_LS515_INVALID_LIBRARY:
					Description = "LS515_INVALID_LIBRARY";
					break;
				case S_LS515_INVALID_IMAGE:
					Description = "LS515_INVALID_IMAGE";
					break;
				case S_LS515_INVALID_IMAGE_FORMAT:
					Description = "LS515_INVALID_IMAGE";
					break;
				case S_LS515_INVALID_BARCODE_TYPE:
					Description = "LS515_INVALID_BARCODE_TYPE";
					break;
				case S_LS515_OPEN_NOT_DONE:
					Description = "LS515_OPEN_NOT_DONE";
					break;
				case S_LS515_INVALID_TYPE_CMD:
					Description = "LS515_INVALID_TYPE_CMD";
					break;
				case S_LS515_INVALID_CLEARBLACK:
					Description = "LS515_INVALID_CLEARBLACK";
					break;
				case S_LS515_INVALID_SIDE:
					Description = "LS515_INVALID_SIDE";
					break;
				case S_LS515_MISSING_IMAGE:
					Description = "LS515_MISSING_IMAGE";
					break;
				case S_LS515_INVALID_TYPE:
					Description = "LS515_INVALID_TYPE";
					break;
				case S_LS515_INVALID_SAVEMODE:
					Description = "LS515_INVALID_SAVEMODE";
					break;
				case S_LS515_INVALID_PAGE_NUMBER:
					Description = "LS515_INVALID_PAGE_NUMBER";
					break;
				case S_LS515_INVALID_NRIMAGE:
					Description = "LS515_INVALID_NRIMAGE";
					break;
				case S_LS515_INVALID_FRONTSTAMP:
					Description = "LS515_INVALID_FRONTSTAMP";
					break;
				case S_LS515_INVALID_WAITTIMEOUT:
					Description = "LS515_INVALID_WAITTIMEOUT";
					break;
				case S_LS515_INVALID_VALIDATE:
					Description = "LS515_INVALID_VALIDATE";
					break;
				case S_LS515_INVALID_CODELINE_TYPE:
					Description = "LS515_INVALID_CODELINE_TYPE";
					break;
				case S_LS515_MISSING_NRIMAGE:
					Description = "LS515_MISSING_NRIMAGE";
					break;
				case S_LS515_INVALID_SCANMODE:
					Description = "LS515_INVALID_SCANMODE";
					break;
				case S_LS515_INVALID_BEEP_PARAM:
					Description = "LS515_INVALID_BEEP_PARAM";
					break;
				case S_LS515_INVALID_FEEDER_PARAM:
					Description = "LS515_INVALID_FEEDER_PARAM";
					break;
				case S_LS515_INVALID_SORTER_PARAM:
					Description = "LS515_INVALID_SORTER_PARAM";
					break;
				case S_LS515_INVALID_BADGE_TRACK:
					Description = "LS515_INVALID_BADGE_TRACK";
					break;
				case S_LS515_MISSING_FILENAME:
					Description = "LS515_MISSING_FILENAME";
					break;
				case S_LS515_INVALID_QUALITY:
					Description = "LS515_INVALID_QUALITY";
					break;
				case S_LS515_INVALID_FILEFORMAT:
					Description = "LS515_INVALID_FILEFORMAT";
					break;
				case S_LS515_INVALID_COORDINATE:
					Description = "LS515_INVALID_COORDINATE";
					break;
				case S_LS515_MISSING_HANDLE_VARIABLE:
					Description = "LS515_MISSING_HANDLE_VARIABLE";
					break;
				case S_LS515_INVALID_POLO_FILTER:
					Description = "LS515_INVALID_POLO_FILTER";
					break;
				case S_LS515_INVALID_ORIGIN_MEASURES:
					Description = "LS515_INVALID_ORIGIN_MEASURES";
					break;
				case S_LS515_INVALID_SIZEH_VALUE:
					Description = "LS515_INVALID_SIZEH_VALUE";
					break;
				case S_LS515_INVALID_FORMAT:
					Description = "LS515_INVALID_FORMAT";
					break;
				case S_LS515_STRINGS_TOO_LONGS:
					Description = "LS515_STRINGS_TOO_LONGS";
					break;
				case S_LS515_READ_IMAGE_FAILED:
					Description = "LS515_READ_IMAGE_FAILED";
					break;
				
				case S_LS515_MISSING_BUFFER_HISTORY:
					Description = "LS515_MISSING_BUFFER_HISTORY";
					break;
				case S_LS515_INVALID_ANSWER:
					Description = "LS515_INVALID_ANSWER";
					break;
				case S_LS515_OPEN_FILE_ERROR_OR_NOT_FOUND:
					Description = "LS515_OPEN_FILE_ERROR_OR_NOT_FOUND";
					break;
				case S_LS515_READ_TIMEOUT_EXPIRED:
					Description = "LS515_READ_TIMEOUT_EXPIRED";
					break;
				case S_LS515_INVALID_METHOD:
					Description = "LS515_INVALID_METHOD";
					break;
				case S_LS515_CALIBRATION_FAILED:
					Description = "LS515_CALIBRATION_FAILED";
					break;
				case S_LS515_INVALID_SAVEIMAGE:
					Description = "LS515_INVALID_SAVEIMAGE";
					break;
				case S_LS515_UNIT_PARAM:
					Description = "LS515_UNIT_PARAM";
					break;
				case S_LS515_INVALID_NRWINDOWS:
					Description = "LS515_INVALID_NRWINDOWS";
					break;
				case S_LS515_INVALID_VALUE:
					Description = "LS515_INVALID_VALUE";
					break;
				case S_LS515_INVALID_DEGREE:
					Description = "LS515_INVALID_DEGREE";
					break;
				case S_LS515_R0TATE_ERROR:
					Description = "LS515_R0TATE_ERROR";
					break;
				case S_LS515_PERIPHERAL_RESERVED:
					Description = "LS515_PERIPHERAL_RESERVED";
					break;
				case S_LS515_INVALID_NCHANGE:
					Description = "LS515_INVALID_NCHANGE";
					break;
				case S_LS515_BRIGHTNESS_ERROR:
					Description = "LS515_BRIGHTNESS_ERROR";
					break;
				case S_LS515_CONTRAST_ERROR:
					Description = "LS515_CONTRAST_ERROR";
					break;
				case S_LS515_DOUBLE_LEAFING_ERROR:
					Description = "LS515_DOUBLE_LEAFING_ERROR";
					break;
				case S_LS515_INVALID_BADGE_TIMEOUT:
					Description = "LS515_INVALID_BADGE_TIMEOUT";
					break;
				case S_LS515_INVALID_RESET_TYPE:
					Description = "LS515_INVALID_RESET_TYPE";
					break;
				case S_LS515_IMAGE_NOT_200_DPI:
					Description = "LS515_IMAGE_NOT_200_DPI";
					break;

				case S_LS515_DECODE_FONT_NOT_PRESENT:
					Description = "LS515_DECODE_FONT_NOT_PRESENT";
					break;
				case S_LS515_DECODE_INVALID_COORDINATE:
					Description = "LS515_DECODE_INVALID_COORDINATE";
					break;
				case S_LS515_DECODE_INVALID_OPTION:
					Description = "LS515_DECODE_INVALID_OPTION";
					break;
				case S_LS515_DECODE_INVALID_CODELINE_TYPE:
					Description = "LS515_DECODE_INVALID_CODELINE_TYPE";
					break;
				case S_LS515_DECODE_SYSTEM_ERROR:
					Description = "LS515_DECODE_SYSTEM_ERROR";
					break;
				case S_LS515_DECODE_DATA_TRUNC:
					Description = "LS515_DECODE_DATA_TRUNC";
					break;
				case S_LS515_DECODE_INVALID_BITMAP:
					Description = "LS515_DECODE_INVALID_BITMAP";
					break;
				case S_LS515_DECODE_ILLEGAL_USE:
					Description = "LS515_DECODE_ILLEGAL_USE";
					break;
				case S_LS515_BARCODE_GENERIC_ERROR:
					Description = "LS515_BARCODE_GENERIC_ERROR";
					break;
				case S_LS515_BARCODE_NOT_DECODABLE:
					Description = "LS515_BARCODE_NOT_DECODABLE";
					break;
				case S_LS515_BARCODE_OPENFILE_ERROR:
					Description = "LS515_BARCODE_OPENFILE_ERROR";
					break;
				case S_LS515_BARCODE_READBMP_ERROR:
					Description = "LS515_BARCODE_READBMP_ERROR";
					break;
				case S_LS515_BARCODE_MEMORY_ERROR:
					Description = "LS515_BARCODE_MEMORY_ERROR";
					break;
				case S_LS515_BARCODE_START_NOTFOUND:
					Description = "LS515_BARCODE_START_NOTFOUND";
					break;
				case S_LS515_BARCODE_STOP_NOTFOUND:
					Description = "LS515_BARCODE_STOP_NOTFOUND";
					break;
				case S_LS515_PDF_NOT_DECODABLE:
					Description = "LS515_PDF_NOT_DECODABLE";
					break;
				case S_LS515_PDF_READBMP_ERROR:
					Description = "LS515_PDF_READBMP_ERROR";
					break;
				case S_LS515_PDF_BITMAP_FORMAT_ERROR:
					Description = "LS515_PDF_BITMAP_FORMAT_ERROR";
					break;
				case S_LS515_PDF_MEMORY_ERROR:
					Description = "LS515_PDF_MEMORY_ERROR";
					break;
				case S_LS515_PDF_START_NOTFOUND:
					Description = "LS515_PDF_START_NOTFOUND";
					break;
				case S_LS515_PDF_STOP_NOTFOUND:
					Description = "LS515_PDF_STOP_NOTFOUND";
					break;
				case S_LS515_PDF_LEFTIND_ERROR:
					Description = "LS515_PDF_LEFTIND_ERROR";
					break;
				case S_LS515_PDF_RIGHTIND_ERROR:
					Description = "LS515_PDF_RIGHTIND_ERROR";
					break;
				case S_LS515_PDF_OPENFILE_ERROR:
					Description = "LS515_PDF_OPENFILE_ERROR";
					break;
				case S_LS515_USER_ABORT:
					Description = "S_LS515_USER_ABORT";
					break;
				case S_LS515_JAM_AT_MICR_PHOTO:
					Description = "S_LS515_JAM_AT_MICR_PHOTO";
					break;
				case S_LS515_JAM_DOC_TOOLONG:
					Description = "S_LS515_JAM_DOC_TOO_LONG";
					break;
				case S_LS515_JAM_AT_SCANNERPHOTO:
					Description = "S_LS515_JAM_AT_SCANNER_PHOTO";
					break;



				case S_LS515_FPGA_NOT_OK:			
					Description = "S_LS515_FPGA_NOT_OK";
					break;
				case S_LS515_BOARD_NOT_OK:
					Description = "S_LS515_BOARD_NOT_OK";
					break;
				case S_LS515_DATA_NOT_OK:
					Description = "S_LS515_DATA_NOT_OK";
					break;
				case S_LS515_FIRMWARE_NOT_OK:
					Description = "S_LS515_FIRMWARE_NOT_OK";
					break;
				case S_LS515_DATA_FPGA_NOT_OK:
					Description = "S_LS515_DATA_FPGA_NOT_OK";
					break;


				// ------------------------------------------------------------------------
				//                  WARNINGS
				// ------------------------------------------------------------------------
				case S_LS515_FEEDEREMPTY:
					Description = "LS515_FEEDEREMPTY";
					break;
				case S_LS515_DATATRUNC:
					Description = "LS515_DATATRUNC";
					break;
				case S_LS515_DOCPRESENT:
					Description = "LS515_DOCPRESENT";
					break;
				case S_LS515_BADGETIMEOUT:
					Description = "LS515_BADGETIMEOUT";
					break;
				case S_LS515_PERIF_BUSY:
					Description = "LS515_PERIF_BUSY";
					break;
				case S_LS515_NO_ENDCMD:
					Description = "LS515_NO_ENDCMD";
					break;
				case S_LS515_RETRY:
					Description = "LS515_RETRY";
					break;
				case S_LS515_NO_OTHER_DOCUMENT:
					Description = "LS515_NO_OTHER_DOCUMENT";
					break;
				case S_LS515_QUEUEFULL:
					Description = "LS515_QUEUEFULL";
					break;
				case S_LS515_NOSENSE:
					Description = "LS515_NOSENSE";
					break;
				case S_LS515_TRY_TO_RESET:
					Description = "LS515_TRY_TO_RESET";
					break;
				case S_LS515_STRINGTRUNCATED:
					Description = "LS515_STRINGTRUNCATED";
					break;
				case S_LS515_COMMAND_NOT_SUPPORTED:
					Description = "LS515_COMMAND_NOT_SUPPORTED";
					break;
				case S_LS515_LOOP_INTERRUPTED:
					Description = "LS515_LOOP_INTERRUPTED";
					break;
				case S_LS515_SORTER_FULL:
					Description = "S_LS515_SORTER_FULL";

			}
			return Description;
		}