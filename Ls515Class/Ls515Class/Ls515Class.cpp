// File DLL principale.

#include <stdafx.h>


//#include <windows.h>
//#include "ls515.h"
#include "LsApi.h"
#include "LsApi_Calibr.h"
#include "CtsCalibrations.h"
#include "LS_SetConfig.h"


#include "Ls515Class.h"
#include "ErrWar.h"
#include "is.h"
//#include ".\Ls515Class.h"



// Defines taratura MICR
#define TENSIONE_VCC					3.3		// Tensione di alimentazione del circuito MICR
#define TENSIONE_VCC_515				5.0		// Tensione di alimentazione del circuito MICR
#define MOLTIPL_RIFERIMENTO_VOLT		0.4		// Moltiplicatore per riferimento in Volt
#define NR_VALORI_CONVERTITORE			256
#define VALORE_100_IN_COUNT				102


#define OFFSET_INZIO_CALCOLO			300
#define NR_VALORI_PER_MEDIA				200
#define ZERO_TEORICO					0x80
#define UNITA_VOLT						0.0190925

#define TRIMMER_PRE_READ				3
#define TRIMMER_MIN_VALUE				0x00
#define TRIMMER_MAX_VALUE				0x63

#define TRIMMER_MIN_VALUE_515_SE		0x00
#define TRIMMER_MAX_VALUE_515_SE		0xff

#define TRIMMER_MIN_VALUE_515			0x01
#define TRIMMER_MAX_VALUE_515			0x63

#define VALORE_MAX_PICCO_MIN			0x60

#define NR_PICCHI						50
#define MAX_READ_RETRY					3
#define PICCO_DISTANZA_MINIMA			100
#define NR_BYTE_BUFFER_E13B				0x8000

#define SCANNER_TIME_DEFAULT			1427
#define SCANNER_TIME_DEFAULT_VE			0x768

#define NR_VALORI_LETTI					512
//#define OFFSET_INZIO_CALCOLO			250
#define NR_VALORI_PER_MEDIA				200
#define OFFSET_PRIMO_PICCO				72

#define SCANNER_BACK					0
#define SCANNER_FRONT					1

#define LIMITE_16_GRIGI				0x06
#define LIMITE_256_GRIGI			0x66
#define NR_VALORI_OK				228
#define E13B_FINE_LLC				0x0d		// Terminatore dati Larghezza char
#define DIM_DESCR_CHAR				5
#define HEIGHT_BAR					50


#define TENSIONE_VCC					3.3		// Tensione di alimentazione del circuito MICR
#define MOLTIPL_RIFERIMENTO_VOLT		0.4		// Moltiplicatore per riferimento in Volt
#define NR_VALORI_CONVERTITORE			256
#define NR_BYTES_TRIMMER				2

#define OFFSET_INZIO_CALCOLO			300
#define NR_VALORI_PER_MEDIA				200
#define ZERO_TEORICO					0x80
// Defines taratura MICR
#define TENSIONE_VCC_LS515ETH				3.3		// Tensione di alimentazione del circuito MICR
#define MOLTIPL_RIFERIMENTO_VOLT_LS515ETH	0.4		// Moltiplicatore per riferimento in Volt
#define NR_VALORI_CONVERTITORE_LS515ETH		256

#define TEST_SPEED_200_DPI				0x00
#define TEST_SPEED_300_DPI				0x01
#define TEST_SPEED_75_DPM				0x02


#define MASK_FEEDER_EMPTY				0x01
#define MASK_LOW_SPEED					0x02
#define MASK_PRINTER					0x08
#define MASK_OCR_HW						0x10
#define MASK_STAMP						0x20
#define MASK_SCANNER_UV					0x04
#define MASK_SCANNER_COLOR				0x20
#define MASK_PRINTER_HD					0x01



#define TEST_SPEED_200_DPI				0x00
#define TEST_SPEED_300_DPI				0x01
#define TEST_SPEED_75_DPM				0x02
#define TEST_SPEED_300_DPI_UV			0x03
#define TEST_SPEED_300_COLOR			0x04



#define VALORE_MAX_PICCO_MIN			0x60
#define NR_PICCHI						50
#define MAX_READ_RETRY					3
#define PICCO_DISTANZA_MINIMA			100
#define PICCO_TIME_DISTANZA_MINIMA		30

#define SOGLIA_INIZIO_NERO			0x80 - 0x10	// Valore per inizio barretta nera
#define SPEED_GRAY						0x0001
#define SPEED_COLOR						0x0101

#define SPEED_200_DPI					0x00
#define SPEED_300_DPI					0x01


// Defines for dump memory
#define MEMORY_EEPROM					0
#define MEMORY_FLASH					1
#define MEMORY_RAM						2
#define MEMORY_EXTERN_RAM				3

#define OFF_PHOTO_1						4
#define OFF_PHOTO_2						5
#define OFF_PHOTO_3						6
#define OFF_PHOTO_4						7
#define OFF_PHOTO_5					16 //8
#define OFF_PHOTO_6					17 //9
#define OFF_PHOTO_7					18 //10





#define TYPE_LS500						500
#define TYPE_LS505						505
#define TYPE_LS510S						510
#define TYPE_LS510D						511
#define TYPE_LS510D_PTT					512
#define TYPE_LS515S						515
#define TYPE_LS515D						517
#define TYPE_LS515_1					521
#define TYPE_LS515_2					522
#define TYPE_LS515_3					523
#define TYPE_LS515_5					525
#define TYPE_LS515_6					526
#define TYPE_LS515_SE					529
#define TYPE_LS520						530


#define MASK_LS515D						0x04	// Maschera per LS515 con netto image
#define MASK_LS515C						0x08	// Maschera per LS515 a colori


#define RETRY_IMAGE_CALIBRATION			1000
#define GOOD_TIME_SAMPLE_VALUE			112.0f
#define MM_OFFSET_BORDO					12
#define SOGLIA_NERO_BIANCO				128


#define SAMPLE_VALUE_MIN_LS515_VE		107.0f
#define SAMPLE_VALUE_MAX_LS515_VE		116.0f


// define per campionamento MICR
#define SAMPLE_VALUE_MIN_LS515			106.0f
#define SAMPLE_VALUE_MAX_LS515			119.0f
#define SAMPLE_VALUE_MIN_LS515_SE		107.0f
#define SAMPLE_VALUE_MAX_LS515_SE		116.0f
#define SAMPLE_VALUE_MIN				107.0f
#define SAMPLE_VALUE_MAX				116.0f
#define SAMPLE_VALUE_MEDIO				112.0f


enum {
	SPEED_GRAY_200_DPI = 0,
	SPEED_COLOR_200_DPI = 1,
//	SPEED_COLOR_300_DPI_SE = 1,
	SPEED_COLOR_300_DPI,
	SPEED_GRAY_UV_200_DPI,
	SPEED_GRAY_UV_300_DPI,
	SPEED_RETAINED
};

enum {
	SPEED_550_mm = 0,			// Gray 200 dpi Normal Speed
	SPEED_450_mm,				// Color 200 dpi
	SPEED_350_mm,				// Color 300 dpi
	SPEED_800_mm = 6			// Gray 200 dpi + High Speed
};


char	IdentStr[12];
char	Model[64];
char	Version[64];
char	Lema[20];
char	InkJet[20];
char	PeripheralNr[16];

 Ls515Class::Periferica::Periferica(void)
{
	/*PhotoValue[0]= 0;
	PhotoValue[1]= 0;
	PhotoValue[2]= 0;
	PhotoValue[3]= 0;
	PhotoValue[4]= 0;
	PhotoValue[5]= 0;
	PhotoValue[6]= 0;
	PhotoValue[7]= 0;*/
//	PhotoValue  = gcnew  array<System::Int16>{0,0,0,0,0,0,0,0};

	ReturnC = gcnew ErrWar();
	strCis = gcnew Cis();
	S_Storico = gcnew Storico();
	GetVersione();

	TipoPeriferica = LS_515_USB;

	Connessione = 0;
	fDatiScannerColore = 0;
	
	
}

int Ls515Class::Periferica::GetVersione()
{
	char Versione[80];

	VersioneDll_1 = "C.T.S. Ls515Class.dll -- Ver. 1.00, Rev 000,  date 15/09/2010";

	Reply = LSGetVersion(Versione,80);
	if(Reply == LS_OKAY)
	{
		VersioneDll_0 = Marshal::PtrToStringAnsi((IntPtr) (char *)Versione);
	}
		

	return 0;
}

 int Ls515Class::Periferica::LeggiCodelineOCR(short Type)
{
	/*
	short ret;
	unsigned long NrDoc;
	short Length_Codeline=80;
	char Code[256];
	char StrNome[256];
	DATAOPTICALWINDOW pDimWindows;
	HANDLE HFrontGray;
	
	Reply = LS515_Open((HWND)hDlg,(HANDLE)hInst,SUSPENSIVE_MODE);
	// Chiamo la funzione per via documento
	if ((Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN))
	{
		pDimWindows.TypeRead = Type;
		pDimWindows.Config = OCRH_BALNKS_NO;
		pDimWindows.XRightBottom = (short)0;
		pDimWindows.YRightBottom = (short)4;
		pDimWindows.Size = (short)-1;
		pDimWindows.Height = (short)(14);

		Reply = LS515_SetOpticalWindows((HWND)hDlg,
										SUSPENSIVE_MODE,
										&pDimWindows,
										1);

		Reply = LS515_DocHandle((HWND)hDlg,
								SUSPENSIVE_MODE,
								NO_FRONT_STAMP,
								NO_PRINT_VALIDATE,
								READ_CODELINE_OPTIC,
								SIDE_FRONT_IMAGE,
								SCAN_MODE_256GR200,
								AUTOFEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								&NrDoc,
								0,
								0);
		if (Reply == LS_OKAY)
		{
			Reply = LS515_ReadCodeline((HWND)hDlg,'S',NULL,&ret,Code,&Length_Codeline);
			if (Reply == LS_OKAY)
			{
				if (HFrontGray)
					LSFreeImage((HWND)hDlg, &HFrontGray);
				Reply = LS515_ReadImage((HWND)hDlg,
									SUSPENSIVE_MODE,
									CLEAR_ALL_BLACK,
		//							(char)(TypeLS == TYPE_LS515_5 ? SIDE_FRONT_RED_IMAGE : SIDE_FRONT_IMAGE),
									SIDE_FRONT_IMAGE,
									NrDoc,
									(LPHANDLE)&HFrontGray,
									NULL,
									NULL,
									NULL);
				CodelineLetta= Code;
				// salvo le immagini
				
					//NomeFileImmagine = "..\\Dati\\FrontImage.bmp";
					for (int ii=0;ii<= NomeFileImmagine->Length-1;ii++)
					{
						StrNome[ii] = NomeFileImmagine->get_Chars(ii);
					}
					Reply = LSSaveDIB((HWND)hDlg,HFrontGray,StrNome);
					// libero le immagini
					if (HFrontGray)
						LSFreeImage((HWND)hDlg, &HFrontGray);
					
				LSDisconnect(Connessione,(HWND)hDlg);
			}
			else
			{	
				
			}
		}
	}
		
	*/
	return Reply;
}



int Ls515Class::Periferica::Identificativo()
{
	
	char IdentStr[64],Version[64],Date_Fw[64],LsName[64],SerialNumber[64],FeederVersion[64],
		DecodeExp[64],inkVer[64], SorterVersion[64],MotorVersion[64],datefpga[64];
	char BoardNr[8];
	char CpldNr[8];
	short Connessione;
	

	CpldNr[1]= 0;
	CpldNr[2]= 0;
	//HWND 
	HINSTANCE hInst = 0;
	ReturnC->FunzioneChiamante = "Test Identificativo";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	// Chiamo la funzione per via documento
	if ((Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN))
	{
		Reply =	LSUnitIdentify(Connessione,(HWND)0, IdentStr,LsName,Version,Date_Fw,SerialNumber,BoardNr,DecodeExp,inkVer, FeederVersion,SorterVersion,MotorVersion, datefpga, NULL);
		//Reply =	LS515_Identify((HWND)0, SUSPENSIVE_MODE,IdentStr,LsName,Version,Date_Fw,BoardNr,FeederVersion,SerialNumber,NULL);
		if (Reply == LS_OKAY)
		{
			TypeLS = GetTypeLS(LsName,Version,IdentStr);

			SIdentStr = Marshal::PtrToStringAnsi((IntPtr) (char *) IdentStr);
				
			SVersion = Marshal::PtrToStringAnsi((IntPtr) (char *)Version);
			SDateFW = Marshal::PtrToStringAnsi((IntPtr) (char *)Date_Fw);
			SLsName = Marshal::PtrToStringAnsi((IntPtr) (char *)LsName);
			SSerialNumber = Marshal::PtrToStringAnsi((IntPtr) (char *)SerialNumber);
			
			SBoardNr = BoardNr[0].ToString();
			CpldNr[0] =  BoardNr[1];


			SCpldNr = Marshal::PtrToStringAnsi(static_cast<IntPtr>(CpldNr));
			
			
			SDateFPGA = Marshal::PtrToStringAnsi((IntPtr) (char *)datefpga);
	
			

			ConfMicr = true;

			if( IdentStr[1] & 0x01 )						// 00000010
				ConfMicr = true;
			
			if( IdentStr[1] & 0x02 )						// 00000100
				ConfMicr = true;

		//	if( IdentStr[0] & 0x04 )						// 00001000
		//		ConfEndorserPrinter = true;     barcode ?????

			if( IdentStr[1] & 0x08 )						// 00100000
			{
				if( IdentStr[2] & 0x08 )						// 00100000
					ConfEndorserPrinter = true;
				else
					ConfEndorserPrinter = true;
			}

			if( IdentStr[2] & 0x01 )						// 00000001
				ConfScannerFront = true;

			if( IdentStr[2] & 0x02 )						// 00000010
				ConfScannerBack = true;


			if (Version == MODEL_LS520)
			{
				if( IdentStr[2] & 0x04 )
				{
					ConfscannerUltra = true;
				}
			}
			else
			{
				if (Version == MODEL_LS515)
				{
					if( IdentStr[2] & 0x0C )
					{
						ConfscannerUltra = true;
					}
					else if( IdentStr[2] & 0x08 )
					{
						ConfscannerColor = true;
					}
				}
			}

			if( IdentStr[2] & 0x10 )
				ConfBadgeReader = 3;			
		}
		else
		{
			ReturnC->FunzioneChiamante = "LSIdentify";
		}
		LSDisconnect(Connessione,(HWND)hDlg);
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}
	ReturnC->ReturnCode = Reply;
	
	return Reply;
}







int Ls515Class::Periferica::CalibrazioneFoto()
{
	//HINSTANCE hInst = 0;
	short Connessione;
	unsigned short pDump[24];
	int llBuffer=0;
	ReturnC->FunzioneChiamante= "CalibrazioneFoto";
	
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	//Reply = LS515_Open((HWND)hDlg,(HANDLE)hInst,SUSPENSIVE_MODE);
	// Chiamo la funzione per via documento
	if ((Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN))
	{
		Reply = LSPhotosCalibration(Connessione,(HWND)hDlg,1);
		if (Reply == LS_OKAY)
		{
			LSReadPhotosValue(Connessione,(HWND)0,pDump,NULL);
			if (Reply == LS_OKAY)
			{
				// Leggo il valore dei photo
				PhotoValue[0] = pDump[0];
				PhotoValue[1] = pDump[1];
				
				// Doppia sfogliatura..... 
				PhotoValue[2] = (unsigned char)pDump[2];
				PhotoValue[3] = (unsigned char)pDump[3];
				PhotoValue[4] = (unsigned char)pDump[4];
				PhotoValue[5] = pDump[5];

				

			}
			else
			{
				ReturnC->FunzioneChiamante= "LSReadPhotosValue";
			}
		}
		else
		{
			ReturnC->FunzioneChiamante= "LSPhotosCalibration";
		}
		LSDisconnect(Connessione,(HWND)hDlg);
	}
	else
	{
		ReturnC->FunzioneChiamante= "LSOpen";
	}
	ReturnC->ReturnCode = Reply;
	
	return Reply;
}


int Ls515Class::Periferica::CalSfogliatura()
{
	unsigned short pDump[24],Threshold[24];
	int llBuffer;
	HINSTANCE hInst = 0;
	short Connessione;
	
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	//Reply = LS515_Open((HWND)0,(HANDLE)hInst,SUSPENSIVE_MODE);
	// Chiamo la funzione per via documento
	if ((Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN))
	{
		// double leafing 
		Reply = LSPhotosCalibration(Connessione,(HWND)hDlg,0);
		if (Reply == LS_OKAY)
		{
			llBuffer = 32;

			LSReadPhotosValue(Connessione,(HWND)0,pDump,Threshold);
			//Reply = LS515_DumpPeripheralMemory((HWND)hDlg, (unsigned char *)pDump, llBuffer, 0, MEMORY_EEPROM);
			if( Reply == LS_OKAY )
			{
				// Leggo il valore dei photo
				PhotoValue[0] = pDump[0];
				PhotoValue[1] = pDump[1];
				
				// Doppia sfogliatura..... 
				PhotoValue[2] = (unsigned char)pDump[2];
				PhotoValue[3] = (unsigned char)pDump[3];
				PhotoValue[4] = (unsigned char)pDump[4];
				PhotoValue[5] = pDump[5];


				PhotoValueDP[0] = (unsigned char)pDump[2];
				PhotoValueDP[1] = (unsigned char)pDump[3];
				PhotoValueDP[2] = (unsigned char)pDump[4];

				PhotoValueDP[3] = (unsigned char)pDump[5];
			}
			else
			{
				ReturnC->FunzioneChiamante= "LSReadPhotosValue";
			}
		}
		else
		{
			ReturnC->FunzioneChiamante= "LSPhotosCalibration";
		}
		LSDisconnect(Connessione,(HWND)hDlg);	
	}
	else
	{
		ReturnC->FunzioneChiamante= "LSOpen";
	}
	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Calibrazione Doppia Sfogl.";
	return Reply;
}


int Ls515Class::Periferica::TestLed(short Color)
{

	short Connessione;

	ReturnC->FunzioneChiamante = "TestLed";
	
	//-----------Open--------------------------------------------
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,&Connessione);
	
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
		Reply = LSLED(Connessione, (HWND)hDlg,Color,500,500);
		ReturnC->FunzioneChiamante =  "LSLed";
	}
	else
	{
		ReturnC->FunzioneChiamante =  "LSConnect";
	}
	
	//-----------Close-------------------------------------------
	LSDisconnect(Connessione,(HWND)hDlg);
	ReturnC->ReturnCode = Reply;
	
	return Reply;


}




//DoMICRCalibration50Barrette di LS515W
int Ls515Class::Periferica::DoMICRCalibration3(int MICR_Value, long NrReadGood, char TypeSpeed, ListBox^ list)
{
	long	ii;
	short	Reply;
	short	fRetryLetture, fRetryCalibration;
	short	NrPicchi, NrPicchiMin, TotPicchi;
	float	RangeMore, RangeLess;
	float	Percento;
	short   ScanMode;
	short   Stamp;

	HANDLE	pFront, pFrontUV;
	char	BufCodelineHW[CODE_LINE_LENGTH];
	short	len_codeline;

	unsigned char	TrimmerValue, NewTrimmerValue;
	unsigned char	buffDati[NR_BYTE_BUFFER_E13B * 2];	// Dati da chiedere
	long   llDati;									// Dati da chiedere
	long	llMin;
	float	SommaMin, TotSommeMin;

	float 	Valore_100_in_Volt;
	double  Unita_Volt;
	
	short Connessione;

	char Version[64];



	// Forzo i valori di test
	MICR_SignalValue = MICR_Value;
	MICR_TolleranceLess = 4;
	MICR_TolleranceMore = 4;
	NrReadGood = 3;

	// Setto i valori iniziali
	fRetryLetture = fRetryCalibration = 0;
	nrLettura = 0;
	Percentile = 0;
	TotPicchi = 0;
	TotSommeMin = 0;
	TrimmerValue = 0;
	// Calcolo tensione di riferimento ed unita' di Volt !!!
	Valore_100_in_Volt = (float)(TENSIONE_VCC * MOLTIPL_RIFERIMENTO_VOLT);
	Unita_Volt = TENSIONE_VCC / NR_VALORI_CONVERTITORE;

	Stamp = NO_STAMP;

	if( TypeSpeed == SPEED_COLOR_200_DPI )
		ScanMode = SCAN_MODE_COLOR_200;
	else if( TypeSpeed == SPEED_COLOR_300_DPI )
		ScanMode = SCAN_MODE_COLOR_300;
	else if( TypeSpeed == SPEED_GRAY_UV_200_DPI )
		ScanMode = SCAN_MODE_256GR200_AND_UV;
	else if( TypeSpeed == SPEED_GRAY_UV_300_DPI )
		ScanMode = SCAN_MODE_256GR300_AND_UV;
	else if( TypeSpeed == SPEED_RETAINED )
		ScanMode = SCAN_MODE_COLOR_300;		// Il retained ha la STESSA velocit� del colore 300
	else  // if( TypeSpeed == SPEED_800_mm )
		ScanMode = SCAN_MODE_256GR200;


	// -------Open ----------
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	//Reply = LS515_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);

	if( Reply == LS_OKAY )
	{
		//----------- Disabilito la doppia sfogliatura -----------
		LSDoubleLeafingSensibility(Connessione, (HWND)hDlg, 0, DOUBLE_LEAFING_LEVEL1);
		LSConfigDoubleLeafingAndDocLength(Connessione, (HWND)hDlg, DOUBLE_LEAFING_DISABLE, 50, 100, 225);
		LSDisableWaitDocument(Connessione, (HWND)hDlg, FALSE);
		
		TypeLS = 0;
		//-----------Identify--------------------------------
		Reply = LSIdentify(Connessione, (HWND)hDlg, IdentStr, Model, Version, NULL, NULL, NULL, NULL);
		if( Reply == LS_OKAY)
		{
			//Identifico la periferica se LS515S o LS515D
			TypeLS = GetTypeLS(IdentStr, Model, Version);
		}
		if( TypeLS == TYPE_LS515_SE )
		{
			// Devo abbassare la velocita' ?
			if( TypeSpeed == SPEED_550_mm )
				Reply = LSSetUnitSpeed(Connessione, (HWND)hDlg, SPEED_NORMAL);
			else
				Reply = LSSetUnitSpeed(Connessione, (HWND)hDlg, SPEED_DEFAULT);
		}

		// Setto il valore del trimmer a meta' (16)
//		TrimmerValue = 16;
		Reply = LSReadTrimmerMICR(Connessione,(HWND)hDlg, TypeSpeed, buffDati, NR_BYTES_TRIMMER);
			

		if( Reply == LS_OKAY)
		{
			TrimmerValue = buffDati[0];
			// Eseguo il primo loop di misurazione di 3 letture
			for( ii = 0; ii < (TRIMMER_PRE_READ + NrReadGood); ii++ )
			{
				nrLettura ++;
//				Sleep(1000);
				// Chiamo la funzione per via documento
				Reply = LSDocHandle(Connessione,(HWND)hDlg,
								Stamp,
								NO_PRINT_VALIDATE,
								READ_CODELINE_MICR,
								SIDE_FRONT_IMAGE,
								ScanMode,
								AUTOFEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								0,
								0);

				if(( Reply == LS_OKAY)|| (Reply == LS_DOUBLE_LEAFING_WARNING ))
				{
					// Lettura segnale
					llDati = NR_BYTE_BUFFER_E13B;		// 32 Kb.
					Reply = LSReadE13BSignal(Connessione, (HWND)hDlg, buffDati, (long *)&llDati);
					if (Reply != LS_OKAY)
					{
						CheckReply((HWND)hDlg, Reply, "LS5xx_ReadE13BSignal");
						LSDisconnect(Connessione, (HWND)hDlg);
						return LS_CALIBRATION_FAILED;
					}
					/*
					sprintf(FileOut, "dati\\SgnE13B_LS5xx_%04d.cts", stParDocHandle.NrNomeFile++);
					if( (fh = fopen(FileOut, "wb")) != NULL )
					{
						fwrite(buffDati, sizeof(unsigned char), llDati, fh);
						fclose( fh );
					}
					*/


					// Leggo la codeline Per liberare i buffer periferica
					len_codeline = CODE_LINE_LENGTH;
					Reply = LSReadCodeline(Connessione, (HWND)hDlg,
										   BufCodelineHW,
										   &len_codeline,
										   NULL, 0,
										   NULL, 0);
					if( Reply != LS_OKAY )
					{
						CheckReply((HWND)hDlg, Reply, "LSReadCodeline");
						LSDisconnect(Connessione, (HWND)hDlg);
						return LS_CALIBRATION_FAILED;
					}

					// Leggo l'immagine per cancellare i buffer nella periferica
					pFront = pFrontUV = 0;
					Reply = LSReadImage(Connessione,
										(HWND)hDlg,
										NO_CLEAR_BLACK,
										SIDE_FRONT_IMAGE,
										0,
										0,
										&pFront,
										NULL, //&pBack,
										&pFrontUV, NULL);
					if( Reply != LS_OKAY )
					{
						CheckReply((HWND)hDlg, Reply, "LS_ReadImage");
						LSDisconnect(Connessione, (HWND)hDlg);
						return LS_CALIBRATION_FAILED;
					}

					if( pFront )
						LSFreeImage( (HWND)hDlg, &pFront );

					if( pFrontUV )
						LSFreeImage( (HWND)hDlg, &pFrontUV );
						
					// Dopo il primo via leggo il valore del trimmer
					if( ! TrimmerValue )
					{
						Reply = LSCalibrationMICR(Connessione, (HWND)hDlg, buffDati, NR_VALORI_LETTI);

						if( Reply == LS_OKAY)
						{
							
							if( TypeLS == TYPE_LS520 )
							{
								// Salvo il valore del trimmer
								TrimmerValue = buffDati[0];
							}
							else
							{
								// Salvo il valore del trimmer
								TrimmerValue = buffDati[0];
							}
						
						}
					}

					// Calcolo la somma dei valori minimi ...
					EstraiDatiSpeedE13B(buffDati, llDati, &NrPicchi, &SommaMin, &llMin, &NrPicchiMin);

					// Controllo se ho trovato tutti i picchi
					if( NrPicchi == NR_PICCHI )
					{
						TotSommeMin += SommaMin;

						// elimino i picchi a zero !!!
						if( llMin == 0 )
							NrPicchi -= NrPicchiMin;
						TotPicchi += NrPicchi;

						// Calcolo il valore da visualizzare in bitmap
						// SOLO se NrPicchi diverso da zero
						if( NrPicchi )
						{
							SommaMin /= NrPicchi;
							SommaMin = ZERO_TEORICO - SommaMin;
							//SommaMin *= (float)Unita_Volt;			// Converto in Volt
							//Percento = SommaMin * 100 / Valore_100_in_Volt;
							Percento = SommaMin * 100 / VALORE_100_IN_COUNT;
						}

						// Disegna segnale e visualizza la bitmap
						ShowMICRSignal( Percento,TrimmerValue);

						if( ii == (TRIMMER_PRE_READ - 1) )
						{
							// Calcolo il valore per regolare il trimmer
							// SOLO se TotPicchi diverso da zero ...
							if( TotPicchi )
							{
								TotSommeMin /= TotPicchi;
								TotSommeMin = ZERO_TEORICO - TotSommeMin;
								//TotSommeMin *= (float)Unita_Volt;			// Converto in Volt
								//Percento = TotSommeMin * 100 / Valore_100_in_Volt;
								Percento = TotSommeMin * 100 / VALORE_100_IN_COUNT;
								
								// Calcolo il nuovo valore del trimmer ...
								NewTrimmerValue = Calc_Trimmer_Position(Percento, TrimmerValue);
							}
							else
							{
								// Il segnale � in SATURAZIONE !!!
								// Decremento il valore del trimmer di 11
								NewTrimmerValue = TrimmerValue - 11;

								ii = -1;			// Cos� ricomincia da inizio pre letture
								TotSommeMin = 0;	// Riazzero il totale delle somme
								TotPicchi = 0;		// Riazzero il totale picchi
							}
							
								
							// Se il valore ritornato � uguale zero 
							// Fallisco la calibrazione !!!!!
							if( NewTrimmerValue == 0 )
							{
								// Salvo il valore da ritornare
								Percentile = NewTrimmerValue;
								Reply = LS_MICR_TRIMMER_VALUE_NEGATIVE;
								break;
							}

							else
							{
							
								if( TypeLS == TYPE_LS515_SE )
								{
									if( (NewTrimmerValue <= TRIMMER_MIN_VALUE_515_SE) ||
										(NewTrimmerValue >= TRIMMER_MAX_VALUE_515_SE) )
									{
										Reply = LS_CALIBRATION_FAILED;
										Percentile = NewTrimmerValue;
										break;
									}
								}
								else
								{
									// Testo se sono nel range accettato dal trimmer
//									if( NewTrimmerValue <= TRIMMER_MIN_VALUE )
//									{
//										NewTrimmerValue = TRIMMER_MIN_VALUE;
//										ii = -1;			// Cos� ricomincia da inizio pre letture
//									}

//									if( NewTrimmerValue > TRIMMER_MAX_VALUE )
//									{
//										NewTrimmerValue = TRIMMER_MAX_VALUE;
//										ii = -1;			// Cos� ricomincia da inizio pre letture
//									}

									if( (NewTrimmerValue <= TRIMMER_MIN_VALUE) ||
										(NewTrimmerValue >= TRIMMER_MAX_VALUE) )
									{
										Reply = LS_CALIBRATION_FAILED;
										Percentile = NewTrimmerValue;
										break;
									}
								}

							}


							// ... e setto il nuovo valore del trimmer
							TrimmerValue = NewTrimmerValue;
							Reply = LSSetTrimmerMICR(Connessione, (HWND)hDlg, TypeSpeed, NewTrimmerValue);

							if( Reply != LS_OKAY )
							{
								CheckReply((HWND)hDlg, Reply, "LS5xx_SetTrimmerMICR");
								break;
							}

							// Riazzero il totale delle somme e dei picchi
							TotSommeMin = 0;
							TotPicchi = 0;
						}
						else if( ii == ((TRIMMER_PRE_READ + NrReadGood) - 1) )
						{
							// Calcolo il valore delle POST LETTURE
							TotSommeMin /= TotPicchi;
							TotSommeMin = ZERO_TEORICO - TotSommeMin;
							//TotSommeMin *= (float)Unita_Volt;			// Converto in Volt
							//Percento = TotSommeMin * 100 / Valore_100_in_Volt;
							Percento = TotSommeMin * 100 / VALORE_100_IN_COUNT;

							// Controllo se la lettura sta nel + o - 4% della tolleranza
							RangeMore = (float)MICR_SignalValue * ((float)MICR_TolleranceMore / 100);
							RangeLess = (float)MICR_SignalValue * ((float)MICR_TolleranceLess / 100);
							if( (Percento >= (MICR_SignalValue - RangeLess)) &&
								(Percento <= (MICR_SignalValue + RangeMore)) )
							{
								// Salvo il valore da ritornare, se ho letto tutti i documenti
								Percentile = Percento;
							}
							else
							{
								// se no controllo se ho gi� fatto 3 tentativi ... o esco !
								if( fRetryCalibration < 3 )
								{
									ii = -1;			// Cos� ricomincia da inizio pre letture
									TotSommeMin = 0;	// Riazzero il totale delle somme
									TotPicchi = 0;		// Riazzero il totale picchi
									fRetryCalibration ++;
								}
								else
								{
									
									Reply = LS_CALIBRATION_FAILED;
									break;
								}
							}
						}
					} //if( NrPicchi == NR_PICCHI )
					else
					{
						// Disegna segnale e visualizza la bitmap
						ShowMICRSignal(0, TrimmerValue);

						// Decremento ii per ripetere l'acquisizione !!!
						ii --;

						
						if(TrimmerValue < 30)
						{
							TrimmerValue += 11;
						}
						if( fRetryLetture >= MAX_READ_RETRY )
						{
							//MessageBox(hDlg, "Document loose !!!\n\nChange Document !", TITLE_ERROR, MB_OK | MB_ICONERROR);
							Reply = LS_CALIBRATION_FAILED;
							break;
						}
						fRetryLetture ++;
					}
				}
				else
				{
					CheckReply(0, Reply, "LSDocHandle");
					break;
				}
			} // fine for
		}
		else
		{
			CheckReply(0, Reply, "LSReadTrimmerMICR");
		}

		// Reimposto la velocita' di default
		LSSetUnitSpeed(Connessione, (HWND)hDlg, SPEED_DEFAULT);

		LSDisconnect(Connessione,(HWND)hDlg);
	}
	else
	{
		CheckReply(0, Reply, "LSOpen");
	}
	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Calibrazione MICR";

	return Reply;
} // DoMICRCalibration3


unsigned char Ls515Class::Periferica::Calc_Trimmer_Position(float ValoreLetto, unsigned char PosTrimmer)
{
	 #define R_SERIE_TRIMMER_520		18000	//33000	// Valore di resistenza presente di versioni vecchie
											// in serie al trimmer digitale
    #define R_X_STEP_TRIMMER_520	1000	// Valore resistivo corrispondente a ogni
											// step del trimmer digitale per 100 passi
    #define R_SERIE_TRIMMER_515		1800	// Valore di restistenza presente
											// in serie al trimmer digitale
    #define R_X_STEP_TRIMMER_515	39		// Valore resistivo corrispondente a ogni
											// step del trimmer digitale per 100 passi
    float Var_Per_Sign;
    float tmp;
    unsigned long  R_trimmer;
	short R_x_step_trimmer;
	long  R_serie_trimmer;


	// Di default setto il valore per LS515
	R_serie_trimmer = R_SERIE_TRIMMER_515;
	R_x_step_trimmer = R_X_STEP_TRIMMER_515;
	
	//Calcolo la variazione percentuale del segnale della media rispetto
	//al valore del segnale magnetico indicato sul documento di test in
	//valore assoluto

	tmp = (float)Math::Abs((int)ValoreLetto - MICR_SignalValue);
	Var_Per_Sign = tmp * 100 / ValoreLetto;

	//Calcolo il valore resistivo fornito dal trimmer nella sua posizione
	//attulale e gli sommo la resistenza presente in serie
	R_trimmer = (PosTrimmer * R_x_step_trimmer);
//	R_trimmer &= 0xFFFF;
	R_trimmer += R_serie_trimmer;

	//Determino se bisogna incrementare o decrementare l'ampiezza del segnale
	if( ValoreLetto > MICR_SignalValue )
		tmp = (100 - Var_Per_Sign);
	else
		tmp = (Var_Per_Sign + 100);


	R_trimmer = (R_trimmer / 100) * (unsigned long)tmp;
	// Se la resistenza totale vale di pi� della resistenza fissa
	// dovrei settare un valore di trimmer negativo (non posso)
	// e quindi fisso il valore di trimmer a 0 !!!
	// Cos� la calibrazione fallir� !!!
	if( R_trimmer > (unsigned long)R_serie_trimmer )
	{
		//Calcolo la variazione in percentuale da effettuare alla resistenza
		//complessiva presente sulla reazione negativa e di conseguenza
		//la nuova posizione del trimmer
	
		PosTrimmer = (unsigned char)((R_trimmer - R_serie_trimmer) / R_x_step_trimmer);
	}
	else
		PosTrimmer = 0;	// Errore !!!

	return PosTrimmer;
} // Calc_Trimmer_Position



// ******************************************************************* //
// Calcolo della posizione del trimmer in base alla variazione         //
// percentuale del segnale della media rispetto al valore del segnale  //
// magnetico indicato sul documento di test                            //
// ******************************************************************* //
unsigned char Ls515Class::Periferica::Calc_Trimmer_Position_ev(float ValoreLetto, unsigned char PosTrimmer)
{
    #define LS515_EV_R_SERIE_TRIMMER			1800	// Valore di restistenza presente
											// in serie al trimmer digitale
    #define LS515_EV_R_X_STEP_TRIMMER		39		// Valore resistivo corrispondente a ogni
											// step del trimmer digitale per LS40
    float Var_Per_Sign;
    float tmp;
    unsigned long  R_trimmer;
	short R_x_step_trimmer;
	long  R_serie_trimmer;


	// Di default setto il valore per LS40
	R_serie_trimmer = LS515_EV_R_SERIE_TRIMMER;
	R_x_step_trimmer = LS515_EV_R_X_STEP_TRIMMER;

	//Calcolo la variazione percentuale del segnale della media rispetto
	//al valore del segnale magnetico indicato sul documento di test in
	//valore assoluto
	tmp = (float)abs((int)ValoreLetto - MICR_SignalValue);
	Var_Per_Sign = tmp * 100 / ValoreLetto;

	//Calcolo il valore resistivo fornito dal trimmer nella sua posizione
	//attulale e gli sommo la resistenza presente in serie
	R_trimmer = (PosTrimmer * R_x_step_trimmer);
//	R_trimmer &= 0xFFFF;
	R_trimmer += R_serie_trimmer;

	//Determino se bisogna incrementare o decrementare l'ampiezza del segnale
	if( ValoreLetto > MICR_SignalValue )
		tmp = (100 - Var_Per_Sign);
	else
		tmp = (Var_Per_Sign + 100);


	if( R_trimmer > (unsigned long)R_serie_trimmer )
	{
		//Calcolo la variazione in percentuale da effettuare alla resistenza
		//complessiva presente sulla reazione negativa e di conseguenza
		//la nuova posizione del trimmer
	
		PosTrimmer = (unsigned char)((R_trimmer - R_serie_trimmer) / R_x_step_trimmer);
	}
	else
		PosTrimmer = 0;	// Errore !!!

	return PosTrimmer;
}//Calc_Trimmer_Position_ev


int Ls515Class::Periferica::CalibraScanner(int Side)
{
	short Connessione;
	ReturnC->FunzioneChiamante = "Calibrazione Scanner";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	//Reply = LS515_OpenDiagnostica((HWND)hDlg,(HANDLE)hInst,SUSPENSIVE_MODE);
	if ((Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN))
	{
		Reply = LSScannerCalibration(Connessione,(HWND)hDlg, Side);
		LSDisconnect(Connessione,(HWND)hDlg);
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}
	ReturnC->ReturnCode = Reply;

	return Reply;
}

int Ls515Class::Periferica::DecodificaBarcode2D(char Side,short ScanMode,short fPdf)
{

	HANDLE HBackGray=0;
	HANDLE HFrontGray=0;
	char *StrNome= NULL ; 
	short floop = false;
	short Connessione;

	char BarcodeRead[2700];
	int len_barcode;

	BarcodeLettoSx = "NULL" ;
	DataMatrixLetto = "NULL";
	BarcodeLettoDx = "NULL" ;

	ReturnC->FunzioneChiamante = "DecodificaBarcode2D";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);

	if( Reply == LS_OKAY )
	{
		
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);
		
	
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								Side,
								ScanMode,
								AUTOFEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);
	

		if( Reply == LS_OKAY )
		{
	
		 Reply = LSReadImage(Connessione, (HWND)hDlg,
										CLEAR_ALL_BLACK,
										Side,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);

			if(Reply == LS_OKAY)
			{
				if(Reply == LS_OKAY)
				{
					Marshal::FreeHGlobal(IntPtr(StrNome));
					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
					
					LSSaveDIB((HWND)hDlg,HFrontGray,StrNome);

					Marshal::FreeHGlobal(IntPtr(StrNome));
					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();
				

					LSSaveDIB((HWND)hDlg,HBackGray,StrNome);

					if (Reply == LS_OKAY )
					{
						memset(BarcodeRead,0,sizeof(BarcodeRead));
						//cerco il primo a sx
				   		len_barcode = sizeof(BarcodeRead);
						Reply = LSReadBarcodeFromBitmap((HWND)hDlg,
												HFrontGray,
												READ_2D_BARCODE_DATAMATRIX,
												142 , //(int)stParDocHandle.Pdf417_Sw_x,
												24 , //(int)stParDocHandle.Pdf417_Sw_y,
												20 , //(int)stParDocHandle.Pdf417_Sw_w,
												20 , //(int)stParDocHandle.Pdf417_Sw_h,
												BarcodeRead,
												(UINT *)&len_barcode);
						if(Reply == LS_OKAY)
						{
							
							//PDFLetto= Marshal::PtrToStringAnsi((IntPtr) (char *)BarcodeRead);
							BarcodeLettoSx= Marshal::PtrToStringAnsi((IntPtr) (char *)BarcodeRead);
							
							memset(BarcodeRead,0,sizeof(BarcodeRead));
							//cerco il datamatrix centrale
				   			len_barcode = sizeof(BarcodeRead);
							Reply = LSReadBarcodeFromBitmap((HWND)hDlg,
												HFrontGray,
												READ_2D_BARCODE_QRCODE ,
												81 , //(int)stParDocHandle.Pdf417_Sw_x,
												17 , //(int)stParDocHandle.Pdf417_Sw_y,
												35 , //(int)stParDocHandle.Pdf417_Sw_w,
												35 , //(int)stParDocHandle.Pdf417_Sw_h,
												BarcodeRead,
												(UINT *)&len_barcode);
							if (Reply == LS_OKAY )
							{
								DataMatrixLetto = Marshal::PtrToStringAnsi((IntPtr) (char *)BarcodeRead);
								memset(BarcodeRead,0,sizeof(BarcodeRead));
								//cerco quello a DX
				   				len_barcode = sizeof(BarcodeRead);
								Reply = LSReadBarcodeFromBitmap((HWND)hDlg,
												HFrontGray,
												READ_2D_BARCODE_DATAMATRIX ,
												15 , //(int)stParDocHandle.Pdf417_Sw_x,
												12 , //(int)stParDocHandle.Pdf417_Sw_y,
												44 , //(int)stParDocHandle.Pdf417_Sw_w,
												44 , //(int)stParDocHandle.Pdf417_Sw_h,
												BarcodeRead,
												(UINT *)&len_barcode);

								if (Reply == LS_OKAY )
								{
                                   BarcodeLettoDx  = Marshal::PtrToStringAnsi((IntPtr) (char *)BarcodeRead);
								}
								else
								{
									ReturnC->FunzioneChiamante = "LSReadBarcodeFromBitmap 3";
								}


							}
							else
							{
								
								ReturnC->FunzioneChiamante = "LSReadBarcodeFromBitmap 2";
							}
					
						}
						else
						{
							// errore lettura retro
							ReturnC->FunzioneChiamante = "LSReadBarcodeFromBitmap 1";
						}
					}
					else
					{
						ReturnC->FunzioneChiamante = "LSSaveDIB";
					}

				}
				else
				{
					ReturnC->FunzioneChiamante = "LSReadImage";
				}
			

			
					

			}
			
			LSDisconnect(Connessione,(HWND)hDlg);
		}
		else
		{
			ReturnC->FunzioneChiamante = "LSDocHandle";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}
	// libero le immagini
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	ReturnC->ReturnCode = Reply;
	
	return Reply;
}






int Ls515Class::Periferica::LeggiImmagini(char Side,short ScanMode)
{

	HANDLE HBackGray=0;
	HANDLE HFrontGray=0;
	HANDLE HFrontUV=0;
	char *StrNome=NULL;
	short Connessione;
	

	short floop = false;
	ReturnC->FunzioneChiamante = "Lettura Immagini";
	
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	//Reply = LS515_Open((HWND)hDlg,(HANDLE)hInst,SUSPENSIVE_MODE);
	if ((Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN))
	{
		/*if (HFrontGray)
			LSFreeImage((HWND)hDlg, &HFrontGray);
		if (HBackGray)
			LSFreeImage((HWND)hDlg, &HBackGray);*/
		// Chiamo la funzione di via documento
			//Reply = LS515_DoubleLeafingSensibility((HWND)hDlg,(unsigned char)DOUBLE_LEAFING_DISABLE);
			Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);
			//Reply  = LS515_ConfigDoubleLeafing((HWND)hDlg,DOUBLE_LEAFING_ERROR,DOUBLE_LEAFING_DISABLE,0);
		do
		{
			//Reply = LS515_DocHandle((HWND)hDlg,
			//					SUSPENSIVE_MODE,
			//					NO_FRONT_STAMP,
			//					NO_PRINT_VALIDATE,
			//					NO_READ_CODELINE,
			//					Side,
			//					SCAN_MODE_256GR200,
			//					AUTOFEED,
			//					SORTER_BAY1,
			//					WAIT_YES,
			//					NO_BEEP,
			//					//&NrDoc,
			//					0,
			//					0);

			Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								Side,
								ScanMode,
								AUTOFEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);
			if (Reply == LS_FEEDER_EMPTY)
			{
				floop = TRUE;
			}
			else
				floop = FALSE;
		}while(floop);

		if( Reply == LS_OKAY )
		{
			// Leggo l'immagine filmata
		//	Reply = LS515_ReadImage((HWND)hDlg,
		//							SUSPENSIVE_MODE,
		//							CLEAR_ALL_BLACK,
		////							(char)(TypeLS == TYPE_LS515_5 ? SIDE_FRONT_RED_IMAGE : SIDE_FRONT_IMAGE),
		//							Side,
		//							(LPHANDLE)&HFrontGray,
		//							(LPHANDLE)&HBackGray,
		//							NULL,
		//							NULL);
			Reply = LSReadImage(Connessione, (HWND)hDlg,
										NO_CLEAR_BLACK,
										Side,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										(LPHANDLE)&HFrontUV,//NULL,
										NULL);
				
			if(Reply == LS_OKAY)
			{
				// salvo le immagini
				if (Side == SIDE_FRONT_IMAGE)
				{
					//NomeFileImmagine = "..\\Dati\\FrontImage.bmp";
	
					//for (int ii=0;ii<= NomeFileImmagine->Length-1;ii++)
					//{
					//StrNome =Marshal::PtrToStringAnsi((IntPtr) (char *)NomeFileImmagine);
						//StrNome[ii] = NomeFileImmagine->
						
					//}
					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
					Reply = LSSaveDIB((HWND)hDlg,  HFrontGray,StrNome);

					if (ScanMode == SCAN_MODE_256GR200_AND_UV)
					{
						StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineUV).ToPointer();
						Reply = LSSaveDIB((HWND)hDlg,  HFrontUV,StrNome);
					}
					
					// Always free trhe unmanaged string.
					Marshal::FreeHGlobal(IntPtr(StrNome));
		
				}
				else if (Side == SIDE_BACK_IMAGE)
				{
					//NomeFileImmagine = "..\\Dati\\BackImage.bmp";	
					//for (int ii=0;ii<= NomeFileImmagine->Length-1;ii++)
					//{
//						StrNome[ii] = NomeFileImmagine->get_Chars(ii);
					//}
					

					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();
					Reply = LSSaveDIB((HWND)hDlg,HBackGray,StrNome);

					 // Always free the unmanaged string.
					  Marshal::FreeHGlobal(IntPtr(StrNome));
				}	
			}
			else
			{
				ReturnC->FunzioneChiamante = "LSReadImage";
			}
			LSDisconnect(Connessione,(HWND)hDlg);
		}
		else
		{
			ReturnC->FunzioneChiamante = "LSDocHandle";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}
	
	// libero le immagini
	if (HFrontUV)
		LSFreeImage((HWND)hDlg, &HFrontUV);
	// libero le immagini
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	ReturnC->ReturnCode = Reply;
	

	return Reply;
}


int Ls515Class::Periferica::LeggiImmCard(char Side)
{

	HANDLE HBackGray=0;
	HANDLE HFrontGray=0;
	//char StrNome[256];
	char *StrNome= NULL ; 
	short floop = false;
	short Connessione;

	ReturnC->FunzioneChiamante = "Lettura Immagini Carta";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	//Reply = LS515_Open((HWND)hDlg,(HANDLE)hInst,SUSPENSIVE_MODE);
	if ((Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN))
	{
		if (HFrontGray)
			LSFreeImage((HWND)hDlg, &HFrontGray);
		if (HBackGray)
			LSFreeImage((HWND)hDlg, &HBackGray);

		//Reply = LS515_DocHandle((HWND)hDlg,
		//						SUSPENSIVE_MODE,
		//						NO_FRONT_STAMP,
		//						NO_PRINT_VALIDATE,
		//						NO_READ_CODELINE,
		//						Side,
		//						SCAN_MODE_256GR300,
		//						PATHFEED,

		//						SORTER_BAY1,
		//						WAIT_YES,
		//						NO_BEEP,
		//						//&NrDoc,
		//						0,
		//						0);
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								Side,
								SCAN_MODE_256GR300,
								PATH_FEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_CARD,
								0);
	

		if( Reply == LS_OKAY )
		{
			// Leggo l'immagine filmata
		//	Reply = LS515_ReadImage((HWND)hDlg,
		//							SUSPENSIVE_MODE,
		//							CLEAR_ALL_BLACK,
		////							(char)(TypeLS == TYPE_LS515_5 ? SIDE_FRONT_RED_IMAGE : SIDE_FRONT_IMAGE),
		//							Side,
		//							(LPHANDLE)&HFrontGray,
		//							(LPHANDLE)&HBackGray,
		//							NULL,
		//							NULL);
			Reply = LSReadImage(Connessione, (HWND)hDlg,
										CLEAR_ALL_BLACK,
										Side,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);
				
			if(Reply == LS_OKAY)
			{
				Marshal::FreeHGlobal(IntPtr(StrNome));
				StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
				
				LSSaveDIB((HWND)hDlg,HFrontGray,StrNome);

				Marshal::FreeHGlobal(IntPtr(StrNome));
				StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();
			

				LSSaveDIB((HWND)hDlg,HBackGray,StrNome);
			


				// COSTRUISCO IL GRAFICO DELLE BARRETTE

				Reply = CalcolaSalvaDistanze((HWND)hDlg,(BITMAPINFOHEADER*)HFrontGray,24);

			}
			else
			{
				ReturnC->FunzioneChiamante = "LSReadImage";
			}
			LSDisconnect(Connessione,(HWND)hDlg);
		}
		else
		{
			ReturnC->FunzioneChiamante = "LSDocHandle";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}
	// libero le immagini
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	ReturnC->ReturnCode = Reply;
	
	return Reply;
}


int Ls515Class::Periferica::CalcolaSalvaDistanze(HWND hDlg, BITMAPINFOHEADER *pImage, short LineToCheck)
{
	BOOL fNeroFound;
	unsigned char *pDati;
	long nColorData;
	long Width, Diff;
	short NrBarreTrovate, OffsetBarra;
	short currPixel, PixelIni;
	short ii;


	ii = 0;
	/*for (ii=0;ii<1024;ii++)
		Distanze[ii]= 0;*/

	// Vado ad inizio dati
	if( pImage->biBitCount <= 8)
		nColorData = 1 << pImage->biBitCount;
	else
		nColorData = 0;
	pDati = (unsigned char *)pImage + sizeof (BITMAPINFOHEADER) + (nColorData * sizeof (RGBQUAD));

	// Mi sposto a meta' documento
	Width = pImage->biWidth;
	if( Diff = Width % 4 )
		Width += (4 - Diff);

	pDati += (Width * (pImage->biHeight / 2));

	// Cerco le barre nere
	NrBarreTrovate = 0;
	OffsetBarra = MM_OFFSET_BORDO;		// Parto da 1 mm dal bordo
	currPixel = OffsetBarra;

	// Se sono arrivato in fondo all'immagine finisco il loop
	while( currPixel < (pImage->biWidth - MM_OFFSET_BORDO) )
	{
		fNeroFound = FALSE;
		currPixel = OffsetBarra;
		while( currPixel < (pImage->biWidth - MM_OFFSET_BORDO) )
		{
			if( (*(pDati + currPixel) < SOGLIA_NERO_BIANCO) && !fNeroFound)
			{
				fNeroFound = TRUE;

				// Salvo il pixel di inizio barra nera
				if( NrBarreTrovate == 0 )
					PixelIni = currPixel;

				NrBarreTrovate ++;
				if( NrBarreTrovate == LineToCheck )
				{
					Distanze[ii] = currPixel - PixelIni;
					ii ++;
					// Reinizzializzo per il prossimo conteggio
					NrBarreTrovate = 0;
					break;
				}
			}
			else
			{
				// Ho ritrovato il bianco
				if( (*(pDati + currPixel) > SOGLIA_NERO_BIANCO) && fNeroFound )
				{
					fNeroFound = FALSE;

					// Salvo l'inizio del prossimo check
					if( NrBarreTrovate == 1 )
					{
						OffsetBarra = currPixel;
					}
				}
			}

			currPixel ++;
		}
	}
return ii-1;

} // CalcolaSalvaDistanze














int Ls515Class::Periferica::LeggiCodeline(short DocHandling)
{

	char	BufCodeLine[CODE_LINE_LENGTH];
	short	llBufCodeLine = 256;
	short ret;
	short Connessione;

	ReturnC->FunzioneChiamante = "LeggiCodeline";
	
	//-----------Open--------------------------------------------

	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	//Reply = LS515_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_WARNING,50,80,225);
		
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								READ_CODELINE_MICR,
								SIDE_NONE_IMAGE,
								SCAN_MODE_256GR200,
								AUTOFEED,
								SORTER_BAY1,
								WAIT_YES,
								BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);
		if (Reply == LS_OKAY)
		{
			////-----------ReadCodeLine--------------------------------------------
			
			Reply = LSReadCodeline(Connessione, (HWND)hDlg,
										   BufCodeLine,
										   &llBufCodeLine,
										   NULL, 0,
										   NULL, 0);

			
			if (Reply == LS_OKAY)
			{
				CodelineLetta = Marshal::PtrToStringAnsi((IntPtr) (char *)BufCodeLine);
			}
			else
			{
				ReturnC->FunzioneChiamante =  "LSReadCodeline";
			}
		}
		else
		{
			ReturnC->FunzioneChiamante =  "LSDocHandle";

		}			
	}
	else
	{
		ReturnC->FunzioneChiamante =  "LSOpen";
	}
	ret  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);
	
	//-----------Close-------------------------------------------
	LSDisconnect(Connessione,(HWND)hDlg);	
	
	ReturnC->ReturnCode = Reply;
	
	return Reply;
}



int Ls515Class::Periferica::LeggiCodelineImmaginiIncasella(short DocHandling)
{

	
	short	llBufCodeLine = 256;
	HANDLE HBackGray;
	HANDLE HFrontGray;
	//short Connessione;

	char *StrNome= NULL ; 
	short ret;
	short ReadMicr;
	short path;

	
	ReadMicr = NO_READ_CODELINE;
	
	ReturnC->FunzioneChiamante = "LeggiCodelineImmaginiincasella";
	
	//-----------Open--------------------------------------------


	//Reply = LS515_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
	
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_WARNING,50,80,225);
		// do via documento
	
		path = AUTOFEED;
	
	
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								ReadMicr,
								SIDE_ALL_IMAGE,
								SCAN_MODE_256GR200,
								path,
								DocHandling,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);
		if (Reply == LS_OKAY)
		{
		
			
			if (Reply == LS_OKAY)
			{				
				// Leggo l'immagine filmata
	
				Reply = LSReadImage(Connessione, (HWND)hDlg,
										CLEAR_ALL_BLACK,
										SIDE_ALL_IMAGE,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);
				
				if (Reply == LS_OKAY)
				{
					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();

					

					Reply = LSSaveDIB((HWND)hDlg, HFrontGray,StrNome);
					 // Always free the unmanaged string.

					if (Reply == LS_OKAY)
					{
						Marshal::FreeHGlobal(IntPtr(StrNome));
						StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();


						Reply = LSSaveDIB((HWND)hDlg,HBackGray,StrNome);									
					}	
					else
					{
						ReturnC->FunzioneChiamante =  "LSSaveimageFronte";
					}
				}
				else
				{
					ReturnC->FunzioneChiamante =  "LSReadImage";
				}

			}
			else
			{
				ReturnC->FunzioneChiamante =  "LSReadCodeline";
	//			Reply = LS5xx_Reset((HWND)hDlg, SUSPENSIVE_MODE, RESET_PERIPHERAL_FREE_PATH);
			}
			//BufCodeLine[llBufCodeLine] = '\0';
		}
		else
		{

			ReturnC->FunzioneChiamante =  "LSDocHandle";
	//			Reply = LS5xx_Reset((HWND)hDlg, SUSPENSIVE_MODE, RESET_PERIPHERAL_FREE_PATH);
		}			
	}
	else
	{
		ReturnC->FunzioneChiamante =  "LSOpen";
	}
	// libero le immagini
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	//ret = LS515_DoubleLeafingSensibility((HWND)hDlg,DOUBLE_LEAFING_DISABLE);
	//ret  = LS515_ConfigDoubleLeafing((HWND)hDlg,DOUBLE_LEAFING_ERROR,DOUBLE_LEAFING_DISABLE,0);
	ret  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);
	LSDisconnect(Connessione,(HWND)hDlg);
	ReturnC->ReturnCode = Reply;
	
	return Reply;
}



int Ls515Class::Periferica::LeggiCodelineImmaginiTrattieni()
{

	char	BufCodeLine[CODE_LINE_LENGTH];
	short	llBufCodeLine = 256;
	HANDLE HBackGray;
	HANDLE HFrontGray;


	char *StrNome= NULL ; 
	short ret;
	short ReadMicr;
	short path;
	short Conn;

	
	ReadMicr = NO_READ_CODELINE;
	
	ReturnC->FunzioneChiamante = "LeggiCodelineImmaginiTrattieni";
	
	//-----------Open--------------------------------------------

	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Conn);
	
	Connessione = Conn;
	//Reply = LS515_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
	
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_WARNING,50,80,225);
		// do via documento
	
		path = AUTOFEED;
	
	
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								ReadMicr,
								SIDE_ALL_IMAGE,
								SCAN_MODE_256GR200,
								path,
								HOLD_DOCUMENT,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);
		if (Reply == LS_OKAY)
		{
			// leggo la codeline solo se la devo leggere
		
				//-----------ReadCodeLine--------------------------------------------
		/*	Reply = LSReadCodeline(Connessione,(HWND)hDlg,
									   BufCodeLine,
									   &llBufCodeLine,
									   NULL, 0,
									   NULL, 0);*/
		
			
			if (Reply == LS_OKAY)
			{
				// setto la codeline di ritorno			
				CodelineLetta = Marshal::PtrToStringAnsi((IntPtr) (char *)BufCodeLine);
				
				// Leggo l'immagine filmata
	
				Reply = LSReadImage(Connessione, (HWND)hDlg,
										CLEAR_ALL_BLACK,
										SIDE_ALL_IMAGE,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);
				
				if (Reply == LS_OKAY)
				{
					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();

					

					Reply = LSSaveDIB((HWND)hDlg, HFrontGray,StrNome);
					 // Always free the unmanaged string.

					if (Reply == LS_OKAY)
					{
						Marshal::FreeHGlobal(IntPtr(StrNome));
						StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();


						Reply = LSSaveDIB((HWND)hDlg,HBackGray,StrNome);									
					}	
					else
					{
						ReturnC->FunzioneChiamante =  "LSSaveimageFronte";
					}
				}
				else
				{
					ReturnC->FunzioneChiamante =  "LSReadImage";
				}

			}
			else
			{
				ReturnC->FunzioneChiamante =  "LSReadCodeline";
	//			Reply = LS5xx_Reset((HWND)hDlg, SUSPENSIVE_MODE, RESET_PERIPHERAL_FREE_PATH);
			}
			//BufCodeLine[llBufCodeLine] = '\0';
		}
		else
		{

			ReturnC->FunzioneChiamante =  "LSDocHandle";
	//			Reply = LS5xx_Reset((HWND)hDlg, SUSPENSIVE_MODE, RESET_PERIPHERAL_FREE_PATH);
		}			
	}
	else
	{
		ReturnC->FunzioneChiamante =  "LSOpen";
	}
	// libero le immagini
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	//ret = LS515_DoubleLeafingSensibility((HWND)hDlg,DOUBLE_LEAFING_DISABLE);
	//ret  = LS515_ConfigDoubleLeafing((HWND)hDlg,DOUBLE_LEAFING_ERROR,DOUBLE_LEAFING_DISABLE,0);
	ret  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);

	ReturnC->ReturnCode = Reply;
	
	return Reply;
}





int Ls515Class::Periferica::LeggiCodelineImmagini(short DocHandling,short FMicr)
{

	char	BufCodeLine[CODE_LINE_LENGTH];
	short	llBufCodeLine = 256;
	HANDLE HBackGray;
	HANDLE HFrontGray;
	short Connessione;

	char *StrNome= NULL ; 
	short ret;
	short ReadMicr;
	short path;

	if (FMicr == 0)
	{
		ReadMicr = NO_READ_CODELINE;
	}
	else
	{
		ReadMicr = READ_CODELINE_MICR;
	}
	ReturnC->FunzioneChiamante = "LeggiCodelineImmagini";
	
	//-----------Open--------------------------------------------

	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	//Reply = LS515_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
	
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_WARNING,50,80,225);
		// do via documento
	
		path = AUTOFEED;
	
	
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								ReadMicr,
								SIDE_ALL_IMAGE,
								SCAN_MODE_256GR200,
								path,
								DocHandling,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);
		if (Reply == LS_OKAY)
		{
			// leggo la codeline solo se la devo leggere
			if (FMicr != 0)
			{
				//-----------ReadCodeLine--------------------------------------------
				Reply = LSReadCodeline(Connessione,(HWND)hDlg,
										   BufCodeLine,
										   &llBufCodeLine,
										   NULL, 0,
										   NULL, 0);
			}
			
			if (Reply == LS_OKAY)
			{
				// setto la codeline di ritorno
				if (FMicr != 0)
				{
					CodelineLetta = Marshal::PtrToStringAnsi((IntPtr) (char *)BufCodeLine);
				}
				// Leggo l'immagine filmata
	
				Reply = LSReadImage(Connessione, (HWND)hDlg,
										CLEAR_ALL_BLACK,
										SIDE_ALL_IMAGE,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);
				
				if (Reply == LS_OKAY)
				{
					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();

					

					Reply = LSSaveDIB((HWND)hDlg, HFrontGray,StrNome);
					 // Always free the unmanaged string.

					if (Reply == LS_OKAY)
					{
						Marshal::FreeHGlobal(IntPtr(StrNome));
						StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();


						Reply = LSSaveDIB((HWND)hDlg,HBackGray,StrNome);									
					}	
					else
					{
						ReturnC->FunzioneChiamante =  "LSSaveimageFronte";
					}
				}
				else
				{
					ReturnC->FunzioneChiamante =  "LSReadImage";
				}

			}
			else
			{
				ReturnC->FunzioneChiamante =  "LSReadCodeline";
	//			Reply = LS5xx_Reset((HWND)hDlg, SUSPENSIVE_MODE, RESET_PERIPHERAL_FREE_PATH);
			}
			//BufCodeLine[llBufCodeLine] = '\0';
		}
		else
		{

			ReturnC->FunzioneChiamante =  "LSDocHandle";
	//			Reply = LS5xx_Reset((HWND)hDlg, SUSPENSIVE_MODE, RESET_PERIPHERAL_FREE_PATH);
		}			
	}
	else
	{
		ReturnC->FunzioneChiamante =  "LSOpen";
	}
	// libero le immagini
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	//ret = LS515_DoubleLeafingSensibility((HWND)hDlg,DOUBLE_LEAFING_DISABLE);
	//ret  = LS515_ConfigDoubleLeafing((HWND)hDlg,DOUBLE_LEAFING_ERROR,DOUBLE_LEAFING_DISABLE,0);
	ret  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);
	//-----------Close-------------------------------------------
	LSDisconnect(Connessione,(HWND)hDlg);	
	ReturnC->ReturnCode = Reply;
	
	return Reply;
}




int Ls515Class::Periferica::LeggiCodelineImmaginiAuto(short DocHandling)
{

	char	BufCodeLine[CODE_LINE_LENGTH];
	short	llBufCodeLine = 256;
	HANDLE HBackGray;
	HANDLE HFrontGray;


	
	char *StrNome ; 
	unsigned long NrDoc;
	int ii=0;



	short ret=0;

	ReturnC->FunzioneChiamante = "LeggiCodelineImmaginiAuto";
	
	//-----------Open--------------------------------------------
//	Reply = LS515_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if ((Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) || (Reply == LS_SORTER1_FULL) || (Reply == LS_SORTER2_FULL) ||(Reply == LS_DOUBLE_LEAFING_WARNING))
	{
		
		Reply  = LSGetDocData(Connessione,
					((HWND)hDlg),
					&NrDoc,
		 			NULL,//FilenameFront,
		 			NULL,//FilenameBack,
		 			NULL,//Reserved1,		// not used must be NULL
		 			NULL,//Reserved2,		// not used must be NULL
		 			(LPHANDLE *)&HFrontGray,
		 			(LPHANDLE *)&HBackGray,
		 			NULL,//*Reserved3,		// not used must be NULL
		 			NULL,//*Reserved4,		// not used must be NULL
		 			NULL,//CodelineSW,
		 			BufCodeLine,
		 			NULL,//BarCode,
		 			NULL,//CodelinesOptical,
		 			NULL,//*DocToRead,
		 			NULL,//*NrPrinted,
		 			NULL,//Reserved5,		// not used must be NULL
			 		NULL);//Reserved6);		// not used must be NULL
	
		if ((Reply == LS_OKAY) || (Reply == LS_SORTER1_FULL) || (Reply == LS_SORTER2_FULL) ||(Reply == LS_DOUBLE_LEAFING_WARNING))
		{
			// setto la codeline di ritorno
			CodelineLetta = Marshal::PtrToStringAnsi((IntPtr) (char *)BufCodeLine);
			
			
			//for (int ii=0;ii<= NomeFileImmagine->Length-1;ii++)
			//{
//				StrNome[ii] = NomeFileImmagine->get_Chars(ii);
			//}
			if (HFrontGray)
			{
				StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
				
				LSSaveDIB((HWND)hDlg,HFrontGray,StrNome);

				 // Always free the unmanaged string.
				 Marshal::FreeHGlobal(IntPtr(StrNome));
			}

			if(HBackGray)
			{
				//NomeFileImmagine = "..\\Dati\\BackImage.bmp";	
			//	for (int ii=0;ii<256;ii++)
			//		StrNome[ii] = 0;
				//for (int ii=0;ii<= NomeFileImmagineRetro->Length-1;ii++)
				//{
//					StrNome[ii] = NomeFileImmagineRetro->get_Chars(ii);
				//}
				StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();

				
				 LSSaveDIB((HWND)hDlg,HBackGray,StrNome);

				 	 // Always free the unmanaged string.
					  Marshal::FreeHGlobal(IntPtr(StrNome));
				
			}	
			/*else
			{
				ReturnC->FunzioneChiamante =  "LS515_SaveimageFronte";
			}
			*/
			
			//BufCodeLine[llBufCodeLine] = '\0';
		}
		else
		{
	
			ReturnC->FunzioneChiamante =  "LSGetdocData";
	//			Reply = LS5xx_Reset((HWND)hDlg, SUSPENSIVE_MODE, RESET_PERIPHERAL_FREE_PATH);
		}			
	}
	// libero le immagini
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	//ret = LS515_DoubleLeafingSensibility((HWND)hDlg,DOUBLE_LEAFING_DISABLE);
	//ret  = LS515_ConfigDoubleLeafing((HWND)hDlg,DOUBLE_LEAFING_ERROR,DOUBLE_LEAFING_DISABLE,0);

	ReturnC->ReturnCode = Reply;
	
	return Reply;
}




int Ls515Class::Periferica::Stopaaaaa()
{
	
	char	BufCodeLine[CODE_LINE_LENGTH];
	short	llBufCodeLine = 256;
	HANDLE HBackGray;
	HANDLE HFrontGray;


	unsigned long NrDoc;
	int ii=0;
	Reply = 0;
	
	LSStopAutoDocHandle(Connessione,(HWND)hDlg);
	do
	{
		Reply  = LSGetDocData(Connessione,
						((HWND)hDlg),
						&NrDoc,
		 				NULL,//FilenameFront,
		 				NULL,//FilenameBack,
		 				NULL,//Reserved1,		// not used must be NULL
		 				NULL,//Reserved2,		// not used must be NULL
		 				(LPHANDLE *)&HFrontGray,
		 				(LPHANDLE *)&HBackGray,
		 				NULL,//*Reserved3,		// not used must be NULL
		 				NULL,//*Reserved4,		// not used must be NULL
		 				NULL,//CodelineSW,
		 				BufCodeLine,
		 				NULL,//BarCode,
		 				NULL,//CodelinesOptical,
		 				NULL,//*DocToRead,
		 				NULL,//*NrPrinted,
		 				NULL,//Reserved5,		// not used must be NULL
			 			NULL);//Reserved6);		// not used must be NULL
		
		// libero le immagini
		if (HFrontGray)
			LSFreeImage((HWND)hDlg, &HFrontGray);
		if (HBackGray)
			LSFreeImage((HWND)hDlg, &HBackGray);
	}while(Reply == LS_OKAY);

	//ret  = LS515_ConfigDoubleLeafing((HWND)hDlg,DOUBLE_LEAFING_ERROR,DOUBLE_LEAFING_DISABLE,0);
	Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);
	//LS515_Wait((HWND)hDlg, &ret, NULL);
	

	LSDisconnect(Connessione,(HWND)hDlg);	
	return Reply;
}


int Ls515Class::Periferica::ViaDocumentiAuto( short ScanMode,short ValDoppia)
{

//	char	BufCodeLine[CODE_LINE_LENGTH];
	short	llBufCodeLine = 256;
	HANDLE HBackGray=0;
	HANDLE HFrontGray=0;
	short miaconn;

//	char StrNome[256];

	short ret=0;

	short nrDoc = 0;
	short Sorter;


	
	//-----------Open--------------------------------------------
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&miaconn);
	
	
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
		Reply  = LSConfigDoubleLeafingAndDocLength(miaconn,(HWND)hDlg,DOUBLE_LEAFING_ERROR,ValDoppia,80,225);

	
		if(ScanMode == SCAN_MODE_COLOR_200)
		{
			Sorter = SORTER_BAY1;
		}
		else
		{
			Sorter = SORTER_BAY2;
		}

		Reply = LSAutoDocHandle(	miaconn,
						((HWND)(hDlg)),
							NO_STAMP,
							NO_PRINT_VALIDATE,
							READ_CODELINE_MICR,
							ScanMode,
							AUTOFEED,
							Sorter,
							nrDoc,
							CLEAR_ALL_BLACK,
							SIDE_ALL_IMAGE,
							READMODE_ALL,
							IMAGE_SAVE_HANDLE,
							NULL,
							NULL,
							0,
							0,
							0,
							0,
							BOTTOM_RIGHT_MM,
							0,
							SAVE_JPEG,
								100,
								SAVE_OVERWRITE,
								1,
							WAIT_YES,
							BEEP,
							NULL,
							NULL,
							NULL);

		if (Reply == LS_OKAY)
		{
			
			
			
		}
		else
		{

			ReturnC->FunzioneChiamante =  "LSautodocHandle";
	
		}			
	}
	else
	{
		ReturnC->FunzioneChiamante =  "LSOpen";
	}
	// libero le immagini
	
	ret  = LSConfigDoubleLeafingAndDocLength(miaconn,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);
	//-----------Close-------------------------------------------
//	LSDisconnect(Connessione,(HWND)hDlg);	
	ReturnC->ReturnCode = Reply;
	Connessione = miaconn ;
	return Reply;
}






int Ls515Class::Periferica::Timbra(short DocHandling)
{

	DateTime iniTime,currTime;
	TimeSpan t;
	HANDLE HBackGray;
	HANDLE HFrontGray;
	char *StrNome = NULL; 
	short Connessione;

	ReturnC->FunzioneChiamante = "Timbra";
	//-----------Open--------------------------------------------Diagnostica
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	//Reply = LS515_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
		// Do il via al documento
		///*Reply = LS515_DocHandle((HWND)hDlg,
		//					SUSPENSIVE_MODE,
		//					FRONT_STAMP,
		//					NO_PRINT_VALIDATE,
		//					NO_READ_CODELINE,
		//					SIDE_ALL_IMAGE,
		//					SCAN_MODE_256GR200,
		//					AUTOFEED,
		//					SORTER_BAY1,
		//					WAIT_YES,
		//					BEEP,
		//					0,
		//					0);*/
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
								FRONT_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								SIDE_ALL_IMAGE,
								SCAN_MODE_256GR200,
								AUTOFEED,
								SORTER_BAY1,
								WAIT_YES,
								BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);
			
		if (Reply == LS_OKAY)
		{
	//		Reply = LS515_ReadImage((HWND)hDlg,
	//							SUSPENSIVE_MODE,
	//							CLEAR_ALL_BLACK,
	////							(char)(TypeLS == TYPE_LS515_5 ? SIDE_FRONT_RED_IMAGE : SIDE_FRONT_IMAGE),
	//							SIDE_ALL_IMAGE,
	//							(LPHANDLE)&HFrontGray,
	//							(LPHANDLE)&HBackGray,
	//							NULL,
	//							NULL);
			Reply = LSReadImage(Connessione, (HWND)hDlg,
										CLEAR_ALL_BLACK,
										SIDE_ALL_IMAGE,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);
				
			if (Reply == LS_OKAY)
			{
				
				Marshal::FreeHGlobal(IntPtr(StrNome));
				StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();				
				
				Reply = LSSaveDIB((HWND)hDlg,HFrontGray,StrNome);
				if (Reply == LS_OKAY)
				{
					Marshal::FreeHGlobal(IntPtr(StrNome));
					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();
				
					Reply = LSSaveDIB((HWND)hDlg,HBackGray,StrNome);
					
				}	
				else
				{
					ReturnC->FunzioneChiamante =  "LSSaveimageFronte";
				}
			}
			else
			{
				ReturnC->FunzioneChiamante = "LSReadImage";
			}
		
		}
		else
		{
			ReturnC->FunzioneChiamante = "LSDocHandle";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}

	ReturnC->ReturnCode = Reply;
	
	LSDisconnect(Connessione,(HWND)hDlg);
	
	return Reply;
}


int Ls515Class::Periferica::TestStampa(int StampaStd)
{

	String ^strstp;
	char *str = NULL; 
	DateTime iniTime,currTime;
	TimeSpan t;
	HANDLE HBackGray;
	HANDLE HFrontGray;
	char *StrNome = NULL; 
	short Connessione;

	ReturnC->FunzioneChiamante =  "TestStampa";

	strstp = "TEST INKJET 888888888888 HHHHHHHHHHHHHHHH 0000000000000000";
	
	if (StampaStd)
	{
		strstp = Stampafinale;
	}
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	//Reply = LS515_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
		Marshal::FreeHGlobal(IntPtr(str));
		str = (char*) Marshal::StringToHGlobalAnsi(strstp ).ToPointer();		
				
		Reply = LSLoadString(Connessione,(HWND)hDlg,
								 FORMAT_BOLD ,//FORMAT_NORMAL,//: ) FORMAT_BOLD 
								 strstp->Length,
								 str);	
		if (Reply == LS_OKAY)
		{
			// via doc
			///*Reply = LS515_DocHandle((HWND)hDlg,
			//			SUSPENSIVE_MODE,
			//			NO_FRONT_STAMP,
			//			PRINT_VALIDATE,
			//			NO_READ_CODELINE,
			//			SIDE_ALL_IMAGE,
			//			SCAN_MODE_256GR200,
			//			AUTOFEED,
			//			SORTER_BAY1,
			//			WAIT_YES,
			//			NO_BEEP,
			//			0,
			//			0);*/
			Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								PRINT_VALIDATE,
								NO_READ_CODELINE,
								SIDE_ALL_IMAGE,
								SCAN_MODE_256GR200,
								AUTOFEED,
								SORTER_BAY1,
								WAIT_YES,
								BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);
			if (Reply == LS_OKAY)
			{
			//	Reply = LS515_ReadImage((HWND)hDlg,
			//							SUSPENSIVE_MODE,
			//							CLEAR_ALL_BLACK,
			////							(char)(TypeLS == TYPE_LS515_5 ? SIDE_FRONT_RED_IMAGE : SIDE_FRONT_IMAGE),
			//							SIDE_ALL_IMAGE,
			//							(LPHANDLE)&HFrontGray,
			//							(LPHANDLE)&HBackGray,
			//							NULL,
			//							NULL);
				Reply = LSReadImage(Connessione, (HWND)hDlg,
										CLEAR_ALL_BLACK,
										SIDE_ALL_IMAGE,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);
				
				if (Reply == LS_OKAY)
				{
					Marshal::FreeHGlobal(IntPtr(StrNome));
					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();			
					
					Reply = LSSaveDIB((HWND)hDlg,HFrontGray,StrNome);
					if (Reply == LS_OKAY)
					{
						Marshal::FreeHGlobal(IntPtr(StrNome));
						StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();			
						
						Reply = LSSaveDIB((HWND)hDlg,HBackGray,StrNome);
						
					}	
					else
					{
						ReturnC->FunzioneChiamante =  "LSSaveimageFronte";
					}
				}
				else
				{
					ReturnC->FunzioneChiamante = "LSReadImage";
				}

			}
			else
			{
				ReturnC->FunzioneChiamante =  "LSDocHandle";
			}
		}
		else
		{
			ReturnC->FunzioneChiamante =  "LSLoadString";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante =  "LSOpen";
	}

	ReturnC->ReturnCode = Reply;


	LSDisconnect(Connessione,(HWND)hDlg);

	return Reply;
}


int Ls515Class::Periferica::TestStampaAlto(int StampaStd)
{

	String  ^strstp;
	char *str = NULL; 
	DateTime iniTime,currTime;
	TimeSpan t;
	HANDLE HBackGray;
	HANDLE HFrontGray;
	char *StrNome = NULL; 
	short Connessione;

	ReturnC->FunzioneChiamante = "TestStampaAlto";
	

	strstp = "TEST INKJET 888888888888 HHHHHHHHHHHHHHHH 0000000000000000";
	
	if (StampaStd)
	{
		strstp = Stampafinale;
	}
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	//Reply = LS515_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
			Marshal::FreeHGlobal(IntPtr(str));
			str = (char*) Marshal::StringToHGlobalAnsi(strstp ).ToPointer();			
		
			Reply = LSLoadString(Connessione,(HWND)hDlg,
									 PRINT_UP_FORMAT_BOLD,//PRINT_UP_FORMAT_NORMAL,//: ) FORMAT_BOLD 
									 strstp->Length,
									 str);
			
			if (Reply == LS_OKAY)
			{
				// via doc
				///*Reply = LS515_DocHandle((HWND)hDlg,
				//			SUSPENSIVE_MODE,
				//			NO_FRONT_STAMP,
				//			PRINT_VALIDATE,
				//			NO_READ_CODELINE,
				//			SIDE_ALL_IMAGE,
				//			SCAN_MODE_256GR200,
				//			AUTOFEED,
				//			SORTER_BAY1,
				//			WAIT_YES,
				//			NO_BEEP,
				//			0,
				//			0);*/
				Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								PRINT_VALIDATE,
								NO_READ_CODELINE,
								SIDE_ALL_IMAGE,
								SCAN_MODE_256GR200,
								AUTOFEED,
								SORTER_BAY1,
								WAIT_YES,
								BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);
				if (Reply == LS_OKAY)
				{
			//			Reply = LS515_ReadImage((HWND)hDlg,
			//							SUSPENSIVE_MODE,
			//							CLEAR_ALL_BLACK,
			////							(char)(TypeLS == TYPE_LS515_5 ? SIDE_FRONT_RED_IMAGE : SIDE_FRONT_IMAGE),
			//							SIDE_ALL_IMAGE,
			//							(LPHANDLE)&HFrontGray,
			//							(LPHANDLE)&HBackGray,
			//							NULL,
			//							NULL);
						Reply = LSReadImage(Connessione, (HWND)hDlg,
										CLEAR_ALL_BLACK,
										SIDE_ALL_IMAGE,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										NULL,
										NULL);
				
				if (Reply == LS_OKAY)
				{
					Marshal::FreeHGlobal(IntPtr(StrNome));
					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();			
					
					Reply = LSSaveDIB((HWND)hDlg,HFrontGray,StrNome);
					if (Reply == LS_OKAY)
					{
						Marshal::FreeHGlobal(IntPtr(StrNome));
						StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();			

						Reply = LSSaveDIB((HWND)hDlg,HBackGray,StrNome);
						
					}	
					else
					{
						ReturnC->FunzioneChiamante =  "LSSaveimageFronte";
					}
				}
				else
				{
					ReturnC->FunzioneChiamante = "LSReadImage";
				}
			}
			else
			{
				ReturnC->FunzioneChiamante = "LSDocHandle";
			}
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}

	ReturnC->ReturnCode = Reply;
	
	LSDisconnect(Connessione,(HWND)hDlg);
	return Reply;
}


int Ls515Class::Periferica::LeggiBadge(short traccia)
{

	short Length = 256;
	char strBadge[256];
	short Connessione;

	ReturnC->FunzioneChiamante = "LeggiBadge";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	//Reply = LS515_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
		if (traccia == 1)
			Reply = LSReadBadgeWithTimeout(Connessione,(HWND)hDlg,FORMAT_IATA_ABA ,256,
				strBadge,&Length,10000);
		if (traccia == 2)
				Reply = LSReadBadge(Connessione, (HWND)hDlg, FORMAT_ABA_MINTS, 256, strBadge, &Length);
			/*Reply = LSReadBadgeWithTimeout(Connessione,(HWND)hDlg,FORMAT_ABA_MINTS ,256,
				strBadge,&Length,10000);*/
		if ( traccia == 3)
			Reply = LSReadBadgeWithTimeout(Connessione,(HWND)hDlg,FORMAT_IATA_ABA_MINTS ,256,
				strBadge,&Length,10000);

		
		BadgeLetto = Marshal::PtrToStringAnsi((IntPtr) (char *)strBadge);

	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}

	ReturnC->ReturnCode = Reply;
	
	LSDisconnect(Connessione,(HWND)hDlg);
	return Reply;
}



int Ls515Class::Periferica::SerialNumber()
{

	short Length = 256;
	char *str;
	short ii=0;
	short Connessione;	

	str = (char*) Marshal::StringToHGlobalAnsi(SSerialNumber ).ToPointer();
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	//Reply = LS515_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
		Reply = LSSetSerialNumber(Connessione,(HWND)hDlg, (unsigned char *)str,1962);
	}
	ReturnC->ReturnCode = Reply;
	LSDisconnect(Connessione,(HWND)hDlg);

	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Serial Number";

	return Reply;
}

int Ls515Class::Periferica::Visual(char * str)
{
	//Ls515Class::FDownload *f;
	
	//f = new Ls515Class::FDownload;
	//f = 
	//fShow();
	//f->PDwl->Value ++;
	return 0;
}


int Ls515Class::Periferica::SetConfigurazione(int E13B,int CMC7,int timbro,int badge12,int badge23,int badge123,int scannerfronte, int scannerretro,int inkjet,int VelocitaBassa,int Sflogliatore50,int ScannerType,int SwAbilitato,int fCardCol)
{

	char BytesCfg[4];
	char IdentStr[64],Version[64],Date_Fw[64],LsName[64],SerialNumber[64],FeederVersion[64];
	char BoardNr[8];
	char CpldNr[8];
	short Connessione;

	CpldNr[1]= 0;
	CpldNr[2]= 0;
	//HWND 
	HINSTANCE hInst = 0;

	// setto configurazione periferica da file
	// Azzero la variabile
	//memset(BytesCfg, 0x40, sizeof(BytesCfg));
	BytesCfg[0] = 0x40;
	BytesCfg[1] = 0x40;
	BytesCfg[2] = 0x40;
	BytesCfg[3] = 0x40;

	if( E13B  || CMC7  )
	{
		BytesCfg[0] |= 0x03;
	}
	if( timbro)
	{
		BytesCfg[0] |= 0x20;
	}
	
	if( badge12 || badge23 || badge123)
	{
		BytesCfg[1] |= 0x10;
	}
	if(inkjet)
	{
		BytesCfg[0] |= 0x08;
	}

	// doppia sfogliatura
	BytesCfg[1] |= 0x20;
	
	if( scannerfronte && scannerretro )
	{
		BytesCfg[1] |= 0x03;		
	}
	else if( scannerretro )
	{
		BytesCfg[1] |= 0x03;
	}
	else if(scannerfronte )
	{
		BytesCfg[1] |= 0x03;	
	}

	if( inkjet)
	{
		BytesCfg[0] |= 0x08;
	}

	
	if (VelocitaBassa == 0)
	{
		//SETTO VELOCITA' ALTA
		BytesCfg[2] |= 0x02;
	}


	switch( ScannerType )
	{
	case 62:	// Scanner a grigio
		break;
	case 63:	// Scanner a colori
		BytesCfg[1] |= 0x08;
		break;
	case 64:	// Scanner a grigio e UV
		BytesCfg[1] |= 0x04;
		break;
	case 75:	// Scanner a colori e UV
		BytesCfg[1] |= 0x04;
		BytesCfg[1] |= 0x08;
		break;
	}

	if(SwAbilitato >0)
	{
		BytesCfg[3] |= SwAbilitato;
	}
	if (fCardCol)
	{
		BytesCfg[2] |= 0x02;
	}

	
	// open
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	//Reply = LS515_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
		
		Reply = LS515_WriteConfiguration((HWND)hDlg,BytesCfg );
		// Setto la configurazione corrente
		Sleep(500);
		Reply = LSSetConfiguration(Connessione,(HWND)hDlg,BytesCfg );
		if (Reply == LS_OKAY)
		{//
			//Reply =	LS515_Identify((HWND)0, SUSPENSIVE_MODE,IdentStr,LsName,Version,Date_Fw,BoardNr,FeederVersion,SerialNumber,NULL);
			Reply =	LSUnitIdentify(Connessione,(HWND)0, IdentStr,LsName,Version,Date_Fw,SerialNumber,BoardNr, NULL, NULL, FeederVersion, NULL, NULL, NULL, NULL);
			
			if (Reply == LS_OKAY)
			{
					SIdentStr = Marshal::PtrToStringAnsi((IntPtr) (char *) IdentStr);
						
					SVersion = Marshal::PtrToStringAnsi((IntPtr) (char *)Version);
					SDateFW = Marshal::PtrToStringAnsi((IntPtr) (char *)Date_Fw);
					SLsName = Marshal::PtrToStringAnsi((IntPtr) (char *)LsName);
					SSerialNumber = Marshal::PtrToStringAnsi((IntPtr) (char *)SerialNumber);
					SFeederVersion = Marshal::PtrToStringAnsi((IntPtr) (char *)FeederVersion);
					SBoardNr = BoardNr[0].ToString();
					CpldNr[0] =  BoardNr[1];
					SCpldNr = Marshal::PtrToStringAnsi(static_cast<IntPtr>(CpldNr));
					SDateFPGA = Marshal::PtrToStringAnsi(static_cast<IntPtr>(&BoardNr[4]));
					
					TypeLS = GetTypeLS(LsName, Version, IdentStr);
					ConfMicr = true;
					
				
						
				
				}
				else
				{
					ReturnC->FunzioneChiamante = "LSIdentify";
				}
			}
			else
			{
				ReturnC->FunzioneChiamante = "LSSet_cfg";
			}

	}	
	// close
	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Set Configurazione";

	LSDisconnect(Connessione,(HWND)hDlg);

	return Reply;
}

int Ls515Class::Periferica::SetHightSpeedMode()
{

	char BytesCfg[8];
	char Version[64],Date_Fw[64],LsName[64],SerialNumber[64],FeederVersion[64];
	char BoardNr[8];
	char CpldNr[8];
	short Connessione;
	char BytesCfgwrite[8];

	CpldNr[1]= 0;
	CpldNr[2]= 0;
	
	HINSTANCE hInst = 0;

	memset(BytesCfgwrite,0x00,sizeof(BytesCfgwrite));
	
	
	
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
		Reply =	LSUnitIdentify(Connessione,(HWND)0, BytesCfg,LsName,Version,Date_Fw,SerialNumber,BoardNr, NULL, NULL, FeederVersion, NULL, NULL, NULL, NULL);
			
		if (Reply == LS_OKAY ) 
		{
			//Nel ls515 e nel Ls100 il BytesCfg[0] viene aggiunto dalla DLL..
			BytesCfgwrite[0] = BytesCfg[1];
			BytesCfgwrite[1] = BytesCfg[2];
			BytesCfgwrite[2] = BytesCfg[3];
			BytesCfgwrite[3] = BytesCfg[4];
			
			//SETTO VELOCITA' ALTA
			BytesCfgwrite[2] |= 0x02;
			
			
			Reply = LS515_WriteConfiguration((HWND)hDlg,BytesCfgwrite );
			
			// Setto la configurazione corrente
			Sleep(100);
		}
	}	
	// close
	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Set Configurazione";

	LSDisconnect(Connessione,(HWND)hDlg);

	return Reply;
}


int Ls515Class::Periferica::SetValoreDefImmagine()
{
	short NewValue;
	short Connessione;
	
	NewValue = 1427;

	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	//Reply = LS515_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
		Reply = LSImageCalibration(Connessione,(HWND)hDlg, TRUE, &NewValue);
	}
	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Calibra immagine";

	LSDisconnect(Connessione,(HWND)hDlg);

	return Reply;
}



int Ls515Class::Periferica::CalibraImmagine()
{

	int ii=0;
	short ret=0;
	short Connessione;

		// open
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	//Reply = LS515_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	
	{
		Reply = DoImageCalibration(Connessione,(HWND)hDlg, 1654);
	}	
	// close

	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Calibra immagine";

	
	LSDisconnect(Connessione,(HWND)hDlg);

	return Reply;

}




int Ls515Class::Periferica::StatoPeriferica()
{
	unsigned char Sense;
	unsigned char Status[16];
	short Connessione;

	ReturnC->FunzioneChiamante = "Stato Periferica";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	//Reply = LS515_Open((HWND)hDlg,(HANDLE)hInst,SUSPENSIVE_MODE);
	if ((Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN))
	{
		Reply = LSPeripheralStatus(Connessione,(HWND)hDlg, &Sense, Status);
		
		StatusByte[0] = Status[0];
		StatusByte[1] = Status[1];
		StatusByte[2] = Status[2];
		StatusByte[3] = Status[3];
		StatusByte[4] = Status[4];
		StatusByte[5] = Status[5];
		StatusByte[6] = Status[6];
		StatusByte[7] = Status[7];
		StatusByte[8] = Status[8];
		StatusByte[9] = Status[9];
		StatusByte[10] = Status[10];
		StatusByte[11] = Status[11];
		StatusByte[12] = Status[12];
		StatusByte[13] = Status[13];
		StatusByte[14] = Status[14];
		StatusByte[15] = Status[15];
		LSDisconnect(Connessione,(HWND)hDlg);
	}
	ReturnC->ReturnCode = Reply;

	return Reply;
}


int Ls515Class::Periferica::DoImageCalibration(short Connessione,HWND hDlg, float TeoricValue)
{
	
	long	*BufFrontImage;
	int	nRet;
	int fretry;
	float	RealValue, DiffPercentuale;
	short	OldValue, NewValue;
	

	nRet = TRUE;
	fretry = FALSE;
	
	
	
	{
		//Reply = LSUnitIdentify(hLS, hDlg, IdentStr, LsName, Version, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

		//----------- Set Block Document if double leafing -----------
		//Reply = LS515_DoubleLeafingSensibility(hDlg, DOUBLE_LEAFING_DISABLE);
		//Reply  = LS515_ConfigDoubleLeafing((HWND)hDlg,DOUBLE_LEAFING_ERROR,DOUBLE_LEAFING_DISABLE,0);
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);
		//----------- Set attesa introduzione documento -----------
		Reply = LSDisableWaitDocument(Connessione,hDlg, FALSE);


	
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
									NO_STAMP,
									NO_PRINT_VALIDATE,
									NO_READ_CODELINE,
									SIDE_FRONT_IMAGE,
									SCAN_MODE_256GR200,
									AUTOFEED,
									SORTER_SWITCH_1_TO_2,
									WAIT_YES,
									NO_BEEP,
									NULL,
									0,
									0);

		if( Reply != LS_OKAY )
		{
			fretry = TRUE;
			return Reply;	
		}
		else
		{
		
			Reply = LSReadImage(Connessione, (HWND)hDlg,
											CLEAR_ALL_BLACK,
											SIDE_FRONT_IMAGE,
											0, 0,
											(LPHANDLE)&BufFrontImage,
											NULL,
											NULL,
											NULL);
					

			if (Reply != LS_OKAY)
			{
				fretry = TRUE;
				return Reply;

			}

			// Tolgo il nero davanti e dietro e NON sopra per avere 864 righe
			ClearFrontAndRearBlack( (BITMAPINFOHEADER *)BufFrontImage );

			
			// Leggo lunghezza image
			RealValue = (float)((BITMAPINFOHEADER *)BufFrontImage)->biWidth;

			// Leggo il valore settato nella periferica
			Reply = LSImageCalibration(Connessione,hDlg, FALSE, &OldValue);
			if( Reply != LS_OKAY )
			{
				CheckReply(hDlg, Reply, "LSImageCalibration");
				fretry = TRUE;
				return Reply;
			}


			// Controllo lunghezza image
			DiffPercentuale = TeoricValue * (float)0.005;		// 0.5%

			if(	RealValue < (TeoricValue - DiffPercentuale) )
			{
				// Calcolo il valore in percentuale da sommare a quello presente nella perifeica
				DiffPercentuale = TeoricValue - RealValue;
				DiffPercentuale = DiffPercentuale * (float)100 / TeoricValue;

				NewValue = (unsigned short)((float)OldValue * DiffPercentuale) / 100;
				NewValue = OldValue - NewValue;

				

				nRet = FALSE;	// Devo riprovare la calibrazione
				fretry = TRUE;
				Reply = LS_RETRY;
				
			}

			// Controllo lunghezza image
			DiffPercentuale = TeoricValue * (float)0.005;		// 0.5%
			
			if( RealValue > (TeoricValue + DiffPercentuale) )
			{
				// Calcolo il valore in percentuale da sommare a quello presente nella perifeica
				DiffPercentuale = RealValue - TeoricValue;
				DiffPercentuale = DiffPercentuale * (float)100 / TeoricValue;

				NewValue = (unsigned short)((float)OldValue * DiffPercentuale) / 100;
				NewValue += OldValue;

			

				nRet = FALSE;	// Devo riprovare la calibrazione
				fretry = TRUE;
				Reply = LS_RETRY;
			}


			// Se nRet = FALSE ho variato i valori, perci� vado a scrivere quelli nuovi
			if( fretry == TRUE)
			{
				

				Reply = LSImageCalibration(Connessione,hDlg, TRUE, &NewValue);
				if( Reply != LS_OKAY )
				{
					CheckReply(hDlg, Reply, "LSImageCalibration");
					nRet = TRUE;
					fretry = TRUE;
					return Reply;
					
				}
				else
				{
					Reply = LS_RETRY;
				}
			}

			// Libero le aree bitmap ritornate
			if( BufFrontImage )
				GlobalFree( BufFrontImage );
		}
	}

	return Reply;
} // DoImageCalibration








void Ls515Class::Periferica::ClearFrontAndRearBlack(BITMAPINFOHEADER *Bitmap)
{
	unsigned long ii, jj;
	long	CondDib;
	short	ValoriOk, NrColonneNere;
	long	NrByte;
	BOOL	ContinueClear;
	BOOL	HalfByteHigh;
	unsigned char *pbm, *pbmt, *pbmtm;
	unsigned char HighValue;
	long	RealWidth, DiffWidth;
	long	nr_col_bmp, nr_row_bmp;


	//Test se � una bitmap a 16 o 256 livelli di grigio
	if( Bitmap->biBitCount == 4 )
	{
		// Bitmap a 16 grigi

//	SalvaDIBBitmap(Bitmap, "\\Progetti\\bmp\\front2.bmp");

		// Inizio dati bitmap
		pbm = (unsigned char *)Bitmap + sizeof(BITMAPINFOHEADER) + (16 * sizeof(RGBQUAD));
		NrByte = Bitmap->biWidth / 2;
		if(CondDib = (Bitmap->biWidth % 8) )
			nr_col_bmp = (Bitmap->biWidth + 8 - CondDib) / 2;
		else
			nr_col_bmp = NrByte;
		nr_row_bmp = Bitmap->biHeight;
		pbmt = pbm;
		ContinueClear = TRUE;
		HalfByteHigh = TRUE;
		ValoriOk = 0;
		NrColonneNere = 0;

		// Tolgo le colonne nere davanti
		do
		{
			for( ii = 0; ii < (unsigned long)nr_row_bmp; ii ++, pbmt += nr_col_bmp)
			{
				// Prendo il valore di grigio parte alta se colonna pari, altrimenti parte bassa
				if( HalfByteHigh )
					HighValue = *pbmt >> 4;
				else
					HighValue = *pbmt & 0x0f;

				if( HighValue > LIMITE_16_GRIGI )
					ValoriOk ++;
			}

			//Incremento il counter nr. colonne nere
			if( ValoriOk <= NR_VALORI_OK )
			{
				NrColonneNere ++;
				ValoriOk = 0;
			}
			else
				ContinueClear = FALSE;

			// Reinizializzo il pointer ad inizio bitmap
			pbmt = pbm + (NrColonneNere / 2);

			HalfByteHigh = !HalfByteHigh;

		} while( ContinueClear && (NrColonneNere < (nr_col_bmp * 2)) );


		//Controllo se shiftare la bitmap
		if( NrColonneNere && (NrColonneNere < (nr_col_bmp * 2)) )
		{
			// Inizializzo i pointer
			pbmt = pbmtm = pbm;
			NrByte = nr_col_bmp;

			// Calcolo il NUOVO numero di colonne e lo salvo
			nr_col_bmp *= 2;
			nr_col_bmp -= NrColonneNere;
			Bitmap->biWidth = nr_col_bmp;

			if( CondDib = (nr_col_bmp % 8) )
				nr_col_bmp = (nr_col_bmp + 8 - CondDib);

			// Trasformo in nr. di byte
			NrColonneNere /= 2;

			// Sposto la bitmap
			for( ii = 0; ii < (unsigned long)Bitmap->biHeight; ii ++)
			{
				pbmt += NrColonneNere;
				for( jj = NrColonneNere; jj < (unsigned long)NrByte; jj ++)
					*(pbmtm++) = *(pbmt++);

				// Aggiungo il nero
				for( jj = 0; jj < (unsigned long)((nr_col_bmp - Bitmap->biWidth) / 2); jj ++)
					*(pbmtm++) = 0x00;	//NERO;
			}

			// Aggiorno il nr. di bytes della bitmap
			Bitmap->biSizeImage = Bitmap->biHeight * nr_col_bmp / 2;
		}

//	SalvaDIBBitmap(Bitmap, "\\Progetti\\bmp\\front3.bmp");


		// Inizio dati bitmap
		pbm = (unsigned char *)Bitmap + sizeof(BITMAPINFOHEADER) + (16 * sizeof(RGBQUAD));
		NrByte = Bitmap->biWidth / 2;
		if(CondDib = (Bitmap->biWidth % 8) )
			nr_col_bmp = (Bitmap->biWidth + 8 - CondDib) / 2;
		else
			nr_col_bmp = NrByte;
		nr_row_bmp = Bitmap->biHeight;
		pbmt = pbm + (nr_col_bmp - 1);
		ContinueClear = TRUE;
		HalfByteHigh = FALSE;
		ValoriOk = 0;
		NrColonneNere = 0;

		// Tolgo le colonne nere dietro
		do
		{
			for( ii = 0; ii < (unsigned long)nr_row_bmp; ii ++, pbmt += nr_col_bmp)
			{
				// Prendo il valore di grigio parte alta se colonna pari, altrimenti parte bassa
				if( HalfByteHigh )
					HighValue = *pbmt >> 4;
				else
					HighValue = *pbmt & 0x0f;

				if( HighValue > LIMITE_16_GRIGI )
					ValoriOk ++;
			}

			//Incremento il counter nr. colonne nere
			if( ValoriOk <= NR_VALORI_OK )
			{
				NrColonneNere ++;
				ValoriOk = 0;
			}
			else
				ContinueClear = FALSE;

			// Reinizializzo il pointer ad inizio bitmap
			pbmt = pbm + (nr_col_bmp - 1) - (NrColonneNere / 2);

			HalfByteHigh = !HalfByteHigh;

		} while( ContinueClear && (NrColonneNere < (nr_col_bmp * 2)) );

		if( NrColonneNere && (NrColonneNere < (nr_col_bmp * 2)) )
		{
			DiffWidth = Bitmap->biWidth;

			// Arrotondo il numero di colonne nere ad un nr. pari
			if( NrColonneNere % 2 )
				NrColonneNere --;

			// Aggiorno l'header della bitmap
			Bitmap->biWidth -= NrColonneNere;

			RealWidth = Bitmap->biWidth;

			// Calcolo la larghezza reale della bitmap
			if( CondDib = DiffWidth % 8 )
				DiffWidth += (8 - CondDib);

			if( CondDib = RealWidth % 8 )
				RealWidth += (8 - CondDib);

			DiffWidth -= RealWidth;

			Bitmap->biSizeImage = (Bitmap->biHeight * RealWidth) / 2;

			// Arrotondo al byte
			RealWidth /= 2;
			DiffWidth /= 2;

			//Controllo se shiftare la bitmap
			if( NrColonneNere )
			{
				pbm += RealWidth;
				pbmt = pbm;

				// Sposto la bitmap
				for( ii = 1; ii < (unsigned long)Bitmap->biHeight; ii ++)
				{
					pbmt += DiffWidth;
					for( jj = 0; jj < (unsigned long)RealWidth; jj ++)
						*(pbm++) = *(pbmt++);
				}
			}
		}

//	SalvaDIBBitmap(Bitmap, "\\Progetti\\bmp\\front_16.bmp");

	}
	else
	{
		// Bitmap a 256 grigi

//	SalvaDIBBitmap(Bitmap, "\\Progetti\\bmp\\front2.bmp");

		// Inizio dati bitmap
		pbm = (unsigned char *)Bitmap + sizeof(BITMAPINFOHEADER) + (256 * sizeof(RGBQUAD));
		if(CondDib = (Bitmap->biWidth % 4) )
			nr_col_bmp = (Bitmap->biWidth + 4 - CondDib);
		else
			nr_col_bmp = Bitmap->biWidth;
		nr_row_bmp = Bitmap->biHeight;
		ContinueClear = TRUE;
		ValoriOk = 0;
		NrColonneNere = 0;

		// Tolgo le colonne nere davanti
		pbmt = pbm;
		do
		{
			for( ii = 0; ii < (unsigned long)nr_row_bmp; ii ++, pbmt += nr_col_bmp)
				if( *pbmt > LIMITE_256_GRIGI )
					ValoriOk ++;

			//Controllo se eliminare la colonna
			if( ValoriOk <= NR_VALORI_OK )
			{
				NrColonneNere ++;
				ValoriOk = 0;
			}
			else
				ContinueClear = FALSE;

			// Reinizializzo il pointer ad inizio bitmap
			pbmt = pbm + NrColonneNere;

		} while( ContinueClear && (NrColonneNere < nr_col_bmp) );


		//Controllo se shiftare la bitmap
		if( NrColonneNere && (NrColonneNere < nr_col_bmp) )
		{
			// Inizializzo i pointer
			pbmt = pbmtm = pbm;
			NrByte = nr_col_bmp;

			// Calcolo il NUOVO numero di colonne e lo salvo
			nr_col_bmp -= NrColonneNere;
			Bitmap->biWidth = nr_col_bmp;

			if( CondDib = (nr_col_bmp % 4) )
				nr_col_bmp = (nr_col_bmp + 4 - CondDib);

			// Sposto la bitmap
			for( ii = 0; ii < (unsigned long)Bitmap->biHeight; ii ++)
			{
				pbmt += NrColonneNere;
				for( jj = NrColonneNere; jj < (unsigned long)NrByte; jj ++)
					*(pbmtm++) = *(pbmt++);

				// Aggiungo il nero
				for( jj = 0; jj < (unsigned long)(nr_col_bmp - Bitmap->biWidth); jj ++)
					*(pbmtm++) = 0x00;	//NERO;
			}

			// Aggiorno il nr. di bytes della bitmap
			Bitmap->biSizeImage = Bitmap->biHeight * nr_col_bmp;
		}

//	SalvaDIBBitmap(Bitmap, "\\Progetti\\bmp\\front3.bmp");


		// Inizio dati bitmap
		pbm = (unsigned char *)Bitmap + sizeof(BITMAPINFOHEADER) + (256 * sizeof(RGBQUAD));
		if(CondDib = (Bitmap->biWidth % 4) )
			nr_col_bmp = (Bitmap->biWidth + 4 - CondDib);
		else
			nr_col_bmp = Bitmap->biWidth;
		nr_row_bmp = Bitmap->biHeight;
		pbmt = pbm + nr_col_bmp - 1;
		ContinueClear = TRUE;
		ValoriOk = 0;
		NrColonneNere = 0;

		// Tolgo le colonne nere dietro
		do
		{
			for( ii = 0; ii < (unsigned long)nr_row_bmp; ii ++, pbmt += nr_col_bmp)
				if( *pbmt > LIMITE_256_GRIGI )
					ValoriOk ++;

			//Incremento il counter nr. colonne nere
			if( ValoriOk <= NR_VALORI_OK )
			{
				NrColonneNere ++;
				ValoriOk = 0;
			}
			else
				ContinueClear = FALSE;

			// Reinizializzo il pointer ad inizio bitmap
			pbmt = pbm + (nr_col_bmp - 1) - NrColonneNere;

		} while( ContinueClear && (NrColonneNere < nr_col_bmp) );

		if( NrColonneNere && (NrColonneNere < nr_col_bmp) )
		{
			DiffWidth = Bitmap->biWidth;

			// Aggiorno l'header della bitmap
			Bitmap->biWidth -= NrColonneNere;

			RealWidth = Bitmap->biWidth;

			// Calcolo la larghezza reale della bitmap
			if( CondDib = DiffWidth % 4 )
				DiffWidth += (4 - CondDib);

			if( CondDib = RealWidth % 4 )
				RealWidth += (4 - CondDib);

			DiffWidth -= RealWidth;

			Bitmap->biSizeImage = Bitmap->biHeight * RealWidth;


			//Controllo se shiftare la bitmap
			if( NrColonneNere )
			{
				pbm += RealWidth;
				pbmt = pbm;

				// Sposto la bitmap
				for( ii = 1; ii < (unsigned long)Bitmap->biHeight; ii ++)
				{
					pbmt += DiffWidth;
					for( jj = 0; jj < (unsigned long)RealWidth; jj ++)
						*(pbm++) = *(pbmt++);
				}
			}
		}

//	SalvaDIBBitmap(Bitmap, "\\Progetti\\bmp\\front_256.bmp");

	} // Fine else

} // ClearFrontAndRearBlack


int Ls515Class::Periferica::ViewE13BSignal(int fView)
{

	char BufCodelineHW[256];
	unsigned long len_codeline;
	long llDati;
	unsigned char pDati[32*1024];
	//int NrDoc;
	short NrChar;
	short NrCampioni;
	short index;
	short Connessione;

	 llDati = 32*1024;
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	 //Reply = LS515_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);

	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
		// Setto il diagnostic mode
	//	Reply = LS515_SetDiagnosticMode((HWND)hDlg, TRUE);
		if (Reply == LS_OKAY) 
		{
			// Chiamo la funzione passando l'ultima configurazione settata
			///*Reply = LS515_DocHandle((HWND)hDlg,
			//				SUSPENSIVE_MODE,
			//				NO_FRONT_STAMP,
			//				NO_PRINT_VALIDATE,
			//				READ_CODELINE_MICR,
			//				SIDE_NONE_IMAGE,
			//				SCAN_MODE_256GR200,
			//				AUTOFEED,
			//				SORTER_BAY1,
			//				WAIT_YES,
			//				NO_BEEP,
			//				0,
			//				0);*/
			Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								READ_CODELINE_MICR,
								SIDE_FRONT_IMAGE,
								SCAN_MODE_256GR200,
								AUTOFEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);
			if (Reply == LS_OKAY) 
			{
				// Leggo la codeline
				len_codeline = 256;
				Reply = LSReadCodeline(Connessione,(HWND)hDlg,
										BufCodelineHW,
										(short *)&len_codeline,NULL,
										NULL,NULL,
										0);
				
				Reply = LSReadE13BSignal(Connessione,(HWND)hDlg, pDati, (long *)&llDati);
				if (Reply == LS_OKAY)
				{
					// salvo nella variabile pubblica i dati 


					if (fView)
					{
						NrChar = 0;
						while( (pDati[NrChar]) != E13B_FINE_LLC )
							NrChar ++;
						NrCampioni =(((pDati[NrChar + 2])<<8) + pDati[NrChar + 1]);
						index = NrChar + 1 + 2;		// Vado ad inizio dati segnale
					}
					else
					{
						NrCampioni	= (short)llDati;
						index = 0;
					}
					//this->E13BSignal   = new Byte[NrCampioni];
					for ( long ii = 0 ; ii<NrCampioni;ii++)
					{
						//this->E13BSignal[ii] = pDati[ii+index];
						E13BSignal[ii] = pDati[ii+index];
					}
				}
				// Tolgo il diagnostic mode
			//	LS515_SetDiagnosticMode((HWND)hDlg, FALSE);

				LSDisconnect(Connessione,(HWND)hDlg);
			}


		}
	}

	return Reply ;
}


int Ls515Class::Periferica::DisegnaEstrectLenBar()
{
	register int  xx;
	short ii, Save_ii;
	BOOL fFine;
	short NrLenghtBar = 7;	// Nr. raggruppamento barre
	short NrCampioni;
	short NrChar, NrValori;
	short NrBarre;

	short Save_pdct;	// Ptr. temp a caratteri




//	FILE *fh;
//	char *ptmp;
//	char FileOut[_MAX_FNAME];
	short index,index2;
//	this->ViewSignal = new Byte[10000];


	NrChar = 0;
	while( (E13BSignal[NrChar]) != E13B_FINE_LLC )
		NrChar ++;
	NrCampioni =(((E13BSignal[NrChar + 2])<<8) + E13BSignal[NrChar + 1]);
	index = NrChar + 1 + 2;		// Vado ad inizio dati segnale
	index = index + (DIM_DESCR_CHAR * NrChar);	// Vado ad inizio dati segnale

	// Da lasciare o da togliere ? Bah !!!   Solo il destino lo dir� !
	NrCampioni -= (DIM_DESCR_CHAR * NrChar);

	// Cerco l'ultimo valore valido
	index2 = index + NrCampioni;
	while( E13BSignal[index2-1] < SOGLIA_INIZIO_NERO )
	{
		index2 --;
		NrCampioni --;
	}
	// Ne tengo uno dei non validi come tappo
	NrCampioni ++;

	// Elimino tre barre finali
	for( ii = 0; ii < 3; ii++)
	{
		// Elimino la prima mezza barra, un'eventuale parte nera ...
		while( E13BSignal[index2-1] < SOGLIA_INIZIO_NERO )
		{
			index2 --;
			NrCampioni --;
		}

		// ... e la parte bianca, cos� parto sempre da un nero
		while( E13BSignal[index2-1] >= SOGLIA_INIZIO_NERO )
		{
			index2 --;
			NrCampioni --;
		}
	}

	// Elimino tre barre iniziali
	for( ii = 0; ii < 3; ii++)
	{
		// Elimino la prima mezza barra, un'eventuale parte nera ...
		while( E13BSignal[index-1] < SOGLIA_INIZIO_NERO )
		{
			index ++;
			NrCampioni --;
		}

		// ... e la parte bianca, cos� parto sempre da un nero
		while( E13BSignal[index-1] >= SOGLIA_INIZIO_NERO )
		{
			index ++;
			NrCampioni --;
		}
	}

	// Conto i valori di una oscillazione

	fFine = FALSE;
	NrValori = 0;
	NrBarre = 1;
	llMax = 0;
	llMin = NrLenghtBar * 0xff;	// setto il massimo
	llMedia = 0;
	xx = 0;
	index2 = index;
	for( ii = 1; ii <= NrCampioni; ii ++)
	{
		//Sono arrivato alla fine di una barra ?
		if( (fFine == TRUE ) && (E13BSignal[index-1] < SOGLIA_INIZIO_NERO) && (E13BSignal[index] < SOGLIA_INIZIO_NERO))
		{
			if( NrBarre == 1 )
			{
				Save_pdct = index;
				Save_ii = ii;
			}

			if( NrBarre < NrLenghtBar )
				NrBarre ++;
			else
			{
				ViewSignal[xx] = (unsigned char)NrValori;
				xx ++;

				// Se il valore esce dal grafico ... lo forzo al valore massimo
				if( NrValori > (HEIGHT_BAR * NrLenghtBar) )
					NrValori = (HEIGHT_BAR * NrLenghtBar) - 1;
				
				// Salvo il minimo ed il massimo
				if( NrValori < llMin )
					llMin = NrValori;
				if( NrValori > llMax )
					llMax = NrValori;

				// Aggiungo il valore alla media
				llMedia += NrValori;

				// Riazzero le variabili
				NrValori = 0;
				NrBarre = 1;
				index = Save_pdct;
				ii = Save_ii;
			}

			fFine = FALSE;
		}

		// Il nero lo somma per FALSE, il bianco per TRUE
		if( (fFine == FALSE) && (E13BSignal[index-1] >= SOGLIA_INIZIO_NERO) && (E13BSignal[index] >= SOGLIA_INIZIO_NERO) )
			fFine = TRUE;

		// Sposto il puntatore
		index ++;
		NrValori ++;
	}


	// Calcolo media
	llMedia = llMedia / xx;

	// Calcolo deviazione standard
	index = index2;	// Vado ad inizio dati
	DevStd = 0;
	for( ii = 1; ii <= NrCampioni; ii ++)
	{
		//Sono arrivato alla fine di una barra ?
		if( (fFine == TRUE ) && (E13BSignal[index-1] < SOGLIA_INIZIO_NERO) )
		{
			if( NrBarre == 1 )
			{
				Save_pdct = index;
				Save_ii = ii;
			}

			if( NrBarre < NrLenghtBar )
				NrBarre ++;
			else
			{
				DevStd += (float)(Math::Pow((double)(NrValori - llMedia), 2));

				// Riazzero le variabili
				NrValori = 0;
				NrBarre = 1;
				index = Save_pdct;
				ii = Save_ii;
			}

			fFine = FALSE;
		}

		// Il nero lo somma per FALSE, il bianco per TRUE
		if( (fFine == FALSE) && (index >= SOGLIA_INIZIO_NERO) )
			fFine = TRUE;

		// Sposto il puntatore
		index ++;
		NrValori ++;
	}

	DevStd /= xx;
	DevStd = (float)Math::Sqrt(DevStd);

return 1;
}




int Ls515Class::Periferica::Docutest04()
{
	ReturnC->FunzioneChiamante = "Test Doppia sfogliatura";
	short Connessione;

	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	//Reply = LS515_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
		//Reply = LS515_DoubleLeafingSensibility((HWND)hDlg,DOUBLE_LEAFING_DEFAULT);
		//Reply  = LS515_ConfigDoubleLeafing((HWND)hDlg,DOUBLE_LEAFING_ERROR,DOUBLE_LEAFING_DEFAULT,0);
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_ERROR,50,80,225);
		///*Reply = LS515_DocHandle((HWND)hDlg,
		//						SUSPENSIVE_MODE,
		//						NO_FRONT_STAMP,
		//						NO_PRINT_VALIDATE,
		//						NO_READ_CODELINE,
		//						SIDE_NONE_IMAGE,
		//						SCAN_MODE_256GR200,
		//						AUTOFEED,
		//						SORTER_BAY1,
		//						WAIT_YES,
		//						NO_BEEP,0,0);*/
		Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								SIDE_NONE_IMAGE,
								SCAN_MODE_256GR200,
								AUTOFEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);
		
		//Reply = LS515_DoubleLeafingSensibility((HWND)hDlg,DOUBLE_LEAFING_DISABLE);
		//Reply  = LS515_ConfigDoubleLeafing((HWND)hDlg,DOUBLE_LEAFING_ERROR,DOUBLE_LEAFING_DISABLE,0);
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);
		LSDisconnect(Connessione,(HWND)hDlg);
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}

	ReturnC->ReturnCode = Reply;
	

	return Reply;	//ret
}

int Ls515Class::Periferica::Reset(short TypeReet)
{
	short Connessione;
	
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	if( ( Reply  == LS_TRY_TO_RESET ) || (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
		Reply = LSReset(Connessione,(HWND)0,(char)TypeReet);

		LSDisconnect(Connessione,(HWND)hDlg);

	}

	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Reset";

	return Reply;	
}




int Ls515Class::Periferica::DoTimeSampleMICRCalibration(short TypeSpeed)
{
	BOOL	nRet;
	short Connessione;	
	float	DiffPercentuale;
	unsigned short	CurrValue, NewValue;
//	MSG		msg;
	char  Stamp;
	short ScanMode;

	short	nrC, nrS;
//	short	Width, Diff;

	char	BufCodelineHW[CODE_LINE_LENGTH];
	short	len_codeline;


	unsigned char buffDati[NR_BYTE_BUFFER_E13B * 2];		// Dati da chiedere
	long   llDati; 					// Dati da chiedere
//	char	FileOut[256];
	unsigned char *pDati;
	float	SampleValueMin, SampleValueMax;
	long valMedio;
	char IdentStr[256],Version[256];
	char Model[256];
	HANDLE	pFront, pFrontUV;


	// Setto i valori iniziali
	nRet = TRUE;
	Percentile = 0;
	Stamp = NO_STAMP;

	if( TypeSpeed == SPEED_COLOR_200_DPI )
		ScanMode = SCAN_MODE_COLOR_200;
	else if( TypeSpeed == SPEED_COLOR_300_DPI )
		ScanMode = SCAN_MODE_COLOR_300;
	else if( TypeSpeed == SPEED_GRAY_UV_200_DPI )
		ScanMode = SCAN_MODE_256GR200_AND_UV;
	else if( TypeSpeed == SPEED_GRAY_UV_300_DPI )
		ScanMode = SCAN_MODE_256GR300_AND_UV;
	else if( TypeSpeed == SPEED_RETAINED )
		ScanMode = SCAN_MODE_COLOR_300;		// Il retained ha la STESSA velocit� del colore 300
	else // if( TypeSpeed == SPEED_800_mm )
		ScanMode = SCAN_MODE_256GR200;

	//-----------Open--------------------------------------------
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	TypeLS = 0;
	//-----------Identify--------------------------------
	Reply = LSIdentify(Connessione, (HWND)hDlg, IdentStr, Model, Version, NULL, NULL, NULL, NULL);
	if( Reply == LS_OKAY)
	{
		//Identifico la periferica se LS515S o LS515D
		TypeLS = GetTypeLS(IdentStr, Model, Version);
	}

	//Reply = LS515_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if (Reply == LS_OKAY || Reply == LS_ALREADY_OPEN)
	{
		// Se LS515 SE aumento il TypeSpeed di 3
		if( TypeLS == TYPE_LS515_SE )
		{
			// Devo abbassare la velocita' ?
			if( TypeSpeed == SPEED_550_mm )
				LSSetUnitSpeed(Connessione, (HWND)hDlg, SPEED_NORMAL);
			else
				LSSetUnitSpeed(Connessione, (HWND)hDlg, SPEED_DEFAULT);

			if( TypeSpeed == SPEED_800_mm )
				TypeSpeed += 0x10;
			else
				TypeSpeed += 3;
		}

		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);
		LSDisableWaitDocument(Connessione, (HWND)hDlg, FALSE);
		if (Reply == LS_OKAY)
		{
			CurrValue = 0;
			Reply = LSReadTimeSampleMICR(Connessione, (HWND)hDlg, (char)TypeSpeed, &CurrValue, sizeof(CurrValue));
			if (Reply == LS_OKAY)
			{
				Reply = LSDocHandle(Connessione,(HWND)hDlg,
								Stamp,
								NO_PRINT_VALIDATE,
								READ_CODELINE_MICR,
								SIDE_FRONT_IMAGE,
								ScanMode,
								AUTOFEED,
								SORTER_BAY2,
								WAIT_YES,
								NO_BEEP,
								NULL,
								0,
								0);

				if( Reply == LS_OKAY )
				{

						// Leggo il segnale E13B
					llDati = NR_BYTE_BUFFER_E13B;
					Reply = LSReadE13BSignal(Connessione, (HWND)hDlg, buffDati, (long *)&llDati);
					if( Reply != LS_OKAY )
					{
						CheckReply((HWND)hDlg, Reply, "LS5xx_ReadE13BSignal");
						LSDisconnect(Connessione, (HWND)hDlg);
						return nRet;
					}
					// Leggo la codeline Per liberare i buffer periferica
					len_codeline = CODE_LINE_LENGTH;
					Reply = LSReadCodeline(Connessione,(HWND)hDlg,
												   BufCodelineHW,
												   &len_codeline,
												   NULL,NULL,
												   NULL,
												   0);
						// Leggo l'immagine per cancellare i buffer nella periferica
					pFront = pFrontUV = 0;
					Reply = LSReadImage(Connessione,
											(HWND)hDlg,
											NO_CLEAR_BLACK,
											SIDE_FRONT_IMAGE,
											0,
											0,
											&pFront,
											NULL, //&pBack,
											&pFrontUV, NULL);
					if( pFront )
						LSFreeImage( (HWND)hDlg, &pFront );

					if( pFrontUV )
						LSFreeImage( (HWND)hDlg, &pFrontUV );
					
			//		llDati = NR_BYTE_BUFFER_E13B;
			//		Reply = LSReadE13BSignal(Connessione,(HWND)hDlg, buffDati, (long *)&llDati);
					if( Reply == LS_OKAY )
					{
					//	CurrValue = 0;
						// Leggo il valore settato nella periferica
					//	Reply = LSReadTimeSampleMICR(Connessione,(HWND)hDlg, TypeSpeed, &CurrValue, 2);
						if( Reply == LS_OKAY )
						{
							// Conto i bytes larghezza char
							//this->E13BSignal = new Byte[llDati];
							for ( long ii = 0 ; ii<llDati;ii++)
							{
								this->E13BSignal[ii] = buffDati[ii];
							}
							pDati = buffDati;
						
						
							// Conto i bytes larghezza char
							nrC = 0;
							while( *(pDati + nrC) != E13B_FINE_LLC )
								nrC ++;
						

							// Controllo se ho trovato il fine lunghezza caratteri
							if( nrC != llDati )
							{
								// Estraggo il numero di segnali
								nrS = *(short *)(pDati + nrC + 1);		// 1 per il CR
								nrS -= 2;	// Il valore comprende anche i 2 bytes di nr. campioni segnale
								
								DisegnaEstrectLenBar();
							}
								// Setto il valore min e max
							
							SampleValueMin = SAMPLE_VALUE_MIN;
							SampleValueMax = SAMPLE_VALUE_MAX;
										
						
							// Controllo differenza minimo massimo
							// 
							if( TypeLS == TYPE_LS515_6 )
							{
								// Setto il valore min e max
								SampleValueMin = SAMPLE_VALUE_MIN_LS515;
								SampleValueMax = SAMPLE_VALUE_MAX_LS515;

								// Aumentato tolleranza per problemi meccanici
								if( (llMax - llMin) > 14 )
								{
									//MessageBox(hDlg, "Bad assembly !", TITLE_ERROR, MB_ICONERROR | MB_OK);
									//LSDisconnect(hLS, hDlg);
									return LS_CALIBRATION_FAILED;
								}
							}
							else if( TypeLS == TYPE_LS515_SE )
							{
								// Setto il valore min e max
								SampleValueMin = SAMPLE_VALUE_MIN_LS515_SE;
								SampleValueMax = SAMPLE_VALUE_MAX_LS515_SE;

								if( (llMax - llMin) > 11 )
								{
									//MessageBox(hDlg, "Bad assembly !", TITLE_ERROR, MB_ICONERROR | MB_OK);
									//LSDisconnect(hLS, hDlg);
									return LS_CALIBRATION_FAILED;
								}
							}
							//else
							//{	
								// Controllo differenza minimo massimo
								if( (llMax - llMin) < 11 )
								{
									// Controllo il valore massimo
									if( llMax > SampleValueMax )
									{
										// Calcolo il valore in percentuale da sommare a quello presente nella perifeica
										DiffPercentuale = (llMax * CurrValue) / SampleValueMax;
										if( (DiffPercentuale - (long)DiffPercentuale) >= 0.5 )
											NewValue = (unsigned short)(DiffPercentuale + 1);
										else
											NewValue = (unsigned short)DiffPercentuale;

												
										NrLettureOk = 0;
										Reply = LS_RETRY;	// Devo riprovare la calibrazione
									}
									else if( llMin < SampleValueMin )
									{
										// Calcolo il valore in percentuale da sommare a quello presente nella perifeica
										DiffPercentuale = (llMin * CurrValue) / SampleValueMin;
										if( (DiffPercentuale - (long)DiffPercentuale) >= 0.5 )
											NewValue = (unsigned short)(DiffPercentuale + 1);
										else
											NewValue = (unsigned short)DiffPercentuale;

											NrLettureOk = 0;
											Reply  = LS_RETRY;	// Devo riprovare la calibrazione
									}
									// Se i valori stanno nei Min e Max controllo se la media � vicino a 112
									else if( (llMin >= SampleValueMin) && (llMax <= SampleValueMax) )
									{
										valMedio = (llMax + llMin) / 2;

										if( TypeLS == TYPE_LS515_SE )
										{
											// Sommo dei valori a seconda della distanza
											if( (valMedio - SAMPLE_VALUE_MEDIO) > 2 )
												NewValue = CurrValue + 3;
											else if( (valMedio - SAMPLE_VALUE_MEDIO) > 1 )
												NewValue = CurrValue + 2;
											else if( (valMedio - SAMPLE_VALUE_MEDIO) < -2 )
												NewValue = CurrValue - 3;
											else if( (valMedio - SAMPLE_VALUE_MEDIO) < -1 )
												NewValue = CurrValue - 2;
											else if( (valMedio - SAMPLE_VALUE_MEDIO) > 0 )
												NewValue = CurrValue + 1;
											else if( (valMedio - SAMPLE_VALUE_MEDIO) < 0 )
												NewValue = CurrValue - 1;
											else
												NewValue = CurrValue;
										}
										else
										{
											valMedio = (llMax + llMin) / 2;

											// Sommo dei valori a seconda della distanza
											if( (valMedio - SAMPLE_VALUE_MEDIO) > 2 )
												NewValue = CurrValue + 3;
											else if( (valMedio - SAMPLE_VALUE_MEDIO) > 1 )
												NewValue = CurrValue + 2;
											else if( (valMedio - SAMPLE_VALUE_MEDIO) < -2 )
												NewValue = CurrValue - 3;
											else if( (valMedio - SAMPLE_VALUE_MEDIO) < -1 )
												NewValue = CurrValue - 2;
											else if( (valMedio - SAMPLE_VALUE_MEDIO) > 0 )
												NewValue = CurrValue + 1;
											else if( (valMedio - SAMPLE_VALUE_MEDIO) < 0 )
												NewValue = CurrValue - 1;
											else
												NewValue = CurrValue;
										
											NrLettureOk ++;
											Reply = LS_RETRY;	// Devo riprovare la calibrazione
										}
									}
										
									// Se nRet = FALSE ho variato i valori, perci� vado a scrivere quelli nuovi
									if( Reply == LS_RETRY )
									{
										// Controllo che non sia inferiore o superiore a default +o- 10%
										if( TypeLS == TYPE_LS520 )
										{
											if( TypeSpeed == SPEED_COLOR_200_DPI )
											{
												if( NewValue < (0x797 * 0.90) )
													NewValue = (unsigned short)(0x797 * 0.90);	// forzo il minimo

												if( NewValue > (0x797 * 1.10) )
													NewValue = (unsigned short)(0x797 * 1.10);	// forzo il massimo
											}
											else if( TypeSpeed == SPEED_COLOR_300_DPI || TypeSpeed == SPEED_RETAINED )
											{
												if( NewValue < (0xb63 * 0.90) )
													NewValue = (unsigned short)(0xb63 * 0.90);	// forzo il minimo

												if( NewValue > (0xb63 * 1.10) )
													NewValue = (unsigned short)(0xb63 * 1.10);	// forzo il massimo
											}
											else if( TypeSpeed == SPEED_GRAY_UV_200_DPI )
											{
												if( NewValue < (0x9c3 * 0.90) )
													NewValue = (unsigned short)(0x9c3 * 0.90);	// forzo il minimo

												if( NewValue > (0x9c3 * 1.10) )
													NewValue = (unsigned short)(0x9c3 * 1.10);	// forzo il massimo
											}
											else if( TypeSpeed == SPEED_GRAY_UV_300_DPI )
											{
												if( NewValue < (0xe62 * 0.90) )
													NewValue = (unsigned short)(0xe62 * 0.90);	// forzo il minimo

												if( NewValue > (0xe62 * 1.10) )
													NewValue = (unsigned short)(0xe62 * 1.10);	// forzo il massimo
											}
											else
											{
												if( NewValue < (0x520 * 0.90) )
													NewValue = (unsigned short)(0x520 * 0.90);	// forzo il minimo

												if( NewValue > (0x520 * 1.10) )
													NewValue = (unsigned short)(0x520 * 1.10);	// forzo il massimo
											}
											}
											else if( TypeLS == TYPE_LS515_SE )
											{
												// Li controlla la periferica
											}
											else
											{
												if( TypeSpeed == SPEED_COLOR_200_DPI )
												{
													if( NewValue < (0x8c3 * 0.90) )
														NewValue = 0x8c3;	// forzo il default

													if( NewValue > (0x8c3 * 1.10) )
														NewValue = 0x8c3;	// forzo il default
												}
												else
												{
													if( NewValue < (0x89f * 0.90) )
														NewValue = 0x89f;	// forzo il default

													if( NewValue > (0x89f * 1.10) )
														NewValue = 0x89f;	// forzo il default
												}
											}
														
											//non faccio pi� i test, li controlla la periferica
											Reply = LSSetTimeSampleMICR(Connessione,(HWND)hDlg, (char)TypeSpeed, NewValue);
											if( Reply == LS_OKAY )
											{
												// ho cambiato il valore del trimmer , devo riprovare , quindi
												// metto reply a RETRY
												Reply = LS_RETRY;
											}		
											else
											{
												CheckReply((HWND)hDlg, Reply, "LSSetTimeSampleMICR");
												//Reply = LS_OKAY;								 
												NrLettureOk = 0;
											}
										}
										else
										{
											Reply = LS_RETRY;
											NrLettureOk ++;
										}
									}
									else
									{
										//MessageBox(hDlg, "Bad assembly !", TITLE_ERROR, MB_ICONERROR | MB_OK);						
										Reply = LS_CALIBRATION_FAILED;
										NrLettureOk = 0;
									}
								//}			
								/*else
								{
									CheckReply((HWND)hDlg, Reply, "SCSILS_ReadTimeSampleMICR");
									NrLettureOk = 0;
								}*/
							}
							else
							{
								CheckReply((HWND)hDlg, Reply, "LSReadE13BSignal");
								NrLettureOk = 0;
							}
						}
						else
						{
							CheckReply((HWND)hDlg, Reply, "LSDocHandle");
							NrLettureOk = 0;
						}
					}
					else
					{
						// errore disable wait document
						NrLettureOk = 0;
					}
				}
				else
				{
					// errore set double leafing
					NrLettureOk = 0;
				}
			}
			else
			{
				CheckReply((HWND)hDlg, Reply, "SCSILS_ReadTimeSampleMICR");
				NrLettureOk = 0;
			}
		}
		else
		{
			if( CheckReply((HWND)hDlg, Reply, "LSOpen"))
			{		    
			}
			NrLettureOk = 0;
		}

	// Reimposto la velocita' di default
	LSSetUnitSpeed(Connessione, (HWND)hDlg, SPEED_DEFAULT);

    LSDisconnect(Connessione,(HWND)hDlg);

	return Reply;
} // DoTimeSampleMICRCalibration





int Ls515Class::Periferica::EraseHistory()
{
//	S_HISTORY_LS5xx History;
	short Connessione;

	ReturnC->FunzioneChiamante = "Cancellazione Storico";
TipoPeriferica = 502;
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	//Reply = LS515_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	//if( Reply == LS_OKAY )
	{
		//Reply = LSHistoryCommand(Connessione,(HWND)hDlg, CMD_READ_HISTORY, NULL);

		/*S_Storico->Documenti_Trattati = History.num_doc_handled;
		S_Storico->Documenti_Trattenuti = History.doc_retain_scan;
		S_Storico->Errori_CMC7 = History.doc_cmc7_err;
		S_Storico->Errori_E13B = History.doc_e13b_err;
		S_Storico->Jam_Card = 0;
		S_Storico->Jam_Micr = History.bourrage_micr;
		S_Storico->Jam_Scanner = History.bourrage_film;
		S_Storico->Num_Accensioni = History.nr_power_on;
		S_Storico->Doc_Timbrati = 0;
		S_Storico->TempoAccensione = History.time_peripheral_on;*/
	

		Reply = LSHistoryCommand(Connessione,(HWND)hDlg, CMD_ERASE_HISTORY, NULL);
		if( Reply != LS_OKAY )
		{
			ReturnC->FunzioneChiamante = "LSHistoryCommand";
		}

		LSDisconnect(Connessione,(HWND)hDlg);
	}
	//else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}


	ReturnC->ReturnCode = Reply;
	return Reply;
}




int Ls515Class::Periferica::AccendiLuce(short Luce)
{
	short ret=0;
	short Connessione;

	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	//Reply = LS515_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( (Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN) )
	{
		Reply = LSSetScannerLight(Connessione,(HWND)hDlg, Luce);	
		LSDisconnect(Connessione,(HWND)hDlg);
	}
	else
	{
		CheckReply(0, Reply, "LSOpen");
	}
	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Test Luci";

	return ret;
}


int Ls515Class::Periferica::MotoreFeeder(short Acceso)
{
	short ret=0;
	short Connessione;
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	//Reply = LS515_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( (Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN) )
	{
		Reply = LSMoveMotorFeeder(Connessione,(HWND)hDlg, Acceso);	
		LSDisconnect(Connessione,(HWND)hDlg);
	}
	else
	{
		CheckReply(0, Reply, "LS515_Open");
	}
	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Test Motore Feeder";

	return ret;
}
//
int Ls515Class::Periferica::sensorFeeder()
{
	short ret=0;
	unsigned short  aa;
	short Connessione;
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	//Reply = LS515_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( (Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN) )
	{
		Reply = LSReadFeederMotorAD(Connessione,(HWND)hDlg,&aa);	
		Sensore = aa;
		LSDisconnect(Connessione,(HWND)hDlg);
	}
	else
	{
		CheckReply(0, Reply, "LSOpen");
	}
	ReturnC->ReturnCode = Reply;
	ReturnC->FunzioneChiamante = "Test Motore Feeder";

	return ret;

}

int Ls515Class::Periferica::TestCinghia()
{
	unsigned char pDati[50*1024];
	long lunDati= 50*1024;
	int ii, max, min;
	float media;
	int somma;
	int MinScarto,MaxScarto;
	short Connessione;
	
	MinScarto = 110;
	MaxScarto = 145;
	ReturnC->FunzioneChiamante = " Test cinghia";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	//Reply = LS515_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
		{//----------- Disabilito la doppia sfogliatura -----------
		//Reply = LS515_DoubleLeafingSensibility((HWND)hDlg, DOUBLE_LEAFING_DISABLE);
		//Reply  = LS515_ConfigDoubleLeafing((HWND)hDlg,DOUBLE_LEAFING_ERROR,DOUBLE_LEAFING_DISABLE,0);
		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,80,225);	
		Reply = LSTestCinghia(Connessione,(HWND)hDlg,pDati,&lunDati);
		if(Reply == LS_OKAY)
		{
			max = 0;
			min = 255;
			media = 0;
			somma = 0;
			if (lunDati <= 400)
			{
				ReturnC->FunzioneChiamante = "LSTestCinghia";
				Reply = -7000;
			}
			else
			{
				for(ii = 10; ii < lunDati-400;ii++)
				{	
					
					if (pDati[ii] < min )
						min = pDati[ii]; 
					if (pDati[ii] > max )
						max = pDati[ii];
					somma += pDati[ii];
				}
				media  = (float)(somma / (lunDati-410));	
				if((min < MinScarto) || (max > MaxScarto))
				{
					ReturnC->FunzioneChiamante = "LSTestCinghia";
					Reply = -6000;
					
				}
				else
				{

				}
				ValMinCinghia = min;
				ValMaxCinghia = max;
			}
		}
		else
		{
			ReturnC->FunzioneChiamante = "LSTestCinghia";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}
	ReturnC->ReturnCode = Reply;
	
	LSDisconnect(Connessione,(HWND)hDlg);
	return Reply;
}


int Ls515Class::Periferica::LeggiValoriCIS()
{
	//char pColonne[5000];
	//short Connessione;
	//AD_LM98714 sScanner ;//= new AD_LM98714;

	//Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	//if ( Reply  == LS_TRY_TO_RESET )
	//{
	//	Reply = LSReset(Connessione,(HWND)0,RESET_FREE_PATH);
	//}
	////Reply = LS515_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	//if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	//{
	//	Reply = LS515_ScannerReadValue((HWND)hDlg, pColonne, &sScanner, 0);	
	//	strCis->Add_PWM_Blue_F = sScanner.Add_PWM_Blue_F;
	//	strCis->Add_PWM_Blue_R = sScanner.Add_PWM_Blue_R;
	//	strCis->Add_PWM_Green_F = sScanner.Add_PWM_Green_F;
	//	strCis->Add_PWM_Green_R = sScanner.Add_PWM_Green_R;
	//	strCis->Add_PWM_Red_F = sScanner.Add_PWM_Red_F;
	//	strCis->Add_PWM_Red_R = sScanner.Add_PWM_Red_R;
	//	
	//	
	//	strCis->Dato_Gain_F = sScanner.Dato_Gain_F;
	//	strCis->Dato_Gain_R = sScanner.Dato_Gain_R;
	//	strCis->Dato_Offset_F = sScanner.Dato_Offset_F;
	//	strCis->Dato_Offset_R = sScanner.Dato_Offset_R;
	//	strCis->Dato_PWM_Blue_F = sScanner.Dato_PWM_Blue_F;
	//	strCis->Dato_PWM_Blue_R = sScanner.Dato_PWM_Blue_R;
	//	strCis->Dato_PWM_Green_F = sScanner.Dato_PWM_Green_F;
	//	strCis->Dato_PWM_Green_R = sScanner.Dato_PWM_Green_R;
	//	strCis->Dato_PWM_Red_F = sScanner.Dato_PWM_Red_F;
	//	strCis->Dato_PWM_Red_R = sScanner.Dato_PWM_Red_R;
	//	


	//}
	//ReturnC->ReturnCode = Reply;
	//
	//LSDisconnect(Connessione,(HWND)hDlg);
	return 0;
}




//extern int APIENTRY LS515_MoveMotorFeeder(HWND, short);

//extern int APIENTRY LS515_ReadFeederMotorAD(HWND, unsigned short *);

int Ls515Class::Periferica::SetDiagnostic(int fbool)
{
	short Connessione;
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	//Reply = LS515_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
		//
		
		Reply = LSSetDiagnosticMode(Connessione,(HWND)hDlg,fbool);
	}
	LSDisconnect(Connessione,(HWND)hDlg);
	return Reply;
}


int Ls515Class::Periferica::SetVelocita(int fSpeed)
{

	short Connessione;

	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	//Reply = LS515_Open((HWND)hDlg, (HANDLE)hInst, SUSPENSIVE_MODE);
	if( (Reply == LS_OKAY) || (Reply == LS_ALREADY_OPEN) )
	{
		//
		Sleep(500);
		Reply = LSSetUnitSpeed(Connessione,(HWND)hDlg,fSpeed);
	}
	LSDisconnect(Connessione,(HWND)hDlg);
	return Reply;
}

int Ls515Class::Periferica::CheckReply(HWND hDlg,int Rep,char *s)
{
	return TRUE;
}


int Ls515Class::Periferica::EstraiDatiSpeedE13B(unsigned char *pd, long llDati,
						 short *NrPicchi, float *SommaValori, long *ValMin, short *NrPicchiMin)
{
	BOOL ret;
	short ii, Save_ii;
	short NrCampioni;
	short NrChar;
	unsigned char *pdct;

//	FILE *fh;


	// Conto i bytes larghezza char
	NrChar = 0;
	while( (*(pd + NrChar) != E13B_FINE_LLC) && (NrChar < llDati) )
		NrChar ++;

	if( NrChar != llDati )
	{
		NrCampioni = *(short *)(pd + NrChar + 1);

		pdct = (unsigned char *)(pd + NrChar + 1 + 2);		// Vado ad inizio dati segnale
		pdct = (unsigned char *)(((unsigned char *)pdct) + (DIM_DESCR_CHAR * NrChar));	// Vado ad inizio dati segnale

		// Tolgo i descrittori dal numero bytes
		NrCampioni -= (DIM_DESCR_CHAR * NrChar);


		// Cerco i picchi e calcolo il valore medio
		*NrPicchi = *NrPicchiMin = 0;
		*ValMin = 0xff;		// setto il massimo
		*SommaValori = 0;
		Save_ii = 0;
		// Elimino una parte iniziale e finale
		for( ii = 20; ii <= (NrCampioni - 20); ii ++)
		{
			// Controllo se � un picco,
			// per essere un picco deve avere 7 valori prima e dopo di valore inferiore
			if( (pdct[ii] <= VALORE_MAX_PICCO_MIN) &&
//			if( (pdct[ii]) && (pdct[ii] <= VALORE_MAX_PICCO_MIN) &&
				((pdct[ii] <= pdct[ii-1]) && (pdct[ii] < pdct[ii+1])) &&
				((pdct[ii] <= pdct[ii-2]) && (pdct[ii] < pdct[ii+2])) &&
				((pdct[ii] <= pdct[ii-3]) && (pdct[ii] < pdct[ii+3])) &&
				((pdct[ii] <= pdct[ii-4]) && (pdct[ii] < pdct[ii+4])) &&
				((pdct[ii] <= pdct[ii-5]) && (pdct[ii] < pdct[ii+5])) &&
				((pdct[ii] <= pdct[ii-6]) && (pdct[ii] < pdct[ii+6])) &&
				((pdct[ii] <= pdct[ii-7]) && (pdct[ii] < pdct[ii+7])) )
			{
				// Controllo se il picco � ad una distanza minima
				if( (ii - Save_ii) >= PICCO_DISTANZA_MINIMA )
				{
					// Media del picco
					*SommaValori += pdct[ii];

//					if( fh = fopen("Trace_Calibrazione_MICR.txt", "at") )
//					{
//						fprintf(fh, "%d ", pdct[ii]);
//						fclose( fh );
//					}

					// Salvo il valore minimo
					if( *ValMin == pdct[ii] )
						*NrPicchiMin = *NrPicchiMin + 1;

					if( *ValMin > pdct[ii] )
					{
						*ValMin = pdct[ii];
						*NrPicchiMin = 1;
					}

					*NrPicchi = *NrPicchi + 1;
					Save_ii = ii;
				}
			}
		}

		// Calcolo media
//		*llMedia = *llMedia / *NrPicchi;

		ret = TRUE;
	}
	else
	{
		// C'� un baco che qualche volta non ritorna i dati corretti,
		// setto tutto a zero e butto l'acquisizione !!!
		*NrPicchi = *NrPicchiMin = 0;
		*ValMin = 0;
		*SommaValori = 0;
		ret = FALSE;
	}


//	if( fh = fopen("Trace_Calibrazione_MICR.txt", "at") )
//	{
//		fprintf(fh, "\n ");
//		fclose( fh );
//	}

	return ret;
}  // EstraiDatiSpeedE13B



int GetTypeLS(char *Config_Ls, char *Model, char *Version)
{
	int TypeLS;

	TypeLS = TYPE_LS510S;	// Default
	
	//Identify the type of peripheral LS500 LS505, LS510, LS515 or LS520
	if( strncmp(Model, MODEL_LS520, strlen(MODEL_LS520)) == 0 )
	{
		TypeLS = TYPE_LS520;
	}
	else if( strncmp(Model, MODEL_LS515_6, strlen(MODEL_LS515_6)) == 0 )
	{
		TypeLS = TYPE_LS515_6;
	
		// Sul LS515 SE ci sono i foto da calibrare
		if( Version[0] >= '4' )
		{
			TypeLS = TYPE_LS515_SE;
		}
	}
	else if( strncmp(Model, MODEL_LS515, strlen(MODEL_LS515)) == 0 )
	{
		// Controllo il tipo di LS515
		TypeLS = TYPE_LS515S;
		if( (Config_Ls[2] & MASK_LS515D) == MASK_LS515D )
			TypeLS = TYPE_LS515D;
		if( (Config_Ls[2] & MASK_LS515C) == MASK_LS515C )
			TypeLS = TYPE_LS515_6;
	}
	else if( strncmp(Model, MODEL_LS510S, strlen(MODEL_LS510S)) == 0 )
		TypeLS = TYPE_LS510S;
	else if( strncmp(Model, MODEL_LS510D, strlen(MODEL_LS510D)) == 0 )
		TypeLS = TYPE_LS510D;
	else if( strncmp(Model, MODEL_LS505, strlen(MODEL_LS505)) == 0 )
		TypeLS = TYPE_LS505;
	else if( strncmp(Model, MODEL_LS500, strlen(MODEL_LS500)) == 0 )
	{
		// 5.33 identifica LS500 per Poste Svizzere
		if( strncmp(Version, "5.33", 4) == 0 )
			TypeLS = TYPE_LS510S;
		else
			TypeLS = TYPE_LS500;
	}
	
	return TypeLS;
} // GetTypeLS




short Ls515Class::Periferica::GetTypeLS( char *Config_Ls,char *Model,char *Version)
{

	int TypeLS;


	TypeLS = TYPE_LS510S;	// Default
	

	//Identify the type of peripheral LS500 LS505, LS510, LS515 or LS520
	if( strncmp(Model, MODEL_LS520, strlen(MODEL_LS520)) == 0 )
	{
		TypeLS = TYPE_LS520;
	}
	else if( strncmp(Model, MODEL_LS515_6, strlen(MODEL_LS515_6)) == 0 )
	{
		TypeLS = TYPE_LS515_6;

		// Sul LS515 SE ci sono i foto da calibrare
		if( Version[0] >= '4' )
		{
			TypeLS = TYPE_LS515_SE;
		}
	}
	else if( strncmp(Model, MODEL_LS515, strlen(MODEL_LS515)) == 0 )
	{
		// Controllo il tipo di LS515
		TypeLS = TYPE_LS515S;
		if( (Config_Ls[2] & MASK_LS515D) == MASK_LS515D )
			TypeLS = TYPE_LS515D;
		if( (Config_Ls[2] & MASK_LS515C) == MASK_LS515C )
			TypeLS = TYPE_LS515_6;
	}
	else if( strncmp(Model, MODEL_LS510S, strlen(MODEL_LS510S)) == 0 )
		TypeLS = TYPE_LS510S;
	else if( strncmp(Model, MODEL_LS510D, strlen(MODEL_LS510D)) == 0 )
		TypeLS = TYPE_LS510D;
	else if( strncmp(Model, MODEL_LS505, strlen(MODEL_LS505)) == 0 )
		TypeLS = TYPE_LS505;
	else if( strncmp(Model, MODEL_LS500, strlen(MODEL_LS500)) == 0 )
	{
		// 5.33 identifica LS500 per Poste Svizzere
		if( strncmp(Version, "5.33", 4) == 0 )
			TypeLS = TYPE_LS510S;
		else
			TypeLS = TYPE_LS500;
	}


	return TypeLS;


	
} // GetTypeLS





int Ls515Class::Periferica::CollaudoLSConnect(HWND hDlg,HANDLE hInst,short TipoPeriferica,short *Connessione)
{
	int Reply;

	// simula la connect della lsapi
	Reply = LSConnectDiagnostica((HWND)0,(HANDLE)hInst,TipoPeriferica,Connessione);
	if(( Reply == LS_OKAY )||( Reply == LS_TRY_TO_RESET )||( Reply == LS_ALREADY_OPEN ))
	{
		Reply = LSReset(*Connessione,(HWND)0,RESET_ERROR);

		if( Reply != LS_OKAY )
			Reply = LSReset(*Connessione,(HWND)0,RESET_FREE_PATH);
	}

	return Reply;
}



void Ls515Class::Periferica::ShowMICRSignal(float Percentile, short Trimmer)
{

	int conta ;
	list->Items->Add("Valore Letto: " + Percentile + " " + "Trimmer Value: "+ Trimmer );
	
	conta = list->Items->Count;
	list->SelectedIndex = conta-1;
	list->Refresh();
}

int Ls515Class::Periferica::LeggiValoriPeriferica()
{
	
	char ch = 0x1;
	char TypeSpeed;
	short VelVal = 0;
	unsigned short CurrValue = 0;
	
	unsigned char buffDati[1024];
	short Connessione;
	unsigned char buffscanner[3000];

	char IdentStr[64],Version[64],Date_Fw[64],LsName[64],SerialNumber[64],FeederVersion[64],
		DecodeExp[64],inkVer[64], SorterVersion[64],MotorVersion[64],datefpga[64];
	char BoardNr[8];
	

	int nrBytes;
	

	ReturnC->FunzioneChiamante = "LeggiValoriPeriferica";
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	if ( ( Reply  == LS_OKAY)  ||( Reply  == LS_ALREADY_OPEN ))
	{
	
		// leggo i valori del trimmer MICR

		
		TypeSpeed = TEST_SPEED_200_DPI; 
		
		Reply = LSReadTimeSampleMICR(Connessione,(HWND)hDlg, TypeSpeed+3, &CurrValue, sizeof(CurrValue));
		if( Reply  == LS_OKAY)
		{
			TempoCamp200 = CurrValue;
		}
		Reply = LSReadTrimmerMICR(Connessione,(HWND)hDlg, TypeSpeed, buffDati, NR_BYTES_TRIMMER);
		if( Reply  == LS_OKAY)
		{
		TrimmerValue200 = buffDati[0];
		}	
		
		TypeSpeed = TEST_SPEED_300_DPI; 
		Reply = LSReadTimeSampleMICR(Connessione,(HWND)hDlg, TypeSpeed+3, &CurrValue, 2);
		if( Reply  == LS_OKAY)
		{
			TempoCamp300 = CurrValue;
		}

		Reply = LSReadTrimmerMICR(Connessione,(HWND)hDlg, TypeSpeed, buffDati, NR_BYTES_TRIMMER);
		if( Reply  == LS_OKAY)
		{
			TrimmerValue300 = buffDati[0];
		}
			
		TypeSpeed = TEST_SPEED_75_DPM; 
		Reply = LSReadTimeSampleMICR(Connessione,(HWND)hDlg, TypeSpeed+3, &CurrValue, 2);
		if( Reply  == LS_OKAY)
		{
			TempoCamp300C = CurrValue;
		}
		Reply = LSReadTrimmerMICR(Connessione,(HWND)hDlg, TypeSpeed, buffDati, NR_BYTES_TRIMMER);
		if( Reply  == LS_OKAY)
		{
			TrimmerValue300C = buffDati[0];
		}
		


		
		// trimmer	

		Reply = LSImageCalibration(Connessione,(HWND)hDlg, FALSE, &VelVal);
		TempoImmagine = VelVal;

		//Almeno non Fallisce se eseguo il Test Singolarmente
		Reply =	LSUnitIdentify(Connessione,(HWND)0, IdentStr,LsName,Version,Date_Fw,SerialNumber,BoardNr,DecodeExp,inkVer, FeederVersion,SorterVersion,MotorVersion, datefpga, NULL);
		if (Reply == LS_OKAY)
			SVersion = Marshal::PtrToStringAnsi((IntPtr) (char *)Version);
		
		
		if( (SVersion[0] > '4') || (SVersion[0] == '4' && SVersion[2] >= '0' && SVersion[3] >= '8') )
		{
			fDatiScannerColore = 1;
			Reply = LSReadScannerData(Connessione,SIDE_FRONT_IMAGE,DATA_SCANNER_GRAY,0,4,(unsigned char *)(&nrBytes));
			if ( Reply == LS_OKAY)
			{
				Reply = LSReadScannerData(Connessione,SIDE_FRONT_IMAGE,DATA_SCANNER_GRAY,1,nrBytes,(unsigned char *)(buffscanner));
				if ( Reply == LS_OKAY)
				{
					for(int iii = 0;iii <nrBytes;iii++)
					{
						BufferDumpGray[iii] = buffscanner[iii];
					}
				}
			}

			Reply = LSReadScannerData(Connessione,SIDE_FRONT_IMAGE,DATA_SCANNER_RED,0,4,(unsigned char *)(&nrBytes));
			if ( Reply == LS_OKAY)
			{
				Reply = LSReadScannerData(Connessione,SIDE_FRONT_IMAGE,DATA_SCANNER_RED,1,nrBytes,(unsigned char *)(buffscanner));
				if ( Reply == LS_OKAY)
				{
					for(int iii = 0;iii <nrBytes;iii++)
					{
						BufferDumpRed[iii] = buffscanner[iii];
					}
				}
			}

			Reply = LSReadScannerData(Connessione,SIDE_FRONT_IMAGE,DATA_SCANNER_GREEN,0,4,(unsigned char *)(&nrBytes));
			if ( Reply == LS_OKAY)
			{
				Reply = LSReadScannerData(Connessione,SIDE_FRONT_IMAGE,DATA_SCANNER_GREEN,1,nrBytes,(unsigned char *)(buffscanner));
				if ( Reply == LS_OKAY)
				{
					for(int iii = 0;iii <nrBytes;iii++)
					{
						BufferDumpGreen[iii] = buffscanner[iii];
					}
				}
			}

			Reply = LSReadScannerData(Connessione,SIDE_FRONT_IMAGE,DATA_SCANNER_BLUE,0,4,(unsigned char *)(&nrBytes));
			if ( Reply == LS_OKAY)
			{
				Reply = LSReadScannerData(Connessione,SIDE_FRONT_IMAGE,DATA_SCANNER_BLUE,1,nrBytes,(unsigned char *)(buffscanner));
				if ( Reply == LS_OKAY)
				{
					for(int iii = 0;iii <nrBytes;iii++)
					{
						BufferDumpBlue[iii] = buffscanner[iii];
					}
				}
			}

			/* SCANNER RETRO */
			Reply = LSReadScannerData(Connessione,SIDE_BACK_IMAGE,DATA_SCANNER_GRAY,0,4,(unsigned char *)(&nrBytes));
			if ( Reply == LS_OKAY)
			{
				Reply = LSReadScannerData(Connessione,SIDE_BACK_IMAGE,DATA_SCANNER_GRAY,1,nrBytes,(unsigned char *)(buffscanner));
				if ( Reply == LS_OKAY)
				{
					for(int iii = 0;iii <nrBytes;iii++)
					{
						BufferDumpGrayR[iii] = buffscanner[iii];
					}
				}
			}

			Reply = LSReadScannerData(Connessione,SIDE_BACK_IMAGE,DATA_SCANNER_RED,0,4,(unsigned char *)(&nrBytes));
			if ( Reply == LS_OKAY)
			{
				Reply = LSReadScannerData(Connessione,SIDE_BACK_IMAGE,DATA_SCANNER_RED,1,nrBytes,(unsigned char *)(buffscanner));
				if ( Reply == LS_OKAY)
				{
					for(int iii = 0;iii <nrBytes;iii++)
					{
						BufferDumpRedR[iii] = buffscanner[iii];
					}
				}
			}

			Reply = LSReadScannerData(Connessione,SIDE_BACK_IMAGE,DATA_SCANNER_GREEN,0,4,(unsigned char *)(&nrBytes));
			if ( Reply == LS_OKAY)
			{
				Reply = LSReadScannerData(Connessione,SIDE_BACK_IMAGE,DATA_SCANNER_GREEN,1,nrBytes,(unsigned char *)(buffscanner));
				if ( Reply == LS_OKAY)
				{
					for(int iii = 0;iii <nrBytes;iii++)
					{
						BufferDumpGreenR[iii] = buffscanner[iii];
					}
				}
			}

			Reply = LSReadScannerData(Connessione,SIDE_BACK_IMAGE,DATA_SCANNER_BLUE,0,4,(unsigned char *)(&nrBytes));
			if ( Reply == LS_OKAY)
			{
				Reply = LSReadScannerData(Connessione,SIDE_BACK_IMAGE,DATA_SCANNER_BLUE,1,nrBytes,(unsigned char *)(buffscanner));
				if ( Reply == LS_OKAY)
				{
					for(int iii = 0;iii <nrBytes;iii++)
					{
						BufferDumpBlueR[iii] = buffscanner[iii];
					}
				}
			}

		}// se la versione � supportata chiedo i dati


		// salvo i valori degli AD
		Reply = LSGetPWMValues(Connessione,SIDE_FRONT_IMAGE,0,4,(unsigned char *)(&nrBytes));
		if( Reply == LS_OKAY)
		{
			Reply = LSGetPWMValues(Connessione,SIDE_FRONT_IMAGE,1,nrBytes,(unsigned char *)(buffscanner));
			if( Reply == LS_OKAY)
			{
				for(int iii = 0;iii <nrBytes;iii++)
				{
					BufferADValues[iii] = buffscanner[iii];
				}
			}
		}



		Reply = LSGetPWMValues(Connessione,SIDE_BACK_IMAGE,0,4,(unsigned char *)(&nrBytes));
		if( Reply == LS_OKAY)
		{
			Reply = LSGetPWMValues(Connessione,SIDE_BACK_IMAGE,1,nrBytes,(unsigned char *)(buffscanner));
			if( Reply == LS_OKAY)
			{
				for(int iii = 0;iii <nrBytes;iii++)
				{
					BufferADValuesR[iii] = buffscanner[iii];
				}
			}
		}


	}
	LSDisconnect(Connessione,(HWND)hDlg);
	return Reply;
}



int Ls515Class::Periferica::LeggiImmaginiAllinea(char Side,short ScanMode)
{

	HANDLE HBackGray=0;
	HANDLE HFrontGray=0;
	HANDLE HFrontUV=0;
	char *StrNome=NULL;
	short Connessione;
	

	short floop = false;
	ReturnC->FunzioneChiamante = "Lettura Immagini";
	
	Reply = CollaudoLSConnect((HWND)0,(HANDLE)hInst,TipoPeriferica,	&Connessione);
	
	
	if ((Reply == LS_OKAY)|| (Reply == LS_ALREADY_OPEN))
	{
		if (HFrontGray)
			LSFreeImage((HWND)hDlg, &HFrontGray);

		if (HFrontUV)
			LSFreeImage((HWND)hDlg, &HFrontUV);

		if (HBackGray)
			LSFreeImage((HWND)hDlg, &HBackGray);

		Reply  = LSConfigDoubleLeafingAndDocLength(Connessione,(HWND)hDlg,DOUBLE_LEAFING_DISABLE,50,150,215);

		Reply = LSSetThresholdClearBlack(Connessione,(HWND)hDlg,0x33);
	
		do
		{
		

			Reply = LSDocHandle(Connessione,(HWND)hDlg,
								NO_STAMP,
								NO_PRINT_VALIDATE,
								NO_READ_CODELINE,
								Side,
								ScanMode,
								AUTOFEED,
								SORTER_BAY1,
								WAIT_YES,
								NO_BEEP,
								NULL,
								SCAN_PAPER_DOCUMENT,
								0);
			if (Reply == LS_FEEDER_EMPTY)
			{
				floop = TRUE;
			}
			else
				floop = FALSE;
		}while(floop);

		if( Reply == LS_OKAY )
		{

			Reply = LSReadImage(Connessione, (HWND)hDlg,
										CLEAR_AND_ALIGN_IMAGE,
										Side,
										0, 0,
										(LPHANDLE)&HFrontGray,
										(LPHANDLE)&HBackGray,
										(LPHANDLE)&HFrontUV,
										NULL);
			LSSetThresholdClearBlack(Connessione,(HWND)hDlg,0x44);
				
			if(Reply == LS_OKAY)
			{
				// salvo le immagini
				if ((Side == SIDE_ALL_IMAGE) ||(Side == SIDE_FRONT_IMAGE))
				{
				//	CTSLoadDIBimage("Teorico.bmp",&HBackGray);
				//	SigmaRetro = CalcolaIstogramma(HBackGray,HistogrammaRetro);

//
				//	SigmaFronte = CalcolaIstogramma(HFrontGray,HistogrammaFronte);

			/*		double Pearson = 0;
					double PearsonX = 0;
					double PearsonY = 0;
					
					for (int iii = 0;iii < 256; iii++)
					{
						Pearson += HistogrammaRetro[iii] * HistogrammaFronte[iii];
						PearsonX += HistogrammaRetro[iii] ;
						PearsonY += HistogrammaFronte[iii] ;

					}
					Pearson = Pearson/256;
					PearsonX = PearsonX/256;
					PearsonY = PearsonY/256;
					Pearson = (Pearson - PearsonX * PearsonY) /(SigmaRetro * SigmaFronte);*/
				
					


					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
					Reply = LSSaveDIB((HWND)hDlg,  HFrontGray,StrNome);

					if (ScanMode == SCAN_MODE_256GR200_AND_UV)
					{
						StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagine ).ToPointer();
						Reply = LSSaveDIB((HWND)hDlg,  HFrontUV,StrNome);
					}
						 // Always free trhe unmanaged string.
					  Marshal::FreeHGlobal(IntPtr(StrNome));
				}
				if ((Side == SIDE_ALL_IMAGE) || (Side == SIDE_BACK_IMAGE))
				{
					
					StrNome = (char*) Marshal::StringToHGlobalAnsi(NomeFileImmagineRetro ).ToPointer();
					Reply = LSSaveDIB((HWND)hDlg,HBackGray,StrNome);

					 // Always free the unmanaged string.
					  Marshal::FreeHGlobal(IntPtr(StrNome));
				}	
			}
			else
			{
				ReturnC->FunzioneChiamante = "LSReadImage";
			}
			
			LSDisconnect(Connessione,(HWND)hDlg);
		}
		else
		{
			ReturnC->FunzioneChiamante = "LSDocHandle";
		}
	}
	else
	{
		ReturnC->FunzioneChiamante = "LSOpen";
	}
	// libero le immagini
	if (HFrontUV)
		LSFreeImage((HWND)hDlg, &HFrontUV);
	if (HFrontGray)
		LSFreeImage((HWND)hDlg, &HFrontGray);
	if (HBackGray)
		LSFreeImage((HWND)hDlg, &HBackGray);
	ReturnC->ReturnCode = Reply;
	

	return Reply;
}

