// Ls515Class.h

#include <windows.h>
#include "ErrWar.h"
#include "is.h"
#include "constanti.h"


#pragma once

using namespace System;
using namespace System::IO;
using namespace System::Runtime::InteropServices;
using namespace System::Runtime::Serialization;
using namespace System::Reflection;
using namespace System::Windows::Forms;



namespace Ls515Class {

	public ref class Storico
	{
	public:
		// TODO: aggiungere qui i metodi per la classe.
		int Documenti_Catturati;
		int Documenti_Ingresso;
		int Documenti_Trattati;
		int Documenti_Trattenuti;
		int Doppia_Sfogliatura;
		int Errori_Barcode;
		int Errori_CMC7;
		int Errori_E13B;
		int Errori_Ottici;
		int Jam_Allinea;
		int Jam_Card;
		int Jam_Cassetto1;
		int Jam_Cassetto2;
		int Jam_Cassetto3;
		int Jam_Feeder;
		int Jam_Feeder_Ret;
		int Jam_Micr;
		int Jam_Micr_Ret;
		int Jam_Percorso_DX;
		int Jam_Percorso_SX;
		int Jam_Print;
		int Jam_Print_Ret;
		int Jam_Scanner;
		int Jam_Scanner_Ret;
		int Jam_Stamp;
		int Jam_Sorter;
		int Num_Accensioni;
		int Doc_Timbrati;
		int Doc_Timbrati_retro;
		int Trattenuti_Micr;
		int Trattenuti_Scan;
		int TempoAccensione;
	};

	
	
	public ref class Periferica
	{
			
	private : 
	 
	
			//int __stdcall Visual(char * str);
			int __clrcall Visual(char * str);
			//short GetTypeLS(String ^Version);
			short GetTypeLS(char *LsName,char *Version, char *Ls515_Config);
			int CheckReply(HWND hDlg,int Rep,char *s);

			short MICR_TolleranceMore;
			short MICR_TolleranceLess;
			float MICR_Valore100 ;
			short	nrLettura;
			int MICR_SignalValue;

			

			// legge le codeline OCR
			//int LeggiCodelineOCR(short Type);

	public : 
		
		//	static const String^ VERSIONEDLL_1 = "Ls515Class Ver 1.00 del 10-06-09";
			  ErrWar ^ReturnC ;
			  Cis ^strCis;
			Storico ^S_Storico;

			String ^  VersioneDll_0;
			String ^ VersioneDll_1;



			short TipoPeriferica;
			short Connessione;
		

			short NrLettureOk;
			short ConfVelocitBassa;
			short ConfMicr;
			short ConfEndorserPrinter;
			short ConfVoidingStamp;
			short ConfScannerFront;
			short ConfScannerBack;
			short ConfBadgeReader;
			short ConfscannerUltra;
			short ConfscannerColor;

			// stringa che contiene l'ultima codeline letta
			String ^  CodelineLetta;
			String ^  BadgeLetto;
			String ^  BarcodeLettoSx;
			String ^  DataMatrixLetto;
			String ^  BarcodeLettoDx;

			// stringhe ritornate dall identificativo
			String   ^ SIdentStr;
			String ^  SLsName;
			String ^  SDateFW;
			String ^  SVersion;
			String ^  SSerialNumber;
			String ^  SFeederVersion;
			String ^  SBoardNr;
			String ^  SCpldNr;
			String ^  SDateFPGA;

			String  ^ FileNameDownload;
			String  ^ NomeFileImmagine;
			String ^  NomeFileImmagineRetro;
			String  ^ NomeFileImmagineUV;
			
			
			String ^Stampafinale;
			
			

			
			// valori photosensori
			static cli::array<int> ^ PhotoValue = gcnew cli::array<int>(8);
			// valori fotosensori doppia SF
			static cli::array<Byte> ^ PhotoValueDP = gcnew cli::array<Byte>(16);
			// Byte di Stato periferica 
			static cli::array<Byte> ^ StatusByte = gcnew cli::array<Byte>(16);
			static cli::array<Int16> ^ Distanze = gcnew cli::array<Int16>(1024);
			
			long llMax, llMin;
			float llMedia;
			double DevStd;
			int stile;
			int prova ;


			int ValMinCinghia;
			int ValMaxCinghia;

			ListBox^ list;

			static cli::array<Byte> ^ E13BSignal  = gcnew cli::array<Byte>(16000);
			//Byte *E13BSignal;
			//Byte *ViewSignal;
			static cli::array<Byte> ^ ViewSignal  = gcnew cli::array<Byte>(10000);
			

			// dump memoria
			static cli::array<Byte> ^ BufferDump = gcnew cli::array<Byte>(0x9000);

			static cli::array<Byte> ^ BufferDumpGray = gcnew cli::array<Byte>(3000);
			static cli::array<Byte> ^ BufferDumpBlue = gcnew cli::array<Byte>(3000);
			static cli::array<Byte> ^ BufferDumpGreen = gcnew cli::array<Byte>(3000);
			static cli::array<Byte> ^ BufferDumpRed = gcnew cli::array<Byte>(3000);
			static cli::array<Byte> ^ BufferDumpUVLow = gcnew cli::array<Byte>(3000);
			static cli::array<Byte> ^ BufferDumpUVHight = gcnew cli::array<Byte>(3000);

			
			static cli::array<Byte> ^ BufferDumpGrayR = gcnew cli::array<Byte>(3000);
			static cli::array<Byte> ^ BufferDumpBlueR = gcnew cli::array<Byte>(3000);
			static cli::array<Byte> ^ BufferDumpGreenR = gcnew cli::array<Byte>(3000);
			static cli::array<Byte> ^ BufferDumpRedR = gcnew cli::array<Byte>(3000);
			static cli::array<Byte> ^ BufferDumpUVLowR = gcnew cli::array<Byte>(3000);
			static cli::array<Byte> ^ BufferDumpUVHightR = gcnew cli::array<Byte>(3000);
			
			static cli::array<Byte> ^ BufferADValues = gcnew cli::array<Byte>(256);
			static cli::array<Byte> ^ BufferADValuesR = gcnew cli::array<Byte>(256);

		unsigned short TempoCamp200;
		unsigned short TempoCamp300;
		unsigned short TempoCamp75;
		unsigned short TempoCamp300C;

		short TempoImmagine;

		unsigned char TrimmerValue200;
		unsigned char TrimmerValue300;
		unsigned char TrimmerValue75;
		unsigned char TrimmerValue300C;
			
		int fDatiScannerColore;

			
		// Reply delle funzioni
		int Reply;

		static int hDlg;
		static int hInst;
		static HANDLE IDPort;
		// tipo di periferica
		short TypeLS;
		// MICR lettura
		float Percentile;
		short ValMax,ValMin;
		short Sensore;

		
		// identificativo
		int Identificativo();
		// Calibra i fotosensori e ritorna i valori
		//int SetConfigurazione(int E13B,int CMC7,int timbro,int badge12,int badge23,int badge123,int scannerfronte, int scannerretro,int inkjet, int velocita,int Sflogliatore50,int ScannerType);
		int SetConfigurazione(int E13B,int CMC7,int timbro,int badge12,int badge23,int badge123,int scannerfronte, int scannerretro,int inkjet, int velocita,int Sflogliatore50,int ScannerType,int SwAbilitato,int fCardCol);
		int CalibraImmagine();
		// legge le codeline OCR
		int LeggiCodelineOCR(short Type);

		int CalibrazioneFoto();
		// imposta la configurazione , calibra il foto DP e reimposta la configurazione
		int CalSfogliatura();

		// Timbra fronte e retro
		int Timbra(short DocHandling);
		//testa la stampante
		int TestStampa(int StampaStd);
		int TestStampaAlto(int StampaStd);
		int LeggiBadge(short Traccia);

		int SerialNumber();
		int DoMICRCalibration3(int MICR_Value,long NrReadGood,	char	TypeSpeed, ListBox^ list);
		

		unsigned char Calc_Trimmer_Position(float ValoreLetto, unsigned char PosTrimmer);
		unsigned char Calc_Trimmer_Position_ev(float ValoreLetto, unsigned char PosTrimmer);

		int EstraiDatiSpeedE13B(unsigned char *pd, long llDati,short *NrPicchi, float *SommaValori, long *ValMin, short *NrPicchiMin);
		
		int LeggiImmagini(char Side,short ScanMode);
		int LeggiImmCard(char Side);
		// calibra gli scanner, viene passato il tipo di scanner fronte o retro
		int CalibraScanner(int Scanner);
		
		// fa il reset della periferica
		int Reset(short TypeReset);

		int StatoPeriferica();

		int ViewE13BSignal(int fView);

		// legge la codeline e permette di trattenere catturare un documento
		int LeggiCodeline(short DocHandling);
		int LeggiCodelineImmagini(short DocHandling,short FMicr);
		
		int LeggiCodelineImmaginiAuto(short DocHandling);
		int LeggiCodelineImmaginiTrattieni();
		int LeggiCodelineImmaginiIncasella(short DocHandling);

		// esegue l'autodoc handle
		int ViaDocumentiAuto(short nrDoc,short DocValue);
		int Stopaaaaa();
		int DisegnaEstrectLenBar();
		int Docutest04();

		int DoImageCalibration(short Connessione,HWND hDlg, float TeoricValue);
		int CalcolaSalvaDistanze(HWND hDlg, BITMAPINFOHEADER *pImage, short LineToCheck);
		void ClearFrontAndRearBlack(BITMAPINFOHEADER *Bitmap);
		
		int DoTimeSampleMICRCalibration(short TypeSpeed);
		int EraseHistory();
		int AccendiLuce(short Luce);
		int MotoreFeeder(short Acceso);
		int sensorFeeder();
		int TestCinghia();
		int LeggiValoriCIS();
		int TestLed(short Color);

	//	void ShowMICRSignal(ListBox ^lista,float p,unsigned char trim);

		int SetValoreDefImmagine();

		// costruttore della classe
		Periferica();

		int SetVelocita(int fSpeed);
		int SetDiagnostic(int fbool);

		int GetVersione();
		int CollaudoLSConnect(HWND hDlg,HANDLE hInst,short TipoPeriferica,short *Connessione);
		 
		void ShowMICRSignal( float Percentile, short Trimmer);

		int LeggiValoriPeriferica();
		int LeggiImmaginiAllinea(char Side,short ScanMode);

		int SetHightSpeedMode();

		int Ls515Class::Periferica::DecodificaBarcode2D(char Side,short ScanMode,short fPdf);
		
	};
}
