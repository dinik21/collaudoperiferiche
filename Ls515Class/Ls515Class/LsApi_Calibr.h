//
// CTS Electronics
// Corso Vercelli, 332 - 10015 Ivrea (TORINO) Italy
// January 2001
//
// Phone ++39 125 235611
// Telefax ++39 125 235623
//
// www.ctsgroup.it		techsupp@ctsgroup.it
//
//
// All rights reserved
//
// We reserve the right to vary technical specification
//
//  MODULE:   LSAPI.H
//
//  PURPOSE:  LS Include Interface


#ifndef LSAPI_CALIBR_H
#define LSAPI_CALIBR_H     1


#define LS_EEPROM_READ					0
#define LS_EEPROM_WRITE					1


#define LS515_LED_ON					0x02
#define LS515_LED_BADGE					0x01
#define LS515_LED_PROCESS				0x04


#define LS150_ELECTROMAGNET_POCKET		1
#define LS150_ELECTROMAGNET_LEAFER		2
#define LS150_ELECTROMAGNET_INKJET		3
#define LS150_ELECTROMAGNET_STAMP		4


enum
{
	DATA_SCANNER_GRAY,
	DATA_SCANNER_BLUE,
	DATA_SCANNER_GREEN,
	DATA_SCANNER_RED,
	DATA_SCANNER_UV_LOW_CONTRAST,
	DATA_SCANNER_UV_HIGH_CONTRAST,
};


typedef struct _BUILDERPARAM
{
	BOOL	Beep;						// Passo il Beep per LS100, LS150
	BOOL	StopLoopOnErrorInCodeline;	// Stop loop di DocHandle su erroe in codeline
	BOOL	SaveE13BSignal;				// Save E13B Signal
	BOOL	fApplDoCheckCodeline;		// L'applicativo segnala errore in codeline
	char	CheckCodeline[CODE_LINE_LENGTH];
} BUILDERPARAM, *LPBUILDERPARAM;




// --------------------------------------------------------------
// CTS Reserved function
// --------------------------------------------------------------
#ifdef __cplusplus
extern "C" {
#endif

extern int APIENTRY LSConnectDiagnostica(HWND, HANDLE, short, short *);

extern int APIENTRY LSConnectDownload(HWND hWnd, HANDLE hInstAppl, short Peripheral, short *hConnect);

extern int APIENTRY LSReadCodelineWhitOCR(short hConnect, HWND hWnd, LPSTR Codeline, short *Length_Codeline, LPSTR Optic, short *Length_Optic);

extern int APIENTRY LSSetConfiguration(short hConnect, HWND hWnd, char *BytesCfg);

extern int APIENTRY LSBuilderSetting(short, HWND, void *);

extern int APIENTRY LSSetSerialNumber(short, HWND, unsigned char *, short);

extern int APIENTRY LSReadE13BSignal(short, HWND, unsigned char *, long *);

extern int APIENTRY LSReadCMC7Signal(short hConnect, HWND hWnd, unsigned char *pBuff, long llBuff);

extern int APIENTRY LSReadImageOCR(short hConnect, HWND hWnd, void **pBuff);

extern int APIENTRY LSScannerCalibration(short, HWND, short);

extern int APIENTRY LSPhotosCalibration(short, HWND, short);

extern int APIENTRY LSReadPhotosValue(short, HWND, unsigned short *, unsigned short *);

extern int APIENTRY LSDumpPeripheralMemory(short, HWND, unsigned char *, long, long, char);

extern int APIENTRY LSCalibrationMICR(short, HWND, unsigned char *, short);

extern int APIENTRY LSSetDiagnosticMode(short, HWND, BOOL);

extern int APIENTRY LSSetTrimmerMICR(short, HWND, short, short);

extern int APIENTRY LSReadTrimmerMICR(short, HWND, char, unsigned char *, short);

extern int APIENTRY LSReadTimeSampleMICR(short, HWND, char, unsigned short *, short);

extern int APIENTRY LSSetTimeSampleMICR(short, HWND, char, short);

extern int APIENTRY LSSetInImageCalibration(short, HWND);

extern int APIENTRY LSImageCalibration(short, HWND, BOOL, short *);

extern int APIENTRY LSMoveMotorFeeder(short, HWND, short);

extern int APIENTRY LSTestElectromagnet(short hConnect, HWND hWnd, short nrElectromagnet);

extern int APIENTRY LSReadFeederMotorAD(short hConnect, HWND hWnd, unsigned short *pDati);

extern int APIENTRY LSSetScannerLight(short hConnect, HWND hWnd, short Light);

extern int APIENTRY LSReloadImage(short	hConnect, HWND hWnd, short ClearBlack, char Side, short nrBank, long ImageLen, unsigned long NrDoc, LPHANDLE FrontImage, LPHANDLE BackImage, LPHANDLE FrontImageNetto, LPHANDLE BackImageNetto);

extern int APIENTRY LSTestCinghia(short hConnect, HWND hWnd, unsigned char *pBuff, long *llBuff);

extern int APIENTRY LSEEPROMCommand(short hConnect, HWND hWnd, short Cmd, short Offset, short NrBytes, unsigned char *pData);

extern int APIENTRY LSGetPWMValues(short hConnect, char Side, long fReadData, long nrBytes, unsigned char *pData);

extern int APIENTRY LSReadScannerData(short hConnect, char Side, short Color, long fReadData, long nrBytes, unsigned char *pData);

extern int APIENTRY LS800_PhotosCalibration(HWND, char, unsigned char *, short);

extern int APIENTRY LS800_ReadPhotosValue(HWND, char, unsigned char *, short);


#ifdef __cplusplus
}
#endif

////////////////////////////////////////////////////////////////////

#endif
