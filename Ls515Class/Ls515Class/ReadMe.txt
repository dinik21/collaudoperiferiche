========================================================================
    LIBRERIA A COLLEGAMENTO DINAMICO: cenni preliminari sul progetto 
    Ls515Class
========================================================================

La creazione guidata applicazione ha creato questa DLL 
Ls515Class.  

Questo file contiene un riepilogo del contenuto di ciascun file che fa 
parte dell'applicazione Ls515Class.

Ls515Class.vcproj
    File di progetto principale per i progetti VC++ generati tramite una 
    creazione guidata applicazione. 
    Contiene informazioni sulla versione di Visual C++ che ha generato 
    il file e informazioni sulle piattaforme, le configurazioni e le 
    caratteristiche del progetto selezionate con la creazione guidata 
    applicazione.

Ls515Class.cpp
    File di origine della DLL principale.

Ls515Class.h
    File contenente una dichiarazione di classe.

AssemblyInfo.cpp
    Contiene attributi personalizzati per la modifica dei metadati 
    degli assembly.

/////////////////////////////////////////////////////////////////////////////
Altre note:

la creazione guidata applicazione utilizza il prefisso "TODO:" per indicare 
le parti del codice sorgente da aggiungere o personalizzare.

/////////////////////////////////////////////////////////////////////////////
