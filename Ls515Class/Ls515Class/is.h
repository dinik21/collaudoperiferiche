#pragma once
using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Diagnostics;

 
namespace Ls515Class
{
	/// <summary> 
	/// Riepilogo per ErrWar
	/// </summary>
	//__gc public class Cis   
	public ref class Cis
	{
	
	protected: 

				
	private:

	public:
		Cis(void);
		
		Byte Add_PWM_Blue_F;
		Byte Dato_PWM_Blue_F;
		Byte Add_PWM_Green_F;
		Byte Dato_PWM_Green_F;
		Byte Add_PWM_Red_F;
		Byte Dato_PWM_Red_F;
		Byte Dato_Offset_F;
		Byte Dato_Gain_F;
	 
		Byte Add_PWM_Blue_R;
		Byte Dato_PWM_Blue_R;
		Byte Add_PWM_Green_R;
		Byte Dato_PWM_Green_R;
		Byte Add_PWM_Red_R;
		Byte Dato_PWM_Red_R;
		Byte  Dato_Offset_R;
		Byte Dato_Gain_R;
	 
		Byte OutMap_0;
		Byte OutMap_1;
		Byte OutMap_2;
		Byte OutMap_3;
	};
}
