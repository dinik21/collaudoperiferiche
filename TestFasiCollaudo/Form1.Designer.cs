﻿namespace TestFasiCollaudo
{
    partial class Form1
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Liberare le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.CPeriferica = new System.Windows.Forms.ComboBox();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.LCommenti = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.LNote = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.LIdFase = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.LTent = new System.Windows.Forms.Label();
            this.faseBar1 = new BarraFasi.FaseBar();
            this.LDesc = new System.Windows.Forms.Label();
            this.LNome = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.LNotel = new System.Windows.Forms.Label();
            this.LRet = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.LResult = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BEseguiTest = new System.Windows.Forms.Button();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // CPeriferica
            // 
            this.CPeriferica.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CPeriferica.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CPeriferica.FormattingEnabled = true;
            this.CPeriferica.Location = new System.Drawing.Point(72, 15);
            this.CPeriferica.Margin = new System.Windows.Forms.Padding(6);
            this.CPeriferica.Name = "CPeriferica";
            this.CPeriferica.Size = new System.Drawing.Size(229, 26);
            this.CPeriferica.TabIndex = 0;
            this.CPeriferica.SelectedIndexChanged += new System.EventHandler(this.CPeriferica_SelectedIndexChanged);
            // 
            // treeView1
            // 
            this.treeView1.CheckBoxes = true;
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.treeView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeView1.Location = new System.Drawing.Point(0, 81);
            this.treeView1.Margin = new System.Windows.Forms.Padding(4);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(373, 621);
            this.treeView1.TabIndex = 1;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(4);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.CPeriferica);
            this.splitContainer1.Panel1.Controls.Add(this.treeView1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1092, 702);
            this.splitContainer1.SplitterDistance = 373;
            this.splitContainer1.SplitterWidth = 6;
            this.splitContainer1.TabIndex = 2;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.LCommenti);
            this.splitContainer2.Panel1.Controls.Add(this.label8);
            this.splitContainer2.Panel1.Controls.Add(this.LNote);
            this.splitContainer2.Panel1.Controls.Add(this.label5);
            this.splitContainer2.Panel1.Controls.Add(this.LIdFase);
            this.splitContainer2.Panel1.Controls.Add(this.label4);
            this.splitContainer2.Panel1.Controls.Add(this.LTent);
            this.splitContainer2.Panel1.Controls.Add(this.faseBar1);
            this.splitContainer2.Panel1.Controls.Add(this.LDesc);
            this.splitContainer2.Panel1.Controls.Add(this.LNome);
            this.splitContainer2.Panel1.Controls.Add(this.label3);
            this.splitContainer2.Panel1.Controls.Add(this.label2);
            this.splitContainer2.Panel1.Controls.Add(this.label1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.label7);
            this.splitContainer2.Panel2.Controls.Add(this.LNotel);
            this.splitContainer2.Panel2.Controls.Add(this.LRet);
            this.splitContainer2.Panel2.Controls.Add(this.label6);
            this.splitContainer2.Panel2.Controls.Add(this.LResult);
            this.splitContainer2.Panel2.Controls.Add(this.pictureBox2);
            this.splitContainer2.Panel2.Controls.Add(this.pictureBox1);
            this.splitContainer2.Panel2.Controls.Add(this.BEseguiTest);
            this.splitContainer2.Size = new System.Drawing.Size(713, 702);
            this.splitContainer2.SplitterDistance = 204;
            this.splitContainer2.TabIndex = 0;
            // 
            // LCommenti
            // 
            this.LCommenti.AutoSize = true;
            this.LCommenti.Location = new System.Drawing.Point(433, 160);
            this.LCommenti.Name = "LCommenti";
            this.LCommenti.Size = new System.Drawing.Size(37, 18);
            this.LCommenti.TabIndex = 11;
            this.LCommenti.Text = "note";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(290, 160);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 18);
            this.label8.TabIndex = 10;
            this.label8.Text = "Note:";
            // 
            // LNote
            // 
            this.LNote.AutoSize = true;
            this.LNote.Location = new System.Drawing.Point(172, 156);
            this.LNote.Name = "LNote";
            this.LNote.Size = new System.Drawing.Size(46, 18);
            this.LNote.TabIndex = 9;
            this.LNote.Text = "label4";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 156);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(141, 18);
            this.label5.TabIndex = 8;
            this.label5.Text = "Parametri Funzione:";
            // 
            // LIdFase
            // 
            this.LIdFase.AutoSize = true;
            this.LIdFase.Location = new System.Drawing.Point(433, 118);
            this.LIdFase.Name = "LIdFase";
            this.LIdFase.Size = new System.Drawing.Size(31, 18);
            this.LIdFase.TabIndex = 7;
            this.LIdFase.Text = "text";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(287, 118);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 18);
            this.label4.TabIndex = 6;
            this.label4.Text = "ID Fase:";
            // 
            // LTent
            // 
            this.LTent.AutoSize = true;
            this.LTent.Location = new System.Drawing.Point(128, 118);
            this.LTent.Name = "LTent";
            this.LTent.Size = new System.Drawing.Size(46, 18);
            this.LTent.TabIndex = 5;
            this.LTent.Text = "label4";
            // 
            // faseBar1
            // 
            this.faseBar1.BackColor = System.Drawing.Color.Silver;
            this.faseBar1.BorderStyle = System.Windows.Forms.Border3DStyle.Flat;
            this.faseBar1.CycleSpeed = 500;
            this.faseBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.faseBar1.FDraw = false;
            this.faseBar1.Location = new System.Drawing.Point(0, 0);
            this.faseBar1.Name = "faseBar1";
            this.faseBar1.Phases = ((Microsoft.VisualBasic.Collection)(resources.GetObject("faseBar1.Phases")));
            this.faseBar1.PointLength = 15;
            this.faseBar1.SelectedIndex = 0;
            this.faseBar1.Size = new System.Drawing.Size(713, 63);
            this.faseBar1.TabIndex = 0;
            this.faseBar1.TextBuffer = 10;
            // 
            // LDesc
            // 
            this.LDesc.AutoSize = true;
            this.LDesc.Location = new System.Drawing.Point(433, 81);
            this.LDesc.Name = "LDesc";
            this.LDesc.Size = new System.Drawing.Size(46, 18);
            this.LDesc.TabIndex = 4;
            this.LDesc.Text = "label4";
            // 
            // LNome
            // 
            this.LNome.AutoSize = true;
            this.LNome.Location = new System.Drawing.Point(128, 81);
            this.LNome.Name = "LNome";
            this.LNome.Size = new System.Drawing.Size(46, 18);
            this.LNome.TabIndex = 3;
            this.LNome.Text = "label4";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "N° Tentativi:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(287, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Descrizione Fase:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome Fase:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(294, 270);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(185, 18);
            this.label7.TabIndex = 8;
            this.label7.Text = "Dettaglio Esecuzione Fase";
            // 
            // LNotel
            // 
            this.LNotel.AutoSize = true;
            this.LNotel.Location = new System.Drawing.Point(499, 270);
            this.LNotel.Name = "LNotel";
            this.LNotel.Size = new System.Drawing.Size(40, 18);
            this.LNotel.TabIndex = 7;
            this.LNotel.Text = "Note";
            // 
            // LRet
            // 
            this.LRet.AutoSize = true;
            this.LRet.Location = new System.Drawing.Point(142, 270);
            this.LRet.Name = "LRet";
            this.LRet.Size = new System.Drawing.Size(46, 18);
            this.LRet.TabIndex = 6;
            this.LRet.Text = "label7";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 270);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 18);
            this.label6.TabIndex = 5;
            this.label6.Text = "Ritorno Funzione:";
            // 
            // LResult
            // 
            this.LResult.AutoSize = true;
            this.LResult.Location = new System.Drawing.Point(142, 307);
            this.LResult.Name = "LResult";
            this.LResult.Size = new System.Drawing.Size(46, 18);
            this.LResult.TabIndex = 4;
            this.LResult.Text = "label6";
            this.LResult.Visible = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(367, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(346, 245);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(346, 245);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // BEseguiTest
            // 
            this.BEseguiTest.Location = new System.Drawing.Point(13, 432);
            this.BEseguiTest.Name = "BEseguiTest";
            this.BEseguiTest.Size = new System.Drawing.Size(129, 50);
            this.BEseguiTest.TabIndex = 1;
            this.BEseguiTest.Text = "ESEGUI TEST";
            this.BEseguiTest.UseVisualStyleBackColor = true;
            this.BEseguiTest.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1092, 702);
            this.Controls.Add(this.splitContainer1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sequenza Collaudo Periferica";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox CPeriferica;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Label LTent;
        private System.Windows.Forms.Label LDesc;
        private System.Windows.Forms.Label LNome;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private BarraFasi.FaseBar faseBar1;
        private System.Windows.Forms.Label LIdFase;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label LNote;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label LResult;
        private System.Windows.Forms.Label LRet;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label LNotel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label LCommenti;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button BEseguiTest;
    }
}

