﻿using System;
using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
//using System.Drawing;
using System.Linq;
//using System.Text;
using System.Windows.Forms;
using System.Reflection;
//using Microsoft.VisualBasic;

namespace TestFasiCollaudo
{
    public partial class Form1 : Form
    {
        
     
        public Collaudi02Entities enti;
        public T_Periferica PerifericaSelezionata;
        public List<SequenzaCollaudo> SequenzaColl;
        public TreeNode NodoFase;
        public BarraFasi.PhaseProgressBarItem fasew;

        //public T_Reply ReplyFunzione;


        public Type TipiPerif;
        public Type TipoTraccia;

        public Type ClassePeriferica;
        public MethodInfo[] MetodiClasse;
        public ParameterInfo[] ParametriMedotoDaChiamare;
        public MethodInfo MetodoDaChiamare;
        public FieldInfo[] aaa;
        public int RitornoMetodo;
        public object[] parametri;
        public object OggettiPerif;
        public object Oggettotrace;
        public SequenzaCollaudo Selez;



        public FieldInfo fase;
        public FieldInfo formpadre;
        public FieldInfo PicFront;
        public FieldInfo PicRear;
        public FieldInfo UltimoRitornoFunzione;
        public FieldInfo Freply;
        public FieldInfo StrRet;
        public FieldInfo Note;
        public FieldInfo T_Filetraccia;



        public Form1()
        {
            InitializeComponent();
            enti = new Collaudi02Entities();
            NodoFase = new TreeNode();
            NodoFase.Nodes.Add(" ");

        }

        private void Form1_Load(object sender, EventArgs e)
        {
           

            CPeriferica.DataSource = (from cc in enti.T_Periferica select cc);
            CPeriferica.DisplayMember = "Descrizione";
            

        }

        private void CPeriferica_SelectedIndexChanged(object sender, EventArgs e)
        {




            PerifericaSelezionata = (T_Periferica)CPeriferica.SelectedItem;

            SequenzaColl = (from cc in enti.SequenzaCollaudo
                                        .Include("T_Fasicollaudo")
                                        .Include("T_Periferica")
                            where cc.T_Periferica.ID_Periferica == PerifericaSelezionata.ID_Periferica
                            orderby cc.Contatore
                            select cc).ToList<SequenzaCollaudo>();

            treeView1.Nodes.Clear();

            foreach (SequenzaCollaudo item in SequenzaColl)
            {

                treeView1.Nodes.Add(item.Id_PassoCollaudo.ToString(), item.T_Fasicollaudo.Nome);
            }

        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
           
            Selez = SequenzaColl.Where(p => p.Id_PassoCollaudo == System.Convert.ToInt16(e.Node.Name)).FirstOrDefault();
            LNome.Text = Selez.T_Fasicollaudo.Nome;
            LDesc.Text = Selez.T_Fasicollaudo.Descrizione;
            LTent.Text = Selez.NTentativiPossibili.ToString();
            LIdFase.Text = Selez.T_Fasicollaudo.Id_fase.ToString();
            LNote.Text = Selez.T_Fasicollaudo.Note;
            LCommenti.Text = Selez.T_Fasicollaudo.Commenti;

            faseBar1.clearPolygons();
            faseBar1.FDraw = true;
            fasew = new BarraFasi.PhaseProgressBarItem();
            fasew.Text = Selez.T_Fasicollaudo.Id_fase+"-"+  Selez.T_Fasicollaudo.Descrizione;
            faseBar1.AddPhase(fasew);

            e.Node.Checked = true;
            this.Refresh();
        }

        public Boolean CaricaDll(String NomeFile)
        {
            string  str;
            
             String [] dir;
            Assembly AsseblyTmp;
            String NomiPerif;
        
      
            
            Type[] TypeTmp;
            Type tipo;
            string[] sss;
            Boolean ret = false;
           
            MethodInfo[] m;
           

            str = Application.StartupPath; 

        

            dir = System.IO.Directory.GetFiles(str,NomeFile);
            if (dir.Length == 0)
            {
                MessageBox.Show("Mancano i files TestFasiCollaudo", "Attenzione",MessageBoxButtons.OK, MessageBoxIcon.Error);
                ret = false;
            }
            else
            {
                sss = (NomeFile.Substring(NomeFile.LastIndexOf("TestFasiCollaudo") + "TestFasiCollaudo".Length)).Split('.');
                //' carico temporaneamente il file per cercare l'oggett
                //' Perifxxx che devo poi usare
                NomiPerif = sss[0];
                AsseblyTmp = Assembly.LoadFrom(NomeFile);
                TypeTmp = AsseblyTmp.GetTypes();
                tipo = TypeTmp[0];
                           
                TipiPerif = tipo;

               
                for (int ii  = 0 ; ii< TypeTmp.Count()-1;ii ++)
                {
                    if (TypeTmp[ii].Name.Contains("Perif"))
                    {
                        TipiPerif = TypeTmp[ii];                      
                    }

                    if (TypeTmp[ii].Name.Contains("trace"))
                    {
                        TipoTraccia = TypeTmp[ii];
                    }   
                }
             
                //' creo un'istanza dell' oggetto perif
                OggettiPerif = Activator.CreateInstance(TipiPerif);

                Oggettotrace = Activator.CreateInstance(TipoTraccia);
              //  ' chiamo il costruttore 

                m = TipiPerif.GetMethods();
                MetodoDaChiamare = TipiPerif.GetMethod("Perif" + NomiPerif);
                MetodoDaChiamare.Invoke(OggettiPerif, null);
                
               
           
              
                ret = true;
                }


            return ret;           
        }


  public void ImpostaMetodo(String Nome, String StrParametri)
 {
        int numparametriObbigatori ;
        MetodoDaChiamare = TipiPerif.GetMethod(Nome);

        parametri = null;

        ParametriMedotoDaChiamare = MetodoDaChiamare.GetParameters();
        //' se ci sono parametri devo compilare il vettore
        if( ParametriMedotoDaChiamare.Length > 0 )
        {
            String[] del = new String[2];
            del[0] = "Param:";
            parametri = new object[ParametriMedotoDaChiamare.Length ];
            String [] vettoreparametri = StrParametri.Split(del,StringSplitOptions.RemoveEmptyEntries);
            //'converto i valori nel vettore di oggetti
            //' 


            //' se la funzione che devo chiamare a parametri opzionali li gestisco
            numparametriObbigatori = ParametriMedotoDaChiamare.Length;
            for (int iii= 0;iii < ParametriMedotoDaChiamare.Length ;iii++)
            {
                if( ParametriMedotoDaChiamare[iii].IsOptional == true )
                {
                    numparametriObbigatori = numparametriObbigatori - 1;
                }
            }

            if (vettoreparametri.Length < numparametriObbigatori )
            {
               // ' errore o nel db o nella classe !!!!!
            }
            else
            {
               // 'For iii As Integer = 0 To ParametriMedotoDaChiamare.Length - 1
                for (int  iii = 0; iii < vettoreparametri.Length;iii++)
                {
                   // 'If ParametriMedotoDaChiamare(iii).IsOptional = False Then
                    switch (ParametriMedotoDaChiamare[iii].ParameterType.Name)
                    {
                        case "Int16":
                            parametri[iii] = System.Convert.ToInt16(vettoreparametri[iii]);
                            break;
                        case "Boolean":
                            parametri[iii] = System.Convert.ToBoolean(vettoreparametri[iii]);
                            break;
                        case "Byte":
                            parametri[iii] = System.Convert.ToByte(vettoreparametri[iii]);
                            break;
                        case "String":
                            parametri[iii] = System.Convert.ToString(vettoreparametri[iii]);
                            break;
                    }
                }
            }
        }
  }

  private void button1_Click(object sender, EventArgs e)
  {
      int RitornoMetodo;
      string NomeFile;
      string DescrizioneRitorno;
      NomeFile = "TestFasiCollaudo" + PerifericaSelezionata.Nome + ".dll";



      CaricaDll(NomeFile);

      //carico l'oggetto fase
      fase = TipiPerif.GetField("fase");
      fase.SetValue(OggettiPerif, (object)faseBar1.Phases[1]);

      // setto il formpadre
      formpadre = TipiPerif.GetField("formpadre");
      formpadre.SetValue(OggettiPerif, this);

      // imposto gli oggetti per l'immagine fronte e retro
      PicFront = TipiPerif.GetField("Pimage");
      PicFront.SetValue(OggettiPerif, pictureBox1);

      PicFront = TipiPerif.GetField("PImageBack");
      PicFront.SetValue(OggettiPerif, pictureBox2);

      // chiamo il metodo per impostare la configurazione della periferica
      MetodoDaChiamare = TipiPerif.GetMethod("ImpostaFlagPerConfigurazione");
      Object[] args = new object[1];

      //args[0] = new int[] { 2, 4, 8, 12,19,63,69,70, Selez.T_Periferica.ID_Periferica };
      //args[0] = new int[] { 2, 4, 8, 12, 19, 64, 69, 70, 81 , Selez.T_Periferica.ID_Periferica };
      //args[0] = new int[] { 2, 4, 8, 12, 19, 62, 69, 70, 81, Selez.T_Periferica.ID_Periferica };
      args[0] = new int[] { 2, 4, 8, 12, 19, 62, 69, Selez.T_Periferica.ID_Periferica };

      MetodoDaChiamare.Invoke(OggettiPerif, args);

      // imposto il metodo da chiamare: setto i parametri e li decodifico
      ImpostaMetodo(LNome.Text, Selez.T_Fasicollaudo.Note);
      
      RitornoMetodo = (int)MetodoDaChiamare.Invoke(OggettiPerif, parametri);

      //  prendo i valori dei campi della classe del reply della funzione 
      Freply = TipiPerif.GetField("Reply");
      
      // prendo i valori aggiornati dell' oggetto filetraccia
      T_Filetraccia = TipiPerif.GetField("FileTraccia");
      Oggettotrace = T_Filetraccia.GetValue(OggettiPerif);

      UltimoRitornoFunzione = TipoTraccia.GetField("UltimoRitornoFunzione");

      RitornoMetodo = (int)UltimoRitornoFunzione.GetValue(Oggettotrace);
          
      StrRet = TipoTraccia.GetField("Note");
      DescrizioneRitorno = (string)StrRet.GetValue(Oggettotrace);

      //ReplyFunzione = (from ee in enti.T_Reply
      //          where ((ee.FK_Classe == 1) &&
      //                  (ee.Valore == RitornoMetodo))
      //          select ee).FirstOrDefault();
      //if (ReplyFunzione != null)
      //  LResult.Text = ReplyFunzione.Descrizione;

      // visualizzo i risultati

      LNotel.Text = DescrizioneRitorno;

      LRet.Text = RitornoMetodo.ToString();
  }


    }
}
