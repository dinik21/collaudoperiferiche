Imports System.Windows
Imports System.Drawing
Public Class CheckCodeline
    '45
    Const CODELINE_E13B_45 As String = ":0=;<1234567890;1234567890;1"
    '46
    Const CODELINE_E13B_46 As String = ":1=;<1234567890;1234567890;1234567890;1234567890;123"
    '47
    Const CODELINE_E13B_47 As String = ":2=;<1234567890;1234567890;1234567890;1234567890;1234567890;1"
    '48
    Const CODELINE_CMC7_48 As String = ":3=;<1234567890;1234567890;1234567890;12345"
    '49
    Const CODELINE_CMC7_49 As String = ":4=;<1234567890;1234567890;1234567890;1234567890;123"
    '50
    Const CODELINE_CMC7_50 As String = ":5=;<1234567890;1234567890;1234567890;1234567890;1234567890;1"

    '63
    Const CODELINE_E13B_63 As String = ":72=;<1234567890;123"
    '66
    Const CODELINE_E13B_66 As String = ":75=;<1234567890;1234567890;1234567890;1234"
    '62
    Const CODELINE_E13B_62 As String = ":71=;<1234567890;1234567890;1234567890;1234567890;1234567890;1234"

    '64
    Const CODELINE_CMC7_64 As String = ":73=;<1234567890;123"
    '65
    Const CODELINE_CMC7_65 As String = ":74=;<1234567890;1234567890;1234567890;1234"
    '61
    Const CODELINE_CMC7_61 As String = ":70=;<1234567890;1234567890;1234567890;1234567890;1234567890;1234"


    Public Const CONTROLLO_LS515_TIMBRO As Integer = 3
    Public Const CONTROLLO_LS515_NO_TIMBRO As Integer = 4
    Public Const CONTROLLO_X_DOC_PER_TIPO As Integer = 2
    Public Const CONTROLLO_1 As Integer = 1
    Public Const CONTROLLO_0 As Integer = 0
    Public Const CONTROLLO_DOPPIA_DEF As Integer = 5
    Public Const CONTROLLO_DOPPIA_SOTTILE As Integer = 6
    Public Const CONTROLLO_DOPPIA_SPESSO As Integer = 7
    Public Const CONTROLLO_SMAG_CMC7 As Integer = 8
    Public Const CONTROLLO_SMAG_E13B As Integer = 9


    Public Tipo_controllo As Short


    Public E13B_45 As Short
    Public E13B_46 As Short
    Public E13B_47 As Short

    Public CMC7_48 As Short
    Public CMC7_49 As Short
    Public CMC7_50 As Short

    Public E13B_63 As Short
    Public E13B_66 As Short
    Public E13B_62 As Short

    Public CMC7_64 As Short
    Public CMC7_65 As Short
    Public CMC7_61 As Short

    Public Totale_E13B As Short
    Public Totale_CMC7 As Short

    Public DaLeggereE13B As Short
    Public DaLeggereCMC7 As Short


    Public F As CtsControls.Frisul
    Private Tipoletto As Short



    Public Function VisualizzaFinestra(ByVal ViewWindows As Integer) As Integer
        If ViewWindows = True Then
            F = New CtsControls.Frisul
            F.Reset()
            F.Show()
            F.BringToFront()
        Else
            F.Close()
        End If
    End Function
    Public Function CheckCodeline(ByVal Codeline As String) As Integer

        Dim CodelineConf As String = ""
        Dim Codel As String
        Dim ret As Integer




        ret = False
        ret = Codeline.LastIndexOf("!")
        If (ret > 0) Then
            Return -1
        End If

        If (ret = 0 Or Codeline.Length > 0) Then
            If ret = -1 Then
                ret = 0
            End If

            Select Case (Codeline.Chars(1))
                Case "0"
                    CodelineConf = CODELINE_E13B_45
                    Tipoletto = 45
                Case "1"
                    CodelineConf = CODELINE_E13B_46
                    Tipoletto = 46
                Case "2"
                    CodelineConf = CODELINE_E13B_47
                    Tipoletto = 47
                Case "3"
                    CodelineConf = CODELINE_CMC7_48
                    Tipoletto = 48
                Case "4"
                    CodelineConf = CODELINE_CMC7_49
                    Tipoletto = 49
                Case "5"
                    CodelineConf = CODELINE_CMC7_50
                    Tipoletto = 50
                    ' vado a controllare che tipo di DOCUTEST 6X ho letto
                Case "7"
                    Select Case (Codeline.Chars(2))
                        Case "0"
                            CodelineConf = CODELINE_CMC7_61
                            Tipoletto = 61
                        Case "1"
                            CodelineConf = CODELINE_E13B_62
                            Tipoletto = 62
                        Case "2"
                            CodelineConf = CODELINE_E13B_63
                            Tipoletto = 63
                        Case "3"
                            CodelineConf = CODELINE_CMC7_64
                            Tipoletto = 64
                        Case "4"
                            CodelineConf = CODELINE_CMC7_65
                            Tipoletto = 65
                        Case "5"
                            CodelineConf = CODELINE_E13B_66
                            Tipoletto = 66
                    End Select
                Case Else
                    CodelineConf = CODELINE_CMC7_49
                    Tipoletto = 1
            End Select
            Codel = Codeline.Trim(" ")
            If String.Compare(Codel, CodelineConf) = 0 Then

                Select Case Tipoletto
                    Case 45
                        E13B_45 += 1
                        Totale_E13B += 1
                    Case 46
                        E13B_46 += 1
                        Totale_E13B += 1
                    Case 47
                        E13B_47 += 1
                        Totale_E13B += 1
                    Case 48
                        CMC7_48 += 1
                        Totale_CMC7 += 1
                    Case 49
                        CMC7_49 += 1
                        Totale_CMC7 += 1
                    Case 50
                        CMC7_50 += 1
                        Totale_CMC7 += 1
                    Case 61
                        CMC7_61 += 1
                        Totale_CMC7 += 1
                    Case 62
                        E13B_62 += 1
                        Totale_E13B += 1
                    Case 63
                        E13B_63 += 1
                        Totale_E13B += 1
                    Case 64
                        CMC7_64 += 1
                        Totale_CMC7 += 1
                    Case 65
                        CMC7_65 += 1
                        Totale_CMC7 += 1
                    Case 66
                        E13B_66 += 1
                        Totale_E13B += 1
                End Select

                Select Case Tipo_controllo
                    Case CONTROLLO_X_DOC_PER_TIPO
                        If Tipoletto <> 45 And Tipoletto <> 46 _
                            And Tipoletto <> 47 And Tipoletto <> 48 _
                            And Tipoletto <> 49 And Tipoletto <> 50 _
                            Then
                            ret = -1
                        End If
                    Case CONTROLLO_LS515_NO_TIMBRO
                        If Tipoletto <> 63 And Tipoletto <> 64 _
                            And Tipoletto <> 46 And Tipoletto <> 48 _
                            And Tipoletto <> 61 And Tipoletto <> 62 Then
                            ret = -1
                        End If
                    Case CONTROLLO_LS515_TIMBRO
                        If Tipoletto <> 66 And Tipoletto <> 65 _
                          And Tipoletto <> 46 And Tipoletto <> 48 _
                          And Tipoletto <> 61 And Tipoletto <> 62 Then
                            ret = -1
                        End If
                    Case CONTROLLO_DOPPIA_DEF
                        If Tipoletto <> 46 And Tipoletto <> 48 And Tipoletto <> 49 Then
                            ret = -1
                        End If

                    Case CONTROLLO_DOPPIA_SOTTILE
                        If Tipoletto <> 47 Then
                            ret = -1
                        End If
                    Case CONTROLLO_DOPPIA_SPESSO
                        If Tipoletto <> 45 And Tipoletto <> 50 Then
                            ret = -1
                        End If
                    Case CONTROLLO_SMAG_CMC7
                        If Tipoletto <> 49 Then
                            ret = -1
                        End If
                    Case CONTROLLO_SMAG_E13B
                        If Tipoletto <> 46 Then
                            ret = -1
                        End If
                End Select

            Else
                ' codeline non del tipo docutest
                ret = 1
            End If
        Else
            ' errore codeline
            ret = -1
        End If


        Return ret
    End Function

    Public Function Conteggiacodeline() As Integer
        Dim ret As Short
        Dim Strview As String

        Strview = ""
        ' 0 - controllo di avere passato solo un certo numero di assegni senza guardare quali ho passato
        ' 1 - controllo di avere passato solo degli assegni cmc7 e degli assegni e13b  _
        '     distingua solo tra CMC7 e E13B
        ' 2 - controllo di avere passato un num di assegni per ogni tipo
        ' 3 - controllo di aver passato num doc per versione di ls515 con timbro
        '       - 14 66
        '       - 14 65
        '       - 14 46
        '       - 14 48
        '       - 14 61
        '       - 14 62
        ' 4 - controllo di aver passato num doc per versione di ls515 Senza timbro
        '       - 14 63
        '       - 14 64
        '       - 14 46
        '       - 14 48
        '       - 14 61
        '       - 14 62
        ' 5 - controllo con documenti grammatura standard
        '       - 14 o 7 xx
        '       - 14 o 7 xx
        ' 6 - controllo con documenti grammatura sottile
        '       - 14 o 7 xx
        '       - 14 o 7 xx
        ' 7 - controllo con documenti grammatura spessa
        '       - 14 o 7 xx
        '       - 14 o 7 xx
        Select Case Tipo_controllo
            Case CONTROLLO_0

                If (E13B_45 >= DaLeggereE13B + DaLeggereCMC7) Or _
                (E13B_46 >= DaLeggereE13B + DaLeggereCMC7) Or _
                (E13B_47 >= DaLeggereE13B + DaLeggereCMC7) Or _
                (CMC7_48 >= DaLeggereE13B + DaLeggereCMC7) Or _
                (CMC7_49 >= DaLeggereE13B + DaLeggereCMC7) Or _
                (CMC7_50 >= DaLeggereE13B + DaLeggereCMC7) Then
                    ret = 1
                Else
                    ret = 0
                End If
            Case CONTROLLO_1

                If ((E13B_45 >= DaLeggereE13B) And _
             (E13B_46 >= DaLeggereE13B) And _
             (E13B_47 >= DaLeggereE13B)) Or _
             ((CMC7_48 >= DaLeggereCMC7) And _
             (CMC7_49 >= DaLeggereCMC7) And _
             (CMC7_50 >= DaLeggereCMC7)) Then
                    ret = 1
                Else
                    ret = 0
                End If
            Case CONTROLLO_X_DOC_PER_TIPO
                If ((E13B_45 >= DaLeggereE13B) And _
                    (E13B_46 >= DaLeggereE13B) And _
                    (E13B_47 >= DaLeggereE13B)) And _
                    ((CMC7_48 >= DaLeggereCMC7) And _
                    (CMC7_49 >= DaLeggereCMC7) And _
                    (CMC7_50 >= DaLeggereCMC7)) Then
                    ret = 1
                Else
                    ret = 0
                End If
            Case CONTROLLO_LS515_TIMBRO
                If ((E13B_66 >= DaLeggereE13B) And _
                    (E13B_46 >= DaLeggereE13B) And _
                    (E13B_62 >= DaLeggereE13B)) And _
                    ((CMC7_48 >= DaLeggereCMC7) And _
                     (CMC7_65 >= DaLeggereCMC7) And _
                     (CMC7_61 >= DaLeggereCMC7)) Then
                    ret = 1
                Else
                    ret = 0
                End If
            Case CONTROLLO_LS515_NO_TIMBRO
                If ((E13B_63 >= DaLeggereE13B) And _
                    (E13B_46 >= DaLeggereE13B) And _
                    (E13B_62 >= DaLeggereE13B)) And _
                    ((CMC7_64 >= DaLeggereCMC7) And _
                    (CMC7_48 >= DaLeggereCMC7) And _
                    (CMC7_61 >= DaLeggereCMC7)) Then
                    ret = 1
                Else
                    ret = 0
                End If

            Case CONTROLLO_DOPPIA_DEF
                If ((E13B_46 >= DaLeggereE13B) And _
                    (CMC7_48 >= DaLeggereCMC7) And _
                    (CMC7_49 >= DaLeggereCMC7)) Then
                    ret = 1
                Else
                    ret = 0
                End If

            Case CONTROLLO_DOPPIA_SOTTILE
                If ((E13B_47 >= DaLeggereE13B)) Then
                    ret = 1
                Else
                    ret = 0
                End If

            Case CONTROLLO_DOPPIA_SPESSO
                If ((E13B_45 >= DaLeggereE13B) And _
                    (CMC7_50 >= DaLeggereCMC7)) Then
                    ret = 1
                Else
                    ret = 0
                End If


        End Select

        If Not F Is Nothing Then
            Select Case Tipo_controllo
                Case CONTROLLO_0
                    If (E13B_45 >= DaLeggereE13B + DaLeggereCMC7) Then
                        F.L1.ForeColor = Color.Green
                    End If
                    If (E13B_46 >= DaLeggereE13B + DaLeggereCMC7) Then
                        F.L2.ForeColor = Color.Green
                    End If
                    If (E13B_47 >= DaLeggereE13B + DaLeggereCMC7) Then
                        F.L3.ForeColor = Color.Green
                    End If
                    If (CMC7_48 >= DaLeggereE13B + DaLeggereCMC7) Then
                        F.L4.ForeColor = Color.Green
                    End If
                    If (CMC7_49 >= DaLeggereE13B + DaLeggereCMC7) Then
                        F.L5.ForeColor = Color.Green
                    End If
                    If (CMC7_50 >= DaLeggereE13B + DaLeggereCMC7) Then
                        F.L6.ForeColor = Color.Green
                    End If
                Case CONTROLLO_1
                Case CONTROLLO_X_DOC_PER_TIPO
                    If (E13B_45 >= DaLeggereE13B) Then
                        F.L1.ForeColor = Color.Green
                    End If
                    If (E13B_46 >= DaLeggereE13B) Then
                        F.L2.ForeColor = Color.Green
                    End If
                    If (E13B_47 >= DaLeggereE13B) Then
                        F.L3.ForeColor = Color.Green
                    End If

                    If (CMC7_48 >= DaLeggereCMC7) Then
                        F.L4.ForeColor = Color.Green
                    End If
                    If (CMC7_49 >= DaLeggereCMC7) Then
                        F.L5.ForeColor = Color.Green
                    End If
                    If (CMC7_50 >= DaLeggereCMC7) Then
                        F.L6.ForeColor = Color.Green
                    End If
                    Strview = "Letti DOCUTEST 045 n� " + E13B_45.ToString + " di " + DaLeggereE13B.ToString + vbCrLf
                    F.L1.Text = Strview
                    Strview = "Letti DOCUTEST 046 n� " + E13B_46.ToString + " di " + DaLeggereE13B.ToString + vbCrLf
                    F.L2.Text = Strview
                    Strview = "Letti DOCUTEST 047 n� " + E13B_47.ToString + " di " + DaLeggereE13B.ToString + vbCrLf
                    F.L3.Text = Strview
                    Strview = "Letti DOCUTEST 048 n� " + CMC7_48.ToString + " di " + DaLeggereCMC7.ToString + vbCrLf
                    F.L4.Text = Strview
                    Strview = "Letti DOCUTEST 049 n� " + CMC7_49.ToString + " di " + DaLeggereCMC7.ToString + vbCrLf
                    F.L5.Text = Strview
                    Strview = "Letti DOCUTEST 050 n� " + CMC7_50.ToString + " di " + DaLeggereCMC7.ToString + vbCrLf
                    F.L6.Text = Strview


                Case CONTROLLO_LS515_TIMBRO
                    If (E13B_66 >= DaLeggereE13B) Then
                        F.L1.ForeColor = Color.Green
                    End If
                    If (E13B_46 >= DaLeggereE13B) Then
                        F.L2.ForeColor = Color.Green
                    End If
                    If (E13B_62 >= DaLeggereE13B) Then
                        F.L3.ForeColor = Color.Green
                    End If

                    If (CMC7_48 >= DaLeggereCMC7) Then
                        F.L4.ForeColor = Color.Green
                    End If
                    If (CMC7_65 >= DaLeggereCMC7) Then
                        F.L5.ForeColor = Color.Green
                    End If
                    If (CMC7_61 >= DaLeggereCMC7) Then
                        F.L6.ForeColor = Color.Green
                    End If

                    Strview = "Letti DOCUTEST 066 n� " + E13B_66.ToString + " di " + DaLeggereE13B.ToString + vbCrLf
                    F.L1.Text = Strview
                    Strview = "Letti DOCUTEST 046 n� " + E13B_46.ToString + " di " + DaLeggereE13B.ToString + vbCrLf
                    F.L2.Text = Strview
                    Strview = "Letti DOCUTEST 062 n� " + E13B_62.ToString + " di " + DaLeggereE13B.ToString + vbCrLf
                    F.L3.Text = Strview
                    Strview = "Letti DOCUTEST 048 n� " + CMC7_48.ToString + " di " + DaLeggereCMC7.ToString + vbCrLf
                    F.L4.Text = Strview
                    Strview = "Letti DOCUTEST 065 n� " + CMC7_65.ToString + " di " + DaLeggereCMC7.ToString + vbCrLf
                    F.L5.Text = Strview
                    Strview = "Letti DOCUTEST 061 n� " + CMC7_61.ToString + " di " + DaLeggereCMC7.ToString + vbCrLf
                    F.L6.Text = Strview



                Case CONTROLLO_LS515_NO_TIMBRO
                    If (E13B_63 >= DaLeggereE13B) Then
                        F.L1.ForeColor = Color.Green
                    End If
                    If (E13B_46 >= DaLeggereE13B) Then
                        F.L2.ForeColor = Color.Green
                    End If
                    If (E13B_62 >= DaLeggereE13B) Then
                        F.L3.ForeColor = Color.Green
                    End If
                    If (CMC7_48 >= DaLeggereCMC7) Then
                        F.L4.ForeColor = Color.Green
                    End If
                    If (CMC7_64 >= DaLeggereCMC7) Then
                        F.L5.ForeColor = Color.Green
                    End If
                    If (CMC7_61 >= DaLeggereCMC7) Then
                        F.L6.ForeColor = Color.Green
                    End If

                    Strview = "Letti DOCUTEST 063 n� " + E13B_63.ToString + " di " + DaLeggereE13B.ToString + vbCrLf
                    F.L1.Text = Strview
                    Strview = "Letti DOCUTEST 046 n� " + E13B_46.ToString + " di " + DaLeggereE13B.ToString + vbCrLf
                    F.L2.Text = Strview
                    Strview = "Letti DOCUTEST 062 n� " + E13B_62.ToString + " di " + DaLeggereE13B.ToString + vbCrLf
                    F.L3.Text = Strview
                    Strview = "Letti DOCUTEST 048 n� " + CMC7_48.ToString + " di " + DaLeggereCMC7.ToString + vbCrLf
                    F.L4.Text = Strview
                    Strview = "Letti DOCUTEST 064 n� " + CMC7_64.ToString + " di " + DaLeggereCMC7.ToString + vbCrLf
                    F.L5.Text = Strview
                    Strview = "Letti DOCUTEST 061 n� " + CMC7_61.ToString + " di " + DaLeggereCMC7.ToString + vbCrLf
                    F.L6.Text = Strview

                Case CONTROLLO_DOPPIA_DEF
                    If (CMC7_48 >= DaLeggereCMC7) Then
                        F.L1.ForeColor = Color.Green
                    End If
                    If (CMC7_49 >= DaLeggereCMC7) Then
                        F.L2.ForeColor = Color.Green
                    End If
                    If (E13B_46 >= DaLeggereE13B) Then
                        F.L3.ForeColor = Color.Green
                    End If

                    Strview = "Letti DOCUTEST 048 n� " + CMC7_48.ToString + " di " + DaLeggereE13B.ToString + vbCrLf
                    F.L1.Text = Strview
                    Strview = "Letti DOCUTEST 049 n� " + CMC7_49.ToString + " di " + DaLeggereE13B.ToString + vbCrLf
                    F.L2.Text = Strview
                    Strview = "Letti DOCUTEST 046 n� " + E13B_46.ToString + " di " + DaLeggereE13B.ToString + vbCrLf
                    F.L3.Text = Strview
                    F.L4.Text = ""
                    F.L5.Text = ""
                    F.L6.Text = ""

                Case CONTROLLO_DOPPIA_SOTTILE
                    If (E13B_47 >= DaLeggereE13B) Then
                        F.L1.ForeColor = Color.Green
                    End If
                    
                    Strview = "Letti DOCUTEST 047 n� " + E13B_47.ToString + " di " + DaLeggereE13B.ToString + vbCrLf
                    F.L1.Text = Strview
                    F.L2.Text = ""
                    F.L3.Text = ""
                    F.L4.Text = ""
                    F.L5.Text = ""
                    F.L6.Text = ""

                Case CONTROLLO_DOPPIA_SPESSO
                    If (E13B_45 >= DaLeggereE13B) Then
                        F.L1.ForeColor = Color.Green
                    End If
                    If (CMC7_50 >= DaLeggereCMC7) Then
                        F.L2.ForeColor = Color.Green
                    End If

                    Strview = "Letti DOCUTEST 045 n� " + E13B_45.ToString + " di " + DaLeggereE13B.ToString + vbCrLf
                    F.L1.Text = Strview
                    Strview = "Letti DOCUTEST 050 n� " + CMC7_50.ToString + " di " + DaLeggereE13B.ToString + vbCrLf
                    F.L2.Text = Strview
                    F.L3.Text = ""
                    F.L4.Text = ""
                    F.L5.Text = ""
                    F.L6.Text = ""
            End Select


        End If
        Return ret
    End Function
    Public Sub Reset()
        Me.E13B_45 = 0
        Me.E13B_46 = 0
        Me.E13B_47 = 0
        Me.E13B_63 = 0
        Me.E13B_66 = 0
        Me.E13B_62 = 0
        Me.CMC7_48 = 0
        Me.CMC7_49 = 0
        Me.CMC7_50 = 0
        Me.CMC7_64 = 0
        Me.CMC7_65 = 0
        Me.CMC7_61 = 0
        Me.Totale_CMC7 = 0
        Me.Totale_E13B = 0
    End Sub
End Class
