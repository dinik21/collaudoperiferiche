﻿Imports System.Windows.Forms
Imports System.Drawing
Imports CtsControls
'Imports ReportPrinting
Imports System.Xml
Imports System.Xml.XPath
Imports System.IO

Public Class PerifLs100
    Public Const COLLAUDO_PIASTRA_ls100 = 1
    Public Const COLLAUDO_PERIFERICA_ls100 = 1
    Public Const LUNGHEZZA_SERIAL_NUMBER = 12
    Public Const LUNGHEZZA_SERIAL_NUMBER_PIASTRA = 16

    Public Const SW_TOP_IMAGE = 1 '(bit1)
    Public Const SW_BAR2D = 2     '(bit2)
    Public Const SW_TOP_BAR2D = 3 '(bit1+2)
    Public Const SW_IQA = 4
    Public Const SW_MICROHOLE = 8
    Public Const SW_BARCODE_MICROHOLE = 10
    Public Const SW_BARCODE_MICROHOLE_IQA = 14


    Public Const PATHFRONTEQ As String = "FrontImage"
    Public Const PATHRETROQ As String = "BackImage"

    Public Const VERSIONE As String = "C.T.S. TestFasiCollaudoLs100.dll -- Ver. 1.00 Rev 008 date 14/12/2010"

    Public Const LOCALDB As String = "\LocalDB"
    Public Const MESSAGGI As String = "\Messaggi"


    Dim ls100 As Ls100Class.Periferica
    Dim fquality As Boolean
    Dim BVisualZoom As Boolean


    Dim IndImgF As Integer
    Dim IndImgR As Integer
    Dim fWaitButton As Boolean
    Dim zzz As String
    Dim FileZoomCorrente As String
    Dim StrMessaggio As String
    Dim VetImg(20) As Image
    Dim ff As CtsControls.Message
    Dim ff2 As CtsControls.Message2

    Public Hstorico As CStorico
    Public FileTraccia As New trace
    Public fase As Object 'BarraFasi.PhaseProgressBarItem
    Public DirZoom As String
    Public StrCommento As String
    Public Pimage As PictureBox
    Public PFronte As PictureBox
    Public PRetro As PictureBox
    Public PImageBack As PictureBox
    Public TipoDiCollaudo As Integer
    Public LettureCMC7 As Integer
    Public LettureE13B As Integer
    Public chcd As CheckCodeline
    Public TipoComtrolloLetture As Integer
    Public formpadre As Windows.Forms.Form
    Public FLed As Led
    Public Fqualit As FqualImg
    Public BIdentificativo As Boolean
    'Public report As ReportPrinting.ReportDocument
    Public fSoftware As Integer
    Public fcardCol As Integer

    ' indicano le versioni di firmware dell ordine corrente
    Public Ver_firm1 As String
    Public Ver_firm2 As String
    Public Ver_firm3 As String
    Public Ver_firm4 As String
    Public Ver_firm5 As String
    Public Ver_firm6 As String
    Public Ver_firm7 As String
    Public Ver_firm8 As String
    Public Ver_firm9 As String
    ' indicano la configurazione della periferica
    Public fTestinaMicr As Boolean
    Public fTestinaCmc7 As Boolean
    Public fTestinaE13b As Boolean
    Public fTimbri As Boolean
    Public fScannerFronte As Boolean
    Public fScannerRetro As Boolean
    Public fStampante As Boolean
    Public fSfogliatore As Boolean
    Public fBadge As Integer ' 0 1 2 3 
    Public fInkDetector As Boolean

    Public MatricolaPiastra As String
    ' il serial number inserito a mano della piastra
    Public SerialNumberPiastra As String
    ' il serial number inserito a mano della PERIFERICA
    Public SerialNumberDaStampare As String
    Public MatricolaPeriferica As String
    Private VettoreOpzioni As Array

    Dim TipodiPeriferica As Integer

    Public VersioneTest As String
    Public VersioneDll_1 As String
    Public VersioneDll_0 As String

    Public PathMessaggi As String

    Dim Titolo, Messaggio, Descrizione, IdMessaggio As String
    Dim dres As DialogResult
    Dim immamsg As Image
    Dim idmsg As Integer
    Public StrUsb As String
    Dim Riprova As Short

    Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
       "GetPrivateProfileStringA" (ByVal lpApplicationName As String, _
           ByVal lpKeyName As String, _
           ByVal lpDefault As String, _
           ByVal lpReturnedString As String, _
           ByVal nSize As Integer, ByVal lpFileName As String) As Integer

    Public Sub FaseOk()
        fase.SetState = 1
        fase.IsBlinking = True
        fase.IsSelected = True
    End Sub
    Public Sub FaseKo()
        fase.SetState = 0
        fase.IsBlinking = True
        fase.IsSelected = True

    End Sub
    Public Sub FaseEsec()
        FileTraccia.Note = ""
        fase.SetState = 2
        fase.IsBlinking = True
        fase.IsSelected = True
    End Sub





    Public Sub PerifLs100()
        Dim sss As String
        Dim ind As Integer

        ls100 = New Ls100Class.Periferica
        Hstorico = New CStorico
        Pimage = New PictureBox
        PFronte = New PictureBox
        PRetro = New PictureBox
        PImageBack = New PictureBox
        sss = Application.StartupPath()
        ind = sss.LastIndexOf("\")
        '        zzz = sss.Substring(0, ind)
        zzz = sss

        'report = New ReportPrinting.ReportDocument
        BIdentificativo = False

        fTestinaMicr = False
        fTestinaCmc7 = False
        fTestinaE13b = False
        fTimbri = False
        fScannerFronte = False
        fScannerRetro = False
        fStampante = False
        fSfogliatore = False
        fBadge = 0 ' 0 1 2 3 


        VersioneTest = VERSIONE

        VersioneDll_0 = ls100.VersioneDll_0
        VersioneDll_1 = ls100.VersioneDll_1


        ' imposto una periferica che non esiste....
        TipodiPeriferica = 0

        Titolo = ""
        Messaggio = ""
        Descrizione = ""
        IdMessaggio = ""

        StrUsb = "3231"
    End Sub

    Public Sub ImpostaFlagPerConfigurazione(ByVal Vettore As Array)
        Dim ii As Short
        VettoreOpzioni = Vettore

        fTestinaMicr = False
        fTestinaCmc7 = False
        fTestinaE13b = False
        fTimbri = False
        fScannerFronte = False
        fScannerRetro = False
        fStampante = False
        fSfogliatore = False
        fBadge = 0 ' 0 1 2 3 
        fInkDetector = False
        Riprova = 0

        ' Controllo le opzioni che vanno da 0 a length -2
        ' perchè in ultim posizione trovo la Periferica
        For ii = 0 To VettoreOpzioni.Length - 2
            Select Case VettoreOpzioni(ii)
                Case 2, 4
                    fSfogliatore = True
                Case 6
                    fTestinaMicr = True
                    fTestinaCmc7 = True
                Case 7
                    fTestinaMicr = True
                    fTestinaE13b = True
                Case 8
                    fTestinaMicr = True
                Case 14
                    fTimbri = True
                Case 10
                    fScannerFronte = True
                Case 11
                    fScannerRetro = True
                Case 12
                    fScannerFronte = True
                    fScannerRetro = True
                Case 18
                    fStampante = True
                Case 22
                    fBadge = 1
                Case 23
                    fBadge = 2
                Case 24
                    fBadge = 3
                Case 52
                    fBadge = 3
                Case 67
                    fSoftware = SW_TOP_IMAGE
                Case 68
                    fSoftware = SW_BAR2D
                Case 69
                    fSoftware = SW_TOP_BAR2D
                Case 70
                    fcardCol = 1
                    'AGGIUNTE 30-09-2014
                Case 76
                    fSoftware = SW_IQA
                Case 77
                    fSoftware = SW_MICROHOLE
                Case 78
                    fSoftware = SW_BARCODE_MICROHOLE
                    '19-02-2015
                Case 79
                    fSoftware = SW_BARCODE_MICROHOLE_IQA
                    fcardCol = 1
                Case 80
                    fSoftware = SW_BARCODE_MICROHOLE_IQA
                Case 83
                    fInkDetector = True
            End Select
        Next
        TipodiPeriferica = VettoreOpzioni(VettoreOpzioni.Length - 1)

        PathMessaggi = Application.StartupPath + LOCALDB + MESSAGGI + TipodiPeriferica.ToString + ".xml"
    End Sub

    Public Sub CaricaRes()
        Try
            Dim ii As Integer
            Dim rsxr As Resources.ResXResourceReader
            Dim a As Resources.ResourceSet


            rsxr = New Resources.ResXResourceReader(Application.StartupPath + "\img2.resx")
            a = New Resources.ResourceSet(rsxr)

            Dim en As IDictionaryEnumerator = a.GetEnumerator()

            ' Goes through the enumerator, printing out the key and value pairs.

            ' finestra di massaggio in esecuzione

            ii = 0
            While en.MoveNext()
                Select Case en.Key.ToString
                    Case "pul2.jpg"
                        VetImg(0) = en.Value
                    Case "scanf.JPG"
                        VetImg(1) = en.Value
                    Case "pul1.jpg"
                        VetImg(2) = en.Value
                    Case "dp.JPG"
                        VetImg(3) = en.Value
                    Case "scanr.JPG"
                        VetImg(4) = en.Value
                    Case "badge.JPG"
                        VetImg(5) = en.Value
                    Case "cmicr.JPG"
                        VetImg(6) = en.Value
                    Case "qualim.JPG"
                        VetImg(7) = en.Value
                    Case "micrb.JPG"
                        VetImg(8) = en.Value
                    Case "qualimr.jpg"
                        VetImg(9) = en.Value
                    Case "docu055.JPG"
                        VetImg(10) = en.Value
                End Select
                VetImg(8) = VetImg(6)
                VetImg(4) = VetImg(0)

                'VetImg(ii) = en.Value

                'ii += 1
                'Me.Refresh()
            End While
            rsxr.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.InnerException.ToString)
        End Try

    End Sub


    Public Sub Sleep(ByVal sec As Integer)
        Dim iniTime, currTime As DateTime
        iniTime = DateTime.Now
        While (True)
            currTime = DateTime.Now
            Dim diff2 As System.TimeSpan
            ' diff2 gets 55 days 4 hours and 20 minutes.
            diff2 = System.DateTime.op_Subtraction(currTime, iniTime)

            If (diff2.Seconds = sec) Then
                Return
            End If
        End While
    End Sub

    Public Sub DisposeImg()
        If Not Pimage.Image Is Nothing Then
            Pimage.Image.Dispose()
            Pimage.Image = Nothing
        End If
        If Not PFronte.Image Is Nothing Then
            PFronte.Image.Dispose()
            PFronte.Image = Nothing
        End If
        If Not PRetro.Image Is Nothing Then
            PRetro.Image.Dispose()
            PRetro.Image = Nothing
        End If
        If Not PImageBack.Image Is Nothing Then
            PImageBack.Image.Dispose()
            PImageBack.Image = Nothing
        End If
    End Sub


    Public Sub ViewError(ByVal p As Object) 'BarraFasi.PhaseProgressBarItem)
        Dim sss As String
        Dim costante As New Ls100Class.Costanti
        sss = ""
        sss = costante.GetDesc(ls100.ReturnC.ReturnCode)
        MessageboxMy(ls100.ReturnC.FunzioneChiamante + " " + ls100.ReturnC.ReturnCode.ToString + "--> " + sss, "Attenzione !!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
    End Sub




    Public Function ShowZoom(ByVal dir As String, ByVal file As String, Optional ByVal VisualizzaZomm As Boolean = True) As DialogResult
        ' visualizzo subito lo zoom dell immagine

        Dim f As New CtsControls.ZoomImage
        f.FileCorrente = file
        f.DirectoryImage = dir
        f.effettuazoom = VisualizzaZomm
        f.ShowDialog()
        Return f.Result
    End Function

    Public Function MessageboxMy(ByVal testo As String, ByVal titolo As String, ByVal bt As MessageBoxButtons, ByVal ic As MessageBoxIcon, ByVal Img As Image) As DialogResult

        ff = New CtsControls.Message
        ff.Text = titolo
        ff.testo = testo
        ff.icona = ic
        ff.pulsanti = bt
        ff.Imag.Image = Img
        'If BVisualZoom = True Then
        '    ff.Bzoom.Visible = True
        '    ff.Directory = DirZoom
        '    ff.FileCorrente = FileZoomCorrente
        'End If


        ff.ShowDialog(formpadre)
        'If ff.fine = True Then
        '    ii = Tentativi
        '    ret = False
        'End If
        formpadre.Refresh()
        Return ff.result

    End Function
    Public Function MessageboxMy(ByVal testo As String, ByVal titolo As String, ByVal bt As MessageBoxButtons, ByVal ic As MessageBoxIcon, ByVal Img As Image, ByVal Id As String, ByVal Img2 As Image) As DialogResult

        ff2 = New CtsControls.Message2
        ff2.Text = titolo
        ff2.testo = testo
        ff2.icona = ic
        ff2.pulsanti = bt
        ff2.Imag.Image = Img
        ff2.LId.Text = Id
        ff2.PUser.Image = Img2

        'If BVisualZoom = True Then
        '    ff.Bzoom.Visible = True
        '    ff.Directory = DirZoom
        '    ff.FileCorrente = FileZoomCorrente
        'End If


        ff2.ShowDialog(formpadre)
        'If ff.fine = True Then
        '    ii = Tentativi
        '    ret = False
        'End If
        formpadre.Refresh()
        Return ff2.result

    End Function
    Public Function InputboxMy(ByVal testo As String, ByVal titolo As String, ByVal Check As Boolean, ByRef valore As Integer) As DialogResult
        Dim ff As New InputBox

        ff.Check = Check

        ff.Text = titolo
        ff.Messaggio = testo
        ff.ShowDialog(formpadre)
        valore = ff.Valore


        Return ff.result
    End Function


#Region "FUNZIONI TEST"


    Public Function ControllaVersione() As Integer
        Dim ret As Integer

        'Dim ch As Char()
        'Dim ch2 As Char()
        ret = 0


        'controllo che la versione sia uguale a quella scritta nell ordine
        ' ch = ls100.SBoardNr.ToCharArray
        'ch2 = ord.Board.ToCharArray
        If (ls100.SVersion.Length <= 0 Or String.Compare(ls100.SVersion.TrimEnd, Ver_firm1) = 0) Then
            If (ls100.SDateFW.Length <= 0 Or String.Compare(ls100.SDateFW.TrimEnd, Ver_firm2) = 0) Then
                If (ls100.SBoard = Ver_firm3) Then
                    If (ls100.SFPGANr.Length <= 0 Or String.Compare(ls100.SFPGANr.TrimEnd, Ver_firm4) = 0) Then
                        'If (ls100.SLema.Length <= 0 Or String.Compare(ls100.SLema.TrimEnd, Ver_firm5) = 0) Then
                        'Else
                        '    ret = Ls100Class.Costanti.S_LS100_FIRMWARE_NOT_OK

                        '    idmsg = 49
                        '    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        '        dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        '    Else
                        '        dres = MessageboxMy("Versione piastra OCR non corrispondente", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                        '    End If

                        'End If
                    Else
                        'errore fpga
                        ret = Ls100Class.Costanti.S_LS100_FPGA_NOT_OK
                        idmsg = 50
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("Versione FPGA non corrispondente", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                        End If

                    End If
                Else
                    ' errore board
                    ret = Ls100Class.Costanti.S_LS100_BOARD_NOT_OK
                    idmsg = 53
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy("Versione BOARD non corrispondente", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                    End If


                End If

            Else
                ' errore data
                ret = Ls100Class.Costanti.S_LS100_DATA_NOT_OK
                idmsg = 52
                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                    dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                Else
                    dres = MessageboxMy("Data Versione non corrispondente", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                End If

            End If
        Else
            ' errore versione
            ret = Ls100Class.Costanti.S_LS100_FIRMWARE_NOT_OK
            idmsg = 51
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Versione Firmware non corrispondente", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
            End If

        End If

        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ret
    End Function

    Public Function TestCheckVersion() As Integer
        Dim ritorno As Integer
        Dim ret As Integer

        ritorno = False
        FaseEsec()

        If BIdentificativo = True Then
            If ls100.SSerialNumber.Contains("OK") = True Then
                ' controllo che la versione sia quella dell ordine
                ret = ControllaVersione()
                If ret = 0 Then
                    ritorno = True
                    FileTraccia.Note = ""

                    FileTraccia.Note = "FwBase =" + Ver_firm1.ToString() + ", DataFW =" + Ver_firm2.ToString() + vbCrLf
                    FileTraccia.Note += "FPGA=" + Ver_firm3.ToString() + ", "
                    FileTraccia.Note += "Board =" + Ver_firm4.ToString + vbCrLf
                    If (Ver_firm9 <> "") Then
                        FileTraccia.Note += "Suite =" + Ver_firm9.ToString() + vbCrLf
                    End If
                Else
                    ritorno = False
                End If
            Else
                ' piastra NON Collaudata
                idmsg = 54
                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                    dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                Else
                    dres = MessageboxMy("Attenzione, possibile Piastra NON collaudata", "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                End If

                ritorno = False
            End If
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret

        Return ritorno
    End Function

    Public Function SetConfigurazione()
        Dim ret As Short
        'Dim str As String
        'Dim str2 As String
        'Dim vstr() As String
        'Dim vstr2() As String
        'Dim lastindex As Short
        Dim ritorno As Integer
        Dim strBlank As String
        Dim conf1, conf2, conf3, conf4, conf5 As Integer
        conf1 = conf2 = conf3 = conf4 = conf5 = 0

        ritorno = False


        ret = True
        strBlank = "          "

        '
        ' DA SCOMMENTARE 
        '

        If (fStampante = True) Then
            If (ls100.SInkJet.Length <= 0 Or String.Compare(ls100.SInkJet, strBlank) = 0) Then
                ret = False
                StrCommento += "Ink Jet non presente"
            End If
        End If
        'If (focr) Then
        '    If (ls100.SLema.Length <= 0 Or String.Compare(ls100.SLema, strBlank) = 0) Then
        '        ret = False
        '        StrCommento += "Ocr non presente"
        '    End If
        'End If

        Dim TestinaCmc7 As Integer
        Dim TestinaE13B As Integer
        Dim Timbri As Integer
        Dim ScannerFronte As Integer
        Dim ScannerRetro As Integer
        Dim Stampante As Integer
        Dim Sfogliatore As Integer

        Dim Badge1 As Integer ' 0 1 2 3 
        Dim Badge2 As Integer ' 0 1 2 3 
        Dim Badge3 As Integer ' 0 1 2 3 
        Dim InkDetector As Integer

        ritorno = False
        FaseEsec()
        Badge1 = Badge2 = Badge3 = 0

        If fTestinaCmc7 = True Or fTestinaMicr Then
            TestinaCmc7 = 1
        Else
            TestinaCmc7 = 0
        End If
        If fTestinaE13b = True Or fTestinaMicr Then
            TestinaE13B = 1
        Else
            TestinaE13B = 0
        End If

        If fTimbri = True Then
            Timbri = 1
        Else
            Timbri = 0
        End If
        'scanner 
        If fScannerFronte = True Then
            ScannerFronte = 1
        Else
            ScannerFronte = 0
        End If
        If fScannerRetro = True Then
            ScannerRetro = 1
        Else
            ScannerRetro = 0
        End If
        'stampante
        If fStampante = True Then
            Stampante = 1
        Else
            Stampante = 0
        End If
        If fSfogliatore = True Then
            Sfogliatore = 1
        Else
            Sfogliatore = 0
        End If


        Select Case fBadge
            Case 1
                Badge1 = 1
                Badge2 = 0
                Badge3 = 0
            Case 2
                Badge1 = 0
                Badge2 = 1
                Badge3 = 0
            Case 3
                Badge1 = 0
                Badge2 = 0
                Badge3 = 1
        End Select

        If fInkDetector = True Then
            InkDetector = 1
        Else
            InkDetector = 0
        End If

        ret = ls100.SetConfigurazione(TestinaE13B, TestinaCmc7, Timbri, Badge1, Badge2, Badge3, ScannerFronte, ScannerRetro, Sfogliatore, fSoftware, InkDetector)
        If ret = Ls100Class.Costanti.S_LS_OKAY Then
            ritorno = True
        Else
            ' imposto gli errori
            FileTraccia.UltimoRitornoFunzione = ret
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function

    Public Function TestIdentificativoSeriale() As Integer
        Dim ritorno As Integer
        Dim com As String
        ritorno = False
        Dim str As String
        str = New String(Chr(0), 50)
        Dim str2 As String
        str2 = New String(Chr(0), 50)
        Dim str3 As String
        str3 = New String(Chr(0), 50)
        Dim str4 As String
        str4 = New String(Chr(0), 50)
        Dim str5 As String
        str5 = New String(Chr(0), 50)
        Dim str6 As String
        str6 = New String(Chr(0), 50)

        TestIdentificativoSeriale = 0
        com = "COM1"
        Dim testok As Boolean
        'If MCom1.Checked = True Then
        '    com = "COM1"
        'End If
        'If MCom2.Checked = True Then
        '    com = "COM2"
        'End If
        'If MCom3.Checked = True Then
        '    com = "COM3"
        'End If

        FaseEsec()
        ls100.SInkJet = ""
        ls100.SVersion = ""
        testok = False
        ' se non è andato bene provo ancora a 9600
        'ritorno = Seriale.LS100_Close(0, 83)
        If (MessageboxMy("I° Test Comunicazione Seriale... (9600 b/s)", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing) = DialogResult.OK) Then
            'ritorno = Seriale.LS100_RS232Configuration(0, com, 9600, 1, 7, 0)
            'ritorno = Seriale.LS100_Open(0, 0, 83)
            'If (ritorno = Ls100Class.Costanti.S_LS_OKAY) Then
            '    ritorno = Seriale.LS100_IdentifyEx(0, 83, str, str2, str3, str4, str5, str6, 0)
            '    If (ritorno = Ls100Class.Costanti.S_LS_OKAY) Then
            '        testok = True
            '    Else
            '        testok = False
            '    End If
            'Else
            '    testok = False
            'End If
        Else
            'FineCollaudo("Test a 9600 BAUD: Annullato", False)
            Return False
        End If ' 9600


        If testok = True Then
            ' imposto le variabili dell' identificativo per averlo aggiornato
            ls100.SIdentStr = str
            ls100.SVersion = str2
            ls100.SDateFW = str3
            ls100.SLema = str4
            ls100.SInkJet = str5
            ls100.SSerialNumber = str6
            ' imposto le variabili di ritorno e la barra 
            TestIdentificativoSeriale = 1
            FaseOk()

            ' aggiorno il file di trace
            FileTraccia.Reserved(0) = 1
        Else
            TestIdentificativoSeriale = 0
            FaseKo()

            '            ViewError(PCalibraPulsanti)
        End If

        '  Seriale.LS100_Close(0, 83)

        Return testok
    End Function

    Public Function TestIdentificativoPiastra() As Integer
        Dim ritorno As Integer
        Dim ret As Integer
        Dim StrVersione As String
        'Dim fpga As Integer

        ' inizializzazioni variabili 

        FaseEsec()
        ritorno = False
        'chiamata a funzione 
        ret = ls100.Identificativo()
        BIdentificativo = True
        MatricolaPiastra = ls100.SSerialNumber
        FileTraccia.VersioneFw = ls100.SVersion

        If (ret = Ls100Class.Costanti.S_LS_OKAY) Then
            ' Lidentif.Text = ls150.SLsName + " " + ls150.SVersion + vbCrLf           
            StrVersione = "Versione FW: " + ls100.SVersion + vbCrLf
            MessageboxMy(StrVersione, "Versione firmware", MessageBoxButtons.OK, MessageBoxIcon.None, Nothing)
        End If
        ritorno = True


        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ls100.ReturnC.ReturnCode
        Return ritorno
    End Function
    Public Function TestIdentificativo() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        ' inizializzazioni variabili 
        FaseEsec()
        ritorno = False
        'chiamata a funzione 
        'idmsg = 191
        'If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
        '    dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        'Else
        '    dres = MessageboxMy("Collegare ORA la periferica da Collaudare", "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
        'End If

        'davide
        'InkDetectorCalibration()

        ret = ls100.Identificativo()

        If (ret = Ls100Class.Costanti.S_LS_OKAY) Then
            BIdentificativo = True
            FileTraccia.VersioneFw = ls100.SVersion
            'Lidentif.Text = ls100.SModel + " " + ls100.SVersion + vbCrLf
            'Lidentif.Text += " " + " " + ls100.SLema
            If ls100.SIdentStr.Length >= 2 Then
                ' da scommentare  e mettere correto per ls100
                'If System.Convert.ToInt16(ls100.SIdentStr.Chars(1)) And &H1 Then
                '    Lidentif.Text += "CMC7 Reader" & vbCrLf
                'End If
                'If System.Convert.ToInt16(ls100.SIdentStr.Chars(1)) And &H2 Then
                '    Lidentif.Text += "E13B Reader" & vbCrLf
                'End If
                'If System.Convert.ToInt16(ls100.SIdentStr.Chars(1)) And &H4 Then
                '    Lidentif.Text += "Barcode Reader" & vbCrLf
                'End If
                'If System.Convert.ToInt16(ls100.SIdentStr.Chars(1)) And &H8 Then
                '    Lidentif.Text += "Ink Jet printer" & vbCrLf
                'End If
                'If System.Convert.ToInt16(ls100.SIdentStr.Chars(1)) And &H10 Then
                '    Lidentif.Text += "Endorsment voiding Stamp" & vbCrLf
                'End If
                'If System.Convert.ToInt16(ls100.SIdentStr.Chars(1)) And &H20 Then
                '    Lidentif.Text += "Front voiding Stamp" & vbCrLf
                'End If
                'If System.Convert.ToInt16(ls100.SIdentStr.Chars(2)) And &H1 Then
                '    Lidentif.Text += "Front Scanner" & vbCrLf
                'End If
                'If System.Convert.ToInt16(ls100.SIdentStr.Chars(2)) And &H2 Then
                '    Lidentif.Text += "Back Scanner" & vbCrLf
                'End If
                'If System.Convert.ToInt16(ls100.SIdentStr.Chars(2)) And &H10 Then
                '    Lidentif.Text += "Badge Reader" & vbCrLf
                'End If
                'If System.Convert.ToInt16(ls100.SIdentStr.Chars(2)) And &H20 Then
                '    Lidentif.Text += "Double Leafing Sensor" & vbCrLf
                'End If
            End If
            MatricolaPiastra = ls100.SSerialNumber
            ritorno = True
        Else
            ' errore da richiesta identificativo
            MessageboxMy(ls100.ReturnC.FunzioneChiamante, "Attenzione !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function
    Public Function TestFoto() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim floop As Boolean = False
        ritorno = False


        FaseEsec()
        ritorno = False

        If MessageboxMy("Togliere i Cappucci!!!", "ATTENZIONE !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning, Nothing) <> DialogResult.OK Then

        Else
            ret = ls100.CalibrazioneFoto()
            If (ret = Ls100Class.Costanti.S_LS_OKAY) Then
                'If (ls100.PhotoValue(0) <> 255 And ls100.PhotoValue(1) <> 255 And ls100.PhotoValue(2) <> 255) Then
                '    PCalibraFotoPercorso.SetState = 1
                '    PCalibraFotoPercorso.IsBlinking = True
                '    PCalibraFotoPercorso.IsSelected = True

                ritorno = True
                'Else
                '    PCalibraFotoPercorso.SetState = 0
                '    PCalibraFotoPercorso.IsBlinking = True
                '    PCalibraFotoPercorso.IsSelected = True
                'End If
            Else
                ViewError(fase)
            End If

        End If
        If ritorno = True Then
            FaseOk()
            FileTraccia.FotoPersorso = 1
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function
    Public Function TestFotoPeriferica() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim floop As Boolean = False
        ritorno = False

        FaseEsec()

        ' seleziono il tab dei fotosensori   
        idmsg = 55
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Togliere i documenti", "ATTENZIONE !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning, Nothing)
        End If


        If dres <> DialogResult.OK Then

        Else
            ret = ls100.CalibrazioneFoto()
            If (ret = Ls100Class.Costanti.S_LS_OKAY) Then
                ' If (ls100.PhotoValue(0) <> 255 And ls100.PhotoValue(1) <> 255 And ls100.PhotoValue(2) <> 255) Then
                ritorno = True
                ' imposto i voleri nella videata del fotosensori
                'Lf1.Text = ls100.PhotoValue(0)
                'Lf2.Text = ls100.PhotoValue(1)

                'Ldp1.Text = ls100.PhotoValue(2)


                'Else
                '    PCalibraFotoPercorso.SetState = 0
                '    PCalibraFotoPercorso.IsBlinking = True
                '    PCalibraFotoPercorso.IsSelected = True
                '    StrCommento += "Valori foto " + ls100.PhotoValue(0).ToString + " " + ls100.PhotoValue(1).ToString + " " + ls100.PhotoValue(2).ToString + " "
                'End If
            Else
                ViewError(fase)
                StrCommento += "Calibrazione non a buon fine: " + ret.ToString + " "
            End If
            FileTraccia.Note = "Fotosensore 1: " + Ls100Class.Periferica.PhotoValue(0).ToString + vbCrLf
            FileTraccia.Note += "Fotosensore 2: " + Ls100Class.Periferica.PhotoValue(1).ToString + vbCrLf
            FileTraccia.Note += "Fotosensore 3: " + Ls100Class.Periferica.PhotoValue(2).ToString + vbCrLf
            FileTraccia.Note += "Fotosensore 4: " + Ls100Class.Periferica.PhotoValue(3).ToString

        End If
        If ritorno = True Then
            FaseOk()
            FileTraccia.FotoPersorso = 1
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function
    Public Function TestFotoDPPiastra() As Integer
        Dim ritorno As Short
        Dim ret As Short

        ritorno = False
        FaseEsec()
        ' faccio la calibrazione del foto doppia sfogliatura
        If (MessageboxMy("INSERIRE I CAPPUCCI", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing) = DialogResult.OK) Then
            If MessageboxMy("Inserire il documento di calibrazione  DOCUTEST-052 nel Feeder", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing) <> DialogResult.OK Then

                ' errore calibrazione doppia sfogliatura
                ViewError(fase)
            Else

                ret = ls100.CalSfogliatura()
                If (ret = Ls100Class.Costanti.S_LS_OKAY) Then

                    ritorno = True
                Else

                    ' errore calibrazione doppia sfogliatura
                    ViewError(fase)
                End If
            End If
        End If
        If ritorno = True Then
            FaseOk()
            FileTraccia.FotoDp = 1
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function
    Public Function TestScannerF() As Integer
        Dim ritorno As Integer
        Dim ret As Short

        ritorno = False
        '  BvisualF.Text = "Controllo immagine Fronte"

        FaseEsec()
        ' faccio la calibrazione scanner fronte
        If (MessageboxMy("Inserire il documento per la calibrazione Scanner FRONTE. DOCUTEST 020 - 04!", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(1)) <> DialogResult.OK) Then
            ret = -1
            fWaitButton = True

        Else




            fquality = False
            ret = ls100.CalibraScanner(Ls100Class.Costanti.SCANNER_FRONT)
            If (ret = Ls100Class.Costanti.S_LS_OKAY) Then
                If MessageboxMy("La calibrazione scanner FRONTE è andata a buon fine ?", "ATTENZIONE", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing) = DialogResult.Yes Then
                    If MessageboxMy("Inserire un documento per verificare l'immagine fronte", "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Question, VetImg(7)) = DialogResult.OK Then
                        ls100.NomeFileImmagine = zzz + "\Dati\FrontImage" + IndImgF.ToString("0000") + ".bmp"
                        ret = ls100.LeggiImmagini(System.Convert.ToSByte(Ls100Class.Costanti.S_SIDE_FRONT_IMAGE))
                        If (ret = Ls100Class.Costanti.S_LS_OKAY) Then
                            DisposeImg()

                            PFronte.Image = System.Drawing.Image.FromFile(zzz + "\Dati\FrontImage" + IndImgF.ToString("0000") + ".bmp")
                            Pimage.Image = PFronte.Image
                            Application.DoEvents()

                            BVisualZoom = True
                            FileZoomCorrente = ls100.NomeFileImmagine
                            DirZoom = Application.StartupPath
                            Dim a As Short
                            a = DirZoom.LastIndexOf("\")
                            DirZoom = DirZoom.Substring(0, a)
                            DirZoom += "\Dati\"
                            IndImgF += 1
                            ShowZoom(DirZoom, ls100.NomeFileImmagine)
                            If MessageboxMy("La qualità dell' immagine Fronte è buona?", "ATTENZIONE", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing) = DialogResult.Yes Then
                                ritorno = True
                                fWaitButton = True
                            Else

                                ' devo aspettare la gestione dei bottoni 
                                ' per lasciare fare all'utente come vuole
                                ritorno = True
                            End If
                        Else

                            ViewError(fase)
                        End If
                    Else


                    End If

                Else
                    ' calibrazione abortita da utente

                    ret = False
                End If
            Else
                ' errore calibrazione fronte

                ViewError(fase)
            End If
        End If


        If ritorno = True Then
            FaseOk()
            FileTraccia.ScannerF = 1
        Else
            FaseKo()
        End If
        BVisualZoom = False
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function
    Public Function TestScannerR() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        ritorno = False

        FaseEsec()

        ' faccio la calibrazione scanner retro
        If (MessageboxMy("Inserire il Documento per la calibrazione Scanner RETRO. DOCUTEST 020 - 04", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(4)) <> DialogResult.OK) Then

            fWaitButton = True

        Else


            fquality = False
            ret = ls100.CalibraScanner(Ls100Class.Costanti.SCANNER_BACK)

            If (ret = Ls100Class.Costanti.S_LS_OKAY) Then
                If MessageboxMy("La calibrazione scanner RETRO è andata a buon fine ?", "ATTENZIONE", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing) = DialogResult.Yes Then
                    MessageboxMy("Inserire un documento per verificare l'immagine retro", "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(9))
                    ls100.NomeFileImmagine = zzz + "\Dati\BackImage" + IndImgR.ToString("0000") + ".bmp"
                    ret = ls100.LeggiImmagini(System.Convert.ToSByte(Ls100Class.Costanti.S_SIDE_BACK_IMAGE))
                    If (ret = Ls100Class.Costanti.S_LS_OKAY) Then

                        DisposeImg()
                        PRetro.Image = System.Drawing.Image.FromFile(zzz + "\Dati\BackImage" + IndImgR.ToString("0000") + ".bmp")
                        Pimage.Image = PRetro.Image
                        Application.DoEvents()
                        BVisualZoom = True
                        FileZoomCorrente = ls100.NomeFileImmagine
                        DirZoom = Application.StartupPath
                        Dim a As Short
                        a = DirZoom.LastIndexOf("\")
                        DirZoom = DirZoom.Substring(0, a)
                        DirZoom += "\Dati\"
                        IndImgR += 1
                        ShowZoom(DirZoom, ls100.NomeFileImmagine)
                        If MessageboxMy("La qualità dell' immagine Retro è buona?", "ATTENZIONE", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing) = DialogResult.Yes Then
                            ritorno = True
                            fWaitButton = True
                        Else

                            ' devo aspettare la gestione dei bottoni 
                            ' per lasciare fare all'utente come vuole
                            ritorno = True
                        End If


                    Else

                        ViewError(fase)
                    End If

                Else
                    ' calibrazione abortita da utente



                End If
            Else

                ' errore calibrazione doppia sfogliatura

                ViewError(fase)
            End If
        End If
        ' Pimage.Image.Dispose()
        ' Pimage.Image = Nothing
        If ritorno = True Then
            FaseOk()
            FileTraccia.ScannerR = 1
        Else
            FaseKo()
        End If
        BVisualZoom = False
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function
    Public Function TestScanners() As Integer
        Dim ritorno As Integer
        Dim ret As Short


        ritorno = False

        ' BvisualF.Text = "Controllo immagine Fronte e Retro"


        FaseEsec()

        ' faccio la calibrazione scanner fronte

        idmsg = 56
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire il documento per la calibrazione Scanners. DOCUTEST-055!", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(10))
        End If

        If (dres <> DialogResult.OK) Then
            ret = -1

            fWaitButton = True

        Else



            fquality = False
            ret = ls100.CalibraScanner(Ls100Class.Costanti.SCANNER_FRONT)
            If (ret = Ls100Class.Costanti.S_LS_OKAY) Then

                '   VisualizzaFronteRetro()
            Else
                ' errore calibrazione fronte

                ViewError(fase)
                'ViewError(PCalibraScannerRetro)
            End If
        End If


        If ritorno = True Then
            FaseOk()
            FileTraccia.ScannerF = 1
        Else
            FaseKo()
        End If
        BVisualZoom = False
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function

    Public Function TestScannersPeriferica() As Integer
        Dim ritorno As Integer
        Dim ret As Short


        ritorno = False
        ' BvisualF.Text = "Controllo immagine Fronte e Retro"
        '  BCalF.Text = "Calibra Scanner Fronte e Retro"


        FaseEsec()
        ' faccio la calibrazione scanner fronte

        idmsg = 56
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire il documento per la calibrazione Scanners. DOCUTEST-055!", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(10))
        End If

        If (dres <> DialogResult.OK) Then
            fWaitButton = True
        Else


            fquality = False
            ret = ls100.CalibraScanner(Ls100Class.Costanti.SCANNER_FRONT)
            If (ret = Ls100Class.Costanti.S_LS_OKAY) Then
                'If (ordine.ord.Scanner_fronte <> ordine.ord.NON_PRESENTE) Then
                '    FileZoomCorrente = ls100.NomeFileImmagine
                '    ret = LeggiImmagine(System.Convert.ToSByte(Ls100Class.Costanti.S_SIDE_FRONT_IMAGE))
                'End If
                'If ret = 1 Then
                '    If (ordine.ord.Scanner_retro <> ordine.ord.NON_PRESENTE) Then
                '        FileZoomCorrente = ls100.NomeFileImmagine
                '        ret = LeggiImmagine(System.Convert.ToSByte(Ls100Class.Costanti.S_SIDE_BACK_IMAGE))
                '    End If
                'End If
                ritorno = True

            Else
                ' errore calibrazione fronte
                ViewError(fase)
                'ViewError(PCalibraScannerRetro)
            End If
        End If

        If ritorno = True Then
            FaseOk()
            FileTraccia.ScannerF = 1
        Else
            FaseKo()
        End If
        BVisualZoom = False
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function
    Public Function TestFotoDP() As Integer
        Dim ritorno As Short
        Dim ret As Short

        ritorno = False
        FaseEsec()
        ' faccio la calibrazione del foto doppia sfogliatura
        idmsg = 57
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire il documento di calibrazione  DOCUTEST-052 nel Feeder", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        End If
        If dres <> DialogResult.OK Then


            ' errore calibrazione doppia sfogliatura
            ret = Ls100Class.Costanti.S_LS100_USER_ABORT
            ViewError(fase)

            StrCommento += "Abortito da utente"
        Else

            ret = ls100.CalSfogliatura()

            FileTraccia.Note = "Fotosensore DP 1: " + Ls100Class.Periferica.PhotoValueDP(0).ToString + vbCrLf
            FileTraccia.Note += "Fotosensore DP 2: " + Ls100Class.Periferica.PhotoValueDP(1).ToString + vbCrLf
            FileTraccia.Note += "Fotosensore DP 3: " + Ls100Class.Periferica.PhotoValueDP(2).ToString + vbCrLf

            FileTraccia.Note += "Fotosensore TH 1: " + Ls100Class.Periferica.PhotoValueDP(3).ToString + vbCrLf
            FileTraccia.Note += "Fotosensore TH 2: " + Ls100Class.Periferica.PhotoValueDP(4).ToString + vbCrLf
            FileTraccia.Note += "Fotosensore TH 3: " + Ls100Class.Periferica.PhotoValueDP(5).ToString + vbCrLf
            FileTraccia.Note += "Fotosensore TH 4: " + Ls100Class.Periferica.PhotoValueDP(6).ToString + vbCrLf
            FileTraccia.Note += "Fotosensore TH 5: " + Ls100Class.Periferica.PhotoValueDP(7).ToString + vbCrLf
            FileTraccia.Note += "Fotosensore TH 6: " + Ls100Class.Periferica.PhotoValueDP(8).ToString


            If (ret = Ls100Class.Costanti.S_LS_OKAY) Then

                ritorno = True
            Else


                ' errore calibrazione doppia sfogliatura
                ViewError(fase)
                StrCommento += "Reply :" + ret.ToString + " "
            End If
        End If

        If ritorno = True Then
            FaseOk()
            FileTraccia.FotoDp = 1
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function

    Public Function TestStampa(ByVal Altezza As Short) As Integer
        Dim ritorno As Integer
        Dim ret As Integer
        Dim strapp As String
        Dim a As Short
        ritorno = False

        FaseEsec()
        'If TipoDiCollaudo = COLLAUDO_PIASTRA_LS100_7 Or TipoDiCollaudo = COLLAUDO_PIASTRA_LS100_7 Then
        '    ordine.ord.Scanner_fronte = ordine.ord.PRESENTE
        '    ordine.ord.Scanner_retro = ordine.ord.PRESENTE
        'End If

        DirZoom = Application.StartupPath
        a = DirZoom.LastIndexOf("\")
        DirZoom = DirZoom.Substring(0, a)
        DirZoom += "\Dati\"

        ls100.NomeFileImmagine = zzz + "\Dati\StampaFImage" + IndImgF.ToString("0000") + ".bmp"
        ls100.NomeFileImmagineRetro = zzz + "\Dati\StampaRImage" + IndImgR.ToString("0000") + ".bmp"
        IndImgF += 1
        IndImgR += 1

        idmsg = 58
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire un documento per la prova di stampa", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(3))
        End If




        If Altezza = 1 Then
            ret = ls100.TestStampaAlto(Ls100Class.Costanti.S_SIDE_ALL_IMAGE)
            strapp = "La stampante ha stampato bene? Il LED rosso a fianco della periferica si è acceso"
            'ShowZoom(DirZoom, ls100.NomeFileImmagineRetro)
        Else
            If fScannerFronte = True And fScannerRetro = True Then
                ret = ls100.TestStampa(System.Convert.ToInt16(Ls100Class.Costanti.S_SIDE_ALL_IMAGE))
            Else
                If fScannerFronte = True Then
                    ret = ls100.TestStampa(System.Convert.ToInt16(Ls100Class.Costanti.S_SIDE_FRONT_IMAGE))
                End If
            End If
            strapp = "La stampante ha stampato bene ?"
        End If

        If (ret = Ls100Class.Costanti.S_LS_OKAY) Then

            Try
                DisposeImg()
                Pimage.Image = System.Drawing.Image.FromFile(ls100.NomeFileImmagine)
                PImageBack.Image = System.Drawing.Image.FromFile(ls100.NomeFileImmagineRetro)
            Catch ex As Exception
                ' DEBUG
                MessageBox.Show(ex.Message + " " + ex.Source + ret.ToString, "errore")
            End Try

            Application.DoEvents()

            idmsg = 59
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy(strapp, "Attenzione!!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing)
            End If


            If dres = DialogResult.Yes Then

                ritorno = True

            Else

            End If
        Else
            ViewError(fase)
        End If

        If ritorno = True Then
            FaseOk()
            FileTraccia.Stampante = 1
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function
    Public Function TestTimbro(ByVal TipoTimbro As Short)
        Dim Ritorno As Integer
        Dim ret As Integer
        Ritorno = False

        idmsg = 60
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire un documento per test Timbri", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(3))
        End If




        ret = ls100.Timbra(Ls100Class.Costanti.S_SORTER_BAY1)
        If (ret = Ls100Class.Costanti.S_LS_OKAY) Then

            idmsg = 61
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy(" Il timbro ha timbrato in modo corretto ?", "Attenzione!! ", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, Nothing)
            End If


            If (dres = DialogResult.Yes) Then

                'LTimR.ForeColor = Color.Green
                'LTimR.Text = "OK"
                Ritorno = True

            Else
                'LTimF.ForeColor = Color.Red
                'LTimF.Text = "KO"
            End If
        Else
            ViewError(fase)
        End If
        If Ritorno = True Then
            FaseOk()
            FileTraccia.TimbroFronte = 1
            FileTraccia.TimbroRetro = 1
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return Ritorno
    End Function
    Public Function TestCalibraTestina() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim RangeMore As Single
        Dim RangeLess As Single
        Dim strmsg As String
        Dim valore As Integer

        Dim finfo As New CtsControls.FormInfoTest


        RangeMore = RangeLess = 1
        ritorno = False

        '  LICodeline.Items.Clear()

        FaseEsec()

        fWaitButton = False

        strmsg = " Attenzione !!! Calibrazione 200 DPI"

        'LICodeline.Items.Add(strmsg + "--> Valore Documento  " + TValMICR.Text)
        'TCollaudo.SelectedTab() = TMessaggi


        finfo.PointToScreen(New Point(300, 100))
        finfo.Show()
        finfo.BringToFront()

        StrMessaggio = strmsg

        idmsg = 62
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            StrMessaggio = Messaggio
        End If

        If InputboxMy(StrMessaggio, "Attenzione", True, valore) = DialogResult.OK Then

            idmsg = 63
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Inserire DOCUTEST 042 lato puntini n° Volte, fino ad avvenuta calibrazione:", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            End If


            If dres = DialogResult.OK Then
                ret = ls100.DoMICRCalibration50Barrette(System.Convert.ToInt32(valore), 5, 1, finfo.Linfo)
                If (ret = Ls100Class.Costanti.S_LS_OKAY) Then
                    ' test per calibrazione MICR ok

                    RangeMore = System.Convert.ToSingle(valore) * 4 / 100
                    RangeLess = System.Convert.ToSingle(valore) * 4 / 100

                    ' scrivo nel file di trace i valori visualizzati a video
                    For Each s As String In finfo.Linfo.Items
                        FileTraccia.Note += s + vbCrLf
                    Next

                    If ((ls100.Percentile > (System.Convert.ToSingle(valore) - RangeLess)) And (ls100.Percentile < (System.Convert.ToSingle(valore) + RangeMore))) Then

                        ritorno = True
                    Else
                        ret = Ls100Class.Costanti.S_LS100_CALIBRATION_FAILED

                    End If
                Else
                    ViewError(fase)
                End If
                'Else
                '    ritorno = False
                'End If
            End If
        End If


        finfo.Close()

        If ritorno = True Then
            FaseOk()
            FileTraccia.CalMicr = 1
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function
    Public Function TestTempoCamp(ByVal Type As Short) As Integer
        Dim ritorno As Integer
        Dim ii As Short
        Dim stamp As Short
        Dim scan As Short
        Dim strmsg As String = ""
        Dim finfo As New CtsControls.FormInfoTest


        Dim replycode As Short
        ii = 0
        ritorno = False
        FaseEsec()


        stamp = 0
        scan = Ls100Class.Costanti.S_SCAN_MODE_256GR200
        strmsg = " Attenzione !!! Calibrazione 200 DPI "
        finfo.PointToScreen(New Point(300, 100))
        finfo.Show()
        finfo.BringToFront()
        'LICodeline.Items.Add(strmsg)

        idmsg = 64
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire DOCUTEST --042 dalla parte delle Barrette Continue", "Attenzione!!! " + strmsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        End If


        If (dres = DialogResult.OK) Then
            ls100.NrLettureOk = 0
            Do
                'If (MessageboxMy("Inserire DOCUTEST -042 dalla parte delle Barrette Continue", "Attenzione!!! " + strmsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing) = DialogResult.OK) Then
                replycode = ls100.DoTimeSampleMICRCalibration(0, 0)
                'LICodeline.Items.Add("Valore min: " + ls150.llMin.ToString + " Valore Medio : " + ls150.llMedia.ToString + "  Valore Massimo : " + ls150.llMax.ToString)
                ii += 1
                Dim strmsginfo As String
                strmsginfo = "Ciclo : " + ii.ToString + " Valore Massimo : " + ls100.llMax.ToString + " Valore minimo : " + ls100.llMin.ToString
                finfo.Linfo.Items.Add(strmsginfo)
                finfo.Linfo.SelectedIndex = finfo.Linfo.Items.Count - 1
                finfo.Refresh()
                FileTraccia.Note += strmsginfo + vbCrLf
                'MessageboxMy(ls100.llMax.ToString + " " + ls100.llMin.ToString, "test", MessageBoxButtons.OK, MessageBoxIcon.None, Nothing)
                'Else
                '   ii = 8
                '    ret = False
                'End If

            Loop While ((ii < 13) And (ls100.NrLettureOk < 3) And (replycode = Ls100Class.Costanti.S_LS_OKAY))
        Else
            replycode = Ls100Class.Costanti.S_LS100_USER_ABORT
            'ret = False
        End If
        If (replycode <> Ls100Class.Costanti.S_LS_OKAY And ls100.NrLettureOk < 3) Then
            ritorno = False
        Else
            ritorno = True
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        finfo.Close()
        FileTraccia.UltimoRitornoFunzione = replycode
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        Return ritorno
    End Function
    'Public Function TestSeriale() As Integer
    '    Dim ritorno As Integer
    '    Dim com As String
    '    ritorno = False
    '    TestSeriale = 0
    '    com = "COM1"
    '    Dim testok As Boolean
    '    'If MCom1.Checked = True Then
    '    '    com = "COM1"
    '    'End If
    '    'If MCom2.Checked = True Then
    '    '    com = "COM2"
    '    'End If
    '    'If MCom3.Checked = True Then
    '    '    com = "COM3"
    '    'End If
    '    FaseEsec()

    '    If (MessageboxMy("Accendere la periferica in Seriale", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing) = DialogResult.OK) Then
    '        TestAspetta(False)
    '        If (MessageboxMy("Test Ponticello Seriale", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing) = DialogResult.OK) Then
    '            'If (ls100.Seriale = Ls100Class.Costanti.S_LS_OKAY) Then
    '            ' test a  115000
    '            If (MessageboxMy("Test Comunicazione Seriale... (115200 b/s)", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing) = DialogResult.OK) Then
    '                'ritorno = Seriale.LS100_RS232Configuration(0, com, 115200, 0, 8, 0)
    '                'ritorno = Seriale.LS100_Open(0, 0, 83)
    '                If (ritorno = Ls100Class.Costanti.S_LS_OKAY) Then
    '                    testok = True
    '                    TestSeriale = 1
    '                Else
    '                    testok = False
    '                End If
    '                If (testok = False) Then
    '                    ' se non è andato bene provo ancora a 9600
    '                    'ritorno = Seriale.LS100_Close(0, 83)
    '                    If (MessageboxMy("II° Test Comunicazione Seriale... (9600 b/s)", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing) = DialogResult.OK) Then
    '                        'ritorno = Seriale.LS100_RS232Configuration(0, com, 9600, 0, 8, 0)
    '                        'ritorno = Seriale.LS100_Open(0, 0, 83)
    '                        If (ritorno = Ls100Class.Costanti.S_LS_OKAY) Then
    '                            TestSeriale = 1
    '                            ritorno = True

    '                        Else
    '                            ritorno = False
    '                            TestSeriale = 0
    '                            ViewError(fase)
    '                        End If
    '                    End If
    '                End If
    '            End If
    '            'Else
    '            '   ViewError(fase)
    '            'End If
    '            'Seriale.LS100_Close(0, 83))
    '        End If
    '    End If



    '    If TestSeriale = 1 Then
    '        FileTraccia.Reserved(0) = 1
    '    End If

    'End Function
    Public Function TestPulsanti() As Integer
        Dim ritorno As Integer
        ritorno = False
        Return ritorno
    End Function
    Public Function TestViewSignal() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim strmsg As String

        ritorno = False
        FaseEsec()
        'Select Case TipoDiCollaudo
        '    Case COLLAUDO_PIASTRA_LS100_7, COLLAUDO_PERIFERICA_LS100_7
        '        strmsg = "Inserire DOCUTEST 42 dalla parte dei punti:"
        '    Case COLLAUDO_PIASTRA_LS100_IP, COLLAUDO_PERIFERICA_LS100_IP
        '        strmsg = "Inserire DOCUTEST 42 dalla parte dei punti:"
        '    Case COLLAUDO_PIASTRA_LS100_3_6, COLLAUDO_PERIFERICA_LS100_3_6
        '        strmsg = "Inserire DOCUTEST 32 dalla parte delle due barrette:"
        'End Select
        strmsg = ""
        MessageboxMy(strmsg, "Attenzione !!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        ls100.Reset(Ls100Class.Costanti.S_RESET_ERROR)
        ret = ls100.ViewE13BSignal(True)
        If (ret = Ls100Class.Costanti.S_LS_OKAY) Then
            'TGrafici.SelectedTab = TviewS
            Dim p As New Pen(Color.Red)
            If Ls100Class.Periferica.E13BSignal Is Nothing Then
                MessageboxMy("Errore: Grafico non disponibile", "Errore", MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing)
            Else
                'Grafico4.SetTraccia(ls100.E13BSignal, ls100.E13BSignal.Length - 1, p)
                'Grafico4.LTitle.Text = "Scorrere tutto il grafico"
                'While Grafico4.fstop = False
                '    Application.DoEvents()
                'End While
                If MessageboxMy("Il grafico è conforme ?", "Attenzione !!", MessageBoxButtons.YesNo, MessageBoxIcon.Information, Nothing) = DialogResult.Yes Then
                    ritorno = True

                Else
                    ritorno = False

                End If

            End If
        Else
            ritorno = False

        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function
    Public Function TestCheckDocS() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim min, max As Byte
        Dim strmsg As String

        ritorno = False
        FaseEsec()

        'Select Case TipoDiCollaudo
        '    Case COLLAUDO_PIASTRA_LS100_7, COLLAUDO_PERIFERICA_LS100_7
        '        strmsg = "Inserire DOCUTEST 42 dalla parte delle barrette continue"
        '    Case COLLAUDO_PIASTRA_LS100_IP, COLLAUDO_PERIFERICA_LS100_IP
        '        strmsg = "Inserire DOCUTEST 42 dalla parte delle barrette continue"
        '    Case COLLAUDO_PIASTRA_LS100_3_6, COLLAUDO_PERIFERICA_LS100_3_6
        '        strmsg = "Inserire DOCUTEST 32 dalla parte delle barrette continue"
        'End Select
        strmsg = ""
        MessageboxMy(strmsg, "Attenzione !!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)

        ret = ls100.ViewE13BSignal(False)
        If (ret = Ls100Class.Costanti.S_LS_OKAY) Then

            Dim p As New Pen(Color.Red)
            If Ls100Class.Periferica.E13BSignal Is Nothing Then
                MessageboxMy("Errore: Grafico non disponibile", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
            Else
                ls100.DisegnaEstrectLenBar()
                min = Byte.MaxValue
                max = Byte.MinValue
                For ii As Short = 0 To 511
                    If Ls100Class.Periferica.E13BSignal(ii) < min Then
                        min = Ls100Class.Periferica.E13BSignal(ii)
                    End If
                    If Ls100Class.Periferica.E13BSignal(ii) > max Then
                        max = Ls100Class.Periferica.E13BSignal(ii)
                    End If
                Next
                'Grafico3.LTitle.Text = "Grafico Check Document Speed" + " Max = " + max.ToString + " Min = " + min.ToString

                'Grafico3.SetTraccia(ls100.ViewSignal, 512, p)

                If MessageboxMy("Il grafico è conforme ?", "Attenzione !!", MessageBoxButtons.YesNo, MessageBoxIcon.Information, Nothing) = DialogResult.Yes Then
                    ritorno = True

                Else
                    ritorno = False

                End If

            End If
        Else
            ritorno = False

        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function
    Public Function TestLetturaOCRX(ByVal ocr As Short) As Short
        Dim ritorno As Integer
        Dim ret As Short

        ritorno = False
        FaseEsec()
        MessageboxMy("Inserire" + ocr.ToString + " DOCUTEST N° 025 per Test OCR", "Attenzione !!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        ret = LeggiCodelinesOCR(ocr, Ls100Class.Costanti.S_READ_OPTIC_E13B_X_SWITCH)
        If (ret = Ls100Class.Costanti.S_LS_OKAY) Then

            ritorno = True
        Else

        End If
        If ritorno = True Then
            FaseKo()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function
    Public Function TestLetturaOCR(ByVal ocr As Short) As Short
        Dim ritorno As Integer
        Dim ret As Short

        ritorno = False
        FaseEsec()
        MessageboxMy("Inserire" + ocr.ToString + " DOCUTEST N° 4 per Test OCR", "Attenzione !!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        ret = LeggiCodelinesOCR(ocr, Ls100Class.Costanti.S_READ_CODELINE_SW_OCRB_ITALY)
        If (ret = Ls100Class.Costanti.S_LS_OKAY) Then

            ritorno = True
        Else

        End If
        If ritorno = True Then
            FaseKo()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function
    Public Function TestLettura(ByVal cmc7 As Short, ByVal e13b As Short, ByVal Tipo_controllo As Short) As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim strmsg As String = ""

        ritorno = False
        FaseEsec()

        Select Case Tipo_controllo
            Case 5
                idmsg = 65
                strmsg = "Inserire DOCUTEST n ° 046 048 049"
            Case 6
                idmsg = 66
                strmsg = "Inserire DOCUTEST n ° 047-02 "
            Case 7
                idmsg = 67
                strmsg = "Inserire DOCUTEST n ° 045-02 050-02"
            Case Else

        End Select


        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy(strmsg, "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        End If


        If dres = DialogResult.Abort Then
            ritorno = False
        Else
            If fSfogliatore = False Then
                cmc7 = cmc7 / 2
                e13b = e13b / 2
            End If
            ret = LeggiCodelines(cmc7, e13b, Tipo_controllo)
        End If

        If (ret = 1) Then
            ritorno = True
        Else

        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function


    Public Function TestConfigurazione(ByVal fLema As Boolean, ByVal fInk As Boolean) As Short
        Dim ritorno As Short
        Dim ret As Short

        ritorno = False
        ret = ls100.Identificativo()
        If (ret = Ls100Class.Costanti.S_LS_OKAY) Then
            If fInk Then
                If ((ls100.SInkJet.Length = 0) Or (String.Compare(ls100.SInkJet, "          ") = 0)) Then
                    MessageboxMy("Ink jet non presente.", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                    FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
                    FileTraccia.UltimoRitornoFunzione = ret
                    Return ritorno
                End If
            End If
            'If fLema Then
            '    If ((ls100.SLema.Length = 0) Or (String.Compare(ls100.SLema, "          ") = 0)) Then
            '        MessageboxMy("OCR non presente.", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
            '        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
            '        FileTraccia.UltimoRitornoFunzione = ret
            '        Return ritorno
            '    End If
            'End If

            '    If System.Convert.ToByte(ls100.SIdentStr.Chars(1)) And &H10 Then
            '    Else
            '        MessageboxMy("Endorsment voiding Stamp non presente.", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
            '        Return ritorno
            '    End If

            '    If System.Convert.ToByte(ls100.SIdentStr.Chars(2)) And &H1 Then
            '        Lidentif.Text += "Front Scanner" & vbCrLf
            '    Else
            '        MessageboxMy("Front Scanner non presente.", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
            '        Return ritorno
            '    End If
            '    If System.Convert.ToByte(ls100.SIdentStr.Chars(2)) And &H2 Then
            '        Lidentif.Text += "Back Scanner" & vbCrLf
            '    Else
            '        MessageboxMy("Back Scanner non presente.", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
            '        Return ritorno
            '    End If
            '    If System.Convert.ToByte(ls100.SIdentStr.Chars(2)) And &H10 Then
            '        Lidentif.Text += "Badge Reader" & vbCrLf
            '    Else
            '        MessageboxMy("Badge Reader non presente.", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
            '        Return ritorno
            '    End If
        Else

        End If
        ritorno = True
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function
    Public Function TestCinghia() As Integer

        Dim ritorno As Integer
        Dim ret As Short


        ritorno = False
        FaseEsec()
        ls100.ValMinCinghia = 0
        ls100.ValMaxCinghia = 0

        ' ls100_5 a colori non gestisce il test cinghia
        If ls100.SVersion <> "Ls100/5_10" Then

            idmsg = 68
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Test Cinghia: Inserire il DOCUTEST", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            End If


            If (dres = DialogResult.OK) Then
                ret = ls100.TestCinghia()
                If (ret = Ls100Class.Costanti.S_LS_OKAY) Then

                    ritorno = True
                Else

                    StrCommento += "Reply :" + ret.ToString + " "
                End If
                FileTraccia.Note = "Valore minimo :" + ls100.ValMinCinghia.ToString + vbCrLf
                FileTraccia.Note += "Valore massimo :" + ls100.ValMaxCinghia.ToString + vbCrLf
                FileTraccia.Note += "Dati Letti:" + ls100.NumDati.ToString + vbCrLf

            Else
                StrCommento += "Abortito Da Utente "
            End If
        Else
            ritorno = True
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function

    Public Function TestAspetta(ByVal Acceso As Boolean) As Short
        Dim fff As New CtsControls.FOnOff
        Dim ret As Short
        If (Acceso = True) Then
            fff.LTezt.Text = "Accendere la periferica"
        Else
            fff.LTezt.Text = "Spegnere la periferica"
        End If
        Application.DoEvents()
        fff.Show()

        If (Acceso = True) Then
            fff.Refresh()
            ret = TestON()
        Else
            fff.Refresh()
            ret = TestOff()
        End If

        If ret = 1 Then
            ret = True
            fff.Close()
        Else
            ret = False
            fff.Close()
        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ret
    End Function
    Public Function TestOff() As Short
        Dim ritorno As Short
        Dim ret As Short
        ret = 0
        While (ret >= 0)
            ret = ls100.Identificativo()
        End While
        If ret = Ls100Class.Costanti.S_LS100_PERIFNOTFOUND Then
            ritorno = True
        End If

        Return ritorno
    End Function
    Public Function TestON() As Short
        Dim ritorno As Short
        Dim ret As Short
        ret = 1
        ritorno = False
        While (ret <> 0)
            ret = ls100.Identificativo()
        End While
        If ret = 0 Then
            ritorno = True
        End If

        Return ritorno
    End Function
    Public Function TestBadge(ByVal Tr As Short) As Short
        Dim ritorno As Short
        Dim ret As Short
        Dim strBadge As String = ""
        Dim strBadgeNew As String = ""

        Select Case Tr
            Case 1
                strBadge = "tCTS ELECTRONICS SPA BADGE CAMPIONE 11111111111111111111111111111111111111111t1111000022223333444455556666777888999"
                strBadgeNew = "tARCA BADGE CAMPIONE 11111111111111111111111111111111111111111111111111111111t1111000022223333444455556666777888999"
            Case 2
                strBadge = "t1111000022223333444455556666777888999t11111111111000000000002222222222233333333333444444444455555555556666666666777777777788888888889999999999"
            Case 3
                strBadge = "tCTS ELECTRONICS SPA BADGE CAMPIONE 11111111111111111111111111111111111111111t1111000022223333444455556666777888999t11111111111000000000002222222222233333333333444444444455555555556666666666777777777788888888889999999999"
                strBadgeNew = "tARCA BADGE CAMPIONE 11111111111111111111111111111111111111111111111111111111t1111000022223333444455556666777888999t11111111111000000000002222222222233333333333444444444455555555556666666666777777777788888888889999999999"
            Case Else

        End Select


        ritorno = False
        FaseEsec()

        idmsg = 69
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Strisciare Badge DOCUTEST --007", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(5))
        End If


        If dres = DialogResult.OK Then
            ret = ls100.LeggiBadge(Tr)
            If ret = Ls100Class.Costanti.S_LS_OKAY Then

                If (ls100.BadgeLetto = strBadge Or ls100.BadgeLetto = strBadgeNew) Then
                    idmsg = 115
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        dres = MessageboxMy(ls100.BadgeLetto + vbCrLf + Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy(ls100.BadgeLetto + vbCrLf + " Le tracce del badge sono corrette", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                    End If
                    ritorno = True
                Else
                    idmsg = 186
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        dres = MessageboxMy(ls100.BadgeLetto + vbCrLf + Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy(ls100.BadgeLetto + vbCrLf + "Tracce del badge lette non correttamente", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                    End If
                    ritorno = False
                    ret = Ls100Class.Costanti.S_LS100_USER_ABORT
                End If

            Else
                ritorno = False
            End If

        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function
    Public Function TestLunghezzaImmagine() As Integer
        Dim ret As Short
        Dim ritorno As Short
        Dim contatore = 1
        ritorno = False
        FaseEsec()

        BVisualZoom = False
        Do
            idmsg = 71
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Inserire DOCUTEST 042 Per calibrazione lunghezza immagine, sino all' esito", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            End If

            If (dres = DialogResult.OK) Then
                ret = ls100.CalibraImmagine()
            Else
                ret = -1000
            End If
            contatore = contatore + 1
        Loop While (ret <> 0 And contatore > 10)
        If ret = 0 Then
            ritorno = True
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
            StrCommento += "Calibrazione non a buon fine: " + ret.ToString + " "
        End If
        Return ritorno
    End Function
    Public Function TestAssegnoSpesso() As Integer
        Dim ret As Short
        Dim ritorno As Integer
        ritorno = False
        FaseEsec()

        idmsg = 72
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire Docutest--052 Piegato Doppio ", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        End If


        If (dres = DialogResult.OK) Then
            ret = ls100.Docutest04()
            If (ret = Ls100Class.Costanti.S_LS100_DOUBLE_LEAFING_ERROR) Then
                Sleep(2)
                ret = ls100.Reset(Ls100Class.Costanti.S_RESET_FREE_PATH)
                If (ret = Ls100Class.Costanti.S_LS_OKAY) Then
                    ' spegnere e accendere la perfierica
                Else

                    idmsg = 73
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy("Accendere e Spegnere la periferica", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                    End If


                    TestAspetta(False)
                    TestAspetta(True)
                    ret = Ls100Class.Costanti.S_LS_OKAY
                End If
            Else
                ret = -3000
            End If
            If ret = Ls100Class.Costanti.S_LS_OKAY Then
                ritorno = True
            Else

            End If

        Else
            ritorno = False
            ret = -1000
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If

        Return ritorno
    End Function
    Public Function TestSmagnetizzato(ByVal BCMC7 As Boolean) As Integer
        Dim ritorno As Integer
        Dim ret As Integer
        Dim str, str1 As String
        Dim chcd As New CheckCodeline
        ritorno = False
        ls100.ReturnC.FunzioneChiamante = "Test Smagnetizzato"
        FaseEsec()
        If (BCMC7 = True) Then
            LettureCMC7 = 1
            LettureE13B = 0
            TipoComtrolloLetture = CheckCodeline.CONTROLLO_SMAG_CMC7
            str = "Smagnetizzare un documento CMC7 DOCUTEST--049"
            idmsg = 74
        Else
            LettureE13B = 1
            LettureCMC7 = 0
            TipoComtrolloLetture = CheckCodeline.CONTROLLO_SMAG_E13B
            str = "Smagnetizzare un documento E13B DOCUTEST--046"
            idmsg = 76
        End If

        TipoComtrolloLetture = 0

        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy(str, "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Information, Nothing)
        End If


        If (dres = DialogResult.OK) Then
            If (BCMC7 = True) Then
                str1 = "Inserire un documento CMC7 DOCUTEST--049 Smagnetizzato"
                idmsg = 75
            Else
                str1 = "Inserire un documento E13B DOCUTEST--046 Smagnetizzato"
                idmsg = 77
            End If
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy(str1, "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Information, Nothing)
            End If

            If (dres = DialogResult.OK) Then
                ret = ls100.LeggiCodeline(Ls100Class.Costanti.S_SORTER_BAY1)
                If (ret = Ls100Class.Costanti.S_LS_OKAY) Then

                    chcd.DaLeggereCMC7 = LettureCMC7
                    chcd.DaLeggereE13B = LettureE13B
                    chcd.Tipo_controllo = TipoComtrolloLetture
                    If chcd.CheckCodeline(ls100.CodelineLetta) = 0 Then
                        'conteggio e vedo se ho finito
                        If chcd.Conteggiacodeline() = 1 Then
                            ritorno = True
                        Else
                            ritorno = False
                            idmsg = 78
                            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                            Else
                                dres = MessageboxMy("Controllare la presenza del magnete", "Errore !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                            End If

                        End If
                    Else
                        idmsg = 78
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("Controllare la presenza del magnete", "Errore !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                        End If
                    End If
                Else
                    ' processa do non a buon fine
                    If (ls100.Reply = Ls100Class.Costanti.S_LS100_FEEDEREMPTY) Then

                        Application.DoEvents()
                    Else

                        ViewError(fase)
                    End If
                    ViewError(fase)
                End If
            Else
                ViewError(fase)
            End If
        Else
            ret = 1000
        End If


        If (ritorno = True) Then
            FaseOk()
        Else
            FaseKo()
        End If
        Return ritorno
    End Function
    Public Function LeggiCodelinesOCR(ByVal ocr As Short, ByVal TypeCode As Short) As Integer
        Dim ret As Short
        Dim ii As Integer
        Dim fstop As Boolean = False
        Dim ferror As Boolean = False
        Dim CodelineConfronto As String = ""
        ii = 0
        ret = 0

        '      MessageboxMy("Inserire Documento lettura ", "Attenzione !! ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        While (fstop = False)
            ls100.NomeFileImmagine = zzz + "\Quality\OcrImage" + IndImgF.ToString("0000") + ".bmp"
            IndImgF += 1
            ret = ls100.LeggiCodelineOCR(TypeCode)
            If (ret = Ls100Class.Costanti.S_LS_OKAY) Then
                DisposeImg()
                PFronte.Image = System.Drawing.Image.FromFile(ls100.NomeFileImmagine)
                Pimage.Image = PFronte.Image
                '    LICodeline.Items.Add(ls100.CodelineLetta)
                ' carico la codeline rispetto al tipo passato
                If (TypeCode = Ls100Class.Costanti.S_READ_OPTIC_E13B_X_SWITCH) Then
                    CodelineConfronto = "B0123456789+><012345678901234567890123456789X"
                End If

                If (TypeCode = Ls100Class.Costanti.S_READ_CODELINE_SW_OCRB_ITALY) Then
                    CodelineConfronto = "B0123456789+><012345678901234567890123456789"
                End If

                If String.Compare(ls100.CodelineLetta, CodelineConfronto) = 0 Then
                    ii += 1
                    If ii = ocr Then
                        fstop = True
                        ferror = False
                    End If
                Else
                    'erroe di lettura nella codeline
                    ii = 0
                    fstop = True
                    ferror = True
                    MessageboxMy("Codeline Errata", "Errore !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                End If
            Else
                ' processa do non a buon fine
                If (ls100.Reply = Ls100Class.Costanti.S_LS100_FEEDEREMPTY) Then

                    Application.DoEvents()
                Else
                    fstop = True
                    ferror = True
                    ViewError(fase)
                End If

            End If
        End While
        If ferror Then
            ret = -30000
        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ret
    End Function
    Public Function LeggiCodelinesDouble(ByVal cmc7 As Short, ByVal e13b As Short, ByVal Tipo_controllo As Short) As Integer
        Dim ret As Short

        Dim fstop As Boolean = False
        Dim ferror As Boolean = False
        Dim scan As Short
        Dim ii As Short
        ii = 1
        chcd = New CheckCodeline
        chcd.Reset()
        chcd.Tipo_controllo = Tipo_controllo
        chcd.VisualizzaFinestra(True)
        'chcd.F.Parent = Me

        If (fScannerFronte = True) Then
            scan = Ls100Class.Costanti.S_SIDE_FRONT_IMAGE
        End If
        If (fScannerRetro = True) Then
            scan = Ls100Class.Costanti.S_SIDE_BACK_IMAGE
        End If
        If (fScannerFronte = True) And (fScannerRetro = True) Then
            scan = Ls100Class.Costanti.S_SIDE_ALL_IMAGE
        End If
        ret = 0
        TipoComtrolloLetture = 2

        '      MessageboxMy("Inserire Documento lettura ", "Attenzione !! ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        While (fstop = False)
            ls100.NomeFileImmagine = zzz + "\Dati\CodeFImage" + IndImgF.ToString("0000") + ".bmp"
            ls100.NomeFileImmagineRetro = zzz + "\Dati\CodeRImage" + IndImgR.ToString("0000") + ".bmp"
            IndImgF += 1
            IndImgR += 1

            ret = ls100.LeggiCodelineImmagini(Ls100Class.Costanti.S_SORTER_BAY1, scan, Tipo_controllo)
            If (ret = Ls100Class.Costanti.S_LS100_DOUBLE_LEAFING_ERROR) Then
                If (MessageboxMy("Errore double leafing...Continuare?", "Errore !!!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing) = DialogResult.Yes) Then
                    ret = ls100.Reset(Ls100Class.Costanti.S_RESET_FREE_PATH)
                    ret = Ls100Class.Costanti.S_LS_OKAY
                Else
                    fstop = True
                    ferror = True
                    ViewError(fase)
                End If

            End If
            If (ret = Ls100Class.Costanti.S_LS_OKAY) Then
                If (Not (ls100.CodelineLetta Is Nothing)) Then
                    '   LICodeline.Items.Add(ls100.CodelineLetta)

                    Try
                        DisposeImg()
                        Pimage.Image = System.Drawing.Image.FromFile(ls100.NomeFileImmagine)
                        PImageBack.Image = System.Drawing.Image.FromFile(ls100.NomeFileImmagineRetro)
                    Catch ex As Exception
                        ' DEBUG
                        ' MessageBox.Show(ex.Message + " " + ex.Source, "errore")
                    End Try

                    LettureCMC7 = cmc7
                    LettureE13B = e13b
                    chcd.DaLeggereCMC7 = LettureCMC7
                    chcd.DaLeggereE13B = LettureE13B
                    chcd.Tipo_controllo = TipoComtrolloLetture

                    If chcd.CheckCodeline(ls100.CodelineLetta) = 0 Then
                        'conteggio e vedo se ho finito

                        If chcd.Conteggiacodeline() = 1 Then

                            'Me.Refresh()
                            chcd.F.Refresh()
                            'chcd.p.Show()
                            ' ho finito di contare senza errori
                            fstop = True
                            ret = 1
                        Else
                            ' non ho finito di contare
                            'Me.Refresh()
                            chcd.F.Refresh()
                        End If
                    Else
                        'erroe di lettura nella codeline
                        fstop = True
                        ferror = True
                        MessageboxMy("Codeline Errata: " + ls100.CodelineLetta, "Errore !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                    End If
                End If
            Else
                ' processa do non a buon fine
                If (ls100.Reply = Ls100Class.Costanti.S_LS100_FEEDEREMPTY) Then

                    Application.DoEvents()
                Else
                    fstop = True
                    ferror = True
                    ViewError(fase)
                End If

            End If
        End While
        chcd.F.Refresh()
        chcd.VisualizzaFinestra(False)

        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ret
    End Function
    Public Function LeggiCodelines(ByVal cmc7 As Short, ByVal e13b As Short, ByVal Tipo_controllo As Short) As Integer
        Dim ret As Short

        Dim fstop As Boolean = False
        Dim ferror As Boolean = False
        Dim scan As Short
        Dim ValDoppia As Short
        Dim ii As Short
        ii = 1
        chcd = New CheckCodeline
        chcd.Reset()
        chcd.Tipo_controllo = Tipo_controllo
        chcd.VisualizzaFinestra(True)
        'chcd.F.Parent = Me

        If (fScannerFronte = True) Then
            scan = Ls100Class.Costanti.S_SIDE_FRONT_IMAGE
        End If
        If (fScannerRetro = True) Then
            scan = Ls100Class.Costanti.S_SIDE_BACK_IMAGE
        End If
        If (fScannerFronte = True) And (fScannerRetro = True) Then
            scan = Ls100Class.Costanti.S_SIDE_ALL_IMAGE
        End If
        ret = 0
        TipoComtrolloLetture = 2

        '      MessageboxMy("Inserire Documento lettura ", "Attenzione !! ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        While (fstop = False)
            ls100.NomeFileImmagine = zzz + "\Dati\CodeFImage" + IndImgF.ToString("0000") + ".bmp"
            ls100.NomeFileImmagineRetro = zzz + "\Dati\CodeRImage" + IndImgR.ToString("0000") + ".bmp"
            IndImgF += 1
            IndImgR += 1
            Select Case Tipo_controllo
                Case 5
                    ValDoppia = Ls100Class.Costanti.S_DOUBLE_LEAFING_DEFAULT
                Case 6
                    ValDoppia = Ls100Class.Costanti.S_DOUBLE_LEAFING_LEVEL5
                Case 7
                    ValDoppia = Ls100Class.Costanti.S_DOUBLE_LEAFING_LEVEL2
            End Select
            ret = ls100.LeggiCodelineImmagini(Ls100Class.Costanti.S_SORTER_BAY1, scan, ValDoppia)
            'ret = -85
            Select Case ret
                Case Ls100Class.Costanti.S_LS100_DOUBLE_LEAFING_ERROR
                    ViewError(fase)
                    ls100.Reset(Ls100Class.Costanti.S_RESET_FREE_PATH)
                    fstop = True
                    ferror = True


                Case Ls100Class.Costanti.S_LS100_FEEDEREMPTY
                    'Application.DoEvents()

                Case Ls100Class.Costanti.S_LS_OKAY
                    If (Not (ls100.CodelineLetta Is Nothing)) Then
                        '   LICodeline.Items.Add(ls100.CodelineLetta)
                        Try
                            DisposeImg()
                            Pimage.Image = System.Drawing.Image.FromFile(ls100.NomeFileImmagine)
                            PImageBack.Image = System.Drawing.Image.FromFile(ls100.NomeFileImmagineRetro)
                        Catch ex As Exception
                            ' DEBUG
                            ' MessageBox.Show(ex.Message + " " + ex.Source, "errore")
                        End Try

                        LettureCMC7 = cmc7
                        LettureE13B = e13b
                        chcd.DaLeggereCMC7 = LettureCMC7
                        chcd.DaLeggereE13B = LettureE13B


                        If chcd.CheckCodeline(ls100.CodelineLetta) = 0 Then
                            'conteggio e vedo se ho finito

                            If chcd.Conteggiacodeline() = 1 Then

                                'Me.Refresh()
                                chcd.F.Refresh()
                                'chcd.p.Show()
                                ' ho finito di contare senza errori
                                fstop = True
                                ret = 1
                            Else
                                ' non ho finito di contare
                                'Me.Refresh()
                                chcd.F.Refresh()
                            End If
                        Else
                            'erroe di lettura nella codeline
                            fstop = True
                            ferror = True
                            ret = Ls100Class.Costanti.S_LS100_CODELINE_ERROR
                            idmsg = 79
                            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                dres = MessageboxMy(Messaggio + ls100.CodelineLetta, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                            Else
                                dres = MessageboxMy("Codeline Errata: " + ls100.CodelineLetta, "Errore !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                            End If
                        End If
                    End If
                    ' Errore generico non specificato prima
                Case Else
                    ViewError(fase)
                    ls100.Reset(Ls100Class.Costanti.S_RESET_FREE_PATH)
                    fstop = True
                    ferror = True
                    'ViewError(fase)
            End Select
        End While

        chcd.F.Refresh()
        chcd.VisualizzaFinestra(False)

        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ret
    End Function
    Public Function LeggiImmagine(ByVal side As System.SByte) As Integer
        Dim ritorno As Integer
        Dim ret As Integer
        Dim Str As String
        DirZoom = Application.StartupPath
        Dim a As Short
        a = DirZoom.LastIndexOf("\")
        DirZoom = DirZoom.Substring(0, a)
        If (System.Convert.ToInt16(side) = Ls100Class.Costanti.S_SIDE_FRONT_IMAGE) Then
            Str = "FRONTE"
            If fquality = True Then
                ls100.NomeFileImmagine = zzz + "\Quality\FrontImage" + IndImgF.ToString("0000") + ".bmp"
                DirZoom += "\Quality\"
            Else
                ls100.NomeFileImmagine = zzz + "\Dati\FrontImage" + IndImgF.ToString("0000") + ".bmp"
                DirZoom += "\Dati\"
            End If

        Else
            Str = "RETRO"
            If fquality = True Then
                ls100.NomeFileImmagine = zzz + "\Quality\BackImage" + IndImgR.ToString("0000") + ".bmp"
                DirZoom += "\Quality\"
            Else
                ls100.NomeFileImmagine = zzz + "\Dati\BackImage" + IndImgR.ToString("0000") + ".bmp"
                DirZoom += "\Dati\"
            End If

        End If
        'DEBUG
        'MessageBox.Show(ls100.NomeFileImmagine)
        FileZoomCorrente = ls100.NomeFileImmagine
        idmsg = 80
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire un documento per verificare l'immagine " + Str, "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Question, VetImg(7))
        End If


        If dres = DialogResult.OK Then

            ret = ls100.LeggiImmagini(side)
            If (ret = Ls100Class.Costanti.S_LS_OKAY) Then
                If (System.Convert.ToInt16(side) = Ls100Class.Costanti.S_SIDE_FRONT_IMAGE) Then
                    DisposeImg()
                    PFronte.Image = System.Drawing.Image.FromFile(ls100.NomeFileImmagine)
                    Pimage.Image = PFronte.Image
                    IndImgF += 1
                Else
                    DisposeImg()
                    PRetro.Image = System.Drawing.Image.FromFile(ls100.NomeFileImmagine)
                    Pimage.Image = PRetro.Image
                    IndImgR += 1
                End If
                Application.DoEvents()

                BVisualZoom = True

                'DirZoom += "\Quality\"

                idmsg = 81
                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                    dres = MessageboxMy(Messaggio + Str, Titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                Else
                    dres = MessageboxMy("La qualità dell' immagine " + Str + " è buona?", "ATTENZIONE", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing)
                End If


                If dres = DialogResult.Yes Then
                    ritorno = True
                    fWaitButton = True
                Else

                    fWaitButton = False
                    If (System.Convert.ToInt16(side) = Ls100Class.Costanti.S_SIDE_FRONT_IMAGE) Then

                    Else
                        'If TipoDiCollaudo = COLLAUDO_PERIFERICA_LS100_7 Then
                        '    BCalF.Enabled = True
                        '    BvisualF.Enabled = True
                        '    BAnnulla.Enabled = True
                        'Else
                        '    BcalR.Enabled = True
                        '    BVisualR.Enabled = True
                        '    BAnnulla.Enabled = True
                        'End If
                    End If

                    ' devo aspettare la gestione dei bottoni 
                    ' per lasciare fare all'utente come vuole
                    ritorno = True
                End If
            Else
                If (System.Convert.ToInt16(side) = Ls100Class.Costanti.S_SIDE_FRONT_IMAGE) Then

                Else
                    'If TipoDiCollaudo = COLLAUDO_PERIFERICA_LS100_7 Then
                    '    BCalF.Enabled = True
                    '    BvisualF.Enabled = True
                    '    BAnnulla.Enabled = True
                    'Else
                    '    BcalR.Enabled = True
                    '    BVisualR.Enabled = True
                    '    BAnnulla.Enabled = True
                    'End If
                End If
                ViewError(fase)
            End If
        Else

            If (System.Convert.ToInt16(side) = Ls100Class.Costanti.S_SIDE_FRONT_IMAGE) Then

            Else
                'If TipoDiCollaudo = COLLAUDO_PERIFERICA_LS100_7 Then
                '    BCalF.Enabled = True
                '    BvisualF.Enabled = True
                '    BAnnulla.Enabled = True
                'Else
                '    BcalR.Enabled = True
                '    BVisualR.Enabled = True
                '    BAnnulla.Enabled = True
                'End If
            End If

        End If
        BVisualZoom = False
        If ritorno = True Then
            FaseKo()
        Else
            FaseKo()
        End If
        Return ritorno
    End Function

    'Public Function TestBackground() As Integer
    '    Dim ritorno As Integer
    '    Dim ret As Integer
    '    Dim stringa As String

    '    stringa = ""
    '    ritorno = False

    '    idmsg = 57
    '    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
    '        dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
    '    Else
    '        dres = MessageboxMy("Inserire il documento di calibrazione  DOCUTEST-052 nel Feeder", "Attenzione!!! ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
    '    End If

    '    If dres = DialogResult.OK Then
    '        'Spostato qui se no LsSaveBid dava -18
    '        DisposeImg()

    '        ls100.NomeFileImmagine = zzz + "\Quality\FrontImage" + IndImgF.ToString("0000") + ".bmp"

    '        ls100.NomeFileImmagineRetro = zzz + "\Quality\BackImage" + IndImgR.ToString("0000") + ".bmp"

    '        ret = ls100.TestBackground(Convert.ToInt32(0))
    '    End If
    '    'debug
    '    'ret = Ls40Class.Costanti.S_L40_BACK_BACKGROUND_1



    '    If (ret = Ls100Class.Costanti.S_LS_OKAY) Then
    '        ritorno = True
    '        MessageboxMy("TestBackground Passato", "ARCA", MessageBoxButtons.OK, MessageBoxIcon.Information, Nothing)
    '    Else
    '        ritorno = False
    '        MessageboxMy("TestBackground NON Passato", "ARCA", MessageBoxButtons.OK, MessageBoxIcon.Information, Nothing)
    '        'ret = Ls40Class.Costanti.S_LS_USER_ABORT

    '        Try
    '            Pimage.Image = System.Drawing.Image.FromFile(ls100.NomeFileImmagine)
    '            IndImgF += 1

    '            PImageBack.Image = System.Drawing.Image.FromFile(ls100.NomeFileImmagineRetro)
    '            IndImgR += 1
    '        Catch ex As Exception
    '            ret = Ls100Class.Costanti.S_LS_OKAY
    '        End Try




    '        If (ret = Ls100Class.Costanti.S_LS_FRONT_BACKGROUND Or ret = Ls100Class.Costanti.S_LS_FRONT_BACKGROUND_1 Or ret = Ls100Class.Costanti.S_LS_FRONT_BACKGROUND_2) Then
    '            'BVisualZoom = True
    '            DirZoom = Application.StartupPath
    '            Dim a As Short
    '            a = DirZoom.LastIndexOf("\")
    '            DirZoom = DirZoom.Substring(0, a)
    '            DirZoom += "\Quality\"
    '            FileZoomCorrente = ls100.NomeFileImmagine
    '            ' Zoom Immagine 
    '            ShowZoom(DirZoom, FileZoomCorrente)
    '        End If

    '        If (ret = Ls100Class.Costanti.S_LS_BACK_BACKGROUND Or ret = Ls100Class.Costanti.S_LS_BACK_BACKGROUND_1 Or ret = Ls100Class.Costanti.S_LS_BACK_BACKGROUND_2) Then
    '            'BVisualZoom = True
    '            DirZoom = Application.StartupPath
    '            Dim a As Short
    '            a = DirZoom.LastIndexOf("\")
    '            DirZoom = DirZoom.Substring(0, a)
    '            DirZoom += "\Quality\"
    '            FileZoomCorrente = ls100.NomeFileImmagineRetro
    '            ' Zoom Immagine 
    '            ShowZoom(DirZoom, FileZoomCorrente)
    '        End If

    '        If (Riprova = 0) Then
    '            Fqualit = New CtsControls.FqualImg
    '            Fqualit.ShowDialog()


    '            Select Case Fqualit.ret
    '                Case 0
    '                    ritorno = False
    '                Case 1, 2
    '                    'Scanner GRIGIO
    '                    ritorno = TestScannersPeriferica()
    '                    If ritorno = True Then
    '                        Riprova = 1
    '                        ritorno = TestBackground()
    '                    End If
    '            End Select
    '        End If
    '    End If





    '    Application.DoEvents()


    '    Riprova = 0
    '    If ritorno = True Then
    '        FaseOk()
    '    Else
    '        ViewError(fase)
    '        FaseKo()
    '    End If

    '    BVisualZoom = False

    '    FileTraccia.UltimoRitornoFunzione = ret
    '    FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante


    '    Return ritorno
    'End Function

    'Funzione per test qualita' immagine :
    ' usata solamente per collaudo periferica ls100-6 e ls100-7
    Public Function TestQualitaImmagine(ByVal side As System.Byte) As Integer
        Dim ritorno As Integer
        Dim retzoom As DialogResult
        Dim ret As Integer
        Dim str As String
        'Dim Iquali As Image
        Dim localDate As String
        Dim hour As String

        If ((System.Convert.ToInt16(side) = Ls100Class.Costanti.S_SIDE_FRONT_IMAGE) And fScannerFronte = False) Then
            FaseOk()
            Return True
        Else

        End If

        If ((System.Convert.ToInt16(side) = Ls100Class.Costanti.S_SIDE_BACK_IMAGE) And fScannerRetro = False) Then
            FaseOk()
            Return True
        Else

        End If

        localDate = Now.ToString("yyyyMMdd")
        hour = Now.ToString("hhmmss")

        Fqualit = New CtsControls.FqualImg
        ritorno = False
        fquality = True


        FaseEsec()
        If (System.Convert.ToInt16(side) = Ls100Class.Costanti.S_SIDE_FRONT_IMAGE) Then
            str = "FRONTE"
            'ls100.NomeFileImmagine = zzz + "\Quality\FrontImage" + IndImgF.ToString("0000") + ".bmp"

            'Iquali = VetImg(7)
        Else
            str = "RETRO"
            'ls100.NomeFileImmagine = zzz + "\Quality\BackImage" + IndImgR.ToString("0000") + ".bmp"

            'Iquali = VetImg(9)
        End If

        If (SerialNumberDaStampare = Nothing) Then
            SerialNumberDaStampare = "PROGETTO"
        End If

        If (SerialNumberPiastra = Nothing) Then
            SerialNumberPiastra = "PROGETTO_PIASTRA"
        End If


        ls100.NomeFileImmagine = zzz + "\Images_Scanner\FrontImage" + SerialNumberDaStampare.ToString + "_" + SerialNumberPiastra.ToString + "_" + IndImgF.ToString("0000") + "_" + localDate.ToString + "_" + hour.ToString & ".bmp"
        ls100.NomeFileImmagineRetro = zzz + "\Images_Scanner\BackImage" + SerialNumberDaStampare.ToString + "_" + SerialNumberPiastra.ToString + "_" + IndImgR.ToString("0000") + "_" + localDate.ToString + "_" + hour.ToString & ".bmp"


        idmsg = 82
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio + str, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire un documento per verificare l'immagine " + str, "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Question, Nothing)
        End If


        If dres = DialogResult.OK Then

            ret = ls100.LeggiImmagini(side)
            If (ret = Ls100Class.Costanti.S_LS_OKAY) Then
                If (System.Convert.ToInt16(side) = Ls100Class.Costanti.S_SIDE_FRONT_IMAGE) Then
                    DisposeImg()
                    Try
                        Pimage.Image = System.Drawing.Image.FromFile(ls100.NomeFileImmagine)
                    Catch ex As Exception
                        MessageBox.Show(ex.ToString())
                    End Try
                Else
                    Try
                        PImageBack.Image = System.Drawing.Image.FromFile(ls100.NomeFileImmagineRetro)
                    Catch ex As Exception
                        MessageBox.Show(ex.ToString())
                    End Try
                End If
                Application.DoEvents()

                Dim a As Short

                If (System.Convert.ToInt16(side) = Ls100Class.Costanti.S_SIDE_FRONT_IMAGE) Then
                    BVisualZoom = True
                    DirZoom = Application.StartupPath

                    a = DirZoom.LastIndexOf("\")
                    DirZoom = DirZoom.Substring(0, a)
                    DirZoom += "\Images_Scanner\"
                    FileZoomCorrente = ls100.NomeFileImmagine
                    ' Zoom Immagine 
                    Try
                        retzoom = ShowZoom(DirZoom, FileZoomCorrente)
                    Catch ex As Exception
                        MessageBox.Show(ex.ToString())
                    End Try
                End If

                If (System.Convert.ToInt16(side) = Ls100Class.Costanti.S_SIDE_BACK_IMAGE) Then
                    'If retZoom = DialogResult.OK Then
                    BVisualZoom = True
                    DirZoom = Application.StartupPath

                    a = DirZoom.LastIndexOf("\")
                    DirZoom = DirZoom.Substring(0, a)
                    DirZoom += "\Images_Scanner\"
                    FileZoomCorrente = ls100.NomeFileImmagineRetro
                    ' Zoom Immagine 
                    Try
                        retzoom = ShowZoom(DirZoom, FileZoomCorrente)
                    Catch ex As Exception
                        MessageBox.Show(ex.ToString())
                    End Try

                    'End If
                End If

                If retzoom = DialogResult.OK Then
                    ritorno = True
                Else
                    Fqualit.ShowDialog()

                    Select Case Fqualit.ret
                        Case 0
                            ritorno = False
                        Case 1
                            ritorno = TestScannersPeriferica()
                            If ritorno = True Then
                                ritorno = TestQualitaImmagine(70)
                                If ritorno = True Then
                                    ritorno = TestQualitaImmagine(66)
                                End If

                            End If
                        Case 2
                            ritorno = TestQualitaImmagine(side)
                    End Select
                End If
            Else

                ViewError(fase)

                Select Case ret
                    Case Ls100Class.Costanti.S_LS100_PAPER_JAM
                        ls100.Reset(Ls100Class.Costanti.S_RESET_FREE_PATH)
                End Select
            End If
        Else
            ret = Ls100Class.Costanti.S_LS100_USER_ABORT
        End If
        BVisualZoom = False
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function
    'Funzione per test sfogliatore:
    ' usata solamente per collaudo periferica ls100-6 e ls100-7
    ' quando non c'è la testina MICR ed è presente lo sfogliatore
    Public Function TestSfogliatore(ByVal ndoc As Short) As Integer
        Dim ret As Integer
        Dim ritorno As Integer
        Dim fSolofronte As Boolean
        Dim ii As Short
        ritorno = False
        FaseEsec()
        If fTestinaMicr Or fTestinaCmc7 Or fTestinaE13b Then
            ritorno = True
        Else
            fSolofronte = False
            ls100.NomeFileImmagine = "img"

            idmsg = 83
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Inserire Documenti per prova sfogliatore o pinch roller", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            End If

            For ii = 1 To ndoc
                '  Lab.Text = ii
                If Math.IEEERemainder(ii, 2) = 0 Then
                    ls100.NomeFileImmagine = zzz + "\Dati\SfogliaFImage" + ii.ToString + ".bmp"
                    ret = ls100.LeggiImmagini(Convert.ToSByte(Ls100Class.Costanti.S_SIDE_FRONT_IMAGE))
                Else
                    ls100.NomeFileImmagine = zzz + "\Dati\SfogliaBImage" + ii.ToString + ".bmp"
                    ret = ls100.LeggiImmagini(Convert.ToSByte(Ls100Class.Costanti.S_SIDE_BACK_IMAGE))
                    ' per gestire la periferica c channel che ha solamente lo scanne fronte
                    If ret = Ls100Class.Costanti.S_LS100_COMMAND_NOT_SUPPORTED Then
                        fSolofronte = True
                        ls100.Reset(Ls100Class.Costanti.S_RESET_ERROR)
                        ls100.NomeFileImmagine = zzz + "\Dati\SfogliaFImage" + ii.ToString + ".bmp"
                        ret = ls100.LeggiImmagini(Convert.ToSByte(Ls100Class.Costanti.S_SIDE_FRONT_IMAGE))
                    End If


                End If

                If ret <> Ls100Class.Costanti.S_LS_OKAY Then
                    If ret = Ls100Class.Costanti.S_LS100_FEEDEREMPTY Then
                        idmsg = 84
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("Inserire Documento per test Sfogliatore", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                        End If

                    End If
                    ii -= 1
                Else
                    If Math.IEEERemainder(ii, 2) = 0 Then
                        DisposeImg()
                        PFronte.Image = System.Drawing.Image.FromFile(zzz + "\Dati\SfogliaFImage" + ii.ToString + ".bmp")
                        Pimage.Image = PFronte.Image
                    Else
                        DisposeImg()
                        If fSolofronte = True Then
                            PFronte.Image = System.Drawing.Image.FromFile(zzz + "\Dati\SfogliaFImage" + ii.ToString + ".bmp")
                            Pimage.Image = PFronte.Image
                        Else
                            PRetro.Image = System.Drawing.Image.FromFile(zzz + "\Dati\SfogliaBImage" + ii.ToString + ".bmp")
                            Pimage.Image = PRetro.Image
                        End If
                    End If

                    Application.DoEvents()
                End If
            Next
            If (ret = Ls100Class.Costanti.S_LS_OKAY) Then
                ritorno = True
            End If
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function
    Public Function TestCancellaStorico(ByVal Veloci As Short) As Integer
        Dim ritorno As Integer
        Dim ret As Integer
        ritorno = False
        FaseEsec()
        ret = ls100.EraseHistory()


        Hstorico.Documenti_Trattati = ls100.S_Storico.Documenti_Trattati
        Hstorico.Documenti_Trattenuti = ls100.S_Storico.Documenti_Catturati
        Hstorico.Errori_CMC7 = ls100.S_Storico.Errori_CMC7
        Hstorico.Errori_E13B = ls100.S_Storico.Errori_E13B
        Hstorico.Jam_Feeder = ls100.S_Storico.Jam_Feeder
        Hstorico.Jam_Micr = ls100.S_Storico.Jam_Micr
        Hstorico.Jam_Sorter = ls100.S_Storico.Jam_Sorter
        Hstorico.Num_Accensioni = ls100.S_Storico.Num_Accensioni
        Hstorico.TempoAccensione = ls100.S_Storico.TempoAccensione
        Hstorico.Doc_Timbrati = ls100.S_Storico.Doc_Timbrati




        If ret = Ls100Class.Costanti.S_LS_OKAY Then

            ' se sono all ' inizio del collaudo quindi setto a true il set diagnistic mode 
            ' poi devo settare la velocità a alta quindi 0

            ' altrimenti se sono alla fine , prima di togliere il seti diagnosti mode 
            ' devo alzare la velocità .

            If ret = Ls100Class.Costanti.S_LS_OKAY Then
                ritorno = True
            Else

            End If
        Else

        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno

    End Function
    Public Function TestSerialNumberPiastra() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim fser As New CtsControls.FSerialnum
        ritorno = False
        FaseEsec()
        FileTraccia.StrRet = "Test Serial number Piastra"
        idmsg = 85
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            fser.Text = Messaggio
        Else
            fser.Text = "Inserire SerialNumber Piastra"
        End If


        'If fser.ShowDialog() = DialogResult.OK Then
        '    SerialNumberPiastra = fser.StrSerialNumber
        '    ritorno = True
        '    FileTraccia.VersioneDll = SerialNumberPiastra
        'Else
        '    ViewError(fase)
        'End If
        If fser.ShowDialog() = DialogResult.OK Then
            If fser.StrSerialNumber.Length = LUNGHEZZA_SERIAL_NUMBER_PIASTRA Then
                SerialNumberPiastra = fser.StrSerialNumber
                FileTraccia.VersioneDll = SerialNumberPiastra
                ritorno = True
            Else
                ls100.ReturnC.ReturnCode = Ls100Class.Costanti.S_LS_ERROR_SN_PIASTRA_NOT_CORRECT
                ViewError(fase)
            End If
        Else
            ViewError(fase)
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = "Test Serial number Piastra"
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function
    Public Function TestSerialNumberPerPiastra() As Integer
        Dim ritorno As Integer
        Dim ret As Short

        ritorno = False
        FaseEsec()

        ls100.ReturnC.FunzioneChiamante = "TestSerialNumberPerPiastra"


        Dim datestr As String
        datestr = Date.Now().Day.ToString("00") + Date.Now().Month.ToString("00") + Date.Now().Year.ToString("0000") + "OK"


        MatricolaPeriferica = datestr
        ls100.SSerialNumber = MatricolaPeriferica
        FileTraccia.Matricola = MatricolaPeriferica
        ret = ls100.SerialNumber()
        If ret = Ls100Class.Costanti.S_LS_OKAY Then
            FileTraccia.Matricola = MatricolaPeriferica
            ritorno = True
        Else
            ViewError(fase)
        End If


        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    'Setta il Serial Number a bordo macchina ... 
    Public Function TestSerialNumber() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim fser As New CtsControls.FSerialnum
        ritorno = False
        FaseEsec()


        If fser.ShowDialog() = DialogResult.OK Then
            If fser.StrSerialNumber.Length = LUNGHEZZA_SERIAL_NUMBER Then

                MatricolaPeriferica = fser.StrSerialNumber
                ls100.SSerialNumber = MatricolaPeriferica
                ret = ls100.SerialNumber()
                If ret = Ls100Class.Costanti.S_LS_OKAY Then
                    FileTraccia.Matricola = MatricolaPeriferica
                    ritorno = True
                Else
                    ViewError(fase)
                End If
            Else
                ret = Ls100Class.Costanti.S_LS_STRINGTRUNCATED
                ViewError(fase)
            End If
        Else
            ViewError(fase)
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante

        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function

    'Inserisce il serial number della periferica ...
    Public Function TestSNPerStampa() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim fser As New CtsControls.FSerialnum
        ritorno = False
        FaseEsec()
        ls100.ReturnC.FunzioneChiamante = "TestSNPerStampa"

        idmsg = 154
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            fser.Text = Messaggio
        Else
            fser.Text = "Inserire il Serial number della periferica da collaudare"
        End If


        If fser.ShowDialog() = DialogResult.OK Then
            If fser.StrSerialNumber.Length = LUNGHEZZA_SERIAL_NUMBER Then
                SerialNumberDaStampare = fser.StrSerialNumber



                If ret = Ls100Class.Costanti.S_LS_OKAY Then
                    ritorno = True
                Else
                    ViewError(fase)
                End If
            Else
                ret = Ls100Class.Costanti.S_LS_STRINGTRUNCATED
                ViewError(fase)
            End If


        Else
            ViewError(fase)
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        Return ritorno
    End Function



    Public Function AddRigaTabella(ByVal Tab As DataTable, ByVal s1 As String, ByVal s2 As String, ByVal s3 As String, ByVal s4 As String) As DataRow
        Dim row As DataRow
        row = Tab.NewRow()
        row.Item(0) = s1
        row.Item(1) = s2
        row.Item(2) = s3
        row.Item(3) = s4

        Tab.Rows.Add(row)
        Return row
    End Function

    Public Function AddRigaTabella2(ByVal Tab As DataTable, ByVal s1 As String, ByVal s2 As String) As DataRow
        Dim row As DataRow
        row = Tab.NewRow()
        row.Item(0) = s1
        row.Item(1) = s2

        Tab.Rows.Add(row)
        Return row
    End Function

    Public Function AddRigaTabella1(ByVal Tab As DataTable, ByVal s1 As String) As DataRow
        Dim row As DataRow
        row = Tab.NewRow()
        row.Item(0) = s1

        Tab.Rows.Add(row)
        Return row
    End Function

    'Public Function StampaReportOLD()

    '    Dim strw As String

    '    FaseEsec()

    '    ls100.ReturnC.FunzioneChiamante = "Stampa Report"


    '    Dim column As DataColumn
    '    Dim row As DataRow
    '    Dim T4col As DataTable = New DataTable("prima")
    '    Dim T2col As DataTable = New DataTable("Seconda")
    '    Dim T3col As DataTable = New DataTable("Terza")
    '    Dim Tcol As DataTable = New DataTable("Quarta")
    '    ' Create new DataColumn, set DataType, ColumnName 
    '    ' and add to DataTable.    
    '    column = New DataColumn()
    '    column.DataType = System.Type.GetType("System.String")
    '    column.ColumnName = "DESCRIPTION"
    '    column.ReadOnly = True
    '    column.Unique = False
    '    ' Add the Column to the DataColumnCollection.
    '    T4col.Columns.Add(column)

    '    ' Create second column.
    '    column = New DataColumn()
    '    column.DataType = System.Type.GetType("System.String")
    '    column.ColumnName = "VALUE"
    '    column.ReadOnly = False
    '    column.Unique = False
    '    ' Add the column to the table.
    '    T4col.Columns.Add(column)

    '    column = New DataColumn()
    '    column.DataType = System.Type.GetType("System.String")
    '    column.ColumnName = "DESCRIPTION "
    '    column.ReadOnly = True
    '    column.Unique = False
    '    ' Add the Column to the DataColumnCollection.
    '    T4col.Columns.Add(column)

    '    column = New DataColumn()
    '    column.DataType = System.Type.GetType("System.String")
    '    column.ColumnName = "VALUE "
    '    column.ReadOnly = True
    '    column.Unique = False
    '    ' Add the Column to the DataColumnCollection.
    '    T4col.Columns.Add(column)

    '    column = New DataColumn()
    '    column.DataType = System.Type.GetType("System.String")
    '    column.ColumnName = "COMPLETED AUTOMATED TESTS"
    '    column.ReadOnly = True
    '    column.Unique = False
    '    ' Add the Column to the DataColumnCollection.
    '    T2col.Columns.Add(column)

    '    column = New DataColumn()
    '    column.DataType = System.Type.GetType("System.String")
    '    column.ColumnName = "RESULT"
    '    column.ReadOnly = True
    '    column.Unique = False
    '    ' Add the Column to the DataColumnCollection.
    '    T2col.Columns.Add(column)

    '    column = New DataColumn()
    '    column.DataType = System.Type.GetType("System.String")
    '    column.ColumnName = "DESCRIPTION"
    '    column.ReadOnly = True
    '    column.Unique = False
    '    ' Add the Column to the DataColumnCollection.
    '    T3col.Columns.Add(column)

    '    column = New DataColumn()
    '    column.DataType = System.Type.GetType("System.String")
    '    column.ColumnName = "CHECKED"
    '    column.ReadOnly = True
    '    column.Unique = False
    '    ' Add the Column to the DataColumnCollection.
    '    T3col.Columns.Add(column)

    '    column = New DataColumn()
    '    column.DataType = System.Type.GetType("System.String")
    '    column.ColumnName = "DETAIL RESULTS"
    '    column.ReadOnly = True
    '    column.Unique = False
    '    ' Add the Column to the DataColumnCollection.
    '    Tcol.Columns.Add(column)




    '    report.DocumentUnit = GraphicsUnit.Millimeter

    '    report.PageHeaderMaxHeight = 0

    '    report.DefaultPageSettings.Margins.Top = 10
    '    report.DefaultPageSettings.Margins.Bottom = 10
    '    report.DefaultPageSettings.Margins.Left = 10
    '    report.DefaultPageSettings.Margins.Right = 10

    '    TextStyle.ResetStyles()


    '    TextStyle.Heading1.FontFamily = New FontFamily("Times New Roman")
    '    TextStyle.Heading1.Brush = Brushes.Black
    '    TextStyle.Heading1.SizeDelta = 8.0F
    '    TextStyle.Heading1.StringAlignment = StringAlignment.Center

    '    TextStyle.Normal.FontFamily = New FontFamily("Courier New")
    '    TextStyle.Normal.Size = 10

    '    TextStyle.Heading2.Size = 12
    '    TextStyle.Heading1.Size = 12
    '    TextStyle.Heading1.Bold = True

    '    TextStyle.Normal.Bold = False


    '    '// create a builder to help with putting the table together.
    '    Dim builder As ReportBuilder = New ReportBuilder(report)




    '    '/ Add a simple page header and footer that is the same on all pages.
    '    'builder.AddPageHeader("CTS Electronics S.P.A.", "Product Report", "Page %p/1")
    '    builder.AddPageHeader("Arca Technologies S.r.l", "Product Report", "Page %p/1")

    '    builder.AddPageFooter(String.Empty, DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToShortTimeString, String.Empty)

    '    row = AddRigaTabella(T4col, "Peripheral Name", FileTraccia.Periferica, "Product Line", FileTraccia.LineaProd)
    '    row = AddRigaTabella(T4col, "S/N #", FileTraccia.Matricola, "Order Number", FileTraccia.Ordine_prod)
    '    row = AddRigaTabella(T4col, "Test SW Ver.", FileTraccia.VersioneSW, "Date", FileTraccia.DataCollaudo)
    '    row = AddRigaTabella(T4col, "Manufacter", FileTraccia.Manufacter, "Model", FileTraccia.Periferica)
    '    row = AddRigaTabella(T4col, "Firmware Versione", FileTraccia.VersioneFw, "Tested By", FileTraccia.Operatore)
    '    row = AddRigaTabella(T4col, "Test Duration :", FileTraccia.Durata_Test.Minutes.ToString + ":" + FileTraccia.Durata_Test.Seconds.ToString, "", "")



    '    ' Don't use the full height, instead, use just the height required.

    '    builder.StartLinearLayout(Direction.Vertical)
    '    'builder.AddSection(New SectionLine(Direction.Horizontal))



    '    builder.StartLinearLayout(Direction.Horizontal)
    '    builder.CurrentContainer.UseFullHeight = False

    '    Dim dw As DataView = New DataView(T4col)
    '    builder.AddTable(dw, False)
    '    builder.AddAllColumns(150.0F, True, True)
    '    builder.CurrentSection.MarginRight = 1.8F
    '    builder.CurrentSection.MarginLeft = 1.8F
    '    builder.CurrentSection.MarginTop = 1.8F
    '    builder.CurrentSection.MarginBottom = 1.8F
    '    builder.CurrentSection.VerticalAlignment = VerticalAlignment.Middle


    '    builder.AddText(vbCrLf + vbCrLf + "Inspected by _____________" + vbCrLf + "Audited   by _____________", TextStyle.Heading2)

    '    builder.FinishLinearLayout()

    '    builder.StartLinearLayout(Direction.Horizontal)
    '    '    builder.AddSection(New SectionLine(Direction.Horizontal))




    '    row = AddRigaTabella2(T2col, "SENSOR TEST", "PASSED")
    '    If (fScannerFronte) Then
    '        strw = "Calibration Front Scanner"
    '        row = AddRigaTabella2(T2col, strw, "PASSED")
    '        strw = "Quality Front Image"
    '        row = AddRigaTabella2(T2col, strw, "PASSED")
    '    End If
    '    If (fScannerRetro) Then
    '        strw = "Calibration Rear Scanner"
    '        row = AddRigaTabella2(T2col, strw, "PASSED")
    '        strw = "Quality Rear Image"
    '        row = AddRigaTabella2(T2col, strw, "PASSED")
    '    End If
    '    If (fSfogliatore) Then
    '        strw = "Double Leafing Sensor Calibration"
    '        row = AddRigaTabella2(T2col, strw, "PASSED")
    '        strw = "Double Leafing Sensor Test"
    '        row = AddRigaTabella2(T2col, strw, "PASSED")
    '    End If


    '    If (fTestinaMicr) Or (fTestinaCmc7) Or (fTestinaE13b) Then
    '        strw = "MICR Head Calibration"
    '        row = AddRigaTabella2(T2col, strw, "PASSED")
    '        strw = "MICR Speed Test"
    '        row = AddRigaTabella2(T2col, strw, "PASSED")
    '        strw = "MICR Reader Test"
    '        row = AddRigaTabella2(T2col, strw, "PASSED")
    '    End If

    '    If (fTimbri = True) Then
    '        strw = "Stamp Test"
    '        row = AddRigaTabella2(T2col, strw, "PASSED")
    '    End If

    '    If (fStampante) Then
    '        strw = "Ink Jet Printer Test"
    '        row = AddRigaTabella2(T2col, strw, "PASSED")
    '    End If


    '    If (fBadge <> 0) Then
    '        strw = "Badge Reader Test"
    '        row = AddRigaTabella2(T2col, strw, "PASSED")
    '    End If




    '    Dim dw2 As DataView = New DataView(T2col)
    '    builder.AddTable(dw2, False)
    '    builder.AddAllColumns(200.0F, True, True)
    '    'builder.CurrentSection.HorizontalAlignment = ReportPrinting.HorizontalAlignment.Center
    '    builder.CurrentSection.MarginRight = 1.8F
    '    builder.CurrentSection.MarginLeft = 1.8F
    '    builder.CurrentSection.MarginTop = 1.8F
    '    builder.CurrentSection.MarginBottom = 1.8F
    '    builder.CurrentSection.VerticalAlignment = VerticalAlignment.Top



    '    builder.CurrentContainer.UseFullHeight = False



    '    row = AddRigaTabella2(T3col, "Visual Inspection", "            ")
    '    row = AddRigaTabella2(T3col, "Labels Check", "            ")
    '    row = AddRigaTabella2(T3col, "Serial Number & Corrisponding Label", "            ")
    '    row = AddRigaTabella2(T3col, "Inkjet cartidge Removed Check", "            ")
    '    row = AddRigaTabella2(T3col, "Inkjet cartidge Present in the box", "            ")
    '    row = AddRigaTabella2(T3col, "Unit Clean", "            ")
    '    row = AddRigaTabella2(T3col, "USB Cable Present", "            ")
    '    row = AddRigaTabella2(T3col, "Power supply Present", "            ")
    '    row = AddRigaTabella2(T3col, "Manuals presente in the box", "            ")


    '    Dim dw3 As DataView = New DataView(T3col)
    '    builder.AddTable(dw3, False)
    '    builder.AddAllColumns(255.0F, True, True)
    '    builder.CurrentSection.MarginRight = 1.8F
    '    builder.CurrentSection.MarginLeft = 1.8F
    '    builder.CurrentSection.MarginTop = 1.8F
    '    builder.CurrentSection.MarginBottom = 1.8F
    '    builder.CurrentSection.VerticalAlignment = VerticalAlignment.Top



    '    builder.FinishLinearLayout()


    '    builder.StartLinearLayout(Direction.Horizontal)





    '    '' Don't use the full height, instead, use just the height required.


    '    strw = "DOCUTEST 045  read --> 14 Errors --> 0" + "   " + "DOCUTEST 046  read --> 14 Errors --> 0"
    '    row = AddRigaTabella1(Tcol, strw)

    '    strw = "DOCUTEST 047  read --> 14 Errors --> 0" + "   " + "DOCUTEST 048  read --> 14 Errors --> 0"
    '    row = AddRigaTabella1(Tcol, strw)

    '    strw = "DOCUTEST 049  read --> 14 Errors --> 0" + "   " + "DOCUTEST 050  read --> 14 Errors --> 0"
    '    row = AddRigaTabella1(Tcol, strw)

    '    strw = "DOCUTEST 051  read --> 14 Errors --> 0" + "   " + "DOCUTEST 052  read --> 14 Errors --> 0"
    '    row = AddRigaTabella1(Tcol, strw)


    '    Dim dw4 As DataView = New DataView(Tcol)
    '    builder.AddTable(dw4, False)
    '    builder.AddAllColumns(800.0F, True, True)
    '    builder.CurrentSection.MarginRight = 1.8F
    '    builder.CurrentSection.MarginLeft = 1.8F
    '    builder.CurrentSection.MarginTop = 1.8F
    '    builder.CurrentSection.MarginBottom = 1.8F
    '    builder.CurrentSection.VerticalAlignment = VerticalAlignment.Top


    '    builder.FinishLinearLayout()

    '    builder.FinishLinearLayout()

    '    report.Print()
    '    FaseOk()
    '    Return True
    'End Function

    Function StampaReport()

        'FileTraccia.Matricola = "LS150.321388"
        Dim nomeFileLog As String = Application.StartupPath & "\Report100\" & FileTraccia.Matricola & "_" & DateTime.Now.ToString("yyyyMMdd_hhmmss") & ".txt"
        Dim headerLine As String = ""
        Dim recordLine As String = ""
        Dim larghezza As Integer = 62

        FaseEsec()

        ls100.ReturnC.FunzioneChiamante = "Stampa Report"
        FileTraccia.UltimoRitornoFunzione = 0
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante

        recordLine = "┌────────────────────────── ARCA ─────────────────────────────┐" & vbNewLine & _
                    ("│ Peripheral Name:                   " & FileTraccia.Periferica).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Product Line:                      " & FileTraccia.LineaProd).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ S/N #:                             " & FileTraccia.Matricola).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Product Code:                      " & FileTraccia.Ordine_prod).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Date :                             " & FileTraccia.DataCollaudo).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Manufacter :                       " & FileTraccia.Manufacter).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Model :                            " & FileTraccia.Periferica).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Firmware Version :                 " & FileTraccia.VersioneFw).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Tested By :                        " & FileTraccia.Operatore & "-" & FileTraccia.NomeStazione).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Test Duration :                    " & FileTraccia.Durata_Test.Minutes.ToString + ":" + FileTraccia.Durata_Test.Seconds.ToString).PadRight(larghezza) & "│" & vbNewLine & _
                     "│                                                             │" & vbNewLine & _
                     "├────────────────── COMPLETED AUTOMATED TESTS ────────────────┤" & vbNewLine & _
                     "│                                                             │" & vbNewLine & _
                    ("│ Sensor Test:                             " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Calibration Front Scanner:               " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Quality Front Image:                     " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Calibration Rear Scanner:                " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Quality Rear Image:                      " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Double Leafing Sensor Calibration:       " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Double Leafing Sensor Test:              " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ MICR Head Calibration:                   " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ MICR Speed Test:                         " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ MICR Reader Test:                        " & "PASSED").PadRight(larghezza) & "│" & vbNewLine


        If (fTimbri = True) Then
            recordLine += ("│ Stamp Test:                              " & "PASSED").PadRight(larghezza) & "│" & vbNewLine
        End If

        If (fStampante = True) Then
            recordLine += ("│ Ink Jet Printer Test:                    " & "PASSED").PadRight(larghezza) & "│" & vbNewLine
        End If

        If (fBadge <> 0) Then
            recordLine += ("│ Badge Reader Test:                       " & "PASSED").PadRight(larghezza) & "│" & vbNewLine
        End If

        recordLine += "│                                                             │" & vbNewLine
        recordLine += "├─────────────────────────────────────────────────────────────┤" & vbNewLine
        recordLine += "│                                                             │" & vbNewLine

        recordLine += ("│ Visual Inspection:                       " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ Label Check:                             " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ Serial Number & Corrisponding Label:     " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ Inkjet cartidge Removed Check:           " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ Inkjet cartidge Present in the box:      " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ Unit Clean:                              " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ USB Cable Present:                       " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ Power supply Present:                    " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ Manuals present  in the box:             " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine

        recordLine += "│                                                             │" & vbNewLine
        recordLine += "│────────────────── DETAIL RESULTS ───────────────────────────┤" & vbNewLine
        recordLine += "│                                                             │" & vbNewLine

        recordLine += ("│ DOCUTEST 045  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine & _
              ("│ DOCUTEST 046  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine & _
              ("│ DOCUTEST 047  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine & _
              ("│ DOCUTEST 048  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine & _
              ("│ DOCUTEST 049  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine & _
              ("│ DOCUTEST 050  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine & _
              ("│ DOCUTEST 051  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine & _
              ("│ DOCUTEST 052  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine


        recordLine += "└─────────────────────────────────────────────────────────────┘"

        Dim filelog As New StreamWriter(nomeFileLog)
        filelog.WriteLine(recordLine)
        filelog.Close()

        FaseOk()
        Return True

    End Function


    'Public Function InkDetectorCalibration() As Integer
    Public Function TestInkDetectorCalibration() As Integer


        Dim ritorno As Integer
        Dim ret As Short
        Dim strmsg As String
        Dim valore As Integer
        Dim fAspetta As CtsControls.FWait
        fAspetta = New CtsControls.FWait

        'Defines for Ink Detector sensor  
        Const INKD_CALIBRATION_STEP_1 = 1   '// Preparazione alla calibrazione
        Const INKD_CALIBRATION_STEP_2 = 2   '// Verifica valori di riferimento
        Const INKD_CALIBRATION_STEP_3 = 3   '// Calibrazione sensore 
        Const INKD_CALIBRATION_STEP_4 = 4   '// Verifica finale e Salvataggio dati

        Dim length As Integer = 255
        Dim value As String = New String(" "c, length)



        'Dim ff As New FormInkDetectorLS100
        'ff.ShowDialog(formpadre)

        'fInkDetector = True
        If (fInkDetector = True) Then

            ritorno = False
            FaseEsec()

            ls100.NomeFileInkDetector = Application.StartupPath + "\\InkDetector.ini"

            GetPrivateProfileString("Files", "FileSPT", "None", value, length, ls100.NomeFileInkDetector)

            ls100.NomeFileSPT = Application.StartupPath + "\SPT\" + value
            fAspetta.Label1.Text = "Attendere...Caricamento SPT in corso..."
            fAspetta.Show()
            Application.DoEvents()
            ret = ls100.InkDetectorLoadCmds()
            fAspetta.Hide()

            If (ret = Ls100Class.Costanti.S_LS_OKAY) Then

                strmsg = "Test InkDetector : Inserire nella Casella di Testo il Valore del DOCUTEST --093 di calibrazione"
                idmsg = 280

                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                    StrMessaggio = Messaggio
                Else
                    StrMessaggio = strmsg
                End If


                If InputboxMy(StrMessaggio, "Attenzione", True, valore) = DialogResult.OK Then

                    'If (dres = DialogResult.OK) Then
                    idmsg = 281
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy("Rimettere il DOCUTEST --093 nel feeder", "Attenzione!!! " + strmsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                    End If

                    If dres = DialogResult.OK Then
                        ret = ls100.InkDetectorCalibration(True, System.Convert.ToInt32(valore), INKD_CALIBRATION_STEP_1)
                        If (ret = Ls100Class.Costanti.S_LS_OKAY) Then
                            ret = ls100.InkDetectorCalibration(False, System.Convert.ToInt32(valore), INKD_CALIBRATION_STEP_2)
                            If (ret = Ls100Class.Costanti.S_LS_OKAY) Then


                                'If dres = DialogResult.OK Then
                                Do
                                    idmsg = 281
                                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                        dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                                    Else
                                        dres = MessageboxMy("Rimettere il DOCUTEST --093 nel feeder", "Attenzione!!! " + strmsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                                    End If

                                    ret = ls100.InkDetectorCalibration(False, System.Convert.ToInt32(valore), INKD_CALIBRATION_STEP_3)
                                Loop While ret = Ls100Class.Costanti.S_LS_ERROR_ID_OUT_TOLERANCE

                                If (ret = Ls100Class.Costanti.S_LS_ID_INDK_DETECTED) Then
                                    idmsg = 57
                                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                        dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                                    Else
                                        dres = MessageboxMy("Inserire il documento di calibrazione  DOCUTEST-052 nel Feeder", "Attenzione!!! " + strmsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                                    End If

                                    If dres = DialogResult.OK Then
                                        ret = ls100.InkDetectorCalibration(False, System.Convert.ToInt32(valore), INKD_CALIBRATION_STEP_4)
                                        If (ret = Ls100Class.Costanti.S_LS_OKAY) Then
                                            ritorno = True
                                        End If
                                    End If
                                End If
                                'Else
                                '    'annullato.....
                                '    StrCommento += "Reply :" + ret.ToString + " "
                                'End If
                            Else
                                StrCommento += "Reply :" + ret.ToString + " "
                            End If
                            'ritorno = True
                        Else
                            StrCommento += "Reply :" + ret.ToString + " "
                        End If
                        'Else
                        '    StrCommento += "Abortito Da Utente "
                        'End If
                    Else
                        StrCommento += "Abortito Da Utente "
                    End If
                Else
                    StrCommento += "Abortito Da Utente "
                    'ritorno = True
                End If

                'Try
                '    'visualizzo grafico
                '    Dim ff As New FormInkDetectorLS100
                '    ff.ShowDialog(formpadre)
                'Catch ex As Exception
                '    ritorno = True
                'End Try

            Else
                ritorno = True
            End If
        Else
            ritorno = True
        End If


        If (ritorno = False) Then
            ViewError(fase)
        End If

        If ritorno = True Then
            FaseOk()
        Else 'visualizzo errori a video

            FaseKo()
        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno

    End Function

    'Public Function TestDownloadFwInkDetector() As Integer
    Public Function TestDownloadFwInkDetectorEx() As Integer

        Dim ritorno As Short
        Dim ret As Short

        ritorno = False

        FaseEsec()

        Dim length As Integer = 255
        Dim value As String = New String(" "c, length)

        If (fInkDetector = True) Then
            'Leggo il file Ultrasonic.ini
            ls100.NomeFileInkDetector = Application.StartupPath + "\\InkDetector.ini"

            'Lettura File di Download...
            GetPrivateProfileString("Files", "FileInkDetector", "GS013_00.dwl", value, length, ls100.NomeFileInkDetector)
            If (value = "") Then
                ret = Ls100Class.Costanti.S_LS_OKAY
            Else
                Dim fAspetta As CtsControls.FWait
                fAspetta = New CtsControls.FWait
                fAspetta.Label1.Text = "Attendere...Download Fw InkDetector in corso"
                fAspetta.Show()
                Application.DoEvents()
                ls100.NomeFileInkDetector = Application.StartupPath + "\\FW\\" + value
                'Download Fw Double Feed 
                ret = ls100.TestDownload()
                'la macchina si deve riavviare 
                Sleep(8)
                fAspetta.Hide()


            End If



            If (ret = Ls100Class.Costanti.S_LS_OKAY) Then
                ritorno = True
            Else
                ls100.ReturnC.ReturnCode = ret
                ViewError(fase)
            End If
        Else
            ritorno = True
        End If


        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        Return ritorno

    End Function



    'fase generica per far apparire un messaggio a video all'operatrice...
    Public Function TestMessaggio() As Integer

        Dim ritorno As Short
        Dim ret As Short

        ritorno = False

        FaseEsec()

        If (fInkDetector = True) Then
            idmsg = 286
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Spegnere e Riaccendere la macchina,Premere Ok solo dopo il Bip", "Attenzione!!! ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            End If
            '...
        Else
            ritorno = True
        End If



        If (ret = Ls100Class.Costanti.S_LS_OKAY) Then
            ritorno = True
        Else
            ls100.ReturnC.ReturnCode = ret
            ViewError(fase)
        End If



        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls100.ReturnC.FunzioneChiamante
        Return ritorno

    End Function
    'Public Sub MakeDocumentSenzaRighe(ByVal rep As ReportDocument)

    '    Dim strw As String

    '    rep.DocumentUnit = GraphicsUnit.Millimeter
    '    rep.PageHeaderMaxHeight = 15

    '    TextStyle.ResetStyles()


    '    TextStyle.Heading1.FontFamily = New FontFamily("Times New Roman")
    '    TextStyle.Heading1.Brush = Brushes.DarkBlue
    '    TextStyle.Heading1.SizeDelta = 8.0F
    '    TextStyle.Heading1.StringAlignment = StringAlignment.Center

    '    TextStyle.Normal.FontFamily = New FontFamily("Courier New")
    '    TextStyle.Normal.Size = 10

    '    TextStyle.Heading2.Size = 12
    '    TextStyle.Heading1.Size = 10
    '    TextStyle.Heading1.Bold = True

    '    TextStyle.Normal.Bold = True


    '    '// create a builder to help with putting the table together.
    '    Dim builder As ReportBuilder = New ReportBuilder(rep)



    '    '/ Add a simple page header and footer that is the same on all pages.
    '    builder.AddPageHeader("CTS Electronics S.P.A.", "Product Report", "Page %p/1")
    '    builder.AddPageFooter(String.Empty, DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToShortTimeString, String.Empty)

    '    'builder.StartLinearLayout(Direction.Vertical)



    '    '' scritta 
    '    'builder.AddText("Production Report ", TextStyle.Heading2)



    '    strw = "Peripheral Name : " + FileTraccia.Periferica
    '    strw += "           Product Line : " + FileTraccia.LineaProd
    '    builder.AddText(strw, TextStyle.Heading1)

    '    strw = "Serial Number :" + FileTraccia.Matricola
    '    strw += "            Order Number :" + FileTraccia.Ordine_prod
    '    builder.AddText(strw, TextStyle.Heading1)

    '    strw = "Test SW Version :" + FileTraccia.VersioneSW + "   " + "Date : " + FileTraccia.DataCollaudo + "      Test Duration :" + FileTraccia.Durata_Test.Minutes.ToString + ":" + FileTraccia.Durata_Test.Seconds.ToString
    '    builder.AddText(strw, TextStyle.Heading1)

    '    strw = "Manufacter :" + FileTraccia.Manufacter + "   " + "Model : " + FileTraccia.Periferica + "Dll Ver. :" + FileTraccia.VersioneDll
    '    builder.AddText(strw, TextStyle.Heading1)

    '    strw = "Firmware Version :" + FileTraccia.VersioneFw + "   " + "Tested By : " + FileTraccia.Operatore
    '    builder.AddText(strw, TextStyle.Heading1)





    '    'builder.AddHorizontalLine()
    '    builder.AddText("COMPLETED AUTOMATED TESTS", TextStyle.Heading2)

    '    ' Info Generali


    '    strw = "Sensors Test                      --> PASSED"
    '    builder.AddText(strw, TextStyle.Normal)

    '    If (ordine.ord.Scanner_fronte = 1) Then
    '        strw = "Calibration Front Scanner         --> PASSED"
    '        strw += vbCrLf + "Quality Front Image               --> PASSED"
    '    Else
    '        strw = "Calibration Front Scanner         --> NOT PRESENT"
    '        strw += vbCrLf + "Quality Front Image               --> NOT PRESENT"
    '    End If
    '    builder.AddText(strw, TextStyle.Normal)

    '    If (ordine.ord.Scanner_retro = 1) Then
    '        strw = "Calibration Rear Scanner          --> PASSED"
    '        strw += vbCrLf + "Quality Rear Image                --> PASSED"
    '    Else
    '        strw = "Calibration Rear Scanner          --> NOT PRESENT"
    '        strw += vbCrLf + "Quality Rear Image                --> NOT PRESENT"
    '    End If
    '    builder.AddText(strw, TextStyle.Normal)

    '    If (ordine.ord.DoppiaSfogliatura = ordine.ord.PRESENTE Or ordine.ord.Sfogliatore = ordine.ord.PRESENTE) Then
    '        strw = "Double Leafing Sensor Calibration --> PASSED"
    '        strw += vbCrLf + "Double Leafing Sensor Test        --> PASSED"
    '    Else
    '        strw = "Double Leafing Sensor Calibration --> NOT PRESENT"
    '        strw += vbCrLf + "Double Leafing Sensor Test        --> NOT PRESENT"
    '    End If
    '    builder.AddText(strw, TextStyle.Normal)

    '    If (ordine.ord.Micr <> ordine.ord.NON_PRESENTE) Then
    '        strw = "MICR Head Calibration             --> PASSED"
    '        strw += vbCrLf + "MICR Speed Test                   --> PASSED"
    '        strw += vbCrLf + "MICR Reader Test                  --> PASSED"
    '    Else
    '        strw = "MICR Head Calibration             --> NOT PRESENT"
    '        strw += vbCrLf + "MICR Speed Test                   --> NOT PRESENT"
    '        strw += vbCrLf + "MICR Reader Test                  --> NOT PRESENT"
    '    End If
    '    builder.AddText(strw, TextStyle.Normal)

    '    If (ordine.ord.Inkjet <> ordine.ord.NON_PRESENTE) Then
    '        strw = "Ink Jet Printer Test              --> PASSED"
    '    Else
    '        strw = "Ink Jet Printer Test              --> NOT PRESENT"
    '    End If
    '    builder.AddText(strw, TextStyle.Normal)

    '    If (ordine.ord.Timbro <> ordine.ord.NON_PRESENTE) Then
    '        strw = "Stamp Test                        --> PASSED"
    '    Else
    '        strw = "Stamp Test                        --> NOT PRESENT"
    '    End If
    '    builder.AddText(strw, TextStyle.Normal)

    '    If (ordine.ord.Badge <> ordine.ord.NON_PRESENTE) Then
    '        strw = "Badge Reader Test                 --> PASSED"
    '    Else
    '        strw = "Badge Reader Test                 --> NOT PRESENT"
    '    End If
    '    builder.AddText(strw, TextStyle.Normal)

    '    If (ordine.ord.Ex_ocr = ordine.ord.PRESENTE) Then
    '        strw = "OCR Reader Test                   --> PASSED"
    '    Else
    '        strw = "OCR Reader Test                   --> NOT PRESENT"
    '    End If
    '    builder.AddText(strw, TextStyle.Normal)





    '    builder.AddText("DETAIL RESULTS", TextStyle.Heading2)


    '    If (ordine.ord.Sfogliatore = ordine.ord.PRESENTE Or ordine.ord.DoppiaSfogliatura = ordine.ord.PRESENTE) Then
    '        strw = "DOCUTEST 045 documents readed --> 14 Errors --> 0"
    '        builder.AddText(strw, TextStyle.Normal)
    '        strw = "DOCUTEST 046 documents readed --> 14 Errors --> 0"
    '        builder.AddText(strw, TextStyle.Normal)
    '        strw = "DOCUTEST 047 documents readed --> 14 Errors --> 0"
    '        builder.AddText(strw, TextStyle.Normal)
    '        strw = "DOCUTEST 048 documents readed --> 14 Errors --> 0"
    '        builder.AddText(strw, TextStyle.Normal)
    '        strw = "DOCUTEST 049 documents readed --> 14 Errors --> 0"
    '        builder.AddText(strw, TextStyle.Normal)
    '        strw = "DOCUTEST 050 documents readed --> 14 Errors --> 0"
    '        builder.AddText(strw, TextStyle.Normal)
    '    Else
    '        strw = "DOCUTEST 045 documents readed --> 7 Errors --> 0"
    '        builder.AddText(strw, TextStyle.Normal)
    '        strw = "DOCUTEST 046 documents readed --> 7 Errors --> 0"
    '        builder.AddText(strw, TextStyle.Normal)
    '        strw = "DOCUTEST 047 documents readed --> 7 Errors --> 0"
    '        builder.AddText(strw, TextStyle.Normal)
    '        strw = "DOCUTEST 048 documents readed --> 7 Errors --> 0"
    '        builder.AddText(strw, TextStyle.Normal)
    '        strw = "DOCUTEST 049 documents readed --> 7 Errors --> 0"
    '        builder.AddText(strw, TextStyle.Normal)
    '        strw = "DOCUTEST 050 documents readed --> 7 Errors --> 0"
    '        builder.AddText(strw, TextStyle.Normal)
    '    End If







    '    builder.AddText("OPERATOR VERIFIES", TextStyle.Heading2)

    '    Dim dt As New DataTable("xxx")
    '    dt.Columns.Add("x", GetType(String))
    '    dt.Columns.Add("y", GetType(String))
    '    Dim a(1) As Object
    '    a(0) = " "

    '    strw = "Visual Inspection"
    '    a(1) = strw
    '    dt.Rows.Add(a)



    '    strw = "Labels Check"
    '    a(1) = strw
    '    dt.Rows.Add(a)

    '    strw = "Serial Number & CorrispondingLabel"
    '    a(1) = strw
    '    dt.Rows.Add(a)


    '    strw = "InkJet Cartidge Removed Check "
    '    a(1) = strw
    '    dt.Rows.Add(a)


    '    strw = "InkJet Cartidge Present In the box "
    '    a(1) = strw
    '    dt.Rows.Add(a)


    '    strw = "Unit Clean "
    '    a(1) = strw
    '    dt.Rows.Add(a)


    '    strw = "USB Cable Present"
    '    a(1) = strw
    '    dt.Rows.Add(a)


    '    strw = "Power Supply Present"
    '    a(1) = strw
    '    dt.Rows.Add(a)


    '    strw = "Manuals Present in the box "
    '    'a(1) = strw
    '    'dt.Rows.Add(a)
    '    'builder.AddTable(dt.DefaultView, True)

    '    'builder.AddColumn("x", "Checked", 20, True, True)

    '    'builder.AddColumn("y", "Description", 100.0F, True, True)

    '    'builder.Table.InnerPenHeaderBottom = New Pen(Color.Red, 0.0500000007F)
    '    'builder.Table.OuterPens = rep.NormalPen
    '    'builder.Table.InnerPenRow = rep.ThinPen
    '    'builder.Table.UseFullWidth = True

    '    ' builder.AddHorizontalLine()
    '    builder.AddText("Inspected by ____________________________", TextStyle.Heading2)
    '    builder.AddText("Audited   by ____________________________", TextStyle.Heading2)
    '    builder.FinishLinearLayout()
    'End Sub


#End Region
    Public Function CercaMessaggio(ByVal filename As String, ByRef Id As Integer, ByRef a As String, ByRef b As String, ByRef c As String, ByRef d As String, ByRef img As Image) As Boolean

        Dim freturn As Boolean
        Dim prova As System.Xml.XmlDocument = New System.Xml.XmlDocument()

        freturn = False
        prova.Load(filename)
        Dim xx As XmlNodeList = prova.GetElementsByTagName("T_Messaggi")

        For Each n As XmlNode In xx

            Dim lll As XmlNodeList = n.ChildNodes
            Dim noddo As XmlNode = n.ChildNodes(0)
            If (noddo.InnerText = Id.ToString()) Then
                a = (n.ChildNodes(0).InnerText)
                b = (n.ChildNodes(1).InnerText)
                c = (n.ChildNodes(2).InnerText)

                d = (n.ChildNodes(3).InnerText)

                If (Not (n.ChildNodes(4) Is Nothing)) Then

                    Dim ms As System.IO.MemoryStream
                    ms = New System.IO.MemoryStream()
                    Dim vet(50 * 1024) As Byte


                    vet = System.Convert.FromBase64CharArray(n.ChildNodes(4).InnerText.ToCharArray(), 0, n.ChildNodes(4).InnerText.Length)

                    ms.Write(vet, 0, vet.Length)
                    img = Image.FromStream(ms)
                End If
                freturn = True
            End If
        Next

        Return freturn
    End Function
End Class
