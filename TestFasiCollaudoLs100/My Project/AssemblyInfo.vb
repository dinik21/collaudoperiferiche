﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' Le informazioni generali relative a un assembly sono controllate dal seguente 
' insieme di attributi. Per modificare le informazioni associate a un assembly
' è necessario modificare i valori di questi attributi.

' Controllare i valori degli attributi dell'assembly

<Assembly: AssemblyTitle("TestFasiCollaudoLs100")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("")> 
<Assembly: AssemblyProduct("TestFasiCollaudoLs100")> 
<Assembly: AssemblyCopyright("Copyright ©  2009")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'Se il progetto viene esposto a COM, il GUID seguente verrà utilizzato come ID della libreria dei tipi
<Assembly: Guid("31bca8a5-ccb0-437a-8b58-3334f6c17016")> 

' Le informazioni sulla versione di un assembly sono costituite dai seguenti quattro valori:
'
'      Numero di versione principale
'      Numero di versione secondario 
'      Numero build
'      Revisione
'
' È possibile specificare tutti i valori oppure impostare i valori predefiniti per i numeri relativi alla revisione e alla build 
' utilizzando l'asterisco (*) come descritto di seguito:

<Assembly: AssemblyVersion("1.0.*")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
