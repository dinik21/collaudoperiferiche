Imports System.Windows.Forms

<Serializable()> Public Class trace

    Public Const NOME_FILE As String = "Trace_"



    Public Class Test
        Public IDTest As Integer
        Public Tentativi As Integer
        Public Commento As String
    End Class

    ''' <summary>
    ''' Intestazione collaudo    '''</summary>
    Public Id_Piastra As String
    Public Periferica As String
    Public DataCollaudo As DateTime
    Public Ordine_prod As String
    Public Matricola As String
    Public Operatore As String
    Public LineaProd As String
    Public VersioneSW As String
    Public VersioneFw As String
    Public VersioneDll As String
    Public Manufacter As String
    Public Durata_Test As TimeSpan
    Public Note As String
    Public NomeStazione As String



    Private TestList As New ArrayList
    Private TestList2 As New ArrayList


    ' indica il numero di errori giornalieri per ogni test di collaudo
    Public ErroriperTest(40) As Int16
    Public ErroriDll(500) As Int16
    Public Nomefile As String
    Public StrRet As String


    <NonSerialized()> Public streamServer As System.Net.Sockets.NetworkStream
    Dim FServer As Boolean


    Public UltimoRitornoFunzione As Integer


    Public FotoPersorso As Int16
    Public FotoDp As Int16
    Public ScannerF As Int16
    Public ScannerR As Int16
    Public LunghezzaImmagine As Int16
    Public CalMicr As Int16
    Public ValueMicr As Single
    Public ViewSignal As Int16
    Public CheckDocSpeed As Int16
    Public AssegnoSmagnetizzato As Int16
    Public TimbroFronte As Int16
    Public TimbroRetro As Int16
    Public Stampante As Int16
    Public Pulsanti As Int16
    Public LetturaCMC7 As Int16
    Public LetturaE13B As Int16
    Public QualImageFreonte As Int16
    Public QualImageRetro As Int16
    Public Reserved(128) As Byte
    Public NumFileSalvati As Integer
    Dim zzz As String
    Dim sss As String
    Dim ind As String

    Public Sub ReadErroriPerTest()
        Dim str As String
        Dim nerr As Integer
        Dim cerr As Integer
        Dim vStr() As String

        sss = Windows.Forms.Application.StartupPath()
        ind = sss.LastIndexOf("\")
        zzz = sss.Substring(0, ind)

        Nomefile = zzz + "\Server\" + Date.Today.ToShortDateString + "_Err_Fasi.txt"
        Nomefile = Nomefile.Replace("/", "_")
        Try
            Dim fs As System.IO.StreamReader = New IO.StreamReader(Nomefile)

            Do
                str = fs.ReadLine()
                vStr = str.Split(" ")
                nerr = CInt(vStr(0))
                cerr = CInt(vStr(1))
                ErroriperTest(nerr - 10000) = cerr
            Loop Until str Is Nothing

            fs.Close()
            NumFileSalvati += 1

        Catch ex As Exception

        End Try

        Nomefile = zzz + "\Server\" + Date.Today.ToShortDateString + "_Err_Dll.txt"
        Nomefile = Nomefile.Replace("/", "_")
        Try
            Dim fs As System.IO.StreamReader = New IO.StreamReader(Nomefile)

            Do
                str = fs.ReadLine()
                vStr = str.Split(" ")
                nerr = CInt(vStr(0))
                cerr = CInt(vStr(1))
                ErroriDll(nerr) = cerr
            Loop Until str Is Nothing

            fs.Close()
            NumFileSalvati += 1

        Catch ex As Exception

        End Try

    End Sub
    'Public rep As ReportBuilder
    Public Sub Addtest(ByVal id As Integer, ByVal nTentativi As Integer, ByVal Commento As String)
        Dim test As New Test
        test.IDTest = id
        test.Commento = Commento
        TestList.Add(test)

    End Sub

    Public Sub AddtestProduzione(ByVal id As Integer, ByVal nTentativi As Integer, ByVal Commentop As String)
        Dim test As New Test
        Dim ii As Integer
        Dim ErroreDll As Integer
        test.IDTest = id
        test.Tentativi = nTentativi
        test.Commento = Commentop
        TestList2.Add(test)
        ' se c� un errore incremento il numero di errori
        ' al test associato
        If (Commentop <> "0") Then
            ii = id - 10000
            ErroriperTest(ii) += 1
            ' incremento il contatore dell'errore della dll
            ErroreDll = CInt(Commentop)
            If ErroreDll < 0 Then
                ' considero solamente gli errori per ora
                ErroreDll = Math.Abs(ErroreDll)
                ' prendo l'errore come indice di un vettore...
                Select Case ErroreDll
                    Case 1000 To 1999
                        ErroreDll -= 1000
                    Case 2000 To 2999
                        ErroreDll -= 2000
                    Case 3000 To 3999
                        ErroreDll -= 3000
                    Case 6000
                        ErroreDll = 401
                    Case 9999
                        ErroreDll = 400
                End Select
                ErroriDll(ErroreDll) += 1
            End If

        End If

    End Sub

    Public Sub Trace()
        sss = Windows.Forms.Application.StartupPath()
        ind = sss.LastIndexOf("\")
        zzz = sss.Substring(0, ind)

        Nomefile = "Trace.bin"
        FServer = False

        DataCollaudo = DateTime.Now

        Periferica = ""
        Matricola = ""

        FotoPersorso = 0
        FotoDp = 0
        ScannerF = 0
        ScannerR = 0
        LunghezzaImmagine = 0
        CalMicr = 0
        ValueMicr = 0
        ViewSignal = 0
        CheckDocSpeed = 0
        AssegnoSmagnetizzato = 0
        TimbroFronte = 0
        TimbroRetro = 0
        Stampante = 0
        Pulsanti = 0
        LetturaCMC7 = 0
        LetturaE13B = 0
        QualImageFreonte = 0
        QualImageRetro = 0
        Reserved(128) = 0
        NumFileSalvati = 0
        For ii As Integer = 0 To 30
            ErroriperTest(ii) = 0
        Next
        For ii As Integer = 0 To 500
            ErroriDll(ii) = 0
        Next
    End Sub

    Public Sub WriteFileBinario()

        'Try
        '    Dim fs As System.IO.StreamWriter = New IO.StreamWriter(Nomefile)
        '    fs.WriteLine(DataCollaudo)
        '    fs.WriteLine(Periferica)
        '    fs.WriteLine(Matricola)
        '    fs.WriteLine(FotoPersorso)
        '    fs.WriteLine(FotoDp)
        '    fs.WriteLine(ScannerF)
        '    fs.WriteLine(ScannerR)
        '    fs.WriteLine(LunghezzaImmagine)
        '    fs.WriteLine(CalMicr)
        '    fs.WriteLine(ValueMicr)
        '    fs.WriteLine(ViewSignal)
        '    fs.WriteLine(CheckDocSpeed)
        '    fs.WriteLine(AssegnoSmagnetizzato)
        '    fs.WriteLine(TimbroFronte)
        '    fs.WriteLine(TimbroRetro)
        '    fs.WriteLine(Stampante)
        '    fs.WriteLine(Pulsanti)
        '    fs.WriteLine(LetturaCMC7)
        '    fs.WriteLine(LetturaE13B)
        '    fs.WriteLine(QualImageFreonte)
        '    fs.WriteLine(QualImageRetro)
        '    fs.WriteLine(Reserved)
        '    fs.Close()
        'Catch ex As Exception

        'End Try
    End Sub

    Public Sub WriteFileTesto()
        Dim str As String

        Dim t1 As New Test

        sss = Windows.Forms.Application.StartupPath()
        ind = sss.LastIndexOf("\")
        zzz = sss.Substring(0, ind)

        Nomefile = zzz + "\Server\" + NOME_FILE + NumFileSalvati.ToString + ".txt"
        Dim fs As System.IO.StreamWriter = New IO.StreamWriter(Nomefile)
        Try
            str = CreaBuffer()
            fs.Write(str)
            fs.Close()
            NumFileSalvati += 1
        Catch ex As Exception

        End Try
        Nomefile = zzz + "\Server\" + Date.Today.ToShortDateString + ".txt"
        Nomefile = Nomefile.Replace("/", "_")
        Dim fs2 As System.IO.StreamWriter = New IO.StreamWriter(Nomefile, True)
        Try
            str = CreaBufferRiga()
            fs2.Write(str)
            fs2.Close()
            NumFileSalvati += 1
        Catch ex As Exception

        End Try

        Nomefile = zzz + "\Server\" + Date.Today.ToShortDateString + "_Err_Fasi.txt"
        Nomefile = Nomefile.Replace("/", "_")

        Dim fs3 As System.IO.StreamWriter = New IO.StreamWriter(Nomefile, True)
        Try
            str = ""
            For iii As Integer = 0 To 35
                str += (iii + 10000).ToString + " " + ErroriperTest(iii).ToString + vbCrLf
            Next
            fs3.Write(str)
            fs3.Close()
            NumFileSalvati += 1
        Catch ex As Exception

        End Try


        ' salvo gli errori della dll
        Nomefile = zzz + "\Server\" + Date.Today.ToShortDateString + "_Err_Dll.txt"
        Nomefile = Nomefile.Replace("/", "_")

        Dim fs4 As System.IO.StreamWriter = New IO.StreamWriter(Nomefile, True)
        Try
            str = ""
            For iii As Integer = 0 To 500
                str += (iii).ToString + " " + ErroriDll(iii).ToString + vbCrLf
            Next
            fs4.Write(str)
            fs4.Close()
            NumFileSalvati += 1
        Catch ex As Exception

        End Try
    End Sub

    Public Function connectServer() As Boolean
        Dim ritorno As Boolean

        ritorno = False
        Try
            Dim a As New System.Net.Sockets.TcpClient("172.18.0.183", 8888)
            streamServer = a.GetStream
            ritorno = True
        Catch ex As Exception
        End Try

        Return ritorno
    End Function

    Public Function WriteonServer() As Boolean
        Dim str As String
        ' Dim iind As Integer
        Dim ritorno As Integer

        ' Dim risposta As String
        Dim ArrC(512) As Byte
        Dim ii As Integer
        Dim myReadBuffer(1024) As Byte
        Dim myCompleteMessage As [String] = ""
        Dim numberOfBytesRead As Integer = 0

        '        Dim ip As New System.Net.IPAddress(172180183)
        'Dim stream As New System.Net.Sockets.NetworkStream(a.)
        Try
            Dim a As New System.Net.Sockets.TcpClient("172.18.0.183", 8888)
            Dim stream As System.Net.Sockets.NetworkStream = a.GetStream
            ' Send the message to the connected TcpServer. 
            str = CreaBuffer()

            stream.Write(System.Text.Encoding.ASCII.GetBytes(str), 0, str.Length)
            ii = 0
            ' Incoming message may be larger than the buffer size.
            Do
                numberOfBytesRead = stream.Read(myReadBuffer, 0, myReadBuffer.Length)
                myCompleteMessage = [String].Concat(myCompleteMessage, System.Text.Encoding.ASCII.GetString(myReadBuffer, 0, numberOfBytesRead))
            Loop While stream.DataAvailable

            stream.Close()

        Catch ex As Exception
            MessageBox.Show(" ie ne al server su")
        End Try
        If myCompleteMessage.StartsWith("2|") Then
            ' ricezione OK
            ritorno = True
        Else
            ritorno = False
            Dim ss() As String
            ss = myCompleteMessage.Split("|")
            StrRet = ss(1)
        End If
        Return ritorno
    End Function

    Public Sub reset()
        '  Dim ii As Integer

        TestList.Clear()
        TestList2.Clear()
    End Sub
    Private Function CreaBuffer() As String
        Dim str As String
        Dim iind As Integer
        Dim t1 As New Test

        str = "||||||||||||||||||2|" + vbCrLf
        str += "id_piastra " + Id_Piastra + vbCrLf
        str += "periferica " + Periferica + vbCrLf
        str += "data_collaudo " + DataCollaudo + vbCrLf
        str += "ordine_prod " + Ordine_prod + vbCrLf
        str += "matricola " + Matricola + vbCrLf
        str += "operatore " + Operatore + vbCrLf
        str += "lineaprod " + LineaProd + vbCrLf
        str += "versione_sw " + VersioneSW + vbCrLf
        str += "versione_fw " + VersioneFw + vbCrLf
        str += "versione_dll " + VersioneDll + vbCrLf
        str += "manufacturer " + Manufacter + vbCrLf
        str += "durata_test " + Durata_Test.ToString + vbCrLf

        For iind = 0 To TestList.Count - 1
            t1 = TestList(iind)
            str += vbCrLf + "TEST:" + vbCrLf
            str += "id_test " + t1.IDTest.ToString + vbCrLf
            str += "commento " + t1.Commento
            str += "/TEST:" + vbCrLf
        Next
        str += "|"
        Return str
    End Function


    Private Function CreaBufferRiga() As String
        Dim str As String = ""
        Dim iind As Integer
        Dim t1 As New Test

        str += vbCrLf + Id_Piastra + " "
        str += Periferica + " "
        str += DataCollaudo + " "
        str += Ordine_prod + " "
        str += Matricola + " "
        str += Operatore + " "
        str += LineaProd + " "
        str += VersioneSW + " "
        str += VersioneFw + " "
        str += VersioneDll + " "
        str += Manufacter + " "
        str += Durata_Test.ToString + " "

        For iind = 0 To TestList2.Count - 1
            t1 = TestList2(iind)
            str += t1.IDTest.ToString + " "
            str += t1.Commento.ToString + " "
        Next

        Return str
    End Function

End Class
