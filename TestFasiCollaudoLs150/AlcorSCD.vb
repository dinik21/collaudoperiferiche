Module Alco


    Public strReaderName As String
    Public ReaderName(50) As String
    Public intReaderCount As Integer


    ' Resource Manager context
    Public lngContext As Long
    ' handle to card
    Public lngCard As Long
    ' active protocol negotiated
    Public lngActiveProtocol As Long

    Public lngOldCardStatus As Long
    Public Protocol_Supported As String

    Public udtReaderStates(1) As SCARD_READERSTATEA

    Public Const lngBufferSize As Long = 2048
    Public Const WORD_PAGE_SIZE As Long = 8
    'Public Const VenderString As String = "Alcor Micro Corp." Alcor Micro, Corp. EMV USB smart card reader 0
    'Public Const ProductString As String = "SmardCard"
    Public Const ProductString As String = "EMV USB smart card reader"
    Public Const VenderString As String = "Alcor Micro, Corp."
    'Public Const ProductString As String = "PC/SC USB smart card reader"
    'Public Const VenderString As String = "Gemplus"
    'Public Const ProductString As String = "GemPC430"
    'Public Const VenderString As String = "ALCOR MICRO CORP"
    'Public Const ProductString As String = "EMV Smartcard Reader"


    Public IsReaderLost As Boolean
    Public bCurSlotNum As Byte

    Public Function Alcor_ReadMCard() As Long
        Dim lngResult As Long
        Dim lngResult2 As Long
        Dim iAddress As Integer
        Dim lngInBufferLen As Long
        Dim bReplyBuffer(1024) As Byte
        Dim lngReplyLen As Long

        iAddress = 0

        ' set the reader mode to I2C mode
        '    lngResult = Set_EEPROM_Card_Mode

        lngInBufferLen = 128
        iAddress = 0
        lngReplyLen = 0
        'fmMain.lblMCContent.Caption = ""

        lngResult = SCardBeginTransaction(lngCard)
        '   lngResult2 = AlcorMCardRead(lngCard, &H50, iAddress, lngInBufferLen, aReplyBuffer.byteData(0), lngReplyLen)
        lngResult2 = AT24CxxCmd_Read(lngCard, bCurSlotNum, &H50, iAddress, lngInBufferLen, bReplyBuffer(0), lngReplyLen)
        lngResult = SCardEndTransaction(lngCard, SCARD_LEAVE_CARD)
        If (lngResult2 = 0) Then
            For i = 0 To lngReplyLen - 1
                '   fmMain.lblMCContent.Caption = fmMain.lblMCContent.Caption & Byte2Char(bReplyBuffer(i)) & " "
            Next i
        Else
            Alcor_ReadMCard = lngResult2
        End If



    End Function


    'Set the reader mode to I2C mode
    'Public Function Set_EEPROM_Card_Mode() As Long

    'Dim OutBuffer(2) As Byte
    'Dim InBuffer(1) As Byte


    'OutBuffer(1) = &H70 'Clock (KHz)
    'OutBuffer(0) = &H50  'Address

    'OpenI2C = SCardControl(lngCard, IOCTL_I2C_COMMAND, OutBuffer(0), 2, _
    '                        InBuffer(0), 0, lngControlReplyLen)


    'End Function

    ' send data to EEPROM card
    'Public Function ReadMCard(Address As Integer, InBufferLen As Long, aInBuffer As ARRAY_TYPE, lngReplyLen As Long) As Long
    '    Dim OutBuffer(4) As Byte
    '    Dim InBuffer(2048) As Byte
    '    Dim lngResult As Long
    '    Dim OutBufferLen As Long
    '    Dim DevAddr As Long

    '   DevAddr = &H50

    '  OutBuffer(0) = &H50 + (Address \ 256)  ' start Address
    ' OutBuffer(1) = Address Mod 256
    '  OutBuffer(2) = InBufferLen Mod 256
    '  OutBuffer(3) = InBufferLen \ 256 ' read &H100 bytes



    '    OutBufferLen = 4
    '    lngReplyLen = 0

    '    ReadMCard = AlcorMCardRead(lngCard, DevAddr, Address, InBufferLen, aInBuffer.byteData(0))
    '    ReadMCard = AlcorMCardRead(lngCard, 80, 0, 128, aInBuffer.byteData(0))

    'send command to get data from EEPROM card
    'OutBuffer(4)=[Address_LowByte,Address_HighByte,InDataLength_LowByte,InDataLength_HighByte]
    '    ReadMCard = SCardControl(lngCard, IOCTL_I2C_READ, OutBuffer(0), OutBufferLen, _
    '                        aInBuffer.byteData(0), InBufferLen, lngReplyLen)
    'End Function



    'Write data to EEPROM card
    'Public Function WriteMCard(ByVal Address As Long, ByVal PageSize As Long, iOutBufferLen As Long, aOutbuffer As ARRAY_TYPE) As Long

    '    Dim lngResult As Long
    '    Dim InBuffer(2) As Byte
    '    Dim InBufferLen As Byte
    '    Dim OutBufferLen As Long
    '    Dim bOutbuffer(1028) As Byte

    '    lngControlReplyLen = 0

    '    strOutBuffer = Trim(strOutBuffer)

    '    For i = 1 To iOutBufferLen
    '        bOutBuffer(3 + i) = Val("&H" & Mid(strOutBuffer, 3 * (i - 1) + 1, 2))
    '    Next

    '    bOutbuffer(0) = &H50 + (Address \ 256)
    '    bOutbuffer(1) = Address Mod 256   ' start Address
    '    bOutbuffer(2) = PageSize
    '    bOutbuffer(3) = iOutBufferLen Mod 256
    '    bOutbuffer(4) = iOutBufferLen \ 256 ' read &H100 bytes
    '    OutBufferLen = iOutBufferLen + 5
    '    For i = 0 To iOutBufferLen - 1
    '        bOutbuffer(i + 5) = aOutbuffer.byteData(i)
    '    Next i




    'bOutBuffer=[Address_LowByte,Address_HighByte,OutDataLength_LowByte,OutDataLength_HighByte,Data0,Data1,Data2,.....]
    '   WriteMCard = SCardControl(lngCard, IOCTL_I2C_WRITE, bOutbuffer(0), OutBufferLen, _
    '                      InBuffer(0), InBufferLen, lngControlReplyLen)

    'End Function




    'connect to the reader
    Public Function ConnectAlcorReader() As Long
        Dim lngResult As Long

        lngCard = 0
        lngActiveProtocol = 0

        If (IsReaderLost = True) Then
            lngResult = SCardDisconnect(lngCard, SCARD_LEAVE_CARD)
            lngResult = SCardConnectA(lngContext, strReaderName, SCARD_SHARE_DIRECT, _
                        0, lngCard, lngActiveProtocol)
            If (lngResult = 0) Then
                '                Call SetUnresponsedCardToMCardMode
                lngResult = SCardDisconnect(lngCard, SCARD_LEAVE_CARD)
                IsReaderLost = False
            End If
        End If
        lngResult = SCardConnectA(lngContext, strReaderName, SCARD_SHARE_SHARED, _
                        SCARD_PROTOCOL_Tx, lngCard, lngActiveProtocol)
        '    If lngresult = 0 Then

        '        fmMain.sbStatus.Panels(1).Text = strReaderName
        '    Else
        '        fmMain.sbStatus.Panels(1).Text = "No Reader"
        '        Exit Function
        '    End If



    End Function

    Public Function GetSynCardATR(ByVal ATRstring As ARRAY_TYPE, ByRef lngControlReplyLen As Long) As Long

        Dim OutBuffer(1) As Byte

        'frank
        GetSynCardATR = SCardControl(lngCard, IOCTL_GET_SYNCARD_ATR + &HC0& * bCurSlotNum, OutBuffer(0), 0, _
                                ATRstring.byteData(0), 4, lngControlReplyLen)
    End Function



    'Send a command IOCTL_SET_UNRESPONSED_CARD_TO_MCARD to Alcor driver.
    'The command make the driver accept an unresponsed card(EEPROM card), and generate a ATR string
    'to cheat PC/SC subsystem as it was a T0 card.
    Public Sub SetUnresponsedCardToMCardMode()

        Dim OutBuffer(1) As Byte
        Dim InBuffer(1) As Byte

        'lngResult = SCardControl(lngCard, IOCTL_SET_UNRESPONSED_CARD_TO_MCARD + &HC0& + bCurSlotNum, OutBuffer(0), 0, _
        '                       InBuffer(0), 0, lngControlReplyLen)
    End Sub




    'Send a command IOCTL_CLEAR_UNRESPONSED_CARD_TO_MCARD to Alcor driver.
    'The command undo IOCTL_SET_UNRESPONSED_CARD_TO_MCARD command
    Public Sub ClearUnresponsedCardToMCardMode()

        Dim OutBuffer(1) As Byte
        Dim InBuffer(1) As Byte

        'lngResult = SCardControl(lngCard, IOCTL_CLEAR_UNRESPONSED_CARD_TO_MCARD + &HC0& * bCurSlotNum, OutBuffer(0), 0, _
        '                       InBuffer(0), 0, lngControlReplyLen)
    End Sub


    'This routine distinquish the EEPROM card from T0 card.
    'After we sent IOCTL_SET_UNRESPONSED_CARD_TO_MCARD to the driver, the driver will generate a T0 ATR
    'for an unresponsed card(EEPROM card) to cheat PC/SC subsystem as it was a T0 Card.
    'so we distinquish the EEPROM card from T0 card by its ATR string
    Public Function IsEEPROMCard(ByVal ATRdata As ARRAY_TYPE, ByVal lngATRLen As Long) As Boolean
        Dim strAlcorEEPROMCardATR As String
        Dim strCurrentATR As String
        Dim i As Byte

        If (lngATRLen <> 18) Then
            IsEEPROMCard = False
            Exit Function
        End If

        'This historical bytes is generated by the driver if the card is an unresponsed card(EPPROM card)
        strAlcorEEPROMCardATR = "I2C_Card=No_ATR"
        strCurrentATR = ""
        For i = 1 To 15
            strCurrentATR = strCurrentATR & Chr(ATRdata.byteData(2 + i))
        Next i

        If (strCurrentATR = strAlcorEEPROMCardATR) Then
            IsEEPROMCard = True
        Else
            IsEEPROMCard = False
        End If



    End Function

    Public Function IsSynchronousCard(ByVal ATRdata As ARRAY_TYPE, ByVal lngATRLen As Long) As Boolean
        Dim strAlcorSynCardATR_0 As String
        Dim strAlcorSynCardATR_1 As String
        Dim strAlcorSynCardATR_2 As String
        Dim strAlcorSynCardATR_3 As String
        Dim strCurrentATR As String
        Dim i As Byte

        If (lngATRLen <> 18) Then
            IsSynchronousCard = False
            Exit Function
        End If

        'This historical bytes is generated by the driver if the card is an unresponsed card(EPPROM card)
        strAlcorSynCardATR_0 = "Sle4418-28"
        strAlcorSynCardATR_1 = "Sle4432-42"
        strAlcorSynCardATR_2 = "InphonCard"
        strAlcorSynCardATR_3 = "AT88SC1608"
        strCurrentATR = ""
        For i = 1 To 10
            strCurrentATR = strCurrentATR & Chr(ATRdata.byteData(2 + i))
        Next i

        If (strCurrentATR = strAlcorSynCardATR_0 Or strCurrentATR = strAlcorSynCardATR_1 Or strCurrentATR = strAlcorSynCardATR_2 Or strCurrentATR = strAlcorSynCardATR_3) Then
            IsSynchronousCard = True
        Else
            IsSynchronousCard = False
        End If



    End Function

    'Public Function MC_StressTest() As Long
    '    Dim iAddress As Integer
    '    Dim lngOutBufferLen As Long
    '    Dim lngReplyLen As Long
    '    Dim bOutBuffer(1024) As Byte
    '    Dim bReplyBuffer(1024) As Byte
    '    Dim lngResult As Long
    '    Dim lngInBufferLen As Long
    '    Dim lngResult2 As Long
    '    Dim refBuffer(1024) As Byte
    '    Dim PageSize As Long




    '    iAddress = Val("&H" & fmMCard.txtRAddress.Text)
    '    PageSize = Val("&H" & fmMCard.txtPageSize.Text)
    '    lngOutBufferLen = Val("&H" & fmMCard.txtRLen.Text)
    '    For i = 1 To lngOutBufferLen
    '        refBuffer(i - 1) = Int(255 * Rnd)
    '    Next

    '    'write data to EEPROM card, write WORD_PAGE_SIZE bytes each time.
    '    lngResult = SCardBeginTransaction(lngCard)

    '    For i = 1 To lngOutBufferLen
    '        bOutBuffer(i - 1) = refBuffer(i - 1)
    '    Next

    '    lngResult = AT24CxxCmd_Write(lngCard, bCurSlotNum, &H50, iAddress, PageSize, lngOutBufferLen, bOutBuffer(0))
    '    If (lngResult <> 0) Then
    '        MC_StressTest = lngResult
    '        lngResult = SCardEndTransaction(lngCard, SCARD_LEAVE_CARD)
    '        Exit Function
    '    End If

    '    ' read back the card data
    '    lngInBufferLen = 0
    '    iAddress = Val("&H" & fmMCard.txtRAddress.Text)
    '    lngInBufferLen = Val("&H" & fmMCard.txtRLen.Text)
    '    lngReplyLen = 0
    '    lngResult = AT24CxxCmd_Read(lngCard, bCurSlotNum, &H50, iAddress, lngInBufferLen, bReplyBuffer(0), lngReplyLen)
    '    MC_StressTest = lngResult
    '    lngResult = SCardEndTransaction(lngCard, SCARD_LEAVE_CARD)
    '    For i = 1 To Val("&H" & fmMCard.txtRLen.Text)
    '        If (bReplyBuffer(i - 1) <> refBuffer(i - 1)) Then
    '            MC_StressTest = 8
    '            Exit Function
    '        End If
    '    Next i





    'End Function

    'Public Function IsAT45D041Card(ByVal ATRdata As ARRAY_TYPE, ByVal lngATRLen As Long) As Boolean
    '    Dim strAlcorAT45D041CardATR As String
    '    Dim strCurrentATR As String
    '    Dim i As Byte

    '    If (lngATRLen <> 18) Then
    '        IsAT45D041Card = False
    '        Exit Function
    '    End If

    '    'This historical bytes is generated by the driver if the card is an unresponsed card(EPPROM card)
    '    strAlcorAT45D041CardATR = "AT45D041"
    '    strCurrentATR = ""
    '    For i = 1 To 15
    '        strCurrentATR = strCurrentATR & Chr(ATRdata.byteData(2 + i))
    '    Next i

    '    If (strCurrentATR = strAlcorAT45D041CardATR) Then
    '        IsAT45D041Card = True
    '    Else
    '        IsAT45D041Card = False
    '    End If



    'End Function

    ' e.g. 64 ==>40 ,10==>0A
    Public Function Byte2Char(ByVal OneByte As Byte) As String

        Dim tempA As String

        tempA = Hex(OneByte)
        If (Len(tempA) = 1) Then
            tempA = "0" & tempA
        End If

        Byte2Char = tempA

    End Function

    Public Function Str2SLE4442CMD(ByVal strCommand As String) As aSLE4442commmand

        Str2SLE4442CMD.bControl = Val("&H" & Mid(strCommand, 1, 2))
        Str2SLE4442CMD.bAddress = Val("&H" & Mid(strCommand, 4, 2))
        Str2SLE4442CMD.bData = Val("&H" & Mid(strCommand, 7, 2))

    End Function
End Module