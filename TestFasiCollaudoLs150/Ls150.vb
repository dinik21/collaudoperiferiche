﻿Imports System.Windows.Forms
Imports System.Drawing
'Imports ReportPrinting
Imports System.Xml
Imports System.Data
Imports System.IO.Compression
Imports System.IO
Imports System.Text




Public Class PerifLs150

    Public Const COLLAUDO_PIASTRA_LS150 = 1
    Public Const COLLAUDO_PERIFERICA_LS150 = 1
    Public Const LUNGHEZZA_SERIAL_NUMBER = 12
    Public Const LUNGHEZZA_SERIAL_NUMBER_PIASTRA = 16
    Public Const ALTEZZA_DOC_78 = 826
    Public Const LUNGHEZZA_DOC_78 = 1653
    Public Const TITOLO_DEF_MESSAGGIO = ""
    Public Const TEST_SPEED_200_DPI = 0
    Public Const TEST_SPEED_300_DPI = 1
    Public Const TEST_SPEED_75_DPM = 2
    Public Const TEST_SPEED_300_DPI_UV = 3
    Public Const TEST_SPEED_300_COLOR = 4
    Public Const MARGINE_RIGHT = 1.8F
    Public Const MARGINE_LEFT = 1.8F
    Public Const MARGINE_TOP = 1.0F '1.8F
    Public Const MARGINE_BOTTOM = 1.0F '1.8F
    Public Const SAMPLE_VALUE_MIN = 107.0F
    Public Const SAMPLE_VALUE_MAX = 116.0F
    Public Const SFOGlIATORE_50_DOC_PRESENTE = 65
    Public Const NOME_CHIAVETTA = "CTS_KEY"
    Public Const NOME_FILE_CHIAVETTA = "Cts_file_Key.txt"
    Public Const NOME_STRINGA_CHIAVETTA = "Hub Usb test OK"
    Public Const LOCALDB As String = "\LocalDB"
    Public Const MESSAGGI As String = "\Messaggi"
    Public Const VERSIONE As String = "C.T.S. TestFasiCollaudoLs150.dll -- Ver. 2.00 Rev 010 date 08/04/2011"
    Public Const SW_TOP_IMAGE = 1 '(bit1)
    Public Const SW_BAR2D = 2     '(bit2)
    Public Const SW_TOP_BAR2D = 3 '(bit1+2)
    Public Const SW_IQA = 4
    Public Const SW_MICROHOLE = 8
    Public Const SW_BARCODE_MICROHOLE = 10
    Public Const SW_BARCODE_MICROHOLE_IQA = 14
    Public Const NO_CLEAR_BLACK = 0
    Public Const CLEAR_ALL_BLACK = 1
    Public Const CLEAR_AND_ALIGN_IMAGE = 2
    Public Const VEL_ALTA = 0
    Public Const VEL_BASSA = 1

    Dim ls150 As Ls150Class.Periferica
    'Dim BVisualZoom As Boolean
    Dim IndImgF As Integer
    Dim IndImgR As Integer
    Dim fWaitButton As Boolean
    Dim zzz As String
    Dim FileZoomCorrente As String
    Dim StrMessaggio As String
    Dim VetImg(20) As Image
    Dim ff As CtsControls.Message
    Dim ff2 As CtsControls.Message2
    Dim Riprova As Short
    Public Hstorico As CStorico
    Public FileTraccia As New trace
    Public fase As Object 'BarraFasi.PhaseProgressBarItem
    Public DirZoom As String
    Public StrCommento As String
    Public Pimage As PictureBox
    Public PFronte As PictureBox
    Public PRetro As PictureBox
    Public PImageBack As PictureBox
    Public TipoDiCollaudo As Integer
    Public LettureCMC7 As Integer
    Public LettureE13B As Integer
    Public chcd As CheckCodeline
    Public TipoComtrolloLetture As Integer
    Public formpadre As Windows.Forms.Form
    Public FLed As CtsControls.Led
    Public Fqualit As CtsControls.FqualImg
    Public BIdentificativo As Boolean
    'Public report As ReportPrinting.ReportDocument
    'Public report2 As ReportPrinting.TextStyle
    Public DatiFine As DatiFineCollaudo
    Public Ver_firm1 As String
    Public Ver_firm2 As String
    Public Ver_firm3 As String
    Public Ver_firm4 As String
    Public Ver_firm5 As String
    'Public Ver_firm6 As String
    'Public Ver_firm7 As String
    'Public Ver_firm8 As String
    Public Ver_firm9 As String

    Public fTimbri As Boolean
    Public fStampante As Integer


    Public fBadge As Integer ' 0 1 2 3 
    Public fSfogliatore As Integer
    Public fSoftware As Integer
    Public fcardCol As Integer
  
    Public fZebrato As Integer
    Public fTestFascioni As Integer
    Public fMacchinaUv As Boolean
    Public MatricolaPiastra As String
    Public SerialNumberPiastra As String
    Public SerialNumberDaStampare As String
    Public MatricolaPeriferica As String
    Private VettoreOpzioni As Array
    Public VersioneTest As String
    Public VersioneDll_1 As String
    Public VersioneDll_0 As String
    Public PathMessaggi As String
    Dim TipodiPeriferica As Integer
    Dim Titolo, Messaggio, Descrizione, IdMessaggio As String
    Dim dres As DialogResult
    Dim immamsg As Image
    Dim idmsg As Integer


    Public fTestinaMicr As Boolean
    Public fScannerFronte As Boolean
    Public fScannerRetro As Boolean
    Public fStampanteFissa As Boolean
    Public fVelBassa As Boolean
    Public FForzaCartaColori As Boolean
    Public FMacchinaColori As Boolean
    Public fCard As Boolean
    'Public StrUsb As String
    'Public RigheContigueFronte As Integer
    'Public RigheContigueRetro As Integer
    'Public RigheTotaliFronte As Integer
    'Public RigheTotaliRetro As Integer

    Private Sub writeDebug(ByVal x As String, ByVal nFile As String)
        'Dim path As String = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
        Dim path As String = Application.StartupPath
        Dim FILE_NAME As String = path & "\" & nFile & ".txt" '"\mydebug.txt"
        'MsgBox(FILE_NAME)
        If System.IO.File.Exists(FILE_NAME) = False Then
            System.IO.File.Create(FILE_NAME).Dispose()
        End If
        ''''''''''''''''
        If x = "Cancella File" Then
            System.IO.File.Delete(FILE_NAME)
        Else
            Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
            objWriter.WriteLine(x)
            objWriter.Close()
        End If
    End Sub

    Public Declare Function GetPrivateProfileString Lib "kernel32" Alias _
        "GetPrivateProfileStringA" (ByVal lpApplicationName As String, _
            ByVal lpKeyName As String, _
            ByVal lpDefault As String, _
            ByVal lpReturnedString As StringBuilder, _
            ByVal nSize As Integer, ByVal lpFileName As String) As Integer

    Public Function ReadValue(ByVal section As String, ByVal key As String, ByVal Path As String) As String
        Dim sb As New System.Text.StringBuilder(255)
        Dim i = GetPrivateProfileString(section, key, "Failed", sb, 255, Path)
        Return sb.ToString()
    End Function

    Public Sub PerifLs150()
        Dim sss As String
        Dim ind As Integer

        'fCalibOld = False

        ls150 = New Ls150Class.Periferica
        Hstorico = New CStorico
        Pimage = New PictureBox
        PFronte = New PictureBox
        PRetro = New PictureBox
        PImageBack = New PictureBox
        sss = Application.StartupPath()
        ind = sss.LastIndexOf("\")
        '        zzz = sss.Substring(0, ind)
        zzz = sss

        'report = New ReportPrinting.ReportDocument
        DatiFine = New DatiFineCollaudo

        BIdentificativo = False

        fTestinaMicr = False
        fTimbri = False
        fScannerFronte = False
        fScannerRetro = False
        fStampante = 0
        fStampanteFissa = False
        fVelBassa = False
        fBadge = 0 ' 0 1 2 3 
        fSfogliatore = 0
        fSoftware = 0
        fcardCol = 0
        FForzaCartaColori = False
        FMacchinaColori = False
        fZebrato = 0
        fMacchinaUv = False

        VersioneTest = VERSIONE

        VersioneDll_0 = ls150.VersioneDll_0
        VersioneDll_1 = ls150.VersioneDll_1

        ' imposto una periferica che non esiste....
        TipodiPeriferica = 0

        Titolo = ""
        Messaggio = ""
        Descrizione = ""
        IdMessaggio = ""
        'StrUsb = "3232"



        'RigheContigueFronte = 0
        'RigheContigueRetro = 0
        'RigheTotaliFronte = 0
        'RigheTotaliRetro = 0
        'FGulliver = False

    End Sub

    Public Sub ImpostaFlagPerConfigurazione(ByVal Vettore As Array)
        Dim ii As Short
        VettoreOpzioni = Vettore

        ' resetto i flag della configurazione da impostare. , sono da azzerare tutti altrimenti al cambio ordine si tiene quelle precedenti ...
        fTestinaMicr = False
        fTimbri = False
        fScannerFronte = False
        fScannerRetro = False
        fStampante = 0
        fStampanteFissa = False
        fZebrato = 0
        fVelBassa = False
        fBadge = 0
        fSfogliatore = 0
        fSoftware = 0
        fcardCol = 0
        Riprova = 0
        fCard = False

        fcardCol = False
        FForzaCartaColori = False
        FMacchinaColori = False
        fMacchinaUv = False    'devo azzerare ogni se cambio ordine senza chiudere il programma !
        'su Ls150 non serve impostare il tipo scanner (Uv/colore/grigio) , e'gia' tutto nel FW

        ' Controllo le opzioni che vanno da 0 a length -2
        ' perchè in ultim posizione trovo la Periferica
        For ii = 0 To VettoreOpzioni.Length - 2
            Select Case VettoreOpzioni(ii)
                Case 6 To 8
                    fTestinaMicr = True
                Case 14 To 16
                    fTimbri = True
                Case 10
                    fScannerFronte = True
                Case 11
                    fScannerRetro = True
                Case 12
                    fScannerFronte = True
                    fScannerRetro = True
                Case 18
                    fStampante = True
                Case 19
                    fStampante = True
                    fStampanteFissa = True
                    'Case 45  velocita' alta settata  articolo OPZ11
                Case 46 'velocita' bassa settata      articolo OPZ11
                    fVelBassa = True
                Case 22
                    fBadge = 1
                Case 50
                    fBadge = 2
                Case 52
                    fBadge = 3
                Case 59 ' card badge
                    fCard = True
                Case 65
                    fSfogliatore = SFOGlIATORE_50_DOC_PRESENTE
                Case 67
                    fSoftware = SW_TOP_IMAGE   'PAGAMENTO ? 
                Case 68
                    fSoftware = SW_BAR2D       'UTILIZZATO  ??
                Case 69
                    fSoftware = SW_TOP_BAR2D   'UTILIZZATO  ??
                Case 70
                    fcardCol = 1   'usata da un ordine ....
                    'AGGIUNTE 30-09-2014
                Case 76
                    fSoftware = SW_IQA                      'NON USATO
                Case 77
                    fSoftware = SW_MICROHOLE                'NON USATO
                Case 78
                    fSoftware = SW_BARCODE_MICROHOLE        'NON USATO
                    '19-02-2015
                Case 79
                    fSoftware = SW_BARCODE_MICROHOLE_IQA    'NON USATO
                    fcardCol = 1 'alzo solo questo flag perche' e' un altro BIT
                Case 80
                    fSoftware = SW_BARCODE_MICROHOLE_IQA    'NON USATO
                Case 81 ' Ale scanner zebrato
                    fScannerFronte = True
                    fScannerRetro = True
                    fZebrato = 1
            End Select
        Next
        TipodiPeriferica = VettoreOpzioni(VettoreOpzioni.Length - 1)

        PathMessaggi = Application.StartupPath + LOCALDB + MESSAGGI + TipodiPeriferica.ToString + ".xml"
    End Sub

    Public Sub CaricaRes()
        Try
            Dim ii As Integer
            Dim rsxr As Resources.ResXResourceReader
            Dim a As Resources.ResourceSet


            rsxr = New Resources.ResXResourceReader(Application.StartupPath + "\img2.resx")
            a = New Resources.ResourceSet(rsxr)

            Dim en As IDictionaryEnumerator = a.GetEnumerator()

            ' Goes through the enumerator, printing out the key and value pairs.

            ' finestra di massaggio in esecuzione

            ii = 0
            While en.MoveNext()
                Select Case en.Key.ToString
                    Case "pul2.jpg"
                        VetImg(0) = en.Value
                    Case "scanf.JPG"
                        VetImg(1) = en.Value
                    Case "pul1.jpg"
                        VetImg(2) = en.Value
                    Case "dp.JPG"
                        VetImg(3) = en.Value
                    Case "scanr.JPG"
                        VetImg(4) = en.Value
                    Case "badge.JPG"
                        VetImg(5) = en.Value
                    Case "cmicr.JPG"
                        VetImg(6) = en.Value
                    Case "qualim.JPG"
                        VetImg(7) = en.Value
                    Case "micrb.JPG"
                        VetImg(8) = en.Value
                    Case "qualimr.jpg"
                        VetImg(9) = en.Value
                    Case "docu055.JPG"
                        VetImg(10) = en.Value
                End Select
                VetImg(8) = VetImg(6)
                VetImg(4) = VetImg(0)

                'VetImg(ii) = en.Value

                'ii += 1
                'Me.Refresh()
            End While
            rsxr.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.InnerException.ToString)
        End Try

    End Sub

    Public Sub Sleep(ByVal sec As Integer)
        Dim iniTime, currTime As DateTime
        iniTime = DateTime.Now
        While (True)
            currTime = DateTime.Now
            Dim diff2 As System.TimeSpan
            ' diff2 gets 55 days 4 hours and 20 minutes.
            diff2 = System.DateTime.op_Subtraction(currTime, iniTime)

            If (diff2.Seconds = sec) Then
                Return
            End If
        End While
    End Sub

    Public Sub DisposeImg()
        If Not Pimage.Image Is Nothing Then
            Pimage.Image.Dispose()
            Pimage.Image = Nothing
        End If
        If Not PFronte.Image Is Nothing Then
            PFronte.Image.Dispose()
            PFronte.Image = Nothing
        End If
        If Not PRetro.Image Is Nothing Then
            PRetro.Image.Dispose()
            PRetro.Image = Nothing
        End If
        If Not PImageBack.Image Is Nothing Then
            PImageBack.Image.Dispose()
            PImageBack.Image = Nothing
        End If
        ' Refresh the form
        Application.DoEvents()
    End Sub

    Public Sub ViewError(ByVal p As Object) 'BarraFasi.PhaseProgressBarItem)
        Dim sss As String
        Dim ccostante As New Ls150Class.Costanti
        sss = ""
        sss = ccostante.GetDesc(ls150.ReturnC.ReturnCode)
        MessageboxMy(ls150.ReturnC.FunzioneChiamante + " " + ls150.ReturnC.ReturnCode.ToString + "--> " + sss, "Attenzione !!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
    End Sub

    Public Sub FaseOk()
        ' azzero allimizio test il campo note
        fase.Font = New Font(FontFamily.GenericSansSerif, 7)
        fase.SetState = 1

        fase.IsBlinking = True
        fase.IsSelected = True

    End Sub

    Public Sub FaseKo()

        fase.Font = New Font(FontFamily.GenericSansSerif, 7)
        fase.SetState = 0

        fase.IsBlinking = True
        fase.IsSelected = True

    End Sub

    Public Sub FaseEsec()

        fase.Font = New Font(FontFamily.GenericSansSerif, 13)
        FileTraccia.Note = ""
        fase.SetState = 2

        fase.IsBlinking = True
        fase.IsSelected = True

    End Sub

    Public Function ShowZoom(ByVal dir As String, ByVal file As String, Optional ByVal VisualizzaZomm As Boolean = True) As DialogResult
        ' visualizzo subito lo zoom dell immagine

        Dim f As New CtsControls.ZoomImage
        f.FileCorrente = file
        f.DirectoryImage = dir
        f.effettuazoom = VisualizzaZomm
        f.ShowDialog()
        Return f.Result
    End Function

    Public Function MessageboxMy(ByVal testo As String, ByVal titolo As String, ByVal bt As MessageBoxButtons, ByVal ic As MessageBoxIcon, ByVal Img As Image) As DialogResult
        'CtsControls.Message   LO USO PER LE IMMAGINI !
        ff = New CtsControls.Message
        ff.Text = titolo
        ff.testo = testo
        ff.icona = ic
        ff.pulsanti = bt
        ff.Imag.Image = Img
        'If BVisualZoom = True Then
        '    ff.Bzoom.Visible = True
        '    ff.Directory = DirZoom
        '    ff.FileCorrente = FileZoomCorrente
        'End If
        ff.ShowDialog(formpadre)

        If Not ff.Imag.Image Is Nothing Then
            ff.Imag.Image.Dispose()
            ff.Imag.Image = Nothing
        End If

        formpadre.Refresh()
        Return ff.result

    End Function

    Public Function MessageboxMy2(ByVal testo As String, ByVal titolo As String, ByVal bt As MessageBoxButtons, ByVal ic As MessageBoxIcon, ByVal Img As Image, ByVal Id As String, ByVal Img2 As Image) As DialogResult
        'CtsControls.Message2   LO USO SOLO PER IL TESTO !
        ff2 = New CtsControls.Message2
        ff2.Text = titolo
        ff2.testo = testo
        ff2.icona = ic
        ff2.pulsanti = bt
        ff2.Imag.Image = Img
        ff2.LId.Text = Id
        ff2.PUser.Image = Img2
        'If BVisualZoom = True Then
        '    ff.Bzoom.Visible = True
        '    ff.Directory = DirZoom
        '    ff.FileCorrente = FileZoomCorrente
        'End If
        ff2.ShowDialog(formpadre)
        formpadre.Refresh()
        Return ff2.result

    End Function

    Public Function InputboxMy(ByVal testo As String, ByVal titolo As String, ByVal Check As Boolean, ByRef valore As Integer, ByVal ValMin As Integer, ByVal ValMax As Integer) As DialogResult
        Dim ff As New CtsControls.InputBox
        ff.Check = Check
        ff.Text = titolo
        ff.Messaggio = testo
        ff.valoreMinimo = ValMin
        ff.valoreMassimo = ValMax
        ff.ShowDialog(formpadre)
        valore = ff.Valore

        Return ff.result
    End Function

    Public Function TestDownloadFwBase(ByVal VersioneFw As String) As Integer

        Dim ritorno As Short


        ritorno = False

        'FaseEsec()

        Dim FwPath As String

        FwPath = Application.StartupPath + "\\FIRMWARE\\"

        'Ricerco nella cartella FIRMWARE se c'e' presente il file DWL con il nome ....
        If IO.Directory.Exists(FwPath) Then
            'Dim files() As String
            VersioneFw = VersioneFw.Replace(".", "_")
            For Each file As String In My.Computer.FileSystem.GetFiles(FwPath, FileIO.SearchOption.SearchTopLevelOnly, "*.dwl")
                If (file.Contains(VersioneFw)) Then
                    ls150.FileNameDownload = file.ToString()
                    Dim fAspetta As CtsControls.FWait
                    fAspetta = New CtsControls.FWait
                    fAspetta.Label1.Text = "Attendere...Download FW " + VersioneFw + "in corso"
                    fAspetta.Show()
                    Application.DoEvents()
                    'Download Fw Double Feed 
                    ritorno = ls150.TestDownload()
                    'la macchina si deve riavviare 
                    Sleep(8)
                    fAspetta.Hide()

                End If
            Next


        End If


        Return ritorno

    End Function

    Public Function ControllaVersione() As Integer
        Dim ret As Integer

        'Dim ch As Char()
        'Dim ch2 As Char()
        ret = 0

        'valori.....
        'Ver_firm1 ->Versione FW macchina
        'Ver_firm2 ->Data Release
        'Ver_firm3->Paletta Motorizzata(OPZ)
        'Ver_firm4->BOARD(HEX)
        'Ver_firm5->FPGA

        'controllo che la versione sia uguale a quella scritta nell ordine
        ' ch = ls150.SBoardNr.ToCharArray
        'ch2 = ord.Board.ToCharArray
        If (ls150.SVersion.Length <= 0 Or String.Compare(ls150.SVersion.TrimEnd, Ver_firm1) = 0) Then
            If (ls150.SDateFW.Length <= 0 Or String.Compare(ls150.SDateFW.TrimEnd, Ver_firm2) = 0) Then
                'se presente la Paletta Motorizzata (opz 65) cancella il valore memorizzato in Ver_firm3 corrispondente al fw della Paletta Motorizzata (PMxxx) - Dino 03/09/2021
                If fSfogliatore = 65 Then Ver_firm3 = ""
                If (ls150.SFeederVersion.Length <= 0 Or String.Compare(ls150.SFeederVersion.TrimEnd, Ver_firm3) = 0) Then
                    Dim str As String = System.Convert.ToInt32(Ver_firm4, 16).ToString()
                    If (ls150.SBoardNr = str) Then
                        If (ls150.SCpldNr.Length <= 0 Or String.Compare(ls150.SCpldNr.TrimEnd, Ver_firm5) = 0) Then
                            ret = 1

                        Else
                            'errore fpga
                            ret = Ls150Class.Costanti.S_LS150_FPGA_NOT_OK
                            idmsg = 87
                            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                'dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                                Messaggio = Messaggio + "   " + String.Format("Richiesto : {0}  Trovato: {1}", Ver_firm5, ls150.SCpldNr)
                                dres = MessageboxMy(Messaggio, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                            Else
                                dres = MessageboxMy("Versione FPGA non corrispondente", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                            End If


                        End If
                    Else
                        ' errore board
                        ret = Ls150Class.Costanti.S_LS150_BOARD_NOT_OK
                        idmsg = 90
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            'dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                            Messaggio = Messaggio + "   " + String.Format("Richiesto : {0}  Trovato: {1}", str, ls150.SBoardNr)
                            dres = MessageboxMy(Messaggio, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                        Else
                            dres = MessageboxMy("Versione BOARD non corrispondente", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                        End If


                    End If
                Else
                    ' errore feeder
                    idmsg = 86
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        'dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Messaggio = Messaggio + "   " + String.Format("Richiesto : {0}  Trovato: {1}", Ver_firm3, ls150.SFeederVersion)
                        dres = MessageboxMy(Messaggio, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                    Else
                        dres = MessageboxMy("Versione Feeder non corrispondente", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                    End If


                End If
            Else
                ' errore data
                ret = Ls150Class.Costanti.S_LS150_DATA_NOT_OK
                idmsg = 89
                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                    'dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Messaggio = Messaggio + "   " + String.Format("Richiesto : {0}  Trovato: {1}", Ver_firm2, ls150.SDateFW)
                    dres = MessageboxMy(Messaggio, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                Else
                    dres = MessageboxMy("Data Versione non corrispondente", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                End If


            End If
        Else
            'ret = TestDownloadFwBase(Ver_firm1)  'deve rifare tutti i controlli , non solo Fw Base ...
            'If (ret = 0) Then
            'Download Ok 
            'Else

            ' errore versione
            ret = Ls150Class.Costanti.S_LS150_FIRMWARE_NOT_OK
            idmsg = 88
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                Messaggio = Messaggio + "   " + String.Format("Richiesto a DB: {0}  Trovato in piastra: {1}", Ver_firm1, ls150.SVersion)
                'dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                dres = MessageboxMy(Messaggio, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
            Else
                dres = MessageboxMy("Versione Firmware non corrispondente", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
            End If
        End If
        'End If



        Return ret
    End Function

    Public Function TestIdentificativo() As Integer
        Dim ritorno As Integer
        Dim ret As Integer
        'Dim aaa As Single
        'Dim Version As String
        ' inizializzazioni variabili 
        ls150.ReturnC.FunzioneChiamante = "TestIdentificativo"

        'prova
        'TestMicroForatura(3)

        FaseEsec()
        ritorno = False

        'questa funzione viene chiamata piu' volte per cui non faccio apparire il MessageBOX
        'idmsg = 191
        'If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
        '    dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        'Else
        '    dres = MessageboxMy("Collegare ORA la periferica da Collaudare", "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
        'End If

        'writeDebug("Cancella File", "TestIdentificativo")
        'writeDebug("Inizio test", "TestIdentificativo")

        'chiamata a funzione 
        ret = ls150.Identificativo()
        'ls150.Identificativo()
        BIdentificativo = True

        MatricolaPiastra = ls150.SSerialNumber
        FileTraccia.VersioneFw = ls150.SVersion
        If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
            If System.Convert.ToInt16(ls150.SIdentStr(0)) And &H2 Then ' == 0x02) == (Velocita))						// 00000010
                FileTraccia.Note += "Veloctià Bassa" + vbCrLf
            End If
            If System.Convert.ToInt16(ls150.SIdentStr(0)) And &H8 Then ' == 0x08) == (inkjet))						// 00000010
                FileTraccia.Note += "Inkjet" + vbCrLf
            End If
            If System.Convert.ToInt16(ls150.SIdentStr(0)) And &H20 Then ' == 0x20) == (timbro))						// 00000010
                FileTraccia.Note += "Timbro" + vbCrLf
            End If
            If System.Convert.ToInt16(ls150.SIdentStr(1)) And &H1 Then ' == 0x01) == (scannerfronte))						// 00000010
                FileTraccia.Note += "Scanner Fronte" + vbCrLf
            End If
            If System.Convert.ToInt16(ls150.SIdentStr(1)) And &H2 Then ' == 0x02) == (scannerretro))						// 00000010
                FileTraccia.Note += "Scanner Retro" + vbCrLf
            End If
            If System.Convert.ToInt16(ls150.SIdentStr(0)) And &H10 Then ' == 0x02) == (sfogliatore50))						// 00000010
                FileTraccia.Note += "Sfogliatore 50" + vbCrLf
            End If
            If System.Convert.ToInt16(ls150.SIdentStr(1)) And &H8 Then
                If System.Convert.ToInt16(ls150.SIdentStr(1)) And &H10 Then ' badge
                    FileTraccia.Note += "Badge" + vbCrLf
                End If
            End If
            'If (ls150.SLsName.Trim(" ") = "LS150_C") Then
            If System.Convert.ToInt16(ls150.SIdentStr(1)) And &H20 Then
                FForzaCartaColori = True
                FMacchinaColori = True
            End If

            'writeDebug("LsName = " + ls150.SLsName, "TestIdentificativo")
            'If (ls150.SVersion = "") Then
            '    writeDebug("SVersion = Stringa nulla", "TestIdentificativo")
            'Else
            '    writeDebug("SVersion = " & ls150.SVersion, "TestIdentificativo")
            'End If

            'If (ls150.SLsName.Trim(" ") = "LS150") Then
            '    ' Per problema VB annulo gli eventuali caratteri stringa !
            '    'Version = ls150.SVersion.Substring(0, 4)
            '    'aaa = System.Convert.ToSingle(Version.Trim(" "))

            '    'If (aaa > 100) Then
            '    '    If (aaa >= 150.0F) Then
            '    '        FForzaCartaColori = True
            '    '    End If
            '    'Else
            '    '    If (aaa >= 1.5F) Then
            '    '        FForzaCartaColori = True
            '    '    End If
            '    'End If

            'End If


            ritorno = True
        Else
            ' errore da richiesta identificativo
            idmsg = 300
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("MANCANZA COMUNICAZIONE PERIFERICA", "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
            End If

            ritorno = False

        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ls150.ReturnC.ReturnCode
        Return ritorno
    End Function

    Public Function TestIdentificativoPiastra() As Integer
        Dim ritorno As Integer
        Dim ret As Integer
        Dim StrVersione As String
        Dim fpga As Integer
        Dim aaa As Single
        ' inizializzazioni variabili 

        ls150.ReturnC.FunzioneChiamante = "TestIdentificativoPiastra"

        FaseEsec()
        ritorno = False
        'chiamata a funzione 
        ret = ls150.Identificativo()
        BIdentificativo = True
        MatricolaPiastra = ls150.SSerialNumber
        FileTraccia.VersioneFw = ls150.SVersion

        If (ls150.SLsName.Trim(" ") = "LS150") Then
            aaa = System.Convert.ToSingle(ls150.SVersion.Trim(" "))

            'If (aaa > 100) Then
            '    If (aaa >= 150.0F) Then
            '        FForzaCartaColori = True
            '    End If
            'Else
            '    If (aaa >= 1.5F) Then
            '        FForzaCartaColori = True
            '    End If
            'End If

        End If
        If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
            ' Lidentif.Text = ls150.SLsName + " " + ls150.SVersion + vbCrLf
            fpga = System.Convert.ToInt32(ls150.SBoardNr)
            If ls150.SVersion.Length >= 2 Then
                StrVersione = "Nome Periferica: " + ls150.SLsName + vbCrLf + "Versione FW: " + ls150.SVersion + vbCrLf + "Data: " + ls150.SDateFW + vbCrLf + "Id Piastra: " + fpga.ToString("x") + vbCrLf + "Fpga: " + ls150.SCpldNr
                MessageboxMy(StrVersione, "Versione firmware", MessageBoxButtons.OK, MessageBoxIcon.None, Nothing)
            End If
            ritorno = True
        Else
            ' errore da richiesta identificativo
            MessageboxMy(ls150.ReturnC.FunzioneChiamante, "Attenzione !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ls150.ReturnC.ReturnCode
        Return ritorno
    End Function

    Public Function TestCheckVersion() As Integer
        Dim ritorno As Integer
        Dim ret As Integer

        ls150.ReturnC.FunzioneChiamante = "TestCheckVersion"

        ritorno = False
        FaseEsec()

        If BIdentificativo = True Then
            If ls150.SSerialNumber.Contains("OK") = True Then
                ' controllo che la versione sia quella dell ordine
                ret = ControllaVersione()
                If ret = 1 Then
                    ritorno = True
                    FileTraccia.Note = ""

                    FileTraccia.Note = "FwBase =" + Ver_firm1.ToString() + ", DataFW =" + Ver_firm2.ToString() + vbCrLf
                    If (Ver_firm3 <> "") Then
                        FileTraccia.Note += "PalettaMotorizzata=" + Ver_firm3.ToString() + ", "
                    End If
                    FileTraccia.Note += "Board =" + Ver_firm4.ToString + vbCrLf
                    FileTraccia.Note += "Fpga =" + Ver_firm5.ToString() + vbCrLf
                    If (Ver_firm9 <> "") Then
                        FileTraccia.Note += "Suite =" + Ver_firm9.ToString() + vbCrLf
                    End If

                Else
                    ritorno = False
                End If
            Else
                ' piastra NON Collaudata
                idmsg = 91
                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                    dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                Else
                    dres = MessageboxMy("Attenzione, possibile Piastra NON collaudata", "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                End If

                ritorno = False
            End If
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function

    Public Function TestSerialNumber() As Integer  'ultmo test
        Dim ritorno As Integer
        Dim ret As Short
        Dim fser As New CtsControls.FSerialnum
        ritorno = False
        FaseEsec()

        'debug
        'ls150.Identificativo()
        'ls150.LeggiValoriPeriferica()

        ls150.ReturnC.FunzioneChiamante = "TestSerialNumber"

        idmsg = 184
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            fser.Text = Messaggio + SerialNumberDaStampare
        Else
            fser.Text = "Inserire SN: " + SerialNumberDaStampare
        End If

        If fser.ShowDialog() = DialogResult.OK Then

            If fser.StrSerialNumber.Length = LUNGHEZZA_SERIAL_NUMBER Then
                If fser.StrSerialNumber = SerialNumberDaStampare Then
                    MatricolaPeriferica = fser.StrSerialNumber
                    ls150.SSerialNumber = MatricolaPeriferica
                    ret = ls150.SerialNumber()
                    If ret = Ls150Class.Costanti.S_LS150_OKAY Then
                        FileTraccia.Matricola = MatricolaPeriferica
                        ls150.Identificativo()
                        ls150.LeggiValoriPeriferica()
                        SalvaDati()
                        ritorno = True
                    Else
                        ViewError(fase)
                    End If
                Else
                    idmsg = 185
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        MessageboxMy("SerialNumber non corrispondente a quello inserito a inizio collaudo.", "Attenzione...", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                    End If
                    ret = Ls150Class.Costanti.S_LS150_STRINGTRUNCATED
                End If
            Else
                ret = Ls150Class.Costanti.S_LS150_STRINGTRUNCATED
                ViewError(fase)
            End If

        Else
            ViewError(fase)
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        ls150.ReturnC.FunzioneChiamante = "TestSerialNumber"
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestCancellaStorico(ByVal Veloci As Short) As Integer
        Dim ritorno As Integer
        Dim ret As Short
        ritorno = False
        FaseEsec()

        ls150.ReturnC.FunzioneChiamante = "TestCancellaStorico"

        ret = ls150.EraseHistory()

        Hstorico.Doc_Timbrati = ls150.S_Storico.Doc_Timbrati
        Hstorico.Doc_Timbrati_retro = ls150.S_Storico.Doc_Timbrati_retro
        Hstorico.Documenti_Catturati = ls150.S_Storico.Documenti_Catturati
        Hstorico.Documenti_Ingresso = ls150.S_Storico.Documenti_Ingresso
        Hstorico.Documenti_Trattati = ls150.S_Storico.Documenti_Trattati
        Hstorico.Documenti_Trattenuti = ls150.S_Storico.Documenti_Catturati
        Hstorico.Doppia_Sfogliatura = ls150.S_Storico.Doppia_Sfogliatura
        Hstorico.Errori_Barcode = ls150.S_Storico.Errori_Barcode
        Hstorico.Errori_CMC7 = ls150.S_Storico.Errori_CMC7
        Hstorico.Errori_E13B = ls150.S_Storico.Errori_E13B
        Hstorico.Errori_Ottici = ls150.S_Storico.Errori_Ottici
        Hstorico.Jam_Allinea = ls150.S_Storico.Jam_Allinea
        Hstorico.Jam_Card = ls150.S_Storico.Jam_Card
        Hstorico.Jam_Cassetto1 = ls150.S_Storico.Jam_Cassetto1
        Hstorico.Jam_Cassetto2 = ls150.S_Storico.Jam_Cassetto2
        Hstorico.Jam_Cassetto3 = ls150.S_Storico.Jam_Cassetto3
        Hstorico.Jam_Feeder = ls150.S_Storico.Jam_Feeder
        Hstorico.Jam_Feeder_Ret = ls150.S_Storico.Jam_Feeder_Ret
        Hstorico.Jam_Micr = ls150.S_Storico.Jam_Micr
        Hstorico.Jam_Micr_Ret = ls150.S_Storico.Jam_Micr_Ret
        Hstorico.Jam_Percorso_DX = ls150.S_Storico.Jam_Percorso_DX
        Hstorico.Jam_Percorso_SX = ls150.S_Storico.Jam_Percorso_SX
        Hstorico.Jam_Print = ls150.S_Storico.Jam_Print
        Hstorico.Jam_Print_Ret = ls150.S_Storico.Jam_Print_Ret
        Hstorico.Jam_Scanner = ls150.S_Storico.Jam_Scanner
        Hstorico.Jam_Scanner_Ret = ls150.S_Storico.Jam_Scanner_Ret
        Hstorico.Jam_Sorter = ls150.S_Storico.Jam_Sorter
        Hstorico.Jam_Stamp = ls150.S_Storico.Jam_Stamp
        Hstorico.Num_Accensioni = ls150.S_Storico.Num_Accensioni
        Hstorico.TempoAccensione = ls150.S_Storico.TempoAccensione
        Hstorico.Trattenuti_Micr = ls150.S_Storico.Trattenuti_Micr
        Hstorico.Trattenuti_Scan = ls150.S_Storico.Trattenuti_Scan

        FileTraccia.Note = ""

        ' Get the stream of the source file.

        ' Compressing:
        ' Prevent compressing hidden and already compressed files.

        ''/Transform string into byte[]  

        If ret = Ls150Class.Costanti.S_LS150_OKAY Then

            ' se sono all ' inizio del collaudo quindi setto a true il set diagnistic mode 
            ' poi devo settare la velocità a alta quindi 0

            ' altrimenti se sono alla fine , prima di togliere il seti diagnosti mode 
            ' devo alzare la velocità .
            If Veloci = 0 Then
                ret = ls150.SetVelocita(0)
            End If
            ret = ls150.SetDiagnostic(Veloci)
            If Veloci = 1 Then
                ret = ls150.SetVelocita(0)
            End If

            If ret = Ls150Class.Costanti.S_LS150_OKAY Then
                ritorno = True
            Else

            End If
        Else

        End If

        If ritorno = True Then
            FaseOk()
        Else
            ViewError(fase)
            FaseKo()
        End If
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function SetConfigurazione() As Integer
        Dim ritorno As Integer
        Dim ret As Short

        Dim TestinaMicr As Integer
        Dim Timbri As Integer
        Dim ScannerFronte As Integer
        Dim ScannerRetro As Integer
        Dim Stampante As Integer
        Dim VelBassa As Integer
        Dim Badge1 As Integer ' 0 1 2 3 
        Dim Badge2 As Integer ' 0 1 2 3 
        Dim Badge3 As Integer ' 0 1 2 3 
        Dim Sfogliatore50 As Integer
        Dim Card As Integer

        ritorno = False
        FaseEsec()
        Badge1 = Badge2 = Badge3 = 0

        If fTestinaMicr = True Then
            TestinaMicr = 1
        Else
            TestinaMicr = 0
        End If
        If fTimbri = True Then
            Timbri = 1
        Else
            Timbri = 0
        End If
        'scanner 
        If fScannerFronte = True Then
            ScannerFronte = 1
        Else
            ScannerFronte = 0
        End If
        If fScannerRetro = True Then
            ScannerRetro = 1
        Else
            ScannerRetro = 0
        End If
        'stampante
        If fStampante = True Then
            Stampante = 1
        Else
            Stampante = 0
        End If
        'veloc
        If fVelBassa = True Then
            VelBassa = 1
        Else
            VelBassa = 0
        End If

        Select Case fBadge
            Case 1
                Badge1 = 1
                Badge2 = 0
                Badge3 = 0
            Case 2
                Badge1 = 0
                Badge2 = 1
                Badge3 = 0
            Case 3
                Badge1 = 0
                Badge2 = 0
                Badge3 = 1
        End Select

        If fSfogliatore = SFOGlIATORE_50_DOC_PRESENTE Then
            Sfogliatore50 = 1
        Else
            Sfogliatore50 = 0
        End If

        'card
        If fCard = True Then
            Card = 1
        Else
            Card = 0
        End If



        FileTraccia.Note = ""
        FileTraccia.Note = "TestinaMicr =" + TestinaMicr.ToString() + ", TestinaMicr =" + TestinaMicr.ToString() + vbCrLf
        FileTraccia.Note += "Timbri =" + Timbri.ToString() + ", Badge1 =" + Badge1.ToString() + vbCrLf
        FileTraccia.Note += "Badge2 =" + Badge2.ToString() + ", Badge3 =" + Badge3.ToString() + vbCrLf
        FileTraccia.Note += "ScannerFronte =" + ScannerFronte.ToString() + ", ScannerRetro =" + ScannerRetro.ToString() + vbCrLf
        FileTraccia.Note += "Stampante =" + Stampante.ToString() + ", VelBassa =" + VelBassa.ToString() + vbCrLf
        FileTraccia.Note += "Sfogliatore50 =" + Sfogliatore50.ToString()
        FileTraccia.Note += "Software Abilitato =" + fSoftware.ToString()
        FileTraccia.Note += "Card Color =" + fcardCol.ToString()


        ret = ls150.SetConfigurazione(TestinaMicr, TestinaMicr, Timbri, Badge1, Badge2, Badge3, ScannerFronte, ScannerRetro, Stampante, VelBassa, Sfogliatore50, fSoftware, fcardCol)
        If ret = Ls150Class.Costanti.S_LS150_OKAY Then
            ritorno = True
        Else
            ' imposto gli errori
            ViewError(fase)
            FileTraccia.UltimoRitornoFunzione = ret
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestFotoPeriferica() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim floop As Boolean = False
        Dim iii As Short
        iii = 0
        ritorno = False
        FLed = New CtsControls.Led
        FLed.LMessaggio.Text = "Coprire fotosensori BIN per almeno 3 secondi"

        FaseEsec()
        idmsg = 93
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Rimuovere eventuali documenti da percorso", "ATTENZIONE !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning, Nothing)
        End If

        If dres <> DialogResult.OK Then
            fase.SetState = 0
            fase.IsBlinking = True
            fase.IsSelected = True
            ritorno = False
            FileTraccia.UltimoRitornoFunzione = Ls150Class.Costanti.S_LS150_USER_ABORT

        Else
            FLed.Show()
            FLed.Refresh()
            ret = ls150.CalibrazioneFoto()

            FLed.LSe1.Text += Ls150Class.Periferica.PhotoValue(0).ToString
            FLed.Lse2.Text += Ls150Class.Periferica.PhotoValue(1).ToString
            FLed.LSe3.Text += Ls150Class.Periferica.PhotoValue(2).ToString

            FileTraccia.Note = "Fotosensore 1: " + Ls150Class.Periferica.PhotoValue(0).ToString + vbCrLf
            FileTraccia.Note += "Fotosensore 2: " + Ls150Class.Periferica.PhotoValue(1).ToString + vbCrLf
            FileTraccia.Note += "Fotosensore 3: " + Ls150Class.Periferica.PhotoValue(2).ToString

            If Ls150Class.Periferica.PhotoValue(0) = 0 Or Ls150Class.Periferica.PhotoValue(0) = 255 Then
                FLed.LbLedSe1.LedColor = Color.Red
            Else
                FLed.LbLedSe1.LedColor = Color.Green
                FLed.LbLedSe1.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.On
            End If

            If Ls150Class.Periferica.PhotoValue(1) = 0 Or Ls150Class.Periferica.PhotoValue(1) = 255 Then
                FLed.LbLedSe2.LedColor = Color.Red
            Else
                FLed.LbLedSe2.LedColor = Color.Green
                FLed.LbLedSe2.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.On
            End If

            If Ls150Class.Periferica.PhotoValue(2) = 0 Or Ls150Class.Periferica.PhotoValue(2) = 255 Then
                FLed.LbLedSe3.LedColor = Color.Red
            Else
                FLed.LbLedSe3.LedColor = Color.Green
                FLed.LbLedSe3.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.On
            End If

            If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
                'idmsg = 92
                'If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                '    dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                'Else
                '    dres = MessageboxMy("Coprire fotosensori BIN per almeno 3 secondi", "ATTENZIONE !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning, Nothing)
                'End If


                'If dres = DialogResult.OK Then
                floop = False
                iii = 1

                Do
                    ret = ls150.StatoPeriferica()
                    If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
                        ' ogni 5 secondi mando il mesagigio a video 
                        If (Math.IEEERemainder(iii, 5) = 0) Then
                            'idmsg = 92
                            'If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            '    dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                            'Else
                            '    dres = MessageboxMy("Coprire fotosensori BIN per almeno 3 secondi", "ATTENZIONE !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning, Nothing)
                            'End If

                            'If (dres <> DialogResult.Yes) Then
                            '    ret = -1
                            'End If
                        End If
                        Application.DoEvents()
                        If (Ls150Class.Periferica.StatusByte(0) And 16) Then
                            '  Lf4.BackColor = Color.Green
                            '  Lf4.Text = "Fotosensore BIN 1 coperto"
                            FLed.LbLedSeBin.LedColor = Color.Green
                            FLed.LbLedSeBin.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.On
                            FLed.Refresh()
                            floop = True
                            ritorno = True
                        Else
                            FLed.LbLedSeBin.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.Off
                            '  Lf4.BackColor = Color.Red
                            '  Lf4.Text = "Fotosensore BIN 1 scoperto"
                        End If
                        ' aspetto un secondo
                        'Sleep(1)
                        Threading.Thread.Sleep(1000)
                    Else
                        FileTraccia.UltimoRitornoFunzione = ret
                        ViewError(fase)
                    End If
                    iii += 1
                    If (iii > 15) Then
                        floop = True
                    End If
                Loop While floop = False
                FLed.Close()

                If (iii >= 15) Then
                    ritorno = False

                    idmsg = 301
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy("TimeOut Scaduto", "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                    End If

                End If

                'Else
                'FLed.Close()
                'ritorno = False
                'FileTraccia.UltimoRitornoFunzione = Ls150Class.Costanti.S_LS150_USER_ABORT
                'End If
            Else
                ViewError(fase)
                ritorno = False
                FLed.Close()
                FileTraccia.UltimoRitornoFunzione = ret
                StrCommento += "Calibrazione non a buon fine: " + ret.ToString + " "
            End If
        End If
        If ritorno = True Then
            FaseOk()
            FileTraccia.FotoPersorso = 1
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestScannersPeriferica() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim valore_lotto As Integer
        Dim fAspetta As CtsControls.FWait
        Dim StrMessaggioWait As String
        Dim NrTentativi As Integer
        ritorno = False
        Dim fControlloBackGround As Integer

        fControlloBackGround = 0
        ls150.ReturnC.FunzioneChiamante = "TestScannersPeriferica"

        If (SerialNumberDaStampare = Nothing) Then
            SerialNumberDaStampare = "PROGETTO"
        End If

        If FForzaCartaColori = True Then
            ls150.SetDiagnostic(3)
        End If

        'prova
        'ret = ls150.TestNormalization(Ls150Class.Costanti.S_SCAN_MODE_256GR300, False, 0, 0, 0, 0, 46)
        'Return ret
        FaseEsec()

        TestIdentificativo()

        'fControlloBackGround = 0
        'If ls150.SLsName.Contains("LS150_UV") Then
        '    fTestFascioni = 1
        '    ret = APPTestControlloFascioni(fTestFascioni, fControlloBackGround)
        '    writeDebug("RET = " + ret.ToString(), "TestControlloFascioni")
        'End If
        'Return ret

        '       GoTo aaa
        If ls150.SLsName.Contains("LS150_UV") Then
            ritorno = False
            fMacchinaUv = True
            FMacchinaColori = False    ' LA MACCHINA UV NON E' A COLORI !

            idmsg = 263
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Inserire il DOCUTEST --069 per la calibrazione Scanner UV. !", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(10))
            End If

            ' faccio la calibrazione scanner UB
            If (dres <> DialogResult.OK) Then
                ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                FaseKo()
                fWaitButton = True
            Else
                'ret = ls150.CalibraScanner(Ls150Class.Costanti.SCANNER_UV)
                ret = ls150.CalibraScanner2()
                If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
                Else
                    ViewError(fase)
                End If
            End If
            If ret = Ls150Class.Costanti.S_LS150_OKAY Then
                ritorno = True
            Else
                ritorno = False
            End If
        Else
            ritorno = True
        End If


        If (ritorno = True) Then
            idmsg = 94
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Inserire il DOCUTEST --055 per la calibrazione Scanner. !", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(10))
            End If

            ' faccio la calibrazione scanner fronte
            If (dres <> DialogResult.OK) Then
                ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                FaseKo()
                fWaitButton = True
            Else
                If fMacchinaUv = True Then
                    ret = ls150.CalibraScanner()
                Else
                    ret = ls150.CalibraScannerOld()
                End If

                If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then

                    If ls150.SLsName.Contains("LS150_UV") Then
                        idmsg = 305
                        If MessageBox.Show("Sul Docutest-069 e' presente il numero di lotto ?", "Titolo", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk) = Windows.Forms.DialogResult.No Then
                            idmsg = 306
                            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing, IdMessaggio, immamsg)
                            Else
                                dres = MessageboxMy("Inserire 10 come numero di LOTTO", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                            End If
                        End If
                        If (dres = DialogResult.None Or dres = DialogResult.Cancel) Then

                        End If

                        idmsg = 294
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("Inserire Docutest-069(ricalcola i coefficienti UV con luce gray)", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(10))
                        End If

                        If (dres <> DialogResult.OK) Then
                            ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                            FaseKo()
                        Else
                            fAspetta = New CtsControls.FWait
                            fAspetta.Label1.Text = "Attendere...Ricalcolo coefficienti UV in corso"
                            fAspetta.Show()
                            ret = ls150.TestScannerUvCalibrationReExecute()
                            fAspetta.Hide()
                            If (ret = Ls150Class.Costanti.S_LS_OKAY) Then

                            Else
                                ViewError(fase)
                                ls150.Reset(Ls150Class.Costanti.S_RESET_FREE_PATH)
                            End If
                        End If
                    End If
                Else
                    ViewError(fase)
                    ls150.Reset(Ls150Class.Costanti.S_RESET_FREE_PATH)
                End If
                If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
                    '                    NrTentativi = 0

doc309:
                    fAspetta = New CtsControls.FWait
                    idmsg = 298
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        StrMessaggioWait = Messaggio
                    Else
                        StrMessaggioWait = "Compensazione in corso...attendere"
                    End If

                    idmsg = 297
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        StrMessaggio = Messaggio
                    Else
                        StrMessaggio = "Inserire il Numero di Lotto del DOCUTEST-309"
                    End If
                    If InputboxMy(StrMessaggio, "Attenzione", True, valore_lotto, 0, 0) = DialogResult.OK Then

                    End If

                    'If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
                    idmsg = 295
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy("Inserire DOCUTEST-309 nel Feeder per la Normalizzazione CIS", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(10))
                    End If
                    fAspetta.Label1.Text = StrMessaggioWait
                    fAspetta.Show()
                    Application.DoEvents()
                    If (FMacchinaColori = True Or fMacchinaUv = True) Then
                        ret = ls150.TestNormalization(Ls150Class.Costanti.S_SCAN_MODE_256GR300, False, 0, 0, 0, 0, valore_lotto, 1, 1)
                    Else
                        ret = ls150.TestNormalization(Ls150Class.Costanti.S_SCAN_MODE_256GR300, False, 0, 0, 0, 0, valore_lotto, 1, 1)
                    End If
                    fAspetta.Hide()
                    'End If

                    If (ret = Ls150Class.Costanti.S_LS150_OKAY And (FMacchinaColori = True Or fMacchinaUv = True)) Then
                        idmsg = 295
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("Inserire DOCUTEST-309 nel Feeder per la Normalizzazione CIS", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(10))
                        End If
                        fAspetta.Label1.Text = StrMessaggioWait
                        fAspetta.Show()
                        Application.DoEvents()
                        ret = ls150.TestNormalization(Ls150Class.Costanti.S_SCAN_MODE_COLOR_300, False, 0, 0, 0, 0, valore_lotto, 1, 1)
                        fAspetta.Hide()
                    End If



                    If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
                        ritorno = True
                    Else
                        ViewError(fase)
                        ls150.Reset(Ls150Class.Costanti.S_RESET_FREE_PATH)
                        NrTentativi = NrTentativi + 1
                        If (NrTentativi < 3) Then
                            GoTo doc309
                        End If
                    End If

                    If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
                        'Nel caso di periferica Uv - effettuo il controllo dei fascioni
                        fTestFascioni = 0
                        fControlloBackGround = 1  ' da scommentare con ciclo ridotto.....
                        If ls150.SLsName.Contains("LS150_UV") Then
                            fTestFascioni = 1
                        End If
                        ret = APPTestControlloFascioni(fTestFascioni, fControlloBackGround)
                        Dim trace As String
                        trace = SerialNumberDaStampare + "-ret = " + ret.ToString()
                        writeDebug(trace, "TestControlloFascioni")


                        ' ret = Ls150Class.Costanti.S_LS150_OKAY

                        If (ret = Ls150Class.Costanti.S_LS150_FRONT_BACKGROUND Or ret = Ls150Class.Costanti.S_LS150_FRONT_BACKGROUND_1 Or ret = Ls150Class.Costanti.S_LS150_FRONT_BACKGROUND_2 Or _
                            ret = Ls150Class.Costanti.S_LS150_BACK_BACKGROUND Or ret = Ls150Class.Costanti.S_LS150_BACK_BACKGROUND_1 Or ret = Ls150Class.Costanti.S_LS150_BACK_BACKGROUND_2) Then

                            ritorno = False
                            'MessageboxMy("TestBackground NON Passato", "ARCA", MessageBoxButtons.OK, MessageBoxIcon.Information, Nothing)

                            Try
                                Pimage.Image = System.Drawing.Image.FromFile(ls150.NomeFileImmagine)
                                IndImgF += 1

                                PImageBack.Image = System.Drawing.Image.FromFile(ls150.NomeFileImmagineRetro)
                                IndImgR += 1
                            Catch ex As Exception
                                MessageBox.Show(ex.ToString())
                                ret = Ls150Class.Costanti.S_LS_OKAY

                            End Try

                            If (ret = Ls150Class.Costanti.S_LS150_FRONT_BACKGROUND Or ret = Ls150Class.Costanti.S_LS150_FRONT_BACKGROUND_1 Or ret = Ls150Class.Costanti.S_LS150_FRONT_BACKGROUND_2) Then
                                'BVisualZoom = True
                                DirZoom = Application.StartupPath
                                Dim a As Short
                                a = DirZoom.LastIndexOf("\")
                                DirZoom = DirZoom.Substring(0, a)
                                DirZoom += "\Images_Fascioni\"
                                FileZoomCorrente = ls150.NomeFileImmagine
                                ' Zoom Immagine 
                                ShowZoom(DirZoom, FileZoomCorrente)
                            End If

                            If (ret = Ls150Class.Costanti.S_LS150_BACK_BACKGROUND Or ret = Ls150Class.Costanti.S_LS150_BACK_BACKGROUND_1 Or ret = Ls150Class.Costanti.S_LS150_BACK_BACKGROUND_2) Then
                                'BVisualZoom = True
                                DirZoom = Application.StartupPath
                                Dim a As Short
                                a = DirZoom.LastIndexOf("\")
                                DirZoom = DirZoom.Substring(0, a)
                                DirZoom += "\Images_Fascioni\"
                                FileZoomCorrente = ls150.NomeFileImmagineRetro
                                ' Zoom Immagine 
                                ShowZoom(DirZoom, FileZoomCorrente)
                            End If

                        ElseIf ret <> Ls150Class.Costanti.S_LS150_OKAY Then
                            ritorno = False
                        End If
                    End If

                End If
                End If
        End If



        FileTraccia.UltimoRitornoFunzione = ret
        If ret = Ls150Class.Costanti.S_LS150_OKAY Then
            ritorno = True
        Else
            ritorno = False
        End If

        If ritorno = True Then
            FaseOk()
            'FileTraccia.ScannerF = 1
        Else
            FaseKo()
        End If
        'BVisualZoom = False
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante

        'ls150.SetDiagnostic(0)

        Return ritorno
    End Function

    'funzione che verifica il funzionamento del DataMatrix + QRCODE
    Public Function TestBarcode2D(ByVal ncili As Short) As Integer

        Dim ritorno As Short
        Dim ret As Short
        Dim fstop As Short
        Dim penna As New Pen(System.Drawing.Color.Red)
        Dim cicli As Short
        Dim font1 As New Font("Time New Roman", 8, FontStyle.Regular, GraphicsUnit.Pixel)
        Dim fAspetta As CtsControls.FWait
        Dim fcicla As Boolean
        Dim fdecode As Short
        Dim strBarcode0 As String
        Dim strBarcode1 As String
        Dim strBarcodeDataMatrix As String

        strBarcode0 = "505428111015181358766f1P4VezM"
        strBarcode1 = "946829746525181358766f1C4ZCts"
        strBarcodeDataMatrix = "CTSqr7794520027358eLeCTroniCS"

        ritorno = False
        fAspetta = New CtsControls.FWait
        fAspetta.Label1.Text = "Attendere... decodifica BARCODE in corso...."
        FaseEsec()

        cicli = 1
        If (fSoftware = SW_BAR2D Or fSoftware = SW_TOP_BAR2D) Then
            fdecode = 1

            If fdecode = 1 Then
                fcicla = True
                While (fcicla)

                    idmsg = 276
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy("Inserire DOCUTEST --089", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                    End If

                    If (dres <> DialogResult.OK) Then
                        ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                        fcicla = False
                        fstop = True
                    Else
                        DisposeImg()
                        StrMessaggio = "Test Barcode2D"

                        ls150.NomeFileImmagine = zzz + "\Dati\ImageBarcode2D" + IndImgF.ToString("0000") + ".bmp"
                        ls150.NomeFileImmagineRetro = zzz + "\Dati\ImageBarcode2DR" + IndImgR.ToString("0000") + ".bmp"

                        fAspetta.Show()
                        ret = ls150.DecodificaBarcode2D(System.Convert.ToSByte(Ls150Class.Costanti.S_SIDE_ALL_IMAGE), Ls150Class.Costanti.S_SCAN_MODE_256GR200, fdecode)
                        fAspetta.Hide()


                        Select Case ret
                            Case Ls150Class.Costanti.S_LS150_PAPER_JAM, + _
                            Ls150Class.Costanti.S_LS150_JAM_AT_MICR_PHOTO, + _
                            Ls150Class.Costanti.S_LS150_JAM_AT_SCANNERPHOTO, + _
                            Ls150Class.Costanti.S_LS150_JAM_DOC_TOOLONG
                                idmsg = 111
                                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                    dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                                Else
                                    dres = MessageboxMy("ATTENZIONE CARTA BLOCCATA LIBERARE IL PERCORSO", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                End If

                                If (dres <> DialogResult.OK) Then
                                    ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                                    fstop = True
                                Else
                                    ls150.Reset(Ls150Class.Costanti.S_RESET_FREE_PATH)
                                End If
                                fcicla = False
                            Case Ls150Class.Costanti.S_LS150_FEEDEREMPTY
                                fstop = True


                            Case Ls150Class.Costanti.S_LS150_OKAY

                                ret = Ls150Class.Costanti.S_LS150_OKAY
                                fcicla = False


                                DisposeImg()
                                'FileZoomCorrente = zzz + "\Dati\cardImageC" + IndImgF.ToString("0000") + ".bmp"
                                PFronte.Image = System.Drawing.Image.FromFile(zzz + "\Dati\ImageBarcode2D" + IndImgF.ToString("0000") + ".bmp")
                                PRetro.Image = System.Drawing.Image.FromFile(zzz + "\Dati\ImageBarcode2DR" + IndImgR.ToString("0000") + ".bmp")
                                Pimage.Image = PFronte.Image
                                PImageBack.Image = PRetro.Image
                                IndImgF += 1
                                IndImgR += 1

                                If dres = DialogResult.Cancel Then
                                    ret = -11111
                                End If



                                If (ls150.BarcodeLetto0 = strBarcode0 Or ls150.BarcodeLetto1 = strBarcode0 Or ls150.BarcodeLetto2 = strBarcode0 And _
                                    ls150.BarcodeLetto0 = strBarcode1 Or ls150.BarcodeLetto1 = strBarcode1 Or ls150.BarcodeLetto2 = strBarcode1 And _
                                    ls150.DataMatrixLetto0 = strBarcodeDataMatrix Or ls150.DataMatrixLetto0 = strBarcodeDataMatrix Or ls150.DataMatrixLetto0 = strBarcodeDataMatrix) Then
                                    dres = MessageboxMy(ls150.BadgeLetto + vbCrLf + "Decodifica Barcode2D OK!", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                                    ritorno = True
                                    fcicla = False
                                Else
                                    dres = MessageboxMy(ls150.BadgeLetto + vbCrLf + "Decodifica Barcode2D KO!, valori letti non corretti", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                    ritorno = False
                                    ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                                End If

                            Case Ls150Class.Costanti.S_LS150_BARCODE_GENERIC_ERROR

                                MessageboxMy("Decodifica BARCODE2D NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                fcicla = False
                                ritorno = False

                            Case Ls150Class.Costanti.S_LS150_BARCODE_NOT_DECODABLE

                                MessageboxMy("Decodifica BARCODE2D NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                fcicla = False
                                ritorno = False

                            Case Ls150Class.Costanti.S_LS150_BARCODE_OPENFILE_ERROR

                                MessageboxMy("Decodifica BARCODE2D NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                fcicla = False
                                ritorno = False

                            Case Ls150Class.Costanti.S_LS150_BARCODE_READBMP_ERROR

                                MessageboxMy("Decodifica BARCODE2D NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                fcicla = False
                                ritorno = False

                            Case Ls150Class.Costanti.S_LS150_BARCODE_MEMORY_ERROR

                                MessageboxMy("Decodifica BARCODE2D NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                fcicla = False
                                ritorno = False

                            Case Ls150Class.Costanti.S_LS150_BARCODE_START_NOTFOUND

                                MessageboxMy("Decodifica BARCODE2D NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                fcicla = False
                                ritorno = False

                            Case Ls150Class.Costanti.S_LS150_BARCODE_STOP_NOTFOUND

                                MessageboxMy("Decodifica BARCODE2D NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                fcicla = False
                                ritorno = False



                        End Select
                    End If
                    If cicli = ncili Then
                        fcicla = False
                    End If
                    cicli += 1
                End While
            Else
                ' non ho fatto nulla, e setto tutto a ok
                ret = Ls150Class.Costanti.S_LS150_OKAY
            End If
        Else
            ritorno = True
            ret = 0
        End If




        FileTraccia.UltimoRitornoFunzione = ret
        If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
            ritorno = True
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        fAspetta.Close()
        Return ritorno
    End Function

    Public Function TestBackground() As Integer
        Dim ritorno As Integer
        Dim ret As Integer
        Dim stringa As String

        stringa = ""
        ritorno = False

        FaseEsec()

        idmsg = 288
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire DOCUTEST--064 nel Feeder", "Attenzione!!! ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        End If

        If dres = DialogResult.OK Then
            'Spostato qui se no LsSaveBid dava -18
            DisposeImg()

            ls150.NomeFileImmagine = zzz + "\Quality\FrontImage" + IndImgF.ToString("0000") + ".bmp"

            ls150.NomeFileImmagineRetro = zzz + "\Quality\BackImage" + IndImgR.ToString("0000") + ".bmp"

            'debug 
            'fZebrato = True
            If (fZebrato = 1) Then
                ret = ls150.TestBackground(Convert.ToInt32(1))
            Else
                ret = ls150.TestBackground(Convert.ToInt32(0))
            End If



            If (ret = Ls150Class.Costanti.S_LS_OKAY) Then
                ritorno = True
                MessageboxMy("TestBackground Passato", "ARCA", MessageBoxButtons.OK, MessageBoxIcon.Information, Nothing)
            Else
                ritorno = False
                MessageboxMy("TestBackground NON Passato", "ARCA", MessageBoxButtons.OK, MessageBoxIcon.Information, Nothing)

                Try
                    Pimage.Image = System.Drawing.Image.FromFile(ls150.NomeFileImmagine)
                    IndImgF += 1

                    PImageBack.Image = System.Drawing.Image.FromFile(ls150.NomeFileImmagineRetro)
                    IndImgR += 1
                Catch ex As Exception
                    ret = Ls150Class.Costanti.S_LS_OKAY
                End Try




                If (ret = Ls150Class.Costanti.S_LS150_FRONT_BACKGROUND Or ret = Ls150Class.Costanti.S_LS150_FRONT_BACKGROUND_1 Or ret = Ls150Class.Costanti.S_LS150_FRONT_BACKGROUND_2) Then
                    'BVisualZoom = True
                    DirZoom = Application.StartupPath
                    Dim a As Short
                    a = DirZoom.LastIndexOf("\")
                    DirZoom = DirZoom.Substring(0, a)
                    DirZoom += "\Quality\"
                    FileZoomCorrente = ls150.NomeFileImmagine
                    ' Zoom Immagine 
                    ShowZoom(DirZoom, FileZoomCorrente)
                End If

                If (ret = Ls150Class.Costanti.S_LS150_BACK_BACKGROUND Or ret = Ls150Class.Costanti.S_LS150_BACK_BACKGROUND_1 Or ret = Ls150Class.Costanti.S_LS150_BACK_BACKGROUND_2) Then
                    'BVisualZoom = True
                    DirZoom = Application.StartupPath
                    Dim a As Short
                    a = DirZoom.LastIndexOf("\")
                    DirZoom = DirZoom.Substring(0, a)
                    DirZoom += "\Quality\"
                    FileZoomCorrente = ls150.NomeFileImmagineRetro
                    ' Zoom Immagine 
                    ShowZoom(DirZoom, FileZoomCorrente)
                End If

                If (Riprova = 0) Then
                    Fqualit = New CtsControls.FqualImg
                    Fqualit.ShowDialog()


                    Select Case Fqualit.ret
                        Case 0
                            ritorno = False
                        Case 1, 2
                            'Scanner GRIGIO + UV + COLORE
                            ritorno = TestScannersPeriferica()
                            If ritorno = True Then
                                Riprova = 1
                                ritorno = TestBackground()
                            End If
                    End Select
                End If
            End If

        End If



        Application.DoEvents()


        Riprova = 0
        If ritorno = True Then
            FaseOk()
        Else
            ViewError(fase)
            FaseKo()
        End If

        'BVisualZoom = False

        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante


        Return ritorno
    End Function

    Public Function TestStampaPiastre(ByVal Altezza As Boolean) As Integer
        Dim ritorno As Integer
        Dim ret As Integer
        Dim strapp As String
        Dim iii As Short
        Dim fcicla As Boolean
        Dim ndoc As Short
        ritorno = False
        ndoc = 3

        FaseEsec()

        If (Altezza = False And fSfogliatore = SFOGlIATORE_50_DOC_PRESENTE) Then
            '' non faccio il test di stampa con l' elettromagnete attivato
            ' percheè la periferica mi da invalid command
            ritorno = True
        Else
            ls150.NomeFileImmagine = zzz + "\Dati\StampaFImage" + IndImgF.ToString("0000") + ".bmp"
            ls150.NomeFileImmagineRetro = zzz + "\Dati\StampaRImage" + IndImgR.ToString("0000") + ".bmp"
            IndImgF += 1
            IndImgR += 1
            ls150.Stampafinale = "Piastra: " + SerialNumberPiastra + " SN: " + SerialNumberDaStampare
            idmsg = 96
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Inserire un documento per la prova di stampa", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(3))
            End If

            If (dres <> DialogResult.OK) Then
                ret = Ls150Class.Costanti.S_LS150_USER_ABORT
            Else
                fase.IsSelected = False
                iii = 0
                fcicla = True
                While (fcicla)

                    If (iii > ndoc - 1) Then
                        idmsg = 183
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("Inserire uno dei documenti timbrati per l'ultima prova di stampa", TITOLO_DEF_MESSAGGIO, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(3))
                        End If
                    End If
                    ls150.NomeFileImmagine = zzz + "\Dati\StampaFImage" + IndImgF.ToString("0000") + ".bmp"
                    ls150.NomeFileImmagineRetro = zzz + "\Dati\StampaRImage" + IndImgR.ToString("0000") + ".bmp"
                    If Altezza = True Then
                        If (iii = ndoc) Then
                            ret = ls150.TestStampaAlto(1)
                        Else
                            ret = ls150.TestStampa(0)
                        End If
                    Else
                        If (iii = ndoc) Then
                            ret = ls150.TestStampa(1)
                        Else
                            ret = ls150.TestStampa(0)
                        End If
                    End If
                    iii += 1
                    If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then

                        Try
                            DisposeImg()
                            Pimage.Image = System.Drawing.Image.FromFile(ls150.NomeFileImmagine)
                            PImageBack.Image = System.Drawing.Image.FromFile(ls150.NomeFileImmagineRetro)
                        Catch ex As Exception
                            ' DEBUG
                            MessageBox.Show(ex.Message + " " + ex.Source + ret.ToString, "errore")
                        End Try

                        Application.DoEvents()

                        strapp = "La stampante ha stampato bene?"
                        idmsg = 97
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy(strapp, "Attenzione!!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing)
                        End If
                        If (dres = DialogResult.No) Then
                            iii -= 1
                        Else

                        End If
                    Else
                        Select Case ret
                            Case Ls150Class.Costanti.S_LS150_FEEDEREMPTY
                                idmsg = 178
                                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                    dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                                Else
                                    dres = MessageboxMy("Inserire Documenti per la prova di stampa", TITOLO_DEF_MESSAGGIO, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                                End If

                                If dres <> DialogResult.OK Then
                                    fcicla = False
                                    ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                                End If
                                ret = Ls150Class.Costanti.S_LS150_OKAY
                                iii -= 1
                            Case Ls150Class.Costanti.S_LS150_PAPER_JAM
                                ls150.Reset(Ls150Class.Costanti.S_RESET_FREE_PATH)
                                ret = Ls150Class.Costanti.S_LS150_OKAY
                                iii -= 1
                            Case Else
                                ls150.Reset(Ls150Class.Costanti.S_RESET_FREE_PATH)
                                ret = Ls150Class.Costanti.S_LS150_OKAY
                                iii -= 1
                        End Select
                    End If

                    If ret <> Ls150Class.Costanti.S_LS150_OKAY Or iii > ndoc Then
                        fcicla = False
                        iii -= 1
                    End If

                    Application.DoEvents()
                    IndImgF += 1
                    IndImgR += 1

                End While

                If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then

                    Try
                        DisposeImg()
                        Pimage.Image = System.Drawing.Image.FromFile(ls150.NomeFileImmagine)
                        PImageBack.Image = System.Drawing.Image.FromFile(ls150.NomeFileImmagineRetro)
                    Catch ex As Exception
                        ' DEBUG
                        MessageBox.Show(ex.Message + " " + ex.Source + ret.ToString, "errore")
                    End Try

                    Application.DoEvents()

                    strapp = "La stampante ha stampato bene?"
                    idmsg = 179
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy(strapp, "Attenzione!!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing)
                    End If

                    If dres = DialogResult.Yes Then
                        fase.SetState = 1
                        fase.IsBlinking = True
                        fase.IsSelected = True
                        ritorno = True
                    Else
                        ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                        fase.SetState = 0
                        fase.IsBlinking = True
                        fase.IsSelected = True
                    End If
                Else
                    ViewError(fase)
                End If
            End If
        End If

        FileTraccia.UltimoRitornoFunzione = ret

        If ritorno = True Then
            FileTraccia.Stampante = 1
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestStampa(ByVal Altezza As Short) As Integer
        Dim ritorno As Integer
        Dim ret As Integer
        Dim strapp As String
        Dim iii As Short
        Dim fcicla As Boolean
        Dim ndoc As Short
        ritorno = False
        ndoc = 3

        FaseEsec()

        If ((Altezza = 0 And fSfogliatore = SFOGlIATORE_50_DOC_PRESENTE) Or (fStampanteFissa = True) And (Altezza = 0)) Then
            ' non faccio il test di stampa con l' elettromagnete attivato perchè la periferica mi da invalid command
            ritorno = True
        Else
            ls150.NomeFileImmagine = zzz + "\Dati\StampaFImage" + IndImgF.ToString("0000") + ".bmp"
            ls150.NomeFileImmagineRetro = zzz + "\Dati\StampaRImage" + IndImgR.ToString("0000") + ".bmp"
            IndImgF += 1
            IndImgR += 1
            ls150.Stampafinale = "Piastra: " + SerialNumberPiastra + " SN: " + SerialNumberDaStampare
            iii = 3
            If (iii = 0) Then
                'idmsg = 96
                idmsg = 307
                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                    dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                Else
                    dres = MessageboxMy("Inserire un documento per la prova di stampa", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(3))
                End If
            Else
                dres = DialogResult.OK
            End If


            If (dres <> DialogResult.OK) Then
                ret = Ls150Class.Costanti.S_LS150_USER_ABORT
            Else
                fase.IsSelected = False
                'iii = 0
                'faccio solomante l'ultimo test e skippo i primi 3 ->richiesta C.roffinot
                iii = 3
                fcicla = True
                While (fcicla)


                    If (iii > ndoc - 1) Then
                        'idmsg = 183
                        idmsg = 307
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("Inserire uno dei documenti timbrati per l'ultima prova di stampa", TITOLO_DEF_MESSAGGIO, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(3))
                        End If
                    Else

                    End If
                    ls150.NomeFileImmagine = zzz + "\Dati\StampaFImage" + IndImgF.ToString("0000") + ".bmp"
                    ls150.NomeFileImmagineRetro = zzz + "\Dati\StampaRImage" + IndImgR.ToString("0000") + ".bmp"
                    If Altezza = 1 Then
                        If (iii = ndoc) Then
                            ret = ls150.TestStampaAlto(1)
                        Else
                            ret = ls150.TestStampaAlto(0)
                        End If
                    Else
                        If (iii = ndoc) Then
                            ret = ls150.TestStampa(1)
                        Else
                            ret = ls150.TestStampa(0)
                        End If
                    End If
                    strapp = "La stampante ha stampato bene?"
                    iii += 1
                    If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then

                        Try
                            DisposeImg()


                            Pimage.Image = System.Drawing.Image.FromFile(ls150.NomeFileImmagine)
                            'DisegnalineaOR(Pimage, Color.Blue)

                            PImageBack.Image = System.Drawing.Image.FromFile(ls150.NomeFileImmagineRetro)
                            'DisegnalineaOR(PImageBack, Color.Red)
                        Catch ex As Exception
                            ' DEBUG
                            MessageBox.Show(ex.Message + " " + ex.Source + ret.ToString, "errore")
                        End Try

                        Application.DoEvents()

                        idmsg = 97
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy(strapp, "Attenzione!!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing)
                        End If
                        If (dres = DialogResult.Cancel) Then
                            iii -= 1
                        Else

                        End If
                    Else
                        Select Case ret
                            Case Ls150Class.Costanti.S_LS150_FEEDEREMPTY
                                'idmsg = 178
                                idmsg = 307
                                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                    dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                                Else
                                    dres = MessageboxMy("Inserire Documenti per la prova di stampa", TITOLO_DEF_MESSAGGIO, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                                End If

                                If dres <> DialogResult.OK Then
                                    fcicla = False
                                    ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                                End If
                                ret = Ls150Class.Costanti.S_LS150_OKAY
                                iii -= 1
                            Case Ls150Class.Costanti.S_LS150_PAPER_JAM
                                ls150.Reset(Ls150Class.Costanti.S_RESET_FREE_PATH)
                                ret = Ls150Class.Costanti.S_LS150_OKAY
                                iii -= 1
                            Case Else
                                ls150.Reset(Ls150Class.Costanti.S_RESET_FREE_PATH)
                                ret = Ls150Class.Costanti.S_LS150_OKAY
                                iii -= 1
                        End Select
                    End If

                    If ret <> Ls150Class.Costanti.S_LS150_OKAY Or iii > ndoc Then
                        fcicla = False
                        iii -= 1
                    End If

                    Application.DoEvents()
                    IndImgF += 1
                    IndImgR += 1

                End While

                If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then

                    Try
                        DisposeImg()
                        Pimage.Image = System.Drawing.Image.FromFile(ls150.NomeFileImmagine)
                        PImageBack.Image = System.Drawing.Image.FromFile(ls150.NomeFileImmagineRetro)
                    Catch ex As Exception
                        ' DEBUG
                        MessageBox.Show(ex.Message + " " + ex.Source + ret.ToString, "errore")
                    End Try

                    Application.DoEvents()
                    fase.SetState = 1
                    fase.IsBlinking = True
                    fase.IsSelected = True
                    ritorno = True
                Else
                    ViewError(fase)
                End If
            End If
        End If

        FileTraccia.UltimoRitornoFunzione = ret

        If ritorno = True Then
            FileTraccia.Stampante = 1
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestSerialNumberPerPiastra() As Integer
        Dim ritorno As Integer
        Dim ret As Short

        ritorno = False
        FaseEsec()

        ls150.ReturnC.FunzioneChiamante = "TestSerialNumberPerPiastra"


        Dim datestr As String
        datestr = Date.Now().Day.ToString("00") + Date.Now().Month.ToString("00") + Date.Now().Year.ToString("0000") + "OK"


        MatricolaPeriferica = datestr
        ls150.SSerialNumber = MatricolaPeriferica
        FileTraccia.Matricola = MatricolaPeriferica
        ret = ls150.SerialNumber()
        If ret = Ls150Class.Costanti.S_LS150_OKAY Then
            FileTraccia.Matricola = MatricolaPeriferica
            ritorno = True
        Else
            ViewError(fase)
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestTimbro(ByVal TipoTimbro As Short) As Short
        Dim Ritorno As Integer
        Dim ret As Integer
        Dim fcicla As Boolean
        Dim iii As Integer
        'Dim resdialog As DialogResult

        Ritorno = False
        'TCollaudo.SelectedTab = TTimbriStampa
        FaseEsec()

        idmsg = 98
        'idmsg = 308
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire 10 Documenti per la prova del Timbro", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(3))
        End If


        If (dres <> DialogResult.OK) Then
            ret = Ls150Class.Costanti.S_LS150_USER_ABORT
        Else
            iii = 0
            fcicla = True
            While (fcicla)
                ls150.NomeFileImmagine = zzz + "\Dati\TimbroFImage" + IndImgF.ToString("0000") + ".bmp"
                ls150.NomeFileImmagineRetro = zzz + "\Dati\TimbroRImage" + IndImgR.ToString("0000") + ".bmp"
                ret = ls150.Timbra(Ls150Class.Costanti.S_SORTER_BAY1)
                iii += 1
                If ret = Ls150Class.Costanti.S_LS150_OKAY Then
                    Try
                        DisposeImg()

                        Application.DoEvents()
                        Pimage.Image = System.Drawing.Image.FromFile(ls150.NomeFileImmagine)
                        PImageBack.Image = System.Drawing.Image.FromFile(ls150.NomeFileImmagineRetro)
                    Catch ex As Exception
                        ' DEBUG
                        MessageBox.Show(ex.Message + " " + ex.Source + ret.ToString, "errore")
                    End Try
                End If

                Select Case ret
                    Case Ls150Class.Costanti.S_LS150_FEEDEREMPTY
                        'idmsg = 98
                        idmsg = 308
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("Inserire 10 Documenti per la prova del Timbro", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(3))
                        End If
                        If dres <> DialogResult.OK Then
                            fcicla = False
                            ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                        End If
                        ret = Ls150Class.Costanti.S_LS150_OKAY
                        iii -= 1
                    Case Ls150Class.Costanti.S_LS150_PAPER_JAM, Ls150Class.Costanti.S_LS150_DOUBLE_LEAFING_ERROR, Ls150Class.Costanti.S_LS150_JAM_AT_MICR_PHOTO, Ls150Class.Costanti.S_LS150_JAM_AT_SCANNERPHOTO, Ls150Class.Costanti.S_LS150_JAM_DOC_TOOLONG

                        idmsg = 99
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("Togliere i documenti dal percorso carta se presenti", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                        End If

                        If dres <> DialogResult.OK Then
                            fcicla = False
                            ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                        End If
                        ls150.Reset(Ls150Class.Costanti.S_RESET_FREE_PATH)
                        ret = Ls150Class.Costanti.S_LS150_OKAY
                        iii -= 1
                End Select

                If ret <> Ls150Class.Costanti.S_LS150_OKAY Or iii > TipoTimbro Then
                    fcicla = False
                    iii -= 1
                End If

                Application.DoEvents()
                IndImgF += 1
                IndImgR += 1
            End While

            If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then

                idmsg = 100
                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                    dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                Else
                    dres = MessageboxMy("Il timbro ha timbrato in modo corretto ?", "Attenzione!! ", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, Nothing)
                End If



                If (dres = DialogResult.Yes) Then
                    'LTimR.ForeColor = Color.Green
                    'LTimR.Text = "OK"
                    Ritorno = True
                Else
                    If dres = DialogResult.No Then
                        ' ii = 
                    End If
                    'LTimF.ForeColor = Color.Red
                    'LTimF.Text = "KO"

                    ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                End If
            Else
                ViewError(fase)
            End If

        End If



        FileTraccia.UltimoRitornoFunzione = ret

        If Ritorno = True Then
            FaseOk()
            FileTraccia.TimbroFronte = 1
            FileTraccia.TimbroRetro = 1
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return Ritorno
    End Function

    Public Function TestCalibraTestina(ByVal Type As Short) As Integer
        Dim ritorno As Integer

        Dim ret As Short
        Dim RangeMore As Single
        Dim RangeLess As Single
        Dim strmsg As String
        Dim TypeSpeed As Short
        Dim valore As Integer
        Dim Velocit As Integer

        Dim finfo As New CtsControls.FormInfoTest

        FileTraccia.Note = ""

        ritorno = False
        'GMicr.Enabled = True
        'LResult.Text = ""

        FaseEsec()

        fWaitButton = False

        strmsg = "Inserire nella Casella di Testo il Valore del DOCUTEST --042 di calibrazione : Togliere il Timbro"



        '   If TipoDiCollaudo = COLLAUDO_PIASTRA_LS150 Or TipoDiCollaudo = COLLAUDO_PERIFERICA_LS150 Then
        Select Case Type
            Case 0
                Velocit = 0
                ret = ls150.SetVelocita(Velocit) ' alta
                TypeSpeed = TEST_SPEED_200_DPI
                idmsg = 103
                strmsg = " Attenzione !!! Calibrazione 200 DPI"
            Case 1
                Velocit = 0
                ret = ls150.SetVelocita(Velocit) ' alta
                TypeSpeed = TEST_SPEED_300_DPI
                idmsg = 101
                strmsg = " Attenzione !!! Calibrazione 300 DPI"
            Case 2
                Velocit = 1
                If fTimbri = True Then
                    ret = ls150.SetVelocita(Velocit) ' bassa
                Else
                    ret = ls150.SetVelocita(Velocit) ' bassa
                End If
                TypeSpeed = TEST_SPEED_75_DPM
                idmsg = 102
                strmsg = " Attenzione !!! Calibrazione 200 DPI Velocità Bassa"
            Case 3
                Velocit = 0
                If fTimbri = True Then
                    ret = ls150.SetVelocita(Velocit) ' alta, la abbasa la periferica
                Else
                    ret = ls150.SetVelocita(Velocit) ' alta 
                End If
                TypeSpeed = TEST_SPEED_300_DPI_UV
                idmsg = 102
                strmsg = " Attenzione !!! Calibrazione 300 DPI UV"
            Case 4
                Velocit = 0
                If fTimbri = True Then
                    ret = ls150.SetVelocita(Velocit) ' alta, la abbasa la periferica
                Else
                    ret = ls150.SetVelocita(Velocit) ' alta 
                End If
                TypeSpeed = TEST_SPEED_300_COLOR
                idmsg = 102
                strmsg = " Attenzione !!! Calibrazione 300 DPI Colore"
        End Select
        'LICodeline.Items.Add(strmsg + "--> Valore Documento  " + TValMICR.Text)
        'TCollaudo.SelectedTab() = TMessaggi
        FileTraccia.Note += "Velocità: " + Velocit.ToString() + vbCrLf
        FileTraccia.Note += "Reply Set Velocità: " + ret.ToString()


        finfo.PointToScreen(New Point(300, 100))
        finfo.Show()
        finfo.BringToFront()




        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            StrMessaggio = Messaggio
        Else
            StrMessaggio = strmsg
        End If


        If InputboxMy(StrMessaggio, "Attenzione", True, valore, 0, 0) = DialogResult.OK Then

            idmsg = 104
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Inserire nel Feeder DOCUTEST --042 'Lato Puntini...', fino ad avvenuta calibrazione:", strmsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            End If

            ' traccio sul DB il valore documento
            FileTraccia.Note += "Valore Documento: " + valore.ToString() + vbCrLf


            If dres = DialogResult.OK Then
                ls150.list = finfo.Linfo
                ret = ls150.DoMICRCalibration3(System.Convert.ToInt32(valore), 5, TypeSpeed, finfo.Linfo)
                '' salvo sul DB sempre 
                For Each s As String In finfo.Linfo.Items
                    FileTraccia.Note += s + vbCrLf
                Next

                If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
                    ' test per calibrazione MICR ok

                    RangeMore = System.Convert.ToSingle(valore) * 4 / 100
                    RangeLess = System.Convert.ToSingle(valore) * 4 / 100

                    ' scrivo nel file di trace i valori visualizzati a video


                    If ((ls150.Percentile > (System.Convert.ToSingle(valore) - RangeLess)) And (ls150.Percentile < (System.Convert.ToSingle(valore) + RangeMore))) Then
                        ritorno = True
                    Else
                        ret = Ls150Class.Costanti.S_LS150_CALIBRATION_FAILED
                    End If

                Else
                    ViewError(fase)
                End If
            Else
                ret = Ls150Class.Costanti.S_LS150_USER_ABORT
            End If
            'Else
            ' non passerà mai di qui perche, il tipo di collaudo lo imposto io
            '   ret = Ls150Class.Costanti.S_LS150_USER_ABORT
            ' End If
        Else
            ret = Ls150Class.Costanti.S_LS150_USER_ABORT
        End If

        Velocit = 0 ' alta
        ret = ls150.SetVelocita(Velocit)


        FileTraccia.Note += vbCrLf
        FileTraccia.Note += "Velocità: " + Velocit.ToString() + vbCrLf
        FileTraccia.Note += "Reply Set Velocità: " + ret.ToString()

        FileTraccia.UltimoRitornoFunzione = ret

        finfo.Close()

        If ritorno = True Then
            FaseOk()
            FileTraccia.CalMicr = 1
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    'La calibrazione della velocità di campionamento permette alla periferica di eseguire in modo corretto la decodifica dei caratteri micr  
    Public Function TestTempoCamp(ByVal Type As Short) As Integer
        Dim ritorno As Integer
        Dim ii As Short
        Dim TypeSpeed As Short
        Dim strmsg As String = ""
        Dim finfo As New CtsControls.FormInfoTest
        Dim strS As String
        Dim RangeTollerance As Single
        Dim fCalibrationDone As Boolean
        Dim replycode As Short

        ii = 0
        ritorno = False
        FaseEsec()
        'Lo azzero da ogni giro altrimenti da' TEST OK se annullo ...190128-U01
        ls150.NrLettureOk = 0

        'If (fMacchinaUv = False And Type = 3) Then
        '    'ho una macchina che non e' UV che non deve fare questa fase 
        '    ritorno = True
        '    replycode = Ls150Class.Costanti.S_LS150_OKAY
        '    GoTo uscita
        'End If

        'If (FMacchinaColori = False And Type = 4) Then
        '    'ho una macchina che non e' UV che non deve fare questa fase 
        '    ritorno = True
        '    replycode = Ls150Class.Costanti.S_LS150_OKAY
        '    GoTo uscita
        'End If

        'If (fTimbri = False And Type = 2) Then
        '    'ho una macchina che non e' UV che non deve fare questa fase 
        '    ritorno = True
        '    replycode = Ls150Class.Costanti.S_LS150_OKAY
        '    GoTo uscita
        'End If

        Select Case Type
            Case 0
                replycode = ls150.SetVelocita(0) ' alta
                If (replycode = Ls150Class.Costanti.S_LS150_ALREADY_OPEN) Then
                    replycode = Ls150Class.Costanti.S_LS150_OKAY
                End If
                If (replycode <> Ls150Class.Costanti.S_LS150_OKAY) Then
                    FaseKo()
                    FileTraccia.UltimoRitornoFunzione = 0 'replycode
                    FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
                    Return replycode
                Else
                    TypeSpeed = TEST_SPEED_200_DPI
                    idmsg = 105
                    strmsg = " Attenzione !!! Calibrazione 200 DPI"
                End If
            Case 1
                ls150.SetVelocita(0) ' alta
                TypeSpeed = TEST_SPEED_300_DPI
                idmsg = 106
                strmsg = " Attenzione !!! Calibrazione 300 DPI"
            Case 2
                If fTimbri = True Then
                    ls150.SetVelocita(1) ' bassa
                Else
                    ls150.SetVelocita(1) ' bassa
                End If

                TypeSpeed = TEST_SPEED_75_DPM
                idmsg = 107
                strmsg = " Attenzione !!! Calibrazione 200 DPI Velocità Bassa"
            Case 3

                ls150.SetVelocita(0) ' alta di def per colori
                TypeSpeed = TEST_SPEED_300_DPI_UV
                idmsg = 107
                strmsg = " Attenzione !!! Calibrazione 300 DPI UV"
            Case 4

                ls150.SetVelocita(0) ' alta di def per colori
                TypeSpeed = TEST_SPEED_300_COLOR
                idmsg = 107
                strmsg = " Attenzione !!! Calibrazione 300 DPI Colori"
        End Select
        strmsg = " Attenzione !!! Calibrazione 200 DPI "
        finfo.PointToScreen(New Point(300, 100))
        finfo.Show()
        finfo.BringToFront()
        'LICodeline.Items.Add(strmsg)

        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire DOCUTEST --042 dalla parte delle Barrette Continue", "Attenzione!!! " + strmsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        End If


        If (dres = DialogResult.OK) Then

            ls150.LastBeforeTimeSample1 = ls150.LastBeforeTimeSample2 = ls150.LastBeforeTimeSample3 = 0

            strS = ReadValue("TimeSample", "Tollerance", Application.StartupPath + "\\Ls150_MICRcalibration.ini")
            'length = GetPrivateProfileString("DocStats", "Doc1", "DOCT-R013-00", value, length, ls40.NomeFileUltrasonic)

            If (strS = "") Then
                RangeTollerance = 1
            Else
                RangeTollerance = Convert.ToSingle(strS)
            End If

            ii = 0
            ls150.NrLettureOk = 0
            fCalibrationDone = False
            ls150.TimeSampleVal = -1


            Do
                'If (MessageboxMy("Inserire DOCUTEST -042 dalla parte delle Barrette Continue", "Attenzione!!! " + strmsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing) = DialogResult.OK) Then
                fCalibrationDone = ls150.DoTimeSampleMICRCalibration(TypeSpeed, RangeTollerance)
                'LICodeline.Items.Add("Valore min: " + ls150.llMin.ToString + " Valore Medio : " + ls150.llMedia.ToString + "  Valore Massimo : " + ls150.llMax.ToString)
                ii += 1
                Dim strmsginfo As String
                strmsginfo = "Ciclo : " + ii.ToString + " Valore Massimo : " + ls150.llMax.ToString + " Valore minimo : " + ls150.llMin.ToString + "Valore Campionamento : " + ls150.TimeSampleVal.ToString
                finfo.Linfo.Items.Add(strmsginfo)
                finfo.Linfo.SelectedIndex = finfo.Linfo.Items.Count - 1
                FileTraccia.Note += strmsginfo + vbCrLf
                finfo.Refresh()
                'while( (fCalibrationDone == FALSE) && (ii <= 20) && (NrLettureOk < stParDocHandle.MICR_TS_NrRead));
            Loop While ((ii < 20) And (ls150.NrLettureOk < 3) And (fCalibrationDone = False))


            'Loop While ((ii < 20) And (ls150.NrLettureOk < 3) And (replycode = Ls150Class.Costanti.S_LS150_RETRY))
        Else
            replycode = Ls150Class.Costanti.S_LS150_USER_ABORT
            ritorno = False
            'ret = False
        End If
        ls150.SetVelocita(0)
        If (replycode <> Ls150Class.Costanti.S_LS150_RETRY And ls150.NrLettureOk < 3) Then
            ritorno = False

        Else
            If ii >= 20 Then
                ritorno = False
            Else
                ritorno = True
            End If
        End If

uscita:
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        finfo.Close()
        FileTraccia.UltimoRitornoFunzione = replycode
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestCalibraTestinaN(ByVal RminG As Short, ByVal RmaxG As Short, ByVal RminU As Short, ByVal RmaxU As Short, ByVal RminC As Short, ByVal RmaxC As Short) As Integer

        Dim ritorno As Integer
        Dim ret As Short
        Dim strmsg As String
        Dim TypeSpeed As Short
        Dim valore As Integer
        Dim finfo As New CtsControls.FormInfoTest
        Dim CalibrazioneT1 As Single
        Dim CalibrazioneT3 As Single
        Dim CalibrazioneT4 As Single   'per Ls150 UV e Colore
        Dim RT As Single
        Dim Rmin As Single
        Dim Rmax As Single
        Dim ResultCalibr As Single

        FileTraccia.Note = ""
        ritorno = False
        CalibrazioneT1 = 0
        CalibrazioneT3 = 0
        CalibrazioneT4 = 0
        RT = 0
        Rmin = 0
        Rmax = 0
        ResultCalibr = 0

        FaseEsec()

        ret = ls150.SetVelocita(VEL_ALTA) ' alta
        TypeSpeed = TEST_SPEED_200_DPI
        strmsg = " Attenzione !!! Calibrazione 200 DPI"

        finfo.PointToScreen(New Point(300, 100))
        finfo.Show()
        finfo.BringToFront()

        idmsg = 12
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            StrMessaggio = Messaggio
        Else
            StrMessaggio = "Inserire nella Casella di Testo il Valore del DOCUTEST --042 di calibrazione"
        End If


        If InputboxMy(StrMessaggio, "Attenzione", True, valore, 60, 140) = DialogResult.OK Then

            idmsg = 104
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                immamsg = My.Resources.LATO_PUNTINI
                dres = MessageboxMy(Messaggio, "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Question, immamsg)
                'dres = MessageboxMy2(StrMessaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                immamsg = My.Resources.LATO_PUNTINI
                dres = MessageboxMy("Inserire nel Feeder DOCUTEST --042 'Lato Puntini...', fino ad avvenuta calibrazione:", strmsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            End If

            ' traccio sul DB il valore documento
            FileTraccia.Note += "Valore Documento: " + valore.ToString() + vbCrLf

            If dres = DialogResult.OK Then
                ls150.list = finfo.Linfo
                ret = ls150.DoMICRCalibration3(System.Convert.ToInt32(valore), 5, TypeSpeed, finfo.Linfo)
                '' salvo sul DB sempre 
                For Each s As String In finfo.Linfo.Items
                    FileTraccia.Note += s + vbCrLf
                Next

                If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
                    CalibrazioneT1 = ls150.ValoreCalibrazione
                    ritorno = True
                    'Else
                    '   ViewError(fase)
                End If
            Else
                ret = Ls150Class.Costanti.S_LS150_USER_ABORT
            End If

            'finfo.Close()

            If (ritorno = True) Then

                If (FMacchinaColori = True) Then
                    '54
                    ret = ls150.SetVelocita(VEL_ALTA) ' alta 
                    TypeSpeed = TEST_SPEED_300_COLOR
                    'strmsg = " Attenzione !!! Calibrazione 300 DPI Colore"
                ElseIf (fMacchinaUv = True) Then
                    '53
                    ret = ls150.SetVelocita(VEL_ALTA) ' alta 
                    TypeSpeed = TEST_SPEED_300_DPI_UV
                    'strmsg = " Attenzione !!! Calibrazione 300 DPI UV"
                Else
                    'If (fTimbri = True) Then
                    'Grigio - 47
                    ret = ls150.SetVelocita(VEL_BASSA) ' bassa
                    TypeSpeed = TEST_SPEED_75_DPM
                    'strmsg = " Attenzione !!! Calibrazione 200 DPI Velocità Bassa"
                    'Else
                    'ret = ls150.SetVelocita(VEL_ALTA) ' alta 
                    'TypeSpeed = TEST_SPEED_300_DPI
                End If
                'End If



                ' traccio sul DB il valore documento
                FileTraccia.Note += "Valore Documento: " + valore.ToString() + vbCrLf

                If dres = DialogResult.OK Then
                    'ls150.list = finfo1.Linfo
                    ls150.list = finfo.Linfo

                    'ret = ls150.DoMICRCalibration3(System.Convert.ToInt32(valore), 5, TypeSpeed, finfo1.Linfo)
                    ret = ls150.DoMICRCalibration3(System.Convert.ToInt32(valore), 5, TypeSpeed, finfo.Linfo)
                    '' salvo sul DB sempre 
                    'For Each s As String In finfo1.Linfo.Items
                    For Each s As String In finfo.Linfo.Items
                        FileTraccia.Note += s + vbCrLf
                    Next

                    If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
                        ' test per calibrazione MICR ok

                        'RangeMore = System.Convert.ToSingle(valore) * 4 / 100
                        'RangeLess = System.Convert.ToSingle(valore) * 4 / 100

                        If (fMacchinaUv = True Or FMacchinaColori = True) Then
                            CalibrazioneT4 = ls150.ValoreCalibrazione
                        Else
                            CalibrazioneT3 = ls150.ValoreCalibrazione
                        End If

                        If (fMacchinaUv = True Or FMacchinaColori = True) Then
                            RT = (CalibrazioneT4 / CalibrazioneT1)
                            If (fMacchinaUv = True) Then
                                Rmin = (RminU / 100)
                                Rmax = (RmaxU / 100)
                            ElseIf (FMacchinaColori = True) Then
                                Rmin = (RminC / 100)
                                Rmax = (RmaxC / 100)
                            End If
                        Else
                            'If (fTimbri = True) Then
                            'perfica(grigio)
                            RT = (CalibrazioneT3 / CalibrazioneT1)
                            Rmin = (RminG / 100)
                            Rmax = (RmaxG / 100)
                            'Else()
                            'GoTo uscita_ok
                        End If

                    End If

                    If (RT >= Rmin And RT <= Rmax) Then
                        If (fMacchinaUv = True) Then
                            Try
                                't2   300 dpi 
                                ResultCalibr = 29.53 - (0.0396 * CalibrazioneT1) + (0.1007 * CalibrazioneT4) + (0.00527 * CalibrazioneT1 * CalibrazioneT4)
                                ret = ls150.SetTrimmer(Math.Round(ResultCalibr), TEST_SPEED_300_DPI)
                                FileTraccia.Note += "ValEquT2: " + Math.Round(ResultCalibr).ToString() + vbCrLf
                                't3  timbro 
                                ResultCalibr = -22.32 - 0.142 * CalibrazioneT1 + 1.133 * CalibrazioneT4 - 0.00698 * (CalibrazioneT1 * CalibrazioneT1) - 0.004056 * (CalibrazioneT4 * CalibrazioneT4) + 0.01054 * CalibrazioneT1 * CalibrazioneT4
                                ret = ls150.SetTrimmer(Math.Round(ResultCalibr), TEST_SPEED_75_DPM)
                                FileTraccia.Note += "ValEquT3: " + Math.Round(ResultCalibr).ToString() + vbCrLf
                            Catch ex As Exception
                                MessageBox.Show(ex.ToString())
                                ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                            End Try

                        ElseIf (FMacchinaColori = True) Then
                            Try
                                't2 300 dpi 
                                ResultCalibr = 26.74 + 0.6449 * CalibrazioneT1 - 0.096 * CalibrazioneT4 + 0.001742 * (CalibrazioneT4 * CalibrazioneT4)
                                ret = ls150.SetTrimmer(Math.Round(ResultCalibr), TEST_SPEED_300_DPI)
                                FileTraccia.Note += "ValEquT2: " + Math.Round(ResultCalibr).ToString() + vbCrLf
                                't3 timbro 
                                ResultCalibr = -6.74 + 0.6078 * CalibrazioneT1 + 0.591 * CalibrazioneT4
                                ret = ls150.SetTrimmer(Math.Round(ResultCalibr), TEST_SPEED_75_DPM)
                                FileTraccia.Note += "ValEquT3: " + Math.Round(ResultCalibr).ToString() + vbCrLf
                            Catch ex As Exception
                                MessageBox.Show(ex.ToString())
                                ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                            End Try

                        Else
                            Try
                                ResultCalibr = 44.715
                                ResultCalibr += (0.24 * CalibrazioneT1)
                                ResultCalibr -= (0.2305 * CalibrazioneT3)
                                ResultCalibr -= (0.00285 * (CalibrazioneT1 * CalibrazioneT1))
                                ResultCalibr += (0.001661 * (CalibrazioneT3 * CalibrazioneT3))
                                ResultCalibr += 0.00643 * (CalibrazioneT1 * CalibrazioneT3)

                                ret = ls150.SetTrimmer(Math.Round(ResultCalibr), TEST_SPEED_300_DPI)
                                FileTraccia.Note += "ValEquT2: " + Math.Round(ResultCalibr).ToString() + vbCrLf

                            Catch ex As Exception
                                MessageBox.Show(ex.ToString())
                                ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                            End Try



                        End If

                        If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
                            'uscita_ok:
                            ritorno = True
                        Else
                            ritorno = False
                        End If

                    Else
                        'errore 
                        ritorno = False
                        ret = Ls150Class.Costanti.S_LS150_CALIBATRION_MICR_NEW
                        ViewError(fase)
                        FileTraccia.Note += "RT: " + RT.ToString() + vbCrLf
                        FileTraccia.Note += "Rmin: " + Rmin.ToString() + vbCrLf
                        FileTraccia.Note += "Rmax: " + Rmax.ToString() + vbCrLf
                    End If

                Else  'If InputboxMy(StrMessaggio, "Attenzione", True, valore) = DialogResult.OK Then
                    ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                End If



            Else
                ViewError(fase)
            End If
        End If




        finfo.Close()

        ls150.SetVelocita(VEL_ALTA)


        'FileTraccia.Note += vbCrLf
        'FileTraccia.Note += "Velocità: " + Velocit.ToString() + vbCrLf
        'FileTraccia.Note += "Reply Set Velocità: " + ret.ToString()

        FileTraccia.UltimoRitornoFunzione = ret



        If ritorno = True Then
            FaseOk()
            FileTraccia.CalMicr = 1
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    'La calibrazione della velocità di campionamento permette alla periferica di eseguire in modo corretto la decodifica dei caratteri micr  
    Public Function TestTempoCampN(ByVal Type As Short) As Integer
        Dim ritorno As Integer
        Dim ii As Short
        Dim TypeSpeed As Short
        Dim strmsg As String = ""
        Dim finfo As New CtsControls.FormInfoTest
        Dim strS As String
        Dim RangeTollerance As Single
        Dim fCalibrationDone As Boolean
        Dim replycode As Short

        ls150.ReturnC.FunzioneChiamante = "TestTempoCampN"

        ii = 0
        'Lo azzero da ogni giro altrimenti da' TEST OK se annullo ...190128-U01
        ls150.NrLettureOk = 0
        ritorno = False
        FaseEsec()

        If (fMacchinaUv = False And Type = 3) Then
            'ho una macchina che non e' UV che non deve fare questa fase 
            ritorno = True
            replycode = Ls150Class.Costanti.S_LS150_OKAY
            GoTo uscita
        End If

        If (FMacchinaColori = False And Type = 4) Then
            'ho una macchina che non e' UV che non deve fare questa fase 
            ritorno = True
            replycode = Ls150Class.Costanti.S_LS150_OKAY
            GoTo uscita
        End If

        'If (fTimbri = False And Type = 2) Then
        '    'ho una macchina che non ha il timbro e non faccio questa fase ...
        '    ritorno = True
        '    replycode = Ls150Class.Costanti.S_LS150_OKAY
        '    GoTo uscita
        'End If

        Select Case Type
            Case 0
                replycode = ls150.SetVelocita(0) ' alta
                If (replycode = Ls150Class.Costanti.S_LS150_ALREADY_OPEN) Then
                    replycode = Ls150Class.Costanti.S_LS150_OKAY
                End If
                If (replycode <> Ls150Class.Costanti.S_LS150_OKAY) Then
                    FaseKo()
                    FileTraccia.UltimoRitornoFunzione = 0 'replycode
                    FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
                    Return replycode
                Else
                    TypeSpeed = TEST_SPEED_200_DPI
                    idmsg = 105
                    strmsg = " Attenzione !!! TEST_SPEED_200_DPI"
                End If
            Case 1
                ls150.SetVelocita(0) ' alta
                TypeSpeed = TEST_SPEED_300_DPI
                idmsg = 106
                strmsg = " Attenzione !!! TEST_SPEED_300_DPI"
            Case 2
                If fTimbri = True Then
                    ls150.SetVelocita(1) ' bassa
                Else
                    ls150.SetVelocita(1) ' bassa
                End If

                TypeSpeed = TEST_SPEED_75_DPM
                idmsg = 107
                strmsg = " Attenzione !!! TEST_SPEED_75_DPM"
            Case 3

                ls150.SetVelocita(0) ' alta di def per colori
                TypeSpeed = TEST_SPEED_300_DPI_UV
                idmsg = 107
                strmsg = " Attenzione !!! TEST_SPEED_300_DPI_UV"
            Case 4

                ls150.SetVelocita(0) ' alta di def per colori
                TypeSpeed = TEST_SPEED_300_COLOR
                idmsg = 107
                strmsg = " Attenzione !!! TEST_SPEED_300_COLOR"
        End Select
        'strmsg = " Attenzione !!! Calibrazione 200 DPI "
        finfo.PointToScreen(New Point(300, 100))
        finfo.Show()
        finfo.BringToFront()
        'LICodeline.Items.Add(strmsg)

        If (Type = 0) Then
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                immamsg = My.Resources.LATO_BARRETTE
                dres = MessageboxMy(Messaggio, strmsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, immamsg)
            Else
                immamsg = My.Resources.LATO_BARRETTE
                dres = MessageboxMy("Inserire DOCUTEST --042 dalla parte delle Barrette Continue", strmsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            End If
        Else
            dres = DialogResult.OK
        End If
       
        If (dres = DialogResult.OK) Then

            ls150.LastBeforeTimeSample1 = ls150.LastBeforeTimeSample2 = ls150.LastBeforeTimeSample3 = 0

            strS = ReadValue("TimeSample", "Tollerance", Application.StartupPath + "\\Ls150_MICRcalibration.ini")
            'length = GetPrivateProfileString("DocStats", "Doc1", "DOCT-R013-00", value, length, ls40.NomeFileUltrasonic)

            If (strS = "") Then
                RangeTollerance = 1
            Else
                RangeTollerance = Convert.ToSingle(strS)
            End If

            ii = 0
            ls150.NrLettureOk = 0
            fCalibrationDone = False
            ls150.TimeSampleVal = -1


            Do
                'If (MessageboxMy("Inserire DOCUTEST -042 dalla parte delle Barrette Continue", "Attenzione!!! " + strmsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing) = DialogResult.OK) Then
                fCalibrationDone = ls150.DoTimeSampleMICRCalibration(TypeSpeed, RangeTollerance)
                'LICodeline.Items.Add("Valore min: " + ls150.llMin.ToString + " Valore Medio : " + ls150.llMedia.ToString + "  Valore Massimo : " + ls150.llMax.ToString)
                ii += 1
                Dim strmsginfo As String
                strmsginfo = "Ciclo : " + ii.ToString + " Valore Massimo : " + ls150.llMax.ToString + " Valore minimo : " + ls150.llMin.ToString + " Valore Campionamento : " + ls150.TimeSampleVal.ToString
                finfo.Linfo.Items.Add(strmsginfo)
                finfo.Linfo.SelectedIndex = finfo.Linfo.Items.Count - 1
                FileTraccia.Note += strmsginfo + vbCrLf
                finfo.Refresh()
                'while( (fCalibrationDone == FALSE) && (ii <= 20) && (NrLettureOk < stParDocHandle.MICR_TS_NrRead));
            Loop While ((ii < 20) And (ls150.NrLettureOk < 3) And (fCalibrationDone = False))


            'Loop While ((ii < 20) And (ls150.NrLettureOk < 3) And (replycode = Ls150Class.Costanti.S_LS150_RETRY))
        Else
            replycode = Ls150Class.Costanti.S_LS150_USER_ABORT
            ritorno = False
            'ret = False
        End If
        ls150.SetVelocita(0)
        If (replycode <> Ls150Class.Costanti.S_LS150_RETRY And ls150.NrLettureOk < 3) Then
            ritorno = False
        Else
            If ii >= 20 Then
                ritorno = False
            Else
                ritorno = True
            End If
        End If

uscita:
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        finfo.Close()
        FileTraccia.UltimoRitornoFunzione = replycode
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestLettura(ByVal cmc7 As Short, ByVal e13b As Short, ByVal Tipo_comtrollo As Short) As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim smessage As String = ""
        'TMicr.Enabled = True
        ritorno = False
        ' per LS150 nei test lettura passo sempre il valore 3
        ' qui devo fare il test di che documenti verificare
        ' in base al fatto che ci sia il timbro o meno

        FaseEsec()
        ' la sequenza ls150 passa o 3 o 13 nel caso di macchiana colori
        If Tipo_comtrollo = CheckCodeline.CONTROLLO_LS150_TIMBRO Or Tipo_comtrollo = CheckCodeline.CONTROLLO_LS150_C_TIMBRO Then
            If fTimbri = True Then
                ' il tipo controllo è già corretto
            Else
                If Tipo_comtrollo = CheckCodeline.CONTROLLO_LS150_TIMBRO Then
                    Tipo_comtrollo = CheckCodeline.CONTROLLO_LS150_NO_TIMBRO
                End If
                If Tipo_comtrollo = CheckCodeline.CONTROLLO_LS150_C_TIMBRO Then
                    Tipo_comtrollo = CheckCodeline.CONTROLLO_LS150_C_NO_TIMBRO
                End If

            End If

        End If


        '29-12-2010 commentato, probabilmente non passo mai di qui 
        'If (fTimbri = True And (Tipo_comtrollo <> CheckCodeline.CONTROLLO_SMAG_CMC7) And (Tipo_comtrollo <> CheckCodeline.CONTROLLO_SMAG_E13B)) Then
        '    Tipo_comtrollo = CheckCodeline.CONTROLLO_LS150_TIMBRO
        '    'Else
        '    '    Tipo_comtrollo = CheckCodeline.CONTROLLO_LS150_NO_TIMBRO
        'End If




        If Tipo_comtrollo = CheckCodeline.CONTROLLO_LS150_NO_TIMBRO Or Tipo_comtrollo = CheckCodeline.CONTROLLO_LS150_C_NO_TIMBRO Then
            smessage = "Inserire documenti per lettura DOCUTEST 63-64-46-48-61-62 "
            idmsg = 108
        End If

        If Tipo_comtrollo = CheckCodeline.CONTROLLO_LS150_TIMBRO Or Tipo_comtrollo = CheckCodeline.CONTROLLO_LS150_C_TIMBRO Then
            smessage = "Inserire documenti per lettura DOCUTEST 66-65-46-48-61-62 "
            idmsg = 109
        End If

        If (Tipo_comtrollo = CheckCodeline.CONTROLLO_SMAG_CMC7) Then
            smessage = "Inserire documento per lettura DOCUTEST 48"
            idmsg = 1200 ' fittizio
        End If
        If (Tipo_comtrollo = CheckCodeline.CONTROLLO_SMAG_E13B) Then
            smessage = "Inserire documento per lettura DOCUTEST 46"
            idmsg = 1200 ' fittizio
        End If






        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy(smessage, "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        End If


        If dres <> DialogResult.OK Then
            ret = Ls150Class.Costanti.S_LS150_USER_ABORT
        Else
            ret = LeggiCodelinesAuto(cmc7, e13b, Tipo_comtrollo)
        End If

        If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
            ritorno = True
        Else
            ritorno = False
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestLetturaN(ByVal cmc7 As Short, ByVal e13b As Short, ByVal Tipo_controllo As Short) As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim smessage As String = ""

        ritorno = False

        FaseEsec()
        ' la sequenza ls150 passa o 3 o 13 nel caso di macchiana colori
        'If Tipo_controllo = CheckCodeline.CONTROLLO_LS150_TIMBRO Then
        '    If fTimbri = True Then
        '        ' il tipo controllo è già corretto
        '    Else
        '        If Tipo_controllo = CheckCodeline.CONTROLLO_LS150_TIMBRO Then
        '            Tipo_controllo = CheckCodeline.CONTROLLO_LS150_NO_TIMBRO
        '        End If
        '    End If

        'End If


        If Tipo_controllo = CheckCodeline.CONTROLLO_LS150_FASE_RIDUZIONE_COSTO Then
            smessage = "Inserire documenti per lettura DOCUTEST062 e DOCT-R147"
            idmsg = 302
        End If

        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy(smessage, "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        End If


        If dres <> DialogResult.OK Then
            ret = Ls150Class.Costanti.S_LS150_USER_ABORT
        Else
            ret = LeggiCodelinesAuto(cmc7, e13b, Tipo_controllo)
        End If

        If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
            ritorno = True
        Else
            ritorno = False
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestCarta(ByVal ndoc As Short) As Integer

        'Dim ritorno As Short
        Dim ritorno As Integer
        Dim ret As Integer
        Dim maxw As Integer
        Dim percentuale As Single
        Dim rr As Short
        Dim fstop As Short
        Dim media As Single
        Dim massimo, minimo As Short
        Dim somma As Integer
        Dim imagra As Bitmap
        Dim gra As Graphics
        Dim penna As New Pen(System.Drawing.Color.Red)
        Dim punti() As Point
        Dim cicli As Short
        Dim font1 As New Font("Time New Roman", 8, FontStyle.Regular, GraphicsUnit.Pixel)

        Dim nrLoop As Integer = 1


        'se devo fare la card a colori non faccio la restante ...
        'se scanner zebrato montato il test non si puo' fare....
        'If (fZebrato = True Or fcardCol = 0) Then
        If (fZebrato = 1) Then
            ret = Ls150Class.Costanti.S_LS150_OKAY
            ritorno = True
        Else



            writeDebug("Cancella File", "TestCarta")
            writeDebug("Inizio test", "TestCarta")

            'If (fSoftware = SW_BAR2D Or fSoftware = SW_TOP_BAR2D) Then
            'ritorno = True
            'Else
            ritorno = False
            rr = 1
            'TCollaudo.SelectedTab = TTimbriStampa
            FaseEsec()

            writeDebug("Dopo : FaseExec()", "TestCarta")

            cicli = 0

            writeDebug("fstop = " & fstop.ToString(), "TestCarta")

            While (fstop = False)
                penna.Color = System.Drawing.Color.Red
                idmsg = 110

                writeDebug("Prima di CercaMessaggio()", "TestCarta")

                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                    dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                Else
                    dres = MessageboxMy("Inserire carta  DOCUTEST --043 dall' ingresso lineare", "Attenzione :  Ciclo : " + (cicli + 1).ToString + "/" + (ndoc + 1).ToString, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                End If

                writeDebug("Dopo CercaMessaggio( dres = " + dres.ToString() + ")", "TestCarta")

                If (dres <> DialogResult.OK) Then
                    ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                    fstop = True
                Else
                    writeDebug("Prima di DisposeImg()", "TestCarta")
                    DisposeImg()
                    writeDebug("Dopo DisposeImg()", "TestCarta")
                    'TCollaudo.SelectedTab = TMessaggi
                    StrMessaggio = "Test Carta " + (cicli + 1).ToString + "/" + (ndoc + 1).ToString + " ciclo"
                    'TMessaggi.Refresh()
                    ls150.NomeFileImmagine = zzz + "\Dati\cardImage" + IndImgF.ToString("0000") + ".bmp"
                    ls150.NomeFileImmagineRetro = zzz + "\Dati\cardImageR" + IndImgR.ToString("0000") + ".bmp"
                    writeDebug("Prima di LeggiImgCard()", "TestCarta")
                    ret = ls150.LeggiImmCard(System.Convert.ToSByte(Ls150Class.Costanti.S_SIDE_ALL_IMAGE), Ls150Class.Costanti.S_SCAN_MODE_256GR300, rr)
                    writeDebug("Dopo LeggiImgCard( ret = " + ret.ToString() + ")", "TestCarta")
                    Select Case ret
                        Case Ls150Class.Costanti.S_LS150_PAPER_JAM, + _
                        Ls150Class.Costanti.S_LS150_JAM_AT_MICR_PHOTO, + _
                        Ls150Class.Costanti.S_LS150_JAM_AT_SCANNERPHOTO, + _
                        Ls150Class.Costanti.S_LS150_JAM_DOC_TOOLONG
                            idmsg = 111
                            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                            Else
                                dres = MessageboxMy("ATTENZIONE CARTA BLOCCATA LIBERARE IL PERCORSO", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                            End If

                            If (dres <> DialogResult.OK) Then
                                ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                                fstop = True
                            Else
                                ls150.Reset(Ls150Class.Costanti.S_RESET_FREE_PATH)
                            End If
                        Case Ls150Class.Costanti.S_LS150_FEEDEREMPTY
                            '  MessageboxMy("Inserire carta  DOCUTEST - 043 da ingresso lineare", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                        Case -1
                            idmsg = 112
                            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                            Else
                                dres = MessageboxMy("DOCUTEST --043 Inserito in modo non corretto", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                            End If

                            If (dres <> DialogResult.OK) Then
                                ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                                fstop = True
                            Else
                            End If
                        Case Ls150Class.Costanti.S_LS150_OKAY
                            If rr >= 0 Then
                                minimo = 32000
                                massimo = 0
                                media = 0
                                somma = 0
                                ' rr = ret  --  Fatto ritornare dalla funzione Ale !
                                ret = Ls150Class.Costanti.S_LS150_OKAY
                                writeDebug("Case OK !!", "TestCarta")
                                ReDim punti(rr)
                                imagra = New Bitmap(160, 100, Imaging.PixelFormat.Format24bppRgb)

                                gra = Graphics.FromImage(imagra)
                                writeDebug("Dopo Graphics.FromImage()", "TestCarta")
                                Dim trasmatrix As New Drawing2D.Matrix(1, 0, 0, -1, 0, 100)
                                gra.MultiplyTransform(trasmatrix)

                                writeDebug("Prima del for( rr = " + rr.ToString() + ")", "TestCarta")
                                For ii = 0 To rr
                                    If (Ls150Class.Periferica.Distanze(ii) < minimo) Then
                                        minimo = Ls150Class.Periferica.Distanze(ii)
                                    End If
                                    If (Ls150Class.Periferica.Distanze(ii) > massimo) Then
                                        massimo = Ls150Class.Periferica.Distanze(ii)
                                    End If
                                    somma += Ls150Class.Periferica.Distanze(ii)
                                    punti(ii).X = ii
                                    punti(ii).Y = Ls150Class.Periferica.Distanze(ii)

                                Next
                                media = somma / (rr + 1)
                                For ii = 0 To rr
                                    punti(ii).Y -= 254
                                    punti(ii).Y += 50
                                Next




                                gra.FillRectangle(New SolidBrush(Color.White), New Rectangle(0, 0, 160, 100))
                                gra.DrawLines(penna, punti)
                                penna.Color = System.Drawing.Color.Blue
                                gra.DrawLine(penna, 0, 50, 160, 50)
                                penna.Color = System.Drawing.Color.Gold
                                gra.DrawLine(penna, 0, 50 + 12, 160, 50 + 12)
                                gra.DrawLine(penna, 0, 50 - 12, 160, 50 - 12)


                                PFronte.Image = System.Drawing.Image.FromFile(zzz + "\Dati\cardImage" + IndImgF.ToString("0000") + ".bmp")
                                writeDebug("PFronte.Image = ", "TestCarta")
                                PRetro.Image = System.Drawing.Image.FromFile(zzz + "\Dati\cardImageR" + IndImgR.ToString("0000") + ".bmp")
                                writeDebug("PRetro.Image = ", "TestCarta")
                                Pimage.Image = PFronte.Image
                                writeDebug("Pimage.Image = ", "TestCarta")
                                PImageBack.Image = PRetro.Image
                                writeDebug("PImageBack.Image = ", "TestCarta")
                                If (PFronte.Image.Width > PRetro.Image.Width) Then
                                    maxw = PFronte.Image.Width
                                Else
                                    maxw = PRetro.Image.Width
                                End If

                                percentuale = ((System.Math.Abs(PFronte.Image.Width - PRetro.Image.Width)) / maxw) * 100

                                If (percentuale > 10) Then
                                    MessageboxMy(percentuale.ToString, "d", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                                    ritorno = False
                                Else
                                    'MessageBox.Show("ANALISI VELCITA'" + vbCrLf _
                                    '                + "Media : " + media.ToString("F") + vbCrLf _
                                    '                + "Massimo  : " + massimo.ToString + vbCrLf _
                                    '                + "Minimo : " + minimo.ToString + vbCrLf _
                                    '                + "Valore Teorico Velorità' : 265" + vbCrLf _
                                    '                + "PERCENTUALI" + vbCrLf _
                                    '                + "Rispetto al Valore teorico:  " + (((265 - minimo) / 265) * 100).ToString("F") + "   " + (((massimo - 265) / 265) * 100).ToString("F") + vbCrLf _
                                    '                + "Rispetto al Valore di Media : " + (((media - minimo) / media) * 100).ToString("F") + "   " + (((massimo - media) / media) * 100).ToString("F") + vbCrLf _
                                    '                , "Risultati Anlisi", MessageBoxButtons.OK)
                                    MessageboxMy("ANALISI VELOCITA'" + vbCrLf _
                                                    + " Valore Teorico Velocità' : 254" + vbCrLf _
                                                    + "Media : " + media.ToString("F") + " " _
                                                    + " Massimo  : " + massimo.ToString _
                                                    + " Minimo : " + minimo.ToString + vbCrLf _
                                                    + "PERCENTUALI" + vbCrLf _
                                                    + "Rispetto al Valore teorico: Min -" + (((254 - minimo) / 254) * 100).ToString("F") + "   MAX " + (((massimo - 254) / 254) * 100).ToString("F") + vbCrLf _
                                                    + "Rispetto al Valore di Media : Min -" + (((media - minimo) / media) * 100).ToString("F") + "   MAX " + (((massimo - media) / media) * 100).ToString("F") + vbCrLf _
                                                    , "Risultati Analisi", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, imagra)
                                End If
                                If cicli = ndoc Then
                                    fstop = True
                                End If
                                cicli += 1
                            Else
                                writeDebug("If Else -*- errore = " + ret.ToString(), "TestCarta")
                                'MessageboxMy("Errore = " + ret.ToString() + " non gestito !" + vbCrLf + "Prego contattare Progetto CTS !!!!", "Test Carta", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                MessageboxMy("Errore = " + ret.ToString() + " non gestito !" + vbCrLf + "Verificare che la carta inserita sia il DOCUTEST --043 ", "Test Carta", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                fstop = True
                                ritorno = False
                                ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                            End If

                        Case Else
                            writeDebug("Case Else -*- errore = " + ret.ToString(), "TestCarta")
                            MessageboxMy("Errore = " + ret.ToString() + " non gestito !" + vbCrLf + "Verificare che la carta inserita sia il DOCUTEST --043", "Test Carta", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                            fstop = True
                            ritorno = False
                            ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                    End Select
                End If
                writeDebug("End While loop = " + nrLoop.ToString(), "TestCarta")
                nrLoop += 1
            End While

            'End If

            writeDebug("End While fine", "TestCarta")

        End If

        FileTraccia.UltimoRitornoFunzione = ret
        If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
            ritorno = True
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If

        writeDebug("ls150.ReturnC.FunzioneChiamante = " + ls150.ReturnC.FunzioneChiamante, "TestCarta")
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        writeDebug("Fine", "TestCarta")
        Return ritorno

    End Function

    Public Function TestCartaColori(ByVal ncili As Short) As Integer

        Dim ritorno As Short
        Dim ret As Short
        Dim rr As Short
        Dim fstop As Short
        'Dim media As Single
        'Dim massimo, minimo As Short
        'Dim somma As Integer
        'Dim imagra As Bitmap
        'Dim gra As Graphics
        Dim penna As New Pen(System.Drawing.Color.Red)
        'Dim punti() As Point
        Dim cicli As Short
        Dim font1 As New Font("Time New Roman", 8, FontStyle.Regular, GraphicsUnit.Pixel)
        Dim fBarc As CtsControls.FBarcodes
        Dim fAspetta As CtsControls.FWait
        Dim fcicla As Boolean
        Dim fdecode As Short
        Dim ReplyTmp As Short

        ritorno = False
        FaseEsec()

        'se scanner zebrato montato non devo fare il test...
        If (fZebrato = 1) Then
            ritorno = True
            ret = 0
        Else

            If (fcardCol = 1 Or fSoftware = SW_BAR2D Or fSoftware = SW_TOP_BAR2D Or FForzaCartaColori = True) Then

                fBarc = New CtsControls.FBarcodes
                fAspetta = New CtsControls.FWait
                fAspetta.Label1.Text = "Attendere... decodifica PDF in corso...."
                rr = 1

                fdecode = 1
                cicli = 1

                If fdecode = 1 Then
                    fcicla = True
                    While (fcicla)

                        idmsg = 269
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("Inserire carta  DOCUTEST --083 dall' ingresso lineare", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                        End If

                        If (dres <> DialogResult.OK) Then
                            ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                            fcicla = False
                            fstop = True
                        Else
                            DisposeImg()
                            'TCollaudo.SelectedTab = TMessaggi
                            StrMessaggio = "Test Carta Colori"
                            'TMessaggi.Refresh()
                            ls150.NomeFileImmagine = zzz + "\Dati\cardImageC" + IndImgF.ToString("0000") + ".bmp"
                            ls150.NomeFileImmagineRetro = zzz + "\Dati\cardImageCR" + IndImgR.ToString("0000") + ".bmp"

                            If (FForzaCartaColori = True) Then
                                ' forza il funzionamento a colori della card
                                ReplyTmp = ls150.SetDiagnostic(3)
                                writeDebug("ls150.SetDiagnostic = " & ReplyTmp.ToString(), "TestCartaColori")
                            End If
                            fAspetta.Show()
                            ret = ls150.LeggiImmCard2(System.Convert.ToSByte(Ls150Class.Costanti.S_SIDE_ALL_IMAGE), Ls150Class.Costanti.S_SCAN_MODE_COLOR_300, fdecode)
                            fAspetta.Hide()
                            Select Case ret
                                Case Ls150Class.Costanti.S_LS150_PAPER_JAM, + _
                                Ls150Class.Costanti.S_LS150_JAM_AT_MICR_PHOTO, + _
                                Ls150Class.Costanti.S_LS150_JAM_AT_SCANNERPHOTO, + _
                                Ls150Class.Costanti.S_LS150_JAM_DOC_TOOLONG
                                    idmsg = 111
                                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                        dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                                    Else
                                        dres = MessageboxMy("ATTENZIONE CARTA BLOCCATA LIBERARE IL PERCORSO", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                    End If

                                    If (dres <> DialogResult.OK) Then
                                        ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                                        fstop = True
                                    Else
                                        ls150.Reset(Ls150Class.Costanti.S_RESET_FREE_PATH)
                                    End If
                                    fcicla = False
                                Case Ls150Class.Costanti.S_LS150_FEEDEREMPTY
                                    ' MessageboxMy("Inserire carta  DOCUTEST - 043 da ingresso lineare", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                                    'Case -1
                                    '    idmsg = 112
                                    '    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                    '        dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                                    '    Else
                                    '        dres = MessageboxMy("DOCUTEST --043 Inserito in modo non corretto", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                    '    End If

                                    '    If (dres <> DialogResult.OK) Then
                                    '        ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                                    '        fstop = True
                                    '    Else
                                    '    End If
                                    fstop = True
                                    fcicla = False

                                Case Ls150Class.Costanti.S_LS150_OKAY

                                    rr = ret
                                    ret = Ls150Class.Costanti.S_LS150_OKAY

                                    fBarc.Label1.Text = ls150.PDFLetto
                                    fBarc.Label2.Text = ls150.BarcodeLetto
                                    fBarc.Button1.Enabled = True
                                    fBarc.ShowDialog()

                                    DisposeImg()
                                    'FileZoomCorrente = zzz + "\Dati\cardImageC" + IndImgF.ToString("0000") + ".bmp"
                                    PFronte.Image = System.Drawing.Image.FromFile(zzz + "\Dati\cardImageC" + IndImgF.ToString("0000") + ".bmp")
                                    PRetro.Image = System.Drawing.Image.FromFile(zzz + "\Dati\cardImageCR" + IndImgR.ToString("0000") + ".bmp")
                                    Pimage.Image = PFronte.Image
                                    PImageBack.Image = PRetro.Image
                                    IndImgF += 1
                                    IndImgR += 1


                                    '    MessageBox.Show(ls150.PDFLetto, "COdeline PDF Letta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                                    'idmsg = 268

                                    'If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                    '    dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                                    'Else
                                    '    dres = MessageboxMy("ATTENZIONE LE IMMAGINI SONO A COLORI ? ", "Attenzione", MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing)
                                    'End If
                                    If dres = DialogResult.Cancel Then
                                        ret = -11111
                                    End If
                                Case -6001
                                    fBarc.Close()
                                    MessageboxMy("Decodifica PDF NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                    fcicla = False

                                Case -6002
                                    fBarc.Close()
                                    MessageboxMy("Decodifica PDF (lungh F/R diff) NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                    fcicla = False
                                Case -6003
                                    fBarc.Close()
                                    MessageboxMy("Decodifica CODE128 NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                    fcicla = False
                                Case -6604
                                    fBarc.Close()
                                    MessageboxMy("Decodifica CODE128 (lungh F/R diff) NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                    fcicla = False
                                Case Else
                                    fBarc.Close()
                                    MessageboxMy("Decodifica Carte _NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                    fcicla = False

                            End Select
                        End If
                        If cicli = ncili Then
                            fcicla = False
                        End If
                        cicli += 1
                    End While
                Else
                    ' non ho fatto nulla, e setto tutto a ok

                    ret = Ls150Class.Costanti.S_LS150_OKAY
                End If

                fAspetta.Close()
            Else
                ritorno = True
                ret = 0
            End If
        End If

        'If (FForzaCartaColori = True) Then
        '    ReplyTmp = ls150.SetDiagnostic(0)
        '    writeDebug("end-ls150.SetDiagnostic = " & ReplyTmp.ToString(), "TestCartaColori")
        'End If

        FileTraccia.UltimoRitornoFunzione = ret
        If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
            ritorno = True
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante

        Return ritorno
    End Function

    Public Function TestCartaN() As Integer

        Dim ritorno As Short
        Dim ret As Short
        Dim rr As Short
        Dim fstop As Short
        Dim penna As New Pen(System.Drawing.Color.Red)
        Dim cicli As Short
        Dim font1 As New Font("Time New Roman", 8, FontStyle.Regular, GraphicsUnit.Pixel)
        Dim fBarc As CtsControls.FBarcodes
        Dim fAspetta As CtsControls.FWait
        Dim fcicla As Boolean
        Dim fdecode As Short
        Dim ReplyTmp As Short
        Dim ncili As Short
        Dim retZoom As DialogResult
        Dim a As Short
        Dim ret_not_used As Short

        ritorno = False
        ncili = 1
        FaseEsec()

        idmsg = 269
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            immamsg = My.Resources.testcarta
            dres = MessageboxMy(Messaggio, Messaggio, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, immamsg)
        Else
            immamsg = My.Resources.testcarta
            dres = MessageboxMy("Inserire carta  DOCUTEST --083 dall' ingresso lineare", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, immamsg)
        End If
        DisposeImg()

        'If (fcardCol = 1 Or fSoftware = SW_BAR2D Or fSoftware = SW_TOP_BAR2D Or FForzaCartaColori = True) Then
        If (fcardCol = 1) Then

            fBarc = New CtsControls.FBarcodes
            fAspetta = New CtsControls.FWait
            fAspetta.Label1.Text = "Attendere... decodifica PDF in corso...."
            rr = 1

            fdecode = 1
            cicli = 1

            If fdecode = 1 Then
                fcicla = True
                While (fcicla)


                    If (dres <> DialogResult.OK) Then
                        ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                        fcicla = False
                        fstop = True
                    Else
                        DisposeImg()
                        StrMessaggio = "Test Carta Colori"

                        ls150.NomeFileImmagine = zzz + "\Dati\cardImageC" + IndImgF.ToString("0000") + ".bmp"
                        ls150.NomeFileImmagineRetro = zzz + "\Dati\cardImageCR" + IndImgR.ToString("0000") + ".bmp"

                        If (FForzaCartaColori = True) Then
                            ' forza il funzionamento a colori della card
                            ReplyTmp = ls150.SetDiagnostic(3)
                            writeDebug("ls150.SetDiagnostic = " & ReplyTmp.ToString(), "TestCartaColori")
                        End If
                        fAspetta.Show()
                        ret = ls150.LeggiImmCard2(System.Convert.ToSByte(Ls150Class.Costanti.S_SIDE_ALL_IMAGE), Ls150Class.Costanti.S_SCAN_MODE_COLOR_300, fdecode)
                        fAspetta.Hide()
                        Select Case ret
                            Case Ls150Class.Costanti.S_LS150_PAPER_JAM, + _
                            Ls150Class.Costanti.S_LS150_JAM_AT_MICR_PHOTO, + _
                            Ls150Class.Costanti.S_LS150_JAM_AT_SCANNERPHOTO, + _
                            Ls150Class.Costanti.S_LS150_JAM_DOC_TOOLONG
                                idmsg = 111
                                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                    dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                                Else
                                    dres = MessageboxMy("ATTENZIONE CARTA BLOCCATA LIBERARE IL PERCORSO", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                End If

                                If (dres <> DialogResult.OK) Then
                                    ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                                    fstop = True
                                Else
                                    ls150.Reset(Ls150Class.Costanti.S_RESET_FREE_PATH)
                                End If
                                fcicla = False
                            Case Ls150Class.Costanti.S_LS150_FEEDEREMPTY
                                fstop = True
                                fcicla = False

                            Case Ls150Class.Costanti.S_LS150_OKAY

                                rr = ret
                                ret = Ls150Class.Costanti.S_LS150_OKAY

                                fBarc.Label1.Text = ls150.PDFLetto
                                fBarc.Label2.Text = ls150.BarcodeLetto
                                fBarc.Button1.Enabled = True
                                fBarc.ShowDialog()

                                DisposeImg()
                                'FileZoomCorrente = zzz + "\Dati\cardImageC" + IndImgF.ToString("0000") + ".bmp"
                                PFronte.Image = System.Drawing.Image.FromFile(zzz + "\Dati\cardImageC" + IndImgF.ToString("0000") + ".bmp")
                                PRetro.Image = System.Drawing.Image.FromFile(zzz + "\Dati\cardImageCR" + IndImgR.ToString("0000") + ".bmp")
                                Pimage.Image = PFronte.Image
                                PImageBack.Image = PRetro.Image
                                IndImgF += 1
                                IndImgR += 1
                                If dres = DialogResult.Cancel Then
                                    ret = -11111
                                End If
                            Case -6001
                                fBarc.Close()
                                MessageboxMy("Decodifica PDF NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                fcicla = False

                            Case -6002
                                fBarc.Close()
                                MessageboxMy("Decodifica PDF (lungh F/R diff) NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                fcicla = False
                            Case -6003
                                fBarc.Close()
                                MessageboxMy("Decodifica CODE128 NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                fcicla = False
                            Case -6604
                                fBarc.Close()
                                MessageboxMy("Decodifica CODE128 (lungh F/R diff) NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                fcicla = False
                            Case Else
                                fBarc.Close()
                                MessageboxMy("Decodifica Carte _NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                fcicla = False

                        End Select
                    End If
                    If cicli = ncili Then
                        fcicla = False
                    End If
                    cicli += 1
                End While
            Else
                ' non ho fatto nulla, e setto tutto a ok

                ret = Ls150Class.Costanti.S_LS150_OKAY
            End If

            fAspetta.Close()
        Else
            'ritorno = True
            'ret = 0
            StrMessaggio = "Test Carta Colori"

            fcicla = True
            IndImgF = 0
            IndImgR = 0
            While (fcicla)

                ls150.NomeFileImmagine = zzz + "\Dati\cardImageC" + IndImgF.ToString("0000") + ".bmp"
                ls150.NomeFileImmagineRetro = zzz + "\Dati\cardImageCR" + IndImgR.ToString("0000") + ".bmp"

                If (FMacchinaColori = True Or fMacchinaUv = True) Then
                    ret = ls150.LeggiImmCard2(System.Convert.ToSByte(Ls150Class.Costanti.S_SIDE_ALL_IMAGE), Ls150Class.Costanti.S_SCAN_MODE_COLOR_300, 0)
                Else
                    ret = ls150.LeggiImmCard2(System.Convert.ToSByte(Ls150Class.Costanti.S_SIDE_ALL_IMAGE), Ls150Class.Costanti.S_SCAN_MODE_256GR300, 0)
                End If
                Select Case ret
                    Case Ls150Class.Costanti.S_LS150_PAPER_JAM, + _
                    Ls150Class.Costanti.S_LS150_JAM_AT_MICR_PHOTO, + _
                    Ls150Class.Costanti.S_LS150_JAM_AT_SCANNERPHOTO, + _
                    Ls150Class.Costanti.S_LS150_JAM_DOC_TOOLONG
                        idmsg = 111
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("ATTENZIONE CARTA BLOCCATA LIBERARE IL PERCORSO", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                        End If

                        If (dres <> DialogResult.OK) Then
                            ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                            fstop = True
                        Else
                            ls150.Reset(Ls150Class.Costanti.S_RESET_FREE_PATH)
                        End If
                        fcicla = False
                    Case Ls150Class.Costanti.S_LS150_FEEDEREMPTY
                        fstop = True
                        fcicla = False

                    Case Ls150Class.Costanti.S_LS150_OKAY

                        ret = Ls150Class.Costanti.S_LS150_OKAY
                        IndImgF += 1
                        IndImgR += 1
                        fcicla = False

                        'If (System.Convert.ToInt16(side) = Ls150Class.Costanti.S_SIDE_FRONT_IMAGE) Then
                        'BVisualZoom = False
                        DirZoom = Application.StartupPath

                        a = DirZoom.LastIndexOf("\")
                        DirZoom = DirZoom.Substring(0, a)
                        DirZoom += "\Dati\"

                        FileZoomCorrente = ls150.NomeFileImmagine


                        ' Zoom Immagine 
                        Try
                            retZoom = ShowZoom(DirZoom, FileZoomCorrente, False)
                        Catch ex As Exception
                            MessageBox.Show(ex.ToString())
                        End Try

                        If (retZoom = Windows.Forms.DialogResult.OK) Then
                            'BVisualZoom = False
                            DirZoom = Application.StartupPath

                            a = DirZoom.LastIndexOf("\")
                            DirZoom = DirZoom.Substring(0, a)
                            DirZoom += "\Dati\"
                            FileZoomCorrente = ls150.NomeFileImmagineRetro
                            ' Zoom Immagine 
                            Try
                                retZoom = ShowZoom(DirZoom, FileZoomCorrente, False)
                            Catch ex As Exception
                                MessageBox.Show(ex.ToString())
                            End Try

                            If (retZoom = Windows.Forms.DialogResult.OK) Then
                                ritorno = True
                            Else
                                ritorno = False
                                ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                                ret_not_used = TestScannersPeriferica()
                            End If
                        Else
                            ritorno = False
                            ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                            ret_not_used = TestScannersPeriferica()
                        End If
                End Select
            End While

        End If

        FileTraccia.UltimoRitornoFunzione = ret
        If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
            ritorno = True
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante

        Return ritorno
    End Function

    'Public Function TestAspetta(ByVal Acceso As Boolean) As Short
    '    'Dim fff As New FOnOff
    '    Dim ret As Short
    '    'If (Acceso = True) Then
    '    '    fff.LTezt.Text = "Accendere la periferica"
    '    'Else
    '    '    fff.LTezt.Text = "Spegnere la periferica"
    '    'End If
    '    'Application.DoEvents()
    '    'fff.Show()

    '    'If (Acceso = True) Then
    '    '    fff.Refresh()
    '    '    ret = TestON()
    '    'Else
    '    '    fff.Refresh()
    '    '    ret = TestOff()
    '    'End If

    '    'If ret = 1 Then
    '    '    ret = True
    '    '    fff.Close()
    '    'Else
    '    '    ret = False
    '    '    fff.Close()
    '    'End If
    '    Return ret
    'End Function

    Public Function TestOff() As Short
        Dim ritorno As Short
        Dim ret As Short
        ret = 0
        ritorno = False
        FaseEsec()

        While (ret >= 0)
            ret = ls150.Identificativo()
        End While
        If ret = Ls150Class.Costanti.S_LS150_PERIFNOTFOUND Then
            ritorno = True
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        Return ritorno
    End Function

    Public Function TestON() As Short
        Dim ritorno As Short
        Dim ret As Short
        ret = 1
        ritorno = False
        FaseEsec()

        While (ret <> 0)
            ret = ls150.Identificativo()
        End While
        If ret = 0 Then
            ritorno = True
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        Return ritorno
    End Function

    Public Function TestBadge(ByVal Tr As Short) As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim strBadge As String
        Dim strBadgeNew As String
        strBadge = "tCTS ELECTRONICS SPA BADGE CAMPIONE 11111111111111111111111111111111111111111t1111000022223333444455556666777888999t11111111111000000000002222222222233333333333444444444455555555556666666666777777777788888888889999999999"
        strBadgeNew = "tARCA BADGE CAMPIONE 11111111111111111111111111111111111111111111111111111111t1111000022223333444455556666777888999t11111111111000000000002222222222233333333333444444444455555555556666666666777777777788888888889999999999"
        ritorno = False
        FaseEsec()



        idmsg = 114
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Strisciare Badge DOCUTEST --007", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(5))
        End If

        If (dres <> DialogResult.OK) Then
            ret = Ls150Class.Costanti.S_LS150_USER_ABORT
        Else
            ret = ls150.LeggiBadge(Tr)
            If ret = Ls150Class.Costanti.S_LS150_OKAY Then
                'LBadge.Text = ls150.BadgeLetto

                If (ls150.BadgeLetto = strBadge Or ls150.BadgeLetto = strBadgeNew) Then
                    idmsg = 115
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        dres = MessageboxMy2(ls150.BadgeLetto + vbCrLf + Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy(ls150.BadgeLetto + vbCrLf + " Le tracce del badge sono corrette", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                    End If
                    ritorno = True
                Else
                    idmsg = 186
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        dres = MessageboxMy2(ls150.BadgeLetto + vbCrLf + Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy(ls150.BadgeLetto + vbCrLf + "Tracce del badge lette non correttamente", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                    End If
                    ritorno = False
                    ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                End If

            Else
                ritorno = False
            End If
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestLunghezzaImmagine() As Integer
        Dim ret As Short
        Dim ritorno As Short
        Dim contatore As Integer = 1

        ritorno = False
        'BVisualZoom = False
        FaseEsec()
        Do
            'idmsg = 116
            idmsg = 311 'Inserire DOCT-R-148 Per calibrazione lunghezza immagine, sino all' esito
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                'dres = MessageboxMy("Inserire DOCUTEST --042 Per calibrazione lunghezza immagine, sino all' esito", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                dres = MessageboxMy("Inserire DOCT-R-148 Per calibrazione lunghezza immagine, sino all' esito", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            End If

            'DAVIDE

            If (dres = DialogResult.OK) Then
                ret = ls150.CalibraImmagineNew()
                'ret = ls150.CalibraImmagine()
            Else
                contatore = 15
                ret = -1000
            End If
            contatore = contatore + 1
        Loop While (ret <> 0 And contatore < 10)
        If ret = 0 Then
            FileTraccia.UltimoRitornoFunzione = ret
            ret = True
        End If
        If ret = -1000 Then
            FileTraccia.UltimoRitornoFunzione = Ls150Class.Costanti.S_LS150_USER_ABORT
            ret = False
        End If

        If ret = True Then
            ritorno = True
        Else

            StrCommento += "Calibrazione non a buon fine: " + ret.ToString + " "
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestFotoDP() As Integer
        Dim ritorno As Short
        Dim ret As Short
        '  Dim R As DialogResult
        ritorno = False
        ls150.ReturnC.FunzioneChiamante = "TestFotoDP"

        FaseEsec()
        'idmsg = 95
        'If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
        '    dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        'Else
        '    dres = MessageboxMy("Inserire il  DOCUTEST --052 nel Feeder per la calibrazione del fotosensore Doppia Sfogliatura", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        'End If

        idmsg = 304
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire DOCT-R-146 nel Feeder per la calibrazione del fotosensore Doppia Sfogliatura", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        End If

        If dres <> DialogResult.OK Then
            fase.SetState = 0
            fase.IsBlinking = True
            fase.IsSelected = True
            ' errore calibrazione doppia sfogliatura
            ViewError(fase)
        Else
            fase.IsSelected = False

            ret = ls150.CalSfogliatura()

            FileTraccia.Note = "Fotosensore DP 1: " + Ls150Class.Periferica.PhotoValueDP(0).ToString + vbCrLf
            FileTraccia.Note += "Fotosensore DP 2: " + Ls150Class.Periferica.PhotoValueDP(1).ToString + vbCrLf
            FileTraccia.Note += "Fotosensore DP 3: " + Ls150Class.Periferica.PhotoValueDP(2).ToString '+ vbCrLf

            'FileTraccia.Note += "Fotosensore TH 1: " + Ls150Class.Periferica.PhotoValueDP(3).ToString + vbCrLf
            'FileTraccia.Note += "Fotosensore TH 2: " + Ls150Class.Periferica.PhotoValueDP(4).ToString + vbCrLf
            'FileTraccia.Note += "Fotosensore TH 3: " + Ls150Class.Periferica.PhotoValueDP(5).ToString + vbCrLf
            'FileTraccia.Note += "Fotosensore TH 4: " + Ls150Class.Periferica.PhotoValueDP(6).ToString + vbCrLf
            'FileTraccia.Note += "Fotosensore TH 5: " + Ls150Class.Periferica.PhotoValueDP(7).ToString + vbCrLf
            'FileTraccia.Note += "Fotosensore TH 6: " + Ls150Class.Periferica.PhotoValueDP(8).ToString

            If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
                ritorno = True

            Else
                ' errore calibrazione doppia sfogliatura
                ViewError(fase)
            End If
        End If

        If ritorno = True Then
            FileTraccia.FotoDp = 1
            FaseOk()
        Else
            FaseKo()
        End If

        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestAssegnoSpesso() As Integer
        Dim ret As Short
        Dim ritorno As Integer
        ritorno = False
        FaseEsec()

        ls150.ReturnC.FunzioneChiamante = "TestAssegnoSpesso"

        'idmsg = 117
        'If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
        '    dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        'Else
        '    dres = MessageboxMy("Inserire Docutest--052 piegato doppio ", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        'End If

        idmsg = 303
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire DOCT-R-146 Piegato Doppio ", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        End If


        If (dres = DialogResult.OK) Then
            ret = ls150.Docutest04()
            If (ret = Ls150Class.Costanti.S_LS150_DOUBLE_LEAFING_ERROR) Then
                Sleep(2)
                ret = ls150.Reset(Ls150Class.Costanti.S_RESET_FREE_PATH)
                If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
                    ' spegnere e accendere la perfierica
                    ritorno = True
                Else
                    idmsg = 118
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy("Accendere e Spegnere la periferica", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                    End If


                    'TestAspetta(False)
                    'TestAspetta(True)
                    ret = Ls150Class.Costanti.S_LS150_OKAY
                    ritorno = True
                End If
            Else


            End If
        Else
            ritorno = False
            ret = Ls150Class.Costanti.S_LS150_USER_ABORT
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestSmagnetizzato(ByVal BCMC7 As Boolean) As Integer
        Dim ritorno As Integer
        Dim ret As Integer
        Dim str, str1 As String
        Dim chcd As New CheckCodeline
        ritorno = False
        FaseEsec()
        If (BCMC7 = True) Then
            LettureCMC7 = 1
            LettureE13B = 0
            TipoComtrolloLetture = CheckCodeline.CONTROLLO_SMAG_CMC7
            str = "Smagnetizzare un DOCUTEST CMC7 Smagnetizzato 49"
            idmsg = 119

        Else
            LettureE13B = 1
            LettureCMC7 = 0
            TipoComtrolloLetture = CheckCodeline.CONTROLLO_SMAG_E13B
            str = "Smagnetizzare un DOCUTEST E13B Smagnetizzato 46"
            idmsg = 121

        End If

        TipoComtrolloLetture = 0

        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy(str, "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Information, Nothing)
        End If


        If (dres = DialogResult.OK) Then
            If BCMC7 = True Then
                idmsg = 120
                str1 = "Inserire un DOCUTEST CMC7 Smagnetizzato 49"
            Else
                idmsg = 122
                str1 = "Inserire un DOCUTEST E13B Smagnetizzato 46"
            End If

            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy(str1, "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Information, Nothing)
            End If


            If (dres = DialogResult.OK) Then
                ret = ls150.LeggiCodeline(Ls150Class.Costanti.S_SORTER_BAY1)
                If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then

                    chcd.DaLeggereCMC7 = LettureCMC7
                    chcd.DaLeggereE13B = LettureE13B
                    chcd.Tipo_controllo = TipoComtrolloLetture
                    If chcd.CheckCodeline(ls150.CodelineLetta) = 0 Then
                        'conteggio e vedo se ho finito
                        If chcd.Conteggiacodeline() = 1 Then
                            ret = Ls150Class.Costanti.S_LS150_OKAY
                        Else
                            ret = Ls150Class.Costanti.S_LS150_CODELINE_ERROR
                            idmsg = 123
                            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                            Else
                                dres = MessageboxMy("Controllare la presenza del magnete", "Errore !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                            End If


                        End If
                    Else
                        ret = Ls150Class.Costanti.S_LS150_CODELINE_ERROR
                        idmsg = 123
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("Controllare la presenza del magnete", "Errore !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                        End If

                    End If
                Else

                    ViewError(fase)
                End If
            Else
                ret = Ls150Class.Costanti.S_LS150_USER_ABORT
            End If
        Else
            ret = Ls150Class.Costanti.S_LS150_USER_ABORT
        End If
        FileTraccia.UltimoRitornoFunzione = ret

        If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
            ritorno = True
        Else
            ritorno = False
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function LeggiCodelines(ByVal cmc7 As Short, ByVal e13b As Short) As Integer
        Dim ret As Short

        Dim fstop As Boolean = False
        Dim ferror As Boolean = False
        Dim ii As Integer
        ii = 1
        chcd.Reset()
        chcd.VisualizzaFinestra(True)
        'chcd.F.Parent = Me

        ret = 0
        TipoComtrolloLetture = 2

        '      MessageboxMy("Inserire Documento lettura ", "Attenzione !! ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        While (fstop = False)
            ls150.NomeFileImmagine = zzz + "\Dati\CodeFImage" + IndImgF.ToString("0000") + ".bmp"
            ls150.NomeFileImmagineRetro = zzz + "\Dati\CodeRImage" + IndImgR.ToString("0000") + ".bmp"
            IndImgF += 1
            IndImgR += 1

            ret = ls150.LeggiCodelineImmagini(Ls150Class.Costanti.S_SORTER_BAY1)
            If (ret = Ls150Class.Costanti.S_LS150_DOUBLE_LEAFING_ERROR) Then
                idmsg = 125
                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                    dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                Else
                    dres = MessageboxMy("Errore double leafing...Continuare?", "Errore !!!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing)
                End If


                If (dres = DialogResult.Yes) Then
                    ret = ls150.Reset(Ls150Class.Costanti.S_RESET_FREE_PATH)
                    ret = Ls150Class.Costanti.S_LS150_OKAY
                Else
                    fstop = True
                    ferror = True
                    ViewError(fase)
                End If

            End If
            If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
                If (Not (ls150.CodelineLetta Is Nothing)) Then
                    'LICodeline.Items.Add(ls150.CodelineLetta)

                    Try
                        DisposeImg()
                        Pimage.Image = System.Drawing.Image.FromFile(ls150.NomeFileImmagine)
                        PImageBack.Image = System.Drawing.Image.FromFile(ls150.NomeFileImmagineRetro)

                    Catch ex As Exception
                        ' DEBUG
                        ' MessageBox.Show(ex.Message + " " + ex.Source, "errore")
                    End Try

                    LettureCMC7 = cmc7
                    LettureE13B = e13b
                    chcd.DaLeggereCMC7 = LettureCMC7
                    chcd.DaLeggereE13B = LettureE13B
                    chcd.Tipo_controllo = TipoComtrolloLetture

                    If chcd.CheckCodeline(ls150.CodelineLetta) = 0 Then
                        'conteggio e vedo se ho finito

                        If chcd.Conteggiacodeline() = 1 Then

                            'Me.Refresh()
                            chcd.F.Refresh()
                            'chcd.p.Show()
                            ' ho finito di contare senza errori
                            fstop = True
                            ret = 1
                        Else
                            ' non ho finito di contare
                            'Me.Refresh()
                            chcd.F.Refresh()
                        End If
                    Else
                        'erroe di lettura nella codeline
                        fstop = True
                        ferror = True
                        idmsg = 124
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy2(Messaggio + ls150.CodelineLetta, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("Codeline Errata: " + ls150.CodelineLetta, "Errore !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                        End If


                    End If
                End If
            Else
                ' processa do non a buon fine
                If (ls150.Reply = Ls150Class.Costanti.S_LS150_FEEDEREMPTY) Then
                    ' Me.Refresh()
                    Application.DoEvents()
                Else
                    fstop = True
                    ferror = True
                    ViewError(fase)
                End If

            End If
        End While
        chcd.F.Refresh()
        chcd.VisualizzaFinestra(False)

        Return ret
    End Function

    Public Function LeggiCodelinesAuto(ByVal cmc7 As Short, ByVal e13b As Short, ByVal Tipo_controllo As Short) As Integer
        Dim ret As Short

        Dim fstop As Boolean = False
        Dim ferror As Boolean = False
        Dim ii As Integer
        Dim ContaSorterFull As Integer
        Dim ScanMode As Integer
        Dim chcd As New CheckCodeline
        ContaSorterFull = 0
        ii = 1
        chcd.Reset()
        chcd.VisualizzaFinestra(True, Tipo_controllo)

        If (Tipo_controllo = CheckCodeline.CONTROLLO_LS150_C_TIMBRO) Or (Tipo_controllo = CheckCodeline.CONTROLLO_LS150_C_NO_TIMBRO) Then
            ScanMode = Ls150Class.Costanti.S_SCAN_MODE_COLOR_300
            Tipo_controllo = Tipo_controllo - 10
        Else
            ' ScanMode = 5 'SCAN_MODE_256GR200
            ScanMode = 21 ' SCAN_MODE_256GR300
        End If
        chcd.Tipo_controllo = Tipo_controllo

        'chcd.F.Parent = Me
        DisposeImg()

        ret = Ls150Class.Costanti.S_LS150_OKAY

        ret = ls150.ViaDocumentiAuto(ScanMode)
        If ret = Ls150Class.Costanti.S_LS150_OKAY Then
            '      MessageboxMy("Inserire Documento lettura ", "Attenzione !! ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            While (fstop = False)
                ls150.NomeFileImmagine = zzz + "\Dati\CodeFImage" + IndImgF.ToString("0000") + ".bmp"
                ls150.NomeFileImmagineRetro = zzz + "\Dati\CodeRImage" + IndImgR.ToString("0000") + ".bmp"
                IndImgF += 1
                IndImgR += 1

                ret = ls150.LeggiCodelineImmaginiAuto(Ls150Class.Costanti.S_SORTER_BAY1)
                ' 35 è sorter full
                '  Me.Refresh()
                Application.DoEvents()
                Select Case ret
                    Case Ls150Class.Costanti.S_LS150_DOUBLE_LEAFING_ERROR
                        'ls150.Stopaaaaa()
                        'If (MessageboxMy("Errore double leafing...Continuare?", "Errore !!!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing) = DialogResult.Yes) Then

                        '    ret = Ls150Class.Costanti.S_LS150_OKAY
                        '    ret = ls150.ViaDocumentiAuto(ScanMode)
                        'Else
                        fstop = True
                        ferror = True
                        ViewError(fase)

                        ls150.Reset(Ls150Class.Costanti.S_RESET_FREE_PATH)
                        'End If
                        'Case 7
                        '    ls150.Stopaaaaa()
                        '    idmsg = 125
                        '    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        '        dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        '    Else
                        '        dres = MessageboxMy("Warning double leafing...Continuare?", "Errore !!!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing)
                        '    End If

                        '    If (dres = DialogResult.Yes) Then
                        '        ret = ls150.Reset(Ls150Class.Costanti.S_RESET_FREE_PATH)
                        '        ret = Ls150Class.Costanti.S_LS150_OKAY
                        '        ret = ls150.ViaDocumentiAuto(ScanMode)
                        '    Else
                        '        fstop = True
                        '        ferror = True
                        '        ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                        '        ViewError(fase)
                        '    End If
                    Case Ls150Class.Costanti.S_LS150_PAPER_JAM, + _
                           Ls150Class.Costanti.S_LS150_JAM_AT_MICR_PHOTO, + _
                           Ls150Class.Costanti.S_LS150_JAM_AT_SCANNERPHOTO, + _
                            Ls150Class.Costanti.S_LS150_JAM_DOC_TOOLONG
                        'ls150.Stopaaaaa()
                        fstop = True
                        ferror = True
                        ViewError(fase)
                        ls150.Reset(Ls150Class.Costanti.S_RESET_FREE_PATH)
                    Case Ls150Class.Costanti.S_LS150_FEEDEREMPTY, 10

                        ret = Ls150Class.Costanti.S_LS150_OKAY
                        ret = ls150.ViaDocumentiAuto(ScanMode)
                        '  Me.Refresh()
                        Application.DoEvents()
                        'Case 35  ' sorter bin full
                        '    ls150.Stopaaaaa()
                        '    '  MessageboxMy("Vuotare sorte", "erore", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                        '    ret = Ls150Class.Costanti.S_LS150_OKAY
                        '    ret = ls150.ViaDocumentiAuto(ScanMode)

                    Case Ls150Class.Costanti.S_LS150_OKAY, 35, 7 ' per okay non faccio nulla

                        If (Not (ls150.CodelineLetta Is Nothing)) Then
                            ' LICodeline.Items.Add(ls150.CodelineLetta)
                            If ret = 35 Then
                                ContaSorterFull += 1
                                If ContaSorterFull >= 2 Then
                                    ContaSorterFull = 0
                                    idmsg = 190
                                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                        dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                                    Else
                                        dres = MessageboxMy("Attenzione Bin Sorter Pieno !! Svuotare Documenti", "Errore !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                    End If
                                End If

                            End If


                            Try
                                DisposeImg()
                                Pimage.Image = System.Drawing.Image.FromFile(ls150.NomeFileImmagine)
                                PImageBack.Image = System.Drawing.Image.FromFile(ls150.NomeFileImmagineRetro)
                                Me.formpadre.Refresh()
                            Catch ex As Exception
                                ' DEBUG
                                MessageBox.Show(ex.Message + " " + ex.Source + ret.ToString, "errore")
                            End Try
                            '  Me.Refresh()
                            Application.DoEvents()
                            LettureCMC7 = cmc7
                            LettureE13B = e13b
                            chcd.DaLeggereCMC7 = LettureCMC7
                            chcd.DaLeggereE13B = LettureE13B


                            If chcd.CheckCodeline(ls150.CodelineLetta) = 0 Then
                                'conteggio e vedo se ho finito
                                If chcd.Conteggiacodeline() = 1 Then

                                    'Me.Refresh()
                                    chcd.F.Refresh()
                                    'chcd.p.Show()
                                    ' ho finito di contare senza errori
                                    ls150.Stopaaaaa()

                                    fstop = True
                                    ferror = False
                                    ret = 0

                                Else
                                    ' non ho finito di contare
                                    'Me.Refresh()

                                    chcd.F.Refresh()
                                End If
                            Else
                                'erroe di lettura nella codeline
                                ls150.Stopaaaaa()
                                fstop = True
                                ferror = True
                                ret = Ls150Class.Costanti.S_LS150_CODELINE_ERROR
                                idmsg = 124
                                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                    dres = MessageboxMy2(Messaggio + ls150.CodelineLetta, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                                Else
                                    dres = MessageboxMy("Codeline Errata: " + ls150.CodelineLetta, "Errore !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                End If



                                'ret = ls150.ViaDocumentiAuto(ScanMode)
                            End If


                        End If

                    Case Else
                        fstop = True
                        ferror = True
                        ViewError(fase)
                End Select


                ' processa do non a buon fine
                'Else ' fi reply <> 0
            End While
        End If

        chcd.F.Refresh()
        chcd.VisualizzaFinestra(False)
        'ls150
        '  ls150.Stopaaaaa()
        Return ret
    End Function

    Public Function TestQualitaImmagine(ByVal side As System.Byte, Optional ByVal ScanMode As Short = 0) As Integer
        Dim ritorno As Integer
        Dim retZoom As DialogResult
        Dim ret As Integer
        Dim str As String
        Dim ReplyTmp As Short
        Dim localDate As String
        Dim hour As String

        Fqualit = New CtsControls.FqualImg

        ritorno = False
        'fquality = True

        localDate = Now.ToString("yyyyMMdd")
        hour = Now.ToString("hhmmss")

        FaseEsec()

        If (fMacchinaUv = False And ScanMode = Ls150Class.Costanti.S_SCAN_MODE_256GR200_AND_UV) Then
            'ho una macchina che non e' UV che non deve fare questa fase 
            ritorno = True
            ret = Ls150Class.Costanti.S_LS150_OKAY
            GoTo uscita
        End If

        If (FMacchinaColori = False And ScanMode = Ls150Class.Costanti.S_SCAN_MODE_COLOR_200) Then
            'ho una macchina che non e' UV che non deve fare questa fase 
            ritorno = True
            ret = Ls150Class.Costanti.S_LS150_OKAY
            GoTo uscita
        End If

        If (fCard = False) Then
            'idmsg = 126
            idmsg = 309
        Else
            idmsg = 271
        End If

        If (SerialNumberDaStampare = Nothing) Then
            SerialNumberDaStampare = "PROGETTO"
        End If

        If (SerialNumberPiastra = Nothing) Then
            SerialNumberPiastra = "PROGETTO_PIASTRA"
        End If



        If (System.Convert.ToInt16(side) = Ls150Class.Costanti.S_SIDE_FRONT_IMAGE) Then
            str = " FRONTE"
        Else
            If (System.Convert.ToInt16(side) = Ls150Class.Costanti.S_SIDE_BACK_IMAGE) Then
                str = " RETRO"
            Else
                str = " FRONTE/RETRO"
            End If
        End If


        ls150.NomeFileImmagine = zzz + "\Images_Scanner\FrontImage" + SerialNumberDaStampare.ToString + "_" + SerialNumberPiastra.ToString + "_" + IndImgF.ToString("0000") + "_" + localDate.ToString + "_" + hour.ToString & ".bmp"
        ls150.NomeFileImmagineUV = zzz + "\Images_Scanner\FrontImageUV" + SerialNumberDaStampare.ToString + "_" + SerialNumberPiastra.ToString + "_" + IndImgF.ToString("0000") + "_" + localDate.ToString + "_" + hour.ToString & ".bmp"
        ls150.NomeFileImmagineRetro = zzz + "\Images_Scanner\BackImage" + SerialNumberDaStampare.ToString + "_" + SerialNumberPiastra.ToString + "_" + IndImgR.ToString("0000") + "_" + localDate.ToString + "_" + hour.ToString & ".bmp"

        'UltimaImmagineFrontToSave = ls150.NomeFileImmagine
        'UltimaImmagineFrontUVToSave = ls150.NomeFileImmagineUV
        'UltimaImmagineBackToSave = ls150.NomeFileImmagineRetro

        Select Case ScanMode
            Case Ls150Class.Costanti.S_SCAN_MODE_COLOR_100, _
                Ls150Class.Costanti.S_SCAN_MODE_COLOR_200, _
                Ls150Class.Costanti.S_SCAN_MODE_COLOR_AND_RED_100, _
                Ls150Class.Costanti.S_SCAN_MODE_COLOR_AND_RED_200, _
                Ls150Class.Costanti.S_SCAN_MODE_COLOR_300

                ScanMode = Ls150Class.Costanti.S_SCAN_MODE_COLOR_300
                str += " Colore"

                If (FMacchinaColori = False) Then
                    FaseOk()
                    FileTraccia.UltimoRitornoFunzione = 0 'replycode
                    FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
                    Return True


                End If



            Case Ls150Class.Costanti.S_SCAN_MODE_256GR100_AND_UV, _
                Ls150Class.Costanti.S_SCAN_MODE_256GR200_AND_UV, _
                Ls150Class.Costanti.S_SCAN_MODE_256GR300_AND_UV

                ScanMode = Ls150Class.Costanti.S_SCAN_MODE_256GR200_AND_UV
                str += " UV"
                idmsg = 273
            Case Else
                ScanMode = Ls150Class.Costanti.S_SCAN_MODE_256GR300


        End Select

        If (fCard = True) Then
            If FForzaCartaColori = False Then
                ' non devo eseguire questa fase !!!
                FaseOk()
                'BVisualZoom = False
                FileTraccia.UltimoRitornoFunzione = ret
                FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
                Return True
            Else
                If fCard = True Then
                    ' chiama la impostazione "volante " ed esegui!!!
                    'sulla macchina a grigio devo forzare la filmatura della carta a colori
                    ReplyTmp = ls150.SetDiagnostic(3)
                    writeDebug("ls150.SetDiagnostic(3)= " & ReplyTmp.ToString(), "TestQualitaImmagine")
                End If
            End If
        End If

        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio + str, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire un documento per verificare l'immagine " + str, "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Question, VetImg(7))
        End If

        If dres = DialogResult.OK Then




            If fCard = True Then
                ret = ls150.LeggiImmCardQual(side, ScanMode)
            Else
                ret = ls150.LeggiImmagini(side, ScanMode, NO_CLEAR_BLACK)
            End If

            If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
                If (System.Convert.ToInt16(side) = Ls150Class.Costanti.S_SIDE_FRONT_IMAGE) Then
                    DisposeImg()
                    Try
                        If (ScanMode = Ls150Class.Costanti.S_SCAN_MODE_256GR200_AND_UV) Then
                            Pimage.Image = System.Drawing.Image.FromFile(ls150.NomeFileImmagineUV)
                        Else
                            Pimage.Image = System.Drawing.Image.FromFile(ls150.NomeFileImmagine)
                        End If

                    Catch ex As Exception
                        MessageBox.Show(ex.ToString())
                    End Try


                    IndImgF += 1
                    If fCard = False Then
                        '' TOLTO URGENTEMENTE IL 3-6-13
                        ' ftestSfondo = TestSfondo(Pimage.Image, 32, RigheContigueFronte, RigheTotaliFronte, indicesfondo)
                        'stringaApp = "Fronte --> Righe contigue: " + RigheContigueFronte.ToString("000") + " Righe Totali: " + RigheTotaliFronte.ToString("000")
                    End If
                Else
                    If (System.Convert.ToInt16(side) = Ls150Class.Costanti.S_SIDE_BACK_IMAGE) Then
                        DisposeImg()
                        Try
                            PImageBack.Image = System.Drawing.Image.FromFile(ls150.NomeFileImmagineRetro)
                        Catch ex As Exception
                            MessageBox.Show(ex.ToString())
                        End Try

                        IndImgR += 1
                        If fCard = False Then
                        End If
                    End If

                End If

                Application.DoEvents()
                Dim a As Short

                If (System.Convert.ToInt16(side) = Ls150Class.Costanti.S_SIDE_FRONT_IMAGE) Then
                    'BVisualZoom = True
                    DirZoom = Application.StartupPath

                    a = DirZoom.LastIndexOf("\")
                    DirZoom = DirZoom.Substring(0, a)
                    DirZoom += "\Images_Scanner\"
                    If (ScanMode = Ls150Class.Costanti.S_SCAN_MODE_256GR200_AND_UV) Then
                        FileZoomCorrente = ls150.NomeFileImmagineUV
                    Else
                        FileZoomCorrente = ls150.NomeFileImmagine
                    End If

                    ' Zoom Immagine 
                    Try
                        retZoom = ShowZoom(DirZoom, FileZoomCorrente)
                    Catch ex As Exception
                        MessageBox.Show(ex.ToString())
                    End Try
                End If



                If (System.Convert.ToInt16(side) = Ls150Class.Costanti.S_SIDE_BACK_IMAGE) Then
                    'If retZoom = DialogResult.OK Then
                    'BVisualZoom = True
                    DirZoom = Application.StartupPath

                    a = DirZoom.LastIndexOf("\")
                    DirZoom = DirZoom.Substring(0, a)
                    DirZoom += "\Images_Scanner\"
                    FileZoomCorrente = ls150.NomeFileImmagineRetro
                    ' Zoom Immagine 
                    Try
                        retZoom = ShowZoom(DirZoom, FileZoomCorrente)
                    Catch ex As Exception
                        MessageBox.Show(ex.ToString())
                    End Try

                    'End If
                End If

                'And (ls150.SLsName <> "LS150_UV") tolto per prove GARDI
                If (fCard = False) Then
                End If

                If retZoom = DialogResult.OK Then
                    ritorno = True
                Else
                    Fqualit.ShowDialog()

                    Select Case Fqualit.ret
                        Case 0
                            ritorno = False
                        Case 1
                            'ls150.SVersion = "LS150UV"
                            TestIdentificativo()
                            If ls150.SLsName.Contains("LS150_UV") Then
                                ' provvisorio per nuovi scanner UV
                                ' prima erano invertiti
                                'ritorno = TestScannerUV()
                                'If ritorno = True Then
                                ritorno = TestScannersPeriferica()
                                If ritorno = True Then
                                    If (fcardCol = 1) Then
                                        ScanMode = Ls150Class.Costanti.S_SCAN_MODE_256GR300
                                    End If
                                    ritorno = TestQualitaImmagine(70, ScanMode)
                                    If ritorno = True Then
                                        If (fcardCol = 1) Then
                                            ScanMode = Ls150Class.Costanti.S_SCAN_MODE_256GR300
                                        End If
                                        ritorno = TestQualitaImmagine(66, ScanMode)
                                    End If
                                End If
                                'End If
                            Else
                                ritorno = TestScannersPeriferica()

                                If ritorno = True Then
                                    If (fcardCol = 1) Then
                                        ScanMode = Ls150Class.Costanti.S_SCAN_MODE_256GR300
                                    End If
                                    ritorno = TestQualitaImmagine(70, ScanMode)
                                    If ritorno = True Then
                                        If (fcardCol = 1) Then
                                            ScanMode = Ls150Class.Costanti.S_SCAN_MODE_256GR300
                                        End If
                                        ritorno = TestQualitaImmagine(66, ScanMode)
                                        If ritorno = True Then
                                            'If fcardCol = 1 Then
                                            '    ritorno = TestQualitaImmagine(70, Ls150Class.Costanti.S_SCAN_MODE_COLOR_300, True)
                                            'End If
                                        End If
                                    End If
                                End If

                            End If

                        Case 2

                            'ritorno = TestQualitaImmagine(side, ScanMode, fCard)
                            ritorno = TestQualitaImmagine(side, ScanMode)
                    End Select
                End If
            Else ' Leggi immagini non a buon ine
                ViewError(fase)

                Select Case ret
                    Case Ls150Class.Costanti.S_LS150_JAM_AT_SCANNERPHOTO, + _
                        Ls150Class.Costanti.S_LS150_JAM_AT_MICR_PHOTO, + _
                        Ls150Class.Costanti.S_LS150_JAM_DOC_TOOLONG, + _
                        Ls150Class.Costanti.S_LS150_PAPER_JAM
                        ret = ls150.Reset(Ls150Class.Costanti.S_RESET_FREE_PATH)
                End Select
            End If
        Else
            ret = Ls150Class.Costanti.S_LS150_USER_ABORT
        End If

uscita:
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If



        'BVisualZoom = False
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function APPTestControlloFascioni(ByVal fTestFascioni As Integer, ByVal fControlloBackGround As Integer) As Integer
        Dim ret_funzione As Integer
        Dim length As Integer = 255
        Dim NrDocForCalib As Integer
        Dim NrDocVerify As Integer
        Dim NrProve As Integer
        Dim fcicla As Boolean

        NrProve = 0
        FaseEsec()


        If (SerialNumberDaStampare = Nothing) Then
            SerialNumberDaStampare = "PROGETTO"
        End If

        If (fTestFascioni = 1) Then
            'non serve testare il reply 
            ls150.TestControlloFascioniParamter(NrDocForCalib, NrDocVerify)
        End If


        'str = "Inserire DOCUTEST--035,nel verso indicato in foto,per verificare l'uniformità dei segmenti del CIS ed il background"

        fcicla = True
        While (fcicla)

            idmsg = 312
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                immamsg = My.Resources.LS150_FACIONI_UV
                dres = MessageboxMy(Messaggio, "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Question, immamsg)
            Else
                immamsg = My.Resources.LS150_FACIONI_UV
                dres = MessageboxMy("Inserire DOCT-R-148,nel verso indicato in foto,per verificare l'uniformità dei segmenti del CIS ed il background", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, immamsg)
            End If

            If dres = DialogResult.OK Then
                'ls150.NomeFileFascioni = Application.StartupPath + "\Images_Fascioni\" + SerialNumberDaStampare + NrProve.ToString("0000") + "FA" + ".bmp"
                ls150.NomeFileImmagine = Application.StartupPath + "\Images_Fascioni\" + SerialNumberDaStampare + NrProve.ToString("0000") + "F" + ".bmp"
                ls150.NomeFileImmagineRetro = Application.StartupPath + "\Images_Fascioni\" + SerialNumberDaStampare + NrProve.ToString("0000") + "B" + ".bmp"
                'ret = ls150.TestControlloFascioni(1, fZebrato, fTestFascioni)
                'fZebrato = 0
                ret_funzione = ls150.TestControlloFascioni(fControlloBackGround, fZebrato, fTestFascioni)
                'ret = Ls150Class.Costanti.S_LS150_OKAY
                If (ret_funzione = Ls150Class.Costanti.S_LS150_OKAY) Then
                    fcicla = False
                Else
                    ViewError(fase)
                    Select Case ret_funzione
                        Case Ls150Class.Costanti.S_LS150_JAM_AT_SCANNERPHOTO, + _
                            Ls150Class.Costanti.S_LS150_JAM_AT_MICR_PHOTO, + _
                            Ls150Class.Costanti.S_LS150_JAM_DOC_TOOLONG, + _
                            Ls150Class.Costanti.S_LS150_PAPER_JAM
                            ls150.Reset(Ls150Class.Costanti.S_RESET_FREE_PATH)
                            'test da rifare in ogni caso 
                        Case Ls150Class.Costanti.S_LS150_FRONT_BACKGROUND, + _
                             Ls150Class.Costanti.S_LS150_FRONT_BACKGROUND_1, + _
                             Ls150Class.Costanti.S_LS150_FRONT_BACKGROUND_2, + _
                             Ls150Class.Costanti.S_LS150_BACK_BACKGROUND, + _
                            Ls150Class.Costanti.S_LS150_BACK_BACKGROUND_1, + _
                             Ls150Class.Costanti.S_LS150_BACK_BACKGROUND_2
                            fcicla = False
                        Case Else
                            NrProve = NrProve + 1
                            If (NrProve > NrDocVerify) Then
                                fcicla = False
                            End If
                            'If (NrProve = NrDocForCalib) Then
                            'fcicla = False
                            'End If
                    End Select
                End If
            Else
                fcicla = False
                ret_funzione = Ls150Class.Costanti.S_LS150_USER_ABORT
            End If

        End While

        FileTraccia.UltimoRitornoFunzione = ret_funzione
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        'Return ritorno
        Return ret_funzione  'viene usata come funzione non come fase ...
    End Function

    Public Function TestSNPerStampa() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim fser As New CtsControls.FSerialnum
        ritorno = False
        FaseEsec()
        ls150.ReturnC.FunzioneChiamante = "TestSerialNumber"

        idmsg = 154
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            fser.Text = Messaggio
        Else
            fser.Text = "Inserire il Serial number della periferica da collaudare"
        End If


        If fser.ShowDialog() = DialogResult.OK Then
            If fser.StrSerialNumber.Length = LUNGHEZZA_SERIAL_NUMBER Then
                SerialNumberDaStampare = fser.StrSerialNumber

                If ret = Ls150Class.Costanti.S_LS150_OKAY Then
                    ritorno = True
                Else
                    ViewError(fase)
                End If
            Else
                ret = Ls150Class.Costanti.S_LS150_STRINGTRUNCATED
                ViewError(fase)
            End If


        Else
            ViewError(fase)
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestCinghia() As Integer

        Dim ritorno As Integer
        Dim ret As Short

        ritorno = False
        ritorno = TestCinghiaEX(118, 138)

        'ritorno = False
        'FaseEsec()

        'idmsg = 113
        'If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
        '    dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        'Else
        '    dres = MessageboxMy("Test Cinghia: Inserire il Docutest", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        'End If

        'If (dres = DialogResult.OK) Then
        '    ls150.ValMaxCinghia = 255
        '    ls150.ValMinCinghia = 0
        '    ret = ls150.TestCinghia()

        '    If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
        '        ritorno = True
        '    Else
        '        If (ret = -6000) Then
        '            MessageboxMy("Max: " + ls150.ValMaxCinghia.ToString + " Min: " + ls150.ValMinCinghia.ToString, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
        '            StrCommento += "Reply :" + ret.ToString + " "
        '            MessageboxMy("Pulire Cinghia", "Cts Electronics", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
        '            ls150.PuliziaCinghia()
        '            ls150.ValMaxCinghia = 255
        '            ls150.ValMinCinghia = 0
        '            idmsg = 113
        '            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
        '                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        '            Else
        '                dres = MessageboxMy("Test Cinghia: Inserire il Docutest", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        '            End If
        '            If (dres = DialogResult.OK) Then
        '                ret = ls150.TestCinghia()

        '                If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
        '                    ritorno = True
        '                Else
        '                    If (ret = -6000) Then
        '                        MessageboxMy("Max: " + ls150.ValMaxCinghia.ToString + " Min: " + ls150.ValMinCinghia.ToString, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
        '                        StrCommento += "Reply :" + ret.ToString + " "
        '                    Else

        '                        ls150.Reset(Ls150Class.Costanti.S_RESET_FREE_PATH)
        '                    End If

        '                End If
        '            End If

        '        Else

        '            ls150.Reset(Ls150Class.Costanti.S_RESET_FREE_PATH)
        '        End If


        '    End If

        '    FileTraccia.Note = "Valore minimo :" + ls150.ValMinCinghia.ToString + vbCrLf
        '    FileTraccia.Note += "Valore massimo :" + ls150.ValMaxCinghia.ToString



        'Else
        '    StrCommento += "Abortito Da Utente "
        '    ret = Ls150Class.Costanti.S_LS150_USER_ABORT
        'End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestCinghiaEX(ByVal ThNegativo As Short, ByVal ThPositivo As Short) As Integer

        Dim ritorno As Integer
        Dim ret As Short
        Dim valore As Integer

        ritorno = False
        FaseEsec()

        idmsg = 12

        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            StrMessaggio = Messaggio
        Else
            StrMessaggio = "Inserire nella Casella di Testo il Valore del Documento di calibrazione DOCUTEST -042:"
        End If


        If InputboxMy(StrMessaggio, "Attenzione", True, valore, 0, 0) = DialogResult.OK Then
            idmsg = 310
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                immamsg = My.Resources.LATO_PUNTINI
                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Test Cinghia: Inserire il DOCUTEST --042-04 lato pallini", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            End If

        End If


        If (dres = DialogResult.OK) Then
            ret = ls150.TestCinghiaEx(ThNegativo, ThPositivo, valore)

            'se voglio salvare dei dati sul DataBase ...
            FileTraccia.Note = "Valore minimo :" + ls150.ValMinCinghiaNew.ToString + vbCrLf
            FileTraccia.Note += "Valore massimo :" + ls150.ValMaxCinghiaNew.ToString + vbCrLf
            FileTraccia.Note += "NrPicchi :" + ls150.NrPicchi.ToString + vbCrLf
            FileTraccia.Note += "SignalValueRead :" + ls150.SignalValueRead.ToString

            If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
                ritorno = True
            Else
                If (ret = -6000) Then
                    MessageboxMy(FileTraccia.Note, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                Else
                    ViewError(fase)
                End If
                ritorno = False

                ls150.Reset(Ls150Class.Costanti.S_RESET_FREE_PATH)
            End If
        Else
            StrCommento += "Abortito Da Utente "
            ret = Ls150Class.Costanti.S_LS150_USER_ABORT
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestSetVelocita(ByVal Velo As Short) As Integer

        Dim ritorno As Integer
        Dim ret As Short

        ls150.ReturnC.FunzioneChiamante = "TestSetVelocita"

        'TCollaudo.SelectedTab = TMicr
        'TMicr.Enabled = True
        ritorno = False
        FaseEsec()

        ret = ls150.SetVelocita(Velo)

        If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
            ritorno = True
        Else
            StrCommento += "Reply :" + ret.ToString + " "
        End If


        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestFotosensori() As Integer
        Dim Reply As Integer
        Dim ritorno As Integer

        Dim write As System.IO.StreamWriter
        Dim stringa As String
        Dim ind As Integer

        Dim pathFile As String


        ritorno = True

        stringa = Application.StartupPath
        ind = stringa.LastIndexOf("\")
        stringa = stringa.Substring(0, ind)

        If IO.Directory.Exists(stringa + "trace") Then
            ' IO.Directory.Delete(zzz + "\Dati", True)
        Else
            IO.Directory.CreateDirectory("trace")
        End If

        pathFile = "\trace\fotosensori.cts"
        stringa = ""

        ' leggo i valori dei Cis
        Reply = ls150.LeggiValoriCIS()

        If (Reply = Ls150Class.Costanti.S_LS150_OKAY) Then

            If (IO.File.Exists(pathFile) = False) Then
                ' la prima volta scrivo anche l'intestazione del file 
                write = New System.IO.StreamWriter(pathFile, True)
                stringa = "Data Ora Foto1 Foto2 Foto3 Doppia1 Doppia2 Doppia3 Soglia1 Soglia2 Soglia3 Soglia4 Soglia5 Soglia6 "
                stringa += "Add_PWM_Blue_F Add_PWM_Blue_R Add_PWM_Green_F Add_PWM_Green_R Add_PWM_Red_F Add_PWM_Red_R"
                stringa += "Dato_Gain_F Dato_Gain_R Dato_Offset_F Dato_Offset_R"
                stringa += "Dato_PWM_Blue_F Dato_PWM_Blue_R Dato_PWM_Green_F Dato_PWM_Green_R Dato_PWM_Red_F Dato_PWM_Red_R"
                write.WriteLine(stringa)
            Else

                write = New System.IO.StreamWriter(pathFile, True)
            End If

            stringa = System.DateTime.Now.ToString
            stringa += " " + Ls150Class.Periferica.PhotoValue(0).ToString
            stringa += " " + Ls150Class.Periferica.PhotoValue(1).ToString
            stringa += " " + Ls150Class.Periferica.PhotoValue(2).ToString

            stringa += " " + Ls150Class.Periferica.PhotoValueDP(0).ToString
            stringa += " " + Ls150Class.Periferica.PhotoValueDP(1).ToString
            stringa += " " + Ls150Class.Periferica.PhotoValueDP(2).ToString
            ' soglie
            stringa += " " + Ls150Class.Periferica.PhotoValueDP(3).ToString
            stringa += " " + Ls150Class.Periferica.PhotoValueDP(4).ToString
            stringa += " " + Ls150Class.Periferica.PhotoValueDP(5).ToString
            stringa += " " + Ls150Class.Periferica.PhotoValueDP(6).ToString
            stringa += " " + Ls150Class.Periferica.PhotoValueDP(7).ToString
            stringa += " " + Ls150Class.Periferica.PhotoValueDP(8).ToString

            stringa += " " + ls150.strCis.Add_PWM_Blue_F.ToString
            stringa += " " + ls150.strCis.Add_PWM_Blue_R.ToString
            stringa += " " + ls150.strCis.Add_PWM_Green_F.ToString
            stringa += " " + ls150.strCis.Add_PWM_Green_R.ToString
            stringa += " " + ls150.strCis.Add_PWM_Red_F.ToString
            stringa += " " + ls150.strCis.Add_PWM_Red_R.ToString

            stringa += " " + ls150.strCis.Dato_Gain_F.ToString
            stringa += " " + ls150.strCis.Dato_Gain_R.ToString

            stringa += " " + ls150.strCis.Dato_Offset_F.ToString
            stringa += " " + ls150.strCis.Dato_Offset_R.ToString

            stringa += " " + ls150.strCis.Dato_PWM_Blue_F.ToString
            stringa += " " + ls150.strCis.Dato_PWM_Blue_R.ToString
            stringa += " " + ls150.strCis.Dato_PWM_Green_F.ToString
            stringa += " " + ls150.strCis.Dato_PWM_Green_R.ToString
            stringa += " " + ls150.strCis.Dato_PWM_Red_F.ToString
            stringa += " " + ls150.strCis.Dato_PWM_Red_R.ToString
            ' scrivo sul file di trace
            write.WriteLine(stringa)
            ' chiudo il file
            write.Close()
        Else
            ' la funzione non è andata a buon fine
            ritorno = False
        End If
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestHubUsb() As Integer
        Dim ret As Integer
        Dim ritorno As Integer

        Dim STR() As String = IO.Directory.GetLogicalDrives()


        Dim fileArray() As String
        Dim fFound As Boolean
        Dim riga As String = ""

        fFound = False
        ret = False
        ritorno = False
        FaseEsec()

        idmsg = 132
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire la chiavetta USB di test nella porta Hub di LS150", "Attenzione !!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        End If


        For Each s As String In STR
            If (s <> "A:\" And s <> "B:\") Then
                Try
                    fileArray = System.IO.Directory.GetFiles(s)
                    'Dim drives As [String]() = System.Environment.GetLogicalDrives()
                    Dim drives As [String]() = diskInfo.GetInformations(s)
2:                  If drives(0) = NOME_CHIAVETTA Then

                        For Each ss As String In fileArray
                            If ss = s + NOME_FILE_CHIAVETTA Then
                                Dim fstr As System.IO.StreamReader
                                fstr = New System.IO.StreamReader(ss)
                                riga = fstr.ReadLine()
                                If riga = NOME_STRINGA_CHIAVETTA Then
                                    fFound = True
                                End If
                            End If
                        Next
                    End If

                Catch ex As Exception

                End Try
            End If
        Next
        If fFound = True Then
            ret = True
            ritorno = True
            MessageboxMy(riga, "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        Else
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        Return ret
    End Function

    Public Function CheckRegistry() As Boolean
        Dim ret As Boolean
        ' verifico che nel registro siano disabilitate le funzioni 
        ' per l'auto play'
        ' se non c'è il valore corretto l' imposto e faccio shut down del PC

        Dim regKey As Microsoft.Win32.RegistryKey
        Dim ver As Decimal
        regKey = Microsoft.Win32.Registry.Users.OpenSubKey(".DEFAULT\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", True)
        'regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(".DEFAULT\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", True)
        'regKey.SetValue("AppName", "MyRegApp")
        ver = regKey.GetValue("NoDriveTypeAutoRun", 0.0)
        If ver <> 181 Then
            regKey.SetValue("NoDriveTypeAutoRun", 181)
            MessageboxMy("Trovato sistema non ottimizzato.....", "Riavvio di Windows...", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
            Shell("shutdown.exe -r")
            'Me.Close()
        End If
        regKey.Close()
        Return ret
    End Function

    Public Function TestSNPerStampaPiastra() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim fser As New CtsControls.FSerialnum
        ritorno = False
        FaseEsec()
        ls150.ReturnC.FunzioneChiamante = "TestSNPerStampaPiastra"

        'idmsg = 154
        'If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
        'fser.Text = Messaggio
        'Else
        fser.Text = "Inserire SerialNumber Piastra(16chr)"
        'End If


        If fser.ShowDialog() = DialogResult.OK Then
            If fser.StrSerialNumber.Length = LUNGHEZZA_SERIAL_NUMBER_PIASTRA Then
                SerialNumberDaStampare = fser.StrSerialNumber



                If ret = Ls150Class.Costanti.S_LS150_OKAY Then
                    ritorno = True
                Else
                    ViewError(fase)
                End If
            Else
                ret = Ls150Class.Costanti.S_LS150_STRINGTRUNCATED
                ViewError(fase)
            End If


        Else
            ViewError(fase)
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestSerialNumberPiastra() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim fser As New CtsControls.FSerialnum
        ritorno = False
        FaseEsec()

        idmsg = 131
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            fser.Text = Messaggio
        Else
            fser.Text = "Inserire SerialNumber Piastra(16 chr)"
        End If

        If fser.ShowDialog() = DialogResult.OK Then
            If fser.StrSerialNumber.Length = LUNGHEZZA_SERIAL_NUMBER_PIASTRA Then
                SerialNumberPiastra = fser.StrSerialNumber
                FileTraccia.VersioneDll = SerialNumberPiastra
                ritorno = True
            Else
                ViewError(fase)
            End If
        Else
            ViewError(fase)
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = "Test Serial number Piastra"
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function

    Public Function TestPalettaMotorizzata() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim Val As Short

        ritorno = False
        FaseEsec()
        If (ls150.SFeederVersion <> "") Then
            idmsg = 270
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Test Paletta motorizzata : Attendere Risultato....", "Attenzione !!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            End If


            ret = ls150.MotoreFeeder(0)
            Sleep(1)

            ls150.sensorFeeder()
            Val = ls150.Sensore
            Val += ls150.Sensore
            Val += ls150.Sensore
            Val += ls150.Sensore
            Val += ls150.Sensore
            Val += ls150.Sensore
            Val += ls150.Sensore
            Val += ls150.Sensore
            Val += ls150.Sensore
            Val += ls150.Sensore
            Val /= 10

            If (Val <> 0) Then
                ret = Val
                ritorno = False
            Else
                ritorno = True
            End If

        Else
            ritorno = True
        End If



        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = "TestPalettaMotorizzata"
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno

    End Function

    Public Function TestMicroForatura(ByVal ncili As Short) As Integer

        Dim ritorno As Short
        Dim ret As Short
        Dim fstop As Short
        Dim penna As New Pen(System.Drawing.Color.Red)
        Dim cicli As Short
        Dim font1 As New Font("Time New Roman", 8, FontStyle.Regular, GraphicsUnit.Pixel)
        'Dim fAspetta As CtsControls.FWait
        Dim fcicla As Boolean
        Dim fdecode As Short
        Dim strapp As String


        cicli = 1
        'If (fSoftware = SW_BARCODE_MICROHOLE Or fSoftware = SW_BARCODE_MICROHOLE_IQA Or fSoftware = SW_MICROHOLE) Then
        'fZebrato = True
        If (fZebrato = 1) Then
            fdecode = 1

            If fdecode = 1 Then
                fcicla = True
                While (fcicla)



                    idmsg = 279
                    'idmsg = 165 'DEFAULT 
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy("Inserire un Documento per testare la MICROFORATURA", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                    End If

                    If (dres <> DialogResult.OK) Then
                        'ret = Ls40Class.Costanti.S_LS_OKAY
                        ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                        fcicla = False
                        fstop = True
                    Else
                        DisposeImg()
                        StrMessaggio = "Test MicroForatura"

                        'ls40.NomeFileImmagine = zzz + "\Dati\ImageBarcode2D" + IndImgF.ToString("0000") + ".bmp"
                        'ls40.NomeFileImmagineRetro = zzz + "\Dati\ImageBarcode2DR" + IndImgR.ToString("0000") + ".bmp"

                        ret = ls150.TestMicroForatura()
                        'SalvaDati()
                        Select Case ret
                            Case Ls150Class.Costanti.S_LS150_PAPER_JAM
                                idmsg = 111
                                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                    dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                                Else
                                    dres = MessageboxMy("ATTENZIONE CARTA BLOCCATA LIBERARE IL PERCORSO", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                End If

                                If (dres <> DialogResult.OK) Then
                                    ret = Ls150Class.Costanti.S_LS150_USER_ABORT
                                    fstop = True
                                Else
                                    ls150.Reset(Ls150Class.Costanti.S_RESET_FREE_PATH)
                                End If
                                fcicla = False
                            Case Ls150Class.Costanti.S_LS150_FEEDEREMPTY
                                fstop = True


                            Case Ls150Class.Costanti.S_LS150_OKAY

                                ret = Ls150Class.Costanti.S_LS150_OKAY
                                fcicla = False


                                'DisposeImg()
                                'FileZoomCorrente = zzz + "\Dati\cardImageC" + IndImgF.ToString("0000") + ".bmp"
                                'PFronte.Image = System.Drawing.Image.FromFile(zzz + "\Dati\ImageBarcode2D" + IndImgF.ToString("0000") + ".bmp")
                                'PRetro.Image = System.Drawing.Image.FromFile(zzz + "\Dati\ImageBarcode2DR" + IndImgR.ToString("0000") + ".bmp")
                                'Pimage.Image = PFronte.Image
                                'PImageBack.Image = PRetro.Image
                                'IndImgF += 1
                                'IndImgR += 1

                                'If dres = DialogResult.Cancel Then
                                '    ret = -11111
                                'End If
                            Case Else
                                If (ret = 125) Then
                                    strapp = "Scanner Zebrato Montato al contrario"
                                Else
                                    strapp = "Errore sulla Verifica Microforatura" + ret.ToString()

                                End If
                                dres = MessageboxMy(strapp, "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Question, Nothing)



                        End Select
                    End If
                    If cicli = ncili Then
                        fcicla = False
                    End If
                    cicli += 1
                End While
            Else
                ' non ho fatto nulla, e setto tutto a ok
                ret = Ls150Class.Costanti.S_LS150_OKAY
            End If
        Else
            ritorno = True
            ret = 0
        End If




        FileTraccia.UltimoRitornoFunzione = ret
        If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
            ritorno = True
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        'fAspetta.Close()
        Return ritorno
    End Function

    Public Function TestRullini() As Integer

        Dim ritorno As Integer
        Dim ret As Short

        ls150.ReturnC.FunzioneChiamante = "TestRullini"
        ritorno = False

        FaseEsec()

        'TestTrasporto(15)

        'controllare l'esatta posizione dei rulli di sfogliatura e premere OK per continuare 
        idmsg = 282

        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            immamsg = My.Resources.Rulli
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Question, immamsg)
        Else
            dres = MessageboxMy("Verificare il corretto codice del primo rullo sfogliatura e premere OK per confermare", "Attenzione", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing)
        End If

        If (dres = DialogResult.Yes) Then
            ret = Ls150Class.Costanti.S_LS150_OKAY
            ritorno = True
        Else
            ret = Ls150Class.Costanti.S_LS150_USER_ABORT
            StrCommento += "Abortito Da Utente "
            ViewError(fase)
        End If


        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If

        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Sub MessageTestTrasporto(ByVal Nrdoc As Short, ByVal strmsg As String, ByVal TypeSpeed As Short)
        Dim cont_fail As Integer
        Dim cont As Integer
        Dim ret As Integer
        Dim floop As Boolean
        Dim str_msg As String
        Dim chcd As New CheckCodeline


        chcd.Reset()
        chcd.VisualizzaFinestra(True)
        chcd.DaLeggereTrasporto = 15
        chcd.Tipo_controllo = CheckCodeline.CONTROLLO_TEST_TRASPORTO


        idmsg = 291
        'If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
        'Titolo = strmsg
        'dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        'Else

        str_msg = "Inserire:" & vbNewLine & "5 DOCUTEST --307" & vbNewLine & "5 DOCUTEST --308" & vbNewLine & "5 DOCUTEST --042-04" & vbNewLine & " (in questa sequenza)dalla parte delle Barrette Continue"
        dres = MessageboxMy2(str_msg, strmsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing, IdMessaggio, immamsg)
        'End If

        If dres = DialogResult.OK Then
            cont = 0
            cont_fail = 0
            floop = False
            Do
                ret = ls150.ExecReadDocSpeed(TypeSpeed, 0, 1)
                If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
                    cont = cont + 1
                    chcd.NrDocTrasport = cont
                    chcd.Conteggiacodeline()
                    ' // ... testo se lettura ok !
                    If (ls150.llMin < SAMPLE_VALUE_MIN Or ls150.llMax > SAMPLE_VALUE_MAX) Then
                        idmsg = 292
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("--------Warning--------", strmsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                        End If
                    End If
                    If (cont >= Nrdoc) Then
                        floop = True
                    End If
                Else
                    If (ret = Ls150Class.Costanti.S_LS150_FEEDEREMPTY Or ret = Ls150Class.Costanti.S_LS150_CALIBRATION_FAILED) Then
                    Else
                        cont_fail = cont_fail + 1
                        ls150.Reset(Ls150Class.Costanti.S_RESET_FREE_PATH)
                        If (cont_fail = 5) Then
                            floop = True
                        End If

                    End If
                End If
            Loop While floop = False
        End If

        chcd.VisualizzaFinestra(False)



    End Sub

    Public Function TestTrasporto(ByVal Nrdoc As Short) As Integer

        Dim ritorno As Integer

        ls150.ReturnC.FunzioneChiamante = "TestTrasporto"

        Try
            Dim ret As Integer
            Dim strmsg As String
            Dim TypeSpeed As Short

            Dim Velocit As Integer

            'Dim finfo As New CtsControls.FormInfoTest
            'FileTraccia.Note = ""
            ritorno = False

            FaseEsec()

            If (SerialNumberDaStampare = Nothing) Then
                SerialNumberDaStampare = "PROGETTO"
            End If

            If (SerialNumberPiastra = Nothing) Then
                SerialNumberPiastra = "PROGETTO_PIASTRA"
            End If

            ls150.NomeFileReport = zzz + "\Images_Scanner\Log\" + SerialNumberDaStampare.ToString

            Velocit = 0
            ret = ls150.SetVelocita(Velocit) ' alta
            TypeSpeed = TEST_SPEED_200_DPI
            strmsg = "Test Trasporto TEST_SPEED_200_DPI"

            MessageTestTrasporto(Nrdoc, strmsg, TypeSpeed)

            Velocit = 0
            ret = ls150.SetVelocita(Velocit) ' alta
            TypeSpeed = TEST_SPEED_300_DPI
            strmsg = "Test Trasporto TEST_SPEED_300_DPI"

            MessageTestTrasporto(Nrdoc, strmsg, TypeSpeed)

            Velocit = 1
            If fTimbri = True Then
                ret = ls150.SetVelocita(Velocit) ' bassa
            Else
                ret = ls150.SetVelocita(Velocit) ' bassa
            End If
            TypeSpeed = TEST_SPEED_75_DPM
            strmsg = "Test Trasporto TEST_SPEED_75_DPM"

            MessageTestTrasporto(Nrdoc, strmsg, TypeSpeed)

            TestIdentificativo()

            If ls150.SLsName.Contains("LS150_UV") Then
                FMacchinaColori = False
                Velocit = 0
                If fTimbri = True Then
                    ret = ls150.SetVelocita(Velocit) ' alta, la abbasa la periferica
                Else
                    ret = ls150.SetVelocita(Velocit) ' alta 
                End If
                TypeSpeed = TEST_SPEED_300_DPI_UV
                strmsg = "Test Trasporto TEST_SPEED_300_DPI_UV"
                MessageTestTrasporto(Nrdoc, strmsg, TypeSpeed)

            End If

            If (FMacchinaColori = True) Then
                Velocit = 0
                If fTimbri = True Then
                    ret = ls150.SetVelocita(Velocit) ' alta, la abbasa la periferica
                Else
                    ret = ls150.SetVelocita(Velocit) ' alta 
                End If
                TypeSpeed = TEST_SPEED_300_COLOR
                strmsg = "Test Trasporto TEST_SPEED_300_COLOR"
                MessageTestTrasporto(Nrdoc, strmsg, TypeSpeed)
            End If

            TestIdentificativo()

            Velocit = 0 ' alta
            ret = ls150.SetVelocita(Velocit)


            'finfo.Close()

            ls150.ReturnC.FunzioneChiamante = "TestTrasporto"

            ritorno = True
            If ritorno = True Then
                FaseOk()
                'FileTraccia.CalMicr = 1
            Else
                FaseKo()
            End If

            FileTraccia.UltimoRitornoFunzione = ret
            FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
            Return ritorno
        Catch ex As Exception
            ritorno = True
            FaseOk()
        End Try
    End Function

    Public Function SalvaDati() As Integer
        Dim ritorno As Integer
        ritorno = 0
        DatiFine.lista.Clear()
        'DatiFine.AddDato(DatiFineCollaudo.TipoDato.DUMP, Ls150Class.Periferica.BufferDump)

        DatiFine.AddDato(DatiFineCollaudo.TipoDato.FOTO1, BitConverter.GetBytes(Ls150Class.Periferica.PhotoValue(0)))
        DatiFine.AddDato(DatiFineCollaudo.TipoDato.FOTO2, BitConverter.GetBytes(Ls150Class.Periferica.PhotoValue(1)))
        DatiFine.AddDato(DatiFineCollaudo.TipoDato.FOTO3, BitConverter.GetBytes(Ls150Class.Periferica.PhotoValue(2)))


        DatiFine.AddDato(DatiFineCollaudo.TipoDato.DOPPIA1, BitConverter.GetBytes(Ls150Class.Periferica.PhotoValueDP(0)))
        DatiFine.AddDato(DatiFineCollaudo.TipoDato.DOPPIA2, BitConverter.GetBytes(Ls150Class.Periferica.PhotoValueDP(1)))
        DatiFine.AddDato(DatiFineCollaudo.TipoDato.DOPPIA3, BitConverter.GetBytes(Ls150Class.Periferica.PhotoValueDP(2)))

        DatiFine.AddDato(DatiFineCollaudo.TipoDato.DP_TH1, BitConverter.GetBytes(Ls150Class.Periferica.PhotoValueDP(3)))
        DatiFine.AddDato(DatiFineCollaudo.TipoDato.DP_TH2, BitConverter.GetBytes(Ls150Class.Periferica.PhotoValueDP(4)))
        DatiFine.AddDato(DatiFineCollaudo.TipoDato.DP_TH3, BitConverter.GetBytes(Ls150Class.Periferica.PhotoValueDP(5)))
        DatiFine.AddDato(DatiFineCollaudo.TipoDato.DP_TH4, BitConverter.GetBytes(Ls150Class.Periferica.PhotoValueDP(6)))
        DatiFine.AddDato(DatiFineCollaudo.TipoDato.DP_TH5, BitConverter.GetBytes(Ls150Class.Periferica.PhotoValueDP(7)))
        DatiFine.AddDato(DatiFineCollaudo.TipoDato.DP_TH6, BitConverter.GetBytes(Ls150Class.Periferica.PhotoValueDP(8)))

        DatiFine.AddDato(DatiFineCollaudo.TipoDato.CINGHIA_MIN, BitConverter.GetBytes(ls150.ValMinCinghia))
        DatiFine.AddDato(DatiFineCollaudo.TipoDato.CINGHIA_MAX, BitConverter.GetBytes(ls150.ValMaxCinghia))

        DatiFine.AddDato(DatiFineCollaudo.TipoDato.TRIMMER200, BitConverter.GetBytes(ls150.TrimmerValue200))
        DatiFine.AddDato(DatiFineCollaudo.TipoDato.TRIMMER300, BitConverter.GetBytes(ls150.TrimmerValue300))
        DatiFine.AddDato(DatiFineCollaudo.TipoDato.TRIMMER75, BitConverter.GetBytes(ls150.TrimmerValue75))


        DatiFine.AddDato(DatiFineCollaudo.TipoDato.TEMPOCAMP200, BitConverter.GetBytes(ls150.TempoCamp200))
        DatiFine.AddDato(DatiFineCollaudo.TipoDato.TEMPOCAMP300, BitConverter.GetBytes(ls150.TempoCamp300))
        DatiFine.AddDato(DatiFineCollaudo.TipoDato.TEMPOCAMP75, BitConverter.GetBytes(ls150.TempoCamp75))





        'DatiFine.AddDato(DatiFineCollaudo.TipoDato.RTOTFRONTE, BitConverter.GetBytes(RigheTotaliFronte))
        'DatiFine.AddDato(DatiFineCollaudo.TipoDato.RTOTRETRO, BitConverter.GetBytes(RigheTotaliRetro))
        'DatiFine.AddDato(DatiFineCollaudo.TipoDato.RCONTFRONTE, BitConverter.GetBytes(RigheContigueFronte))
        'DatiFine.AddDato(DatiFineCollaudo.TipoDato.RCONTRETRO, BitConverter.GetBytes(RigheContigueRetro))



        DatiFine.AddDato(DatiFineCollaudo.TipoDato.VELOCITA, BitConverter.GetBytes(ls150.TempoImmagine))

        'perche' commentati ???
        If (FMacchinaColori = True) Then
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.TRIMMER300C, BitConverter.GetBytes(ls150.TrimmerValue300C))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.TEMPOCAMP300C, BitConverter.GetBytes(ls150.TempoCamp300C))
        End If

        If (fMacchinaUv = True) Then
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.TRIMMER300UV, BitConverter.GetBytes(ls150.TrimmerValue300UV))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.TEMPOCAMP300UV, BitConverter.GetBytes(ls150.TempoCamp300UV))
        End If
        If (ls150.fDatiScannerColore = True) Then
            '  DatiFine.AddDato(DatiFineCollaudo.TipoDato.TRIMMER300C, BitConverter.GetBytes(ls150.TrimmerValue300C))
            '  DatiFine.AddDato(DatiFineCollaudo.TipoDato.TEMPOCAMP300C, BitConverter.GetBytes(ls150.TempoCamp300C))

            ''DATI SCANNER FRONTE
            'DatiFine.AddDato(DatiFineCollaudo.TipoDato.FRONTEGRIGI, Ls150Class.Periferica.BufferDumpGray)
            'DatiFine.AddDato(DatiFineCollaudo.TipoDato.FRONTERED, Ls150Class.Periferica.BufferDumpRed)
            'DatiFine.AddDato(DatiFineCollaudo.TipoDato.FRONTEGREEN, Ls150Class.Periferica.BufferDumpGreen)
            'DatiFine.AddDato(DatiFineCollaudo.TipoDato.FRONTEBLU, Ls150Class.Periferica.BufferDumpBlue)
            ''DATI SCANNER RETRO
            'DatiFine.AddDato(DatiFineCollaudo.TipoDato.RETROGRIGI, Ls150Class.Periferica.BufferDumpGrayR)
            'DatiFine.AddDato(DatiFineCollaudo.TipoDato.FRONTERED, Ls150Class.Periferica.BufferDumpRedR)
            'DatiFine.AddDato(DatiFineCollaudo.TipoDato.RETROGREEN, Ls150Class.Periferica.BufferDumpGreenR)
            'DatiFine.AddDato(DatiFineCollaudo.TipoDato.RETROBLU, Ls150Class.Periferica.BufferDumpBlueR)


            ''DATI VALORI AD FRONTE RETRO
            'DatiFine.AddDato(DatiFineCollaudo.TipoDato.ADFRONTE, Ls150Class.Periferica.BufferADValues)
            'DatiFine.AddDato(DatiFineCollaudo.TipoDato.ADRETRO, Ls150Class.Periferica.BufferADValuesR)

        End If

        DatiFine.AddDato(DatiFineCollaudo.TipoDato.TEMPOCAMP300UV, BitConverter.GetBytes(ls150.TempoCamp300UV))
        DatiFine.AddDato(DatiFineCollaudo.TipoDato.TEMPOCAMP300C, BitConverter.GetBytes(ls150.TempoCamp300C))

        If (fZebrato = 1) Then
            '14-09-2015
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_ERRORFLAGS, BitConverter.GetBytes(ls150.errorFlags))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_innerLevValidPerc, BitConverter.GetBytes(ls150.innerLevValidPerc))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_innerDimValidPerc, BitConverter.GetBytes(ls150.innerDimValidPerc))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_outerDimValidPerc, BitConverter.GetBytes(ls150.outerDimValidPerc))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_outerLevValidPerc, BitConverter.GetBytes(ls150.outerLevValidPerc))

            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_wDimStatmean, BitConverter.GetBytes(ls150.wDimStatmean))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_wDimStatStdDev, BitConverter.GetBytes(ls150.wDimStatStdDev))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_bDimStatmean, BitConverter.GetBytes(ls150.bDimStatmean))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_bDimStatStdDev, BitConverter.GetBytes(ls150.bDimStatStdDev))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_gDimStatmean, BitConverter.GetBytes(ls150.gDimStatmean))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_gDimStatDev, BitConverter.GetBytes(ls150.gDimStatDev))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_wLevStatmean, BitConverter.GetBytes(ls150.wLevStatmean))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_wLevStatStdDev, BitConverter.GetBytes(ls150.wLevStatStdDev))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_bLevStatmean, BitConverter.GetBytes(ls150.bLevStatmean))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_bLevStatStdDev, BitConverter.GetBytes(ls150.bLevStatStdDev))
        End If

        '29/01/2018 , Salvo a DataBase le versioni FW
        'DatiFine.AddDato(DatiFineCollaudo.TipoDato.Ver_firm1 ,


        ''XX/01/2017 le immagini vengono salvate su server e non piu' dentro il DataBase
        'Try
        '    'Save Report File on Server
        '    ' Loop through each file in the directory
        '    For Each fileD As IO.FileInfo In New IO.DirectoryInfo(Application.StartupPath() + "\Images_Scanner").GetFiles

        '        If fileD.Name <> "Thumbs.db" Then
        '            If (fileD.Name.ToString() = UltimaImmagineFrontToSave - ToString() Or fileD.Name = UltimaImmagineBackToSave Or fileD.Name = UltimaImmagineFrontUVToSave) Then
        '            Else
        '                File.Delete(Application.StartupPath() + "\Images_Scanner\" & fileD.Name)
        '            End If

        '        End If
        '    Next
        'Catch ex As Exception
        '    ritorno = 0
        'End Try



        Return ritorno
    End Function

    Public Function AddRigaTabella(ByVal Tab As DataTable, ByVal s1 As String, ByVal s2 As String, ByVal s3 As String, ByVal s4 As String) As DataRow
        Dim row As DataRow
        row = Tab.NewRow()
        row.Item(0) = s1
        row.Item(1) = s2
        row.Item(2) = s3
        row.Item(3) = s4

        Tab.Rows.Add(row)
        Return row
    End Function

    Public Function AddRigaTabella2(ByVal Tab As DataTable, ByVal s1 As String, ByVal s2 As String) As DataRow
        Dim row As DataRow
        row = Tab.NewRow()
        row.Item(0) = s1
        row.Item(1) = s2

        Tab.Rows.Add(row)
        Return row
    End Function

    Public Function AddRigaTabella1(ByVal Tab As DataTable, ByVal s1 As String) As DataRow
        Dim row As DataRow
        row = Tab.NewRow()
        row.Item(0) = s1

        Tab.Rows.Add(row)
        Return row
    End Function

    Function StampaReport()


        'FileTraccia.Matricola = "LS150.321388"
        Dim nomeFileLog As String = Application.StartupPath & "\Report150\" & FileTraccia.Matricola & "_" & DateTime.Now.ToString("yyyyMMdd_hhmmss") & ".txt"
        Dim headerLine As String = ""
        Dim recordLine As String = ""
        Dim larghezza As Integer = 62

        FaseEsec()

        ls150.ReturnC.FunzioneChiamante = "Stampa Report"
        FileTraccia.UltimoRitornoFunzione = 0
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante

        recordLine = "┌────────────────────────── ARCA ─────────────────────────────┐" & vbNewLine & _
                    ("│ Peripheral Name:                   " & FileTraccia.Periferica).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Product Line:                      " & FileTraccia.LineaProd).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ S/N #:                             " & FileTraccia.Matricola).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Product Code:                      " & FileTraccia.Ordine_prod).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Date :                             " & FileTraccia.DataCollaudo).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Manufacter :                       " & FileTraccia.Manufacter).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Model :                            " & FileTraccia.Periferica).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Firmware Version :                 " & FileTraccia.VersioneFw).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Tested By :                        " & FileTraccia.Operatore & "-" & FileTraccia.NomeStazione).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Test Duration :                    " & FileTraccia.Durata_Test.Minutes.ToString + ":" + FileTraccia.Durata_Test.Seconds.ToString).PadRight(larghezza) & "│" & vbNewLine & _
                     "│                                                             │" & vbNewLine & _
                     "├────────────────── COMPLETED AUTOMATED TESTS ────────────────┤" & vbNewLine & _
                     "│                                                             │" & vbNewLine & _
                    ("│ Sensor Test:                             " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Calibration Front Scanner:               " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Quality Front Image:                     " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Calibration Rear Scanner:                " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Quality Rear Image:                      " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Double Leafing Sensor Calibration:       " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Double Leafing Sensor Test:              " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ MICR Head Calibration:                   " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ MICR Speed Test:                         " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ MICR Reader Test:                        " & "PASSED").PadRight(larghezza) & "│" & vbNewLine


        If (fTimbri = True) Then
            recordLine += ("│ Stamp Test:                              " & "PASSED").PadRight(larghezza) & "│" & vbNewLine
        End If

        If (fStampante = True) Then
            recordLine += ("│ Ink Jet Printer Test:                    " & "PASSED").PadRight(larghezza) & "│" & vbNewLine
        End If

        If (fBadge <> 0) Then
            recordLine += ("│ Badge Reader Test:                       " & "PASSED").PadRight(larghezza) & "│" & vbNewLine
        End If

        recordLine += "│                                                             │" & vbNewLine
        recordLine += "├─────────────────────────────────────────────────────────────┤" & vbNewLine
        recordLine += "│                                                             │" & vbNewLine

        recordLine += ("│ Visual Inspection:                       " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ Label Check:                             " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ Serial Number & Corrisponding Label:     " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ Inkjet cartidge Removed Check:           " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ Inkjet cartidge Present in the box:      " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ Unit Clean:                              " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ USB Cable Present:                       " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ Power supply Present:                    " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ Manuals present  in the box:             " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine

        recordLine += "│                                                             │" & vbNewLine
        recordLine += "│────────────────── DETAIL RESULTS ───────────────────────────┤" & vbNewLine
        recordLine += "│                                                             │" & vbNewLine

        'recordLine += ("│ DOCUTEST 061  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine & _
        '      ("│ DOCUTEST 062  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine & _
        '      ("│ DOCUTEST 063  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine & _
        '      ("│ DOCUTEST 064  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine & _
        '      ("│ DOCUTEST 065  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine & _
        '      ("│ DOCUTEST 066  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine & _
        '      ("│ DOCUTEST 048  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine & _
        '      ("│ DOCUTEST 046  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine

        recordLine += ("│ DOCUTEST 062  :read --> 10 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine & _
                      ("│ DOCT-R147     :read --> 10 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine



        recordLine += "└─────────────────────────────────────────────────────────────┘"

        Dim filelog As New StreamWriter(nomeFileLog)
        filelog.WriteLine(recordLine)
        filelog.Close()

        FaseOk()
        Return True

    End Function

    Public Function CercaMessaggio(ByVal filename As String, ByRef Id As Integer, ByRef a As String, ByRef b As String, ByRef c As String, ByRef d As String, ByRef img As Image) As Boolean

        Dim freturn As Boolean
        Dim prova As System.Xml.XmlDocument = New System.Xml.XmlDocument()

        If Not img Is Nothing Then
            img.Dispose()
            img = Nothing
        End If

        freturn = False
        If (System.IO.File.Exists(filename)) Then
            prova.Load(filename)
            Dim xx As XmlNodeList = prova.GetElementsByTagName("T_Messaggi")

            For Each n As XmlNode In xx

                Dim lll As XmlNodeList = n.ChildNodes
                Dim noddo As XmlNode = n.ChildNodes(0)
                If (noddo.InnerText = Id.ToString()) Then
                    a = (n.ChildNodes(0).InnerText)
                    b = (n.ChildNodes(1).InnerText)
                    c = (n.ChildNodes(2).InnerText)

                    d = (n.ChildNodes(3).InnerText)

                    If (Not (n.ChildNodes(4) Is Nothing)) Then

                        Dim ms As System.IO.MemoryStream
                        ms = New System.IO.MemoryStream()
                        Dim vet(50 * 1024) As Byte


                        vet = System.Convert.FromBase64CharArray(n.ChildNodes(4).InnerText.ToCharArray(), 0, n.ChildNodes(4).InnerText.Length)

                        ms.Write(vet, 0, vet.Length)
                        img = Image.FromStream(ms)
                    End If
                    freturn = True
                End If
            Next
        Else

        End If



        Return freturn
    End Function

    Public Function TestButterwoth(ByVal ScanMode As Short) As Integer
        Dim ritorno As Integer
        Dim ret As Integer
        'Dim str As String
        Dim localDate As String
        Dim hour As String

        ritorno = False

        localDate = Now.ToString("yyyyMMdd")
        hour = Now.ToString("hhmmss")

        FaseEsec()

        idmsg = 299

        If (SerialNumberDaStampare = Nothing) Then
            SerialNumberDaStampare = "PROGETTO"
        End If

        If (SerialNumberPiastra = Nothing) Then
            SerialNumberPiastra = "PROGETTO_PIASTRA"
        End If

        ls150.NomeFileImmagine = zzz + "\Images_Scanner\FrontImage" + SerialNumberDaStampare.ToString + "_" + SerialNumberPiastra.ToString + "_" + IndImgF.ToString("0000") + "_" + localDate.ToString + "_" + hour.ToString & ".bmp"
        ls150.NomeFileImmagineUV = zzz + "\Images_Scanner\FrontImageUV" + SerialNumberDaStampare.ToString + "_" + SerialNumberPiastra.ToString + "_" + IndImgF.ToString("0000") + "_" + localDate.ToString + "_" + hour.ToString & ".bmp"
        ls150.NomeFileImmagineRetro = zzz + "\Images_Scanner\BackImage" + SerialNumberDaStampare.ToString + "_" + SerialNumberPiastra.ToString + "_" + IndImgR.ToString("0000") + "_" + localDate.ToString + "_" + hour.ToString & ".bmp"

        Select Case ScanMode
            Case Ls150Class.Costanti.S_SCAN_MODE_COLOR_100, _
                Ls150Class.Costanti.S_SCAN_MODE_COLOR_200, _
                Ls150Class.Costanti.S_SCAN_MODE_COLOR_AND_RED_100, _
                Ls150Class.Costanti.S_SCAN_MODE_COLOR_AND_RED_200, _
                Ls150Class.Costanti.S_SCAN_MODE_COLOR_300

                ScanMode = Ls150Class.Costanti.S_SCAN_MODE_COLOR_300


                If (FMacchinaColori = False) Then
                    FaseOk()
                    FileTraccia.UltimoRitornoFunzione = 0 'replycode
                    FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
                    Return True
                End If

            Case Ls150Class.Costanti.S_SCAN_MODE_256GR100_AND_UV, _
                Ls150Class.Costanti.S_SCAN_MODE_256GR200_AND_UV, _
                Ls150Class.Costanti.S_SCAN_MODE_256GR300_AND_UV

                ScanMode = Ls150Class.Costanti.S_SCAN_MODE_256GR200_AND_UV

                idmsg = 273
            Case Else
                ScanMode = Ls150Class.Costanti.S_SCAN_MODE_256GR300
        End Select

        idmsg = 299

        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire DOCUTEST-309", "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Question, VetImg(7))
        End If

        If dres = DialogResult.OK Then
            ret = ls150.LeggiImmagini(Ls150Class.Costanti.S_SIDE_ALL_IMAGE, ScanMode, CLEAR_AND_ALIGN_IMAGE)
        End If

        If (ret = Ls150Class.Costanti.S_LS150_OKAY) Then
            
            Select Case ret
                Case Ls150Class.Costanti.S_LS150_JAM_AT_SCANNERPHOTO, + _
                    Ls150Class.Costanti.S_LS150_JAM_AT_MICR_PHOTO, + _
                    Ls150Class.Costanti.S_LS150_JAM_DOC_TOOLONG, + _
                    Ls150Class.Costanti.S_LS150_PAPER_JAM
                    ret = ls150.Reset(Ls150Class.Costanti.S_RESET_FREE_PATH)
            End Select
        End If


        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If

        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

End Class