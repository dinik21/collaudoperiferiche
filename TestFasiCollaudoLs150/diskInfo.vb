Imports System

Imports System.Text

Imports System.Runtime.InteropServices

Public Class diskInfo

    Public Const DRIVE_UNKNOWN As Integer = &H0
    Public Const DRIVE_NO_ROOT_DIR As Integer = &H1
    Public Const DRIVE_REMOVABLE As Integer = &H2
    Public Const DRIVE_FIXED As Integer = &H3
    Public Const DRIVE_REMOTE As Integer = &H4
    Public Const DRIVE_CDROM As Integer = &H5
    Public Const DRIVE_RAMDISK As Integer = &H6
    <DllImport("kernel32.dll")> Public Shared Function GetDriveType(ByVal lpRootPathName As String) As Integer
    End Function
    <DllImport("kernel32.dll")> Private Shared Function GetVolumeInformation(ByVal lpRootPathName As String, ByVal lpVolumeNameBuffer As StringBuilder, ByVal nVolumeNameSize As Integer, ByRef lpVolumeSerialNumber As Integer, ByRef lpMaximumComponentLength As Integer, ByRef lpFileSystemFlags As Integer, ByVal lpFileSystemNameBuffer As StringBuilder, ByVal nFileSystemNameSize As Integer) As Boolean
    End Function

    Public Shared Function GetVolumeName(ByVal strRootPath As String) As String

        Dim sbVolumeName As StringBuilder = New StringBuilder(256)
        Dim sbFileSystemName As StringBuilder = New StringBuilder(256)
        Dim nVolSerial As Integer = 0
        Dim nMaxCompLength As Integer = 0
        Dim nFSFlags As Integer = 0
        Dim bResult As Boolean = GetVolumeInformation(strRootPath, sbVolumeName, 256, nVolSerial, nMaxCompLength, nFSFlags, sbFileSystemName, 256)
        If bResult Then
            Return sbVolumeName.ToString
        Else
            Return ""
        End If
    End Function

    Public Shared Function GetInformations(ByVal strRootPath As String) As String()
        Dim sbVolumeName As StringBuilder = New StringBuilder(256)
        Dim sbFileSystemName As StringBuilder = New StringBuilder(256)
        Dim nVolSerial As Integer = 0
        Dim nMaxCompLength As Integer = 0
        Dim nFSFlags As Integer = 0
        Dim result As String() = {"", "", "", "", ""}
        Dim bResult As Boolean = GetVolumeInformation(strRootPath, sbVolumeName, 256, nVolSerial, nMaxCompLength, nFSFlags, sbFileSystemName, 256)
        If bResult Then
            result(0) = sbVolumeName.ToString
            result(1) = sbFileSystemName.ToString
            result(2) = nVolSerial.ToString
            result(3) = nMaxCompLength.ToString
            result(4) = nFSFlags.ToString
        End If
        Return result
    End Function
End Class
