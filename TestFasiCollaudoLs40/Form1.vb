﻿
Imports System.Windows.Forms
Imports System.Drawing
'Imports ReportPrinting
Imports System.Xml
Imports System.Xml.XPath

Imports TestFasiCollaudoLs40.Alco
Imports TestFasiCollaudoLs40.Mod_Card
Imports TestFasiCollaudoLs40.SCARD_READERSTATEA





Public Class Form1


    Public Sub WriteMemory()



        Dim lReturn As Integer
        Dim err As String
        Dim OutBuffer(1) As Byte

        'lReturn = SCardEstablishContext(SCARD_SCOPE_SYSTEM,NULL,NULL,&hSC)
        lReturn = SCardEstablishContext(SCARD_SCOPE_SYSTEM, lngNull, lngNull, lngContext)

        If (SCARD_S_SUCCESS <> lReturn) Then
            MessageBox.Show("PC/SC SMART CARD SERVICE UNAVAILABLE", "SCardEstablishContext")
        Else


            Dim dwAP As Integer = 0
            'lReturn = SCardConnect(hSC, L"Generic Usb Smart Card Reader 0", SCARD_SHARE_DIRECT, SCARD_PROTOCOL_UNDEFINED, &hCard, &dwAP);
            strReaderName = "Generic Usb Smart Card Reader 0"
            lReturn = SCardConnectA(lngContext, strReaderName, SCARD_SHARE_DIRECT, _
                       SCARD_PROTOCOL_UNDEFINED, lngCard, lngActiveProtocol)
            If (SCARD_S_SUCCESS <> lReturn) Then
                err = String.Format("UNABLE TO CONTACT READER: 0x%.2X", lReturn)
                MessageBox.Show(err, "SCardConnect - SCARD_SHARE_DIRECT")
            End If
            'lReturn = Alcor_SwitchCardMode(hCard, 0, SYNCHRONOUS_CARD_SLE4442_MODE);

            OutBuffer(0) = SYNCHRONOUS_CARD_SLE4442_MODE


            lReturn = Alcor_SwitchCardMode(lngCard, 0, OutBuffer(0))
            If (SCARD_S_SUCCESS <> lReturn) Then

                err = String.Format("UNABLE TO SWITCH CARD MODE: 0x%.2X", lReturn)
                MessageBox.Show(Err, "Alcor_SwitchCardMode")


            End If

            lReturn = SCardDisconnect(lngCard, SCARD_LEAVE_CARD)
            If (SCARD_S_SUCCESS <> lReturn) Then

                err = String.Format("UNABLE TO DISCONNECT FROM SYNC MODE: 0x%.2X", lReturn)
                MessageBox.Show(err, "SCardDisconnect")

            End If
            lngCard = 0

            ' SCardReleaseContext(lngCard)
            ' lReturn = SCardEstablishContext(SCARD_SCOPE_SYSTEM, lngNull, lngNull, lngContext)

            'lReturn = SCardConnect(hSC, L"Generic Usb Smart Card Reader 0", SCARD_SHARE_SHARED, SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1, &hCard, &dwAP);
            lReturn = SCardConnectA(lngContext, strReaderName, SCARD_SHARE_SHARED, SCARD_PROTOCOL_Tx, lngCard, lngActiveProtocol)


            If (SCARD_S_SUCCESS <> lReturn) Then
                err = String.Format("UNABLE TO CONTACT CARD IN T0|T1 MODE: 0x%.2X", lReturn)
                MessageBox.Show(err, "SCardConnect - SCARD_SHARE_SHARED")
            End If

            'WRITE MEMORY
            Dim BufferData(90) As Byte
            Dim bite As Byte
            Dim add As Byte
            Dim slot As Byte = 0


            BufferData(0) = 67
            BufferData(1) = 65
            BufferData(2) = 82
            BufferData(3) = 84
            BufferData(4) = 65


            For i As Integer = 0 To BufferData.Length
                bite = BufferData(1)
                add = 60 + i
                lReturn = SLE4442Cmd_UpdateMainMemory(lngCard, slot, add, bite)
            Next

            lReturn = SCardDisconnect(lngCard, SCARD_LEAVE_CARD)
            If (SCARD_S_SUCCESS <> lReturn) Then
              
                err = String.Format("UNABLE TO DISCONNECT CARD: 0x%.2X", lReturn)
                MessageBox.Show(err, "ERROR - SCardDisconnect")
            End If
            SCardReleaseContext(lngCard)
            If (SCARD_S_SUCCESS = lReturn) Then
                MessageBox.Show("CARD ENCODED SUCCESSFULLY", "ENCODING COMPLETE")
            End If
        End If

    End Sub


    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim lngResult As Long = 0
        Dim lngReadersLen As Integer
        Dim astrReaders As Object
        Dim strReaders As String
        Dim intbuffersize As Integer
        Dim bSeq As Integer



        Dim bAddress As Byte
        Dim bData As Byte
        Dim bReadLen As Byte

        Dim bReadBuffer(256) As Byte
        Dim bReturnLen As Byte

        Dim lngNull As Integer = 0

        Dim Index As Integer = 0

        bCurSlotNum = 0
        lngNull = Nothing




        ' WriteMemory()


        ' Connect to the smartcard resource manager
        lngResult = SCardEstablishContext(SCARD_SCOPE_SYSTEM, lngNull, lngNull, lngContext)
        If (lngResult <> 0) Then
            MessageBox.Show("SCardEstablishContext Failed")

        End If
        intbuffersize = 2048
        bSeq = 0

        strReaders = New String("", 2600)
        lngReadersLen = Len(strReaders)



        ' get reader list
        ' pass in implicit request for all readers, both valid and invalid,
        '    using NULL context, NULL group parameters
        lngResult = SCardListReaders(lngContext, vbNullString, strReaders, lngReadersLen)
        If SCARD_S_SUCCESS = lngResult Then
            ' process returned multistring
            astrReaders = ParseMultistring(strReaders, intReaderCount)
            If 0 < intReaderCount Then
                ' put readers into combobox
                For i = 1 To intReaderCount
                    ReaderName(i) = astrReaders(i - 1)

                Next i
            Else
                ' no readers actually returned by api call
                MessageBox.Show("No Reader Found!")

            End If
        Else
            ' could not get list of readers

            '        MsgBox (strErrNoReaders)
        End If


        strReaderName = ReaderName(1)

        IsReaderLost = True
        ' lngResult = ConnectAlcorReader()




        Dim OutBuffer(1) As Byte
        Dim InBuffer(1) As Byte

        lngResult = SCardDisconnect(lngCard, SCARD_LEAVE_CARD)
        lngCard = 0
        lngActiveProtocol = 0

        lngResult = SCardConnectA(lngContext, strReaderName, SCARD_SHARE_DIRECT, _
                        0, lngCard, lngActiveProtocol)
        If (lngResult = 0) Then
         
            OutBuffer(0) = SYNCHRONOUS_CARD_SLE4442_MODE
        

            lngResult = Alcor_SwitchCardMode(lngCard, 0, OutBuffer(0))
            If (lngResult <> 0) Then
                lngResult = MsgBox("Can't not switch to reader mode", vbOKOnly)
            End If



            ' lngResult = SCardDisconnect(lngCard, SCARD_LEAVE_CARD)

        Else
            MsgBox("Can't connect to the reader")
        End If


        '  lngResult = SCardConnectA(lngContext, strReaderName, SCARD_SHARE_SHARED, _
        '                SCARD_PROTOCOL_Tx, lngCard, lngActiveProtocol)



        bAddress = 0
        bData = 0
        bReadLen = 255
        bReturnLen = 0



        Select Case Index

            Case 0 'Read Main Memory
                lngResult = SLE4442Cmd_ReadMainMemory(lngCard, bCurSlotNum, bAddress, bReadLen, bReadBuffer(0), bReturnLen)
                'lngResult = SLE4442Cmd_UpdateMainMemory(lngCard, bCurSlotNum, bAddress, bData)
            Case 1 'Update Main Memory


            Case 2 'Read Protection Memory
                lngResult = SLE4442Cmd_ReadProtectionMemory(lngCard, bCurSlotNum, bReadLen, bReadBuffer(0), bReturnLen)

            Case 3 'Write Protection Memory
                lngResult = SLE4442Cmd_WriteProtectionMemory(lngCard, bCurSlotNum, bAddress, bData)

            Case 4 'Read Security Memory
                lngResult = SLE4442Cmd_ReadSecurityMemory(lngCard, bCurSlotNum, bReadLen, bReadBuffer(0), bReturnLen)

            Case 5 'Update Security Memory
                lngResult = SLE4442Cmd_UpdateSecurityMemory(lngCard, bCurSlotNum, bAddress, bData)

            Case 6 'Compare Verification Data
                lngResult = SLE4442Cmd_CompareVerificationData(lngCard, bCurSlotNum, bAddress, bData)

        End Select


        If (lngResult = SCARD_S_SUCCESS) Then
            LStato.Text = "" & "  OK !" & vbCrLf
            If (bReturnLen > 0) Then
                LStato.Text = LStato.Text & "========================" & vbCrLf
                For i = 0 To bReturnLen - 1
                    LStato.Text = LStato.Text & Byte2Char(bReadBuffer(i)) & " "
                Next i
                LStato.Text = LStato.Text & vbCrLf
            End If

        Else
            LStato.Text = "  Failed !" & vbCrLf
        End If

        lngResult = SCardDisconnect(lngCard, SCARD_LEAVE_CARD)

        'Timer1.Start()
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        ' length of list of readers
        'Dim lngReadersLen As Long
        ' array of readerstate with only one element
        Dim lngResult As Long
        ' cardinality of readerstate array
        'Dim lngReaderNameLen As Long
        Dim lngSCardStatus As Long
        Dim ATR As ARRAY_TYPE
        'Dim ATRLen As Long
        Dim lngReaderStateLen As Integer

        Dim dwCurrentState As SCARD_READERSTATEA

        dwCurrentState = New SCARD_READERSTATEA()

        ATR = New ARRAY_TYPE()
       
        Timer1.Enabled = False

        lngReaderStateLen = 1
        '  udtReaderStates(0).dwCurrentState = 0

        lngOldCardStatus = dwCurrentState.dwEventState
        ' check if the reader state was changed
        lngResult = SCardGetStatusChangeA(lngContext, 0, dwCurrentState, lngReaderStateLen)


        lngSCardStatus = udtReaderStates(0).dwEventState

        If (lngResult = 0) Then ' a reader presents
            If (lngOldCardStatus = lngSCardStatus) Then ' the reader state no changed
                GoTo Success_Exit
            Else 'the reader state changed
                'lblMCContent.Caption = ""
                'lblATRstring.Caption = ""
                'lblSynCardATR.Caption = ""
                lngOldCardStatus = lngSCardStatus
            End If
        Else ' no reader found
            IsReaderLost = True
            LStato.Text = "No Reader"
            LStato.Text = ""
            'lblMCContent.Caption = ""
            'lblATRstring.Caption = ""
            'lblSynCardATR.Caption = ""

        End If

        If (lngCard <> 0) Then
            lngResult = SCardDisconnect(lngCard, SCARD_LEAVE_CARD) ' disconnect the previou connection with the reader
        End If
        lngCard = lngNull
        lngActiveProtocol = 0
        lngResult = ConnectAlcorReader() ' get the current reader
        'Reader exists
        'check the card status
        lngResult = SCardGetStatusChangeA(lngContext, 0, udtReaderStates(0), _
                                            lngReaderStateLen)
        If (lngResult = 0) Then
            LStato.Text = strReaderName
            If ((udtReaderStates(0).dwEventState And SCARD_STATE_PRESENT) = SCARD_STATE_PRESENT) Then 'found a card in the reader
                For i = 0 To udtReaderStates(0).cbAtr - 1
                    ATR.byteData(i) = udtReaderStates(0).rgbAtr(i)
                Next i

                If (udtReaderStates(0).cbAtr = 0) Then
                    LStato.Text = "Card Present"
                End If
            End If
        ElseIf ((udtReaderStates(0).dwEventState And SCARD_STATE_EMPTY) = SCARD_STATE_EMPTY) Then  'no card found in the reader
            LStato.Text = "No Card"
        ElseIf (udtReaderStates(0).dwEventState = 0) Then
            IsReaderLost = True
            LStato.Text = "No Reader"
            LStato.Text = ""
        Else
            Debug.Assert(0)
        End If

            GoTo Success_Exit


        LStato.Text = "No Reader"
        LStato.Text = ""

        lngResult = SCardGetStatusChangeA(lngContext, 0, udtReaderStates(0), _
                                        lngReaderStateLen)
        GoTo Success_Exit

        'End If

Success_Exit:

        Timer1.Enabled = True

    End Sub
End Class