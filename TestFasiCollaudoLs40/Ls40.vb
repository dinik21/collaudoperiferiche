﻿Imports System.Windows.Forms
Imports System.Drawing
'Imports ReportPrinting
Imports System.Xml
Imports System.Xml.XPath
Imports System.Text
Imports System.IO


Public Class PerifLs40


    Public Const LUNGHEZZA_SERIAL_NUMBER = 12
    Public Const LUNGHEZZA_SERIAL_NUMBER_PIASTRA = 16

    Public Const PATHFRONTEQ As String = "FrontImage"
    Public Const PATHRETROQ As String = "BackImage"


    Public Const TEST_SPEED_300_DPI = 0
    Public Const TEST_SPEED_300_DPI_UV = 1
    Public Const TEST_SPEED_300_DPI_COLOR = 2

    Public Const NOME_CHIAVETTA = "CTS_KEY"
    Public Const NOME_FILE_CHIAVETTA = "Cts_file_Key.txt"
    Public Const NOME_STRINGA_CHIAVETTA = "Hub Usb test OK"
    Public Const TITOLO_DEF_MESSAGGIO = ""

    Public Const LOCALDB As String = "\LocalDB"
    Public Const MESSAGGI As String = "\Messaggi"

    Public Const VERSIONE As String = "C.T.S. TestFasiCollaudoLs40.dll -- Ver. 1.00 Rev 003 date 08/06/2011"

    Public Const SW_TOP_IMAGE = 1
    Public Const SW_BAR2D = 2
    Public Const SW_TOP_BAR2D = 3
    Public Const SW_IQA = 4
    Public Const SW_MICROHOLE = 8
    Public Const SW_BARCODE_MICROHOLE = 10
    Public Const SW_BARCODE_MICROHOLE_IQA = 14


    Public Const MARGINE_RIGHT = 1.8F
    Public Const MARGINE_LEFT = 1.8F
    Public Const MARGINE_TOP = 1.8F
    Public Const MARGINE_BOTTOM = 1.8F

    Dim ls40 As Ls40Class.Periferica
    'Dim fquality As Boolean
    'Dim BVisualZoom As Boolean



    Dim IndImgF As Integer
    Dim IndImgR As Integer
    'Dim fWaitButton As Boolean
    Dim zzz As String
    Dim FileZoomCorrente As String
    Dim StrMessaggio As String
    Dim VetImg(20) As Image
    Dim ff As CtsControls.Message
    Dim ff2 As CtsControls.Message2
    Public DatiFine As DatiFineCollaudo
    Public Hstorico As CStorico
    Public FileTraccia As New trace
    Public fase As Object 'BarraFasi.PhaseProgressBarItem
    Public DirZoom As String
    Public StrCommento As String
    Public Pimage As PictureBox
    Public PFronte As PictureBox
    Public PRetro As PictureBox
    Public PImageBack As PictureBox
    Public TipoDiCollaudo As Integer
    Public LettureCMC7 As Integer
    Public LettureE13B As Integer
    Public chcd As CheckCodeline
    Public TipoComtrolloLetture As Integer
    Public formpadre As Windows.Forms.Form
    Public FLed As CtsControls.Led
    Public Fqualit As CtsControls.FqualImg
    Public BIdentificativo As Boolean
    'Public report As ReportPrinting.ReportDocument

    ' indicano le versioni di firmware dell ordine corrente
    Public Ver_firm1 As String
    Public Ver_firm2 As String
    Public Ver_firm3 As String
    Public Ver_firm4 As String
    Public Ver_firm5 As String
    Public Ver_firm6 As String
    Public Ver_firm7 As String
    Public Ver_firm8 As String
    Public Ver_firm9 As String

    ' indicano la configurazione della periferica
    Public fTestinaMicr As Boolean
    Public fTimbri As Boolean
    Public fScannerFronte As Boolean
    Public fScannerRetro As Boolean
    Public fZebrato As Boolean
    Public fAtmConfiguration As Boolean
    Public fStampante As Boolean
    Public fBadge As Integer ' 0 1 2 3 
    Public fCondensat As Boolean
    Public fCard As Boolean
    Public fSmartCardPresent As Boolean
    Public fDoubleLeafingPresente As Boolean
    Public MatricolaPiastra As String
    Public SerialNumberPiastra As String
    Public MatricolaPeriferica As String
    Public SerialNumberDaStampare As String
    Private VettoreOpzioni As Array
    Public VersioneTest As String
    Public VersioneDll_1 As String
    Public VersioneDll_0 As String
    Public fSoftware As Integer
    Public fcardCol As Integer
    Public PathMessaggi As String
    Dim TipodiPeriferica As Integer
    Dim Titolo, Messaggio, Descrizione, IdMessaggio As String
    Dim dres As DialogResult
    Dim immamsg As Image
    Dim idmsg As Integer
    'Public RigheContigueFronte As Integer
    'Public RigheContigueRetro As Integer
    'Public RigheTotaliFronte As Integer
    'Public RigheTotaliRetro As Integer
    Public fScannerType As Integer
    Public StrUsb As String

    Public Const TIPO_SCANNER_COLORI = 63
    Public Const TIPO_SCANNER_UV = 64
    Public Const TIPO_SCANNER_GRIGIO = 62

#Const COLLAUDO_PIASTRE = False

    Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
        "GetPrivateProfileStringA" (ByVal lpApplicationName As String, _
            ByVal lpKeyName As String, _
            ByVal lpDefault As String, _
            ByVal lpReturnedString As StringBuilder, _
            ByVal nSize As Integer, ByVal lpFileName As String) As Integer



    Private Sub writeDebug(ByVal x As String, ByVal nFile As String)
        'Dim path As String = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
        Dim path As String = Application.StartupPath
        Dim FILE_NAME As String = path & "\" & nFile & ".txt" '"\mydebug.txt"
        'MsgBox(FILE_NAME)
        If System.IO.File.Exists(FILE_NAME) = False Then
            System.IO.File.Create(FILE_NAME).Dispose()
        End If
        ''''''''''''''''
        If x = "Cancella File" Then
            System.IO.File.Delete(FILE_NAME)
        Else
            Dim objWriter As New System.IO.StreamWriter(FILE_NAME, True)
            objWriter.WriteLine(x)
            objWriter.Close()
        End If
    End Sub

    ''' <summary>
    ''' Read value from INI file
    ''' </summary>
    ''' <param name="section">The section of the file to look in</param>
    ''' <param name="key">The key in the section to look for</param>
    Public Function ReadValue(ByVal section As String, ByVal key As String, ByVal Path As String) As String
        Dim sb As New System.Text.StringBuilder(255)
        Dim i = GetPrivateProfileString(section, key, "Failed", sb, 255, Path)
        Return sb.ToString()
    End Function


    Public Sub PerifLs40()
        Dim sss As String
        Dim ind As Integer

        'writeDebug("PerifLs40 inizio", "PerifLs40.txt")

        ls40 = New Ls40Class.Periferica
        Hstorico = New CStorico
        Pimage = New PictureBox
        PFronte = New PictureBox
        PRetro = New PictureBox
        PImageBack = New PictureBox
        sss = Application.StartupPath()
        ind = sss.LastIndexOf("\")
        '        zzz = sss.Substring(0, ind)
        zzz = sss

        'report = New ReportPrinting.ReportDocument

        DatiFine = New DatiFineCollaudo

        BIdentificativo = False

        fTestinaMicr = False
        fTimbri = False
        fScannerFronte = False
        fScannerRetro = False
        fStampante = False
        fCondensat = False
        fCard = False
        fBadge = 0 ' 0 1 2 3 
        fDoubleLeafingPresente = False
        fZebrato = False
        fAtmConfiguration = False


        VersioneTest = VERSIONE

        VersioneDll_0 = ls40.VersioneDll_0
        VersioneDll_1 = ls40.VersioneDll_1

        ' imposto una periferica che non esiste....
        TipodiPeriferica = 0

        Titolo = ""
        Messaggio = ""
        Descrizione = ""
        IdMessaggio = ""
        ls40.strRead = ""
        StrUsb = "3531"

        fSoftware = 0

        'writeDebug("PerifLs40 end", "PerifLs40.txt")


    End Sub

    Public Sub ImpostaFlagPerConfigurazione(ByVal Vettore As Array)
        Dim ii As Short
        VettoreOpzioni = Vettore

        fTestinaMicr = False
        fTimbri = False
        fScannerFronte = False
        fScannerRetro = False
        fStampante = False
        fCondensat = False
        fCard = False
        fSmartCardPresent = False
        fBadge = 0 ' 0 1 2 3 
        'fScannerType = 63   ' Per default setto lo scanner a colori->MODIFICA per collaudo PIASTRE
        fScannerType = 62   ' Per default setto lo scanner a GRIGIO->MODIFICA per collaudo PIASTRE 16/10/2015
        fDoubleLeafingPresente = False
        fZebrato = False
        fAtmConfiguration = False
        fSoftware = 0

        'se in questa funzione arrivo da COLLAUDO PIASTRE vi e' gia' una configurazione di default caricata (vedere PiastreLS40ToolStripMenuItem_Click)

        ' Controllo le opzioni che vanno da 0 a length -2
        ' perchè in ultim posizione trovo la Periferica
        'es case 5 testina micr non presente...non la controlla
        For ii = 0 To VettoreOpzioni.Length - 2
            Select Case VettoreOpzioni(ii)
                Case 4 'DL Presente 3 DL non presente
                    fDoubleLeafingPresente = True
                Case 6 To 8
                    fTestinaMicr = True
                Case 10
                    fScannerFronte = True
                Case 11
                    fScannerRetro = True
                Case 12
                    fScannerFronte = True
                    fScannerRetro = True
                Case 14 To 16
                    fTimbri = True
                Case 18
                    fStampante = True
                Case 22
                    fBadge = 1
                Case 50
                    fBadge = 2
                Case 52
                    fBadge = 3
                Case 59 ' card badge
                    fCard = True
                Case 61 ' card badge
                    fCondensat = True
                Case 62, 63, 64, 75 '62 grigio 63 colori 64 grigio+UV 75 colore+UV 
                    fScannerType = VettoreOpzioni(ii)
                Case 67
                    fSoftware = SW_TOP_IMAGE    'PAGAMENTO ? 
                Case 68
                    fSoftware = SW_BAR2D        'NON USATO
                Case 69
                    fSoftware = SW_TOP_BAR2D    'NON USATO
                Case 70
                    fcardCol = 1
                    'AGGIUNTE 30-09-2014
                Case 74 'Aggiunto DM 26/04/21 per gestione test lettore chipcard
                    fSmartCardPresent = True
                Case 76
                    fSoftware = SW_IQA          'NON USATO
                Case 77
                    fSoftware = SW_MICROHOLE    'NON USATO
                Case 78
                    fSoftware = SW_BARCODE_MICROHOLE  'USATA PER LA DECODICA DEL BARCODE 2D - TestDataMatrix
                    '19-02-2015
                Case 79
                    fSoftware = SW_BARCODE_MICROHOLE_IQA  'NON USATO
                    fcardCol = 1
                Case 80
                    fSoftware = SW_BARCODE_MICROHOLE_IQA  'NON USATO
                Case 81
                    fScannerFronte = True
                    fScannerRetro = True
                    fZebrato = True
                Case 85
                    fAtmConfiguration = True

            End Select
        Next
        TipodiPeriferica = VettoreOpzioni(VettoreOpzioni.Length - 1)

        PathMessaggi = Application.StartupPath + LOCALDB + MESSAGGI + TipodiPeriferica.ToString + ".xml"
    End Sub

    Public Sub CaricaRes()
        Try
            Dim ii As Integer
            Dim rsxr As Resources.ResXResourceReader
            Dim a As Resources.ResourceSet


            rsxr = New Resources.ResXResourceReader(Application.StartupPath + "\img2.resx")
            a = New Resources.ResourceSet(rsxr)

            Dim en As IDictionaryEnumerator = a.GetEnumerator()

            ' Goes through the enumerator, printing out the key and value pairs.

            ' finestra di massaggio in esecuzione

            ii = 0
            While en.MoveNext()
                Select Case en.Key.ToString
                    Case "pul2.jpg"
                        VetImg(0) = en.Value
                    Case "scanf.JPG"
                        VetImg(1) = en.Value
                    Case "pul1.jpg"
                        VetImg(2) = en.Value
                    Case "dp.JPG"
                        VetImg(3) = en.Value
                    Case "scanr.JPG"
                        VetImg(4) = en.Value
                    Case "badge.JPG"
                        VetImg(5) = en.Value
                    Case "cmicr.JPG"
                        VetImg(6) = en.Value
                    Case "qualim.JPG"
                        VetImg(7) = en.Value
                    Case "micrb.JPG"
                        VetImg(8) = en.Value
                    Case "qualimr.jpg"
                        VetImg(9) = en.Value
                    Case "docu055.JPG"
                        VetImg(10) = en.Value
                End Select
                VetImg(8) = VetImg(6)
                VetImg(4) = VetImg(0)

                'VetImg(ii) = en.Value

                'ii += 1
                'Me.Refresh()
            End While
            rsxr.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.InnerException.ToString)
        End Try

    End Sub

    Public Sub Sleep(ByVal sec As Integer)
        Dim iniTime, currTime As DateTime
        iniTime = DateTime.Now
        While (True)
            currTime = DateTime.Now
            Dim diff2 As System.TimeSpan
            ' diff2 gets 55 days 4 hours and 20 minutes.
            diff2 = System.DateTime.op_Subtraction(currTime, iniTime)

            If (diff2.Seconds = sec) Then
                Return
            End If
        End While
    End Sub

    Public Sub DisposeImg()
        If Not Pimage.Image Is Nothing Then
            Pimage.Image.Dispose()
            Pimage.Image = Nothing
        End If
        If Not PFronte.Image Is Nothing Then
            PFronte.Image.Dispose()
            PFronte.Image = Nothing
        End If
        If Not PRetro.Image Is Nothing Then
            PRetro.Image.Dispose()
            PRetro.Image = Nothing
        End If
        If Not PImageBack.Image Is Nothing Then
            PImageBack.Image.Dispose()
            PImageBack.Image = Nothing
        End If
    End Sub

    Public Sub ViewError(ByVal p As Object) 'BarraFasi.PhaseProgressBarItem)
        Dim sss As String
        Dim ccostante As New Ls40Class.Costanti
        sss = ""
        sss = ccostante.GetDesc(ls40.ReturnC.ReturnCode)
        MessageboxMy(ls40.ReturnC.FunzioneChiamante + " " + ls40.ReturnC.ReturnCode.ToString + "--> " + sss, "Attenzione !!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
    End Sub

    Public Sub FaseOk()
        ' azzero allimizio test il campo note

        fase.SetState = 1
        fase.IsBlinking = True
        fase.IsSelected = True
    End Sub

    Public Sub FaseKo()
        fase.SetState = 0
        fase.IsBlinking = True
        fase.IsSelected = True

    End Sub

    Public Sub FaseEsec()
        FileTraccia.Note = ""
        fase.SetState = 2
        fase.IsBlinking = True
        fase.IsSelected = True
    End Sub

    Public Function ShowZoom(ByVal dir As String, ByVal file As String, Optional ByVal VisualizzaZomm As Boolean = True) As DialogResult
        ' visualizzo subito lo zoom dell immagine
        Dim f As New CtsControls.ZoomImage
        f.FileCorrente = file
        f.DirectoryImage = dir
        f.effettuazoom = VisualizzaZomm
        f.ShowDialog()
        Return f.Result

    End Function

    Public Function MessageboxMy(ByVal testo As String, ByVal titolo As String, ByVal bt As MessageBoxButtons, ByVal ic As MessageBoxIcon, ByVal Img As Image) As DialogResult
        'CtsControls.Message   LO USO PER LE IMMAGINI !
        ff = New CtsControls.Message
        ff.Text = titolo
        ff.testo = testo
        ff.icona = ic
        ff.pulsanti = bt
        ff.Imag.Image = Img
        'If BVisualZoom = True Then
        '    ff.Bzoom.Visible = True
        '    ff.Directory = DirZoom
        '    ff.FileCorrente = FileZoomCorrente
        'End If


        ff.ShowDialog(formpadre)
        'If ff.fine = True Then
        '    ii = Tentativi
        '    ret = False
        'End If
        formpadre.Refresh()
        Return ff.result

    End Function

    Public Function MessageboxMy2(ByVal testo As String, ByVal titolo As String, ByVal bt As MessageBoxButtons, ByVal ic As MessageBoxIcon, ByVal Img As Image, ByVal Id As String, ByVal Img2 As Image) As DialogResult
        'CtsControls.Message2   LO USO SOLO PER IL TESTO !
        ff2 = New CtsControls.Message2
        ff2.Text = titolo
        ff2.testo = testo
        ff2.icona = ic
        ff2.pulsanti = bt
        ff2.Imag.Image = Img
        ff2.LId.Text = Id
        ff2.PUser.Image = Img2

        'If BVisualZoom = True Then
        '    ff.Bzoom.Visible = True
        '    ff.Directory = DirZoom
        '    ff.FileCorrente = FileZoomCorrente
        'End If


        ff2.ShowDialog(formpadre)
        'If ff.fine = True Then
        '    ii = Tentativi
        '    ret = False
        'End If
        formpadre.Refresh()
        Return ff2.result

    End Function

    Public Function InputboxMy(ByVal testo As String, ByVal titolo As String, ByVal Check As Boolean, ByRef valore As Integer) As DialogResult
        Dim ff As New CtsControls.InputBox
        ff.Check = Check
        ff.Text = titolo
        ff.Messaggio = testo
        ff.ShowDialog(formpadre)
        valore = ff.Valore

        Return ff.result
    End Function

    Public Function InputboxMyString(ByVal testo As String, ByVal titolo As String, ByVal Check As Boolean, ByRef strvalore As String) As DialogResult
        Dim ff As New CtsControls.InputStringForm
        ff.Check = Check
        ff.Text = titolo
        ff.Messaggio = testo
        ff.ShowDialog(formpadre)
        strvalore = ff.InputValore

        Return ff.result
    End Function

    Public Function ControllaVersione() As Integer
        Dim ret As Integer

        'Dim ch As Char()
        'Dim ch2 As Char()www.pani
        ret = 0


        'controllo che la versione sia uguale a quella scritta nell ordine
        ' ch = ls150.SBoardNr.ToCharArray
        'ch2 = ord.Board.ToCharArray

        If (ls40.SVersion.Length <= 0 Or String.Compare(ls40.SVersion.TrimEnd, Ver_firm1) = 0) Then
            If (ls40.SDateFW.Length <= 0 Or String.Compare(ls40.SDateFW.TrimEnd, Ver_firm2) = 0) Then

                If (ls40.SBoardNr = Ver_firm3) Then
                    If (ls40.SFpga.Length <= 0 Or String.Compare(ls40.SFpga.TrimEnd, Ver_firm4) = 0) Then
                        If fStampante = False Then
                            ret = 1
                        Else
                            If (String.Compare(ls40.SStrStampa.TrimEnd, Ver_firm5) = 0) And Ver_firm5 <> "" Then
                                ret = 1
                            Else
                                'errore InkJet
                                ret = Ls40Class.Costanti.S_LS40_INK__NOT_CONFIGURED
                                idmsg = 45
                                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                    Messaggio = Messaggio + "   " + String.Format("Richiesto : {0}  Trovato: {1}", Ver_firm5, ls40.SStrStampa)
                                    dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                                Else
                                    dres = MessageboxMy("Versione InkJet non corrispondente", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                End If

                            End If
                        End If
                    Else
                        'errore fpga
                        ret = Ls40Class.Costanti.S_LS40_FPGA_NOT_OK
                        idmsg = 150
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            Messaggio = Messaggio + "   " + String.Format("Richiesto : {0}  Trovato: {1}", Ver_firm4, ls40.SFpga)
                            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("Versione FPGA non corrispondente", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                        End If


                    End If
                Else
                    ' errore board
                    ret = Ls40Class.Costanti.S_LS40_BOARD_NOT_OK
                    idmsg = 151
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        Messaggio = Messaggio + "   " + String.Format("Richiesto : {0}  Trovato: {1}", Ver_firm3, ls40.SBoardNr)
                        dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy("Versione BOARD non corrispondente", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                    End If
                End If
            Else
                ' errore data
                ret = Ls40Class.Costanti.S_LS40_DATA_NOT_OK
                idmsg = 152
                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                    Messaggio = Messaggio + "   " + String.Format("Richiesto : {0}  Trovato: {1}", Ver_firm2, ls40.SDateFW)
                    dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                Else
                    dres = MessageboxMy("Data Versione non corrispondente", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                End If


            End If
        Else
            ' errore versione
            ret = Ls40Class.Costanti.S_LS40_FIRMWARE_NOT_OK
            idmsg = 153
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                Messaggio = Messaggio + "   " + String.Format("Richiesto : {0}  Trovato: {1}", Ver_firm1, ls40.SVersion)
                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Versione Firmware non corrispondente", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
            End If
        End If

        Return ret
    End Function

    Public Function TestIdentificativo() As Integer
        Dim ritorno As Integer
        Dim ret As Integer
        ' inizializzazioni variabili 

        FaseEsec()
        ritorno = False



        'idmsg = 191
        'If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
        '    dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        'Else
        '    dres = MessageboxMy("Collegare ORA la periferica da Collaudare", "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
        'End If

        'chiamata a funzione 
        ret = ls40.Identificativo()
        BIdentificativo = True
        MatricolaPiastra = ls40.SSerialNumber
        FileTraccia.VersioneFw = ls40.SVersion

        If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
            ' Lidentif.Text = ls150.SLsName + " " + ls150.SVersion + vbCrLf




            If ls40.SIdentStr.Length >= 2 Then

            End If
            ritorno = True
        Else
            ' errore da richiesta identificativo
            'MessageboxMy(ls40.ReturnC.FunzioneChiamante, "Attenzione !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
            ' errore da richiesta identificativo
            idmsg = 300
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("MANCANZA COMUNICAZIONE PERIFERICA", "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
            End If

            ritorno = False

        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ls40.ReturnC.ReturnCode
        Return ritorno
    End Function

    Public Function TestIdentificativoPiastra() As Integer
        Dim ritorno As Integer
        Dim ret As Integer
        Dim StrVersione As String
        ' inizializzazioni variabili 

        FaseEsec()
        ritorno = False
        'chiamata a funzione 
        ret = ls40.Identificativo()
        BIdentificativo = True
        MatricolaPiastra = ls40.SSerialNumber
        FileTraccia.VersioneFw = ls40.SVersion

        If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
            ' Lidentif.Text = ls150.SLsName + " " + ls150.SVersion + vbCrLf
            If ls40.SIdentStr.Length >= 2 Then
                ls40.SLsName = ls40.SLsName.Substring(0, 4)
                ls40.SVersion = ls40.SVersion.Substring(0, 4)
                StrVersione = ls40.SLsName + " " + ls40.SVersion + " " + ls40.SDateFW + "   FPGA   " + ls40.SFpga
                MessageboxMy(StrVersione, "Versione firmware", MessageBoxButtons.OK, MessageBoxIcon.None, Nothing)
            End If
            ritorno = True
        Else
            ' errore da richiesta identificativo
            MessageboxMy(ls40.ReturnC.FunzioneChiamante, "Attenzione !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)

        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ls40.ReturnC.ReturnCode
        Return ritorno
    End Function

    Public Function TestCheckVersion() As Integer
        Dim ritorno As Integer
        Dim ret As Integer

        ritorno = False
        FaseEsec()

        If BIdentificativo = True Then
            If ls40.SSerialNumber.Contains("OK") = True Then
                ' controllo che la versione sia quella dell ordine
                ret = ControllaVersione()
                If ret = 1 Then
                    ritorno = True

                    FileTraccia.Note = ""

                    FileTraccia.Note = "FwBase =" + Ver_firm1.ToString() + ", DataFW =" + Ver_firm2.ToString() + vbCrLf
                    FileTraccia.Note += "Board=" + Ver_firm3.ToString() + ", "
                    FileTraccia.Note += "Fpga =" + Ver_firm4.ToString + vbCrLf
                    If (Ver_firm5 <> "") Then
                        FileTraccia.Note += "InkJet =" + Ver_firm5.ToString() + vbCrLf
                    End If
                    If (Ver_firm9 <> "") Then
                        FileTraccia.Note += "Suite =" + Ver_firm9.ToString() + vbCrLf
                    End If
                Else
                    ritorno = False
                End If
            Else
                ' piastra NON Collaudata
                idmsg = 149
                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                    dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                Else
                    dres = MessageboxMy("Attenzione, possibile Piastra NON collaudata", "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                End If


                ritorno = False
            End If
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function

    Public Function TestSerialNumber() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim fser As New CtsControls.FSerialnum
        ritorno = False
        FaseEsec()



        ls40.ReturnC.FunzioneChiamante = "TestSerialNumber"


        idmsg = 184
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            fser.Text = Messaggio + SerialNumberDaStampare
        Else
            fser.Text = "Inserire SN: " + SerialNumberDaStampare
        End If


        If fser.ShowDialog() = DialogResult.OK Then


            If fser.StrSerialNumber.Length = LUNGHEZZA_SERIAL_NUMBER Then
                If fser.StrSerialNumber = SerialNumberDaStampare Then
                    MatricolaPeriferica = fser.StrSerialNumber
                    ls40.SSerialNumber = MatricolaPeriferica
                    FileTraccia.Matricola = MatricolaPeriferica
                    ret = ls40.SerialNumber()
                    If ret = Ls40Class.Costanti.S_LS_OKAY Then
                        FileTraccia.Matricola = MatricolaPeriferica
                        SalvaDati()
                        ritorno = True
                    Else
                        ViewError(fase)
                    End If
                Else
                    idmsg = 185
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        MessageboxMy("SerialNumber non corrispondente a quello inserito a inizio collaudo.", "Attenzione...", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                    End If
                    ret = Ls40Class.Costanti.S_LS_STRINGTRUNCATED

                End If

            Else
                ret = Ls40Class.Costanti.S_LS_STRINGTRUNCATED
                ViewError(fase)
            End If



        Else
            ViewError(fase)
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestSerialNumberPerPiastra() As Integer
        Dim ritorno As Integer
        Dim ret As Short

        ritorno = False
        FaseEsec()

        ls40.ReturnC.FunzioneChiamante = "TestSerialNumber"


        Dim datestr As String
        datestr = Date.Now().Day.ToString("00") + Date.Now().Month.ToString("00") + Date.Now().Year.ToString("0000") + "OK"


        MatricolaPeriferica = datestr
        ls40.SSerialNumber = MatricolaPeriferica
        FileTraccia.Matricola = MatricolaPeriferica
        ret = ls40.SerialNumber()
        If ret = Ls40Class.Costanti.S_LS_OKAY Then
            FileTraccia.Matricola = MatricolaPeriferica
            ritorno = True
        Else
            ViewError(fase)
        End If







        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestCancellaStorico(ByVal Veloci As Short) As Integer
        Dim ritorno As Integer
        Dim ret As Short
        ritorno = False
        FaseEsec()
        ret = ls40.EraseHistory()

        Hstorico.Doc_Timbrati = ls40.S_Storico.Doc_Timbrati
        Hstorico.Doc_Timbrati_retro = ls40.S_Storico.Doc_Timbrati_retro
        Hstorico.Documenti_Catturati = ls40.S_Storico.Documenti_Catturati
        Hstorico.Documenti_Ingresso = ls40.S_Storico.Documenti_Ingresso
        Hstorico.Documenti_Trattati = ls40.S_Storico.Documenti_Trattati
        Hstorico.Documenti_Trattenuti = ls40.S_Storico.Documenti_Catturati
        Hstorico.Doppia_Sfogliatura = ls40.S_Storico.Doppia_Sfogliatura
        Hstorico.Errori_Barcode = ls40.S_Storico.Errori_Barcode
        Hstorico.Errori_CMC7 = ls40.S_Storico.Errori_CMC7
        Hstorico.Errori_E13B = ls40.S_Storico.Errori_E13B
        Hstorico.Errori_Ottici = ls40.S_Storico.Errori_Ottici
        Hstorico.Jam_Allinea = ls40.S_Storico.Jam_Allinea
        Hstorico.Jam_Card = ls40.S_Storico.Jam_Card
        Hstorico.Jam_Cassetto1 = ls40.S_Storico.Jam_Cassetto1
        Hstorico.Jam_Cassetto2 = ls40.S_Storico.Jam_Cassetto2
        Hstorico.Jam_Cassetto3 = ls40.S_Storico.Jam_Cassetto3
        Hstorico.Jam_Feeder = ls40.S_Storico.Jam_Feeder
        Hstorico.Jam_Feeder_Ret = ls40.S_Storico.Jam_Feeder_Ret
        Hstorico.Jam_Micr = ls40.S_Storico.Jam_Micr
        Hstorico.Jam_Micr_Ret = ls40.S_Storico.Jam_Micr_Ret
        Hstorico.Jam_Percorso_DX = ls40.S_Storico.Jam_Percorso_DX
        Hstorico.Jam_Percorso_SX = ls40.S_Storico.Jam_Percorso_SX
        Hstorico.Jam_Print = ls40.S_Storico.Jam_Print
        Hstorico.Jam_Print_Ret = ls40.S_Storico.Jam_Print_Ret
        Hstorico.Jam_Scanner = ls40.S_Storico.Jam_Scanner
        Hstorico.Jam_Scanner_Ret = ls40.S_Storico.Jam_Scanner_Ret
        Hstorico.Jam_Sorter = ls40.S_Storico.Jam_Sorter
        Hstorico.Jam_Stamp = ls40.S_Storico.Jam_Stamp
        Hstorico.Num_Accensioni = ls40.S_Storico.Num_Accensioni
        Hstorico.TempoAccensione = ls40.S_Storico.TempoAccensione
        Hstorico.Trattenuti_Micr = ls40.S_Storico.Trattenuti_Micr
        Hstorico.Trattenuti_Scan = ls40.S_Storico.Trattenuti_Scan



        If ret = Ls40Class.Costanti.S_LS_OKAY Then
            ritorno = True
        Else

        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function SetConfigurazione() As Integer
        Dim ritorno As Integer
        Dim ret As Short

        Dim TestinaMicr As Integer
        Dim Timbri As Integer
        Dim ScannerFronte As Integer
        Dim ScannerRetro As Integer
        Dim Stampante As Integer
        Dim SensoreDoubleLeafing As Integer
        Dim ATMMode As Integer

        Dim Card As Integer
        Dim Condensatori As Integer
        Dim Badge1 As Integer ' 0 1 2 3 
        Dim Badge2 As Integer ' 0 1 2 3 
        Dim Badge3 As Integer ' 0 1 2 3 

        ritorno = False
        FaseEsec()
        Badge1 = Badge2 = Badge3 = 0

        If fTestinaMicr = True Then
            TestinaMicr = 1
        Else
            TestinaMicr = 0
        End If
        If fTimbri = True Then
            Timbri = 1
        Else
            Timbri = 0
        End If
        'scanner 
        If fScannerFronte = True Then
            ScannerFronte = 1
        Else
            ScannerFronte = 0
        End If
        If fScannerRetro = True Then
            ScannerRetro = 1
        Else
            ScannerRetro = 0
        End If
        'stampante
        If fStampante = True Then
            Stampante = 1
        Else
            Stampante = 0
        End If
        'card
        If fCard = True Then
            Card = 1
        Else
            Card = 0
        End If

        If fDoubleLeafingPresente = True Then
            SensoreDoubleLeafing = 1
        Else
            SensoreDoubleLeafing = 0
        End If

        If fAtmConfiguration = True Then
            ATMMode = 1
        Else
            ATMMode = 0
        End If

        'Card = 0

        'condensatori
        If fCondensat = True Then
            Condensatori = 1
        Else
            Condensatori = 0
        End If

        Select Case fBadge
            Case 1
                Badge1 = 1
                Badge2 = 0
                Badge3 = 0
            Case 2
                Badge1 = 0
                Badge2 = 1
                Badge3 = 0
            Case 3
                Badge1 = 0
                Badge2 = 0
                Badge3 = 1
        End Select

        'configurazione gia' fatta in precedenza per il download del FW
        If (fAtmConfiguration = True) Then
            ret = Ls40Class.Costanti.S_LS_OKAY
        Else
            ret = ls40.SetConfigurazione(TestinaMicr, TestinaMicr, Timbri, Badge1, Badge2, Badge3, ScannerFronte, ScannerRetro, Stampante, Card, Condensatori, fSoftware, fScannerType, SensoreDoubleLeafing, ATMMode)
        End If
        If ret = Ls40Class.Costanti.S_LS_OKAY Then
            ritorno = True
        Else
            ' imposto gli errori
            FileTraccia.UltimoRitornoFunzione = ret
            ViewError(fase)
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function SetConfigurazioneMyTeller() As Integer
        Dim ritorno As Integer
        Dim ret As Short

        Dim TestinaMicr As Integer
        Dim Timbri As Integer
        Dim ScannerFronte As Integer
        Dim ScannerRetro As Integer
        Dim Stampante As Integer
        Dim SensoreDoubleLeafing As Integer
        Dim ATMMode As Integer

        Dim Card As Integer
        Dim Condensatori As Integer
        Dim Badge1 As Integer ' 0 1 2 3 
        Dim Badge2 As Integer ' 0 1 2 3 
        Dim Badge3 As Integer ' 0 1 2 3 

        ritorno = False
        FaseEsec()
        Badge1 = Badge2 = Badge3 = 0

        If fTestinaMicr = True Then
            TestinaMicr = 1
        Else
            TestinaMicr = 0
        End If
        If fTimbri = True Then
            Timbri = 1
        Else
            Timbri = 0
        End If
        'scanner 
        If fScannerFronte = True Then
            ScannerFronte = 1
        Else
            ScannerFronte = 0
        End If
        If fScannerRetro = True Then
            ScannerRetro = 1
        Else
            ScannerRetro = 0
        End If
        'stampante
        If fStampante = True Then
            Stampante = 1
        Else
            Stampante = 0
        End If
        'card
        If fCard = True Then
            Card = 1
        Else
            Card = 0
        End If

        If fDoubleLeafingPresente = True Then
            SensoreDoubleLeafing = 1
        Else
            SensoreDoubleLeafing = 0
        End If

        If fAtmConfiguration = True Then
            ATMMode = 1
        Else
            ATMMode = 0
        End If

        'Card = 0

        'condensatori
        If fCondensat = True Then
            Condensatori = 1
        Else
            Condensatori = 0
        End If

        Select Case fBadge
            Case 1
                Badge1 = 1
                Badge2 = 0
                Badge3 = 0
            Case 2
                Badge1 = 0
                Badge2 = 1
                Badge3 = 0
            Case 3
                Badge1 = 0
                Badge2 = 0
                Badge3 = 1
        End Select

        'fAtmConfiguration = True
        If (fAtmConfiguration = True) Then
            ret = ls40.SetConfigurazioneMyTeller(TestinaMicr, TestinaMicr, Timbri, Badge1, Badge2, Badge3, ScannerFronte, ScannerRetro, Stampante, Card, Condensatori, fSoftware, fScannerType, SensoreDoubleLeafing, ATMMode)
            'Call Sleep(2)
        Else
            ret = Ls40Class.Costanti.S_LS_OKAY
        End If
        If ret = Ls40Class.Costanti.S_LS_OKAY Then
            ritorno = True
        Else
            ' imposto gli errori
            FileTraccia.UltimoRitornoFunzione = ret
            ViewError(fase)
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If

        If (fAtmConfiguration = True And ritorno = True) Then
            dres = MessageboxMy("Spegnere e Riaccendere la macchina,Premere Ok solo dopo il Bip ", "ATTENZIONE !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning, Nothing)
            'aspetto che la periferica si riconnetta al sistema....
            Dim index As Integer = 0
            Do

                ret = ls40.Identificativo()
                Call Sleep(2)
                index += 1
            Loop While (ret <> Ls40Class.Costanti.S_LS_OKAY And index <= 7)
            'Call Sleep(10)


        Else
            ret = Ls40Class.Costanti.S_LS_OKAY
        End If

        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestFotoPeriferica() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim ret_not_used As Short
        Dim floop As Boolean = False
        Dim iii As Short
        'Dim strmsg As String
        Dim valore As String = New String(" "c, 255)
        'Dim fAspetta As CtsControls.FWait


        iii = 0
        ritorno = False

        FaseEsec()

        ret = ls40.Identificativo()
        If (ret = Ls40Class.Costanti.S_LS_OKAY) Then

#If COLLAUDO_PIASTRE Then
            dres = MessageboxMy("Rimuovere , se presenti , i Cappucci FOTO", "ATTENZIONE !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning, Nothing)

#Else
             idmsg = 162
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Rimuovere eventuali documenti da percorso", "ATTENZIONE !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning, Nothing)
            End If
#End If

           


            FLed = New CtsControls.Led

            If dres <> DialogResult.OK Then
                fase.SetState = 0
                fase.IsBlinking = True
                fase.IsSelected = True
                ritorno = False
                FileTraccia.UltimoRitornoFunzione = Ls40Class.Costanti.S_LS_USER_ABORT

            Else
                'If fAtmConfiguration = True Then
                '    FLed.LbLedSe3.Visible = True
                'Else

                FLed.LSe3.Text = ""
                FLed.Label1.Text = ""
                FLed.LbLedSe3.Visible = False
                FLed.LbLedSeBin.Visible = False
                'End If

                FLed.Show()
                FLed.Refresh()
                ret = ls40.CalibrazioneFoto()
                If ret = Ls40Class.Costanti.S_LS_OKAY Then
                    FLed.LSe1.Text += Ls40Class.Periferica.PhotoValue(0).ToString
                    FLed.Lse2.Text += Ls40Class.Periferica.PhotoValue(1).ToString


                    FileTraccia.Note = "Fotosensore 1: " + Ls40Class.Periferica.PhotoValue(0).ToString + vbCrLf
                    FileTraccia.Note += "Fotosensore 2: " + Ls40Class.Periferica.PhotoValue(1).ToString + vbCrLf


                    'If Ls40Class.Periferica.PhotoValue(0) = 0 Or Ls40Class.Periferica.PhotoValue(0) = 127 Then
                    If Ls40Class.Periferica.PhotoValue(0) = 0 Or Ls40Class.Periferica.PhotoValue(0) >= 101 Then '80% di 127
                        FLed.LbLedSe1.LedColor = Color.Red
                        ritorno = False
                    Else
                        FLed.LbLedSe1.LedColor = Color.Green
                        FLed.LbLedSe1.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.Blink
                        ritorno = True
                    End If

                    'If Ls40Class.Periferica.PhotoValue(1) = 0 Or Ls40Class.Periferica.PhotoValue(1) = 127 Then
                    If Ls40Class.Periferica.PhotoValue(1) = 0 Or Ls40Class.Periferica.PhotoValue(1) >= 101 Then '80% di 127
                        FLed.LbLedSe2.LedColor = Color.Red
                        ritorno = False
                    Else
                        FLed.LbLedSe2.LedColor = Color.Green
                        FLed.LbLedSe2.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.Blink
                        ritorno = True
                    End If

                    FLed.Refresh()
                    Sleep(2)
                    FLed.Close()

#If COLLAUDO_PIASTRE Then
                    dres = MessageboxMy("Inserire i Cappucci FOTO nella sede", "ATTENZIONE !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning, Nothing)
#End If

                Else
                    ritorno = False

                    FLed.Close()

                    ViewError(fase)
                End If



            End If

        Else
            ' errore da richiesta identificativo
            'MessageboxMy(ls40.ReturnC.FunzioneChiamante, "Attenzione !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
            ' errore da richiesta identificativo
            If (ret = Ls40Class.Costanti.S_LS_PAPER_JAM) Then
                ret_not_used = ls40.CalibrazioneFoto()
            Else
                idmsg = 300
                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                    dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                Else
                    dres = MessageboxMy("MANCANZA COMUNICAZIONE PERIFERICA", "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                End If

            End If
            
            ritorno = False

        End If



        If ritorno = True Then
            FaseOk()
            FileTraccia.FotoPersorso = 1
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestFotoPerifericaPiastre() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim ret_not_used As Short
        Dim floop As Boolean = False
        Dim iii As Short
        'Dim strmsg As String
        Dim valore As String = New String(" "c, 255)
        'Dim fAspetta As CtsControls.FWait


        iii = 0
        ritorno = False

        FaseEsec()

        ret = ls40.Identificativo()
        If (ret = Ls40Class.Costanti.S_LS_OKAY) Then


            dres = MessageboxMy("Rimuovere , se presenti , i Cappucci FOTO", "ATTENZIONE !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning, Nothing)

            FLed = New CtsControls.Led

            If dres <> DialogResult.OK Then
                fase.SetState = 0
                fase.IsBlinking = True
                fase.IsSelected = True
                ritorno = False
                FileTraccia.UltimoRitornoFunzione = Ls40Class.Costanti.S_LS_USER_ABORT

            Else
                'If fAtmConfiguration = True Then
                '    FLed.LbLedSe3.Visible = True
                'Else

                FLed.LSe3.Text = ""
                FLed.Label1.Text = ""
                FLed.LbLedSe3.Visible = False
                FLed.LbLedSeBin.Visible = False
                'End If

                FLed.Show()
                FLed.Refresh()
                ret = ls40.CalibrazioneFoto()
                If ret = Ls40Class.Costanti.S_LS_OKAY Then
                    FLed.LSe1.Text += Ls40Class.Periferica.PhotoValue(0).ToString
                    FLed.Lse2.Text += Ls40Class.Periferica.PhotoValue(1).ToString


                    FileTraccia.Note = "Fotosensore 1: " + Ls40Class.Periferica.PhotoValue(0).ToString + vbCrLf
                    FileTraccia.Note += "Fotosensore 2: " + Ls40Class.Periferica.PhotoValue(1).ToString + vbCrLf


                    'If Ls40Class.Periferica.PhotoValue(0) = 0 Or Ls40Class.Periferica.PhotoValue(0) = 127 Then
                    If Ls40Class.Periferica.PhotoValue(0) = 0 Or Ls40Class.Periferica.PhotoValue(0) >= 101 Then '80% di 127
                        FLed.LbLedSe1.LedColor = Color.Red
                        ritorno = False
                    Else
                        FLed.LbLedSe1.LedColor = Color.Green
                        FLed.LbLedSe1.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.Blink
                        ritorno = True
                    End If

                    'If Ls40Class.Periferica.PhotoValue(1) = 0 Or Ls40Class.Periferica.PhotoValue(1) = 127 Then
                    If Ls40Class.Periferica.PhotoValue(1) = 0 Or Ls40Class.Periferica.PhotoValue(1) >= 101 Then '80% di 127
                        FLed.LbLedSe2.LedColor = Color.Red
                        ritorno = False
                    Else
                        FLed.LbLedSe2.LedColor = Color.Green
                        FLed.LbLedSe2.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.Blink
                        ritorno = True
                    End If

                    FLed.Refresh()
                    Sleep(2)
                    FLed.Close()

                    dres = MessageboxMy("Inserire i Cappucci FOTO nella sede", "ATTENZIONE !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning, Nothing)

                Else
                    ritorno = False

                    FLed.Close()

                    ViewError(fase)
                End If



            End If

        Else
            ' errore da richiesta identificativo
            'MessageboxMy(ls40.ReturnC.FunzioneChiamante, "Attenzione !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
            ' errore da richiesta identificativo
            If (ret = Ls40Class.Costanti.S_LS_PAPER_JAM) Then
                ret_not_used = ls40.CalibrazioneFoto()
            Else
                idmsg = 300
                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                    dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                Else
                    dres = MessageboxMy("MANCANZA COMUNICAZIONE PERIFERICA", "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                End If

            End If

            ritorno = False

        End If



        If ritorno = True Then
            FaseOk()
            FileTraccia.FotoPersorso = 1
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestCalibrazioneFotoPeriferica() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim floop As Boolean = False
        Dim iii As Short
        'Dim strmsg As String
        Dim valore As String = New String(" "c, 255)
        Dim fAspetta As CtsControls.FWait
        'Dim length As Integer = 255
        'Dim value As String = New String(" "c, 255)
        'Dim length1 As Integer = 255
        'Dim value1 As String = New String(" "c, 255)
        Dim strS As String


        iii = 0
        ritorno = False
        FaseEsec()

        'fAtmConfiguration = True
        If (fAtmConfiguration = True) Then

            idmsg = 162
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Rimuovere eventuali documenti da percorso", "ATTENZIONE !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning, Nothing)
            End If

            If dres <> DialogResult.OK Then
                fase.SetState = 0
                fase.IsBlinking = True
                fase.IsSelected = True
                ritorno = False
                FileTraccia.UltimoRitornoFunzione = Ls40Class.Costanti.S_LS_USER_ABORT

            Else

                'Leggo il file Ultrasonic.ini
                ls40.NomeFileUltrasonic = Application.StartupPath + "\\Ultrasonic.ini"



                ret = Ls40Class.Costanti.S_LS_OKAY
                strS = ReadValue("DocStats", "Doc1", ls40.NomeFileUltrasonic)
                'length = GetPrivateProfileString("DocStats", "Doc1", "DOCT-R013-00", value, length, ls40.NomeFileUltrasonic)

                If (strS = "") Then
                    StrMessaggio = "Inserire nella Casella di Testo il Valore del numero di lotto del DOCT-R013-00"
                Else
                    'value = value.Substring(0, length)
                    StrMessaggio = "Inserire nella Casella di Testo il Valore del numero di lotto del " + strS
                End If


                'Inserire Lotto Documents
                'strmsg = "Test Foto Periferica  : Inserire nella Casella di Testo il Valore del numero di lotto"
                'idmsg = 283

                'If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                'StrMessaggio = Messaggio
                'Else
                'StrMessaggio = strmsg
                'End If


                If InputboxMyString(StrMessaggio, "Attenzione", True, valore) = DialogResult.OK Then
                    'Dim length As Integer = 15
                    'Dim value As String = New String(" "c, length)


                    fAspetta = New CtsControls.FWait
                    fAspetta.Label1.Text = "Attendere...Calibrazione Double Feed in corso"
                    fAspetta.Show()

                    'length = 255
                    'value = New String(" "c, 255)
                    strS = ReadValue("Files", "FileSPT", ls40.NomeFileUltrasonic)
                    'length = GetPrivateProfileString("Files", "FileSPT", "14371-001.spt", value, length, ls40.NomeFileUltrasonic)
                    'value = value.Substring(0, length)
                    If (strS = "") Then
                        ls40.NomeFileSPT = Application.StartupPath + "\\SPT\\" + "14371-001.spt"

                    Else
                        ls40.NomeFileSPT = Application.StartupPath + "\\SPT\\" + strS
                    End If
                    ret = ls40.InkDetectorLoadCmds()


                    'System.Convert.ToInt32(valore)
                    ret = ls40.DoCalibrationUltrasonic(valore, 1)



                    If (ret = Ls40Class.Costanti.S_LS_OKAY) Then

                        'length = GetPrivateProfileString("DocStats", "Doc1", "DOCT-R013-00", value, length, ls40.NomeFileUltrasonic)
                        strS = ReadValue("DocStats", "Doc1", ls40.NomeFileUltrasonic)
                        If (strS = "") Then
                            dres = MessageboxMy("Inserire il DOCT-R013-00 nel feeder", "ATTENZIONE !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning, Nothing)
                        Else
                            'value = value.Substring(0, length)
                            StrMessaggio = "Inserire il" + strS + "nel feeder"
                            dres = MessageboxMy(StrMessaggio, "ATTENZIONE !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning, Nothing)
                        End If


                        If dres <> DialogResult.OK Then
                            ritorno = False
                            FileTraccia.UltimoRitornoFunzione = Ls40Class.Costanti.S_LS_USER_ABORT
                        Else
                            ret = ls40.DoCalibrationUltrasonic(valore, 2)
                            If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
                                ritorno = True
                            Else
                                ls40.ReturnC.ReturnCode = ret
                                ViewError(fase)
                            End If


                        End If
                    Else
                        ls40.ReturnC.ReturnCode = ret
                        ViewError(fase)
                    End If

                    fAspetta.Hide()

                End If
            End If
        Else
            ritorno = True
        End If


        If ritorno = True Then
            FaseOk()
            FileTraccia.FotoPersorso = 1
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestDownloadFwDoubleFeed() As Integer
        Dim ritorno As Short
        Dim ret As Short

        ritorno = False

        FaseEsec()

        ' Dim length As Integer = 255
        'Dim value As String = New String(" "c, length)
        Dim strS As String

        If (fAtmConfiguration = True) Then
            'Leggo il file Ultrasonic.ini
            ls40.NomeFileUltrasonic = Application.StartupPath + "\Ultrasonic.ini"

            'Lettura File di Download...
            'GetPrivateProfileString("Files", "FileDoubleFeed", "DS013_00.dwl", value, length, ls40.NomeFileUltrasonic)
            strS = ReadValue("Files", "FileDoubleFeed", ls40.NomeFileUltrasonic)
            If (strS = "") Then
                ls40.FileNameDownload = Application.StartupPath + "\FW\" + "DS013_01.dwl"
                'ret = Ls40Class.Costanti.S_LS_OKAY
            Else
                ls40.FileNameDownload = Application.StartupPath + "\FW\" + strS
            End If

            Dim fAspetta As CtsControls.FWait
            fAspetta = New CtsControls.FWait
            fAspetta.Label1.Text = "Attendere...Download Fw Double Feed in corso"
            fAspetta.Show()
            Application.DoEvents()

            'Download Fw Double Feed 
            ret = ls40.TestDownload()
            fAspetta.Hide()
            'la macchina si deve riavviare 
            Sleep(8)





            If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
                ritorno = True
            Else
                ls40.ReturnC.ReturnCode = ret
                ViewError(fase)
            End If
        Else
            ritorno = True
        End If


        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno

    End Function

    Public Function TestScannersPeriferica() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim fAspetta As CtsControls.FWait

        ritorno = False
        FaseEsec()

        'se grigio solo docutest055
        'se colore solo docutest055
        'se uv serve il docutest069 + docutest055 



        If (fScannerType = TIPO_SCANNER_UV) Then
            idmsg = 263
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Inserire il DOCUTEST --069 per la calibrazione Scanner UV. !", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(10))
            End If

            ' faccio la calibrazione scanner fronte
            If (dres <> DialogResult.OK) Then
                ret = Ls40Class.Costanti.S_LS_USER_ABORT
                FaseKo()
                FileTraccia.UltimoRitornoFunzione = ret 'replycode
                FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
                ritorno = False
                Return ritorno 'OCCHIO !!!!!!!!!!!!
                'fWaitButton = True
            Else

                'fquality = False
                fAspetta = New CtsControls.FWait
                fAspetta.Label1.Text = "Attendere...Calibrazione Scanner in corso"
                fAspetta.Show()
                ret = ls40.CalibraScanner(Ls40Class.Costanti.SCANNER_UV)
                fAspetta.Hide()
                If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
                Else
                    ViewError(fase)
                    FileTraccia.UltimoRitornoFunzione = ret 'replycode
                    FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
                    ritorno = False
                    ls40.Reset(Ls40Class.Costanti.S_RESET_FREE_PATH)
                    Return ritorno 'OCCHIO !!!!!!!!!!!!
                End If
            End If
        End If


        If (fAtmConfiguration = True) Then 'MYT2
            idmsg = 284
        Else
            idmsg = 164
        End If

        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            If (fAtmConfiguration = True) Then 'MYT2
                dres = MessageboxMy("Inserire il DOCT-R111 per la calibrazione Scanner. !", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(10))
            Else
                dres = MessageboxMy("Inserire il DOCUTEST --055 per la calibrazione Scanner. !", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(10))
            End If
        End If


        ' faccio la calibrazione scanner fronte
        If (dres <> DialogResult.OK) Then
            ret = Ls40Class.Costanti.S_LS_USER_ABORT
            FaseKo()
            FileTraccia.UltimoRitornoFunzione = ret 'replycode
            FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
            ritorno = False
            Return ritorno 'OCCHIO !!!!!!!!!!!!
            'fWaitButton = True
        Else


            'fquality = False
            fAspetta = New CtsControls.FWait
            fAspetta.Label1.Text = "Attendere...Calibrazione Scanner in corso"
            fAspetta.Show()

            If (fScannerType = TIPO_SCANNER_COLORI) Then
                ret = ls40.CalibraScanner(Ls40Class.Costanti.SCANNER_COLORE)
            ElseIf (fScannerType = TIPO_SCANNER_UV) Then
                ret = ls40.CalibraScanner(Ls40Class.Costanti.SCANNER_UVGR)
            Else
                ret = ls40.CalibraScanner(Ls40Class.Costanti.SCANNER_FRONT)
            End If

            fAspetta.Hide()

            If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
            Else
                ViewError(fase)
                ls40.Reset(Ls40Class.Costanti.S_RESET_FREE_PATH)
                FileTraccia.UltimoRitornoFunzione = ret 'replycode
                FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
                ritorno = False
                Return ritorno 'OCCHIO !!!!!!!!!!!!
            End If
        End If

        If (fScannerType = TIPO_SCANNER_UV) Then
            idmsg = 294
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Inserire Docutest-069(ricalcola i coefficienti UV con luce gray)", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(10))
            End If

            If (dres <> DialogResult.OK) Then
                ret = Ls40Class.Costanti.S_LS_USER_ABORT
                FaseKo()
            Else
                fAspetta = New CtsControls.FWait
                fAspetta.Label1.Text = "Attendere...Ricalcolo coefficienti UV in corso"
                fAspetta.Show()
                ret = ls40.TestScannerUvCalibrationReExecute()
                fAspetta.Hide()
                If (ret = Ls40Class.Costanti.S_LS_OKAY) Then

                Else
                    ViewError(fase)
                    ls40.Reset(Ls40Class.Costanti.S_RESET_FREE_PATH)
                End If
            End If
        End If

        FileTraccia.UltimoRitornoFunzione = ret
        If ret = Ls40Class.Costanti.S_LS_OKAY Then
            ritorno = True
        Else
            ritorno = False
        End If

        If ritorno = True Then

            FaseOk()
            FileTraccia.ScannerF = 1
        End If
        'BVisualZoom = False
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestScannersPerifericaPiastre() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim fAspetta As CtsControls.FWait

        ritorno = False
        FaseEsec()

        'se grigio solo docutest055
        'se colore solo docutest055
        'se uv serve il docutest069 + docutest055 



        If (fScannerType = TIPO_SCANNER_UV) Then
            idmsg = 263
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Inserire il DOCUTEST --069 per la calibrazione Scanner UV. !", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(10))
            End If

            ' faccio la calibrazione scanner fronte
            If (dres <> DialogResult.OK) Then
                ret = Ls40Class.Costanti.S_LS_USER_ABORT
                FaseKo()
                FileTraccia.UltimoRitornoFunzione = ret 'replycode
                FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
                ritorno = False
                Return ritorno 'OCCHIO !!!!!!!!!!!!
                'fWaitButton = True
            Else

                'fquality = False
                fAspetta = New CtsControls.FWait
                fAspetta.Label1.Text = "Attendere...Calibrazione Scanner in corso"
                fAspetta.Show()
                ret = ls40.CalibraScanner(Ls40Class.Costanti.SCANNER_UV)
                fAspetta.Hide()
                If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
                Else
                    ViewError(fase)
                    FileTraccia.UltimoRitornoFunzione = ret 'replycode
                    FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
                    ritorno = False
                    ls40.Reset(Ls40Class.Costanti.S_RESET_FREE_PATH)
                    Return ritorno 'OCCHIO !!!!!!!!!!!!
                End If
            End If
        End If


        If (fAtmConfiguration = True) Then 'MYT2
            idmsg = 284
        Else
            idmsg = 164
        End If

        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            If (fAtmConfiguration = True) Then 'MYT2
                dres = MessageboxMy("Inserire il DOCT-R111 per la calibrazione Scanner. !", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(10))
            Else
                dres = MessageboxMy("Inserire il DOCUTEST --055 per la calibrazione Scanner. !", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(10))
            End If
        End If


        ' faccio la calibrazione scanner fronte
        If (dres <> DialogResult.OK) Then
            ret = Ls40Class.Costanti.S_LS_USER_ABORT
            FaseKo()
            FileTraccia.UltimoRitornoFunzione = ret 'replycode
            FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
            ritorno = False
            Return ritorno 'OCCHIO !!!!!!!!!!!!
            'fWaitButton = True
        Else


            'fquality = False
            fAspetta = New CtsControls.FWait
            fAspetta.Label1.Text = "Attendere...Calibrazione Scanner in corso"
            fAspetta.Show()

            If (fScannerType = TIPO_SCANNER_COLORI) Then
                ret = ls40.CalibraScanner(Ls40Class.Costanti.SCANNER_COLORE)
            ElseIf (fScannerType = TIPO_SCANNER_UV) Then
                ret = ls40.CalibraScanner(Ls40Class.Costanti.SCANNER_UVGR)
            Else
                ret = ls40.CalibraScanner(Ls40Class.Costanti.SCANNER_FRONT)
            End If

            fAspetta.Hide()

            If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
            Else
                ViewError(fase)
                ls40.Reset(Ls40Class.Costanti.S_RESET_FREE_PATH)
                FileTraccia.UltimoRitornoFunzione = ret 'replycode
                FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
                ritorno = False
                Return ritorno 'OCCHIO !!!!!!!!!!!!
            End If
        End If

        'Tolto secondo giro con docutest-069 su richiesta di Alasotto F
        'If (fScannerType = TIPO_SCANNER_UV) Then
        '    idmsg = 294
        '    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
        '        dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        '    Else
        '        dres = MessageboxMy("Inserire Docutest-069(ricalcola i coefficienti UV con luce gray)", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(10))
        '    End If

        '    If (dres <> DialogResult.OK) Then
        '        ret = Ls40Class.Costanti.S_LS_USER_ABORT
        '        FaseKo()
        '    Else
        '        fAspetta = New CtsControls.FWait
        '        fAspetta.Label1.Text = "Attendere...Ricalcolo coefficienti UV in corso"
        '        fAspetta.Show()
        '        ret = ls40.TestScannerUvCalibrationReExecute()
        '        fAspetta.Hide()
        '        If (ret = Ls40Class.Costanti.S_LS_OKAY) Then

        '        Else
        '            ViewError(fase)
        '            ls40.Reset(Ls40Class.Costanti.S_RESET_FREE_PATH)
        '        End If
        '    End If
        'End If

        FileTraccia.UltimoRitornoFunzione = ret
        If ret = Ls40Class.Costanti.S_LS_OKAY Then
            ritorno = True
        Else
            ritorno = False
        End If

        If ritorno = True Then

            FaseOk()
            FileTraccia.ScannerF = 1
        End If
        'BVisualZoom = False
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestStampa(ByVal ndoc As Short) As Integer
        Dim ritorno As Integer
        Dim ret As Integer
        Dim strapp As String
        Dim iii As Short
        Dim fcicla As Boolean

        ritorno = False

        FaseEsec()

        ls40.NomeFileImmagine = zzz + "\Dati\StampaFImage" + IndImgF.ToString("0000") + ".bmp"
        ls40.NomeFileImmagineRetro = zzz + "\Dati\StampaRImage" + IndImgR.ToString("0000") + ".bmp"
        IndImgF += 1
        IndImgR += 1

        ls40.Stampafinale = "Piastra: " + SerialNumberPiastra + " SN: " + SerialNumberDaStampare + " " + ls40.SLsName + " " + ls40.SVersion + " FPGA: " + ls40.SFpga


        idmsg = 314 'Inserire un DOCUTEST -059 per la prova di stampa
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire un documento per la prova di stampa", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(3))
        End If


        If (dres <> DialogResult.OK) Then
            ret = Ls40Class.Costanti.S_LS_USER_ABORT
        Else
            fase.IsSelected = False
            iii = 0
            fcicla = True
            While (fcicla)
                If (iii > ndoc - 1) Then
                    idmsg = 183
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy("Inserire uno dei documenti timbrati per l'ultima prova di stampa", TITOLO_DEF_MESSAGGIO, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(3))
                    End If
                End If
                ls40.NomeFileImmagine = zzz + "\Dati\StampaFImage" + IndImgF.ToString("0000") + ".bmp"
                ls40.NomeFileImmagineRetro = zzz + "\Dati\StampaRImage" + IndImgR.ToString("0000") + ".bmp"
                If (iii = ndoc) Then
                    ret = ls40.TestStampa(1)
                Else
                    ret = ls40.TestStampa(0)
                End If

                iii += 1

                If (ret = Ls40Class.Costanti.S_LS_OKAY) Then

                    Try
                        DisposeImg()
                        Pimage.Image = System.Drawing.Image.FromFile(ls40.NomeFileImmagine)
                        PImageBack.Image = System.Drawing.Image.FromFile(ls40.NomeFileImmagineRetro)
                    Catch ex As Exception
                        ' DEBUG
                        MessageBox.Show(ex.Message + " " + ex.Source + ret.ToString, "errore")
                    End Try

                    Application.DoEvents()
                Else
                    Select Case ret
                        Case Ls40Class.Costanti.S_LS_FEEDEREMPTY
                            idmsg = 178
                            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                            Else
                                dres = MessageboxMy("Inserire Documenti per la prova di stampa", TITOLO_DEF_MESSAGGIO, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                            End If

                            If dres <> DialogResult.OK Then
                                fcicla = False
                                ret = Ls40Class.Costanti.S_LS_USER_ABORT
                            End If
                            ret = Ls40Class.Costanti.S_LS_OKAY
                            iii -= 1
                        Case Ls40Class.Costanti.S_LS_PAPER_JAM
                            ret = ls40.Reset(Ls40Class.Costanti.S_RESET_FREE_PATH)
                            'ret = Ls40Class.Costanti.S_LS_OKAY
                            iii -= 1
                        Case Else
                            ret = ls40.Reset(Ls40Class.Costanti.S_RESET_FREE_PATH)
                            'ret = Ls40Class.Costanti.S_LS_OKAY
                            iii -= 1
                    End Select
                End If

                If ret <> Ls40Class.Costanti.S_LS_OKAY Or iii > ndoc Then
                    fcicla = False
                    iii -= 1
                End If

                Application.DoEvents()
                IndImgF += 1
                IndImgR += 1
            End While
            If (ret = Ls40Class.Costanti.S_LS_OKAY) Then

                Try
                    DisposeImg()
                    Pimage.Image = System.Drawing.Image.FromFile(ls40.NomeFileImmagine)
                    PImageBack.Image = System.Drawing.Image.FromFile(ls40.NomeFileImmagineRetro)
                Catch ex As Exception
                    ' DEBUG
                    MessageBox.Show(ex.Message + " " + ex.Source + ret.ToString, "errore")
                End Try

                Application.DoEvents()

                strapp = "La stampante ha stampato bene?"
                idmsg = 179
                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                    dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                Else
                    dres = MessageboxMy(strapp, "Attenzione!!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing)
                End If


                If dres = DialogResult.Yes Then
                    fase.SetState = 1
                    fase.IsBlinking = True
                    fase.IsSelected = True
                    ritorno = True
                Else
                    ret = Ls40Class.Costanti.S_LS_USER_ABORT
                    fase.SetState = 0
                    fase.IsBlinking = True
                    fase.IsSelected = True
                End If
            Else
                ViewError(fase)
            End If
        End If

        FileTraccia.UltimoRitornoFunzione = ret

        If ritorno = True Then
            FileTraccia.Stampante = 1
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    
    Public Function TestTimbro(ByVal TipoTimbro As Short) As Integer
        Dim Ritorno As Integer
        Dim ret As Integer
        Dim fcicla As Boolean
        Dim iii As Integer
        'Dim resdialog As DialogResult
        Dim strmsg As String

        Ritorno = False
        'TCollaudo.SelectedTab = TTimbriStampa
        FaseEsec()


        strmsg = "Inserire " + (TipoTimbro + 1).ToString + " documenti per test Timbro (Fac-simile Assegno con codeline su lato destro)."

        idmsg = 313 'Inserire DOCT-R146 per test Timbro (Fac-simile Assegno con codeline su lato destro) :
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio + " " + (TipoTimbro + 1).ToString, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy(strmsg, "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(3))
        End If


        If (dres <> DialogResult.OK) Then
            ret = Ls40Class.Costanti.S_LS_USER_ABORT
        Else
            iii = 0
            fcicla = True
            While (fcicla)
                ls40.NomeFileImmagine = zzz + "\Dati\TimbroFImage" + IndImgF.ToString("0000") + ".bmp"
                ls40.NomeFileImmagineRetro = zzz + "\Dati\TimbroRImage" + IndImgR.ToString("0000") + ".bmp"
                ret = ls40.Timbra()
                iii += 1
                If ret = Ls40Class.Costanti.S_LS_OKAY Then
                    Try
                        DisposeImg()

                        Application.DoEvents()
                        Pimage.Image = System.Drawing.Image.FromFile(ls40.NomeFileImmagine)
                        PImageBack.Image = System.Drawing.Image.FromFile(ls40.NomeFileImmagineRetro)
                    Catch ex As Exception
                        ' DEBUG
                        MessageBox.Show(ex.Message + " " + ex.Source + ret.ToString, "errore")
                    End Try
                End If

                Select Case ret
                    Case Ls40Class.Costanti.S_LS_FEEDEREMPTY
                        idmsg = 180
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy2(Messaggio + " " + (TipoTimbro + 1).ToString, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy(strmsg, "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(3))
                        End If
                        If dres <> DialogResult.OK Then
                            fcicla = False
                            ret = Ls40Class.Costanti.S_LS_USER_ABORT
                        End If
                        ret = Ls40Class.Costanti.S_LS_OKAY
                        iii -= 1
                    Case Ls40Class.Costanti.S_LS_PAPER_JAM, Ls40Class.Costanti.S_LS_JAM_AT_MICR_PHOTO, Ls40Class.Costanti.S_LS_JAM_AT_SCANNERPHOTO, Ls40Class.Costanti.S_LS_JAM_DOC_TOOLONG

                        idmsg = 181
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("Togliere i documenti dal percorso carta se presenti", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                        End If

                        If dres <> DialogResult.OK Then
                            fcicla = False
                            ret = Ls40Class.Costanti.S_LS_USER_ABORT
                        End If
                        ls40.Reset(Ls40Class.Costanti.S_RESET_FREE_PATH)
                        ret = Ls40Class.Costanti.S_LS_OKAY
                        iii -= 1
                End Select

                If ret <> Ls40Class.Costanti.S_LS_OKAY Or iii > TipoTimbro Then
                    fcicla = False
                    iii -= 1
                End If

                Application.DoEvents()
                IndImgF += 1
                IndImgR += 1
            End While

            If (ret = Ls40Class.Costanti.S_LS_OKAY) Then

                idmsg = 182
                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                    dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                Else
                    dres = MessageboxMy("Il timbro ha timbrato in modo corretto ?", "Attenzione!! ", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, Nothing)
                End If



                If (dres = DialogResult.Yes) Then
                    'LTimR.ForeColor = Color.Green
                    'LTimR.Text = "OK"
                    Ritorno = True
                Else
                    If dres = DialogResult.No Then
                        ' ii = 
                    End If
                    'LTimF.ForeColor = Color.Red
                    'LTimF.Text = "KO"

                    ret = Ls40Class.Costanti.S_LS_USER_ABORT
                End If
            Else
                ViewError(fase)
            End If

        End If



        FileTraccia.UltimoRitornoFunzione = ret

        If Ritorno = True Then
            FaseOk()
            FileTraccia.TimbroFronte = 1
            FileTraccia.TimbroRetro = 1
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return Ritorno
    End Function



    Public Function TestCalibraTestina(ByVal Type As Short) As Integer
        Dim ritorno As Integer

        Dim ret As Short
        Dim RangeMore As Single
        Dim RangeLess As Single
        Dim strmsg As String
        Dim TypeSpeed As Short
        Dim valore As Integer

        Dim finfo As New CtsControls.FormInfoTest



        ritorno = False
        'GMicr.Enabled = True
        'LResult.Text = ""

        FaseEsec()

        'fWaitButton = False





        Select Case Type
            Case 0

                TypeSpeed = TEST_SPEED_300_DPI
                idmsg = 170
                strmsg = " Attenzione !!! Calibrazione 300 DPI"
                ' commentato perchè per ora c'è solo una calibrazione
            Case 1

                TypeSpeed = TEST_SPEED_300_DPI_UV
                idmsg = 170
                strmsg = " Attenzione !!! Calibrazione 300 DPI UV"

            Case 2
                TypeSpeed = TEST_SPEED_300_DPI_COLOR
                idmsg = 102
                strmsg = " Attenzione !!! Calibrazione 300 DPI COLORE"
        End Select

        'in caso di scanner a colori forzo uscita se viene chiesta la type spped UV 
        'la fase  viene sempre caricata 
        If (fScannerType = TIPO_SCANNER_COLORI And TypeSpeed = TEST_SPEED_300_DPI_UV) Then
            FaseOk()
            FileTraccia.UltimoRitornoFunzione = 0 'replycode
            FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
            ritorno = True
            FileTraccia.CalMicr = 1
            'BVisualZoom = False
            Return ritorno 'OCCHIO !!!!!!!!!!!!
        End If

        'in caso di scanner a UV  forzo uscita se viene chiesta la type spped COLORI
        'la fase  viene sempre caricata 
        If (fScannerType = TIPO_SCANNER_UV And TypeSpeed = TEST_SPEED_300_DPI_COLOR) Then
            FaseOk()
            FileTraccia.UltimoRitornoFunzione = 0 'replycode
            FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
            ritorno = True
            FileTraccia.CalMicr = 1
            'BVisualZoom = False
            Return ritorno 'OCCHIO !!!!!!!!!!!!
        End If

        If (fScannerType = TIPO_SCANNER_GRIGIO And (TypeSpeed = TEST_SPEED_300_DPI_UV Or TypeSpeed = TEST_SPEED_300_DPI_COLOR)) Then
            FaseOk()
            FileTraccia.UltimoRitornoFunzione = 0 'replycode
            FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
            ritorno = True
            FileTraccia.CalMicr = 1
            'BVisualZoom = False
            Return ritorno 'OCCHIO !!!!!!!!!!!!
        Else
            TypeSpeed = TEST_SPEED_300_DPI_UV
        End If


        finfo.PointToScreen(New Point(300, 100))
        finfo.Show()
        finfo.BringToFront()



        strmsg = "Inserire nella Casella di Testo il Valore del DOCUTEST --042 di calibrazione "
        idmsg = 12

        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            StrMessaggio = Messaggio
        Else
            StrMessaggio = strmsg
        End If


        If InputboxMy(StrMessaggio, "Attenzione", True, valore) = DialogResult.OK Then

            idmsg = 171
            immamsg = My.Resources._042_04Pallini
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Inserire nel Feeder DOCUTEST --042 'Lato Puntini...', fino ad avvenuta calibrazione:", strmsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            End If
            immamsg = Nothing

            If dres = DialogResult.OK Then
                ls40.list = finfo.Linfo
                'ls40.Reset(Ls40Class.Costanti.S_RESET_FREE_PATH)
                ls40.Reset(Ls40Class.Costanti.S_RESET_ERROR)
                ret = ls40.DoMICRCalibration3(System.Convert.ToInt32(valore), 5, TypeSpeed, finfo.Linfo)
                If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
                    ' test per calibrazione MICR ok

                    RangeMore = System.Convert.ToSingle(valore) * 4 / 100
                    RangeLess = System.Convert.ToSingle(valore) * 4 / 100

                    ' scrivo nel file di trace i valori visualizzati a video
                    For Each s As String In finfo.Linfo.Items
                        FileTraccia.Note += s + vbCrLf
                    Next


                    If ((ls40.Percentile > (System.Convert.ToSingle(valore) - RangeLess)) And (ls40.Percentile < (System.Convert.ToSingle(valore) + RangeMore))) Then
                        ritorno = True
                    Else
                        ret = Ls40Class.Costanti.S_LS_CALIBRATION_FAILED
                    End If
                Else
                    ViewError(fase)
                End If
            Else
                ret = Ls40Class.Costanti.S_LS_USER_ABORT
            End If
            'Else
            ' non passerà mai di qui perche, il tipo di collaudo lo imposto io
            '   ret = Ls40Class.Costanti.S_LS_USER_ABORT
            ' End If
        Else
            ret = Ls40Class.Costanti.S_LS_USER_ABORT
        End If
        'noyet ls150.SetVelocita(0)
        FileTraccia.UltimoRitornoFunzione = ret

        finfo.Close()

        If ritorno = True Then
            FaseOk()
            FileTraccia.CalMicr = 1
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestCalibraTestinaPiastre(ByVal Type As Short) As Integer
        Dim ritorno As Integer

        Dim ret As Short
        Dim RangeMore As Single
        Dim RangeLess As Single
        Dim strmsg As String
        Dim TypeSpeed As Short
        Dim valore As Integer

        Dim finfo As New CtsControls.FormInfoTest



        ritorno = False
        'GMicr.Enabled = True
        'LResult.Text = ""

        FaseEsec()

        'fWaitButton = False





        Select Case Type
            Case 0

                TypeSpeed = TEST_SPEED_300_DPI
                idmsg = 170
                strmsg = " Attenzione !!! Calibrazione 300 DPI"
                ' commentato perchè per ora c'è solo una calibrazione
            Case 1

                TypeSpeed = TEST_SPEED_300_DPI_UV
                idmsg = 170
                strmsg = " Attenzione !!! Calibrazione 300 DPI UV"

            Case 2
                TypeSpeed = TEST_SPEED_300_DPI_COLOR
                idmsg = 102
                strmsg = " Attenzione !!! Calibrazione 300 DPI COLORE"
        End Select

        'in caso di scanner a colori forzo uscita se viene chiesta la type spped UV 
        'la fase  viene sempre caricata 
        If (fScannerType = TIPO_SCANNER_COLORI And TypeSpeed = TEST_SPEED_300_DPI_UV) Then
            FaseOk()
            FileTraccia.UltimoRitornoFunzione = 0 'replycode
            FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
            ritorno = True
            FileTraccia.CalMicr = 1
            'BVisualZoom = False
            Return ritorno 'OCCHIO !!!!!!!!!!!!
        End If

        'in caso di scanner a UV  forzo uscita se viene chiesta la type spped COLORI
        'la fase  viene sempre caricata 
        If (fScannerType = TIPO_SCANNER_UV And TypeSpeed = TEST_SPEED_300_DPI_COLOR) Then
            FaseOk()
            FileTraccia.UltimoRitornoFunzione = 0 'replycode
            FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
            ritorno = True
            FileTraccia.CalMicr = 1
            'BVisualZoom = False
            Return ritorno 'OCCHIO !!!!!!!!!!!!
        End If


        finfo.PointToScreen(New Point(300, 100))
        finfo.Show()
        finfo.BringToFront()



        strmsg = "Inserire nella Casella di Testo il Valore del DOCUTEST --042 di calibrazione "
        idmsg = 12

        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            StrMessaggio = Messaggio
        Else
            StrMessaggio = strmsg
        End If


        If InputboxMy(StrMessaggio, "Attenzione", True, valore) = DialogResult.OK Then

            idmsg = 171
            immamsg = My.Resources._042_04Pallini
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Inserire nel Feeder DOCUTEST --042 'Lato Puntini...', fino ad avvenuta calibrazione:", strmsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            End If
            immamsg = Nothing

            If dres = DialogResult.OK Then
                ls40.list = finfo.Linfo
                'ls40.Reset(Ls40Class.Costanti.S_RESET_FREE_PATH)
                ls40.Reset(Ls40Class.Costanti.S_RESET_ERROR)
                ret = ls40.DoMICRCalibration3(System.Convert.ToInt32(valore), 5, TypeSpeed, finfo.Linfo)
                If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
                    ' test per calibrazione MICR ok

                    RangeMore = System.Convert.ToSingle(valore) * 4 / 100
                    RangeLess = System.Convert.ToSingle(valore) * 4 / 100

                    ' scrivo nel file di trace i valori visualizzati a video
                    For Each s As String In finfo.Linfo.Items
                        FileTraccia.Note += s + vbCrLf
                    Next


                    If ((ls40.Percentile > (System.Convert.ToSingle(valore) - RangeLess)) And (ls40.Percentile < (System.Convert.ToSingle(valore) + RangeMore))) Then
                        ritorno = True
                    Else
                        ret = Ls40Class.Costanti.S_LS_CALIBRATION_FAILED
                    End If
                Else
                    ViewError(fase)
                End If
            Else
                ret = Ls40Class.Costanti.S_LS_USER_ABORT
            End If
            'Else
            ' non passerà mai di qui perche, il tipo di collaudo lo imposto io
            '   ret = Ls40Class.Costanti.S_LS_USER_ABORT
            ' End If
        Else
            ret = Ls40Class.Costanti.S_LS_USER_ABORT
        End If
        'noyet ls150.SetVelocita(0)
        FileTraccia.UltimoRitornoFunzione = ret

        finfo.Close()

        If ritorno = True Then
            FaseOk()
            FileTraccia.CalMicr = 1
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function ChipCardLS40()
        Dim ritorno As Integer = False
        Dim strmsg As String = ""
        If fSmartCardPresent = False Then Return True
        Dim myProcess As New Process()
        myProcess.StartInfo.FileName = Application.StartupPath & "\SmartCardReader\SmartPCSCDiag.exe"
        myProcess.StartInfo.WorkingDirectory = Application.StartupPath & "\SmartCardReader"
        dres = MessageboxMy("Premere OK per avviare il sw SmartPCSCDiag e verificare il funzionamento della Smart Card", "TEST SMART CARD", MessageBoxButtons.OK, MessageBoxIcon.Information, Nothing)
        myProcess.Start()
        myProcess.WaitForExit()

        strmsg = "La smart card ha funzionato correttamente?"
        dres = MessageboxMy2(strmsg, "Attenzione!!! ", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing, Nothing, Nothing)

        If dres = DialogResult.Yes Then ritorno = True

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        Return ritorno
    End Function

    Public Function TestTempoCamp(ByVal Type As Short) As Integer
        Dim ritorno As Integer
        Dim ii As Short
        Dim TypeSpeed As Short

        Dim strmsg As String = ""
        Dim finfo As New CtsControls.FormInfoTest


        Dim replycode As Short
        ii = 0
        ritorno = False
        FaseEsec()


        Select Case Type
            Case 0
                immamsg = My.Resources._042_04Barrette
                TypeSpeed = TEST_SPEED_300_DPI
                'idmsg = 172 'Inserire DOCUTEST --042 dalla parte delle Barrette Continue
                idmsg = 316 ' Inserire DOCUTEST--042 come in figura
                strmsg = " Attenzione !!! Calibrazione 300 DPI Grigi"
            Case 1
                immamsg = My.Resources._042_04Barrette
                TypeSpeed = TEST_SPEED_300_DPI_UV
                'idmsg = 172 'Inserire DOCUTEST --042 dalla parte delle Barrette Continue
                idmsg = 316 ' Inserire DOCUTEST--042 come in figura
                strmsg = " Attenzione !!! Calibrazione 300 DPI UV"
            Case 2
                immamsg = My.Resources._042_04Barrette
                TypeSpeed = TEST_SPEED_300_DPI_COLOR
                'idmsg = 172 'Inserire DOCUTEST --042 dalla parte delle Barrette Continue
                idmsg = 316 ' Inserire DOCUTEST--042 come in figura
                strmsg = " Attenzione !!! Calibrazione 300 DPI Colore"
        End Select

        'in caso di scanner a colori forzo uscita se viene chiesta la type spped UV 
        'la fase  viene sempre caricata 
        If (fScannerType = TIPO_SCANNER_COLORI And TypeSpeed = TEST_SPEED_300_DPI_UV) Then
            FaseOk()
            FileTraccia.UltimoRitornoFunzione = 0 'replycode
            FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
            ritorno = True
            immamsg = Nothing
            'FileTraccia.CalMicr = 1
            'BVisualZoom = False
            Return ritorno 'OCCHIO !!!!!!!!!!!!
        End If

        'in caso di scanner a UV  forzo uscita se viene chiesta la type spped COLORI
        'la fase  viene sempre caricata 
        If (fScannerType = TIPO_SCANNER_UV And TypeSpeed = TEST_SPEED_300_DPI_COLOR) Then
            FaseOk()
            FileTraccia.UltimoRitornoFunzione = 0 'replycode
            FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
            ritorno = True
            immamsg = Nothing
            'FileTraccia.CalMicr = 1
            'BVisualZoom = False
            Return ritorno 'OCCHIO !!!!!!!!!!!!
        End If

        If (fScannerType = TIPO_SCANNER_GRIGIO And (TypeSpeed = TEST_SPEED_300_DPI_COLOR Or TypeSpeed = TEST_SPEED_300_DPI_UV)) Then
            FaseOk()
            FileTraccia.UltimoRitornoFunzione = 0 'replycode
            FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
            ritorno = True
            immamsg = Nothing
            'FileTraccia.CalMicr = 1
            'BVisualZoom = False
            Return ritorno 'OCCHIO !!!!!!!!!!!!
        Else
            TypeSpeed = TEST_SPEED_300_DPI_UV
        End If

        finfo.PointToScreen(New Point(300, 100))
        finfo.Show()
        finfo.BringToFront()

        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire DOCUTEST --042 dalla parte delle Barrette Continue", "Attenzione!!! " + strmsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        End If
        immamsg = Nothing

        If (dres = DialogResult.OK) Then
            ls40.NrLettureOk = 0
            ls40.list = finfo.Linfo

            'If (MessageboxMy("Inserire DOCUTEST -042 dalla parte delle Barrette Continue", "Attenzione!!! " + strmsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing) = DialogResult.OK) Then
            replycode = ls40.DoTimeSampleMICRCalibration(TypeSpeed, ls40.list)

            Dim strmsginfo As String
            strmsginfo = "Ciclo : " + ii.ToString + " Valore Massimo : " + ls40.llMax.ToString + " Valore minimo : " + ls40.llMin.ToString
            finfo.Linfo.Items.Add(strmsginfo)
            finfo.Linfo.SelectedIndex = finfo.Linfo.Items.Count - 1
            FileTraccia.Note += strmsginfo + vbCrLf
            finfo.Refresh()


            If (replycode = Ls40Class.Costanti.S_LS_OKAY And (ls40.NrLettureOk = 3)) Then
                ritorno = True
            Else
                ls40.Reset(Ls40Class.Costanti.S_RESET_FREE_PATH)
            End If
        Else
            replycode = Ls40Class.Costanti.S_LS_USER_ABORT

        End If

        If ritorno = True Then
            FaseOk()
        Else
            ViewError(fase)
            FaseKo()
        End If
        finfo.Close()
        FileTraccia.UltimoRitornoFunzione = replycode
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestTempoCampPiastre(ByVal Type As Short) As Integer
        Dim ritorno As Integer
        Dim ii As Short
        Dim TypeSpeed As Short

        Dim strmsg As String = ""
        Dim finfo As New CtsControls.FormInfoTest


        Dim replycode As Short
        ii = 0
        ritorno = False
        FaseEsec()


        Select Case Type
            Case 0
                immamsg = My.Resources._042_04Barrette
                TypeSpeed = TEST_SPEED_300_DPI
                'idmsg = 172 'Inserire DOCUTEST --042 dalla parte delle Barrette Continue
                idmsg = 316 ' Inserire DOCUTEST--042 come in figura
                strmsg = " Attenzione !!! Calibrazione 300 DPI Grigi"
            Case 1
                immamsg = My.Resources._042_04Barrette
                TypeSpeed = TEST_SPEED_300_DPI_UV
                'idmsg = 172 'Inserire DOCUTEST --042 dalla parte delle Barrette Continue
                idmsg = 316 ' Inserire DOCUTEST--042 come in figura
                strmsg = " Attenzione !!! Calibrazione 300 DPI UV"
            Case 2
                immamsg = My.Resources._042_04Barrette
                TypeSpeed = TEST_SPEED_300_DPI_COLOR
                'idmsg = 172 'Inserire DOCUTEST --042 dalla parte delle Barrette Continue
                idmsg = 316 ' Inserire DOCUTEST--042 come in figura
                strmsg = " Attenzione !!! Calibrazione 300 DPI Colore"
        End Select

        'in caso di scanner a colori forzo uscita se viene chiesta la type spped UV 
        'la fase  viene sempre caricata 
        If (fScannerType = TIPO_SCANNER_COLORI And TypeSpeed = TEST_SPEED_300_DPI_UV) Then
            FaseOk()
            FileTraccia.UltimoRitornoFunzione = 0 'replycode
            FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
            ritorno = True
            immamsg = Nothing
            'FileTraccia.CalMicr = 1
            'BVisualZoom = False
            Return ritorno 'OCCHIO !!!!!!!!!!!!
        End If

        'in caso di scanner a UV  forzo uscita se viene chiesta la type spped COLORI
        'la fase  viene sempre caricata 
        If (fScannerType = TIPO_SCANNER_UV And TypeSpeed = TEST_SPEED_300_DPI_COLOR) Then
            FaseOk()
            FileTraccia.UltimoRitornoFunzione = 0 'replycode
            FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
            ritorno = True
            immamsg = Nothing
            'FileTraccia.CalMicr = 1
            'BVisualZoom = False
            Return ritorno 'OCCHIO !!!!!!!!!!!!
        End If



        finfo.PointToScreen(New Point(300, 100))
        finfo.Show()
        finfo.BringToFront()

        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire DOCUTEST --042 dalla parte delle Barrette Continue", "Attenzione!!! " + strmsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        End If
        immamsg = Nothing

        If (dres = DialogResult.OK) Then
            ls40.NrLettureOk = 0
            ls40.list = finfo.Linfo

            'If (MessageboxMy("Inserire DOCUTEST -042 dalla parte delle Barrette Continue", "Attenzione!!! " + strmsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing) = DialogResult.OK) Then
            replycode = ls40.DoTimeSampleMICRCalibration(TypeSpeed, ls40.list)

            Dim strmsginfo As String
            strmsginfo = "Ciclo : " + ii.ToString + " Valore Massimo : " + ls40.llMax.ToString + " Valore minimo : " + ls40.llMin.ToString
            finfo.Linfo.Items.Add(strmsginfo)
            finfo.Linfo.SelectedIndex = finfo.Linfo.Items.Count - 1
            FileTraccia.Note += strmsginfo + vbCrLf
            finfo.Refresh()


            If (replycode = Ls40Class.Costanti.S_LS_OKAY And (ls40.NrLettureOk = 3)) Then
                ritorno = True
            Else
                ls40.Reset(Ls40Class.Costanti.S_RESET_FREE_PATH)
            End If
        Else
            replycode = Ls40Class.Costanti.S_LS_USER_ABORT

        End If

        If ritorno = True Then
            FaseOk()
        Else
            ViewError(fase)
            FaseKo()
        End If
        finfo.Close()
        FileTraccia.UltimoRitornoFunzione = replycode
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestViewSignal() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim strmsg As String
        'TMicr.Enabled = True
        ritorno = False
        FaseEsec()

        strmsg = "Inserire DOCUTEST --042 dalla parte dei punti:"

        MessageboxMy(strmsg, "Attenzione !!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        'noyet   ls150.Reset(Ls40Class.Costanti.S_RESET_ERROR)
        'noyet    ret = ls150.ViewE13BSignal(True)
        If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
            'TGrafici.SelectedTab = TviewS
            Dim p As New Pen(Color.Red)

            If Ls40Class.Periferica.E13BSignal Is Nothing Then
                MessageboxMy("Errore: Grafico non disponibile", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
            Else
                ' Grafico4.SetTraccia(Ls40Class.Periferica.E13BSignal, Ls40Class.Periferica.E13BSignal.Length - 1, p)
                ' Grafico4.LTitle.Text = "Scorrere tutto il grafico"
                ' While Grafico4.fstop = False
                ' Application.DoEvents()
                ' End While
                If MessageboxMy("Il grafico è conforme ?", "Attenzione !!", MessageBoxButtons.YesNo, MessageBoxIcon.Information, Nothing) = DialogResult.Yes Then
                    ritorno = True

                Else
                    ritorno = False

                End If

            End If
        Else
            ritorno = False

        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        'TGrafici.SelectedTab = TEsci
        'Grafico4.HScrollBar1.Value = 0
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestCheckDocS() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim min, max As Byte
        Dim strmsg As String
        'TMicr.Enabled = True
        ritorno = False
        FaseEsec()

        strmsg = "Inserire DOCUTEST --042 dalla parte delle barrette continue"

        MessageboxMy(strmsg, "Attenzione !!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        'noyet ls150.Reset(Ls40Class.Costanti.S_RESET_ERROR)
        'noyet  ret = ls150.ViewE13BSignal(False)
        If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
            'TGrafici.SelectedTab = TCKDocS
            Dim p As New Pen(Color.Red)
            If Ls40Class.Periferica.E13BSignal Is Nothing Then
                MessageboxMy("Errore: Grafico non disponibile", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
            Else
                'noyet      ls150.DisegnaEstrectLenBar()
                min = Byte.MaxValue
                max = Byte.MinValue
                For ii As Short = 0 To 511
                    If Ls40Class.Periferica.E13BSignal(ii) < min Then
                        min = Ls40Class.Periferica.E13BSignal(ii)
                    End If
                    If Ls40Class.Periferica.E13BSignal(ii) > max Then
                        max = Ls40Class.Periferica.E13BSignal(ii)
                    End If
                Next
                'Grafico3.LTitle.Text = "Grafico Check Document Speed" + " Max = " + max.ToString + " Min = " + min.ToString

                'Grafico3.SetTraccia(Ls40Class.Periferica.ViewSignal, 512, p)

                If MessageboxMy("Il grafico è conforme ?", "Attenzione !!", MessageBoxButtons.YesNo, MessageBoxIcon.Information, Nothing) = DialogResult.Yes Then
                    ritorno = True
                Else
                    ritorno = False
                End If

            End If
        Else
            ritorno = False
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        ' TGrafici.SelectedTab = TEsci
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestLettura(ByVal cmc7 As Short, ByVal e13b As Short, ByVal Tipo_comtrollo As Short) As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim smessage As String = ""
        'TMicr.Enabled = True
        ritorno = False
        FaseEsec()

        'PROVA
        'fAtmConfiguration = True
        If (fAtmConfiguration = True) Then
            Tipo_comtrollo = CheckCodeline.CONTROLLO_DOC_LS40MYT2
        End If

        If (Tipo_comtrollo = 5) Then
            smessage = "Inserire documento per lettura DOCUTEST 48"
            idmsg = 1200 ' fittizio
        ElseIf (Tipo_comtrollo = CheckCodeline.CONTROLLO_DOC_LS40MYT2) Then
            smessage = "Inserire documenti per lettura DOCUTEST 46 liv.1-48-61-62"
            idmsg = 285
        Else
            If (Tipo_comtrollo = 6) Then
                smessage = "Inserire documento per lettura DOCUTEST 46"
                idmsg = 1200 ' fittizio
            Else
                If fTimbri = True Then
                    Tipo_comtrollo = CheckCodeline.CONTROLLO_LS40_NO_TIMBRO
                Else
                    Tipo_comtrollo = CheckCodeline.CONTROLLO_LS40_NO_TIMBRO
                End If
                If Tipo_comtrollo = CheckCodeline.CONTROLLO_LS40_NO_TIMBRO Then
                    smessage = "Inserire documenti per lettura DOCUTEST 63-64-46 liv.1-48-61-62 "
                    idmsg = 173
                End If
                If Tipo_comtrollo = CheckCodeline.CONTROLLO_LS40_TIMBRO Then
                    smessage = "Inserire documenti per lettura DOCUTEST 66-65-46-48-61-62 "
                    idmsg = 174
                End If

            End If
        End If


        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy(smessage, "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        End If


        If dres <> DialogResult.OK Then
            ret = Ls40Class.Costanti.S_LS_USER_ABORT
        Else
            ret = LeggiCodelinesAuto(cmc7, e13b, Tipo_comtrollo)
        End If

        If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
            ritorno = True
        Else
            ritorno = False
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestLetturaN(ByVal cmc7 As Short, ByVal e13b As Short, ByVal Tipo_comtrollo As Short) As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim smessage As String = ""
        'TMicr.Enabled = True
        ritorno = False
        FaseEsec()

        'PROVA
        'fAtmConfiguration = True
        If (fAtmConfiguration = True) Then
            Tipo_comtrollo = CheckCodeline.CONTROLLO_DOC_LS40MYT2
        End If

        If (Tipo_comtrollo = 5) Then
            smessage = "Inserire documento per lettura DOCUTEST 48"
            idmsg = 1200 ' fittizio
        ElseIf (Tipo_comtrollo = CheckCodeline.CONTROLLO_DOC_LS40MYT2) Then
            smessage = "Inserire documenti per lettura DOCUTEST 46 liv.1-48-61-62"
            idmsg = 285
        Else
            If (Tipo_comtrollo = 6) Then
                smessage = "Inserire documento per lettura DOCUTEST 46"
                idmsg = 1200 ' fittizio
            Else
                If fTimbri = True Then
                    Tipo_comtrollo = CheckCodeline.CONTROLLO_LS40_NO_TIMBRO
                Else
                    Tipo_comtrollo = CheckCodeline.CONTROLLO_LS40_NO_TIMBRO
                End If
                If Tipo_comtrollo = CheckCodeline.CONTROLLO_LS40_NO_TIMBRO Then
                    smessage = "Inserire documenti per lettura DOCUTEST-062 e DOCT-R147"
                    idmsg = 317 'Inserire documenti per lettura DOCUTEST-062 e DOCT-R147
                End If
                If Tipo_comtrollo = CheckCodeline.CONTROLLO_LS40_TIMBRO Then
                    smessage = "Inserire documenti per lettura DOCUTEST 66-65-46-48-61-62 "
                    idmsg = 174
                End If

            End If
        End If


        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy(smessage, "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        End If


        If dres <> DialogResult.OK Then
            ret = Ls40Class.Costanti.S_LS_USER_ABORT
        Else
            ret = LeggiCodelinesAutoN(cmc7, e13b, Tipo_comtrollo)
        End If

        If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
            ritorno = True
        Else
            ritorno = False
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestCarta(ByVal ndoc As Short) As Integer  'FASE 142   NON PIU' USATA 

        'Dim ritorno As Boolean
        Dim ritorno As Short
        Dim ret As Integer
        Dim maxw As Integer
        Dim percentuale As Single
        Dim rr As Short
        Dim fstop As Short
        Dim media As Single
        Dim massimo, minimo As Short
        Dim somma As Integer
        Dim imagra As Bitmap
        Dim gra As Graphics
        Dim penna As New Pen(System.Drawing.Color.Red)
        Dim punti() As Point
        Dim cicli As Short
        Dim font1 As New Font("Time New Roman", 8, FontStyle.Regular, GraphicsUnit.Pixel)



        'TestCartaN()

        ritorno = False
        rr = 1
        'TCollaudo.SelectedTab = TTimbriStampa
        FaseEsec()

        cicli = 0


        While (fstop = False)
            penna.Color = System.Drawing.Color.Red
            idmsg = 266
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Inserire carta  DOCUTEST --043 ", "Attenzione :  Ciclo : " + (cicli + 1).ToString + "/" + (ndoc + 1).ToString, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            End If

            If (dres <> DialogResult.OK) Then
                ret = Ls40Class.Costanti.S_LS_USER_ABORT
                fstop = True
            Else
                DisposeImg()
                'TCollaudo.SelectedTab = TMessaggi
                StrMessaggio = "Test Carta " + (cicli + 1).ToString + "/" + (ndoc + 1).ToString + " ciclo"
                'TMessaggi.Refresh()
                ls40.NomeFileImmagine = zzz + "\Dati\cardImage" + IndImgF.ToString("0000") + ".bmp"
                ls40.NomeFileImmagineRetro = zzz + "\Dati\cardImageR" + IndImgR.ToString("0000") + ".bmp"

                ret = ls40.LeggiImmCard(System.Convert.ToSByte(Ls40Class.Costanti.S_SIDE_ALL_IMAGE), rr, Ls40Class.Costanti.S_SCAN_MODE_256GR300)
                Select Case ret
                    Case Ls40Class.Costanti.S_LS_PAPER_JAM, + _
                    Ls40Class.Costanti.S_LS_JAM_AT_MICR_PHOTO, + _
                    Ls40Class.Costanti.S_LS_JAM_AT_SCANNERPHOTO, + _
                    Ls40Class.Costanti.S_LS_JAM_DOC_TOOLONG
                        idmsg = 111
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("ATTENZIONE CARTA BLOCCATA LIBERARE IL PERCORSO", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                        End If

                        If (dres <> DialogResult.OK) Then
                            ret = Ls40Class.Costanti.S_LS_USER_ABORT
                            fstop = True
                        Else
                            'noyet     ls150.Reset(Ls40Class.Costanti.S_RESET_FREE_PATH)
                        End If
                    Case Ls40Class.Costanti.S_LS_FEEDEREMPTY
                        '  MessageboxMy("Inserire carta  DOCUTEST - 043 da ingresso lineare", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                        'Case -1
                        '    idmsg = 112
                        '    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        '        dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        '    Else
                        '        dres = MessageboxMy("DOCUTEST --043 Inserito in modo non corretto", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                        '    End If

                        '    If (dres <> DialogResult.OK) Then
                        '        ret = Ls40Class.Costanti.S_LS_USER_ABORT
                        '        fstop = True
                        '    Else
                        '    End If
                    Case -1
                        idmsg = 112
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("DOCUTEST --043 Inserito in modo non corretto", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                        End If

                        If (dres <> DialogResult.OK) Then
                            ret = Ls40Class.Costanti.S_LS_USER_ABORT
                            fstop = True
                        Else
                        End If
                    Case Ls40Class.Costanti.S_LS_OKAY
                        If rr >= 0 Then
                            minimo = 32000
                            massimo = 0
                            media = 0
                            somma = 0
                            ' rr = ret  --  Fatto ritornare dalla funzione Ale !
                            ret = Ls40Class.Costanti.S_LS_OKAY
                            ReDim punti(rr)
                            imagra = New Bitmap(160, 100, Imaging.PixelFormat.Format24bppRgb)

                            gra = Graphics.FromImage(imagra)
                            Dim trasmatrix As New Drawing2D.Matrix(1, 0, 0, -1, 0, 100)
                            gra.MultiplyTransform(trasmatrix)

                            For ii = 0 To rr
                                If (Ls40Class.Periferica.Distanze(ii) < minimo) Then
                                    minimo = Ls40Class.Periferica.Distanze(ii)
                                End If
                                If (Ls40Class.Periferica.Distanze(ii) > massimo) Then
                                    massimo = Ls40Class.Periferica.Distanze(ii)
                                End If
                                somma += Ls40Class.Periferica.Distanze(ii)
                                punti(ii).X = ii
                                punti(ii).Y = Ls40Class.Periferica.Distanze(ii)

                            Next
                            media = somma / (rr + 1)
                            For ii = 0 To rr
                                punti(ii).Y -= 254
                                punti(ii).Y += 50
                            Next




                            gra.FillRectangle(New SolidBrush(Color.White), New Rectangle(0, 0, 160, 100))
                            gra.DrawLines(penna, punti)
                            penna.Color = System.Drawing.Color.Blue
                            gra.DrawLine(penna, 0, 50, 160, 50)
                            penna.Color = System.Drawing.Color.Gold
                            gra.DrawLine(penna, 0, 50 + 12, 160, 50 + 12)
                            gra.DrawLine(penna, 0, 50 - 12, 160, 50 - 12)


                            PFronte.Image = System.Drawing.Image.FromFile(zzz + "\Dati\cardImage" + IndImgF.ToString("0000") + ".bmp")
                            PRetro.Image = System.Drawing.Image.FromFile(zzz + "\Dati\cardImageR" + IndImgR.ToString("0000") + ".bmp")
                            Pimage.Image = PFronte.Image
                            PImageBack.Image = PRetro.Image
                            If (PFronte.Image.Width > PRetro.Image.Width) Then
                                maxw = PFronte.Image.Width
                            Else
                                maxw = PRetro.Image.Width
                            End If

                            percentuale = ((System.Math.Abs(PFronte.Image.Width - PRetro.Image.Width)) / maxw) * 100

                            If (percentuale > 10) Then
                                MessageboxMy(percentuale.ToString, "d", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                                ritorno = False
                            Else
                                'MessageBox.Show("ANALISI VELCITA'" + vbCrLf _
                                '                + "Media : " + media.ToString("F") + vbCrLf _
                                '                + "Massimo  : " + massimo.ToString + vbCrLf _
                                '                + "Minimo : " + minimo.ToString + vbCrLf _
                                '                + "Valore Teorico Velorità' : 265" + vbCrLf _
                                '                + "PERCENTUALI" + vbCrLf _
                                '                + "Rispetto al Valore teorico:  " + (((265 - minimo) / 265) * 100).ToString("F") + "   " + (((massimo - 265) / 265) * 100).ToString("F") + vbCrLf _
                                '                + "Rispetto al Valore di Media : " + (((media - minimo) / media) * 100).ToString("F") + "   " + (((massimo - media) / media) * 100).ToString("F") + vbCrLf _
                                '                , "Risultati Anlisi", MessageBoxButtons.OK)
                                MessageboxMy("ANALISI VELOCITA'" + vbCrLf _
                                                + " Valore Teorico Velocità' : 254" + vbCrLf _
                                                + "Media : " + media.ToString("F") + " " _
                                                + " Massimo  : " + massimo.ToString _
                                                + " Minimo : " + minimo.ToString + vbCrLf _
                                                + "PERCENTUALI" + vbCrLf _
                                                + "Rispetto al Valore teorico: Min -" + (((254 - minimo) / 254) * 100).ToString("F") + "   MAX " + (((massimo - 254) / 254) * 100).ToString("F") + vbCrLf _
                                                + "Rispetto al Valore di Media : Min -" + (((media - minimo) / media) * 100).ToString("F") + "   MAX " + (((massimo - media) / media) * 100).ToString("F") + vbCrLf _
                                                , "Risultati Analisi", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, imagra)
                            End If
                            If cicli = ndoc Then
                                fstop = True
                            End If
                            cicli += 1
                        Else
                            'writeDebug("If Else -*- errore = " + ret.ToString(), "TestCarta")
                            ''MessageboxMy("Errore = " + ret.ToString() + " non gestito !" + vbCrLf + "Prego contattare Progetto CTS !!!!", "Test Carta", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                            'MessageboxMy("Errore = " + ret.ToString() + " non gestito !" + vbCrLf + "Carta Inserira in Modo non corretto , Ripetere il test", "Test Carta", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                            'ret = Ls40Class.Costanti.S_LS_USER_ABORT
                            'fstop = True
                            idmsg = 112
                            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                            Else
                                dres = MessageboxMy("DOCUTEST --043 Inserito in modo non corretto o Errore di LETTURA", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                            End If

                            'If (dres <> DialogResult.OK) Then
                            '   ret = Ls40Class.Costanti.S_LS_USER_ABORT
                            fstop = True
                            ritorno = False
                            ret = Ls40Class.Costanti.S_LS_USER_ABORT
                            'Else
                            'End If
                        End If

                    Case Else
                        writeDebug("Case Else -*- errore = " + ret.ToString(), "TestCarta")
                        'MessageboxMy("Errore = " + ret.ToString() + " non gestito !" + vbCrLf + "Prego contattare Progetto CTS !!!!", "Test Carta", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                        MessageboxMy("Errore = " + ret.ToString() + " non gestito !" + vbCrLf + "Carta Inserira in Modo non corretto , Ripetere il test", "Test Carta", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                        ret = Ls40Class.Costanti.S_LS_USER_ABORT
                        fstop = True
                        ritorno = False
                End Select
            End If
        End While


        FileTraccia.UltimoRitornoFunzione = ret
        If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
            ritorno = True
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If

        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante

        Return ritorno
    End Function

    Public Function TestOff() As Integer
        Dim ritorno As Short
        Dim ret As Short
        ret = 0
        ritorno = False
        FaseEsec()

        While (ret >= 0)
            ret = ls40.Identificativo()
        End While
        If ret = Ls40Class.Costanti.S_LS_PERIFNOTFOUND Then
            ritorno = True
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        Return ritorno
    End Function

    Public Function TestON() As Integer
        Dim ritorno As Short
        Dim ret As Short
        ret = 1
        ritorno = False
        FaseEsec()

        While (ret <> 0)
            ret = ls40.Identificativo()
        End While
        If ret = 0 Then
            ritorno = True
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        Return ritorno
    End Function

    Public Function TestBadge(ByVal Tr As Short) As Integer
        Dim ritorno As Short
        Dim ret As Short
        Dim strBadgeNew As String
        'TBadge.Enabled = True
        'TCollaudo.SelectedTab = TBadge
        Dim strBadge As String
        strBadge = "tCTS ELECTRONICS SPA BADGE CAMPIONE 11111111111111111111111111111111111111111t1111000022223333444455556666777888999t11111111111000000000002222222222233333333333444444444455555555556666666666777777777788888888889999999999"
        strBadgeNew = "tARCA BADGE CAMPIONE 11111111111111111111111111111111111111111111111111111111t1111000022223333444455556666777888999t11111111111000000000002222222222233333333333444444444455555555556666666666777777777788888888889999999999"
        ritorno = False
        FaseEsec()


        idmsg = 176
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Strisciare Badge DOCUTEST --007", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(5))
        End If

        If (dres <> DialogResult.OK) Then
            ret = Ls40Class.Costanti.S_LS_USER_ABORT
        Else
            ret = ls40.LeggiBadge(Tr)
            If ret = Ls40Class.Costanti.S_LS_OKAY Then
                'LBadge.Text = ls150.BadgeLetto
                If (ls40.BadgeLetto = strBadge Or ls40.BadgeLetto = strBadgeNew) Then
                    idmsg = 177
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        dres = MessageboxMy2(ls40.BadgeLetto + vbCrLf + Messaggio, Titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy(ls40.BadgeLetto + vbCrLf + " Le tracce del badge sono corrette ?", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Question, Nothing)
                    End If
                    ritorno = True
                Else
                    idmsg = 187
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        dres = MessageboxMy2(ls40.BadgeLetto + vbCrLf + Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy(ls40.BadgeLetto + vbCrLf + "Tracce del badge lette non correttamente", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                    End If
                    ritorno = False
                    ret = Ls40Class.Costanti.S_LS_USER_ABORT
                End If
            Else
                ritorno = False
            End If
        End If

        ' lettura badge 08 e 10 rimossi per RAC 210506-P01
        'If (ritorno = True) Then
        '    ritorno = False

        '    strBadge = "tCTS ELECTRONICS SPA BADGE CARD 08 111111111111111111111111111111111111111111t1111000022223333444455556666777888999t11111111111000000000002222222222233333333333444444444455555555556666666666777777777788888888889999999999"
        '    strBadgeNew = "tARCA BADGE CAMPIONE BADGE CARD 08 111111111111111111111111111111111111111111t1111000022223333444455556666777888999t11111111111000000000002222222222233333333333444444444455555555556666666666777777777788888888889999999999"

        '    idmsg = 188
        '    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
        '        dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        '    Else
        '        dres = MessageboxMy("Strisciare Badge DOCUTEST --008", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(5))
        '    End If

        '    If (dres <> DialogResult.OK) Then
        '        ret = Ls40Class.Costanti.S_LS_USER_ABORT
        '    Else
        '        ret = ls40.LeggiBadge(Tr)
        '        If ret = Ls40Class.Costanti.S_LS_OKAY Then
        '            'LBadge.Text = ls150.BadgeLetto
        '            If (ls40.BadgeLetto = strBadge Or ls40.BadgeLetto = strBadgeNew) Then
        '                idmsg = 177
        '                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
        '                    dres = MessageboxMy2(ls40.BadgeLetto + vbCrLf + Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        '                Else
        '                    dres = MessageboxMy(ls40.BadgeLetto + vbCrLf + " Le tracce del badge sono corrette ?", "Attenzione", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing)
        '                End If
        '                ritorno = True
        '            Else
        '                idmsg = 187
        '                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
        '                    dres = MessageboxMy2(ls40.BadgeLetto + vbCrLf + Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        '                Else
        '                    dres = MessageboxMy(ls40.BadgeLetto + vbCrLf + "Tracce del badge lette non correttamente", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
        '                End If
        '                ritorno = False
        '                ret = Ls40Class.Costanti.S_LS_USER_ABORT
        '            End If
        '        Else
        '            ritorno = False
        '        End If
        '    End If

        'End If

        'If (ritorno = True) Then
        '    ritorno = False

        '    strBadge = "tCTS ELECTRONICS SPA BADGE CARD 10 111111111111111111111111111111111111111111t1111000022223333444455556666777888999t11111111111000000000002222222222233333333333444444444455555555556666666666777777777788888888889999999999"
        '    strBadgeNew = "tARCA BADGE CARD 10 111111111111111111111111111111111111111111t1111000022223333444455556666777888999t11111111111000000000002222222222233333333333444444444455555555556666666666777777777788888888889999999999"

        '    idmsg = 189
        '    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
        '        dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        '    Else
        '        dres = MessageboxMy("Strisciare Badge DOCUTEST --010", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(5))
        '    End If

        '    If (dres <> DialogResult.OK) Then
        '        ret = Ls40Class.Costanti.S_LS_USER_ABORT
        '    Else
        '        ret = ls40.LeggiBadge(Tr)
        '        If ret = Ls40Class.Costanti.S_LS_OKAY Then
        '            'LBadge.Text = ls150.BadgeLetto
        '            If (ls40.BadgeLetto = strBadge Or ls40.BadgeLetto = strBadgeNew) Then
        '                idmsg = 177
        '                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
        '                    dres = MessageboxMy2(ls40.BadgeLetto + vbCrLf + Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        '                Else
        '                    dres = MessageboxMy(ls40.BadgeLetto + vbCrLf + " Le tracce del badge sono corrette ?", "Attenzione", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing)
        '                End If
        '                ritorno = True
        '            Else
        '                idmsg = 187
        '                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
        '                    dres = MessageboxMy2(ls40.BadgeLetto + vbCrLf + Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        '                Else
        '                    dres = MessageboxMy(ls40.BadgeLetto + vbCrLf + "Tracce del badge lette non correttamente", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
        '                End If
        '                ritorno = False
        '                ret = Ls40Class.Costanti.S_LS_USER_ABORT
        '            End If
        '        Else
        '            ritorno = False
        '        End If
        '    End If

        'End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestBadgePiastra(ByVal Tr As Short) As Integer
        Dim ritorno As Short
        Dim ret As Short
        Dim strBadgeNew As String
        'TBadge.Enabled = True
        'TCollaudo.SelectedTab = TBadge
        Dim strBadge As String
        strBadge = "tCTS ELECTRONICS SPA BADGE CAMPIONE 11111111111111111111111111111111111111111t1111000022223333444455556666777888999t11111111111000000000002222222222233333333333444444444455555555556666666666777777777788888888889999999999"
        strBadgeNew = "tARCA BADGE CAMPIONE 11111111111111111111111111111111111111111111111111111111t1111000022223333444455556666777888999t11111111111000000000002222222222233333333333444444444455555555556666666666777777777788888888889999999999"
        ritorno = False
        FaseEsec()


        idmsg = 176
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Strisciare Badge DOCUTEST --007", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(5))
        End If

        If (dres <> DialogResult.OK) Then
            ret = Ls40Class.Costanti.S_LS_USER_ABORT
        Else
            ret = ls40.LeggiBadge(Tr)
            If ret = Ls40Class.Costanti.S_LS_OKAY Then
                'LBadge.Text = ls150.BadgeLetto
                If (ls40.BadgeLetto = strBadge Or ls40.BadgeLetto = strBadgeNew) Then
                    idmsg = 177
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        dres = MessageboxMy2(ls40.BadgeLetto + vbCrLf + Messaggio, Titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy(ls40.BadgeLetto + vbCrLf + " Le tracce del badge sono corrette ?", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Question, Nothing)
                    End If
                    ritorno = True
                Else
                    idmsg = 187
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        dres = MessageboxMy2(ls40.BadgeLetto + vbCrLf + Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy(ls40.BadgeLetto + vbCrLf + "Tracce del badge lette non correttamente", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                    End If
                    ritorno = False
                    ret = Ls40Class.Costanti.S_LS_USER_ABORT
                End If
            Else
                ritorno = False
            End If
        End If


        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestBeepPeriferica() As Integer
        Dim ritorno As Short
        Dim ret As Short
        'TBadge.Enabled = True
        'TCollaudo.SelectedTab = TBadge
        ritorno = False
        FaseEsec()
        idmsg = 156
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Iniziare il test Beep ( ascoltare 5 beep)", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(5))
        End If

        If (dres <> DialogResult.OK) Then
            ret = Ls40Class.Costanti.S_LS_USER_ABORT
        Else
            ret = ls40.TestBeep()
            If ret = Ls40Class.Costanti.S_LS_OKAY Then
                idmsg = 157
                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                    dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                Else
                    dres = MessageboxMy("La periferica ha fatto Beep 5 volte?", "Attenzione", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, VetImg(5))
                End If
                If (dres <> DialogResult.Yes) Then
                    ret = Ls40Class.Costanti.S_LS_USER_ABORT
                Else
                    ritorno = True
                End If
            Else
                ritorno = False
                ret = Ls40Class.Costanti.S_LS_USER_ABORT
            End If
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestLedPeriferica() As Integer
        Dim ritorno As Short
        Dim ret As Short
        'TBadge.Enabled = True
        'TCollaudo.SelectedTab = TBadge
        ritorno = False
        FaseEsec()
        idmsg = 158
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Iniziare il test LED (Verificare lampeggio ROSSO)", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(5))
        End If

        If (dres <> DialogResult.OK) Then
            ret = Ls40Class.Costanti.S_LS_USER_ABORT
        Else
            'ret = ls40.TestLed(1)
            ret = ls40.TestLed(2)
            If ret = Ls40Class.Costanti.S_LS_OKAY Then
                idmsg = 159
                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                    dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                Else
                    dres = MessageboxMy("Il Led Lampeggia ROSSO ?", "Attenzione", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, VetImg(5))
                End If
                If (dres <> DialogResult.Yes) Then
                    ret = Ls40Class.Costanti.S_LS_USER_ABORT
                Else

                    idmsg = 160
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy("Iniziare il test LED (Verificare lampeggio VERDE)", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(5))
                    End If

                    If (dres <> DialogResult.OK) Then
                        ret = Ls40Class.Costanti.S_LS_USER_ABORT
                    Else
                        ret = ls40.TestLed(0)
                        If ret = Ls40Class.Costanti.S_LS_OKAY Then
                            idmsg = 161
                            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                            Else
                                dres = MessageboxMy("Il Led Lampeggia VERDE ?", "Attenzione", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, VetImg(5))
                            End If
                            ls40.Reset(Ls40Class.Costanti.S_RESET_ERROR)
                            If (dres <> DialogResult.Yes) Then
                                ret = Ls40Class.Costanti.S_LS_USER_ABORT
                            Else
                                ritorno = True
                            End If

                        Else
                            ritorno = False
                            ret = Ls40Class.Costanti.S_LS_USER_ABORT
                        End If
                    End If
                End If
            End If
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestLunghezzaImmagineN() As Integer
        Dim ret As Short
        Dim ritorno As Short
        Dim finfo As CtsControls.FormInfoTest

        finfo = New CtsControls.FormInfoTest()
        finfo.PointToScreen(New Point(300, 100))
        finfo.Show()
        finfo.BringToFront()

        ritorno = False
        'BVisualZoom = False
        FaseEsec()
        idmsg = 318 'Inserire DOCT-148 Per calibrazione lunghezza immagine, sino all' esito", "Attenzione!!
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire DOCT-148 Per calibrazione lunghezza immagine, sino all' esito", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        End If

        If (dres = DialogResult.OK) Then
            ret = ls40.CalibraImmagineNew()
            Dim strmsginfo As String
            strmsginfo = "Valore Teorico: 1902 Pixel,Valore Impostato: " + ls40.ValoreLunghezzaImmagine.ToString() + " Pixel"
            FileTraccia.Note += strmsginfo + vbCrLf
            finfo.Linfo.Items.Add(strmsginfo)
            finfo.Linfo.SelectedIndex = finfo.Linfo.Items.Count - 1
            FileTraccia.Note += strmsginfo + vbCrLf
            finfo.Refresh()
        Else

            ret = -1000
        End If

        Sleep(4)
        If ret = 0 Then
            FileTraccia.UltimoRitornoFunzione = ret
            ret = True
        End If
        If ret = -1000 Then
            FileTraccia.UltimoRitornoFunzione = Ls40Class.Costanti.S_LS_USER_ABORT
            ret = False
        End If

        If ret = True Then
            ritorno = True
        Else

            StrCommento += "Calibrazione non a buon fine: " + ret.ToString + " "
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        finfo.Close()
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestLunghezzaImmagine() As Integer
        Dim ret As Short
        Dim ritorno As Short
        Dim finfo As CtsControls.FormInfoTest

        finfo = New CtsControls.FormInfoTest()
        finfo.PointToScreen(New Point(300, 100))
        finfo.Show()
        finfo.BringToFront()

        ritorno = False
        'BVisualZoom = False
        FaseEsec()
        idmsg = 166
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire DOCUTEST --085 Per calibrazione lunghezza immagine, sino all' esito", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        End If

        If (dres = DialogResult.OK) Then
            ret = ls40.CalibraImmagine()
            Dim strmsginfo As String
            strmsginfo = "Valore Teorico: 1654 Pixel,Valore Impostato: " + ls40.ValoreLunghezzaImmagine.ToString() + " Pixel"
            FileTraccia.Note += strmsginfo + vbCrLf
            finfo.Linfo.Items.Add(strmsginfo)
            finfo.Linfo.SelectedIndex = finfo.Linfo.Items.Count - 1
            FileTraccia.Note += strmsginfo + vbCrLf
            finfo.Refresh()
        Else

            ret = -1000
        End If

        Sleep(4)
        If ret = 0 Then
            FileTraccia.UltimoRitornoFunzione = ret
            ret = True
        End If
        If ret = -1000 Then
            FileTraccia.UltimoRitornoFunzione = Ls40Class.Costanti.S_LS_USER_ABORT
            ret = False
        End If

        If ret = True Then
            ritorno = True
        Else

            StrCommento += "Calibrazione non a buon fine: " + ret.ToString + " "
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        finfo.Close()
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestFotoDPLs40() As Integer ' faccio la calibrazione del foto doppia sfogliatura
        Dim ritorno As Short
        Dim ret As Short
        '  Dim R As DialogResult
        ritorno = False

        FaseEsec()

        If (fDoubleLeafingPresente = True) Then
            ' faccio la calibrazione del foto doppia sfogliatura
            idmsg = 95
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Inserire il  DOCUTEST --052 nel Feeder per la calibrazione del fotosensore Doppia Sfogliatura", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            End If

            If dres <> DialogResult.OK Then
                fase.SetState = 0
                fase.IsBlinking = True
                fase.IsSelected = True
                ' errore calibrazione doppia sfogliatura
                ViewError(fase)
            Else
                fase.IsSelected = False

                ret = ls40.CalSfogliatura()

                FileTraccia.Note = "Fotosensore DP 1: " + Ls40Class.Periferica.PhotoValueDP(0).ToString + vbCrLf
                FileTraccia.Note += "Fotosensore DP 2: " + Ls40Class.Periferica.PhotoValueDP(1).ToString + vbCrLf
                FileTraccia.Note += "Fotosensore DP 3: " + Ls40Class.Periferica.PhotoValueDP(2).ToString + vbCrLf

                FileTraccia.Note += "Fotosensore TH 1: " + Ls40Class.Periferica.PhotoValueDP(3).ToString + vbCrLf
                FileTraccia.Note += "Fotosensore TH 2: " + Ls40Class.Periferica.PhotoValueDP(4).ToString + vbCrLf
                FileTraccia.Note += "Fotosensore TH 3: " + Ls40Class.Periferica.PhotoValueDP(5).ToString + vbCrLf
                FileTraccia.Note += "Fotosensore TH 4: " + Ls40Class.Periferica.PhotoValueDP(6).ToString + vbCrLf
                FileTraccia.Note += "Fotosensore TH 5: " + Ls40Class.Periferica.PhotoValueDP(7).ToString + vbCrLf
                FileTraccia.Note += "Fotosensore TH 6: " + Ls40Class.Periferica.PhotoValueDP(8).ToString

                If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
                    ritorno = True

                Else
                    ' errore calibrazione doppia sfogliatura
                    ViewError(fase)
                End If
            End If
        Else
            ritorno = True
            ret = Ls40Class.Costanti.S_LS_OKAY
        End If
        If ritorno = True Then
            If (fDoubleLeafingPresente = True) Then
                FileTraccia.FotoDp = 1
            End If
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestAssegnoSpessoLs40() As Integer
        Dim ret As Short
        Dim ritorno As Integer
        ritorno = False
        FaseEsec()

        If (fDoubleLeafingPresente = True) Then
            idmsg = 117
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Inserire Docutest--052 piegato doppio ", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            End If


            If (dres = DialogResult.OK) Then
                ret = ls40.Docutest04()
                If (ret = Ls40Class.Costanti.S_LS_DOUBLE_LEAFING_ERROR) Then
                    Sleep(2)
                    ret = ls40.Reset(Ls40Class.Costanti.S_RESET_FREE_PATH)
                    If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
                        ritorno = True
                    Else
                        idmsg = 118
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("Accendere e Spegnere la periferica", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                        End If


                        'TestAspetta(False)
                        'TestAspetta(True)
                        ret = Ls40Class.Costanti.S_LS_OKAY
                        ritorno = True
                    End If
                Else


                End If
            Else
                ritorno = False
                ret = Ls40Class.Costanti.S_LS_USER_ABORT
            End If
        Else
            ritorno = True
            ret = Ls40Class.Costanti.S_LS_OKAY
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestSmagnetizzato(ByVal BCMC7 As Boolean) As Integer
        Dim ritorno As Integer
        Dim ret As Integer
        Dim str, str1 As String
        Dim chcd As New CheckCodeline
        ritorno = False
        FaseEsec()
        If (BCMC7 = True) Then
            LettureCMC7 = 1
            LettureE13B = 0
            TipoComtrolloLetture = CheckCodeline.CONTROLLO_SMAG_CMC7
            str = "Smagnetizzare un DOCUTEST CMC7 Smagnetizzato 49"
            idmsg = 167

        Else
            LettureE13B = 1
            LettureCMC7 = 0
            TipoComtrolloLetture = CheckCodeline.CONTROLLO_SMAG_E13B
            str = "Smagnetizzare un DOCUTEST E13B Smagnetizzato 46"
            idmsg = 121

        End If

        TipoComtrolloLetture = 0

        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy(str, "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Information, Nothing)
        End If


        If (dres = DialogResult.OK) Then
            If BCMC7 = True Then
                idmsg = 168
                str1 = "Inserire un DOCUTEST CMC7 Smagnetizzato 49"
            Else
                idmsg = 122
                str1 = "Inserire un DOCUTEST E13B Smagnetizzato 46"
            End If

            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy(str1, "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Information, Nothing)
            End If


            If (dres = DialogResult.OK) Then
                ret = ls40.LeggiCodeline(Ls40Class.Costanti.S_SORTER_BAY1)
                If (ret = Ls40Class.Costanti.S_LS_OKAY) Then

                    chcd.DaLeggereCMC7 = LettureCMC7
                    chcd.DaLeggereE13B = LettureE13B
                    chcd.Tipo_controllo = TipoComtrolloLetture
                    If chcd.CheckCodeline(ls40.CodelineLetta) = 0 Then
                        'conteggio e vedo se ho finito
                        If chcd.Conteggiacodeline() = 1 Then
                            ret = Ls40Class.Costanti.S_LS_OKAY
                        Else
                            ret = Ls40Class.Costanti.S_LS_CODELINE_ERROR
                            idmsg = 169
                            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                            Else
                                dres = MessageboxMy("Controllare la presenza del magnete", "Errore !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                            End If


                        End If
                    Else
                        ret = Ls40Class.Costanti.S_LS_CODELINE_ERROR
                        idmsg = 169
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("Controllare la presenza del magnete", "Errore !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                        End If

                    End If
                Else

                    ViewError(fase)
                End If
            Else
                ret = Ls40Class.Costanti.S_LS_USER_ABORT
            End If
        Else
            ret = Ls40Class.Costanti.S_LS_USER_ABORT
        End If
        FileTraccia.UltimoRitornoFunzione = ret

        If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
            ritorno = True
        Else
            ritorno = False
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function LeggiCodelines(ByVal cmc7 As Short, ByVal e13b As Short) As Integer
        Dim ret As Short

        Dim fstop As Boolean = False
        Dim ferror As Boolean = False
        Dim ii As Integer
        ii = 1
        chcd.Reset()
        chcd.VisualizzaFinestra(True)
        'chcd.F.Parent = Me

        ret = 0
        TipoComtrolloLetture = 2

        '      MessageboxMy("Inserire Documento lettura ", "Attenzione !! ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        While (fstop = False)
            ls40.NomeFileImmagine = zzz + "\Dati\CodeFImage" + IndImgF.ToString("0000") + ".bmp"
            ls40.NomeFileImmagineRetro = zzz + "\Dati\CodeRImage" + IndImgR.ToString("0000") + ".bmp"
            IndImgF += 1
            IndImgR += 1

            'noyet  ret = ls150.LeggiCodelineImmagini(Ls40Class.Costanti.S_SORTER_BAY1)
            If (ret = Ls40Class.Costanti.S_LS_DOUBLE_LEAFING_ERROR) Then
                idmsg = 125
                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                    dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                Else
                    dres = MessageboxMy("Errore double leafing...Continuare?", "Errore !!!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing)
                End If


                If (dres = DialogResult.Yes) Then
                    'noyet  ret = ls150.Reset(Ls40Class.Costanti.S_RESET_FREE_PATH)
                    ret = Ls40Class.Costanti.S_LS_OKAY
                Else
                    fstop = True
                    ferror = True
                    ViewError(fase)
                End If

            End If
            If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
                If (Not (ls40.CodelineLetta Is Nothing)) Then
                    'LICodeline.Items.Add(ls150.CodelineLetta)

                    Try
                        DisposeImg()
                        Pimage.Image = System.Drawing.Image.FromFile(ls40.NomeFileImmagine)
                        PImageBack.Image = System.Drawing.Image.FromFile(ls40.NomeFileImmagineRetro)

                    Catch ex As Exception
                        ' DEBUG
                        ' MessageBox.Show(ex.Message + " " + ex.Source, "errore")
                    End Try

                    LettureCMC7 = cmc7
                    LettureE13B = e13b
                    chcd.DaLeggereCMC7 = LettureCMC7
                    chcd.DaLeggereE13B = LettureE13B
                    chcd.Tipo_controllo = TipoComtrolloLetture

                    If chcd.CheckCodeline(ls40.CodelineLetta) = 0 Then
                        'conteggio e vedo se ho finito

                        If chcd.Conteggiacodeline() = 1 Then

                            'Me.Refresh()
                            chcd.F.Refresh()
                            'chcd.p.Show()
                            ' ho finito di contare senza errori
                            fstop = True
                            ret = 1
                        Else
                            ' non ho finito di contare
                            'Me.Refresh()
                            chcd.F.Refresh()
                        End If
                    Else
                        'erroe di lettura nella codeline
                        fstop = True
                        ferror = True
                        idmsg = 124
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy2(Messaggio + ls40.CodelineLetta, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("Codeline Errata: " + ls40.CodelineLetta, "Errore !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                        End If


                    End If
                End If
            Else
                ' processa do non a buon fine
                If (ls40.Reply = Ls40Class.Costanti.S_LS_FEEDEREMPTY) Then
                    ' Me.Refresh()
                    Application.DoEvents()
                Else
                    fstop = True
                    ferror = True
                    ViewError(fase)
                End If

            End If
        End While
        chcd.F.Refresh()
        chcd.VisualizzaFinestra(False)

        Return ret
    End Function

    Public Function LeggiCodelinesAuto(ByVal cmc7 As Short, ByVal e13b As Short, ByVal Tipo_controllo As Short) As Integer
        Dim ret As Short
        'Dim retTMP As Short
        Dim fstop As Boolean = False
        Dim ferror As Boolean = False
        Dim ii As Integer
        Dim chcd As New CheckCodeline
        ii = 1
        chcd.Reset()
        chcd.VisualizzaFinestra(True)
        chcd.Tipo_controllo = Tipo_controllo

        'chcd.F.Parent = Me

        ret = Ls40Class.Costanti.S_LS_OKAY


        If ret = Ls40Class.Costanti.S_LS_OKAY Then

            'If (fAtmConfiguration = True) Then
            '    retTMP = ls40.DisableATMConfiguration(0)
            'End If

            '      MessageboxMy("Inserire Documento lettura ", "Attenzione !! ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            While (fstop = False)
                ls40.NomeFileImmagine = zzz + "\Dati\CodeFImage" + IndImgF.ToString("0000") + ".bmp"
                ls40.NomeFileImmagineRetro = zzz + "\Dati\CodeRImage" + IndImgR.ToString("0000") + ".bmp"
                IndImgF += 1
                IndImgR += 1

                ret = ls40.LeggiCodelineImmagini(Ls40Class.Costanti.S_SORTER_BAY1)
                ' 35 è sorter full
                '  Me.Refresh()
                Application.DoEvents()
                Select Case ret

                    Case 7
                        'noyet    ls150.Stopaaaaa()
                        idmsg = 125
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("Warning double leafing...Continuare?", "Errore !!!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing)
                        End If

                        If (dres = DialogResult.Yes) Then
                            ret = ls40.Reset(Ls40Class.Costanti.S_RESET_FREE_PATH)
                            ret = Ls40Class.Costanti.S_LS_OKAY

                        Else
                            fstop = True
                            ferror = True
                            ret = Ls40Class.Costanti.S_LS_USER_ABORT
                            ViewError(fase)
                        End If
                    Case Ls40Class.Costanti.S_LS_PAPER_JAM, + _
                           Ls40Class.Costanti.S_LS_JAM_AT_MICR_PHOTO, + _
                           Ls40Class.Costanti.S_LS_JAM_AT_SCANNERPHOTO, + _
                            Ls40Class.Costanti.S_LS_JAM_DOC_TOOLONG

                        fstop = True
                        ferror = True
                        ViewError(fase)
                        ls40.Reset(Ls40Class.Costanti.S_RESET_FREE_PATH)
                    Case Ls40Class.Costanti.S_LS_FEEDEREMPTY
                        ret = Ls40Class.Costanti.S_LS_OKAY
                        '  Me.Refresh()
                        Application.DoEvents()

                    Case Ls40Class.Costanti.S_LS_OKAY ' per okay non faccio nulla
                        If (Not (ls40.CodelineLetta Is Nothing)) Then
                            ' LICodeline.Items.Add(ls150.CodelineLetta)

                            Try
                                DisposeImg()
                                Pimage.Image = System.Drawing.Image.FromFile(ls40.NomeFileImmagine)
                                PImageBack.Image = System.Drawing.Image.FromFile(ls40.NomeFileImmagineRetro)
                                Me.formpadre.Refresh()
                            Catch ex As Exception
                                ' DEBUG
                                MessageBox.Show(ex.Message + " " + ex.Source + ret.ToString, "errore")
                            End Try
                            '  Me.Refresh()
                            Application.DoEvents()
                            LettureCMC7 = cmc7
                            LettureE13B = e13b
                            chcd.DaLeggereCMC7 = LettureCMC7
                            chcd.DaLeggereE13B = LettureE13B


                            If chcd.CheckCodeline(ls40.CodelineLetta) = 0 Then
                                'conteggio e vedo se ho finito
                                If chcd.Conteggiacodeline() = 1 Then

                                    'Me.Refresh()
                                    chcd.F.Refresh()
                                    'chcd.p.Show()
                                    ' ho finito di contare senza errori

                                    fstop = True
                                    ferror = False

                                Else
                                    ' non ho finito di contare
                                    'Me.Refresh()
                                    chcd.F.Refresh()
                                End If
                            Else
                                'erroe di lettura nella codeline
                                'noyet    ls150.Stopaaaaa()
                                fstop = True
                                ferror = True
                                ret = Ls40Class.Costanti.S_LS_CODELINE_ERROR
                                idmsg = 175
                                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                    dres = MessageboxMy2(Messaggio + ls40.CodelineLetta, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                                Else
                                    dres = MessageboxMy("Codeline Errata: " + ls40.CodelineLetta, "Errore !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                End If



                                'ret = ls150.ViaDocumentiAuto(0)
                            End If
                        End If
                    Case Else
                        fstop = True
                        ferror = True
                        ViewError(fase)
                End Select


                ' processa do non a buon fine
                'Else ' fi reply <> 0
            End While
        End If

        'If (fAtmConfiguration = True) Then
        '    retTMP = ls40.DisableATMConfiguration(1)
        'End If

        chcd.F.Refresh()
        chcd.VisualizzaFinestra(False)
        'ls150
        'noyet    ls150.Stopaaaaa()
        Return ret
    End Function

    Public Function LeggiCodelinesAutoN(ByVal cmc7 As Short, ByVal e13b As Short, ByVal Tipo_controllo As Short) As Integer
        Dim ret As Short
        'Dim retTMP As Short
        Dim fstop As Boolean = False
        Dim ferror As Boolean = False
        Dim ii As Integer
        Dim chcd As New CheckCodeline
        ii = 1
        chcd.Reset()
        chcd.VisualizzaFinestra(True)
        chcd.Tipo_controllo = Tipo_controllo

        'chcd.F.Parent = Me

        ret = Ls40Class.Costanti.S_LS_OKAY


        If ret = Ls40Class.Costanti.S_LS_OKAY Then

            'If (fAtmConfiguration = True) Then
            '    retTMP = ls40.DisableATMConfiguration(0)
            'End If

            '      MessageboxMy("Inserire Documento lettura ", "Attenzione !! ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            While (fstop = False)
                ls40.NomeFileImmagine = zzz + "\Dati\CodeFImage" + IndImgF.ToString("0000") + ".bmp"
                ls40.NomeFileImmagineRetro = zzz + "\Dati\CodeRImage" + IndImgR.ToString("0000") + ".bmp"
                IndImgF += 1
                IndImgR += 1

                ret = ls40.LeggiCodelineImmagini(Ls40Class.Costanti.S_SORTER_BAY1)
                ' 35 è sorter full
                '  Me.Refresh()
                Application.DoEvents()
                Select Case ret

                    Case 7
                        'noyet    ls150.Stopaaaaa()
                        idmsg = 125 'Errore double leafing...Continuare?
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("Warning double leafing...Continuare?", "Errore !!!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing)
                        End If

                        If (dres = DialogResult.Yes) Then
                            ret = ls40.Reset(Ls40Class.Costanti.S_RESET_FREE_PATH)
                            ret = Ls40Class.Costanti.S_LS_OKAY

                        Else
                            fstop = True
                            ferror = True
                            ret = Ls40Class.Costanti.S_LS_USER_ABORT
                            ViewError(fase)
                        End If
                    Case Ls40Class.Costanti.S_LS_PAPER_JAM, + _
                           Ls40Class.Costanti.S_LS_JAM_AT_MICR_PHOTO, + _
                           Ls40Class.Costanti.S_LS_JAM_AT_SCANNERPHOTO, + _
                            Ls40Class.Costanti.S_LS_JAM_DOC_TOOLONG

                        fstop = True
                        ferror = True
                        ViewError(fase)
                        ls40.Reset(Ls40Class.Costanti.S_RESET_FREE_PATH)
                    Case Ls40Class.Costanti.S_LS_FEEDEREMPTY
                        ret = Ls40Class.Costanti.S_LS_OKAY
                        '  Me.Refresh()
                        Application.DoEvents()

                    Case Ls40Class.Costanti.S_LS_OKAY ' per okay non faccio nulla
                        If (Not (ls40.CodelineLetta Is Nothing)) Then
                            ' LICodeline.Items.Add(ls150.CodelineLetta)

                            Try
                                DisposeImg()
                                Pimage.Image = System.Drawing.Image.FromFile(ls40.NomeFileImmagine)
                                PImageBack.Image = System.Drawing.Image.FromFile(ls40.NomeFileImmagineRetro)
                                Me.formpadre.Refresh()
                            Catch ex As Exception
                                ' DEBUG
                                MessageBox.Show(ex.Message + " " + ex.Source + ret.ToString, "errore")
                            End Try
                            '  Me.Refresh()
                            Application.DoEvents()
                            LettureCMC7 = cmc7
                            LettureE13B = e13b
                            chcd.DaLeggereCMC7 = LettureCMC7
                            chcd.DaLeggereE13B = LettureE13B


                            If chcd.CheckCodelineN(ls40.CodelineLetta) = 0 Then
                                'conteggio e vedo se ho finito
                                If chcd.ConteggiacodelineN() = 1 Then

                                    'Me.Refresh()
                                    chcd.F.Refresh()
                                    'chcd.p.Show()
                                    ' ho finito di contare senza errori

                                    fstop = True
                                    ferror = False

                                Else
                                    ' non ho finito di contare
                                    'Me.Refresh()
                                    chcd.F.Refresh()
                                End If
                            Else
                                'erroe di lettura nella codeline
                                'noyet    ls150.Stopaaaaa()
                                fstop = True
                                ferror = True
                                ret = Ls40Class.Costanti.S_LS_CODELINE_ERROR
                                idmsg = 175
                                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                    dres = MessageboxMy2(Messaggio + ls40.CodelineLetta, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                                Else
                                    dres = MessageboxMy("Codeline Errata: " + ls40.CodelineLetta, "Errore !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                End If



                                'ret = ls150.ViaDocumentiAuto(0)
                            End If
                        End If
                    Case Else
                        fstop = True
                        ferror = True
                        ViewError(fase)
                End Select


                ' processa do non a buon fine
                'Else ' fi reply <> 0
            End While
        End If

        'If (fAtmConfiguration = True) Then
        '    retTMP = ls40.DisableATMConfiguration(1)
        'End If

        chcd.F.Refresh()
        chcd.VisualizzaFinestra(False)
        'ls150
        'noyet    ls150.Stopaaaaa()
        Return ret
    End Function

    Public Function TestMessaggioLs40() As Integer

        Dim ritorno As Short
        Dim ret As Short

        ritorno = False

        FaseEsec()

        'If (fInkDetector = True) Then
        idmsg = 289
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire i Cappucci sui foto", "Attenzione!!! ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        End If
        '...
        'Else
        'ritorno = True
        'End If



        If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
            ritorno = True
        Else
            ls40.ReturnC.ReturnCode = ret
            ViewError(fase)
        End If



        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno

    End Function

    Public Function TestDimensioniImmagini(ByVal side As Short) As Integer
        Dim ritorno As Integer
        Dim ret As Integer
        Dim stringa As String
        Dim retZoom As Short
        stringa = ""
        ritorno = False

        If (fZebrato = True) Then
            ritorno = True
        Else

            If side = 0 Then
                idmsg = 264
            Else
                idmsg = 265
            End If

            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Inserire un DOCUTEST-064 per verificare l'immagine ", "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Question, VetImg(7))
            End If

            If dres = DialogResult.OK Then
                'Spostato qui se no LsSaveBid dava -18
                DisposeImg()

                ls40.NomeFileImmagine = zzz + "\Quality\FrontImage" + IndImgF.ToString("0000") + ".bmp"

                ls40.NomeFileImmagineRetro = zzz + "\Quality\BackImage" + IndImgR.ToString("0000") + ".bmp"

                ret = ls40.LeggiImmaginiAllinea(Ls40Class.Costanti.S_SIDE_ALL_IMAGE, Ls40Class.Costanti.S_SCAN_MODE_256GR300)



                If (ret = Ls40Class.Costanti.S_LS_OKAY) Then

                    Pimage.Image = System.Drawing.Image.FromFile(ls40.NomeFileImmagine)
                    IndImgF += 1

                    PImageBack.Image = System.Drawing.Image.FromFile(ls40.NomeFileImmagineRetro)
                    IndImgR += 1

                    If (Math.Abs(Pimage.Image.Height - PImageBack.Image.Height) > 20) Then

                        ritorno = False
                    Else

                        'BVisualZoom = True
                        DirZoom = Application.StartupPath
                        Dim a As Short
                        a = DirZoom.LastIndexOf("\")
                        DirZoom = DirZoom.Substring(0, a)
                        DirZoom += "\Quality\"
                        FileZoomCorrente = ls40.NomeFileImmagine
                        ' Zoom Immagine 
                        retZoom = ShowZoom(DirZoom, FileZoomCorrente)

                        If retZoom = DialogResult.OK Then
                            ritorno = True
                        End If



                        'Dim TempBitmap As New Bitmap(Pimage.Image)

                        'If(Pimage.Image.)
                        'If (Pimage.Image.Height < ALTEZZA_DOC_78 - 20 Or Pimage.Image.Height > ALTEZZA_DOC_78 + 10) Or _
                        '   (PImageBack.Image.Height < ALTEZZA_DOC_78 - 20 Or PImageBack.Image.Height > ALTEZZA_DOC_78 + 10) Or _
                        '   (Pimage.Image.Width < LUNGHEZZA_DOC_78 - 20 Or Pimage.Image.Width > LUNGHEZZA_DOC_78 + 10) Or _
                        '   (PImageBack.Image.Width < LUNGHEZZA_DOC_78 - 20 Or PImageBack.Image.Width > LUNGHEZZA_DOC_78 + 10) Then
                        '    stringa = "Immagine fronte o retro fuori dimensioni"
                        '    ritorno = False
                        'Else
                        '    ritorno = True
                        'End If

                    End If
                    FileTraccia.Note = "Differenza in pixel : " + Math.Abs(Pimage.Image.Height - PImageBack.Image.Height).ToString()
                    MessageboxMy("Differenza in pixel tra fronte e retro  = " + Math.Abs(Pimage.Image.Height - PImageBack.Image.Height).ToString() + " " + stringa, "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Information, Nothing)

                End If

            Else
                ritorno = False
                ret = Ls40Class.Costanti.S_LS_USER_ABORT

            End If

            Application.DoEvents()

        End If

        If ritorno = True Then
            FaseOk()
        Else
            ViewError(fase)
            FaseKo()
        End If

        'BVisualZoom = False

        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante


        Return ritorno
    End Function

    Public Function TestBackground() As Integer
        Dim ritorno As Integer
        Dim ret As Integer
        Dim stringa As String
        'Dim Riprova As Short   ->a cosa serve ??

        ls40.ReturnC.FunzioneChiamante = "TestBackground"

        stringa = ""
        ritorno = False

        'idmsg = 293  'Inserire DOCUTEST--052 nel Feeder
        idmsg = 319 'Inserire DOCT-148 nel Feeder
        'il docutest 064 e' troppo corto e si BLOCCA nel feeder !
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire DOCUTEST--052 nel Feeder", "Attenzione!!! ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        End If

        If dres = DialogResult.OK Then
            'Spostato qui se no LsSaveBid dava -18
            DisposeImg()

            ls40.NomeFileImmagine = zzz + "\Quality\FrontImage" + IndImgF.ToString("0000") + ".bmp"

            ls40.NomeFileImmagineRetro = zzz + "\Quality\BackImage" + IndImgR.ToString("0000") + ".bmp"

            'debug 
            'fZebrato = True
            If (fZebrato = True) Then
                ret = ls40.TestBackground(Convert.ToInt32(1))
            Else
                ret = ls40.TestBackground(Convert.ToInt32(0))
            End If

            If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
                ritorno = True
                MessageboxMy("TestBackground Passato", "ARCA", MessageBoxButtons.OK, MessageBoxIcon.Information, Nothing)
            Else
                ritorno = False
                MessageboxMy("TestBackground NON Passato", "ARCA", MessageBoxButtons.OK, MessageBoxIcon.Information, Nothing)
                'ret = Ls40Class.Costanti.S_LS_USER_ABORT

                Try
                    Pimage.Image = System.Drawing.Image.FromFile(ls40.NomeFileImmagine)
                    IndImgF += 1

                    PImageBack.Image = System.Drawing.Image.FromFile(ls40.NomeFileImmagineRetro)
                    IndImgR += 1
                Catch ex As Exception
                    'ret = Ls40Class.Costanti.S_LS_OKAY
                    ret = Ls40Class.Costanti.S_LS_USER_ABORT
                End Try

                If (ret = Ls40Class.Costanti.S_L40_FRONT_BACKGROUND Or ret = Ls40Class.Costanti.S_L40_FRONT_BACKGROUND_1 Or ret = Ls40Class.Costanti.S_L40_FRONT_BACKGROUND_2) Then
                    'BVisualZoom = True
                    DirZoom = Application.StartupPath
                    Dim a As Short
                    a = DirZoom.LastIndexOf("\")
                    DirZoom = DirZoom.Substring(0, a)
                    DirZoom += "\Quality\"
                    FileZoomCorrente = ls40.NomeFileImmagine
                    ' Zoom Immagine 
                    ShowZoom(DirZoom, FileZoomCorrente)
                End If

                If (ret = Ls40Class.Costanti.S_L40_BACK_BACKGROUND Or ret = Ls40Class.Costanti.S_L40_BACK_BACKGROUND_1 Or ret = Ls40Class.Costanti.S_L40_BACK_BACKGROUND_2) Then
                    'BVisualZoom = True
                    DirZoom = Application.StartupPath
                    Dim a As Short
                    a = DirZoom.LastIndexOf("\")
                    DirZoom = DirZoom.Substring(0, a)
                    DirZoom += "\Quality\"
                    FileZoomCorrente = ls40.NomeFileImmagineRetro
                    ' Zoom Immagine 
                    ShowZoom(DirZoom, FileZoomCorrente)
                End If

                'If (Riprova = 0) Then
                Fqualit = New CtsControls.FqualImg
                Fqualit.ShowDialog()

                Select Case Fqualit.ret
                    Case 0
                        ritorno = False
                    Case 1, 2
                        ritorno = TestScannersPeriferica() 'calibrazione scanner GRIGIO + COLORE + GRIGIO
                        If ritorno = True Then
                            'Riprova = 1
                            ritorno = TestBackground()
                        End If
                End Select
                'End If
            End If

        End If

        Application.DoEvents()

        'Riprova = 0
        If ritorno = True Then
            FaseOk()
        Else
            ViewError(fase)
            FaseKo()
        End If

        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante

        Return ritorno
    End Function

    'Funzione per test sfogliatore:
    ' usata solamente per collaudo periferica ls150-6 e ls150-7
    ' quando non c'è la testina MICR ed è presente lo sfogliatore

    Public Function TestSfogliatore(ByVal ndoc As Integer) As Integer
        Dim ritorno As Integer
        Dim ret As Short

        ritorno = False
        FaseEsec()
        If fTestinaMicr = True Then
            '    MessageboxMy("Prova già eseguita nella prova di lettura MICR", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            ritorno = True
        Else
            ls40.NomeFileImmagine = "img"
            idmsg = 129
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Inserire Documenti per prova sfogliatore", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            End If



            For ii = 1 To ndoc
                'Lab.Text = ii
                If Math.IEEERemainder(ii, 2) = 0 Then
                    ls40.NomeFileImmagine = zzz + "\Dati\SfogliaFImage" + ii.ToString + ".bmp"
                    'noyet  ret = ls150.LeggiImmagini(Convert.ToSByte(Ls40Class.Costanti.S_SIDE_FRONT_IMAGE))
                Else
                    ls40.NomeFileImmagine = zzz + "\Dati\SfogliaBImage" + ii.ToString + ".bmp"
                    'noyet    ret = ls150.LeggiImmagini(Convert.ToSByte(Ls40Class.Costanti.S_SIDE_BACK_IMAGE))
                End If

                If ret <> Ls40Class.Costanti.S_LS_OKAY Then
                    If ret = Ls40Class.Costanti.S_LS_FEEDEREMPTY Then
                        idmsg = 130
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("Inserire Documenti per prova sfogliatore", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                        End If
                    End If
                    ii -= 1
                Else
                    If Math.IEEERemainder(ii, 2) = 0 Then
                        DisposeImg()
                        PFronte.Image = System.Drawing.Image.FromFile(zzz + "\Dati\SfogliaFImage" + ii.ToString + ".bmp")
                        Pimage.Image = PFronte.Image
                    Else
                        DisposeImg()
                        PRetro.Image = System.Drawing.Image.FromFile(zzz + "\Dati\SfogliaBImage" + ii.ToString + ".bmp")
                        Pimage.Image = PRetro.Image
                    End If

                    Application.DoEvents()
                End If
            Next
        End If

        If (ritorno = True) Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function InputboxMy(ByVal testo As String, ByVal titolo As String, ByVal Check As Boolean, ByRef valore As Integer, ByVal ValMin As Integer, ByVal ValMax As Integer) As DialogResult
        Dim ff As New CtsControls.InputBox
        ff.Check = Check
        ff.Text = titolo
        ff.Messaggio = testo
        ff.valoreMinimo = ValMin
        ff.valoreMassimo = ValMax
        ff.ShowDialog(formpadre)
        valore = ff.Valore

        Return ff.result
    End Function

    ' Funzione per eseguire il tst della cinghia
    Public Function TestCinghiaEXLs40(ByVal ThNegativo As Short, ByVal ThPositivo As Short) As Integer

        Dim ritorno As Integer
        Dim ret As Short
        Dim valore As Integer

        ritorno = False
        FaseEsec()

        idmsg = 12

        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            StrMessaggio = Messaggio
        Else
            StrMessaggio = "Inserire nella Casella di Testo il Valore del Documento di calibrazione DOCUTEST -042:"
        End If


        If InputboxMy(StrMessaggio, "Attenzione", True, valore, 0, 0) = DialogResult.OK Then
            immamsg = My.Resources._042_04Pallini
            idmsg = 310
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Test Cinghia: Inserire il DOCUTEST --042-04 lato pallini", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            End If
            immamsg = Nothing
        End If


        If (dres = DialogResult.OK) Then
            ret = ls40.TestCinghiaEx(ThNegativo, ThPositivo, valore)

            'se voglio salvare dei dati sul DataBase ...
            FileTraccia.Note = "Valore minimo :" + ls40.ValMinCinghiaNew.ToString + vbCrLf
            FileTraccia.Note += "Valore massimo :" + ls40.ValMaxCinghiaNew.ToString + vbCrLf
            FileTraccia.Note += "NrPicchi :" + ls40.NrPicchi.ToString + vbCrLf
            FileTraccia.Note += "SignalValueRead :" + ls40.SignalValueRead.ToString

            If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
                ritorno = True
            Else
                If (ret = -6000) Then
                    MessageboxMy(FileTraccia.Note, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                Else
                    ViewError(fase)
                End If
                ritorno = False

                ls40.Reset(Ls40Class.Costanti.S_RESET_FREE_PATH)
            End If
        Else
            StrCommento += "Abortito Da Utente "
            ret = Ls40Class.Costanti.S_LS_USER_ABORT
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestCinghia() As Integer

        Dim ritorno As Integer
        Dim ret As Short

        'TCollaudo.SelectedTab = TMicr
        'TMicr.Enabled = True

        ritorno = False
        FaseEsec()

        idmsg = 163

        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Test Cinghia: Inserire il Docutest-067", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        End If

        If (dres = DialogResult.OK) Then
            ret = ls40.TestCinghia()

            If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
                ritorno = True
            Else
                StrCommento += "Reply :" + ret.ToString + " "
            End If

            FileTraccia.Note = "Valore minimo :" + ls40.ValMinCinghia.ToString + vbCrLf
            FileTraccia.Note += "Valore massimo :" + ls40.ValMaxCinghia.ToString
        Else
            StrCommento += "Abortito Da Utente "
            ret = Ls40Class.Costanti.S_LS_USER_ABORT
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestSetVelocita(ByVal Velo As Short) As Integer

        Dim ritorno As Integer
        Dim ret As Short

        'TCollaudo.SelectedTab = TMicr
        'TMicr.Enabled = True
        ritorno = False
        FaseEsec()

        'noyet   ret = ls150.SetVelocita(Velo)

        If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
            ritorno = True
        Else
            StrCommento += "Reply :" + ret.ToString + " "
        End If


        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    ' Funzione che salva i valori dei foto e quelli del CIS

    Public Function TestFotosensori() As Integer
        Dim Reply As Integer
        Dim ritorno As Integer

        Dim write As System.IO.StreamWriter
        Dim stringa As String
        Dim ind As Integer

        Dim pathFile As String


        ritorno = True

        stringa = Application.StartupPath
        ind = stringa.LastIndexOf("\")
        stringa = stringa.Substring(0, ind)

        If IO.Directory.Exists(stringa + "trace") Then
            ' IO.Directory.Delete(zzz + "\Dati", True)
        Else
            IO.Directory.CreateDirectory("trace")
        End If

        pathFile = "\trace\fotosensori.cts"
        stringa = ""

        ' leggo i valori dei Cis
        'noyet   Reply = ls150.LeggiValoriCIS()

        If (Reply = Ls40Class.Costanti.S_LS_OKAY) Then

            If (IO.File.Exists(pathFile) = False) Then
                ' la prima volta scrivo anche l'intestazione del file 
                write = New System.IO.StreamWriter(pathFile, True)
                stringa = "Data Ora Foto1 Foto2 Foto3 Doppia1 Doppia2 Doppia3 Soglia1 Soglia2 Soglia3 Soglia4 Soglia5 Soglia6 "
                stringa += "Add_PWM_Blue_F Add_PWM_Blue_R Add_PWM_Green_F Add_PWM_Green_R Add_PWM_Red_F Add_PWM_Red_R"
                stringa += "Dato_Gain_F Dato_Gain_R Dato_Offset_F Dato_Offset_R"
                stringa += "Dato_PWM_Blue_F Dato_PWM_Blue_R Dato_PWM_Green_F Dato_PWM_Green_R Dato_PWM_Red_F Dato_PWM_Red_R"
                write.WriteLine(stringa)
            Else

                write = New System.IO.StreamWriter(pathFile, True)
            End If

            stringa = System.DateTime.Now.ToString
            stringa += " " + Ls40Class.Periferica.PhotoValue(0).ToString
            stringa += " " + Ls40Class.Periferica.PhotoValue(1).ToString
            stringa += " " + Ls40Class.Periferica.PhotoValue(2).ToString

            stringa += " " + Ls40Class.Periferica.PhotoValueDP(0).ToString
            stringa += " " + Ls40Class.Periferica.PhotoValueDP(1).ToString
            stringa += " " + Ls40Class.Periferica.PhotoValueDP(2).ToString
            ' soglie
            stringa += " " + Ls40Class.Periferica.PhotoValueDP(3).ToString
            stringa += " " + Ls40Class.Periferica.PhotoValueDP(4).ToString
            stringa += " " + Ls40Class.Periferica.PhotoValueDP(5).ToString
            stringa += " " + Ls40Class.Periferica.PhotoValueDP(6).ToString
            stringa += " " + Ls40Class.Periferica.PhotoValueDP(7).ToString
            stringa += " " + Ls40Class.Periferica.PhotoValueDP(8).ToString

            ' scrivo sul file di trace
            write.WriteLine(stringa)
            ' chiudo il file
            write.Close()
        Else
            ' la funzione non è andata a buon fine
            ritorno = False
        End If
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestHubUsb() As Integer
        Dim ret As Integer
        Dim ritorno As Integer

        Dim STR() As String = IO.Directory.GetLogicalDrives()


        Dim fileArray() As String
        Dim fFound As Boolean
        Dim riga As String = ""

        fFound = False
        ret = False
        ritorno = False
        FaseEsec()

        idmsg = 132
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire la chiavetta USB di test nella porta Hub di LS40", "Attenzione !!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        End If


        For Each s As String In STR
            If (s <> "A:\" And s <> "B:\") Then
                Try
                    fileArray = System.IO.Directory.GetFiles(s)
                    'Dim drives As [String]() = System.Environment.GetLogicalDrives()
                    Dim drives As [String]() = diskInfo.GetInformations(s)
2:                  If drives(0) = NOME_CHIAVETTA Then

                        For Each ss As String In fileArray
                            If ss = s + NOME_FILE_CHIAVETTA Then
                                Dim fstr As System.IO.StreamReader
                                fstr = New System.IO.StreamReader(ss)
                                riga = fstr.ReadLine()
                                If riga = NOME_STRINGA_CHIAVETTA Then
                                    fFound = True
                                End If
                            End If
                        Next
                    End If

                Catch ex As Exception

                End Try
            End If
        Next
        If fFound = True Then
            ret = True
            ritorno = True
            MessageboxMy(riga, "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        Else
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        Return ret
    End Function

    'funzione che verifica il funzionamento del DataMatrix su Ls40 
    Public Function TestDataMatrix(ByVal ncili As Short, ByVal StrDaControllare As String, ByVal notUsed As String) As Integer

        Dim ritorno As Short
        Dim ret As Short
        Dim fstop As Short
        Dim penna As New Pen(System.Drawing.Color.Red)
        Dim cicli As Short
        Dim font1 As New Font("Time New Roman", 8, FontStyle.Regular, GraphicsUnit.Pixel)
        Dim fAspetta As CtsControls.FWait
        Dim fcicla As Boolean



        ritorno = False
        FaseEsec()

        cicli = 1
        If (fSoftware = SW_BAR2D Or fSoftware = SW_TOP_BAR2D Or fSoftware = SW_BARCODE_MICROHOLE) Then
            'fdecode = 1
            fAspetta = New CtsControls.FWait
            fAspetta.Label1.Text = "Attendere... decodifica BARCODE in corso...."

            If (StrDaControllare = "") Then
                StrDaControllare = "ARCA - DataMatrix decoding docutest - 112233445566778899"
            End If

            fcicla = True
            While (fcicla)

                idmsg = 287
                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                    dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                Else
                    dres = MessageboxMy("Inserire Documento di test provissiorio per verificare DataMatrix", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                End If

                If (dres <> DialogResult.OK) Then
                    ret = Ls40Class.Costanti.S_LS_USER_ABORT
                    fcicla = False
                    fstop = True
                Else
                    DisposeImg()
                    StrMessaggio = "TestDataMatrix"

                    ls40.NomeFileImmagine = zzz + "\Dati\ImageBarcode2D" + IndImgF.ToString("0000") + ".bmp"
                    ls40.NomeFileImmagineRetro = zzz + "\Dati\ImageBarcode2DR" + IndImgR.ToString("0000") + ".bmp"

                    fAspetta.Show()
                    ret = ls40.DecodificaDataMatrix(System.Convert.ToSByte(Ls40Class.Costanti.S_SIDE_ALL_IMAGE), Ls40Class.Costanti.S_SCAN_MODE_256GR200)
                    fAspetta.Hide()


                    Select Case ret
                        Case Ls40Class.Costanti.S_LS_PAPER_JAM, + _
                        Ls40Class.Costanti.S_LS_JAM_AT_MICR_PHOTO, + _
                        Ls40Class.Costanti.S_LS_JAM_AT_SCANNERPHOTO, + _
                        Ls40Class.Costanti.S_LS_JAM_DOC_TOOLONG
                            idmsg = 111
                            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                            Else
                                dres = MessageboxMy("ATTENZIONE CARTA BLOCCATA LIBERARE IL PERCORSO", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                            End If

                            If (dres <> Ls40Class.Costanti.S_LS_OKAY) Then
                                ret = Ls40Class.Costanti.S_LS_USER_ABORT
                                fstop = True
                            Else
                                ls40.Reset(Ls40Class.Costanti.S_RESET_FREE_PATH)
                            End If
                            fcicla = False
                        Case Ls40Class.Costanti.S_LS_FEEDEREMPTY
                            fstop = True


                        Case Ls40Class.Costanti.S_LS_OKAY

                            ret = Ls40Class.Costanti.S_LS_OKAY
                            'fcicla = False


                            DisposeImg()

                            PFronte.Image = System.Drawing.Image.FromFile(zzz + "\Dati\ImageBarcode2D" + IndImgF.ToString("0000") + ".bmp")
                            PRetro.Image = System.Drawing.Image.FromFile(zzz + "\Dati\ImageBarcode2DR" + IndImgR.ToString("0000") + ".bmp")
                            Pimage.Image = PFronte.Image
                            PImageBack.Image = PRetro.Image
                            IndImgF += 1
                            IndImgR += 1

                            'If dres = DialogResult.Cancel Then
                            '    ret = -11111
                            'End If



                            If (Left(ls40.DataMatrixLetto, 55) = Left(StrDaControllare, 55)) Then
                                dres = MessageboxMy(ls40.DataMatrixLetto + vbCrLf + "Decodifica Barcode2D OK!", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                                ritorno = True
                                'fcicla = False
                            Else
                                dres = MessageboxMy(ls40.DataMatrixLetto + vbCrLf + "Decodifica Barcode2D KO!, valori letti non corretti", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                ritorno = False
                                ret = Ls40Class.Costanti.S_LS_USER_ABORT
                                fcicla = False
                            End If

                        Case Ls40Class.Costanti.S_LS_BARCODE_GENERIC_ERROR

                            MessageboxMy("Decodifica BARCODE2D NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                            ritorno = False
                            fcicla = False

                        Case Ls40Class.Costanti.S_LS_BARCODE_NOT_DECODABLE

                            MessageboxMy("Decodifica BARCODE2D NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                            ritorno = False
                            fcicla = False

                        Case Ls40Class.Costanti.S_LS_BARCODE_OPENFILE_ERROR

                            MessageboxMy("Decodifica BARCODE2D NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                            ritorno = False
                            fcicla = False

                        Case Ls40Class.Costanti.S_LS_BARCODE_READBMP_ERROR

                            MessageboxMy("Decodifica BARCODE2D NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                            ritorno = False
                            fcicla = False

                        Case Ls40Class.Costanti.S_LS_BARCODE_MEMORY_ERROR

                            MessageboxMy("Decodifica BARCODE2D NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                            ritorno = False
                            fcicla = False

                        Case Ls40Class.Costanti.S_LS_BARCODE_START_NOTFOUND

                            MessageboxMy("Decodifica BARCODE2D NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                            ritorno = False
                            fcicla = False

                        Case Ls40Class.Costanti.S_LS_BARCODE_STOP_NOTFOUND

                            MessageboxMy("Decodifica BARCODE2D NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                            ritorno = False
                            fcicla = False



                    End Select
                End If
                If cicli = ncili Then
                    fcicla = False
                End If
                cicli += 1
            End While
            fAspetta.Close()

        Else
            ritorno = True
            ret = 0
        End If


        FileTraccia.UltimoRitornoFunzione = ret
        If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
            ritorno = True
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante

        Return ritorno
    End Function

    Public Function CheckRegistry() As Boolean
        Dim ret As Boolean
        ' verifico che nel registro siano disabilitate le funzioni 
        ' per l'auto play'
        ' se non c'è il valore corretto l' imposto e faccio shut down del PC

        Dim regKey As Microsoft.Win32.RegistryKey
        Dim ver As Decimal
        regKey = Microsoft.Win32.Registry.Users.OpenSubKey(".DEFAULT\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", True)
        'regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(".DEFAULT\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", True)
        'regKey.SetValue("AppName", "MyRegApp")
        ver = regKey.GetValue("NoDriveTypeAutoRun", 0.0)
        If ver <> 181 Then
            regKey.SetValue("NoDriveTypeAutoRun", 181)
            MessageboxMy("Trovato sistema non ottimizzato.....", "Riavvio di Windows...", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
            Shell("shutdown.exe -r")
            'Me.Close()
        End If
        regKey.Close()
        Return ret
    End Function

    Public Function TestSerialNumberPiastra() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim fser As New CtsControls.FSerialnum
        ritorno = False
        FaseEsec()

        idmsg = 155

        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            fser.Text = Messaggio
        Else
            fser.Text = "Inserire SerialNumber Piastra"
        End If




        If fser.ShowDialog() = DialogResult.OK Then
            If fser.StrSerialNumber.Length = LUNGHEZZA_SERIAL_NUMBER_PIASTRA Then
                SerialNumberPiastra = fser.StrSerialNumber
                FileTraccia.VersioneDll = SerialNumberPiastra
                ritorno = True

                '#If COLLAUDO_PIASTRE Then
                '                ''solo per il CollaudoPiastre
                '                'MessageboxMy("Chiudure la fixture , accendere la periferica , attendere il BIP e premere OK per continuare", "Collaudo Piastra Ls40", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                '#End If
            Else
                ViewError(fase)
            End If

        Else
            ViewError(fase)
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = "Test Serial number Piastra"
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function

    Public Function TestSNPerStampaPiastra() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim fser As New CtsControls.FSerialnum
        ritorno = False
        FaseEsec()
        ls40.ReturnC.FunzioneChiamante = "TestSNPerStampaPiastra"

        idmsg = 155
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            fser.Text = Messaggio
        Else
            fser.Text = "Inserire il Serial number della periferica da collaudare"
        End If


        If fser.ShowDialog() = DialogResult.OK Then
            If fser.StrSerialNumber.Length = LUNGHEZZA_SERIAL_NUMBER_PIASTRA Then
                SerialNumberDaStampare = fser.StrSerialNumber

                MessageboxMy("Chiudure la fixture , accendere la periferica , attendere il BIP e premere OK per continuare", "Collaudo Piastra Ls40", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)

                If ret = Ls40Class.Costanti.S_LS_OKAY Then
                    ritorno = True
                Else
                    ViewError(fase)
                End If
            Else
                ret = Ls40Class.Costanti.S_LS_STRINGTRUNCATED
                ViewError(fase)
            End If


        Else
            ViewError(fase)
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestSNPerStampa() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim fser As New CtsControls.FSerialnum
        ritorno = False
        FaseEsec()
        ls40.ReturnC.FunzioneChiamante = "TestSNPerStampa"

        idmsg = 154
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            fser.Text = Messaggio
        Else
            fser.Text = "Inserire il Serial number della periferica da collaudare"
        End If


        If fser.ShowDialog() = DialogResult.OK Then
            If fser.StrSerialNumber.Length = LUNGHEZZA_SERIAL_NUMBER Then
                SerialNumberDaStampare = fser.StrSerialNumber



                If ret = Ls40Class.Costanti.S_LS_OKAY Then
                    ritorno = True
                Else
                    ViewError(fase)
                End If
            Else
                ret = Ls40Class.Costanti.S_LS_STRINGTRUNCATED
                ViewError(fase)
            End If


        Else
            ViewError(fase)
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    'nome funzione da inserire nel DB
    Public Function TestMicroForatura(ByVal ncili As Short) As Integer

        Dim ritorno As Short
        Dim ret As Short
        Dim fstop As Short
        Dim penna As New Pen(System.Drawing.Color.Red)
        Dim cicli As Short
        Dim font1 As New Font("Time New Roman", 8, FontStyle.Regular, GraphicsUnit.Pixel)
        'Dim fAspetta As CtsControls.FWait
        Dim fcicla As Boolean
        Dim fdecode As Short
        Dim strapp As String


        cicli = 1
        'If (fSoftware = SW_BARCODE_MICROHOLE Or fSoftware = SW_BARCODE_MICROHOLE_IQA Or fSoftware = SW_MICROHOLE) Then
        'fZebrato = True
        If (fZebrato = True) Then
            fdecode = 1

            If fdecode = 1 Then
                fcicla = True
                While (fcicla)



                    'idmsg = 279 ' Inserire DOCUTEST -035 per testare la MICROFORATURA
                    idmsg = 315 'Inserire un documento DOCUTEST-0148 per verificare l'immagine 
                    'idmsg = 165 'DEFAULT 
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy("Inserire un Documento per testare la MICROFORATURA", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                    End If

                    If (dres <> DialogResult.OK) Then
                        'ret = Ls40Class.Costanti.S_LS_OKAY
                        ret = Ls40Class.Costanti.S_LS_USER_ABORT
                        fcicla = False
                        fstop = True
                    Else
                        DisposeImg()
                        StrMessaggio = "Test MicroForatura"

                        'ls40.NomeFileImmagine = zzz + "\Dati\ImageBarcode2D" + IndImgF.ToString("0000") + ".bmp"
                        'ls40.NomeFileImmagineRetro = zzz + "\Dati\ImageBarcode2DR" + IndImgR.ToString("0000") + ".bmp"

                        ret = ls40.TestMicroForatura()

                        Select Case ret
                            Case Ls40Class.Costanti.S_LS_PAPER_JAM
                                idmsg = 111 'ATTENZIONE CARTA BLOCCATA LIBERARE IL PERCORSO
                                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                    dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                                Else
                                    dres = MessageboxMy("ATTENZIONE CARTA BLOCCATA LIBERARE IL PERCORSO", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                End If

                                If (dres <> DialogResult.OK) Then
                                    ret = Ls40Class.Costanti.S_LS_USER_ABORT
                                    fstop = True
                                Else
                                    ls40.Reset(Ls40Class.Costanti.S_RESET_FREE_PATH)
                                End If
                                fcicla = False
                            Case Ls40Class.Costanti.S_LS_FEEDEREMPTY
                                fstop = True


                            Case Ls40Class.Costanti.S_LS_OKAY

                                SalvaDati()
                                ret = Ls40Class.Costanti.S_LS_OKAY
                                fcicla = False


                                'DisposeImg()
                                'FileZoomCorrente = zzz + "\Dati\cardImageC" + IndImgF.ToString("0000") + ".bmp"
                                'PFronte.Image = System.Drawing.Image.FromFile(zzz + "\Dati\ImageBarcode2D" + IndImgF.ToString("0000") + ".bmp")
                                'PRetro.Image = System.Drawing.Image.FromFile(zzz + "\Dati\ImageBarcode2DR" + IndImgR.ToString("0000") + ".bmp")
                                'Pimage.Image = PFronte.Image
                                'PImageBack.Image = PRetro.Image
                                'IndImgF += 1
                                'IndImgR += 1

                                'If dres = DialogResult.Cancel Then
                                '    ret = -11111
                                'End If
                            Case Else
                                If (ret = 125) Then
                                    strapp = "Scanner Zebrato Montato al contrario"
                                Else
                                    strapp = "Errore sulla Verifica Microforatura" + ret.ToString()

                                End If
                                dres = MessageboxMy(strapp, "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Question, Nothing)


                        End Select
                    End If
                    If cicli = ncili Then
                        fcicla = False
                    End If
                    cicli += 1
                End While
            Else
                ' non ho fatto nulla, e setto tutto a ok
                ret = Ls40Class.Costanti.S_LS_OKAY
            End If
        Else
            ritorno = True
            ret = 0
        End If




        FileTraccia.UltimoRitornoFunzione = ret
        If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
            ritorno = True
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        'fAspetta.Close()
        Return ritorno
    End Function



    Public Function AddRigaTabella(ByVal Tab As DataTable, ByVal s1 As String, ByVal s2 As String, ByVal s3 As String, ByVal s4 As String) As DataRow
        Dim row As DataRow
        row = Tab.NewRow()
        row.Item(0) = s1
        row.Item(1) = s2
        row.Item(2) = s3
        row.Item(3) = s4

        Tab.Rows.Add(row)
        Return row
    End Function

    Public Function AddRigaTabella2(ByVal Tab As DataTable, ByVal s1 As String, ByVal s2 As String) As DataRow
        Dim row As DataRow
        row = Tab.NewRow()
        row.Item(0) = s1
        row.Item(1) = s2

        Tab.Rows.Add(row)
        Return row
    End Function

    Public Function AddRigaTabella1(ByVal Tab As DataTable, ByVal s1 As String) As DataRow
        Dim row As DataRow
        row = Tab.NewRow()
        row.Item(0) = s1

        Tab.Rows.Add(row)
        Return row
    End Function

    Public Function CercaMessaggio(ByVal filename As String, ByRef Id As Integer, ByRef a As String, ByRef b As String, ByRef c As String, ByRef d As String, ByRef img As Image) As Boolean

        Dim freturn As Boolean
        Dim prova As System.Xml.XmlDocument = New System.Xml.XmlDocument()

        freturn = False
        prova.Load(filename)
        Dim xx As XmlNodeList = prova.GetElementsByTagName("T_Messaggi")

        For Each n As XmlNode In xx

            Dim lll As XmlNodeList = n.ChildNodes
            Dim noddo As XmlNode = n.ChildNodes(0)
            If (noddo.InnerText = Id.ToString()) Then
                a = (n.ChildNodes(0).InnerText)
                b = (n.ChildNodes(1).InnerText)
                c = (n.ChildNodes(2).InnerText)

                d = (n.ChildNodes(3).InnerText)

                If (Not (n.ChildNodes(4) Is Nothing)) Then

                    Dim ms As System.IO.MemoryStream
                    ms = New System.IO.MemoryStream()
                    Dim vet(50 * 1024) As Byte


                    vet = System.Convert.FromBase64CharArray(n.ChildNodes(4).InnerText.ToCharArray(), 0, n.ChildNodes(4).InnerText.Length)

                    ms.Write(vet, 0, vet.Length)
                    img = Image.FromStream(ms)
                End If
                freturn = True
            End If
        Next

        Return freturn
    End Function

    Public Function SalvaDati() As Integer
        Dim ritorno As Integer
        ritorno = 0
        DatiFine.lista.Clear()
        'DatiFine.AddDato(DatiFineCollaudo.TipoDato.DUMP, Ls150Class.Periferica.BufferDump)

        DatiFine.AddDato(DatiFineCollaudo.TipoDato.FOTO1, BitConverter.GetBytes(Ls40Class.Periferica.PhotoValue(0)))
        DatiFine.AddDato(DatiFineCollaudo.TipoDato.CINGHIA_MIN, BitConverter.GetBytes(ls40.ValMinCinghia))
        DatiFine.AddDato(DatiFineCollaudo.TipoDato.CINGHIA_MAX, BitConverter.GetBytes(ls40.ValMaxCinghia))

        If (fDoubleLeafingPresente = True) Then
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.DOPPIA1, BitConverter.GetBytes(Ls40Class.Periferica.PhotoValueDP(0)))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.DOPPIA2, BitConverter.GetBytes(Ls40Class.Periferica.PhotoValueDP(1)))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.DOPPIA3, BitConverter.GetBytes(Ls40Class.Periferica.PhotoValueDP(2)))

            DatiFine.AddDato(DatiFineCollaudo.TipoDato.DP_TH1, BitConverter.GetBytes(Ls40Class.Periferica.PhotoValueDP(3)))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.DP_TH2, BitConverter.GetBytes(Ls40Class.Periferica.PhotoValueDP(4)))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.DP_TH3, BitConverter.GetBytes(Ls40Class.Periferica.PhotoValueDP(5)))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.DP_TH4, BitConverter.GetBytes(Ls40Class.Periferica.PhotoValueDP(6)))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.DP_TH5, BitConverter.GetBytes(Ls40Class.Periferica.PhotoValueDP(7)))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.DP_TH6, BitConverter.GetBytes(Ls40Class.Periferica.PhotoValueDP(8)))
        End If

        If (fZebrato = True) Then
            '15-05-2015
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_ERRORFLAGS, BitConverter.GetBytes(ls40.errorFlags))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_innerLevValidPerc, BitConverter.GetBytes(ls40.innerLevValidPerc))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_innerDimValidPerc, BitConverter.GetBytes(ls40.innerDimValidPerc))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_outerDimValidPerc, BitConverter.GetBytes(ls40.outerDimValidPerc))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_outerLevValidPerc, BitConverter.GetBytes(ls40.outerLevValidPerc))

            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_wDimStatmean, BitConverter.GetBytes(ls40.wDimStatmean))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_wDimStatStdDev, BitConverter.GetBytes(ls40.wDimStatStdDev))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_bDimStatmean, BitConverter.GetBytes(ls40.bDimStatmean))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_bDimStatStdDev, BitConverter.GetBytes(ls40.bDimStatStdDev))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_gDimStatmean, BitConverter.GetBytes(ls40.gDimStatmean))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_gDimStatDev, BitConverter.GetBytes(ls40.gDimStatDev))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_wLevStatmean, BitConverter.GetBytes(ls40.wLevStatmean))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_wLevStatStdDev, BitConverter.GetBytes(ls40.wLevStatStdDev))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_bLevStatmean, BitConverter.GetBytes(ls40.bLevStatmean))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.MICROHOLE_bLevStatStdDev, BitConverter.GetBytes(ls40.bLevStatStdDev))
        End If


        Return ritorno
    End Function

    Public Function TestSmartCard() As Integer
        Dim strCard As String
        Dim stracardn As String
        Dim ritorno As Integer
        Dim ret As Integer
        ' inizializzazioni variabili 

        FaseEsec()
        ritorno = False

        idmsg = 9999
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire una SmartCard nel lettore", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(5))
        End If

        strCard = ""
        stracardn = "Test Smart Card: Card inizializzata dal programma di collaudo ---" + Date.Now.ToString() + " "
        ret = ls40.ScriviCard(stracardn)
        ls40.LeggiCard()
        MessageboxMy(ls40.strRead, "Collaudo", MessageBoxButtons.OK, MessageBoxIcon.Information, Nothing)
        stracardn = "Test Smart Card: Lettore funzionante in lettura e scrittura  ---" + Date.Now.ToString() + " "
        ret = ls40.ScriviCard(stracardn)

        If ls40.LeggiCard() = 0 Then
            ritorno = True
        End If


        MessageboxMy(ls40.strRead, "Collaudo", MessageBoxButtons.OK, MessageBoxIcon.Information, Nothing)

        MessageboxMy("Ricordarsi di togliere la carta dal lettore", "Collaudo", MessageBoxButtons.OK, MessageBoxIcon.Information, Nothing)
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ls40.ReturnC.ReturnCode
        Return ritorno
    End Function

    Public Function TestQualitaImmagine(ByVal side As System.Byte, Optional ByVal ScanMode As Short = 0) As Integer
        Dim ritorno As Integer
        Dim retZoom As DialogResult
        Dim ret As Integer
        Dim str As String
        Dim localDate As String
        Dim hour As String

        'Dim Iquali As Image

        'Dim ftestSfondo As Boolean
        'Dim stringaApp As String

        Fqualit = New CtsControls.FqualImg

        ritorno = False
        'fquality = True


        localDate = Now.ToString("yyyyMMdd")
        hour = Now.ToString("hhmmss")

        If (ScanMode = Ls40Class.Costanti.S_SCAN_MODE_256GR200_AND_UV) Then
            ScanMode = Ls40Class.Costanti.S_SCAN_MODE_256GR300_AND_UV
        End If
        'in caso di scanner a colori forzo uscita se viene chiesta l'immagine UV
        'la fase UV viene sempre caricata , to do...
        'If (fScannerType = TIPO_SCANNER_COLORI And ScanMode = Ls40Class.Costanti.S_SCAN_MODE_256GR200_AND_UV) Then
        If (fScannerType = TIPO_SCANNER_COLORI And ScanMode = Ls40Class.Costanti.S_SCAN_MODE_256GR300_AND_UV) Then
            FaseOk()
            FileTraccia.UltimoRitornoFunzione = 0 'replycode
            FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
            ritorno = True
            FileTraccia.ScannerF = 1
            'BVisualZoom = False
            Return ritorno 'OCCHIO !!!!!!!!!!!!
        End If

        'in caso di scanner a UV forzo uscita se viene chiesta l'immagine a COLORI
        'la fase Qualita' Immagini  viene sempre caricata , to do...
        If (fScannerType = TIPO_SCANNER_UV And ScanMode = Ls40Class.Costanti.S_SCAN_MODE_COLOR_200) Then
            FaseOk()
            FileTraccia.UltimoRitornoFunzione = 0 'replycode
            FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
            ritorno = True
            FileTraccia.ScannerF = 1
            'BVisualZoom = False
            Return ritorno 'OCCHIO !!!!!!!!!!!!
        End If


        If (fScannerType = TIPO_SCANNER_GRIGIO And (ScanMode = Ls40Class.Costanti.S_SCAN_MODE_COLOR_200 Or ScanMode = Ls40Class.Costanti.S_SCAN_MODE_256GR300_AND_UV)) Then
            FaseOk()
            FileTraccia.UltimoRitornoFunzione = 0 'replycode
            FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
            ritorno = True
            FileTraccia.ScannerF = 1
            'BVisualZoom = False
            Return ritorno 'OCCHIO !!!!!!!!!!!!
        End If

        FaseEsec()
        If (System.Convert.ToInt16(side) = Ls40Class.Costanti.S_SIDE_FRONT_IMAGE) Then
            str = "FRONTE"
            'ls40.NomeFileImmagine = zzz + "\Quality\FrontImage" + IndImgF.ToString("0000") + ".bmp"
        Else
            str = "RETRO"
            'ls40.NomeFileImmagine = zzz + "\Quality\BackImage" + IndImgR.ToString("0000") + ".bmp"
        End If

        If (SerialNumberDaStampare = Nothing) Then
            SerialNumberDaStampare = "PROGETTO"
        End If

        If (SerialNumberPiastra = Nothing) Then
            SerialNumberPiastra = "PROGETTO_PIASTRA"
        End If

        ls40.NomeFileImmagine = zzz + "\Images_Scanner\FrontImage" + SerialNumberDaStampare.ToString + "_" + SerialNumberPiastra.ToString + "_" + IndImgF.ToString("0000") + "_" + localDate.ToString + "_" + hour.ToString & ".bmp"
        ls40.NomeFileImmagineUV = zzz + "\Images_Scanner\FrontImageUV" + SerialNumberDaStampare.ToString + "_" + SerialNumberPiastra.ToString + "_" + IndImgF.ToString("0000") + "_" + localDate.ToString + "_" + hour.ToString & ".bmp"
        ls40.NomeFileImmagineRetro = zzz + "\Images_Scanner\BackImage" + SerialNumberDaStampare.ToString + "_" + SerialNumberPiastra.ToString + "_" + IndImgR.ToString("0000") + "_" + localDate.ToString + "_" + hour.ToString & ".bmp"


        'idmsg = 165 'Inserire un documento DOCUTEST -035  per verificare l'immagine  
        idmsg = 315 'Inserire un documento DOCUTEST-0148  per verificare l'immagine  
        Select Case ScanMode
            Case Ls40Class.Costanti.S_SCAN_MODE_COLOR_100, Ls40Class.Costanti.S_SCAN_MODE_COLOR_200, Ls40Class.Costanti.S_SCAN_MODE_COLOR_AND_RED_100, Ls40Class.Costanti.S_SCAN_MODE_COLOR_AND_RED_200
                'Case Ls515Class.Costanti.S_SCAN_MODE_COLOR_200
                'Case Ls515Class.Costanti.S_SCAN_MODE_COLOR_AND_RED_100
                'Case Ls515Class.Costanti.S_SCAN_MODE_COLOR_AND_RED_200
                'ScanMode = Ls515Class.Costanti.S_SCAN_MODE_COLOR_200
                str += " Colore"

            Case Ls40Class.Costanti.S_SCAN_MODE_256GR100_AND_UV, Ls40Class.Costanti.S_SCAN_MODE_256GR200_AND_UV, Ls40Class.Costanti.S_SCAN_MODE_256GR300_AND_UV
                'Case Ls515Class.Costanti.S_SCAN_MODE_256GR300_AND_UV
                'Case Ls515Class.Costanti.S_SCAN_MODE_256GR300_AND_UV
                'ScanMode = Ls515Class.Costanti.S_SCAN_MODE_COLOR_200
                str += " UV"
                'idmsg = 273 'Inserire il Docutest 69 per controllo qualità immagine UV
                idmsg = 315 'Inserire un documento DOCUTEST-0148  per verificare l'immagine  
            Case Else
                ScanMode = Ls40Class.Costanti.S_SCAN_MODE_256GR300

        End Select


        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy2(Messaggio + str, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire un documento per verificare l'immagine " + str, "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Question, VetImg(7))
        End If

        If dres = DialogResult.OK Then

            ret = ls40.LeggiImmagini(side, ScanMode)
            If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
                If (System.Convert.ToInt16(side) = Ls40Class.Costanti.S_SIDE_FRONT_IMAGE) Then



                    'If IO.Directory.Exists(zzz + "\Test") Then
                    'Else
                    '    IO.Directory.CreateDirectory(zzz + "\Test")
                    'End If

                    'System.IO.File.Copy(ls40.NomeFileImmagine, zzz + "\Test\FR" + DateTime.Now().ToString("ddMM-hhmmss") + ".bmp")

                    'Application.DoEvents()
                    Try
                        DisposeImg()
                        'If (ScanMode = Ls40Class.Costanti.S_SCAN_MODE_256GR200_AND_UV) Then
                        If (ScanMode = Ls40Class.Costanti.S_SCAN_MODE_256GR300_AND_UV) Then
                            Pimage.Image = System.Drawing.Image.FromFile(ls40.NomeFileImmagineUV)
                        Else
                            Pimage.Image = System.Drawing.Image.FromFile(ls40.NomeFileImmagine)
                        End If
                        IndImgF += 1
                    Catch ex As Exception
                        ' DEBUG
                        MessageBox.Show(ex.Message + " " + ex.Source + ret.ToString, "errore")
                    End Try



                    'ftestSfondo = TestSfondo(Pimage.Image, 32, RigheContigueFronte, RigheTotaliFronte)
                    'stringaApp = "Fronte --> Righe contigue: " + RigheContigueFronte.ToString("000") + " Righe Totali: " + RigheTotaliFronte.ToString("000")
                Else

                    'If IO.Directory.Exists(zzz + "\Test") Then
                    'Else
                    '    IO.Directory.CreateDirectory(zzz + "\Test")
                    'End If

                    'System.IO.File.Copy(ls40.NomeFileImmagine, zzz + "\Test\RR" + DateTime.Now().ToString("ddMM-hhmmss") + ".bmp")

                    Try
                        DisposeImg()
                        PImageBack.Image = System.Drawing.Image.FromFile(ls40.NomeFileImmagineRetro)
                        IndImgR += 1
                    Catch ex As Exception
                        ' DEBUG
                        MessageBox.Show(ex.Message + " " + ex.Source + ret.ToString, "errore")
                    End Try

                    'ftestSfondo = TestSfondo(PImageBack.Image, 32, RigheContigueRetro, RigheTotaliRetro)
                    'stringaApp = "Retro --> Righe contigue: " + RigheContigueRetro.ToString("000") + " Righe Totali: " + RigheTotaliRetro.ToString("000")
                End If
                Application.DoEvents()

                Dim a As Short
                If (System.Convert.ToInt16(side) = Ls40Class.Costanti.S_SIDE_FRONT_IMAGE) Then


                    'BVisualZoom = True
                    DirZoom = Application.StartupPath

                    a = DirZoom.LastIndexOf("\")
                    DirZoom = DirZoom.Substring(0, a)
                    DirZoom += "\Images_Scanner\"
                    'If (ScanMode = Ls40Class.Costanti.S_SCAN_MODE_256GR200_AND_UV) Then
                    If (ScanMode = Ls40Class.Costanti.S_SCAN_MODE_256GR300_AND_UV) Then
                        FileZoomCorrente = ls40.NomeFileImmagineUV
                    Else
                        FileZoomCorrente = ls40.NomeFileImmagine
                    End If
                    ' Zoom Immagine 
                    Try
                        retZoom = ShowZoom(DirZoom, FileZoomCorrente)
                    Catch ex As Exception
                        MessageBox.Show(ex.Message + " " + ex.Source + retZoom.ToString, "errore")
                    End Try
                End If

                If (System.Convert.ToInt16(side) = Ls40Class.Costanti.S_SIDE_BACK_IMAGE) Then
                    'If retZoom = DialogResult.OK Then
                    'BVisualZoom = True
                    DirZoom = Application.StartupPath

                    a = DirZoom.LastIndexOf("\")
                    DirZoom = DirZoom.Substring(0, a)
                    DirZoom += "\Images_Scanner\"
                    FileZoomCorrente = ls40.NomeFileImmagineRetro
                    ' Zoom Immagine 
                    Try
                        retZoom = ShowZoom(DirZoom, FileZoomCorrente)
                    Catch ex As Exception
                        MessageBox.Show(ex.ToString())
                    End Try

                    'End If
                End If

                If retZoom = DialogResult.OK Then
                    ritorno = True
                Else
                    Fqualit.ShowDialog()

                    Select Case Fqualit.ret
                        Case 0
                            ritorno = False
                        Case 1  'calibra e visualizza
                            If (fScannerType = TIPO_SCANNER_UV) Then
                                'ritorno = CalibrazioneScannerUV() ' calbrazione scanner UV 
                                'If ritorno = True Then
                                ritorno = TestScannersPeriferica() 'calibrazione scanner GRIGIO e COLORE + UV
                                If ritorno = True Then
                                    'ritorno = TestQualitaImmagine(70) ' faccio vedere img front grigio / colore 
                                    'ritorno = TestQualitaImmagine(70, Ls40Class.Costanti.S_SCAN_MODE_256GR200_AND_UV)
                                    ritorno = TestQualitaImmagine(70, Ls40Class.Costanti.S_SCAN_MODE_256GR300_AND_UV)
                                    If ritorno = True Then
                                        'ritorno = TestQualitaImmagine(70) ' faccio vedere img front grigio / colore 
                                        'ritorno = TestQualitaImmagine(66) ' faccio vedere img retro grigio / colore 
                                        If (ritorno = True) Then
                                            'ritorno = TestQualitaImmagine(70, Ls40Class.Costanti.S_SCAN_MODE_256GR200_AND_UV) ' faccio vedere img UV 
                                            ritorno = TestQualitaImmagine(66) ' faccio vedere img retro grigio / colore 
                                        End If
                                    End If

                                    'End If
                                End If
                            Else
                                ritorno = TestScannersPeriferica() 'calibrazione scanner GRIGIO e COLORE
                                If (ritorno = True And fScannerType = TIPO_SCANNER_COLORI) Then
                                    ritorno = TestQualitaImmagine(70, Ls40Class.Costanti.S_SCAN_MODE_COLOR_200)
                                    If (ritorno = True) Then
                                        ritorno = TestQualitaImmagine(66, Ls40Class.Costanti.S_SCAN_MODE_COLOR_200)
                                    End If
                                Else
                                    If ritorno = True Then
                                        ritorno = TestQualitaImmagine(70)
                                        If ritorno = True Then
                                            ritorno = TestQualitaImmagine(66)
                                        End If
                                    End If
                                End If
                            End If
                        Case 2  'Visualizza SOLO
                            If (fScannerType = TIPO_SCANNER_COLORI) Then
                                ritorno = TestQualitaImmagine(side, Ls40Class.Costanti.S_SCAN_MODE_COLOR_200)
                            ElseIf (fScannerType = TIPO_SCANNER_UV) Then
                                'ritorno = TestQualitaImmagine(side, Ls40Class.Costanti.S_SCAN_MODE_256GR200_AND_UV)
                                ritorno = TestQualitaImmagine(side, Ls40Class.Costanti.S_SCAN_MODE_256GR300_AND_UV)
                            Else
                                ritorno = TestQualitaImmagine(side)
                            End If

                    End Select
                End If

                'DisposeImg()
            Else ' Leggi immagini non a buon ine
                ViewError(fase)
                Select Case ret
                    Case Ls40Class.Costanti.S_LS_PAPER_JAM
                        ret = ls40.Reset(Ls40Class.Costanti.S_RESET_FREE_PATH)
                End Select
            End If
        Else
            ret = Ls40Class.Costanti.S_LS_USER_ABORT
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If

        'BVisualZoom = False
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante

        Return ritorno
    End Function

    Public Function TestRetained(ByVal NR_cicli As Short)
        Dim ritorno As Integer
        Dim ret As Integer
        Dim fstop As Short
        Dim cicli As Short
        Dim ret_not_used As Integer


ripeti_test:
        ritorno = False
        fstop = False
        cicli = 1


        If (fAtmConfiguration = True) Then
            ritorno = True
        Else


            idmsg = 296
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Inserire un documento DOCUTEST -045-03 per prova RETAINED", "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Question, VetImg(7))
            End If

            If dres = DialogResult.OK Then

                While (fstop = False)

                    ret = ls40.TestRetained()

                    If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
                        fstop = False
                        ritorno = True
                    Else
                        fstop = True
                        ViewError(fase)
                        ret_not_used = ls40.Reset(Ls40Class.Costanti.S_RESET_FREE_PATH)
                    End If

                    If cicli = NR_cicli Then
                        fstop = True
                    End If
                    cicli += 1
                End While

                If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
                    cicli = 1
                Else
                    'devo ricalibrare lo scanner
                    ritorno = TestScannersPeriferica() 'calibrazione scanner GRIGIO e COLORE + UV
                    If ritorno = True Then
                        ritorno = False
                        GoTo ripeti_test
                    End If
                End If
            End If
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If

        'BVisualZoom = False
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante

        Return ritorno
    End Function

    Function StampaReport()


        Dim nomeFileLog As String = Application.StartupPath & "\Report40\" & FileTraccia.Matricola & "_" & DateTime.Now.ToString("yyyyMMdd_hhmmss") & ".txt"
        Dim headerLine As String = ""
        Dim recordLine As String = ""
        Dim larghezza As Integer = 62

        FaseEsec()

        ls40.ReturnC.FunzioneChiamante = "Stampa Report"
        FileTraccia.UltimoRitornoFunzione = 0
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante

        recordLine = "┌────────────────────────── ARCA ─────────────────────────────┐" & vbNewLine & _
                    ("│ Peripheral Name:                   " & FileTraccia.Periferica).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Product Line:                      " & FileTraccia.LineaProd).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ S/N #:                             " & FileTraccia.Matricola).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Product Code:                      " & FileTraccia.Ordine_prod).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Date :                             " & FileTraccia.DataCollaudo).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Manufacter :                       " & FileTraccia.Manufacter).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Model :                            " & FileTraccia.Periferica).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Firmware Version :                 " & FileTraccia.VersioneFw).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Tested By :                        " & FileTraccia.Operatore & "-" & FileTraccia.NomeStazione).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Test Duration :                    " & FileTraccia.Durata_Test.Minutes.ToString + ":" + FileTraccia.Durata_Test.Seconds.ToString).PadRight(larghezza) & "│" & vbNewLine & _
                     "│                                                             │" & vbNewLine & _
                     "├────────────────── COMPLETED AUTOMATED TESTS ────────────────┤" & vbNewLine & _
                     "│                                                             │" & vbNewLine & _
                    ("│ Sensor Test:                             " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Calibration Front Scanner:               " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Quality Front Image:                     " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Calibration Rear Scanner:                " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Quality Rear Image:                      " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Double Leafing Sensor Calibration:       " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Double Leafing Sensor Test:              " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ MICR Head Calibration:                   " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ MICR Speed Test:                         " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ MICR Reader Test:                        " & "PASSED").PadRight(larghezza) & "│" & vbNewLine


        If (fTimbri = True) Then
            recordLine += ("│ Stamp Test:                              " & "PASSED").PadRight(larghezza) & "│" & vbNewLine
        End If

        If (fStampante = True) Then
            recordLine += ("│ Ink Jet Printer Test:                    " & "PASSED").PadRight(larghezza) & "│" & vbNewLine
        End If

        If (fBadge <> 0) Then
            recordLine += ("│ Badge Reader Test:                       " & "PASSED").PadRight(larghezza) & "│" & vbNewLine
        End If

        recordLine += "│                                                             │" & vbNewLine
        recordLine += "├─────────────────────────────────────────────────────────────┤" & vbNewLine
        recordLine += "│                                                             │" & vbNewLine

        recordLine += ("│ Visual Inspection:                       " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ Label Check:                             " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ Serial Number & Corrisponding Label:     " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ Inkjet cartidge Removed Check:           " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ Inkjet cartidge Present in the box:      " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ Unit Clean:                              " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ USB Cable Present:                       " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ Power supply Present:                    " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ Manuals present  in the box:             " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine

        recordLine += "│                                                             │" & vbNewLine
        recordLine += "│────────────────── DETAIL RESULTS ───────────────────────────┤" & vbNewLine
        recordLine += "│                                                             │" & vbNewLine

        recordLine += ("│ DOCT-R147     :read --> 10 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine & _
                     ("│ DOCUTEST 062  :read --> 10 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine

        'recordLine += ("│ DOCUTEST 046  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine & _
        '              ("│ DOCUTEST 048  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine & _
        '              ("│ DOCUTEST 061  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine & _
        '              ("│ DOCUTEST 062  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine & _
        '              ("│ DOCUTEST 063  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine




        recordLine += "└─────────────────────────────────────────────────────────────┘"

        Dim filelog As New StreamWriter(nomeFileLog)
        filelog.WriteLine(recordLine)
        filelog.Close()

        FaseOk()
        Return True

    End Function

    Public Function TestCartaN() As Integer

        Dim ritorno As Short
        Dim ret As Integer
        Dim rr As Short
        Dim fstop As Short
        Dim retZoom As DialogResult
        Dim a As Short
        Dim cicli As Short
        Dim ScanMode As Short

        ritorno = False
        rr = 1
        FaseEsec()
        cicli = 0

        While (fstop = False)
            idmsg = 269
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Inserire carta  DOCUTEST --083 dall' ingresso lineare", "Attenzione :  Ciclo : " + (cicli + 1).ToString + "/" + ("1").ToString, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            End If

            If (dres <> DialogResult.OK) Then
                ret = Ls40Class.Costanti.S_LS_USER_ABORT
                fstop = True
            Else
                DisposeImg()
                StrMessaggio = "Test Carta " + (cicli + 1).ToString + "/" + ("1").ToString + " ciclo"
                ls40.NomeFileImmagine = zzz + "\Dati\cardImage" + IndImgF.ToString("0000") + ".bmp"
                ls40.NomeFileImmagineRetro = zzz + "\Dati\cardImageR" + IndImgR.ToString("0000") + ".bmp"
                If (fScannerType = TIPO_SCANNER_COLORI) Then
                    ScanMode = Ls40Class.Costanti.S_SCAN_MODE_COLOR_200
                Else
                    ScanMode = Ls40Class.Costanti.S_SCAN_MODE_256GR300
                End If

                ret = ls40.LeggiImmCard(System.Convert.ToSByte(Ls40Class.Costanti.S_SIDE_ALL_IMAGE), rr, ScanMode)
                Select Case ret
                    Case Ls40Class.Costanti.S_LS_PAPER_JAM, + _
                    Ls40Class.Costanti.S_LS_JAM_AT_MICR_PHOTO, + _
                    Ls40Class.Costanti.S_LS_JAM_AT_SCANNERPHOTO, + _
                    Ls40Class.Costanti.S_LS_JAM_DOC_TOOLONG
                        idmsg = 111
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("ATTENZIONE CARTA BLOCCATA LIBERARE IL PERCORSO", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                        End If

                        If (dres <> DialogResult.OK) Then
                            ret = Ls40Class.Costanti.S_LS_USER_ABORT
                            fstop = True
                        Else
                            'noyet     ls150.Reset(Ls40Class.Costanti.S_RESET_FREE_PATH)
                        End If
                    Case Ls40Class.Costanti.S_LS_FEEDEREMPTY
                        Console.WriteLine("Ls40Class.Costanti.S_LS_FEEDEREMPTY")
                        'Case -1
                        '    idmsg = 112
                        '    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        '        dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        '    Else
                        '        dres = MessageboxMy("DOCUTEST --043 Inserito in modo non corretto", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                        '    End If

                        '    If (dres <> DialogResult.OK) Then
                        '        ret = Ls40Class.Costanti.S_LS_USER_ABORT
                        '        fstop = True
                        '    Else
                        '    End If
                    Case Ls40Class.Costanti.S_LS_OKAY
                        'If rr >= 0 Then

                        '    If cicli = ndoc Then
                        '        fstop = True
                        '    End If
                        '    cicli += 1
                        'Else
                        '    idmsg = 112
                        '    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        '        dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        '    Else
                        '        dres = MessageboxMy("DOCUTEST --043 Inserito in modo non corretto o Errore di LETTURA", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                        '    End If

                        '    fstop = True
                        '    ritorno = False
                        '    ret = Ls40Class.Costanti.S_LS_USER_ABORT

                        'End If
                        ret = Ls40Class.Costanti.S_LS_OKAY
                        IndImgF += 1
                        IndImgR += 1
                        fstop = True

                        'If (System.Convert.ToInt16(side) = Ls150Class.Costanti.S_SIDE_FRONT_IMAGE) Then
                        'BVisualZoom = False
                        DirZoom = Application.StartupPath

                        a = DirZoom.LastIndexOf("\")
                        DirZoom = DirZoom.Substring(0, a)
                        DirZoom += "\Dati\"

                        FileZoomCorrente = ls40.NomeFileImmagine


                        ' Zoom Immagine 
                        Try
                            retZoom = ShowZoom(DirZoom, FileZoomCorrente, False)
                        Catch ex As Exception
                            MessageBox.Show(ex.ToString())
                        End Try

                        If (retZoom = Windows.Forms.DialogResult.OK) Then
                            'BVisualZoom = False
                            DirZoom = Application.StartupPath

                            a = DirZoom.LastIndexOf("\")
                            DirZoom = DirZoom.Substring(0, a)
                            DirZoom += "\Dati\"
                            FileZoomCorrente = ls40.NomeFileImmagineRetro
                            ' Zoom Immagine 
                            Try
                                retZoom = ShowZoom(DirZoom, FileZoomCorrente, False)
                            Catch ex As Exception
                                MessageBox.Show(ex.ToString())
                            End Try

                            If (retZoom = Windows.Forms.DialogResult.OK) Then
                                ritorno = True
                            Else
                                ritorno = False
                                ret = Ls40Class.Costanti.S_LS_USER_ABORT
                            End If
                        Else
                            ritorno = False
                            ret = Ls40Class.Costanti.S_LS_USER_ABORT
                        End If

                    Case Else
                        writeDebug("Case Else -*- errore = " + ret.ToString(), "TestCarta")
                        'MessageboxMy("Errore = " + ret.ToString() + " non gestito !" + vbCrLf + "Prego contattare Progetto CTS !!!!", "Test Carta", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                        MessageboxMy("Errore = " + ret.ToString() + " non gestito !" + vbCrLf + "Carta Inserira in Modo non corretto , Ripetere il test", "Test Carta", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                        ret = Ls40Class.Costanti.S_LS_USER_ABORT
                        fstop = True
                        ritorno = False
                End Select
            End If
        End While


        FileTraccia.UltimoRitornoFunzione = ret
        If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
            ritorno = True
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If

        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante

        Return ritorno
    End Function

    Public Function TestQualitaCarta() As Integer

        Dim ritorno As Short
        Dim ret As Short
        Dim ret_not_used As Short
        Dim fstop As Short
        Dim fcicla As Boolean
        Dim ncili As Short
        Dim retZoom As DialogResult
        Dim a As Short

        ritorno = False
        ncili = 1
        FaseEsec()

        If fAtmConfiguration = True Then
            FaseOk()
            FileTraccia.UltimoRitornoFunzione = 0 'replycode
            FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante
            ritorno = True
            Return ritorno 'OCCHIO !!!!!!!!!!!!
        End If


        idmsg = 266
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            'immamsg = My.Resources.testcarta
            dres = MessageboxMy(Messaggio, Messaggio, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, immamsg)
        Else
            'immamsg = My.Resources.testcarta
            dres = MessageboxMy("Inserire DOCUTEST CARD 043", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, immamsg)
        End If
        DisposeImg()

        'StrMessaggio = "Test Carta Colori"

        fcicla = True
        IndImgF = 0
        IndImgR = 0
        While (fcicla)

            ls40.NomeFileImmagine = zzz + "\Dati\cardImageC" + IndImgF.ToString("0000") + ".bmp"
            ls40.NomeFileImmagineRetro = zzz + "\Dati\cardImageCR" + IndImgR.ToString("0000") + ".bmp"



            ret = ls40.LeggiImmCard2(System.Convert.ToSByte(Ls40Class.Costanti.S_SIDE_ALL_IMAGE), Ls40Class.Costanti.S_SCAN_MODE_256GR300)

            Select Case ret
                Case Ls40Class.Costanti.S_LS_PAPER_JAM, + _
                Ls40Class.Costanti.S_LS_JAM_AT_MICR_PHOTO, + _
                Ls40Class.Costanti.S_LS_JAM_AT_SCANNERPHOTO, + _
                Ls40Class.Costanti.S_LS_JAM_DOC_TOOLONG
                    idmsg = 111
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        dres = MessageboxMy2(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy("ATTENZIONE CARTA BLOCCATA LIBERARE IL PERCORSO", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                    End If

                    If (dres <> DialogResult.OK) Then
                        ret = Ls40Class.Costanti.S_LS_USER_ABORT
                        fstop = True
                    Else
                        ls40.Reset(Ls40Class.Costanti.S_RESET_FREE_PATH)
                    End If
                    fcicla = False
                Case Ls40Class.Costanti.S_LS_FEEDEREMPTY
                    fstop = True
                    fcicla = False

                Case Ls40Class.Costanti.S_LS_OKAY

                    ret = Ls40Class.Costanti.S_LS_OKAY
                    IndImgF += 1
                    IndImgR += 1
                    fcicla = False

                    'If (System.Convert.ToInt16(side) = Ls150Class.Costanti.S_SIDE_FRONT_IMAGE) Then
                    'BVisualZoom = False
                    DirZoom = Application.StartupPath

                    a = DirZoom.LastIndexOf("\")
                    DirZoom = DirZoom.Substring(0, a)
                    DirZoom += "\Dati\"

                    FileZoomCorrente = ls40.NomeFileImmagine


                    ' Zoom Immagine 
                    Try
                        retZoom = ShowZoom(DirZoom, FileZoomCorrente, False)
                    Catch ex As Exception
                        MessageBox.Show(ex.ToString())
                    End Try

                    If (retZoom = Windows.Forms.DialogResult.OK) Then
                        'BVisualZoom = False
                        DirZoom = Application.StartupPath

                        a = DirZoom.LastIndexOf("\")
                        DirZoom = DirZoom.Substring(0, a)
                        DirZoom += "\Dati\"
                        FileZoomCorrente = ls40.NomeFileImmagineRetro
                        ' Zoom Immagine 
                        Try
                            retZoom = ShowZoom(DirZoom, FileZoomCorrente, False)
                        Catch ex As Exception
                            MessageBox.Show(ex.ToString())
                        End Try

                        If (retZoom = Windows.Forms.DialogResult.OK) Then
                            ritorno = True
                        Else
                            ritorno = False
                            ret = Ls40Class.Costanti.S_LS_USER_ABORT
                            'se qualita' KO devo ricalibrare ....
                            ret_not_used = ritorno = TestScannersPeriferica() 'calibrazione scanner GRIGIO e COLORE + UV
                        End If
                    Else
                        ritorno = False
                        ret = Ls40Class.Costanti.S_LS_USER_ABORT
                        'se qualita' KO devo ricalibrare ....
                        ret_not_used = ritorno = TestScannersPeriferica() 'calibrazione scanner GRIGIO e COLORE + UV
                    End If
            End Select
        End While



        FileTraccia.UltimoRitornoFunzione = ret
        If (ret = Ls40Class.Costanti.S_LS_OKAY) Then
            ritorno = True
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls40.ReturnC.FunzioneChiamante

        Return ritorno
    End Function



End Class