﻿Public Class DatiFineCollaudo
    Public Enum TipoDato
        DUMP = 1
        FOTO1
        FOTO2
        FOTO3
        FOTO4
        FOTO5
        FOTO6
        DOPPIA1
        DOPPIA2
        DOPPIA3
        DP_TH1
        DP_TH2
        DP_TH3
        DP_TH4
        DP_TH5
        DP_TH6
        CINGHIA_MIN
        CINGHIA_MAX
        TEMPOCAMP200
        TEMPOCAMP300
        TEMPOCAMP75
        TEMPOCAMP300C
        TRIMMER200
        TRIMMER300
        TRIMMER75
        TRIMMER300C
        VELOCITA
        FRONTEGRIGI
        FRONTERED
        FRONTEGREEN
        FRONTEBLU
        RETROGRIGI
        RETRORED
        RETROGREEN
        RETROBLU
        ADFRONTE
        ADRETRO
    End Enum

    Class Dati
        Public Tipo As Integer
        Public Buffer() As Byte
        Public Sub Dati(ByVal xx As Integer)
            Array.Resize(Buffer, xx)

        End Sub
    End Class


    Public lista As New List(Of Dati)
    Public Sub AddDato(ByVal TIpo As TipoDato, ByRef Dato As Byte())
        Dim dd As New Dati()
        dd.Dati(Dato.Length + 1)
        dd.Tipo = TIpo
        For ii As Integer = 0 To Dato.Length - 1
            dd.Buffer(ii) = Dato(ii)
        Next

        lista.Add(dd)
    End Sub
End Class
