﻿Imports System.Windows.Forms
Imports System.Drawing
'Imports ReportPrinting
Imports System.Xml
Imports System.Xml.XPath
Imports System.Data
Imports System.IO


Public Class PerifLs515

    Public Const COLLAUDO_PIASTRA_LS515 = 1
    Public Const COLLAUDO_PERIFERICA_LS515 = 1
    Public Const LUNGHEZZA_SERIAL_NUMBER = 12
    Public Const LUNGHEZZA_SERIAL_NUMBER_PIASTRA = 16

    Public Const PATHFRONTEQ As String = "FrontImage"
    Public Const PATHRETROQ As String = "BackImage"

    Public Const TITOLO_DEF_MESSAGGIO = ""

    Public Const TEST_SPEED_GRAY = 0
    Public Const TEST_SPEED_COLOR_200_DPI = 1
    Public Const TEST_SPEED_COLOR_300_DPI = 2
    Public Const TEST_SPEED_GRAY_UV_200_DPI = 3 ' Non più usate
    Public Const TEST_SPEED_GRAY_UV_300_DPI = 4 ' Non più usate
    Public Const TEST_SPEED_RETAINED = 5        ' Non più usate

    Public Const TEST_SPEED_550_mm = 0          ' Gray 200 dpi Normal Speed
    Public Const TEST_SPEED_450_mm = 1          ' Color 200 dpi
    Public Const TEST_SPEED_350_mm = 2          ' Color 300 dpi
    Public Const TEST_SPEED_800_mm = 6          ' Gray 200 dpi + High Speed

    Public Const MARGINE_RIGHT = 1.8F
    Public Const MARGINE_LEFT = 1.8F
    Public Const MARGINE_TOP = 1.0F '1.8F
    Public Const MARGINE_BOTTOM = 1.0F '1.8F

    Public Const SFOGlIATORE_50_DOC_PRESENTE = 65

    Public Const NOME_CHIAVETTA = "CTS_KEY"
    Public Const NOME_FILE_CHIAVETTA = "Cts_file_Key.txt"
    Public Const NOME_STRINGA_CHIAVETTA = "Hub Usb test OK"

    Public Const LOCALDB As String = "\LocalDB"
    Public Const MESSAGGI As String = "\Messaggi"

    Public Const VERSIONE As String = "C.T.S. TestFasiCollaudoLs515.dll -- Ver. 2.00 Rev 000 date 04/03/2011"

    Public Const SW_TOP_IMAGE = 1 '(bit1)
    Public Const SW_BAR2D = 2     '(bit2)
    Public Const SW_TOP_BAR2D = 3 '(bit1+2)
    Public Const SW_IQA = 4
    Public Const SW_MICROHOLE = 8
    Public Const SW_BARCODE_MICROHOLE = 10
    Public Const SW_BARCODE_MICROHOLE_IQA = 14


    Public Const TIPO_SCANNER_UV_515 = 75
    Public Const TIPO_SCANNER_UV = 64

    Dim ls515 As Ls515Class.Periferica
    Dim fquality As Boolean
    Dim BVisualZoom As Boolean


    Dim IndImgF As Integer
    Dim IndImgR As Integer
    Dim fWaitButton As Boolean
    Dim zzz As String
    Dim FileZoomCorrente As String
    Dim StrMessaggio As String
    Dim VetImg(20) As Image
    Dim ff As CtsControls.Message
    Dim ff2 As CtsControls.Message2


    Public Hstorico As CStorico
    Public FileTraccia As New trace
    Public fase As Object 'BarraFasi.PhaseProgressBarItem
    Public DirZoom As String
    Public StrCommento As String
    Public Pimage As PictureBox
    Public PFronte As PictureBox
    Public PRetro As PictureBox
    Public PImageBack As PictureBox
    Public TipoDiCollaudo As Integer
    Public LettureCMC7 As Integer
    Public LettureE13B As Integer
    Public chcd As CheckCodeline
    Public TipoComtrolloLetture As Integer
    Public formpadre As Windows.Forms.Form
    Public FLed As CtsControls.Led5xx
    Public Fqualit As CtsControls.FqualImg
    Public BIdentificativo As Boolean
    'Public report As ReportPrinting.ReportDocument

    ' indicano le versioni di firmware dell ordine corrente
    Public Ver_firm1 As String
    Public Ver_firm2 As String
    Public Ver_firm3 As String
    Public Ver_firm4 As String
    Public Ver_firm5 As String
    Public Ver_firm6 As String
    Public Ver_firm7 As String
    Public Ver_firm8 As String
    Public Ver_firm9 As String

    ' indicano la configurazione della periferica
    Public fTestinaMicr As Boolean
    Public fTimbri As Boolean
    Public fScannerFronte As Boolean
    Public fScannerRetro As Boolean
    Public fStampante As Integer
    Public fVelBassa As Boolean
    Public fBadge As Integer ' 0 1 2 3 
    Public fSfogliatore As Integer
    Public fScannerType As Integer
    Public fSoftware As Integer
    Public fcardCol As Integer

    Public MatricolaPiastra As String
    ' il serial number inserito a mano della piastra
    Public SerialNumberPiastra As String
    Public SerialNumberDaStampare As String


    Public MatricolaPeriferica As String
    Private VettoreOpzioni As Array


    Public VersioneTest As String
    Public VersioneDll_1 As String
    Public VersioneDll_0 As String

    Public PathMessaggi As String
    Dim TipodiPeriferica As Integer
    Dim Titolo, Messaggio, Descrizione, IdMessaggio As String
    Dim dres As DialogResult
    Dim immamsg As Image
    Dim idmsg As Integer

    Public RigheContigueFronte As Integer
    Public RigheContigueRetro As Integer
    Public RigheTotaliFronte As Integer
    Public RigheTotaliRetro As Integer



    Public StrUsb As String

    Public DatiFine As DatiFineCollaudo





    Public Sub PerifLs515()
        Dim sss As String
        Dim ind As Integer

        ls515 = New Ls515Class.Periferica
        Hstorico = New CStorico
        Pimage = New PictureBox
        PFronte = New PictureBox
        PRetro = New PictureBox
        PImageBack = New PictureBox
        sss = Application.StartupPath()
        ind = sss.LastIndexOf("\")
        '        zzz = sss.Substring(0, ind)
        zzz = sss

        'report = New ReportPrinting.ReportDocument

        BIdentificativo = False

        fTestinaMicr = False
        fTimbri = False
        fScannerFronte = False
        fScannerRetro = False
        fStampante = 0
        fVelBassa = False
        fBadge = 0 ' 0 1 2 3 
        fSfogliatore = 0


        VersioneTest = VERSIONE

        VersioneDll_0 = ls515.VersioneDll_0
        VersioneDll_1 = ls515.VersioneDll_1

        ' imposto una periferica che non esiste....
        TipodiPeriferica = 0

        Titolo = ""
        Messaggio = ""
        Descrizione = ""
        IdMessaggio = ""
        StrUsb = "3131"

        DatiFine = New DatiFineCollaudo

    End Sub

    Public Sub ImpostaFlagPerConfigurazione(ByVal Vettore As Array)
        Dim ii As Short
        VettoreOpzioni = Vettore


        ' resetto i flag della configurazione da impostare.

        fTestinaMicr = False
        fTimbri = False
        fScannerFronte = False
        fScannerRetro = False
        fStampante = 0
        fVelBassa = False
        fBadge = 0 ' 0 1 2 3 
        fSfogliatore = 0
        fScannerType = 63   ' Per default setto lo scanner a colori
        'fScannerType = 75   ' Per default setto lo scanner a colori +uv 

        ' Controllo le opzioni che vanno da 0 a length -2
        ' perchè in ultim posizione trovo la Periferica
        'es case 5 testina micr non presente...non la controlla
        For ii = 0 To VettoreOpzioni.Length - 1
            Select Case VettoreOpzioni(ii)
                Case 6 To 8
                    fTestinaMicr = True
                Case 14 To 16
                    fTimbri = True
                Case 10
                    fScannerFronte = True
                Case 11
                    fScannerRetro = True
                Case 12
                    fScannerFronte = True
                    fScannerRetro = True
                Case 18
                    fStampante = True
                Case 45
                    fVelBassa = False
                Case 46
                    fVelBassa = True
                Case 22
                    fBadge = 1
                Case 50
                    fBadge = 2
                Case 52
                    fBadge = 3
                Case 62, 63, 64, 75  '63 colori 64 grigio+UV 75 colore+UV 62 grigio
                    fScannerType = VettoreOpzioni(ii)
                Case 65
                    fSfogliatore = SFOGlIATORE_50_DOC_PRESENTE
                Case 67
                    fSoftware = SW_TOP_IMAGE
                Case 68
                    fSoftware = SW_BAR2D
                Case 69
                    fSoftware = SW_TOP_BAR2D
                Case 70
                    fcardCol = 1
                    'AGGIUNTE 30-09-2014
                Case 76
                    fSoftware = SW_IQA
                Case 77
                    fSoftware = SW_MICROHOLE
                Case 78
                    fSoftware = SW_BARCODE_MICROHOLE
                    '19-02-2015
                Case 79
                    fSoftware = SW_BARCODE_MICROHOLE_IQA
                    fcardCol = 1
                Case 80
                    fSoftware = SW_BARCODE_MICROHOLE_IQA

            End Select
        Next
        TipodiPeriferica = VettoreOpzioni(VettoreOpzioni.Length - 1)

        PathMessaggi = Application.StartupPath + LOCALDB + MESSAGGI + TipodiPeriferica.ToString + ".xml"
    End Sub

    Public Sub CaricaRes()
        Try
            Dim ii As Integer
            Dim rsxr As Resources.ResXResourceReader
            Dim a As Resources.ResourceSet


            rsxr = New Resources.ResXResourceReader(Application.StartupPath + "\img2.resx")
            a = New Resources.ResourceSet(rsxr)

            Dim en As IDictionaryEnumerator = a.GetEnumerator()

            ' Goes through the enumerator, printing out the key and value pairs.

            ' finestra di massaggio in esecuzione

            ii = 0
            While en.MoveNext()
                Select Case en.Key.ToString
                    Case "pul2.jpg"
                        VetImg(0) = en.Value
                    Case "scanf.JPG"
                        VetImg(1) = en.Value
                    Case "pul1.jpg"
                        VetImg(2) = en.Value
                    Case "dp.JPG"
                        VetImg(3) = en.Value
                    Case "scanr.JPG"
                        VetImg(4) = en.Value
                    Case "badge.JPG"
                        VetImg(5) = en.Value
                    Case "cmicr.JPG"
                        VetImg(6) = en.Value
                    Case "qualim.JPG"
                        VetImg(7) = en.Value
                    Case "micrb.JPG"
                        VetImg(8) = en.Value
                    Case "qualimr.jpg"
                        VetImg(9) = en.Value
                    Case "docu055.JPG"
                        VetImg(10) = en.Value
                End Select
                VetImg(8) = VetImg(6)
                VetImg(4) = VetImg(0)

                'VetImg(ii) = en.Value

                'ii += 1
                'Me.Refresh()
            End While
            rsxr.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.InnerException.ToString)
        End Try

    End Sub


    Public Sub Sleep(ByVal sec As Integer)
        Dim iniTime, currTime As DateTime
        iniTime = DateTime.Now
        While (True)
            currTime = DateTime.Now
            Dim diff2 As System.TimeSpan
            ' diff2 gets 55 days 4 hours and 20 minutes.
            diff2 = System.DateTime.op_Subtraction(currTime, iniTime)

            If (diff2.Seconds = sec) Then
                Return
            End If
        End While
    End Sub

    Public Sub DisposeImg()
        If Not Pimage.Image Is Nothing Then
            Pimage.Image.Dispose()
            Pimage.Image = Nothing
        End If
        If Not PFronte.Image Is Nothing Then
            PFronte.Image.Dispose()
            PFronte.Image = Nothing
        End If
        If Not PRetro.Image Is Nothing Then
            PRetro.Image.Dispose()
            PRetro.Image = Nothing
        End If
        If Not PImageBack.Image Is Nothing Then
            PImageBack.Image.Dispose()
            PImageBack.Image = Nothing
        End If
    End Sub


    Public Sub ViewError(ByVal p As Object) 'BarraFasi.PhaseProgressBarItem)
        Dim sss As String
        Dim ccostante As New Ls515Class.Costanti
        sss = ""
        sss = ccostante.GetDesc(ls515.ReturnC.ReturnCode)
        MessageboxMy(ls515.ReturnC.FunzioneChiamante + " " + ls515.ReturnC.ReturnCode.ToString + "--> " + sss, "Attenzione !!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
    End Sub

    Public Sub FaseOk()
        ' azzero allimizio test il campo note

        fase.SetState = 1
        fase.IsBlinking = True
        fase.IsSelected = True
    End Sub

    Public Sub FaseKo()
        fase.SetState = 0
        fase.IsBlinking = True
        fase.IsSelected = True

    End Sub

    Public Sub FaseEsec()
        FileTraccia.Note = ""
        fase.SetState = 2
        fase.IsBlinking = True
        fase.IsSelected = True
    End Sub


    Public Function ShowZoom(ByVal dir As String, ByVal file As String, Optional ByVal VisualizzaZomm As Boolean = True) As DialogResult
        ' visualizzo subito lo zoom dell immagine

        Dim f As New CtsControls.ZoomImage
        f.FileCorrente = file
        f.DirectoryImage = dir
        f.effettuazoom = VisualizzaZomm
        f.ShowDialog()
        Return f.Result
    End Function


    Public Function MessageboxMy(ByVal testo As String, ByVal titolo As String, ByVal bt As MessageBoxButtons, ByVal ic As MessageBoxIcon, ByVal Img As Image) As DialogResult

        ff = New CtsControls.Message
        ff.Text = titolo
        ff.testo = testo
        ff.icona = ic
        ff.pulsanti = bt
        ff.Imag.Image = Img
        'If BVisualZoom = True Then
        '    ff.Bzoom.Visible = True
        '    ff.Directory = DirZoom
        '    ff.FileCorrente = FileZoomCorrente
        'End If


        ff.ShowDialog(formpadre)
        'If ff.fine = True Then
        '    ii = Tentativi
        '    ret = False
        'End If
        formpadre.Refresh()
        Return ff.result

    End Function

    Public Function MessageboxMy(ByVal testo As String, ByVal titolo As String, ByVal bt As MessageBoxButtons, ByVal ic As MessageBoxIcon, ByVal Img As Image, ByVal Id As String, ByVal Img2 As Image) As DialogResult

        ff2 = New CtsControls.Message2
        ff2.Text = titolo
        ff2.testo = testo
        ff2.icona = ic
        ff2.pulsanti = bt
        ff2.Imag.Image = Img
        ff2.LId.Text = Id
        ff2.PUser.Image = Img2

        'If BVisualZoom = True Then
        '    ff.Bzoom.Visible = True
        '    ff.Directory = DirZoom
        '    ff.FileCorrente = FileZoomCorrente
        'End If


        ff2.ShowDialog(formpadre)
        'If ff.fine = True Then
        '    ii = Tentativi
        '    ret = False
        'End If
        formpadre.Refresh()
        Return ff2.result

    End Function

    Public Function InputboxMy(ByVal testo As String, ByVal titolo As String, ByVal Check As Boolean, ByRef valore As Integer) As DialogResult
        Dim ff As New CtsControls.InputBox
        ff.Check = Check
        ff.Text = titolo
        ff.Messaggio = testo
        ff.ShowDialog(formpadre)
        valore = ff.Valore

        Return ff.result
    End Function


#Region "FUNZIONI TEST"

    Public Function ControllaVersione() As Integer
        Dim ret As Integer

        'Dim ch As Char()
        'Dim ch2 As Char()
        ret = 0


        'controllo che la versione sia uguale a quella scritta nell ordine
        ' ch = ls515.SBoardNr.ToCharArray
        'ch2 = ord.Board.ToCharArray
        If (ls515.SVersion.Length <= 0 Or String.Compare(ls515.SVersion.TrimEnd, Ver_firm1) = 0) Then
            If (ls515.SDateFW.Length <= 0 Or String.Compare(ls515.SDateFW.TrimEnd, Ver_firm2) = 0) Then
                Dim str As String = System.Convert.ToInt32(Ver_firm3, 16).ToString()
                If (ls515.SCpldNr.Length <= 0 Or String.Compare(ls515.SCpldNr.TrimEnd, Ver_firm4) = 0) Then
                    If (ls515.SBoardNr = str) Then
                        'If (ls515.SBoardNr = Ver_firm3) Then
                        If (ls515.SDateFPGA.Length <= 0 Or String.Compare(ls515.SDateFPGA.TrimEnd, Ver_firm5) = 0) Then
                            ret = 1

                        Else
                            'errore  data fpga
                            ret = Ls515Class.Costanti.S_LS515_DATA_FPGA_NOT_OK
                            idmsg = 195
                            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                Messaggio = Messaggio + "   " + String.Format("Richiesto : {0}  Trovato: {1}", Ver_firm5, ls515.SDateFPGA)
                                dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                            Else
                                dres = MessageboxMy("Versione Data FPGA non corrispondente", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                            End If


                        End If
                    Else
                        ' errore cpld
                        ret = Ls515Class.Costanti.S_LS515_BOARD_NOT_OK
                        idmsg = 198
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            Messaggio = Messaggio + "   " + String.Format("Richiesto : {0}  Trovato: {1}", str, ls515.SBoardNr)
                            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("Versione CPLD non corrispondente", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                        End If


                    End If
                Else
                    ' errore board
                    idmsg = 194
                    ret = Ls515Class.Costanti.S_LS515_BOARD_NOT_OK
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        Messaggio = Messaggio + "   " + String.Format("Richiesto : {0}  Trovato: {1}", Ver_firm4, ls515.SBoardNr)
                        dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy("Versione Board non corrispondente", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                    End If


                End If
            Else
                ' errore data
                ret = Ls515Class.Costanti.S_LS515_DATA_NOT_OK
                idmsg = 197
                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                    Messaggio = Messaggio + "   " + String.Format("Richiesto : {0}  Trovato: {1}", Ver_firm2, ls515.SDateFW)
                    dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                Else
                    dres = MessageboxMy("Data Versione non corrispondente", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                End If
            End If
        Else
            ' errore versione
            ret = Ls515Class.Costanti.S_LS515_FIRMWARE_NOT_OK
            idmsg = 196
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                Messaggio = Messaggio + "   " + String.Format("Richiesto : {0}  Trovato: {1}", Ver_firm1, ls515.SVersion)
                dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Versione Firmware non corrispondente", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
            End If
        End If

        Return ret
    End Function


    Public Function TestIdentificativoPiastra() As Integer
        Dim ritorno As Integer
        Dim ret As Integer
        Dim StrVersione As String
        Dim fpga As Integer
        ' inizializzazioni variabili 

        FaseEsec()
        ritorno = False
        'chiamata a funzione 
        ret = ls515.Identificativo()
        BIdentificativo = True
        MatricolaPiastra = ls515.SSerialNumber
        FileTraccia.VersioneFw = ls515.SVersion

        If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then
            ' Lidentif.Text = ls150.SLsName + " " + ls150.SVersion + vbCrLf
            fpga = System.Convert.ToInt32(ls515.SBoardNr)
            If ls515.SIdentStr.Length >= 2 Then
                StrVersione = "Nome Periferica: " + ls515.SLsName + vbCrLf + "Versione FW: " + ls515.SVersion + vbCrLf + "Data: " + ls515.SDateFW + vbCrLf + "Id Piastra: " + fpga.ToString("x") + vbCrLf + "Fpga: " + ls515.SCpldNr
                MessageboxMy(StrVersione, "Versione firmware", MessageBoxButtons.OK, MessageBoxIcon.None, Nothing)
            End If
            ritorno = True
        Else
            ' errore da richiesta identificativo
            MessageboxMy(ls515.ReturnC.FunzioneChiamante, "Attenzione !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)

        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ls515.ReturnC.ReturnCode
        Return ritorno
    End Function

    Public Function TestIdentificativo(ByVal NotUsed As Short) As Integer
        Dim ritorno As Integer
        Dim ret As Integer
        ' inizializzazioni variabili 


        'TestStampa(False)
        FaseEsec()
        ritorno = False


        'If usb = 1 Then
        '    idmsg = 243
        '    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
        '        dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        '    Else
        '        dres = MessageboxMy("Collegare ORA la periferica da Collaudare", "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
        '    End If
        'End If


        'chiamata a funzione 
        ret = ls515.Identificativo()
        BIdentificativo = True
        MatricolaPiastra = ls515.SSerialNumber
        FileTraccia.VersioneFw = ls515.SVersion
        If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then
            If System.Convert.ToInt16(ls515.SIdentStr(0)) And &H2 Then ' == 0x02) == (Velocita))						// 00000010
                FileTraccia.Note += "Veloctià Bassa" + vbCrLf
            End If
            If System.Convert.ToInt16(ls515.SIdentStr(0)) And &H8 Then ' == 0x08) == (inkjet))						// 00000010
                FileTraccia.Note += "Inkjet" + vbCrLf
            End If
            If System.Convert.ToInt16(ls515.SIdentStr(0)) And &H20 Then ' == 0x20) == (timbro))						// 00000010
                FileTraccia.Note += "Timbro" + vbCrLf
            End If
            If System.Convert.ToInt16(ls515.SIdentStr(1)) And &H1 Then ' == 0x01) == (scannerfronte))						// 00000010
                FileTraccia.Note += "Scanner Fronte" + vbCrLf
            End If
            If System.Convert.ToInt16(ls515.SIdentStr(1)) And &H2 Then ' == 0x02) == (scannerretro))						// 00000010
                FileTraccia.Note += "Scanner Retro" + vbCrLf
            End If
            If System.Convert.ToInt16(ls515.SIdentStr(0)) And &H10 Then ' == 0x02) == (sfogliatore50))						// 00000010
                FileTraccia.Note += "Sfogliatore 50" + vbCrLf
            End If
            If System.Convert.ToInt16(ls515.SIdentStr(1)) And &H8 Then
                If System.Convert.ToInt16(ls515.SIdentStr(1)) And &H10 Then ' badge
                    FileTraccia.Note += "Badge" + vbCrLf
                End If
            End If


            ritorno = True
        Else
            ' errore da richiesta identificativo
            MessageboxMy(ls515.ReturnC.FunzioneChiamante, "Attenzione !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)

        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ls515.ReturnC.ReturnCode
        Return ritorno
    End Function

    Public Function TestCheckVersion() As Integer
        Dim ritorno As Integer
        Dim ret As Integer

        ritorno = False
        FaseEsec()

        If BIdentificativo = True Then
            If ls515.SSerialNumber.Contains("OK") = True Then
                ' controllo che la versione sia quella dell ordine
                ret = ControllaVersione()
                If ret = 1 Then
                    ritorno = True
                    FileTraccia.Note = ""

                    FileTraccia.Note = "FwBase =" + Ver_firm1.ToString() + ", DataFW =" + Ver_firm2.ToString() + vbCrLf
                    FileTraccia.Note += "Board=" + Ver_firm3.ToString() + ", "
                    FileTraccia.Note = "FPGA =" + Ver_firm4.ToString() + ", DataFPGA =" + Ver_firm5.ToString() + vbCrLf
                    If (Ver_firm9 <> "") Then
                        FileTraccia.Note += "Suite =" + Ver_firm9.ToString() + vbCrLf
                    End If
                Else
                    ritorno = False
                End If
            Else
                ' piastra NON Collaudata
                idmsg = 199
                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                    dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                Else
                    dres = MessageboxMy("Attenzione, possibile Piastra NON collaudata", "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                End If


                ritorno = False
            End If
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function

    Public Function TestSerialNumber() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim fser As New CtsControls.FSerialnum
        ritorno = False
        FaseEsec()


        ls515.ReturnC.FunzioneChiamante = "TestSerialNumber"

        idmsg = 184
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            fser.Text = Messaggio + SerialNumberDaStampare
        Else
            fser.Text = "Inserire SN: " + SerialNumberDaStampare
        End If

        If fser.ShowDialog() = DialogResult.OK Then

            If fser.StrSerialNumber.Length = LUNGHEZZA_SERIAL_NUMBER Then
                If fser.StrSerialNumber = SerialNumberDaStampare Then
                    MatricolaPeriferica = fser.StrSerialNumber
                    ls515.SSerialNumber = MatricolaPeriferica
                    ret = ls515.SerialNumber()
                    If ret = Ls515Class.Costanti.S_LS515_OKAY Then
                        FileTraccia.Matricola = MatricolaPeriferica

                        ' leggo come ho impostato la macchina
                        ls515.LeggiValoriPeriferica()
                        SalvaDati()

                        ritorno = True
                    Else
                        ViewError(fase)
                    End If
                Else
                    idmsg = 185
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        MessageboxMy("SerialNumber non corrispondente a quello inserito a inizio collaudo.", "Attenzione...", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                    End If
                    ret = Ls515Class.Costanti.S_LS515_STRINGTRUNCATED
                End If
            Else
                ret = Ls515Class.Costanti.S_LS515_STRINGTRUNCATED
                ViewError(fase)
            End If


        Else
            ViewError(fase)
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    'nome funzione da inserire nel DB
    Public Function TestBarcode2D(ByVal ncili As Short) As Integer

        Dim ritorno As Short
        Dim ret As Short
        Dim fstop As Short
        Dim penna As New Pen(System.Drawing.Color.Red)
        Dim cicli As Short
        Dim font1 As New Font("Time New Roman", 8, FontStyle.Regular, GraphicsUnit.Pixel)
        Dim fAspetta As CtsControls.FWait
        Dim fcicla As Boolean
        Dim fdecode As Short
        Dim strBarcode2dSx As String
        Dim strBarcode2dDx As String
        Dim strBadgeBarcodeDataMatrix As String

        strBarcode2dSx = "505428111015181358766f1P4VezM"
        strBarcode2dDx = "946829746525181358766f1C4ZCts"
        strBadgeBarcodeDataMatrix = "CTSqr7794520027358eLeCTroniCS"

        ritorno = False
        fAspetta = New CtsControls.FWait
        fAspetta.Label1.Text = "Attendere... decodifica BARCODE in corso...."
        FaseEsec()

        cicli = 1
        If (fSoftware = SW_BAR2D Or fSoftware = SW_TOP_BAR2D) Then
            fdecode = 1

            If fdecode = 1 Then
                fcicla = True
                While (fcicla)

                    idmsg = 276
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy("Inserire DOCUTEST --089", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                    End If

                    If (dres <> DialogResult.OK) Then
                        ret = Ls515Class.Costanti.S_LS515_USER_ABORT
                        fcicla = False
                        fstop = True
                    Else
                        DisposeImg()
                        StrMessaggio = "Test Barcode2D"

                        ls515.NomeFileImmagine = zzz + "\Dati\ImageBarcode2D" + IndImgF.ToString("0000") + ".bmp"
                        ls515.NomeFileImmagineRetro = zzz + "\Dati\ImageBarcode2DR" + IndImgR.ToString("0000") + ".bmp"

                        fAspetta.Show()
                        ret = ls515.DecodificaBarcode2D(System.Convert.ToSByte(Ls515Class.Costanti.S_SIDE_ALL_IMAGE), Ls515Class.Costanti.S_SCAN_MODE_256GR200, fdecode)
                        fAspetta.Hide()


                        Select Case ret
                            Case Ls515Class.Costanti.S_LS515_PAPER_JAM, + _
                            Ls515Class.Costanti.S_LS515_JAM_AT_MICR_PHOTO, + _
                            Ls515Class.Costanti.S_LS515_JAM_AT_SCANNERPHOTO, + _
                            Ls515Class.Costanti.S_LS515_JAM_DOC_TOOLONG
                                idmsg = 111
                                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                    dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                                Else
                                    dres = MessageboxMy("ATTENZIONE CARTA BLOCCATA LIBERARE IL PERCORSO", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                End If

                                If (dres <> DialogResult.OK) Then
                                    ret = Ls515Class.Costanti.S_LS515_USER_ABORT
                                    fstop = True
                                Else
                                    ls515.Reset(Ls515Class.Costanti.S_RESET_FREE_PATH)
                                End If
                                fcicla = False
                            Case Ls515Class.Costanti.S_LS515_FEEDEREMPTY
                                fstop = True


                            Case Ls515Class.Costanti.S_LS515_OKAY

                                ret = Ls515Class.Costanti.S_LS515_OKAY
                                fcicla = False


                                DisposeImg()
                                'FileZoomCorrente = zzz + "\Dati\cardImageC" + IndImgF.ToString("0000") + ".bmp"
                                PFronte.Image = System.Drawing.Image.FromFile(zzz + "\Dati\ImageBarcode2D" + IndImgF.ToString("0000") + ".bmp")
                                PRetro.Image = System.Drawing.Image.FromFile(zzz + "\Dati\ImageBarcode2DR" + IndImgR.ToString("0000") + ".bmp")
                                Pimage.Image = PFronte.Image
                                PImageBack.Image = PRetro.Image
                                IndImgF += 1
                                IndImgR += 1

                                If dres = DialogResult.Cancel Then
                                    ret = -11111
                                End If

                                If (ls515.BarcodeLettoSx = strBarcode2dSx And ls515.BarcodeLettoDx = strBarcode2dDx And ls515.DataMatrixLetto = strBadgeBarcodeDataMatrix) Then
                                    dres = MessageboxMy(ls515.BadgeLetto + vbCrLf + "Decodifica Barcode2D OK!", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                                    ritorno = True
                                    fcicla = False
                                Else
                                    dres = MessageboxMy(ls515.BadgeLetto + vbCrLf + "Decodifica Barcode2D KO!, valori letti non corretti", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                    ritorno = False
                                    ret = Ls515Class.Costanti.S_LS515_USER_ABORT
                                End If

                            Case Ls515Class.Costanti.S_LS515_BARCODE_GENERIC_ERROR

                                MessageboxMy("Decodifica BARCODE2D NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                fcicla = False
                                ritorno = False

                            Case Ls515Class.Costanti.S_LS515_BARCODE_NOT_DECODABLE

                                MessageboxMy("Decodifica BARCODE2D NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                fcicla = False
                                ritorno = False

                            Case Ls515Class.Costanti.S_LS515_BARCODE_OPENFILE_ERROR

                                MessageboxMy("Decodifica BARCODE2D NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                fcicla = False
                                ritorno = False

                            Case Ls515Class.Costanti.S_LS515_BARCODE_READBMP_ERROR

                                MessageboxMy("Decodifica BARCODE2D NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                fcicla = False
                                ritorno = False

                            Case Ls515Class.Costanti.S_LS515_BARCODE_MEMORY_ERROR

                                MessageboxMy("Decodifica BARCODE2D NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                fcicla = False
                                ritorno = False

                            Case Ls515Class.Costanti.S_LS515_BARCODE_START_NOTFOUND

                                MessageboxMy("Decodifica BARCODE2D NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                fcicla = False
                                ritorno = False

                            Case Ls515Class.Costanti.S_LS515_BARCODE_STOP_NOTFOUND

                                MessageboxMy("Decodifica BARCODE2D NON riuscita " + ret.ToString(), "Test Fallito", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                fcicla = False
                                ritorno = False



                        End Select
                    End If
                    If cicli = ncili Then
                        fcicla = False
                    End If
                    cicli += 1
                End While
            Else
                ' non ho fatto nulla, e setto tutto a ok
                ret = Ls515Class.Costanti.S_LS515_OKAY
            End If
        Else
            ritorno = True
            ret = 0
        End If




        FileTraccia.UltimoRitornoFunzione = ret
        If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then
            ritorno = True
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        fAspetta.Close()
        Return ritorno
    End Function

    Public Function TestCancellaStorico(ByVal Veloci As Short) As Integer
        Dim ritorno As Integer
        Dim ret As Short
        ritorno = False
        FaseEsec()
        ret = ls515.EraseHistory()

        Hstorico.Doc_Timbrati = ls515.S_Storico.Doc_Timbrati
        Hstorico.Doc_Timbrati_retro = ls515.S_Storico.Doc_Timbrati_retro
        Hstorico.Documenti_Catturati = ls515.S_Storico.Documenti_Catturati
        Hstorico.Documenti_Ingresso = ls515.S_Storico.Documenti_Ingresso
        Hstorico.Documenti_Trattati = ls515.S_Storico.Documenti_Trattati
        Hstorico.Documenti_Trattenuti = ls515.S_Storico.Documenti_Catturati
        Hstorico.Doppia_Sfogliatura = ls515.S_Storico.Doppia_Sfogliatura
        Hstorico.Errori_Barcode = ls515.S_Storico.Errori_Barcode
        Hstorico.Errori_CMC7 = ls515.S_Storico.Errori_CMC7
        Hstorico.Errori_E13B = ls515.S_Storico.Errori_E13B
        Hstorico.Errori_Ottici = ls515.S_Storico.Errori_Ottici
        Hstorico.Jam_Allinea = ls515.S_Storico.Jam_Allinea
        Hstorico.Jam_Card = ls515.S_Storico.Jam_Card
        Hstorico.Jam_Cassetto1 = ls515.S_Storico.Jam_Cassetto1
        Hstorico.Jam_Cassetto2 = ls515.S_Storico.Jam_Cassetto2
        Hstorico.Jam_Cassetto3 = ls515.S_Storico.Jam_Cassetto3
        Hstorico.Jam_Feeder = ls515.S_Storico.Jam_Feeder
        Hstorico.Jam_Feeder_Ret = ls515.S_Storico.Jam_Feeder_Ret
        Hstorico.Jam_Micr = ls515.S_Storico.Jam_Micr
        Hstorico.Jam_Micr_Ret = ls515.S_Storico.Jam_Micr_Ret
        Hstorico.Jam_Percorso_DX = ls515.S_Storico.Jam_Percorso_DX
        Hstorico.Jam_Percorso_SX = ls515.S_Storico.Jam_Percorso_SX
        Hstorico.Jam_Print = ls515.S_Storico.Jam_Print
        Hstorico.Jam_Print_Ret = ls515.S_Storico.Jam_Print_Ret
        Hstorico.Jam_Scanner = ls515.S_Storico.Jam_Scanner
        Hstorico.Jam_Scanner_Ret = ls515.S_Storico.Jam_Scanner_Ret
        Hstorico.Jam_Sorter = ls515.S_Storico.Jam_Sorter
        Hstorico.Jam_Stamp = ls515.S_Storico.Jam_Stamp
        Hstorico.Num_Accensioni = ls515.S_Storico.Num_Accensioni
        Hstorico.TempoAccensione = ls515.S_Storico.TempoAccensione
        Hstorico.Trattenuti_Micr = ls515.S_Storico.Trattenuti_Micr
        Hstorico.Trattenuti_Scan = ls515.S_Storico.Trattenuti_Scan



        If ret = Ls515Class.Costanti.S_LS515_OKAY Then

            ' se sono all ' inizio del collaudo quindi setto a true il set diagnistic mode 
            ' poi devo settare la velocità a alta quindi 0

            ' altrimenti se sono alla fine , prima di togliere il seti diagnosti mode 
            ' devo alzare la velocità .

            If ret = Ls515Class.Costanti.S_LS515_OKAY Then
                ritorno = True
            Else

            End If
        Else
            MessageboxMy("ritorno " + ret.ToString, "", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function SetConfigurazione() As Integer
        Dim ritorno As Integer
        Dim ret As Short

        Dim TestinaMicr As Integer
        Dim Timbri As Integer
        Dim ScannerFronte As Integer
        Dim ScannerRetro As Integer
        Dim Stampante As Integer
        Dim VelBassa As Integer
        Dim Badge1 As Integer ' 0 1 2 3 
        Dim Badge2 As Integer ' 0 1 2 3 
        Dim Badge3 As Integer ' 0 1 2 3 
        Dim Sfogliatore50 As Integer

        ritorno = False
        FaseEsec()
        Badge1 = Badge2 = Badge3 = 0

        If fTestinaMicr = True Then
            TestinaMicr = 1
        Else
            TestinaMicr = 0
        End If
        If fTimbri = True Then
            Timbri = 1
        Else
            Timbri = 0
        End If
        'scanner 
        If fScannerFronte = True Then
            ScannerFronte = 1
        Else
            ScannerFronte = 0
        End If
        If fScannerRetro = True Then
            ScannerRetro = 1
        Else
            ScannerRetro = 0
        End If
        'stampante
        If fStampante = True Then
            Stampante = 1
        Else
            Stampante = 0
        End If
        'veloc
        If fVelBassa = True Then
            VelBassa = 1
        Else
            VelBassa = 0
        End If

        Select Case fBadge
            Case 1
                Badge1 = 1
                Badge2 = 0
                Badge3 = 0
            Case 2
                Badge1 = 0
                Badge2 = 1
                Badge3 = 0
            Case 3
                Badge1 = 0
                Badge2 = 0
                Badge3 = 1
        End Select

        If fSfogliatore = SFOGlIATORE_50_DOC_PRESENTE Then
            Sfogliatore50 = 1
        Else
            Sfogliatore50 = 0
        End If

        FileTraccia.Note = ""
        FileTraccia.Note = "TestinaMicr =" + TestinaMicr.ToString() + ", TestinaMicr =" + TestinaMicr.ToString() + vbCrLf
        FileTraccia.Note += "Timbri =" + Timbri.ToString() + ", Badge1 =" + Badge1.ToString() + vbCrLf
        FileTraccia.Note += "Badge2 =" + Badge2.ToString() + ", Badge3 =" + Badge3.ToString() + vbCrLf
        FileTraccia.Note += "ScannerFronte =" + ScannerFronte.ToString() + ", ScannerRetro =" + ScannerRetro.ToString() + vbCrLf
        FileTraccia.Note += "Stampante =" + Stampante.ToString() + ", VelBassa =" + VelBassa.ToString() + vbCrLf
        FileTraccia.Note += "Sfogliatore50 =" + Sfogliatore50.ToString()

        ret = ls515.SetConfigurazione(TestinaMicr, TestinaMicr, Timbri, Badge1, Badge2, Badge3, ScannerFronte, ScannerRetro, Stampante, VelBassa, Sfogliatore50, fScannerType, fSoftware, fcardCol)
        If ret = Ls515Class.Costanti.S_LS515_OKAY Then
            ritorno = True
        Else
            ' imposto gli errori
            ViewError(fase)
            FileTraccia.UltimoRitornoFunzione = ret
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    ' per Ora la funzione TestFoto non è utilizzata
    'Public Function TestFoto() As Integer
    '    Dim ritorno As Integer
    '    Dim ret As Short
    '    Dim floop As Boolean = False
    '    ritorno = false
    '    'GFoto.Enabled = True

    '    fase.SetState = 2
    '    fase.IsBlinking = True
    '    fase.IsSelected = True

    '    ' seleziono il tab dei fotosensori
    '    fase.IsSelected = False
    '    'TCollaudo.SelectedTab() = TFoto


    '    If MessageboxMy("Calibrazione Fotosensori!!!", "ATTENZIONE !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning, Nothing) <> DialogResult.OK Then
    '        fase.SetState = 0
    '        fase.IsBlinking = True
    '        fase.IsSelected = True
    '        ritorno = false
    '    Else
    '        ret = ls515.CalibrazioneFoto()
    '        If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then

    '            If (Ls515Class.Periferica.PhotoValue(0) <> 255 And Ls515Class.Periferica.PhotoValue(1) <> 255 And Ls515Class.Periferica.PhotoValue(2) <> 255) Then
    '                fase.SetState = 1
    '                fase.IsBlinking = True
    '                fase.IsSelected = True
    '                ritorno = true
    '            Else
    '                fase.SetState = 0
    '                fase.IsBlinking = True
    '                fase.IsSelected = True
    '            End If
    '        Else
    '            ViewError(fase)
    '        End If

    '    End If
    '    If ritorno = true Then
    '        FileTraccia.FotoPersorso = 1
    '    End If
    '    FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
    '    Return ritorno
    'End Function




    Public Function TestFotoPeriferica() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim floop As Boolean = False
        Dim iii As Short
        iii = 0
        ritorno = False
        FLed = New CtsControls.Led5xx
        FLed.LMessaggio.Text = "Coprire fotosensori BIN per almeno 3 secondi"

        FaseEsec()
        idmsg = 201
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Rimuovere eventuali documenti da percorso", "ATTENZIONE !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning, Nothing)
        End If


        If dres <> DialogResult.OK Then
            fase.SetState = 0
            fase.IsBlinking = True
            fase.IsSelected = True
            ritorno = False
            FileTraccia.UltimoRitornoFunzione = Ls515Class.Costanti.S_LS515_USER_ABORT


        Else
            FLed.Show()
            FLed.Refresh()
            ret = ls515.CalibrazioneFoto()

            FLed.LSe1.Text += Ls515Class.Periferica.PhotoValue(0).ToString
            FLed.Lse2.Text += Ls515Class.Periferica.PhotoValue(1).ToString
            FLed.LSe3.Text += Ls515Class.Periferica.PhotoValue(2).ToString
            FLed.LSe4.Text += Ls515Class.Periferica.PhotoValue(3).ToString
            FLed.LSe5.Text += Ls515Class.Periferica.PhotoValue(4).ToString
            FLed.LSe6.Text += Ls515Class.Periferica.PhotoValue(5).ToString


            FileTraccia.Note = "Fotosensore 1: " + Ls515Class.Periferica.PhotoValue(0).ToString + vbCrLf
            FileTraccia.Note += "Fotosensore 2: " + Ls515Class.Periferica.PhotoValue(1).ToString + vbCrLf
            FileTraccia.Note += "Fotosensore 3: " + Ls515Class.Periferica.PhotoValue(2).ToString + vbCrLf
            FileTraccia.Note += "Fotosensore 4: " + Ls515Class.Periferica.PhotoValue(4).ToString + vbCrLf
            FileTraccia.Note += "Fotosensore 5: " + Ls515Class.Periferica.PhotoValue(5).ToString + vbCrLf
            FileTraccia.Note += "Fotosensore 6: " + Ls515Class.Periferica.PhotoValue(6).ToString



            If Ls515Class.Periferica.PhotoValue(0) = 0 Or Ls515Class.Periferica.PhotoValue(0) = 255 Then
                FLed.LbLedSe1.LedColor = Color.Red
            Else
                FLed.LbLedSe1.LedColor = Color.Green
                FLed.LbLedSe1.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.On
            End If

            If Ls515Class.Periferica.PhotoValue(1) = 0 Or Ls515Class.Periferica.PhotoValue(1) = 255 Then
                FLed.LbLedSe2.LedColor = Color.Red
            Else
                FLed.LbLedSe2.LedColor = Color.Green
                FLed.LbLedSe2.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.On
            End If

            If Ls515Class.Periferica.PhotoValue(2) = 0 Or Ls515Class.Periferica.PhotoValue(2) = 255 Then
                FLed.LbLedSe3.LedColor = Color.Red
            Else
                FLed.LbLedSe3.LedColor = Color.Green
                FLed.LbLedSe3.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.On
            End If

            If Ls515Class.Periferica.PhotoValue(3) = 0 Or Ls515Class.Periferica.PhotoValue(3) = 255 Then
                FLed.LbLedSe4.LedColor = Color.Red
            Else
                FLed.LbLedSe4.LedColor = Color.Green
                FLed.LbLedSe4.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.On
            End If

            If Ls515Class.Periferica.PhotoValue(4) = 0 Or Ls515Class.Periferica.PhotoValue(4) = 255 Then
                FLed.LbLedSe5.LedColor = Color.Red
            Else
                FLed.LbLedSe5.LedColor = Color.Green
                FLed.LbLedSe5.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.On
            End If
            If Ls515Class.Periferica.PhotoValue(5) = 0 Or Ls515Class.Periferica.PhotoValue(5) = 255 Then
                FLed.LbLedSe6.LedColor = Color.Red
            Else
                FLed.LbLedSe6.LedColor = Color.Green
                FLed.LbLedSe6.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.On
            End If

            If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then
                'idmsg = 200
                idmsg = 277
                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                    dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                Else
                    'dres = MessageboxMy("Coprire fotosensori BIN per almeno 3 secondi", "ATTENZIONE !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning, Nothing)
                    dres = MessageboxMy("Coprire fotosensore BIN 1 per almeno 3 secondi", "ATTENZIONE !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning, Nothing)
                End If


                If dres = DialogResult.OK Then
                    floop = False
                    iii = 1

                    'copro FOTOSENSORE BIN 1
                    Do
                        ret = ls515.StatoPeriferica()
                        If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then
                            ' ogni 5 secondi mando il mesagigio a video 
                            If (Math.IEEERemainder(iii, 5) = 0) Then
                                'idmsg = 200
                                idmsg = 277
                                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                    dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                                Else
                                    'dres = MessageboxMy("Coprire fotosensori BIN per almeno 3 secondi", "ATTENZIONE !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning, Nothing)
                                    dres = MessageboxMy("Coprire fotosensore BIN 1 per almeno 3 secondi", "ATTENZIONE !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning, Nothing)
                                End If

                                If (dres <> DialogResult.Yes) Then
                                    ret = -1
                                End If
                            End If
                            Application.DoEvents()
                            'If (((Ls515Class.Periferica.StatusByte(1) And 2) = 2) And ((Ls515Class.Periferica.StatusByte(1) And 4) = 4)) Then
                            If (((Ls515Class.Periferica.StatusByte(1) And 2) = 2)) Then
                                FLed.LbLedSeBin.LedColor = Color.Green
                                FLed.LbLedSeBin.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.On
                                'FLed.LbLedSeBin2.LedColor = Color.Green
                                'FLed.LbLedSeBin2.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.On
                                FLed.Refresh()
                                floop = True
                                ritorno = True
                            Else
                                FLed.LbLedSeBin.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.Off
                            End If
                            ' aspetto un secondo
                            Sleep(1)
                        Else
                            FileTraccia.UltimoRitornoFunzione = ret
                            ViewError(fase)
                        End If
                        iii += 1
                    Loop While floop = False

                    floop = False
                    ritorno = False

                    'idmsg = 200
                    idmsg = 278
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        'dres = MessageboxMy("Coprire fotosensori BIN per almeno 3 secondi", "ATTENZIONE !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning, Nothing)
                        dres = MessageboxMy("Coprire fotosensore BIN 2 per almeno 3 secondi", "ATTENZIONE !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning, Nothing)
                    End If
                    If (dres <> DialogResult.Yes) Then
                        ret = -1
                    End If
                    'copro FOTOSENSORE BIN 1
                    Do
                        ret = ls515.StatoPeriferica()
                        If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then
                            ' ogni 5 secondi mando il mesagigio a video 
                            If (Math.IEEERemainder(iii, 5) = 0) Then
                                ''idmsg = 200
                                idmsg = 278
                                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                    dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                                Else
                                    'dres = MessageboxMy("Coprire fotosensori BIN per almeno 3 secondi", "ATTENZIONE !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning, Nothing)
                                    dres = MessageboxMy("Coprire fotosensore BIN 2 per almeno 3 secondi", "ATTENZIONE !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning, Nothing)
                                End If

                                If (dres <> DialogResult.Yes) Then
                                    ret = -1
                                End If
                            End If
                            Application.DoEvents()
                            'If (((Ls515Class.Periferica.StatusByte(1) And 2) = 2) And ((Ls515Class.Periferica.StatusByte(1) And 4) = 4)) Then
                            If (((Ls515Class.Periferica.StatusByte(1) And 4) = 4)) Then
                                'FLed.LbLedSeBin.LedColor = Color.Green
                                'FLed.LbLedSeBin.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.On
                                FLed.LbLedSeBin2.LedColor = Color.Green
                                FLed.LbLedSeBin2.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.On
                                FLed.Refresh()
                                floop = True
                                ritorno = True
                            Else
                                FLed.LbLedSeBin.State = LBSoft.IndustrialCtrls.Leds.LBLed.LedState.Off
                            End If
                            ' aspetto un secondo
                            Sleep(1)
                        Else
                            FileTraccia.UltimoRitornoFunzione = ret
                            ViewError(fase)
                        End If
                        iii += 1
                    Loop While floop = False
                    FLed.Close()

                Else
                    FLed.Close()
                    ritorno = False
                    FileTraccia.UltimoRitornoFunzione = Ls515Class.Costanti.S_LS515_USER_ABORT
                End If
            Else
                ViewError(fase)
                ritorno = False
                FLed.Close()
                FileTraccia.UltimoRitornoFunzione = ret
                StrCommento += "Calibrazione non a buon fine: " + ret.ToString + " "
            End If
        End If
        If ritorno = True Then
            FaseOk()
            FileTraccia.FotoPersorso = 1
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestFotoDP() As Integer
        Dim ritorno As Short
        Dim ret As Short
        '  Dim R As DialogResult
        ritorno = False

        FaseEsec()
        ' faccio la calibrazione del foto doppia sfogliatura
        idmsg = 203
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire il  DOCUTEST --052 nel Feeder per la calibrazione del fotosensore Doppia Sfogliatura", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        End If

        If dres <> DialogResult.OK Then
            fase.SetState = 0
            fase.IsBlinking = True
            fase.IsSelected = True
            ' errore calibrazione doppia sfogliatura
            ViewError(fase)
        Else
            fase.IsSelected = False

            ret = ls515.CalSfogliatura()

            FileTraccia.Note = "Fotosensore  1: " + Ls515Class.Periferica.PhotoValue(0).ToString + vbCrLf
            FileTraccia.Note += "Fotosensore 2: " + Ls515Class.Periferica.PhotoValue(1).ToString + vbCrLf
            FileTraccia.Note += "Fotosensore 3: " + Ls515Class.Periferica.PhotoValue(2).ToString + vbCrLf

            FileTraccia.Note += "Fotosensore 4: " + Ls515Class.Periferica.PhotoValue(3).ToString + vbCrLf
            FileTraccia.Note += "Fotosensore 5: " + Ls515Class.Periferica.PhotoValue(4).ToString + vbCrLf
            FileTraccia.Note += "Fotosensore 6: " + Ls515Class.Periferica.PhotoValue(5).ToString + vbCrLf

            If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then
                ritorno = True
            Else
                ' errore calibrazione doppia sfogliatura
                ViewError(fase)
            End If
        End If

        If ritorno = True Then
            FileTraccia.FotoDp = 1
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function



    Public Function TestScannersPeriferica() As Integer
        Dim ritorno As Integer
        Dim ret As Short



        ritorno = False
        'TCollaudo.SelectedTab() = TScanner
        'GscanR.Enabled = True
        'BvisualF.Text = "Controllo immagine Fronte e Retro NP2"
        'BCalF.Text = "Calibra Scanner Fronte e Retro NP1"


        FaseEsec()

        idmsg = 202
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire il DOCUTEST --055 per la calibrazione Scanner. !", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(10))
        End If


        ' faccio la calibrazione scanner fronte
        If (dres <> DialogResult.OK) Then
            ret = Ls515Class.Costanti.S_LS515_USER_ABORT
            FaseKo()
            fWaitButton = True
        Else



            fquality = False
            ret = ls515.CalibraScanner(Ls515Class.Costanti.SCANNER_GREY)
            If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then
                'If (ordine.ScannerFronte = 1) Then
                '    FileZoomCorrente = ls515.NomeFileImmagine
                '    ret = LeggiImmagine(System.Convert.ToSByte(Ls515Class.Costanti.S_SIDE_FRONT_IMAGE))
                'End If
                'If ret = Ls515Class.Costanti.S_LS515_OKAY Then
                '    If (ordine.ScannerRetro = 1) Then
                '        FileZoomCorrente = ls515.NomeFileImmagine
                '        ret = LeggiImmagine(System.Convert.ToSByte(Ls515Class.Costanti.S_SIDE_BACK_IMAGE))
                '    End If
                'End If

            Else
                ' errore calibrazione fronte
                'BCalF.Enabled = True
                'BvisualF.Enabled = True
                'BAnnulla.Enabled = True
                ViewError(fase)
                'ViewError(fase)
            End If
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        If ret = Ls515Class.Costanti.S_LS515_OKAY Then
            ritorno = True
        Else
            ritorno = False
        End If

        If ritorno = True Then

            FaseOk()
            FileTraccia.ScannerF = 1
        End If
        BVisualZoom = False
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function
    Public Function TestTrattenuto(ByVal nDoc As Short) As Integer
        Dim ritorno As Integer
        Dim ret As Integer
        'Dim strapp As String
        Dim iii As Short
        Dim fcicla As Boolean
        Dim sorter As Short

        ritorno = False


        FaseEsec()


        ls515.NomeFileImmagine = zzz + "\Dati\Trattenuto" + IndImgF.ToString("0000") + ".bmp"
        ls515.NomeFileImmagineRetro = zzz + "\Dati\TrattenutoR" + IndImgR.ToString("0000") + ".bmp"
        IndImgF += 1
        IndImgR += 1
        idmsg = 262
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire Docutest- 62 per test trattenuto", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(3))
        End If


        If (dres <> DialogResult.OK) Then
            ret = Ls515Class.Costanti.S_LS515_USER_ABORT
        Else
            fase.IsSelected = False
            iii = 0
            fcicla = True
            While (fcicla)

                ls515.NomeFileImmagine = zzz + "\Dati\Trattenuto" + IndImgF.ToString("0000") + ".bmp"
                ls515.NomeFileImmagineRetro = zzz + "\Dati\TrattenutoR" + IndImgR.ToString("0000") + ".bmp"


                ret = ls515.LeggiCodelineImmaginiTrattieni()

                If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then

                    Try
                        DisposeImg()
                        Pimage.Image = System.Drawing.Image.FromFile(ls515.NomeFileImmagine)
                        PImageBack.Image = System.Drawing.Image.FromFile(ls515.NomeFileImmagineRetro)
                    Catch ex As Exception
                        ' DEBUG
                        MessageBox.Show(ex.Message + " " + ex.Source + ret.ToString, "errore")
                    End Try


                    IndImgF += 1
                    IndImgR += 1
                    Application.DoEvents()


                    If (Math.IEEERemainder(iii, 2) = 0) Then
                        sorter = Ls515Class.Costanti.S_SORTER_BAY1
                    Else
                        sorter = Ls515Class.Costanti.S_SORTER_BAY2
                    End If

                    ' eslpello il documento
                    ls515.NomeFileImmagine = zzz + "\Dati\Trattenuto" + IndImgF.ToString("0000") + ".bmp"
                    ls515.NomeFileImmagineRetro = zzz + "\Dati\TrattenutoR" + IndImgR.ToString("0000") + ".bmp"
                    ret = ls515.LeggiCodelineImmaginiIncasella(sorter)

                    If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then
                        Try
                            DisposeImg()
                            Pimage.Image = System.Drawing.Image.FromFile(ls515.NomeFileImmagine)
                            PImageBack.Image = System.Drawing.Image.FromFile(ls515.NomeFileImmagineRetro)
                        Catch ex As Exception
                            ' DEBUG
                            MessageBox.Show(ex.Message + " " + ex.Source + ret.ToString, "errore")
                        End Try
                    End If
                    iii += 1


                    Application.DoEvents()


                Else
                    Select Case ret
                        Case Ls515Class.Costanti.S_LS515_FEEDEREMPTY
                            idmsg = 262
                            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                            Else
                                dres = MessageboxMy("Inserire Docutest- 62 per test trattenuto", TITOLO_DEF_MESSAGGIO, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                            End If

                            If dres <> DialogResult.OK Then
                                fcicla = False
                                ret = Ls515Class.Costanti.S_LS515_USER_ABORT
                            End If
                            ret = Ls515Class.Costanti.S_LS515_OKAY
                            iii -= 1
                        Case Ls515Class.Costanti.S_LS515_PAPER_JAM
                            ls515.Reset(Ls515Class.Costanti.S_RESET_FREE_PATH)
                            ret = Ls515Class.Costanti.S_LS515_OKAY
                            iii -= 1
                        Case Else
                            ls515.Reset(Ls515Class.Costanti.S_RESET_FREE_PATH)
                            ret = Ls515Class.Costanti.S_LS515_OKAY
                            iii -= 1
                    End Select
                End If

                If ret <> Ls515Class.Costanti.S_LS515_OKAY Or iii > nDoc Then
                    fcicla = False
                    iii -= 1
                End If

                Application.DoEvents()
                IndImgF += 1
                IndImgR += 1


            End While




            If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then

                Try
                    DisposeImg()
                    Pimage.Image = System.Drawing.Image.FromFile(ls515.NomeFileImmagine)
                    PImageBack.Image = System.Drawing.Image.FromFile(ls515.NomeFileImmagineRetro)
                Catch ex As Exception
                    ' DEBUG
                    MessageBox.Show(ex.Message + " " + ex.Source + ret.ToString, "errore")
                End Try

                Application.DoEvents()



                fase.SetState = 1
                fase.IsBlinking = True
                fase.IsSelected = True
                ritorno = True



            Else
                ret = Ls515Class.Costanti.S_LS515_USER_ABORT
                fase.SetState = 0
                fase.IsBlinking = True
                fase.IsSelected = True
                ViewError(fase)
            End If
        End If





        FileTraccia.UltimoRitornoFunzione = ret

        If ritorno = True Then
            FileTraccia.Stampante = 1
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function


    Public Function TestStampa(ByVal Altezza As Boolean) As Integer
        Dim ritorno As Integer
        Dim ret As Integer
        Dim strapp As String
        Dim iii As Short
        Dim fcicla As Boolean
        Dim ndoc As Short
        ritorno = False
        ndoc = 3

        FaseEsec()


        ls515.NomeFileImmagine = zzz + "\Dati\StampaFImage" + IndImgF.ToString("0000") + ".bmp"
        ls515.NomeFileImmagineRetro = zzz + "\Dati\StampaRImage" + IndImgR.ToString("0000") + ".bmp"
        IndImgF += 1
        IndImgR += 1
        ls515.Stampafinale = "Piastra: " + SerialNumberPiastra + " SN: " + SerialNumberDaStampare
        idmsg = 204
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire un documento per la prova di stampa", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(3))
        End If


        If (dres <> DialogResult.OK) Then
            ret = Ls515Class.Costanti.S_LS515_USER_ABORT
        Else
            fase.IsSelected = False
            iii = 0
            fcicla = True
            While (fcicla)


                If (iii > ndoc - 1) Then
                    idmsg = 183
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy("Inserire uno dei documenti timbrati per l'ultima prova di stampa", TITOLO_DEF_MESSAGGIO, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(3))
                    End If
                End If
                ls515.NomeFileImmagine = zzz + "\Dati\StampaFImage" + IndImgF.ToString("0000") + ".bmp"
                ls515.NomeFileImmagineRetro = zzz + "\Dati\StampaRImage" + IndImgR.ToString("0000") + ".bmp"

                If (iii = ndoc) Then
                    ret = ls515.TestStampa(1)
                Else
                    ret = ls515.TestStampa(0)
                End If
                strapp = "La stampante ha stampato bene?"

                iii += 1
                If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then

                    Try
                        DisposeImg()
                        Pimage.Image = System.Drawing.Image.FromFile(ls515.NomeFileImmagine)
                        PImageBack.Image = System.Drawing.Image.FromFile(ls515.NomeFileImmagineRetro)
                    Catch ex As Exception
                        ' DEBUG
                        MessageBox.Show(ex.Message + " " + ex.Source + ret.ToString, "errore")
                    End Try

                    Application.DoEvents()

                    idmsg = 205
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy(strapp, "Attenzione!!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing)
                    End If
                    'If (dres = DialogResult.No) Then
                    '    iii -= 1
                    ' End If
                    If (dres = DialogResult.Cancel) Then
                        iii -= 1
                    End If
                Else
                    Select Case ret
                        Case Ls515Class.Costanti.S_LS515_FEEDEREMPTY
                            idmsg = 178
                            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                            Else
                                dres = MessageboxMy("Inserire Documenti per la prova di stampa", TITOLO_DEF_MESSAGGIO, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                            End If

                            If dres <> DialogResult.OK Then
                                fcicla = False
                                ret = Ls515Class.Costanti.S_LS515_USER_ABORT
                            End If
                            ret = Ls515Class.Costanti.S_LS515_OKAY
                            iii -= 1
                        Case Ls515Class.Costanti.S_LS515_PAPER_JAM
                            ls515.Reset(Ls515Class.Costanti.S_RESET_FREE_PATH)
                            ret = Ls515Class.Costanti.S_LS515_OKAY
                            iii -= 1
                        Case Else
                            ls515.Reset(Ls515Class.Costanti.S_RESET_FREE_PATH)
                            ret = Ls515Class.Costanti.S_LS515_OKAY
                            iii -= 1
                    End Select
                End If

                If ret <> Ls515Class.Costanti.S_LS515_OKAY Or iii > ndoc Then
                    fcicla = False
                    iii -= 1
                End If

                Application.DoEvents()
                IndImgF += 1
                IndImgR += 1


            End While




            If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then

                Try
                    DisposeImg()
                    Pimage.Image = System.Drawing.Image.FromFile(ls515.NomeFileImmagine)
                    PImageBack.Image = System.Drawing.Image.FromFile(ls515.NomeFileImmagineRetro)
                Catch ex As Exception
                    ' DEBUG
                    MessageBox.Show(ex.Message + " " + ex.Source + ret.ToString, "errore")
                End Try

                Application.DoEvents()



                fase.SetState = 1
                fase.IsBlinking = True
                fase.IsSelected = True
                ritorno = True



            Else
                ret = Ls515Class.Costanti.S_LS515_USER_ABORT
                fase.SetState = 0
                fase.IsBlinking = True
                fase.IsSelected = True
                ViewError(fase)
            End If
        End If





        FileTraccia.UltimoRitornoFunzione = ret

        If ritorno = True Then
            FileTraccia.Stampante = 1
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestLedPeriferica() As Short
        Dim ritorno As Short
        Dim ret As Short
        'TBadge.Enabled = True
        'TCollaudo.SelectedTab = TBadge
        ritorno = False
        FaseEsec()
        idmsg = 257
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Iniziare il test LED (Verificare i tre Led Accesi)", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(5))
        End If

        If (dres <> DialogResult.OK) Then
            ret = Ls515Class.Costanti.S_LS515_USER_ABORT
        Else
            ret = ls515.TestLed(7)
            If ret = Ls515Class.Costanti.S_LS515_OKAY Then
                idmsg = 258
                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                    dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                Else
                    dres = MessageboxMy("I tre Led sono accesi ?", "Attenzione", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, VetImg(5))
                End If
                If (dres <> DialogResult.Yes) Then
                    ret = Ls515Class.Costanti.S_LS515_USER_ABORT
                Else

                    ritorno = True
                End If
                ret = ls515.TestLed(0)
            End If
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestTimbro(ByVal TipoTimbro As Short) As Short
        Dim Ritorno As Integer
        Dim ret As Integer
        Dim fcicla As Boolean
        Dim iii As Integer
        'Dim resdialog As DialogResult

        Ritorno = False
        'TCollaudo.SelectedTab = TTimbriStampa
        FaseEsec()

        idmsg = 206
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire 10 Documenti per la prova del Timbro", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(3))
        End If


        If (dres <> DialogResult.OK) Then
            ret = Ls515Class.Costanti.S_LS515_USER_ABORT
        Else
            iii = 0
            fcicla = True
            While (fcicla)
                ls515.NomeFileImmagine = zzz + "\Dati\TimbroFImage" + IndImgF.ToString("0000") + ".bmp"
                ls515.NomeFileImmagineRetro = zzz + "\Dati\TimbroRImage" + IndImgR.ToString("0000") + ".bmp"
                ret = ls515.Timbra(Ls515Class.Costanti.S_SORTER_BAY1)
                iii += 1
                If ret = Ls515Class.Costanti.S_LS515_OKAY Then
                    Try
                        DisposeImg()

                        Application.DoEvents()
                        Pimage.Image = System.Drawing.Image.FromFile(ls515.NomeFileImmagine)
                        PImageBack.Image = System.Drawing.Image.FromFile(ls515.NomeFileImmagineRetro)
                    Catch ex As Exception
                        ' DEBUG
                        MessageBox.Show(ex.Message + " " + ex.Source + ret.ToString, "errore")
                    End Try
                End If

                Select Case ret
                    Case Ls515Class.Costanti.S_LS515_FEEDEREMPTY
                        idmsg = 206
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("Inserire 10 Documenti per la prova del Timbro", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(3))
                        End If
                        If dres <> DialogResult.OK Then
                            fcicla = False
                            ret = Ls515Class.Costanti.S_LS515_USER_ABORT
                        End If
                        ret = Ls515Class.Costanti.S_LS515_OKAY
                        iii -= 1
                    Case Ls515Class.Costanti.S_LS515_PAPER_JAM, Ls515Class.Costanti.S_LS515_DOUBLE_LEAFING_ERROR, Ls515Class.Costanti.S_LS515_JAM_AT_MICR_PHOTO, Ls515Class.Costanti.S_LS515_JAM_AT_SCANNERPHOTO, Ls515Class.Costanti.S_LS515_JAM_DOC_TOOLONG

                        idmsg = 207
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("Togliere i documenti dal percorso carta se presenti", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                        End If

                        If dres <> DialogResult.OK Then
                            fcicla = False
                            ret = Ls515Class.Costanti.S_LS515_USER_ABORT
                        End If
                        ls515.Reset(Ls515Class.Costanti.S_RESET_FREE_PATH)
                        ret = Ls515Class.Costanti.S_LS515_OKAY
                        iii -= 1
                End Select

                If ret <> Ls515Class.Costanti.S_LS515_OKAY Or iii > TipoTimbro Then
                    fcicla = False
                    iii -= 1
                End If

                Application.DoEvents()
                IndImgF += 1
                IndImgR += 1
            End While

            If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then

                idmsg = 208
                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                    dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                Else
                    dres = MessageboxMy("Il timbro ha timbrato in modo corretto ?", "Attenzione!! ", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, Nothing)
                End If



                If (dres = DialogResult.Yes) Then
                    'LTimR.ForeColor = Color.Green
                    'LTimR.Text = "OK"
                    Ritorno = True
                Else
                    If dres = DialogResult.No Then
                        ' ii = 
                    End If
                    'LTimF.ForeColor = Color.Red
                    'LTimF.Text = "KO"

                    ret = Ls515Class.Costanti.S_LS515_USER_ABORT
                End If
            Else
                ViewError(fase)
            End If

        End If



        FileTraccia.UltimoRitornoFunzione = ret

        If Ritorno = True Then
            FaseOk()
            FileTraccia.TimbroFronte = 1
            FileTraccia.TimbroRetro = 1
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return Ritorno
    End Function

    Public Function TestCalibraTestina(ByVal Type As Short) As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim RangeMore As Single
        Dim RangeLess As Single
        Dim strmsg As String
        Dim TypeSpeed As Short
        Dim valore As Integer
        Dim Velocit As Integer
        Dim Version As String
        Dim S_Version As Single

        Dim finfo As New CtsControls.FormInfoTest

        FileTraccia.Note = ""

        ritorno = False
        'GMicr.Enabled = True
        'LResult.Text = ""

        FaseEsec()

        fWaitButton = False

        strmsg = "Inserire nella Casella di Testo il Valore del DOCUTEST --042 di calibrazione : Togliere il Timbro"



        '   If TipoDiCollaudo = COLLAUDO_PIASTRA_LS515 Or TipoDiCollaudo = COLLAUDO_PERIFERICA_LS515 Then
        Select Case Type
            Case 0
                TypeSpeed = TEST_SPEED_GRAY
                idmsg = 211
                strmsg = " Attenzione !!! Calibrazione Grigi 200 DPI Normal Speed"
            Case 1
                TypeSpeed = TEST_SPEED_COLOR_200_DPI
                idmsg = 209
                strmsg = " Attenzione !!! Calibrazione Colori 200 DPI"
            Case 2
                TypeSpeed = TEST_SPEED_COLOR_300_DPI
                idmsg = 210
                strmsg = " Attenzione !!! Calibrazione Colori 300 DPI"
            Case 6
                TypeSpeed = TEST_SPEED_800_mm
                idmsg = 274
                strmsg = " Attenzione !!! Calibrazione Grigi 200 DPI High Speed"
        End Select
        'LICodeline.Items.Add(strmsg + "--> Valore Documento  " + TValMICR.Text)
        'TCollaudo.SelectedTab() = TMessaggi

        If (TypeSpeed = TEST_SPEED_800_mm) Then
            '
            'chiamata a funzione 
            ritorno = ls515.Identificativo()
            FileTraccia.VersioneFw = ls515.SVersion

            ' Per problema VB annulo gli eventuali caratteri stringa !
            Version = ls515.SVersion.Substring(0, 4)
            S_Version = System.Convert.ToSingle(Version.Trim(" "))
            If (S_Version > 500) Then
                If (S_Version < 520.0F) Then
                    FaseOk()
                    FileTraccia.UltimoRitornoFunzione = 0 'replycode
                    FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
                    Return True
                Else
                    ret = ls515.SetHightSpeedMode()
                    If (ret <> Ls515Class.Costanti.S_LS515_OKAY) Then
                        Return False
                    End If
                End If
            Else
                If (S_Version < 5.2F) Then
                    FaseOk()
                    FileTraccia.UltimoRitornoFunzione = 0 'replycode
                    FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
                    Return True
                Else
                    ret = ls515.SetHightSpeedMode()
                    MessageBox.Show("ret <500 :" & ret)
                    If (ret <> Ls515Class.Costanti.S_LS515_OKAY) Then
                        Return False
                    End If
                End If
            End If
        End If



        finfo.PointToScreen(New Point(300, 100))
        finfo.Show()
        finfo.BringToFront()




        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            StrMessaggio = Messaggio
        Else
            StrMessaggio = strmsg
        End If


        If InputboxMy(StrMessaggio, "Attenzione", True, valore) = DialogResult.OK Then

            idmsg = 212
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Inserire nel Feeder DOCUTEST --042 'Lato Puntini...', fino ad avvenuta calibrazione:", strmsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            End If

            ' traccio sul DB il valore documento
            FileTraccia.Note += "Valore Documento: " + valore.ToString() + vbCrLf


            If dres = DialogResult.OK Then
                ls515.list = finfo.Linfo
                ret = ls515.DoMICRCalibration3(System.Convert.ToInt32(valore), 5, TypeSpeed, finfo.Linfo)
                '' salvo sul DB sempre 
                For Each s As String In finfo.Linfo.Items
                    FileTraccia.Note += s + vbCrLf
                Next

                If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then
                    ' test per calibrazione MICR ok

                    RangeMore = System.Convert.ToSingle(valore) * 4 / 100
                    RangeLess = System.Convert.ToSingle(valore) * 4 / 100

                    ' scrivo nel file di trace i valori visualizzati a video


                    If ((ls515.Percentile > (System.Convert.ToSingle(valore) - RangeLess)) And (ls515.Percentile < (System.Convert.ToSingle(valore) + RangeMore))) Then
                        ritorno = True
                    Else
                        ret = Ls515Class.Costanti.S_LS515_CALIBRATION_FAILED
                    End If

                Else
                    ViewError(fase)
                End If
            Else
                ret = Ls515Class.Costanti.S_LS515_USER_ABORT
            End If
            'Else
            ' non passerà mai di qui perche, il tipo di collaudo lo imposto io
            '   ret = Ls515Class.Costanti.S_LS515_USER_ABORT
            ' End If
        Else
            ret = Ls515Class.Costanti.S_LS515_USER_ABORT
        End If

        Velocit = 0 'velocita' di default...
        ret = ls515.SetVelocita(Velocit)


        FileTraccia.Note += vbCrLf
        FileTraccia.Note += "Velocità: " + Velocit.ToString() + vbCrLf
        FileTraccia.Note += "Reply Set Velocità: " + ret.ToString()

        FileTraccia.UltimoRitornoFunzione = ret

        finfo.Close()

        If ritorno = True Then
            FaseOk()
            FileTraccia.CalMicr = 1
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestTempoCamp(ByVal Type As Short) As Integer
        Dim ritorno As Integer
        Dim ii As Short
        Dim TypeSpeed As Short
        'Dim scan As Short
        Dim strmsg As String = ""
        Dim finfo As New CtsControls.FormInfoTest
        Dim ret As Short
        Dim Version As String
        Dim S_Version As Single




        ii = 0
        ritorno = False
        FaseEsec()

        Select Case Type
            Case 0
                TypeSpeed = TEST_SPEED_GRAY
                idmsg = 213
                strmsg = " Attenzione !!! Calibrazione Grigi 200 DPI Velocita Normale"
            Case 1
                TypeSpeed = TEST_SPEED_COLOR_200_DPI
                idmsg = 214
                strmsg = " Attenzione !!! Calibrazione Colore 200 DPI"
            Case 2
                TypeSpeed = TEST_SPEED_COLOR_300_DPI
                idmsg = 215
                strmsg = " Attenzione !!! Calibrazione Colore 300 DPI"
            Case 6
                TypeSpeed = TEST_SPEED_800_mm
                idmsg = 275
                strmsg = " Attenzione !!! Calibrazione Grigi 200 DPI Velocita Alta"
        End Select

        If (TypeSpeed = TEST_SPEED_800_mm) Then
            '
            'chiamata a funzione 
            ritorno = ls515.Identificativo()
            FileTraccia.VersioneFw = ls515.SVersion

            ' Per problema VB annulo gli eventuali caratteri stringa !
            Version = ls515.SVersion.Substring(0, 4)
            S_Version = System.Convert.ToSingle(Version.Trim(" "))

            If (S_Version > 500) Then
                If (S_Version < 520.0F) Then
                    FaseOk()
                    FileTraccia.UltimoRitornoFunzione = 0 'replycode
                    FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
                    Return True 'OCCHIO !!!!!!!!!!!!
                Else
                    ret = ls515.SetHightSpeedMode()
                    If (ret <> Ls515Class.Costanti.S_LS515_OKAY) Then
                        Return False
                    End If
                End If
            Else
                If (S_Version < 5.2F) Then
                    FaseOk()
                    FileTraccia.UltimoRitornoFunzione = 0 'replycode
                    FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
                    Return True
                Else
                    ret = ls515.SetHightSpeedMode()
                    If (ret <> Ls515Class.Costanti.S_LS515_OKAY) Then
                        Return False
                    End If
                End If
            End If
        End If


        strmsg = " Attenzione !!! Calibrazione 200 DPI "
        finfo.PointToScreen(New Point(300, 100))
        finfo.Show()
        finfo.BringToFront()
        'LICodeline.Items.Add(strmsg)

        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire DOCUTEST --042 dalla parte delle Barrette Continue", "Attenzione!!! " + strmsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        End If


        If (dres = DialogResult.OK) Then
            ls515.NrLettureOk = 0
            Do
                'If (MessageboxMy("Inserire DOCUTEST -042 dalla parte delle Barrette Continue", "Attenzione!!! " + strmsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing) = DialogResult.OK) Then
                ret = ls515.DoTimeSampleMICRCalibration(TypeSpeed)
                'LICodeline.Items.Add("Valore min: " + ls515.llMin.ToString + " Valore Medio : " + ls515.llMedia.ToString + "  Valore Massimo : " + ls515.llMax.ToString)
                ii += 1
                Dim strmsginfo As String
                strmsginfo = "Ciclo : " + ii.ToString + " Valore Massimo : " + ls515.llMax.ToString + " Valore minimo : " + ls515.llMin.ToString
                finfo.Linfo.Items.Add(strmsginfo)
                finfo.Linfo.SelectedIndex = finfo.Linfo.Items.Count - 1
                FileTraccia.Note += strmsginfo + vbCrLf
                finfo.Refresh()

                'Else
                '   ii = 8
                '    ret = False
                'End If


            Loop While ((ii < 13) And (ls515.NrLettureOk < 3) And (ret = Ls515Class.Costanti.S_LS515_RETRY))
        Else
            ret = Ls515Class.Costanti.S_LS515_USER_ABORT
            'ret = False
        End If
        ls515.SetVelocita(0)
        If (ret <> Ls515Class.Costanti.S_LS515_RETRY And ls515.NrLettureOk < 3) Then
            ritorno = False

        Else
            If ii >= 13 Then
                ritorno = False
            Else
                ritorno = True
            End If
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        finfo.Close()
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function


    Public Function TestViewSignal() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim strmsg As String
        'TMicr.Enabled = True
        ritorno = False
        FaseEsec()

        strmsg = "Inserire DOCUTEST --042 dalla parte dei punti:"

        MessageboxMy(strmsg, "Attenzione !!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        ls515.Reset(Ls515Class.Costanti.S_RESET_ERROR)
        ret = ls515.ViewE13BSignal(True)
        If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then
            'TGrafici.SelectedTab = TviewS
            Dim p As New Pen(Color.Red)

            If Ls515Class.Periferica.E13BSignal Is Nothing Then
                MessageboxMy("Errore: Grafico non disponibile", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
            Else
                ' Grafico4.SetTraccia(Ls515Class.Periferica.E13BSignal, Ls515Class.Periferica.E13BSignal.Length - 1, p)
                ' Grafico4.LTitle.Text = "Scorrere tutto il grafico"
                ' While Grafico4.fstop = False
                ' Application.DoEvents()
                ' End While
                If MessageboxMy("Il grafico è conforme ?", "Attenzione !!", MessageBoxButtons.YesNo, MessageBoxIcon.Information, Nothing) = DialogResult.Yes Then
                    ritorno = True

                Else
                    ritorno = False

                End If

            End If
        Else
            ritorno = False

        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        'TGrafici.SelectedTab = TEsci
        'Grafico4.HScrollBar1.Value = 0
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function


    Public Function TestCheckDocS() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim min, max As Byte
        Dim strmsg As String
        'TMicr.Enabled = True
        ritorno = False
        FaseEsec()

        strmsg = "Inserire DOCUTEST --042 dalla parte delle barrette continue"

        MessageboxMy(strmsg, "Attenzione !!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        ls515.Reset(Ls515Class.Costanti.S_RESET_ERROR)
        ret = ls515.ViewE13BSignal(False)
        If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then
            'TGrafici.SelectedTab = TCKDocS
            Dim p As New Pen(Color.Red)
            If Ls515Class.Periferica.E13BSignal Is Nothing Then
                MessageboxMy("Errore: Grafico non disponibile", "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
            Else
                ls515.DisegnaEstrectLenBar()
                min = Byte.MaxValue
                max = Byte.MinValue
                For ii As Short = 0 To 511
                    If Ls515Class.Periferica.E13BSignal(ii) < min Then
                        min = Ls515Class.Periferica.E13BSignal(ii)
                    End If
                    If Ls515Class.Periferica.E13BSignal(ii) > max Then
                        max = Ls515Class.Periferica.E13BSignal(ii)
                    End If
                Next
                'Grafico3.LTitle.Text = "Grafico Check Document Speed" + " Max = " + max.ToString + " Min = " + min.ToString

                'Grafico3.SetTraccia(Ls515Class.Periferica.ViewSignal, 512, p)

                If MessageboxMy("Il grafico è conforme ?", "Attenzione !!", MessageBoxButtons.YesNo, MessageBoxIcon.Information, Nothing) = DialogResult.Yes Then
                    ritorno = True
                Else
                    ritorno = False
                End If

            End If
        Else
            ritorno = False
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        ' TGrafici.SelectedTab = TEsci
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function




    Public Function TestLettura(ByVal cmc7 As Short, ByVal e13b As Short, ByVal Tipo_comtrollo As Short, ByVal ScanMode As Short) As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim smessage As String = ""
        'TMicr.Enabled = True
        ritorno = False
        FaseEsec()

        If Tipo_comtrollo = CheckCodeline.CONTROLLO_LS515_NO_TIMBRO Then
            smessage = "Inserire documenti per lettura DOCUTEST 63-46-48-61 "
            idmsg = 11216
        End If
        If Tipo_comtrollo = CheckCodeline.CONTROLLO_DOPPIA_SPESSO Then
            smessage = "Inserire documenti per lettura DOCUTEST 64-62"
            idmsg = 11216
        End If
        If Tipo_comtrollo = CheckCodeline.CONTROLLO_LS515_TIMBRO Then
            smessage = "Inserire documenti per lettura DOCUTEST 66-65-46-48-61-62 "
            idmsg = 217
        End If

        If (Tipo_comtrollo = CheckCodeline.CONTROLLO_SMAG_CMC7) Then
            smessage = "Inserire documento per lettura DOCUTEST 48"
            idmsg = 1200 ' fittizio
        End If
        If (Tipo_comtrollo = CheckCodeline.CONTROLLO_SMAG_E13B) Then
            smessage = "Inserire documento per lettura DOCUTEST 46"
            idmsg = 1200 ' fittizio
        End If


        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy(smessage, "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        End If


        If dres <> DialogResult.OK Then
            ret = Ls515Class.Costanti.S_LS515_USER_ABORT
        Else
            ret = LeggiCodelinesAuto(cmc7, e13b, Tipo_comtrollo, ScanMode)
        End If

        If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then
            ritorno = True
        Else
            ritorno = False
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function


    Public Function TestCarta(ByVal ndoc As Short) As Short

        Dim ritorno As Short
        Dim ret As Short
        Dim maxw As Integer
        Dim percentuale As Single
        Dim rr As Short
        Dim fstop As Short
        Dim media As Single
        Dim massimo, minimo As Short
        Dim somma As Integer
        Dim imagra As Bitmap
        Dim gra As Graphics
        Dim penna As New Pen(System.Drawing.Color.Red)
        Dim punti() As Point
        Dim cicli As Short
        Dim font1 As New Font("Time New Roman", 8, FontStyle.Regular, GraphicsUnit.Pixel)


        ritorno = False
        rr = 1
        'TCollaudo.SelectedTab = TTimbriStampa
        FaseEsec()

        cicli = 0


        While (fstop = False)
            penna.Color = System.Drawing.Color.Red
            idmsg = 218
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Inserire carta  DOCUTEST --043 dall' ingresso lineare", "Attenzione :  Ciclo : " + (cicli + 1).ToString + "/" + (ndoc + 1).ToString, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            End If

            If (dres <> DialogResult.OK) Then
                ret = Ls515Class.Costanti.S_LS515_USER_ABORT
                fstop = True
            Else
                DisposeImg()
                'TCollaudo.SelectedTab = TMessaggi
                StrMessaggio = "Test Carta " + (cicli + 1).ToString + "/" + (ndoc + 1).ToString + " ciclo"
                'TMessaggi.Refresh()
                ls515.NomeFileImmagine = zzz + "\Dati\cardImage" + IndImgF.ToString("0000") + ".bmp"
                ls515.NomeFileImmagineRetro = zzz + "\Dati\cardImageR" + IndImgR.ToString("0000") + ".bmp"
                ret = ls515.LeggiImmCard(System.Convert.ToSByte(Ls515Class.Costanti.S_SIDE_ALL_IMAGE))
                Select Case ret
                    Case Ls515Class.Costanti.S_LS515_PAPER_JAM, + _
                    Ls515Class.Costanti.S_LS515_JAM_AT_MICR_PHOTO, + _
                    Ls515Class.Costanti.S_LS515_JAM_AT_SCANNERPHOTO, + _
                    Ls515Class.Costanti.S_LS515_JAM_DOC_TOOLONG
                        idmsg = 219
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("ATTENZIONE CARTA BLOCCATA LIBERARE IL PERCORSO", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                        End If

                        If (dres <> DialogResult.OK) Then
                            ret = Ls515Class.Costanti.S_LS515_USER_ABORT
                            fstop = True
                        Else
                            ls515.Reset(Ls515Class.Costanti.S_RESET_FREE_PATH)
                        End If
                    Case Ls515Class.Costanti.S_LS515_FEEDEREMPTY
                        '  MessageboxMy("Inserire carta  DOCUTEST - 043 da ingresso lineare", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                    Case -1
                        idmsg = 220
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("DOCUTEST --043 Inserito in modo non corretto", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                        End If

                        If (dres <> DialogResult.OK) Then
                            ret = Ls515Class.Costanti.S_LS515_USER_ABORT
                            fstop = True
                        Else
                        End If
                    Case Else
                        minimo = 32000
                        massimo = 0
                        media = 0
                        somma = 0
                        rr = ret
                        ret = Ls515Class.Costanti.S_LS515_OKAY
                        ReDim punti(rr)
                        imagra = New Bitmap(160, 100, Imaging.PixelFormat.Format24bppRgb)

                        gra = Graphics.FromImage(imagra)
                        Dim trasmatrix As New Drawing2D.Matrix(1, 0, 0, -1, 0, 100)
                        gra.MultiplyTransform(trasmatrix)

                        For ii = 0 To rr
                            If (Ls515Class.Periferica.Distanze(ii) < minimo) Then
                                minimo = Ls515Class.Periferica.Distanze(ii)
                            End If
                            If (Ls515Class.Periferica.Distanze(ii) > massimo) Then
                                massimo = Ls515Class.Periferica.Distanze(ii)
                            End If
                            somma += Ls515Class.Periferica.Distanze(ii)
                            punti(ii).X = ii
                            punti(ii).Y = Ls515Class.Periferica.Distanze(ii)

                        Next
                        media = somma / (rr + 1)
                        For ii = 0 To rr
                            punti(ii).Y -= 254
                            punti(ii).Y += 50
                        Next




                        gra.FillRectangle(New SolidBrush(Color.White), New Rectangle(0, 0, 160, 100))
                        gra.DrawLines(penna, punti)
                        penna.Color = System.Drawing.Color.Blue
                        gra.DrawLine(penna, 0, 50, 160, 50)
                        penna.Color = System.Drawing.Color.Gold
                        gra.DrawLine(penna, 0, 50 + 12, 160, 50 + 12)
                        gra.DrawLine(penna, 0, 50 - 12, 160, 50 - 12)


                        PFronte.Image = System.Drawing.Image.FromFile(zzz + "\Dati\cardImage" + IndImgF.ToString("0000") + ".bmp")
                        PRetro.Image = System.Drawing.Image.FromFile(zzz + "\Dati\cardImageR" + IndImgR.ToString("0000") + ".bmp")
                        Pimage.Image = PFronte.Image
                        PImageBack.Image = PRetro.Image
                        If (PFronte.Image.Width > PRetro.Image.Width) Then
                            maxw = PFronte.Image.Width
                        Else
                            maxw = PRetro.Image.Width
                        End If

                        percentuale = ((System.Math.Abs(PFronte.Image.Width - PRetro.Image.Width)) / maxw) * 100

                        If (percentuale > 10) Then
                            MessageboxMy(percentuale.ToString, "d", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                        Else
                            'MessageBox.Show("ANALISI VELCITA'" + vbCrLf _
                            '                + "Media : " + media.ToString("F") + vbCrLf _
                            '                + "Massimo  : " + massimo.ToString + vbCrLf _
                            '                + "Minimo : " + minimo.ToString + vbCrLf _
                            '                + "Valore Teorico Velorità' : 265" + vbCrLf _
                            '                + "PERCENTUALI" + vbCrLf _
                            '                + "Rispetto al Valore teorico:  " + (((265 - minimo) / 265) * 100).ToString("F") + "   " + (((massimo - 265) / 265) * 100).ToString("F") + vbCrLf _
                            '                + "Rispetto al Valore di Media : " + (((media - minimo) / media) * 100).ToString("F") + "   " + (((massimo - media) / media) * 100).ToString("F") + vbCrLf _
                            '                , "Risultati Anlisi", MessageBoxButtons.OK)
                            MessageboxMy("ANALISI VELCITA'" + vbCrLf _
                                            + " Valore Teorico Velocità' : 254" + vbCrLf _
                                            + "Media : " + media.ToString("F") + " " _
                                            + " Massimo  : " + massimo.ToString _
                                            + " Minimo : " + minimo.ToString + vbCrLf _
                                            + "PERCENTUALI" + vbCrLf _
                                            + "Rispetto al Valore teorico: Min -" + (((254 - minimo) / 254) * 100).ToString("F") + "   MAX " + (((massimo - 254) / 254) * 100).ToString("F") + vbCrLf _
                                            + "Rispetto al Valore di Media : Min -" + (((media - minimo) / media) * 100).ToString("F") + "   MAX " + (((massimo - media) / media) * 100).ToString("F") + vbCrLf _
                                            , "Risultati Analisi", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, imagra)
                        End If
                        If cicli = ndoc Then
                            fstop = True
                        End If
                        cicli += 1
                End Select
            End If
        End While


        FileTraccia.UltimoRitornoFunzione = ret
        If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then
            ritorno = True
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestAspetta(ByVal Acceso As Boolean) As Short
        'Dim fff As New FOnOff
        Dim ret As Short
        'If (Acceso = True) Then
        '    fff.LTezt.Text = "Accendere la periferica"
        'Else
        '    fff.LTezt.Text = "Spegnere la periferica"
        'End If
        'Application.DoEvents()
        'fff.Show()

        'If (Acceso = True) Then
        '    fff.Refresh()
        '    ret = TestON()
        'Else
        '    fff.Refresh()
        '    ret = TestOff()
        'End If

        'If ret = 1 Then
        '    ret = True
        '    fff.Close()
        'Else
        '    ret = False
        '    fff.Close()
        'End If
        Return ret
    End Function

    Public Function TestOff() As Short
        Dim ritorno As Short
        Dim ret As Short
        ret = 0
        ritorno = False
        FaseEsec()

        While (ret >= 0)
            ret = ls515.Identificativo()
        End While
        If ret = Ls515Class.Costanti.S_LS515_PERIFNOTFOUND Then
            ritorno = True
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        Return ritorno
    End Function

    Public Function TestON() As Short
        Dim ritorno As Short
        Dim ret As Short
        ret = 1
        ritorno = False
        FaseEsec()

        While (ret <> 0)
            ret = ls515.Identificativo()
        End While
        If ret = 0 Then
            ritorno = True
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        Return ritorno
    End Function

    Public Function TestSfondo(ByVal image As Image, ByVal Soglia As Integer, ByRef RigheContigue As Integer, ByRef ContatoretotaleRighe As Integer, ByRef indice As Integer) As Boolean

        ' il test fallisce se ho un NumeroMaxRigheContigue di righe countigue che superano la soglia
        ' o se una riga supera  la sogliaMassima

        Dim sommariga As Short
        Dim fallito As Boolean
        'Dim ritorno As Integer

        Dim NumeroMaxRigheContigue As Integer
        Dim SogliaMassima As Integer

        Dim TempBitmap As New Bitmap(image)
        Dim MyColor As Color
        Dim ii As Integer
        MyColor = TempBitmap.GetPixel(1, 1)

        Soglia = 32
        SogliaMassima = 64
        NumeroMaxRigheContigue = 10
        ContatoretotaleRighe = 0
        RigheContigue = 0
        'FaseEsec()

        fallito = False



        For ii = TempBitmap.Height - 1 To 0 Step -1
            sommariga = 0
            MyColor = TempBitmap.GetPixel(0, ii)
            sommariga = sommariga + ((System.Convert.ToInt16(MyColor.R) + System.Convert.ToInt16(MyColor.G) + System.Convert.ToInt16(MyColor.B)) / 3)
            MyColor = TempBitmap.GetPixel(1, ii)
            sommariga = sommariga + ((System.Convert.ToInt16(MyColor.R) + System.Convert.ToInt16(MyColor.G) + System.Convert.ToInt16(MyColor.B)) / 3)
            MyColor = TempBitmap.GetPixel(2, ii)
            sommariga = sommariga + ((System.Convert.ToInt16(MyColor.R) + System.Convert.ToInt16(MyColor.G) + System.Convert.ToInt16(MyColor.B)) / 3)
            MyColor = TempBitmap.GetPixel(3, ii)
            sommariga = sommariga + ((System.Convert.ToInt16(MyColor.R) + System.Convert.ToInt16(MyColor.G) + System.Convert.ToInt16(MyColor.B)) / 3)
            MyColor = TempBitmap.GetPixel(4, ii)
            sommariga = sommariga + ((System.Convert.ToInt16(MyColor.R) + System.Convert.ToInt16(MyColor.G) + System.Convert.ToInt16(MyColor.B)) / 3)
            MyColor = TempBitmap.GetPixel(5, ii)
            sommariga = sommariga + ((System.Convert.ToInt16(MyColor.R) + System.Convert.ToInt16(MyColor.G) + System.Convert.ToInt16(MyColor.B)) / 3)
            MyColor = TempBitmap.GetPixel(6, ii)
            sommariga = sommariga + ((System.Convert.ToInt16(MyColor.R) + System.Convert.ToInt16(MyColor.G) + System.Convert.ToInt16(MyColor.B)) / 3)

            sommariga = sommariga / 7

            If (sommariga >= SogliaMassima) Then
                fallito = True
                Exit For
            End If

            If (sommariga > Soglia) Then
                RigheContigue = RigheContigue + 1
                ContatoretotaleRighe = ContatoretotaleRighe + 1

                If (RigheContigue >= NumeroMaxRigheContigue) Then
                    fallito = True
                    Exit For
                End If

                If ContatoretotaleRighe > 80 Then
                    fallito = True
                    Exit For
                End If

            Else
                RigheContigue = 0

            End If
        Next
        'If fallito = True Then
        '    ritorno = True
        'Else
        '    ritorno = False
        'End If

        'If ritorno = True Then
        '    FaseOk()
        'Else
        '    FaseKo()
        'End If
        indice = TempBitmap.Height - ii
        'BVisualZoom = False
        'FileTraccia.UltimoRitornoFunzione = ret
        'FileTraccia.StrRet = ls150.ReturnC.FunzioneChiamante
        Return fallito

    End Function


    'non viene mai montato sul 515
    Public Function TestBadge(ByVal Tr As Short) As Short
        Dim ritorno As Short
        Dim ret As Short
        Dim strBadge As String
        Dim strBadgeNew As String
        strBadge = "A1111000022223333444455556666777888999A11111111111000000000002222222222233333333333444444444455555555556666666666777777777788888888889999999999"
        strBadgeNew = "tARCA BADGE CAMPIONE 11111111111111111111111111111111111111111111111111111111t1111000022223333444455556666777888999t11111111111000000000002222222222233333333333444444444455555555556666666666777777777788888888889999999999"
        'TBadge.Enabled = True
        'TCollaudo.SelectedTab = TBadge
        ritorno = False
        FaseEsec()



        idmsg = 222
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Strisciare Badge DOCUTEST --007", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(5))
        End If

        If (dres <> DialogResult.OK) Then
            ret = Ls515Class.Costanti.S_LS515_USER_ABORT
        Else
            ret = ls515.LeggiBadge(Tr)
            If ret = Ls515Class.Costanti.S_LS515_OKAY Then
                'LBadge.Text = ls515.BadgeLetto

                If (ls515.BadgeLetto = strBadge Or ls515.BadgeLetto = strBadgeNew) Then
                    idmsg = 223
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        dres = MessageboxMy(ls515.BadgeLetto + vbCrLf + Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy(ls515.BadgeLetto + vbCrLf + " Le tracce del badge sono corrette", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                    End If
                    ritorno = True
                Else
                    idmsg = 241
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        dres = MessageboxMy(ls515.BadgeLetto + vbCrLf + Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy(ls515.BadgeLetto + vbCrLf + "Tracce del badge lette non correttamente", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                    End If
                    ritorno = False
                    ret = Ls515Class.Costanti.S_LS515_USER_ABORT
                End If

            Else
                ritorno = False
            End If
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function



    Public Function TestLunghezzaImmagine() As Integer
        Dim ret As Short
        Dim ritorno As Short
        Dim contatore As Integer = 1

        ritorno = False
        BVisualZoom = False
        FaseEsec()
        Do
            idmsg = 224
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Inserire DOCUTEST --042 Per calibrazione lunghezza immagine, sino all' esito", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            End If

            If (dres = DialogResult.OK) Then
                ret = ls515.CalibraImmagine()
            Else
                contatore = 15
                ret = -1000
            End If
            contatore = contatore + 1
        Loop While (ret <> 0 And contatore < 10)
        If ret = 0 Then
            FileTraccia.UltimoRitornoFunzione = ret
            ret = True
        End If
        If ret = -1000 Then
            FileTraccia.UltimoRitornoFunzione = Ls515Class.Costanti.S_LS515_USER_ABORT
            ret = False
        End If

        If ret = True Then
            ritorno = True
        Else

            StrCommento += "Calibrazione non a buon fine: " + ret.ToString + " "
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestAssegnoSpesso() As Integer
        Dim ret As Short
        Dim ritorno As Integer
        ritorno = False
        FaseEsec()

        idmsg = 225
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire Docutest--052 piegato doppio ", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        End If


        If (dres = DialogResult.OK) Then
            ret = ls515.Docutest04()
            If (ret = Ls515Class.Costanti.S_LS515_DOUBLE_LEAFING_ERROR) Then
                Sleep(2)
                ret = ls515.Reset(Ls515Class.Costanti.S_RESET_FREE_PATH)
                If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then
                    ' spegnere e accendere la perfierica
                    ritorno = True
                Else
                    idmsg = 226
                    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                    Else
                        dres = MessageboxMy("Accendere e Spegnere la periferica", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                    End If


                    TestAspetta(False)
                    TestAspetta(True)
                    ret = Ls515Class.Costanti.S_LS515_OKAY
                    ritorno = True
                End If
            Else


            End If
        Else
            ritorno = False
            ret = Ls515Class.Costanti.S_LS515_USER_ABORT
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestSmagnetizzato(ByVal BCMC7 As Boolean) As Integer
        Dim ritorno As Integer
        Dim ret As Integer
        Dim str, str1 As String
        Dim chcd As New CheckCodeline
        ritorno = False
        FaseEsec()
        If (BCMC7 = True) Then
            LettureCMC7 = 1
            LettureE13B = 0
            TipoComtrolloLetture = CheckCodeline.CONTROLLO_SMAG_CMC7
            str = "Smagnetizzare un DOCUTEST CMC7 Smagnetizzato 49"
            idmsg = 227

        Else
            LettureE13B = 1
            LettureCMC7 = 0
            TipoComtrolloLetture = CheckCodeline.CONTROLLO_SMAG_E13B
            str = "Smagnetizzare un DOCUTEST E13B Smagnetizzato 46"
            idmsg = 229

        End If

        TipoComtrolloLetture = 0

        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy(str, "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Information, Nothing)
        End If


        If (dres = DialogResult.OK) Then
            If BCMC7 = True Then
                idmsg = 228
                str1 = "Inserire un DOCUTEST CMC7 Smagnetizzato 49"
            Else
                idmsg = 230
                str1 = "Inserire un DOCUTEST E13B Smagnetizzato 46"
            End If

            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy(str1, "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Information, Nothing)
            End If


            If (dres = DialogResult.OK) Then
                ret = ls515.LeggiCodeline(Ls515Class.Costanti.S_SORTER_BAY1)
                If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then

                    chcd.DaLeggereCMC7 = LettureCMC7
                    chcd.DaLeggereE13B = LettureE13B
                    chcd.Tipo_controllo = TipoComtrolloLetture
                    If chcd.CheckCodeline(ls515.CodelineLetta) = 0 Then
                        'conteggio e vedo se ho finito
                        If chcd.Conteggiacodeline() = 1 Then
                            ret = Ls515Class.Costanti.S_LS515_OKAY
                        Else
                            ret = Ls515Class.Costanti.S_LS515_CODELINE_ERROR
                            idmsg = 231
                            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                            Else
                                dres = MessageboxMy("Controllare la presenza del magnete", "Errore !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                            End If


                        End If
                    Else
                        ret = Ls515Class.Costanti.S_LS515_CODELINE_ERROR
                        idmsg = 231
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("Controllare la presenza del magnete", "Errore !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                        End If

                    End If
                Else

                    ViewError(fase)
                End If
            Else
                ret = Ls515Class.Costanti.S_LS515_USER_ABORT
            End If
        Else
            ret = Ls515Class.Costanti.S_LS515_USER_ABORT
        End If
        FileTraccia.UltimoRitornoFunzione = ret

        If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then
            ritorno = True
        Else
            ritorno = False
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    'Public Function LeggiCodelinesOCR(ByVal ocr As Short, ByVal TypeCode As Short) As Integer
    '    Dim ret As Short

    '    Dim fstop As Boolean = False
    '    Dim ferror As Boolean = False
    '    Dim CodelineConfronto As String = ""
    '    Dim ii As Integer
    '    ii = 0
    '    ret = 0

    '    '      MessageboxMy("Inserire Documento lettura ", "Attenzione !! ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
    '    While (fstop = False)
    '        ls515.NomeFileImmagine = zzz + "\Quality\OcrImage" + IndImgF.ToString("0000")  + ".bmp"
    '        IndImgF += 1
    '        ret = ls515.LeggiCodelineOCR(TypeCode)
    '        If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then
    '            DisposeImg()

    '            PFronte.Image = System.Drawing.Image.FromFile(ls515.NomeFileImmagine)
    '            Pimage.Image = PFronte.Image
    '            ' LICodeline.Items.Add(ls515.CodelineLetta)
    '            ' carico la codeline rispetto al tipo passato
    '            If (TypeCode = Ls515Class.Costanti.S_READ_OPTIC_E13B_X_SWITCH) Then
    '                CodelineConfronto = "B0123456789+><012345678901234567890123456789X"
    '            End If

    '            If (TypeCode = Ls515Class.Costanti.S_READ_CODELINE_SW_OCRB_ITALY) Then
    '                CodelineConfronto = "B0123456789+><012345678901234567890123456789"
    '            End If

    '            If String.Compare(ls515.CodelineLetta, CodelineConfronto) = 0 Then
    '                ii += 1
    '                If ii = ocr Then
    '                    fstop = True
    '                    ferror = False
    '                End If
    '            Else
    '                'erroe di lettura nella codeline
    '                ii = 0
    '                fstop = True
    '                ferror = True
    '                MessageboxMy("Codeline Errata", "Errore !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
    '            End If
    '        Else
    '            ' processa do non a buon fine
    '            If (ls515.Reply = Ls515Class.Costanti.S_LS515_FEEDEREMPTY) Then
    '                ' Me.Refresh()
    '                Application.DoEvents()
    '            Else
    '                fstop = True
    '                ferror = True
    '                ViewError(fase)
    '            End If

    '        End If
    '    End While
    '    If ferror Then
    '        ret = -30000
    '    End If
    '    Return ret
    'End Function


    Public Function LeggiCodelines(ByVal cmc7 As Short, ByVal e13b As Short) As Integer
        Dim ret As Short

        Dim fstop As Boolean = False
        Dim ferror As Boolean = False
        Dim ii As Integer
        ii = 1
        chcd.Reset()
        chcd.VisualizzaFinestra(True)
        'chcd.F.Parent = Me

        ret = 0
        TipoComtrolloLetture = 2

        '      MessageboxMy("Inserire Documento lettura ", "Attenzione !! ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        While (fstop = False)
            ls515.NomeFileImmagine = zzz + "\Dati\CodeFImage" + IndImgF.ToString("0000") + ".bmp"
            ls515.NomeFileImmagineRetro = zzz + "\Dati\CodeRImage" + IndImgR.ToString("0000") + ".bmp"
            IndImgF += 1
            IndImgR += 1

            ret = ls515.LeggiCodelineImmagini(Ls515Class.Costanti.S_SORTER_BAY1, 1)
            If (ret = Ls515Class.Costanti.S_LS515_DOUBLE_LEAFING_ERROR) Then
                idmsg = 233
                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                    dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                Else
                    dres = MessageboxMy("Errore double leafing...Continuare?", "Errore !!!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing)
                End If


                If (dres = DialogResult.Yes) Then
                    ret = ls515.Reset(Ls515Class.Costanti.S_RESET_FREE_PATH)
                    ret = Ls515Class.Costanti.S_LS515_OKAY
                Else
                    fstop = True
                    ferror = True
                    ViewError(fase)
                End If

            End If
            If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then
                If (Not (ls515.CodelineLetta Is Nothing)) Then
                    'LICodeline.Items.Add(ls515.CodelineLetta)

                    Try
                        DisposeImg()
                        Pimage.Image = System.Drawing.Image.FromFile(ls515.NomeFileImmagine)
                        PImageBack.Image = System.Drawing.Image.FromFile(ls515.NomeFileImmagineRetro)

                    Catch ex As Exception
                        ' DEBUG
                        ' MessageBox.Show(ex.Message + " " + ex.Source, "errore")
                    End Try

                    LettureCMC7 = cmc7
                    LettureE13B = e13b
                    chcd.DaLeggereCMC7 = LettureCMC7
                    chcd.DaLeggereE13B = LettureE13B
                    chcd.Tipo_controllo = TipoComtrolloLetture

                    If chcd.CheckCodeline(ls515.CodelineLetta) = 0 Then
                        'conteggio e vedo se ho finito

                        If chcd.Conteggiacodeline() = 1 Then

                            'Me.Refresh()
                            chcd.F.Refresh()
                            'chcd.p.Show()
                            ' ho finito di contare senza errori
                            fstop = True
                            ret = 1
                        Else
                            ' non ho finito di contare
                            'Me.Refresh()
                            chcd.F.Refresh()
                        End If
                    Else
                        'erroe di lettura nella codeline
                        fstop = True
                        ferror = True
                        idmsg = 232
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy(Messaggio + ls515.CodelineLetta, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("Codeline Errata: " + ls515.CodelineLetta, "Errore !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                        End If


                    End If
                End If
            Else
                ' processa do non a buon fine
                If (ls515.Reply = Ls515Class.Costanti.S_LS515_FEEDEREMPTY) Then
                    ' Me.Refresh()
                    Application.DoEvents()
                Else
                    fstop = True
                    ferror = True
                    ViewError(fase)
                End If

            End If
        End While
        chcd.F.Refresh()
        chcd.VisualizzaFinestra(False)

        Return ret
    End Function

    Public Function LeggiCodelinesAuto(ByVal cmc7 As Short, ByVal e13b As Short, ByVal Tipo_controllo As Short, ByVal ScanMode As Short) As Integer
        Dim ret As Short
        Dim ValDoppia As Short

        Dim fstop As Boolean = False
        Dim ferror As Boolean = False
        Dim ii As Integer
        Dim ContaSorterFull As Integer
        Dim chcd As New CheckCodeline
        ContaSorterFull = 0
        ii = 1
        chcd.Reset()
        chcd.VisualizzaFinestra(True)
        chcd.Tipo_controllo = Tipo_controllo

        ValDoppia = 50
        'chcd.F.Parent = Me
        Select Case Tipo_controllo
            Case CheckCodeline.CONTROLLO_DOPPIA_DEF
                ValDoppia = 50
            Case CheckCodeline.CONTROLLO_DOPPIA_SPESSO
                ValDoppia = 40

        End Select

        ret = Ls515Class.Costanti.S_LS515_OKAY

        ret = ls515.ViaDocumentiAuto(ScanMode, ValDoppia)
        If ret = Ls515Class.Costanti.S_LS515_OKAY Then
            '      MessageboxMy("Inserire Documento lettura ", "Attenzione !! ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            While (fstop = False)
                ls515.NomeFileImmagine = zzz + "\Dati\CodeFImage" + IndImgF.ToString("0000") + ".bmp"
                ls515.NomeFileImmagineRetro = zzz + "\Dati\CodeRImage" + IndImgR.ToString("0000") + ".bmp"
                IndImgF += 1
                IndImgR += 1


                ret = ls515.LeggiCodelineImmaginiAuto(Ls515Class.Costanti.S_SORTER_BAY1)
                ' 35 è sorter full
                '  Me.Refresh()
                Application.DoEvents()
                Select Case ret
                    Case Ls515Class.Costanti.S_LS515_DOUBLE_LEAFING_ERROR
                        ls515.Stopaaaaa()
                        'If (MessageboxMy("Errore double leafing...Continuare?", "Errore !!!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing) = DialogResult.Yes) Then

                        '    ret = Ls515Class.Costanti.S_LS515_OKAY
                        '    ret = ls515.ViaDocumentiAuto(ScanMode)
                        'Else
                        fstop = True
                        ferror = True
                        ViewError(fase)
                        ls515.Reset(Ls515Class.Costanti.S_RESET_FREE_PATH)
                        'End If
                        'Case 7
                        '    ls515.Stopaaaaa()
                        '    idmsg = 125
                        '    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                        '        dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.YesNo, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        '    Else
                        '        dres = MessageboxMy("Warning double leafing...Continuare?", "Errore !!!", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing)
                        '    End If

                        '    If (dres = DialogResult.Yes) Then
                        '        ret = ls515.Reset(Ls515Class.Costanti.S_RESET_FREE_PATH)
                        '        ret = Ls515Class.Costanti.S_LS515_OKAY
                        '        ret = ls515.ViaDocumentiAuto(ScanMode)
                        '    Else
                        '        fstop = True
                        '        ferror = True
                        '        ret = Ls515Class.Costanti.S_LS515_USER_ABORT
                        '        ViewError(fase)
                        '    End If
                    Case Ls515Class.Costanti.S_LS515_PAPER_JAM, + _
                           Ls515Class.Costanti.S_LS515_JAM_AT_MICR_PHOTO, + _
                           Ls515Class.Costanti.S_LS515_JAM_AT_SCANNERPHOTO, + _
                            Ls515Class.Costanti.S_LS515_JAM_DOC_TOOLONG
                        ls515.Stopaaaaa()
                        fstop = True
                        ferror = True
                        ViewError(fase)
                        ls515.Reset(Ls515Class.Costanti.S_RESET_FREE_PATH)
                    Case Ls515Class.Costanti.S_LS515_FEEDEREMPTY, 10
                        ls515.Stopaaaaa()

                        ret = Ls515Class.Costanti.S_LS515_OKAY
                        ret = ls515.ViaDocumentiAuto(ScanMode, ValDoppia)
                        '  Me.Refresh()
                        Application.DoEvents()
                        'Case 35  ' sorter bin full
                        '    ls515.Stopaaaaa()
                        '    '  MessageboxMy("Vuotare sorte", "erore", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                        '    ret = Ls515Class.Costanti.S_LS515_OKAY
                        '    ret = ls515.ViaDocumentiAuto(ScanMode)

                    Case 35
                        ContaSorterFull += 1
                        If ContaSorterFull >= 2 Then
                            ContaSorterFull = 0
                            idmsg = 242
                            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                            Else
                                dres = MessageboxMy("Attenzione Bin Sorter 1 Pieno !! Svuotare Documenti", "Errore !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                            End If
                        End If
                    Case 36
                        ContaSorterFull += 1
                        If ContaSorterFull >= 2 Then
                            ContaSorterFull = 0
                            idmsg = 242
                            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                            Else
                                dres = MessageboxMy("Attenzione Bin Sorter 2 Pieno !! Svuotare Documenti", "Errore !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                            End If
                        End If

                    Case Ls515Class.Costanti.S_LS515_OKAY, 7 ' per okay non faccio nulla

                        If (Not (ls515.CodelineLetta Is Nothing)) Then
                            ' LICodeline.Items.Add(ls515.CodelineLetta)


                            Try
                                DisposeImg()

                                Pimage.Image = System.Drawing.Image.FromFile(ls515.NomeFileImmagine)
                                PImageBack.Image = System.Drawing.Image.FromFile(ls515.NomeFileImmagineRetro)
                                Me.formpadre.Refresh()

                            Catch ex As Exception
                                ' DEBUG
                                MessageBox.Show(ex.Message + " " + ex.Source + ret.ToString, "errore")
                            End Try
                            '  Me.Refresh()
                            Application.DoEvents()
                            LettureCMC7 = cmc7
                            LettureE13B = e13b
                            chcd.DaLeggereCMC7 = LettureCMC7
                            chcd.DaLeggereE13B = LettureE13B


                            If chcd.CheckCodeline(ls515.CodelineLetta) = 0 Then
                                'conteggio e vedo se ho finito
                                If chcd.Conteggiacodeline() = 1 Then

                                    'Me.Refresh()
                                    chcd.F.Refresh()
                                    'chcd.p.Show()
                                    ' ho finito di contare senza errori

                                    fstop = True
                                    ferror = False

                                Else
                                    ' non ho finito di contare
                                    'Me.Refresh()
                                    chcd.F.Refresh()
                                End If
                            Else
                                'erroe di lettura nella codeline
                                ls515.Stopaaaaa()
                                fstop = True
                                ferror = True
                                ret = Ls515Class.Costanti.S_LS515_CODELINE_ERROR
                                idmsg = 232
                                If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                                    dres = MessageboxMy(Messaggio + ls515.CodelineLetta, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                                Else
                                    dres = MessageboxMy("Codeline Errata: " + ls515.CodelineLetta, "Errore !!", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                                End If



                                'ret = ls515.ViaDocumentiAuto(ScanMode)
                            End If


                        End If

                    Case Else
                        fstop = True
                        ferror = True
                        ViewError(fase)
                End Select


                ' processa do non a buon fine
                'Else ' fi reply <> 0
            End While
        End If

        chcd.F.Refresh()
        chcd.VisualizzaFinestra(False)
        'ls515
        ls515.Stopaaaaa()
        Return ret
    End Function

#If 0 Then
    Public Function LeggiImmagine(ByVal side As System.Byte, ByVal ScanMode As Short) As Integer

        Dim retZoom As DialogResult
        Dim ret As Integer
        Dim Str As String


        DirZoom = Application.StartupPath
        Dim a As Short
        a = DirZoom.LastIndexOf("\")
        DirZoom = DirZoom.Substring(0, a)
        If (System.Convert.ToInt16(side) = Ls515Class.Costanti.S_SIDE_FRONT_IMAGE) Then
            Str = "FRONTE"
            If fquality = True Then
                ls515.NomeFileImmagine = zzz + "\Quality\FrontImage" + IndImgF.ToString("0000") + ".bmp"
                DirZoom += "\Quality\"
            Else
                ls515.NomeFileImmagine = zzz + "\Dati\FrontImage" + IndImgF.ToString("0000") + ".bmp"
                DirZoom += "\Dati\"
            End If
            'GScanF.Enabled = True
        Else
            Str = "RETRO"
            If fquality = True Then
                ls515.NomeFileImmagine = zzz + "\Quality\BackImage" + IndImgR.ToString("0000") + ".bmp"
                DirZoom += "\Quality\"
            Else
                ls515.NomeFileImmagine = zzz + "\Dati\BackImage" + IndImgR.ToString("0000") + ".bmp"
                DirZoom += "\Dati\"
            End If
            'GscanR.Enabled = True
        End If
        'DEBUG
        'MessageBox.Show(ls515.NomeFileImmagine)
        FileZoomCorrente = ls515.NomeFileImmagine

        idmsg = 234
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio + Str, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire un documento per verificare l'immagine " + Str, "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Question, VetImg(7))
        End If

        If dres = DialogResult.OK Then

            ret = ls515.LeggiImmagini(side, ScanMode)
            If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then
                If (System.Convert.ToInt16(side) = Ls515Class.Costanti.S_SIDE_FRONT_IMAGE) Then
                    DisposeImg()
                    PFronte.Image = System.Drawing.Image.FromFile(ls515.NomeFileImmagine)
                    Pimage.Image = PFronte.Image
                    IndImgF += 1

                    ' retZoom = ShowZoom(DirZoom, ls515.NomeFileImmagine)
                Else
                    DisposeImg()
                    PRetro.Image = System.Drawing.Image.FromFile(ls515.NomeFileImmagine)
                    Pimage.Image = PRetro.Image
                    IndImgR += 1

                    'retZoom = ShowZoom(DirZoom, ls515.NomeFileImmagine)
                End If
                Application.DoEvents()

                BVisualZoom = True
                'DirZoom += "\Quality\"
                ' MessageboxMy("La qualità dell' immagine " + Str + " è buona?", "ATTENZIONE", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing) = DialogResult.Yes 
                If retZoom = DialogResult.OK Then
                    ret = Ls515Class.Costanti.S_LS515_OKAY
                    fWaitButton = True
                Else

                    fWaitButton = False
                    If (System.Convert.ToInt16(side) = Ls515Class.Costanti.S_SIDE_FRONT_IMAGE) Then


                        'BCalF.Enabled = True
                        'BvisualF.Enabled = True
                        'BAnnulla.Enabled = True
                    Else
                        'BCalF.Enabled = True
                        'BvisualF.Enabled = True
                        'BAnnulla.Enabled = True
                    End If

                    ret = Ls515Class.Costanti.S_LS515_USER_ABORT
                    ' devo aspettare la gestione dei bottoni 
                    ' per lasciare fare all'utente come vuole
                End If
            Else
                If (System.Convert.ToInt16(side) = Ls515Class.Costanti.S_SIDE_FRONT_IMAGE) Then
                    'BCalF.Enabled = True
                    'BvisualF.Enabled = True
                    'BAnnulla.Enabled = True
                Else

                    'BCalF.Enabled = True
                    'BvisualF.Enabled = True
                    'BAnnulla.Enabled = True
                End If
                ViewError(fase)
            End If
        Else
            ret = Ls515Class.Costanti.S_LS515_USER_ABORT
            If (System.Convert.ToInt16(side) = Ls515Class.Costanti.S_SIDE_FRONT_IMAGE) Then
                'BCalF.Enabled = True
                'BvisualF.Enabled = True
                'BAnnulla.Enabled = True
            Else

                'BCalF.Enabled = True
                'BvisualF.Enabled = True
                'BAnnulla.Enabled = True

            End If
        End If
        BVisualZoom = False
        Return ret
    End Function
#End If

    'Funzione per test qulità immagine :
    ' usata solamente per collaudo periferica ls515-6 e ls515-7

    'Public Function TestQualitaImmagine(ByVal side As System.Byte) As Integer
    '    Dim ritorno As Integer
    '    Dim retZoom As DialogResult
    '    Dim ret As Integer
    '    Dim str As String
    '    Dim Iquali As Image

    '    Fqualit = New CtsControls.FqualImg

    '    ritorno = False
    '    fquality = True

    '    ' TCollaudo.SelectedTab() = TScanner
    '    FaseEsec()
    '    If (System.Convert.ToInt16(side) = Ls515Class.Costanti.S_SIDE_FRONT_IMAGE) Then
    '        str = "FRONTE"
    '        ls515.NomeFileImmagine = zzz + "\Quality\FrontImage" + IndImgF.ToString("0000") + ".bmp"
    '        'GScanF.Enabled = True
    '        ' Iquali = VetImg(7)
    '    Else
    '        str = "RETRO"
    '        ls515.NomeFileImmagine = zzz + "\Quality\BackImage" + IndImgR.ToString("0000") + ".bmp"
    '        'GscanR.Enabled = True
    '        'Iquali = VetImg(9)
    '    End If
    '    'DEBUG
    '    'MessageBox.Show(ls515.NomeFileImmagine)

    '    idmsg = 126
    '    If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
    '        dres = MessageboxMy(Messaggio + str, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
    '    Else
    '        dres = MessageboxMy("Inserire un documento per verificare l'immagine " + str, "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Question, VetImg(7))
    '    End If

    '    If dres = DialogResult.OK Then

    '        ret = ls515.LeggiImmagini(side)
    '        If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then
    '            If (System.Convert.ToInt16(side) = Ls515Class.Costanti.S_SIDE_FRONT_IMAGE) Then
    '                DisposeImg()

    '                Pimage.Image = System.Drawing.Image.FromFile(ls515.NomeFileImmagine)
    '                IndImgF += 1
    '            Else
    '                DisposeImg()
    '                PImageBack.Image = System.Drawing.Image.FromFile(ls515.NomeFileImmagine)
    '                IndImgR += 1
    '            End If
    '            Application.DoEvents()

    '            BVisualZoom = True
    '            DirZoom = Application.StartupPath
    '            Dim a As Short
    '            a = DirZoom.LastIndexOf("\")
    '            DirZoom = DirZoom.Substring(0, a)
    '            DirZoom += "\Quality\"
    '            FileZoomCorrente = ls515.NomeFileImmagine
    '            ' Zoom Immagine 
    '            retZoom = ShowZoom(DirZoom, FileZoomCorrente)
    '            'MessageboxMy("La qualità dell' immagine " + str + " è buona?", "ATTENZIONE", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing) = DialogResult.Yes
    '            If retZoom = DialogResult.OK Then
    '                ritorno = True
    '            Else
    '                Fqualit.ShowDialog()

    '                Select Case Fqualit.ret
    '                    Case 0
    '                        ritorno = False
    '                    Case 1
    '                        ritorno = TestScannersPeriferica()
    '                        If ritorno = True Then
    '                            ritorno = TestQualitaImmagine(70)
    '                            If ritorno = True Then
    '                                ritorno = TestQualitaImmagine(66)
    '                            End If

    '                        End If

    '                    Case 2
    '                        ritorno = TestQualitaImmagine(side)
    '                End Select
    '            End If
    '        Else ' Leggi immagini non a buon ine
    '            ViewError(fase)
    '            Select Case ret
    '                Case Ls515Class.Costanti.S_LS515_JAM_AT_SCANNERPHOTO, + _
    '                    Ls515Class.Costanti.S_LS515_JAM_AT_MICR_PHOTO, + _
    '                    Ls515Class.Costanti.S_LS515_JAM_DOC_TOOLONG, + _
    '                    Ls515Class.Costanti.S_LS515_PAPER_JAM
    '                    ret = ls515.Reset(Ls515Class.Costanti.S_RESET_FREE_PATH)
    '            End Select
    '        End If
    '    Else
    '        ret = Ls515Class.Costanti.S_LS515_USER_ABORT
    '    End If
    '    If ritorno = True Then
    '        FaseOk()
    '    Else
    '        FaseKo()
    '    End If

    '    BVisualZoom = False
    '    FileTraccia.UltimoRitornoFunzione = ret
    '    FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
    '    Return ritorno
    'End Function
    Public Function TestQualitaImmagine(ByVal side As System.Byte, Optional ByVal ScanMode As Short = 0) As Integer
        Dim ritorno As Integer
        Dim retZoom As DialogResult
        Dim ret As Integer
        Dim str As String
        'Dim Iquali As Image
        'Dim ftestSfondo As Boolean
        'Dim stringaApp As String
        'Dim indicesfondo As Integer
        Dim localDate As String
        Dim hour As String

        Fqualit = New CtsControls.FqualImg

        ritorno = False
        fquality = True
        localDate = Now.ToString("yyyyMMdd")
        hour = Now.ToString("hhmmss")

        ' TCollaudo.SelectedTab() = TScanner
        FaseEsec()

        If (SerialNumberDaStampare = Nothing) Then
            SerialNumberDaStampare = "PROGETTO"
        End If

        If (SerialNumberPiastra = Nothing) Then
            SerialNumberPiastra = "PROGETTO_PIASTRA"
        End If

        If (System.Convert.ToInt16(side) = Ls515Class.Costanti.S_SIDE_FRONT_IMAGE) Then
            str = "FRONTE"
            'ls515.NomeFileImmagine = zzz + "\Quality\FrontImage" + IndImgF.ToString("0000") + ".bmp"
            'GScanF.Enabled = True
            ' Iquali = VetImg(7)
        Else
            str = "RETRO"
            'ls515.NomeFileImmagine = zzz + "\Quality\BackImage" + IndImgR.ToString("0000") + ".bmp"
            'GscanR.Enabled = True
            'Iquali = VetImg(9)
        End If
        'DEBUG

        ls515.NomeFileImmagine = zzz + "\Images_Scanner\FrontImage" + SerialNumberDaStampare.ToString + "_" + SerialNumberPiastra.ToString + "_" + IndImgF.ToString("0000") + "_" + localDate.ToString + "_" + hour.ToString & ".bmp"
        ls515.NomeFileImmagineUV = zzz + "\Images_Scanner\FrontImageUV" + SerialNumberDaStampare.ToString + "_" + SerialNumberPiastra.ToString + "_" + IndImgF.ToString("0000") + "_" + localDate.ToString + "_" + hour.ToString & ".bmp"
        ls515.NomeFileImmagineRetro = zzz + "\Images_Scanner\BackImage" + SerialNumberDaStampare.ToString + "_" + SerialNumberPiastra.ToString + "_" + IndImgR.ToString("0000") + "_" + localDate.ToString + "_" + hour.ToString & ".bmp"


        idmsg = 234
        Select Case ScanMode
            Case Ls515Class.Costanti.S_SCAN_MODE_COLOR_100, Ls515Class.Costanti.S_SCAN_MODE_COLOR_200, Ls515Class.Costanti.S_SCAN_MODE_COLOR_AND_RED_100, Ls515Class.Costanti.S_SCAN_MODE_COLOR_AND_RED_200
                'Case Ls515Class.Costanti.S_SCAN_MODE_COLOR_200
                'Case Ls515Class.Costanti.S_SCAN_MODE_COLOR_AND_RED_100
                'Case Ls515Class.Costanti.S_SCAN_MODE_COLOR_AND_RED_200
                'ScanMode = Ls515Class.Costanti.S_SCAN_MODE_COLOR_200
                str += " Colore"

            Case Ls515Class.Costanti.S_SCAN_MODE_256GR100_AND_UV, Ls515Class.Costanti.S_SCAN_MODE_256GR200_AND_UV, Ls515Class.Costanti.S_SCAN_MODE_256GR300_AND_UV
                'Case Ls515Class.Costanti.S_SCAN_MODE_256GR300_AND_UV
                'Case Ls515Class.Costanti.S_SCAN_MODE_256GR300_AND_UV
                'ScanMode = Ls515Class.Costanti.S_SCAN_MODE_COLOR_200
                str += " UV"
                idmsg = 273
            Case Else
                ScanMode = Ls515Class.Costanti.S_SCAN_MODE_256GR200

        End Select



        'MessageBox.Show(ls515.NomeFileImmagine)


        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio + str, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire un documento per verificare l'immagine " + str, "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Question, VetImg(7))
        End If

        If dres = DialogResult.OK Then

            ret = ls515.LeggiImmagini(side, ScanMode)
            If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then
                If (System.Convert.ToInt16(side) = Ls515Class.Costanti.S_SIDE_FRONT_IMAGE) Then
                    Try
                        DisposeImg()
                        If (ScanMode = Ls515Class.Costanti.S_SCAN_MODE_256GR200_AND_UV) Then
                            Pimage.Image = System.Drawing.Image.FromFile(ls515.NomeFileImmagineUV)
                        Else
                            Pimage.Image = System.Drawing.Image.FromFile(ls515.NomeFileImmagine)
                        End If

                        IndImgF += 1
                    Catch ex As Exception
                        MessageBox.Show(ex.ToString())
                    End Try

                    'ftestSfondo = TestSfondo(Pimage.Image, 32, RigheContigueFronte, RigheTotaliFronte, indicesfondo)
                    'stringaApp = "Fronte --> Righe contigue: " + RigheContigueFronte.ToString("000") + " Righe Totali: " + RigheTotaliFronte.ToString("000")
                Else
                    Try
                        DisposeImg()
                        PImageBack.Image = System.Drawing.Image.FromFile(ls515.NomeFileImmagineRetro)
                        IndImgR += 1
                    Catch ex As Exception
                        MessageBox.Show(ex.ToString())
                    End Try

                    'ftestSfondo = TestSfondo(PImageBack.Image, 32, RigheContigueRetro, RigheTotaliRetro, indicesfondo)
                    'stringaApp = "Retro --> Righe contigue: " + RigheContigueRetro.ToString("000") + " Righe Totali: " + RigheTotaliRetro.ToString("000")
                End If
                Application.DoEvents()

                Dim a As Short


                If (System.Convert.ToInt16(side) = Ls515Class.Costanti.S_SIDE_FRONT_IMAGE) Then
                    BVisualZoom = True
                    DirZoom = Application.StartupPath

                    a = DirZoom.LastIndexOf("\")
                    DirZoom = DirZoom.Substring(0, a)
                    DirZoom += "\Images_Scanner\"
                    If (ScanMode = Ls515Class.Costanti.S_SCAN_MODE_256GR200_AND_UV) Then
                        FileZoomCorrente = ls515.NomeFileImmagineUV
                    Else
                        FileZoomCorrente = ls515.NomeFileImmagine
                    End If

                    ' Zoom Immagine 
                    Try
                        retZoom = ShowZoom(DirZoom, FileZoomCorrente)
                    Catch ex As Exception
                        MessageBox.Show(ex.ToString())
                    End Try

                End If

                If (System.Convert.ToInt16(side) = Ls515Class.Costanti.S_SIDE_BACK_IMAGE) Then
                    'If retZoom = DialogResult.OK Then
                    BVisualZoom = True
                    DirZoom = Application.StartupPath

                    a = DirZoom.LastIndexOf("\")
                    DirZoom = DirZoom.Substring(0, a)
                    DirZoom += "\Images_Scanner\"
                    FileZoomCorrente = ls515.NomeFileImmagineRetro
                    ' Zoom Immagine 
                    Try
                        retZoom = ShowZoom(DirZoom, FileZoomCorrente)
                    Catch ex As Exception
                        MessageBox.Show(ex.ToString())
                    End Try

                    'End If
                End If
                'MessageboxMy("La qualità dell' immagine " + str + " è buona?", "ATTENZIONE", MessageBoxButtons.YesNo, MessageBoxIcon.Question, Nothing) = DialogResult.Yes
                If retZoom = DialogResult.OK Then
                    ritorno = True
                Else
                    Fqualit.ShowDialog()

                    Select Case Fqualit.ret
                        Case 0
                            ritorno = False
                        Case 1 '->devo rifare la calibrazione scanner....
                            If (fScannerType = TIPO_SCANNER_UV_515 Or fScannerType = TIPO_SCANNER_UV) Then
                                ' provvisorio per nuovi scanner UV
                                ' prima erano invertiti
                                ritorno = TestScannerUV() ' calbrazione scanner UV 
                                If ritorno = True Then
                                    ritorno = TestScannersPeriferica() 'calibrazione scanner GRIGIO
                                    If ritorno = True Then
                                        ritorno = TestQualitaImmagine(70, Ls515Class.Costanti.S_SCAN_MODE_COLOR_200) ' faccio vedere img front grigio / colore 
                                        If ritorno = True Then
                                            ritorno = TestQualitaImmagine(66, Ls515Class.Costanti.S_SCAN_MODE_COLOR_200) ' faccio vedere img retro grigio / colore 
                                            If (ritorno = True) Then
                                                ritorno = TestQualitaImmagine(70, Ls515Class.Costanti.S_SCAN_MODE_256GR200_AND_UV) ' faccio vedere img UV 
                                            End If
                                        End If

                                    End If
                                End If
                            Else
                                ritorno = TestScannersPeriferica()
                                If ritorno = True Then
                                    ritorno = TestQualitaImmagine(70, Ls515Class.Costanti.S_SCAN_MODE_COLOR_200)
                                    If ritorno = True Then
                                        ritorno = TestQualitaImmagine(66, Ls515Class.Costanti.S_SCAN_MODE_COLOR_200)
                                    End If

                                End If
                            End If

                        Case 2
                            If (fScannerType = TIPO_SCANNER_UV_515) Then
                                ritorno = TestQualitaImmagine(70, Ls515Class.Costanti.S_SCAN_MODE_256GR200_AND_UV)
                            Else
                                ritorno = TestQualitaImmagine(side, Ls515Class.Costanti.S_SCAN_MODE_COLOR_200)
                            End If
                    End Select
                End If
            Else ' Leggi immagini non a buon ine
                ViewError(fase)

                Select Case ret
                    Case Ls515Class.Costanti.S_LS515_JAM_AT_SCANNERPHOTO, + _
                        Ls515Class.Costanti.S_LS515_JAM_AT_MICR_PHOTO, + _
                        Ls515Class.Costanti.S_LS515_JAM_DOC_TOOLONG, + _
                        Ls515Class.Costanti.S_LS515_PAPER_JAM
                        ret = ls515.Reset(Ls515Class.Costanti.S_RESET_FREE_PATH)
                End Select
            End If
        Else
            ret = Ls515Class.Costanti.S_LS515_USER_ABORT
        End If

        'If (ftestSfondo = True) Then
        '    MessageboxMy("Sfondo KO: " + stringaApp, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
        '    retZoom = DialogResult.No
        'Else
        '    MessageboxMy("Sfondo OK: " + stringaApp, "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Information, Nothing)
        'End If


        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If

        BVisualZoom = False
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function


    Public Function TestDimensioniImmagini(ByVal side As Short) As Integer
        Dim ritorno As Integer
        Dim ret As Integer
        Dim stringa As String
        Dim retZoom As Short
        stringa = ""
        ritorno = False

        If side = 0 Then
            idmsg = 264
        Else
            idmsg = 265
        End If

        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire un documento per verificare l'immagine ", "ATTENZIONE", MessageBoxButtons.OK, MessageBoxIcon.Question, VetImg(7))
        End If

        If dres = DialogResult.OK Then
            'Spostato qui se no LsSaveBid dava -18
            DisposeImg()

            ls515.NomeFileImmagine = zzz + "\Quality\FrontImage" + IndImgF.ToString("0000") + ".bmp"

            ls515.NomeFileImmagineRetro = zzz + "\Quality\BackImage" + IndImgR.ToString("0000") + ".bmp"
            ret = ls515.LeggiImmaginiAllinea(Ls515Class.Costanti.S_SIDE_ALL_IMAGE, Ls515Class.Costanti.S_SCAN_MODE_256GR300)

            If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then
                ' DisposeImg()
                Pimage.Image = System.Drawing.Image.FromFile(ls515.NomeFileImmagine)
                IndImgF += 1

                PImageBack.Image = System.Drawing.Image.FromFile(ls515.NomeFileImmagineRetro)
                IndImgR += 1

                If (Math.Abs(Pimage.Image.Height - PImageBack.Image.Height) > 20) Then

                    ritorno = False
                Else

                    BVisualZoom = True
                    DirZoom = Application.StartupPath
                    Dim a As Short
                    a = DirZoom.LastIndexOf("\")
                    DirZoom = DirZoom.Substring(0, a)
                    DirZoom += "\Quality\"

                    If side = 0 Then
                        FileZoomCorrente = ls515.NomeFileImmagine
                    Else
                        FileZoomCorrente = ls515.NomeFileImmagineRetro
                    End If

                    'FileZoomCorrente = ls150.NomeFileImmagine
                    ' Zoom Immagine 
                    retZoom = ShowZoom(DirZoom, FileZoomCorrente)

                    If retZoom = DialogResult.OK Then
                        ritorno = True
                    End If

                    'Dim TempBitmap As New Bitmap(Pimage.Image)

                    'If(Pimage.Image.)
                    'If (Pimage.Image.Height < ALTEZZA_DOC_78 - 20 Or Pimage.Image.Height > ALTEZZA_DOC_78 + 10) Or _
                    '   (PImageBack.Image.Height < ALTEZZA_DOC_78 - 20 Or PImageBack.Image.Height > ALTEZZA_DOC_78 + 10) Or _
                    '   (Pimage.Image.Width < LUNGHEZZA_DOC_78 - 20 Or Pimage.Image.Width > LUNGHEZZA_DOC_78 + 10) Or _
                    '   (PImageBack.Image.Width < LUNGHEZZA_DOC_78 - 20 Or PImageBack.Image.Width > LUNGHEZZA_DOC_78 + 10) Then
                    '    stringa = "Immagine fronte o retro fuori dimensioni"
                    '    ritorno = False
                    'Else
                    '    ritorno = True
                    'End If

                End If
                FileTraccia.Note = "Differenza in pixel : " + Math.Abs(Pimage.Image.Height - PImageBack.Image.Height).ToString()
                MessageboxMy("Differenza in pixel tra fronte e retro  = " + Math.Abs(Pimage.Image.Height - PImageBack.Image.Height).ToString() + " " + stringa, "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Information, Nothing)

            End If

        Else
            ritorno = False
            ret = Ls515Class.Costanti.S_LS515_USER_ABORT

        End If

        Application.DoEvents()



        If ritorno = True Then
            FaseOk()
        Else
            ViewError(fase)
            FaseKo()
        End If

        BVisualZoom = False

        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante


        Return ritorno
    End Function


    'Funzione per test sfogliatore:
    ' usata solamente per collaudo periferica ls515-6 e ls515-7
    ' quando non c'è la testina MICR ed è presente lo sfogliatore
    Public Function TestSfogliatore(ByVal ndoc As Integer) As Integer
        Dim ritorno As Integer
        Dim ret As Short

        ritorno = False
        FaseEsec()
        If fTestinaMicr = True Then
            '    MessageboxMy("Prova già eseguita nella prova di lettura MICR", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            ritorno = True
        Else
            ls515.NomeFileImmagine = "img"
            idmsg = 237
            If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
            Else
                dres = MessageboxMy("Inserire Documenti per prova sfogliatore", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
            End If



            For ii = 1 To ndoc
                'Lab.Text = ii
                If Math.IEEERemainder(ii, 2) = 0 Then
                    ls515.NomeFileImmagine = zzz + "\Dati\SfogliaFImage" + ii.ToString + ".bmp"
                    ret = ls515.LeggiImmagini(Convert.ToSByte(Ls515Class.Costanti.S_SIDE_FRONT_IMAGE), Ls515Class.Costanti.S_SCAN_MODE_256GR200)
                Else
                    ls515.NomeFileImmagine = zzz + "\Dati\SfogliaBImage" + ii.ToString + ".bmp"
                    ret = ls515.LeggiImmagini(Convert.ToSByte(Ls515Class.Costanti.S_SIDE_BACK_IMAGE), Ls515Class.Costanti.S_SCAN_MODE_256GR200)
                End If

                If ret <> Ls515Class.Costanti.S_LS515_OKAY Then
                    If ret = Ls515Class.Costanti.S_LS515_FEEDEREMPTY Then
                        idmsg = 238
                        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
                            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
                        Else
                            dres = MessageboxMy("Inserire Documenti per prova sfogliatore", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
                        End If
                    End If
                    ii -= 1
                Else
                    If Math.IEEERemainder(ii, 2) = 0 Then
                        DisposeImg()
                        PFronte.Image = System.Drawing.Image.FromFile(zzz + "\Dati\SfogliaFImage" + ii.ToString + ".bmp")
                        Pimage.Image = PFronte.Image
                    Else
                        DisposeImg()
                        PRetro.Image = System.Drawing.Image.FromFile(zzz + "\Dati\SfogliaBImage" + ii.ToString + ".bmp")
                        Pimage.Image = PRetro.Image
                    End If

                    Application.DoEvents()
                End If
            Next
        End If

        If (ritorno = True) Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function
    Public Function TestSNPerStampaPiastra() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim fser As New CtsControls.FSerialnum
        ritorno = False
        FaseEsec()
        ls515.ReturnC.FunzioneChiamante = "TestSerialNumber"

        idmsg = 154
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            fser.Text = Messaggio
        Else
            fser.Text = "Inserire il Serial number della periferica da collaudare"
        End If


        If fser.ShowDialog() = DialogResult.OK Then
            If fser.StrSerialNumber.Length = LUNGHEZZA_SERIAL_NUMBER_PIASTRA Then
                SerialNumberDaStampare = fser.StrSerialNumber



                If ret = Ls515Class.Costanti.S_LS515_OKAY Then
                    ritorno = True
                Else
                    ViewError(fase)
                End If
            Else
                ret = Ls515Class.Costanti.S_LS515_STRINGTRUNCATED
                ViewError(fase)
            End If


        Else
            ViewError(fase)
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function



    Public Function TestSNPerStampa() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim fser As New CtsControls.FSerialnum
        ritorno = False
        FaseEsec()
        ls515.ReturnC.FunzioneChiamante = "TestSerialNumber"

        idmsg = 154
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            fser.Text = Messaggio
        Else
            fser.Text = "Inserire il Serial number della periferica da collaudare"
        End If


        If fser.ShowDialog() = DialogResult.OK Then
            If fser.StrSerialNumber.Length = LUNGHEZZA_SERIAL_NUMBER Then
                SerialNumberDaStampare = fser.StrSerialNumber

                If ret = Ls515Class.Costanti.S_LS515_OKAY Then
                    ritorno = True
                Else
                    ViewError(fase)
                End If
            Else
                ret = Ls515Class.Costanti.S_LS515_STRINGTRUNCATED
                ViewError(fase)
            End If


        Else
            ViewError(fase)
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function


    ' Funzione per eseguire il tst della cinghia

    Public Function TestCinghia() As Integer

        Dim ritorno As Integer
        Dim ret As Short

        'TCollaudo.SelectedTab = TMicr
        'TMicr.Enabled = True

        ritorno = False
        FaseEsec()
        ' leggo come ho impostato la macchina







        idmsg = 221
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Test Cinghia: Inserire il Docutest", "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        End If

        If (dres = DialogResult.OK) Then
            ret = ls515.TestCinghia()

            If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then
                ritorno = True
            Else
                MessageboxMy("Max: " + ls515.ValMaxCinghia.ToString + " Min: " + ls515.ValMinCinghia.ToString, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
                StrCommento += "Reply :" + ret.ToString + " "
            End If

            FileTraccia.Note = "Valore minimo :" + ls515.ValMinCinghia.ToString + vbCrLf
            FileTraccia.Note += "Valore massimo :" + ls515.ValMaxCinghia.ToString
        Else
            StrCommento += "Abortito Da Utente "
            ret = Ls515Class.Costanti.S_LS515_USER_ABORT
        End If
        If ritorno = True Then

            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestSetVelocita(ByVal Velo As Short) As Integer

        Dim ritorno As Integer
        Dim ret As Short

        'TCollaudo.SelectedTab = TMicr
        'TMicr.Enabled = True
        ritorno = False
        FaseEsec()

        ret = ls515.SetVelocita(Velo)

        If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then
            ritorno = True
        Else
            StrCommento += "Reply :" + ret.ToString + " "
        End If


        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    ' Funzione che salva i valori dei foto e quelli del CIS

    Public Function TestFotosensori() As Integer
        Dim Reply As Integer
        Dim ritorno As Integer

        Dim write As System.IO.StreamWriter
        Dim stringa As String
        Dim ind As Integer

        Dim pathFile As String


        ritorno = True

        stringa = Application.StartupPath
        ind = stringa.LastIndexOf("\")
        stringa = stringa.Substring(0, ind)

        If IO.Directory.Exists(stringa + "trace") Then
            ' IO.Directory.Delete(zzz + "\Dati", True)
        Else
            IO.Directory.CreateDirectory("trace")
        End If

        pathFile = "\trace\fotosensori.cts"
        stringa = ""

        ' leggo i valori dei Cis
        Reply = ls515.LeggiValoriCIS()

        If (Reply = Ls515Class.Costanti.S_LS515_OKAY) Then

            If (IO.File.Exists(pathFile) = False) Then
                ' la prima volta scrivo anche l'intestazione del file 
                write = New System.IO.StreamWriter(pathFile, True)
                stringa = "Data Ora Foto1 Foto2 Foto3 Doppia1 Doppia2 Doppia3 Soglia1 Soglia2 Soglia3 Soglia4 Soglia5 Soglia6 "
                stringa += "Add_PWM_Blue_F Add_PWM_Blue_R Add_PWM_Green_F Add_PWM_Green_R Add_PWM_Red_F Add_PWM_Red_R"
                stringa += "Dato_Gain_F Dato_Gain_R Dato_Offset_F Dato_Offset_R"
                stringa += "Dato_PWM_Blue_F Dato_PWM_Blue_R Dato_PWM_Green_F Dato_PWM_Green_R Dato_PWM_Red_F Dato_PWM_Red_R"
                write.WriteLine(stringa)
            Else

                write = New System.IO.StreamWriter(pathFile, True)
            End If

            stringa = System.DateTime.Now.ToString
            stringa += " " + Ls515Class.Periferica.PhotoValue(0).ToString
            stringa += " " + Ls515Class.Periferica.PhotoValue(1).ToString
            stringa += " " + Ls515Class.Periferica.PhotoValue(2).ToString

            stringa += " " + Ls515Class.Periferica.PhotoValueDP(0).ToString
            stringa += " " + Ls515Class.Periferica.PhotoValueDP(1).ToString
            stringa += " " + Ls515Class.Periferica.PhotoValueDP(2).ToString
            ' soglie
            stringa += " " + Ls515Class.Periferica.PhotoValueDP(3).ToString
            stringa += " " + Ls515Class.Periferica.PhotoValueDP(4).ToString
            stringa += " " + Ls515Class.Periferica.PhotoValueDP(5).ToString
            stringa += " " + Ls515Class.Periferica.PhotoValueDP(6).ToString
            stringa += " " + Ls515Class.Periferica.PhotoValueDP(7).ToString
            stringa += " " + Ls515Class.Periferica.PhotoValueDP(8).ToString

            stringa += " " + ls515.strCis.Add_PWM_Blue_F.ToString
            stringa += " " + ls515.strCis.Add_PWM_Blue_R.ToString
            stringa += " " + ls515.strCis.Add_PWM_Green_F.ToString
            stringa += " " + ls515.strCis.Add_PWM_Green_R.ToString
            stringa += " " + ls515.strCis.Add_PWM_Red_F.ToString
            stringa += " " + ls515.strCis.Add_PWM_Red_R.ToString

            stringa += " " + ls515.strCis.Dato_Gain_F.ToString
            stringa += " " + ls515.strCis.Dato_Gain_R.ToString

            stringa += " " + ls515.strCis.Dato_Offset_F.ToString
            stringa += " " + ls515.strCis.Dato_Offset_R.ToString

            stringa += " " + ls515.strCis.Dato_PWM_Blue_F.ToString
            stringa += " " + ls515.strCis.Dato_PWM_Blue_R.ToString
            stringa += " " + ls515.strCis.Dato_PWM_Green_F.ToString
            stringa += " " + ls515.strCis.Dato_PWM_Green_R.ToString
            stringa += " " + ls515.strCis.Dato_PWM_Red_F.ToString
            stringa += " " + ls515.strCis.Dato_PWM_Red_R.ToString
            ' scrivo sul file di trace
            write.WriteLine(stringa)
            ' chiudo il file
            write.Close()
        Else
            ' la funzione non è andata a buon fine
            ritorno = False
        End If
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function

    Public Function TestScannerUV() As Integer
        Dim ritorno As Integer
        Dim ret As Short

        ritorno = False
        'TCollaudo.SelectedTab() = TScanner
        'GscanR.Enabled = True
        'BvisualF.Text = "Controllo immagine Fronte e Retro NP2"
        'BCalF.Text = "Calibra Scanner Fronte e Retro NP1"

        FaseEsec()

        idmsg = 263
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire il DOCUTEST --069 per la calibrazione Scanner UV. !", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(10))
        End If


        ' faccio la calibrazione scanner fronte
        If (dres <> DialogResult.OK) Then
            ret = Ls515Class.Costanti.S_LS515_USER_ABORT
            FaseKo()
            fWaitButton = True
        Else

            fquality = False
            ret = ls515.CalibraScanner(Ls515Class.Costanti.SCANNER_UV)
            If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then
                'If (ordine.ScannerFronte = 1) Then
                '    FileZoomCorrente = ls150.NomeFileImmagine
                '    ret = LeggiImmagine(System.Convert.ToSByte(Ls150Class.Costanti.S_SIDE_FRONT_IMAGE))
                'End If
                'If ret = Ls150Class.Costanti.S_LS150_OKAY Then
                '    If (ordine.ScannerRetro = 1) Then
                '        FileZoomCorrente = ls150.NomeFileImmagine
                '        ret = LeggiImmagine(System.Convert.ToSByte(Ls150Class.Costanti.S_SIDE_BACK_IMAGE))
                '    End If
                'End If

            Else
                ' errore calibrazione fronte
                'BCalF.Enabled = True
                'BvisualF.Enabled = True
                'BAnnulla.Enabled = True
                ViewError(fase)
                'ViewError(fase)
            End If
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        If ret = Ls515Class.Costanti.S_LS515_OKAY Then
            ritorno = True
        Else
            ritorno = False
        End If

        If ritorno = True Then

            FaseOk()
            FileTraccia.ScannerF = 1
        End If
        BVisualZoom = False
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function


    Public Function CalibrazioneScannerUV() As Integer
        Dim ritorno As Integer
        Dim ret As Short

        ritorno = False
        'TCollaudo.SelectedTab() = TScanner
        'GscanR.Enabled = True
        'BvisualF.Text = "Controllo immagine Fronte e Retro NP2"
        'BCalF.Text = "Calibra Scanner Fronte e Retro NP1"

        FaseEsec()

        idmsg = 263
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire il DOCUTEST --069 per la calibrazione Scanner UV. !", "Attenzione!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, VetImg(10))
        End If


        ' faccio la calibrazione scanner fronte
        If (dres <> DialogResult.OK) Then
            ret = Ls515Class.Costanti.S_LS515_USER_ABORT
            FaseKo()
            fWaitButton = True
        Else

            fquality = False
            ret = ls515.CalibraScanner(Ls515Class.Costanti.SCANNER_UV)
            If (ret = Ls515Class.Costanti.S_LS515_OKAY) Then
                'If (ordine.ScannerFronte = 1) Then
                '    FileZoomCorrente = ls150.NomeFileImmagine
                '    ret = LeggiImmagine(System.Convert.ToSByte(Ls150Class.Costanti.S_SIDE_FRONT_IMAGE))
                'End If
                'If ret = Ls150Class.Costanti.S_LS150_OKAY Then
                '    If (ordine.ScannerRetro = 1) Then
                '        FileZoomCorrente = ls150.NomeFileImmagine
                '        ret = LeggiImmagine(System.Convert.ToSByte(Ls150Class.Costanti.S_SIDE_BACK_IMAGE))
                '    End If
                'End If

            Else
                ' errore calibrazione fronte
                'BCalF.Enabled = True
                'BvisualF.Enabled = True
                'BAnnulla.Enabled = True
                ViewError(fase)
                'ViewError(fase)
            End If
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        If ret = Ls515Class.Costanti.S_LS515_OKAY Then
            ritorno = True
        Else
            ritorno = False
        End If

        If ritorno = True Then

            FaseOk()
            FileTraccia.ScannerF = 1
        End If
        BVisualZoom = False
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function


    Public Function TestHubUsb() As Integer
        Dim ret As Integer
        Dim ritorno As Integer

        Dim STR() As String = IO.Directory.GetLogicalDrives()


        Dim fileArray() As String
        Dim fFound As Boolean
        Dim riga As String = ""

        fFound = False
        ret = False
        ritorno = False
        FaseEsec()

        idmsg = 240
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            dres = MessageboxMy(Messaggio, Titolo, MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing, IdMessaggio, immamsg)
        Else
            dres = MessageboxMy("Inserire la chiavetta USB di test nella porta Hub di LS515", "Attenzione !!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        End If


        For Each s As String In STR
            If (s <> "A:\" And s <> "B:\") Then
                Try
                    fileArray = System.IO.Directory.GetFiles(s)
                    'Dim drives As [String]() = System.Environment.GetLogicalDrives()
                    Dim drives As [String]() = diskInfo.GetInformations(s)
2:                  If drives(0) = NOME_CHIAVETTA Then

                        For Each ss As String In fileArray
                            If ss = s + NOME_FILE_CHIAVETTA Then
                                Dim fstr As System.IO.StreamReader
                                fstr = New System.IO.StreamReader(ss)
                                riga = fstr.ReadLine()
                                If riga = NOME_STRINGA_CHIAVETTA Then
                                    fFound = True
                                End If
                            End If
                        Next
                    End If

                Catch ex As Exception

                End Try
            End If
        Next
        If fFound = True Then
            ret = True
            ritorno = True
            MessageboxMy(riga, "Attenzione", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, Nothing)
        Else
        End If
        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.UltimoRitornoFunzione = ret
        Return ret
    End Function

    Public Function CheckRegistry() As Boolean
        Dim ret As Boolean
        ' verifico che nel registro siano disabilitate le funzioni 
        ' per l'auto play'
        ' se non c'è il valore corretto l' imposto e faccio shut down del PC

        Dim regKey As Microsoft.Win32.RegistryKey
        Dim ver As Decimal
        regKey = Microsoft.Win32.Registry.Users.OpenSubKey(".DEFAULT\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", True)
        'regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(".DEFAULT\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\Explorer", True)
        'regKey.SetValue("AppName", "MyRegApp")
        ver = regKey.GetValue("NoDriveTypeAutoRun", 0.0)
        If ver <> 181 Then
            regKey.SetValue("NoDriveTypeAutoRun", 181)
            MessageboxMy("Trovato sistema non ottimizzato.....", "Riavvio di Windows...", MessageBoxButtons.OK, MessageBoxIcon.Error, Nothing)
            Shell("shutdown.exe -r")
            'Me.Close()
        End If
        regKey.Close()
        Return ret
    End Function


    Public Function TestSerialNumberPerPiastra() As Integer
        Dim ritorno As Integer
        Dim ret As Short

        ritorno = False
        FaseEsec()

        ls515.ReturnC.FunzioneChiamante = "TestSerialNumber"


        Dim datestr As String
        datestr = Date.Now().Day.ToString("00") + Date.Now().Month.ToString("00") + Date.Now().Year.ToString("0000") + "OK"


        MatricolaPeriferica = datestr
        ls515.SSerialNumber = MatricolaPeriferica
        FileTraccia.Matricola = MatricolaPeriferica
        ret = ls515.SerialNumber()
        If ret = Ls515Class.Costanti.S_LS515_OKAY Then
            FileTraccia.Matricola = MatricolaPeriferica
            ritorno = True
        Else
            ViewError(fase)
        End If







        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante
        Return ritorno
    End Function
    Public Function TestSerialNumberPiastra() As Integer
        Dim ritorno As Integer
        Dim ret As Short
        Dim fser As New CtsControls.FSerialnum
        ritorno = False
        FaseEsec()

        idmsg = 239
        If (CercaMessaggio(PathMessaggi, idmsg, IdMessaggio, Descrizione, Titolo, Messaggio, immamsg) = True) Then
            fser.Text = Messaggio
        Else
            fser.Text = "Inserire SerialNumber Piastra"
        End If



        If fser.ShowDialog() = DialogResult.OK Then
            If fser.StrSerialNumber.Length = LUNGHEZZA_SERIAL_NUMBER_PIASTRA Then
                SerialNumberPiastra = fser.StrSerialNumber
                FileTraccia.VersioneDll = SerialNumberPiastra
                ritorno = True
            Else
                ViewError(fase)
            End If
        Else
            ViewError(fase)
        End If

        If ritorno = True Then
            FaseOk()
        Else
            FaseKo()
        End If
        FileTraccia.StrRet = "Test Serial number Piastra"
        FileTraccia.UltimoRitornoFunzione = ret
        Return ritorno
    End Function



    Public Function AddRigaTabella(ByVal Tab As DataTable, ByVal s1 As String, ByVal s2 As String, ByVal s3 As String, ByVal s4 As String) As DataRow
        Dim row As DataRow
        row = Tab.NewRow()
        row.Item(0) = s1
        row.Item(1) = s2
        row.Item(2) = s3
        row.Item(3) = s4

        Tab.Rows.Add(row)
        Return row
    End Function

    Public Function AddRigaTabella2(ByVal Tab As DataTable, ByVal s1 As String, ByVal s2 As String) As DataRow
        Dim row As DataRow
        row = Tab.NewRow()
        row.Item(0) = s1
        row.Item(1) = s2

        Tab.Rows.Add(row)
        Return row
    End Function

    Public Function AddRigaTabella1(ByVal Tab As DataTable, ByVal s1 As String) As DataRow
        Dim row As DataRow
        row = Tab.NewRow()
        row.Item(0) = s1

        Tab.Rows.Add(row)
        Return row
    End Function

    'Public Function StampaReportOLD()

    '    Dim strw As String

    '    FaseEsec()

    '    ls515.ReturnC.FunzioneChiamante = "Stampa Report"


    '    Dim column As DataColumn
    '    Dim row As DataRow
    '    Dim T4col As DataTable = New DataTable("prima")
    '    Dim T2col As DataTable = New DataTable("Seconda")
    '    Dim T3col As DataTable = New DataTable("Terza")
    '    Dim Tcol As DataTable = New DataTable("Quarta")
    '    ' Create new DataColumn, set DataType, ColumnName 
    '    ' and add to DataTable.    
    '    column = New DataColumn()
    '    column.DataType = System.Type.GetType("System.String")
    '    column.ColumnName = "DESCRIPTION"
    '    column.ReadOnly = True
    '    column.Unique = False
    '    ' Add the Column to the DataColumnCollection.
    '    T4col.Columns.Add(column)

    '    ' Create second column.
    '    column = New DataColumn()
    '    column.DataType = System.Type.GetType("System.String")
    '    column.ColumnName = "VALUE"
    '    column.ReadOnly = False
    '    column.Unique = False
    '    ' Add the column to the table.
    '    T4col.Columns.Add(column)

    '    column = New DataColumn()
    '    column.DataType = System.Type.GetType("System.String")
    '    column.ColumnName = "DESCRIPTION "
    '    column.ReadOnly = True
    '    column.Unique = False
    '    ' Add the Column to the DataColumnCollection.
    '    T4col.Columns.Add(column)

    '    column = New DataColumn()
    '    column.DataType = System.Type.GetType("System.String")
    '    column.ColumnName = "VALUE "
    '    column.ReadOnly = True
    '    column.Unique = False
    '    ' Add the Column to the DataColumnCollection.
    '    T4col.Columns.Add(column)

    '    column = New DataColumn()
    '    column.DataType = System.Type.GetType("System.String")
    '    column.ColumnName = "COMPLETED AUTOMATED TESTS"
    '    column.ReadOnly = True
    '    column.Unique = False
    '    ' Add the Column to the DataColumnCollection.
    '    T2col.Columns.Add(column)

    '    column = New DataColumn()
    '    column.DataType = System.Type.GetType("System.String")
    '    column.ColumnName = "RESULT"
    '    column.ReadOnly = True
    '    column.Unique = False
    '    ' Add the Column to the DataColumnCollection.
    '    T2col.Columns.Add(column)

    '    column = New DataColumn()
    '    column.DataType = System.Type.GetType("System.String")
    '    column.ColumnName = "DESCRIPTION"
    '    column.ReadOnly = True
    '    column.Unique = False
    '    ' Add the Column to the DataColumnCollection.
    '    T3col.Columns.Add(column)

    '    column = New DataColumn()
    '    column.DataType = System.Type.GetType("System.String")
    '    column.ColumnName = "CHECKED"
    '    column.ReadOnly = True
    '    column.Unique = False
    '    ' Add the Column to the DataColumnCollection.
    '    T3col.Columns.Add(column)

    '    column = New DataColumn()
    '    column.DataType = System.Type.GetType("System.String")
    '    column.ColumnName = "DETAIL RESULTS"
    '    column.ReadOnly = True
    '    column.Unique = False
    '    ' Add the Column to the DataColumnCollection.
    '    Tcol.Columns.Add(column)




    '    report.DocumentUnit = GraphicsUnit.Millimeter
    '    'report.SetPageOrientation(1, PageOrientation.Portrait)

    '    report.PageHeaderMaxHeight = 0

    '    report.DefaultPageSettings.Margins.Top = 10
    '    report.DefaultPageSettings.Margins.Bottom = 10
    '    report.DefaultPageSettings.Margins.Left = 10
    '    report.DefaultPageSettings.Margins.Right = 10

    '    TextStyle.ResetStyles()


    '    TextStyle.Heading1.FontFamily = New FontFamily("Times New Roman")
    '    TextStyle.Heading1.Brush = Brushes.Black
    '    TextStyle.Heading1.SizeDelta = 8.0F
    '    TextStyle.Heading1.StringAlignment = StringAlignment.Center

    '    TextStyle.Normal.FontFamily = New FontFamily("Courier New")
    '    TextStyle.Normal.Size = 10

    '    TextStyle.Heading2.Size = 12
    '    TextStyle.Heading1.Size = 12
    '    TextStyle.Heading1.Bold = True

    '    TextStyle.Normal.Bold = False


    '    '// create a builder to help with putting the table together.
    '    Dim builder As ReportBuilder = New ReportBuilder(report)




    '    '/ Add a simple page header and footer that is the same on all pages.
    '    'builder.AddPageHeader("CTS Electronics S.P.A.", "Product Report", "Page %p/1")
    '    builder.AddPageHeader("Arca Technologies S.r.l", "Product Report", "Page %p/1")

    '    builder.AddPageFooter(String.Empty, DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToShortTimeString, String.Empty)




    '    If (FileTraccia.Periferica = Nothing) Then
    '        row = AddRigaTabella(T4col, "Peripheral Name", "LS515 Test Stampa", "Product Line", "FileTraccia.LineaProd")
    '        row = AddRigaTabella(T4col, "S/N #", "Matricola", "Order Number", "FileTraccia.Ordine_prod")
    '        row = AddRigaTabella(T4col, "Test SW Ver.", "VersioneSW", "Date", "FileTraccia.DataCollaudo")
    '        row = AddRigaTabella(T4col, "Manufacter", "Manufacter", "Model", "FileTraccia.Periferica")
    '        row = AddRigaTabella(T4col, "Firmware Versione", "VersioneFw", "Tested By", "FileTraccia.Operatore")
    '        row = AddRigaTabella(T4col, "Test Duration :", "Durata_Test.Minutes" + ":" + "Durata_Test.Seconds", "", "")
    '    Else
    '        row = AddRigaTabella(T4col, "Peripheral Name", FileTraccia.Periferica, "Product Line", FileTraccia.LineaProd)
    '        row = AddRigaTabella(T4col, "S/N #", FileTraccia.Matricola, "Order Number", FileTraccia.Ordine_prod)
    '        row = AddRigaTabella(T4col, "Test SW Ver.", FileTraccia.VersioneSW, "Date", FileTraccia.DataCollaudo)
    '        row = AddRigaTabella(T4col, "Manufacter", FileTraccia.Manufacter, "Model", FileTraccia.Periferica)
    '        row = AddRigaTabella(T4col, "Firmware Versione", FileTraccia.VersioneFw, "Tested By", FileTraccia.Operatore)
    '        row = AddRigaTabella(T4col, "Test Duration :", FileTraccia.Durata_Test.Minutes.ToString + ":" + FileTraccia.Durata_Test.Seconds.ToString, "", "")
    '    End If




    '    ' Don't use the full height, instead, use just the height required.

    '    builder.StartLinearLayout(Direction.Vertical)
    '    'builder.AddSection(New SectionLine(Direction.Horizontal))



    '    builder.StartLinearLayout(Direction.Horizontal)
    '    builder.CurrentContainer.UseFullHeight = False

    '    Dim dw As DataView = New DataView(T4col)
    '    builder.AddTable(dw, False)
    '    builder.AddAllColumns(515.0F, True, True)
    '    builder.CurrentSection.MarginRight = MARGINE_RIGHT
    '    builder.CurrentSection.MarginLeft = MARGINE_LEFT
    '    builder.CurrentSection.MarginTop = MARGINE_TOP
    '    builder.CurrentSection.MarginBottom = MARGINE_BOTTOM
    '    builder.CurrentSection.VerticalAlignment = VerticalAlignment.Middle


    '    builder.AddText(vbCrLf + vbCrLf + "Inspected by _____________" + vbCrLf + "Audited   by _____________", TextStyle.Heading2)

    '    builder.FinishLinearLayout()

    '    builder.StartLinearLayout(Direction.Horizontal)
    '    '    builder.AddSection(New SectionLine(Direction.Horizontal))




    '    row = AddRigaTabella2(T2col, "SENSOR TEST", "PASSED")
    '    If (fScannerFronte) Then
    '        strw = "Calibration Front Scanner"
    '        row = AddRigaTabella2(T2col, strw, "PASSED")
    '        strw = "Quality Front Image"
    '        row = AddRigaTabella2(T2col, strw, "PASSED")
    '    End If
    '    If (fScannerRetro) Then
    '        strw = "Calibration Rear Scanner"
    '        row = AddRigaTabella2(T2col, strw, "PASSED")
    '        strw = "Quality Rear Image"
    '        row = AddRigaTabella2(T2col, strw, "PASSED")
    '    End If
    '    strw = "Double Leafing Sensor Calibration"
    '    row = AddRigaTabella2(T2col, strw, "PASSED")
    '    strw = "Double Leafing Sensor Test"
    '    row = AddRigaTabella2(T2col, strw, "PASSED")

    '    If (fTestinaMicr) Then
    '        strw = "MICR Head Calibration"
    '        row = AddRigaTabella2(T2col, strw, "PASSED")
    '        strw = "MICR Speed Test"
    '        row = AddRigaTabella2(T2col, strw, "PASSED")
    '        strw = "MICR Reader Test"
    '        row = AddRigaTabella2(T2col, strw, "PASSED")
    '    End If

    '    If (fTimbri = True) Then
    '        strw = "Stamp Test"
    '        row = AddRigaTabella2(T2col, strw, "PASSED")
    '    End If

    '    If (fStampante) Then
    '        strw = "Ink Jet Printer Test"
    '        row = AddRigaTabella2(T2col, strw, "PASSED")
    '    End If


    '    If (fBadge <> 0) Then
    '        strw = "Badge Reader Test"
    '        row = AddRigaTabella2(T2col, strw, "PASSED")
    '    End If




    '    Dim dw2 As DataView = New DataView(T2col)
    '    builder.AddTable(dw2, False)
    '    builder.AddAllColumns(200.0F, True, True)
    '    'builder.CurrentSection.HorizontalAlignment = ReportPrinting.HorizontalAlignment.Center
    '    builder.CurrentSection.MarginRight = MARGINE_RIGHT
    '    builder.CurrentSection.MarginLeft = MARGINE_LEFT
    '    builder.CurrentSection.MarginTop = MARGINE_TOP
    '    builder.CurrentSection.MarginBottom = MARGINE_BOTTOM
    '    builder.CurrentSection.VerticalAlignment = VerticalAlignment.Top



    '    builder.CurrentContainer.UseFullHeight = False



    '    row = AddRigaTabella2(T3col, "Visual Inspection", "            ")
    '    row = AddRigaTabella2(T3col, "Labels Check", "            ")
    '    row = AddRigaTabella2(T3col, "Serial Number & Corrisponding Label", "            ")
    '    row = AddRigaTabella2(T3col, "Inkjet cartidge Removed Check", "            ")
    '    row = AddRigaTabella2(T3col, "Inkjet cartidge Present in the box", "            ")
    '    row = AddRigaTabella2(T3col, "Unit Clean", "            ")
    '    row = AddRigaTabella2(T3col, "USB Cable Present", "            ")
    '    row = AddRigaTabella2(T3col, "Power supply Present", "            ")
    '    row = AddRigaTabella2(T3col, "Manuals presente in the box", "            ")


    '    Dim dw3 As DataView = New DataView(T3col)
    '    builder.AddTable(dw3, False)
    '    builder.AddAllColumns(255.0F, True, True)
    '    builder.CurrentSection.MarginRight = MARGINE_RIGHT
    '    builder.CurrentSection.MarginLeft = MARGINE_LEFT
    '    builder.CurrentSection.MarginTop = MARGINE_TOP
    '    builder.CurrentSection.MarginBottom = MARGINE_BOTTOM
    '    builder.CurrentSection.VerticalAlignment = VerticalAlignment.Top



    '    builder.FinishLinearLayout()


    '    builder.StartLinearLayout(Direction.Horizontal)





    '    '' Don't use the full height, instead, use just the height required.


    '    strw = "DOCUTEST 061  read --> 14 Errors --> 0" + "   " + "DOCUTEST 062  read --> 14 Errors --> 0"
    '    row = AddRigaTabella1(Tcol, strw)

    '    strw = "DOCUTEST 063  read --> 14 Errors --> 0" + "   " + "DOCUTEST 064  read --> 14 Errors --> 0"
    '    row = AddRigaTabella1(Tcol, strw)

    '    strw = "DOCUTEST 065  read --> 14 Errors --> 0" + "   " + "DOCUTEST 066  read --> 14 Errors --> 0"
    '    row = AddRigaTabella1(Tcol, strw)

    '    strw = "DOCUTEST 048  read --> 14 Errors --> 0" + "   " + "DOCUTEST 046  read --> 14 Errors --> 0"
    '    row = AddRigaTabella1(Tcol, strw)


    '    Dim dw4 As DataView = New DataView(Tcol)
    '    builder.AddTable(dw4, False)
    '    builder.AddAllColumns(800.0F, True, True)
    '    builder.CurrentSection.MarginRight = MARGINE_RIGHT
    '    builder.CurrentSection.MarginLeft = MARGINE_LEFT
    '    builder.CurrentSection.MarginTop = MARGINE_TOP
    '    builder.CurrentSection.MarginBottom = MARGINE_BOTTOM
    '    builder.CurrentSection.VerticalAlignment = VerticalAlignment.Top


    '    builder.FinishLinearLayout()

    '    builder.FinishLinearLayout()

    '    report.Print()
    '    FaseOk()
    '    Return True
    'End Function

    Function StampaReport()

        Dim nomeFileLog As String = Application.StartupPath & "\Report515\" & FileTraccia.Matricola & "_" & DateTime.Now.ToString("yyyyMMdd_hhmmss") & ".txt"
        Dim headerLine As String = ""
        Dim recordLine As String = ""
        Dim larghezza As Integer = 62

        FaseEsec()

        ls515.ReturnC.FunzioneChiamante = "Stampa Report"
        FileTraccia.UltimoRitornoFunzione = 0
        FileTraccia.StrRet = ls515.ReturnC.FunzioneChiamante

        recordLine = "┌────────────────────────── ARCA ─────────────────────────────┐" & vbNewLine & _
                    ("│ Peripheral Name:                   " & FileTraccia.Periferica).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Product Line:                      " & FileTraccia.LineaProd).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ S/N #:                             " & FileTraccia.Matricola).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Product Code:                      " & FileTraccia.Ordine_prod).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Date :                             " & FileTraccia.DataCollaudo).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Manufacter :                       " & FileTraccia.Manufacter).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Model :                            " & FileTraccia.Periferica).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Firmware Version :                 " & FileTraccia.VersioneFw).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Tested By :                        " & FileTraccia.Operatore & "-" & FileTraccia.NomeStazione).PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Test Duration :                    " & FileTraccia.Durata_Test.Minutes.ToString + ":" + FileTraccia.Durata_Test.Seconds.ToString).PadRight(larghezza) & "│" & vbNewLine & _
                     "│                                                             │" & vbNewLine & _
                     "├────────────────── COMPLETED AUTOMATED TESTS ────────────────┤" & vbNewLine & _
                     "│                                                             │" & vbNewLine & _
                    ("│ Sensor Test:                             " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Calibration Front Scanner:               " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Quality Front Image:                     " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Calibration Rear Scanner:                " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Quality Rear Image:                      " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Double Leafing Sensor Calibration:       " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ Double Leafing Sensor Test:              " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ MICR Head Calibration:                   " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ MICR Speed Test:                         " & "PASSED").PadRight(larghezza) & "│" & vbNewLine & _
                    ("│ MICR Reader Test:                        " & "PASSED").PadRight(larghezza) & "│" & vbNewLine


        If (fTimbri = True) Then
            recordLine += ("│ Stamp Test:                              " & "PASSED").PadRight(larghezza) & "│" & vbNewLine
        End If

        If (fStampante = True) Then
            recordLine += ("│ Ink Jet Printer Test:                    " & "PASSED").PadRight(larghezza) & "│" & vbNewLine
        End If

        If (fBadge <> 0) Then
            recordLine += ("│ Badge Reader Test:                       " & "PASSED").PadRight(larghezza) & "│" & vbNewLine
        End If

        recordLine += "│                                                             │" & vbNewLine
        recordLine += "├─────────────────────────────────────────────────────────────┤" & vbNewLine
        recordLine += "│                                                             │" & vbNewLine

        recordLine += ("│ Visual Inspection:                       " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ Label Check:                             " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ Serial Number & Corrisponding Label:     " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ Inkjet cartidge Removed Check:           " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ Inkjet cartidge Present in the box:      " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ Unit Clean:                              " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ USB Cable Present:                       " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ Power supply Present:                    " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine & _
                         ("│ Manuals present  in the box:             " & "CHECKED").PadRight(larghezza) & "│" & vbNewLine

        recordLine += "│                                                             │" & vbNewLine
        recordLine += "│────────────────── DETAIL RESULTS ───────────────────────────┤" & vbNewLine
        recordLine += "│                                                             │" & vbNewLine

        recordLine += ("│ DOCUTEST 061  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine & _
              ("│ DOCUTEST 062  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine & _
              ("│ DOCUTEST 063  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine & _
              ("│ DOCUTEST 064  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine & _
              ("│ DOCUTEST 065  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine & _
              ("│ DOCUTEST 066  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine & _
              ("│ DOCUTEST 048  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine & _
              ("│ DOCUTEST 046  :read --> 14 Errors --> 0").PadRight(larghezza) & "│" & vbNewLine


    

    

 


        recordLine += "└─────────────────────────────────────────────────────────────┘"

        Dim filelog As New StreamWriter(nomeFileLog)
        filelog.WriteLine(recordLine)
        filelog.Close()

        FaseOk()
        Return True

    End Function


#End Region
    Public Function CercaMessaggio(ByVal filename As String, ByRef Id As Integer, ByRef a As String, ByRef b As String, ByRef c As String, ByRef d As String, ByRef img As Image) As Boolean

        Dim freturn As Boolean
        Dim prova As System.Xml.XmlDocument = New System.Xml.XmlDocument()

        freturn = False
        prova.Load(filename)
        Dim xx As XmlNodeList = prova.GetElementsByTagName("T_Messaggi")

        For Each n As XmlNode In xx

            Dim lll As XmlNodeList = n.ChildNodes
            Dim noddo As XmlNode = n.ChildNodes(0)
            If (noddo.InnerText = Id.ToString()) Then
                a = (n.ChildNodes(0).InnerText)
                b = (n.ChildNodes(1).InnerText)
                c = (n.ChildNodes(2).InnerText)

                d = (n.ChildNodes(3).InnerText)

                If (Not (n.ChildNodes(4) Is Nothing)) Then

                    Dim ms As System.IO.MemoryStream
                    ms = New System.IO.MemoryStream()
                    Dim vet(50 * 1024) As Byte


                    vet = System.Convert.FromBase64CharArray(n.ChildNodes(4).InnerText.ToCharArray(), 0, n.ChildNodes(4).InnerText.Length)

                    ms.Write(vet, 0, vet.Length)
                    img = Image.FromStream(ms)
                End If
                freturn = True
            End If
        Next

        Return freturn
    End Function

    Public Function SalvaDati() As Integer
        Dim ritorno As Integer
        ritorno = 0
        DatiFine.lista.Clear()

        DatiFine.AddDato(DatiFineCollaudo.TipoDato.FOTO1, BitConverter.GetBytes(Ls515Class.Periferica.PhotoValue(0)))
        DatiFine.AddDato(DatiFineCollaudo.TipoDato.FOTO2, BitConverter.GetBytes(Ls515Class.Periferica.PhotoValue(1)))



        DatiFine.AddDato(DatiFineCollaudo.TipoDato.DOPPIA1, BitConverter.GetBytes(Ls515Class.Periferica.PhotoValue(2)))
        DatiFine.AddDato(DatiFineCollaudo.TipoDato.DOPPIA2, BitConverter.GetBytes(Ls515Class.Periferica.PhotoValue(3)))
        DatiFine.AddDato(DatiFineCollaudo.TipoDato.DOPPIA3, BitConverter.GetBytes(Ls515Class.Periferica.PhotoValue(4)))

        DatiFine.AddDato(DatiFineCollaudo.TipoDato.DP_TH1, BitConverter.GetBytes(Ls515Class.Periferica.PhotoValue(5)))
        DatiFine.AddDato(DatiFineCollaudo.TipoDato.DP_TH2, BitConverter.GetBytes(Ls515Class.Periferica.PhotoValueDP(4)))

        DatiFine.AddDato(DatiFineCollaudo.TipoDato.CINGHIA_MIN, BitConverter.GetBytes(ls515.ValMinCinghia))
        DatiFine.AddDato(DatiFineCollaudo.TipoDato.CINGHIA_MAX, BitConverter.GetBytes(ls515.ValMaxCinghia))

        DatiFine.AddDato(DatiFineCollaudo.TipoDato.TRIMMER200, BitConverter.GetBytes(ls515.TrimmerValue200))
        DatiFine.AddDato(DatiFineCollaudo.TipoDato.TRIMMER300, BitConverter.GetBytes(ls515.TrimmerValue300))
        DatiFine.AddDato(DatiFineCollaudo.TipoDato.TRIMMER75, BitConverter.GetBytes(ls515.TrimmerValue75))


        DatiFine.AddDato(DatiFineCollaudo.TipoDato.TEMPOCAMP200, BitConverter.GetBytes(ls515.TempoCamp200))
        DatiFine.AddDato(DatiFineCollaudo.TipoDato.TEMPOCAMP300, BitConverter.GetBytes(ls515.TempoCamp300))
        DatiFine.AddDato(DatiFineCollaudo.TipoDato.TEMPOCAMP75, BitConverter.GetBytes(ls515.TempoCamp75))



        DatiFine.AddDato(DatiFineCollaudo.TipoDato.VELOCITA, BitConverter.GetBytes(ls515.TempoImmagine))

        If (ls515.fDatiScannerColore = 1) Then
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.TRIMMER300C, BitConverter.GetBytes(ls515.TrimmerValue300C))
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.TEMPOCAMP300C, BitConverter.GetBytes(ls515.TempoCamp300C))

            ''DATI SCANNER FRONTE
            'DatiFine.AddDato(DatiFineCollaudo.TipoDato.FRONTEGRIGI, Ls515Class.Periferica.BufferDumpGray)
            'DatiFine.AddDato(DatiFineCollaudo.TipoDato.FRONTERED, Ls515Class.Periferica.BufferDumpRed)
            'DatiFine.AddDato(DatiFineCollaudo.TipoDato.FRONTEGREEN, Ls515Class.Periferica.BufferDumpGreen)
            'DatiFine.AddDato(DatiFineCollaudo.TipoDato.FRONTEBLU, Ls515Class.Periferica.BufferDumpBlue)
            ''DATI SCANNER RETRO
            'DatiFine.AddDato(DatiFineCollaudo.TipoDato.RETROGRIGI, Ls515Class.Periferica.BufferDumpGrayR)
            'DatiFine.AddDato(DatiFineCollaudo.TipoDato.FRONTERED, Ls515Class.Periferica.BufferDumpRedR)
            'DatiFine.AddDato(DatiFineCollaudo.TipoDato.RETROGREEN, Ls515Class.Periferica.BufferDumpGreenR)
            'DatiFine.AddDato(DatiFineCollaudo.TipoDato.RETROBLU, Ls515Class.Periferica.BufferDumpBlueR)


            'DATI VALORI AD FRONTE RETRO
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.ADFRONTE, Ls515Class.Periferica.BufferADValues)
            DatiFine.AddDato(DatiFineCollaudo.TipoDato.ADRETRO, Ls515Class.Periferica.BufferADValuesR)

        End If


        Return ritorno
    End Function
End Class